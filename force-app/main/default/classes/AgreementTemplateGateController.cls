public with sharing class AgreementTemplateGateController {

    @testVisible public static String MASTER_ID = 'masterId';
    /*@testVisible private static Set<String> approvedClassifications {
        get {
            if (approvedClassifications == null) {
                Approved_Channel_Detail__mdt[] AppLabelList = [SELECT Label FROM Approved_Channel_Detail__mdt];
                for (Approved_Channel_Detail__mdt AppLab : AppLabelList){
                  approvedClassifications.add(AppLab.Label);            
                }
            }
            return approvedClassifications;
        }
        set;
    }
    */

    public PageReference redirectIfApproved() {
    
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        Id opportunityId = (Id) parameters.get(MASTER_ID);
        if (opportunityId == null) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR,
                'The Opportunity ID was expected in the "masterId" parameter, but wasn\'t found.'
            ));
            return null;
        }
        // Start of Modification in One CRM
        Set<String> approvedClassifications = new Set<String>();
        Approved_Channel_Detail__mdt[] AppLabelList = [SELECT Label FROM Approved_Channel_Detail__mdt];
        for (Approved_Channel_Detail__mdt AppLab : AppLabelList){
            approvedClassifications.add(AppLab.Label);            
        }
        // End of Modification in One CRM
        Opportunity thisOpportunity = [
            SELECT Approval_status__c, Channel_Detail__c
            FROM Opportunity
            WHERE Id = :opportunityId
        ];
        if (thisOpportunity.Approval_status__c != 'Approved') {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.INFO,
                Label.AgreementTemplateGate_OpportunityNotApproved
            ));
            return null;
        }
        if (!approvedClassifications.contains(thisOpportunity.Channel_Detail__c)) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.INFO,
                Label.AgreementTemplateGate_OpportunityChannelDetailNotApproved
            ));
            return null;
        }
        PageReference agreementTemplatePage = Page.echosign_dev1__AgreementTemplateProcess;
        agreementTemplatePage.getParameters().putAll(ApexPages.currentPage().getParameters());
        return agreementTemplatePage;
    }
    
}