@isTest (seeAllData=false)
public class CopyAttachmentsToEASTest {
    /*************************************************************************************************************
* Name        : CopyAttachmentsToEASTest
* Description : CR-02419: Copy Attachments from Case to External Assignment Service.   
* Input       : 
* Output      : 
*************************************************************************************************************/
    static testMethod void testAttachments(){
        Case CS = new Case();
        CS.Status = 'New';
        CS.Origin = 'Web';
        insert CS;

        List<ContentVersion> ContVerlst = new List<ContentVersion>();
        ContentVersion newcont = new ContentVersion();
        newcont.Title  = 'Capture1';
        newcont.PathOnClient ='test';
        Blob b=Blob.valueOf('Unit Test Attachment Body');
        newcont.versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body');
        ContVerlst.add(newcont);
        insert ContVerlst;
        
        List <ContentDocument> testContent = [SELECT id, Title, LatestPublishedVersionId from ContentDocument];
        ContentVersion cvRes = [select id, ContentDocumentId, ContentDocument.title from ContentVersion where id= :newcont.id];
        System.debug('CVRES'+cvRes.id);
        ContentDocumentLink condlink = new ContentDocumentLink();
        condlink.LinkedEntityId = CS.Id;
        System.debug('@@@CVRESSCS'+CS.Id);
        condlink.ContentDocumentId = cvRes.ContentDocumentId;
        System.debug('@@@CVRESS'+cvRes.ContentDocument.ID);
        condlink.ShareType = 'v';
        insert condlink;
        
        External_Assignment_Service__c EAS = new External_Assignment_Service__c();
        EAS.Case_Number__c = CS.Id;
        insert EAS;
    }
}