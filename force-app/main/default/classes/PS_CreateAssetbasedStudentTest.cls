/*******************************************************************************************************************
* Class Name       : PS_CreateAssetbasedStudentRegistration
* Version          : 1.0 
* Created Date     : 30 March 2015
* Function         : Test Class of the PS_CreateAssetbasedStudentRegistration
* Modification Log :
*
* Developer                                Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*   Accenture IDC                          9/05/2015              Created Initial Version
*    Abhinav                                9/3/2016                populating the mandatory contact fields.
*   Mani                                   07/25/2018             Replaced Qualification__c field by QuoteMigration_Qualification__c as part of ZA Apttus Decomm 
*******************************************************************************************************************/

@isTest 
/* a private class for creating student based on asset */            
private class PS_CreateAssetbasedStudentTest{
private static List<User> userlist;

/* a test method to ensure that students are created based on the criteria */
 static testMethod void myTest() { 

/*list<Account> accountdatalist = TestDataFactory.createAccount(3,'Learner');   
Insert accountdatalist;
Update accountdatalist;*/
    userlist = new List<User>();
    userlist = TestDataFactory.createUser(userinfo.getProfileId());
    insert userlist;
    
    system.runAs(userlist[0]){
    
    Account acc = new Account();
     acc.Name = 'Test';
     acc.Line_of_Business__c= 'Higher Ed';
     acc.Geography__c = 'North America';
     acc.Market2__c = 'US';
     acc.IsCreatedFromLead__c = true;
     acc.ShippingCity = 'Bangalore';
     acc.ShippingCountry = 'India';
     acc.ShippingStreet = 'Ecospace';
     acc.ShippingPostalCode = '560103';
     insert acc;     
     
     contact con = new contact();
     con.Lastname= 'testcon';
     con.Firstname= 'testcon1';
     con.MobilePhone = '9999'; 
     con.Preferred_Address__c = 'Other Address';
     con.OtherCountry  = 'India';
     con.OtherStreet = 'Test';
     con.OtherCity  = 'Test';
     con.OtherPostalCode  = '123456';
     con.accountId = acc.Id;
     //Abhinav - populating the mandatory contact fields
     con.MobilePhone = '0987654321';
     con.Salutation = 'Mr.';
     con.First_Language__c = 'English';
     insert con;
     
     Test.startTest();
  //for(Account newAcc : accountdatalist){}
     List<Opportunity> opplist = new List<Opportunity>();
     Map<Id,Opportunity> optyMap = new Map<Id,Opportunity>();
     Map<Id,Opportunity> oldMapOpty = new Map<Id,Opportunity>();
     
     Opportunity opp = new opportunity();
      opp.AccountId = acc.id;    
      opp.Name = 'OppTest';
     opp.StageName = 'Qualification';
     // opp.StageName = 'Closed';
      opp.Type = 'New Business';
      opp.Enrolment_Status__c = 'Enrolled';
      opp.CloseDate = system.today();
      opp.International_Student_Status__c = 'Yes';
      opp.QuoteMigration_Qualification__c = 'Bachelor of Arts in Psychology and Political Science Level 1, 2 & 3';
      opp.Market__c = 'US';
      opp.Academic_Vetting_Status__c='Vetted - Approved';
      
      Opportunity opp1 = new opportunity();
      opp1.AccountId = acc.id;    
      opp1.Name = 'OppTest';
      opp1.StageName = 'Qualification';
      opp1.Type = 'Returning Business';
      opp1.Enrolment_Status__c = 'Pre-enrolled';
      opp1.CloseDate = system.today();
      opp1.International_Student_Status__c = 'No';
      //opp1.QuoteMigration_Qualification__c = 'Graphic Design';
      opp1.Market__c = 'US';
      opp1.Academic_Vetting_Status__c='Vetted - Approved';
      
      Opportunity opp2 = new opportunity();
      opp2.AccountId = acc.id;    
      opp2.Name = 'OppTest';
      opp2.StageName = 'Qualification';
      opp2.Type = 'Returning Business';
      opp2.Enrolment_Status__c = 'Pre-enrolled';
      opp2.CloseDate = system.today();
      opp2.International_Student_Status__c = 'YES';
      opp2.QuoteMigration_Qualification__c = 'Psychology';
      opp2.Market__c = 'US';
      opp2.Academic_Vetting_Status__c='Vetted - Approved';
      insert opp2;
      opp2.QuoteMigration_Qualification__c = 'Graphic Design';
      update  opp2;  
        
     Opportunity opp3 = new opportunity();
      opp3.AccountId = acc.id;    
      opp3.Name = 'OppTest';
      opp3.StageName = 'Qualification';
      opp3.Type = 'New Business';
      opp3.Enrolment_Status__c = 'Enrolled';
      opp3.CloseDate = system.today();
      opp3.International_Student_Status__c = 'No';
      opp3.QuoteMigration_Qualification__c = 'Graphic Design';
      opp3.Market__c = 'US';
      opp3.Academic_Vetting_Status__c='Vetted - Approved';
      
      insert opp3;
        
        
      opplist.add(opp);
      opplist.add(opp1);
      //opplist.add(opp2);
      //opplist.add(opp3);      
      
      insert opplist;
      
    opp3.QuoteMigration_Qualification__c = 'Psychology';
    
        
    update  opp3;
    system.Debug('SyncedQuote_Qualification__c' +opp3.SyncedQuote_Qualification__c);
    opplist.add(opp2); 
    opplist.add(opp3); 
    system.Debug('SyncedQuote_Qualification__c' +opp1.SyncedQuote_Qualification__c); 
    system.Debug('SyncedQuote_Qualification__c' +opp2.SyncedQuote_Qualification__c);    
    
    Id recTypeId = [Select Id From RecordType Where SObjectType ='Task' AND Name = 'D2L' Limit 1][0].Id;
        Task tsk1 = new Task();
        tsk1.Status = 'Not Started';
        tsk1.WhatId = opp.Id;
        tsk1.RecordTypeId = recTypeId;
        tsk1.subject = 'Obtain Matric Certificate';
        tsk1.Auto_Generated__c = true;              
        insert tsk1;
          
        Task tsk2 = new Task();
        tsk2.Status = 'Not Started';
        tsk2.WhatId = opp.Id;  
        tsk2.RecordTypeId = recTypeId;      
        tsk2.subject = 'Received Signed Enrolment Contract';
        tsk2.Auto_Generated__c = true;
        insert tsk2;
          
        Task tsk3 = new Task();
        tsk3.Status = 'Not Started';
        tsk3.WhatId = opp3.Id;
        tsk3.subject = 'Student Attended Panel Interview';
        tsk3.Auto_Generated__c = true;
        insert tsk3;     

        Task tsk4 = new Task();
        tsk4.Status = 'completed';
        tsk4.WhatId = opp1.Id;
        tsk4.subject = 'Obtain Study Permit';
        tsk4.Auto_Generated__c = true;
        insert tsk4;  
        
        Task tsk5 = new Task();
        tsk5.Status = 'Not Started';
        tsk5.WhatId = opp2.Id;
        tsk5.subject = 'Obtain Study Permit';        
        tsk5.Auto_Generated__c = true;
        insert tsk5;
        
        Task tsk6 = new Task();
        tsk5.Status = 'completed';
        tsk5.WhatId = opp1.Id;
        tsk5.subject = 'Obtain Study Permit';        
        tsk5.Auto_Generated__c = true;
        insert tsk6;  
        
             
        Event evn = new Event();                
        evn.subject = 'Bachelor of Psychology Interview';
        evn.Status__c = 'To Be Scheduled';
        evn.WhatId = opp1.id;
        evn.ActivityDate = date.today();
        evn.Type = 'Qualification Interview';
        
        datetime dtstart = Datetime.newInstance(2015, 15 , 4, 00, 00, 00);
        datetime dtend = Datetime.newInstance(2015, 15 , 4, 01, 00, 00);
        evn.EndDateTime = dtend;
        evn.StartDateTime = dtstart; 
        evn.ActivityDateTime = dtstart; 
        evn.DurationInMinutes = 60;  
         
        Insert evn;
         
       PS_createAssetbasedStudentRegistration.createTask(opplist);
       PS_createAssetbasedStudentRegistration.OpportunityStage(optyMap,opplist,oldMapOpty );
       PS_createAssetbasedStudentRegistration.createTask_Method(opplist);
       PS_createAssetbasedStudentRegistration.createTask_Method1(opplist);
       PS_createAssetbasedStudentRegistration.ReturningBuisinesscreateTask(opplist);
       PS_createAssetbasedStudentRegistration.createTaskOnPsychology(opplist);
       PS_createAssetbasedStudentRegistration.ValidationOnTask(optyMap,opplist);
       PS_createAssetbasedStudentRegistration.ValidationOnTaskAndEvent(optyMap,opplist);
       
       Test.stoptest();
   }    
  }
}