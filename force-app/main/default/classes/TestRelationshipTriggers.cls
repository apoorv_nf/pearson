@isTest(SeeAllData=true)
private class TestRelationshipTriggers {

  private static testMethod void myUnitTest() {
      
      Account acc = [select Id, Name from Account Limit 1];
        Account acc1 = [select Id, Name from Account where Id !=: acc.Id Limit 1];
    
        
            Relationship__c  sRelationship      = (Relationship__c)TestClassAutomation.createSObject('Relationship__c');
            sRelationship.Account__c            = acc.Id;
            sRelationship.Related_To__c         = acc1.Id;
            sRelationship.RelationshipClone__c  = null;
            insert sRelationship;
            
            Delete sRelationship;

  }

}