/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:         	PS_MarketingBlackoutController
   Description: 	Controller for the Visualforce page which is used to control marketing blackout preferences. Visualforce page is
   					typically called from a button on the Account.   
                
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   23/10/2015       1.0           Tom Carman - Accenture                              Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */


public class PS_MarketingBlackoutController {

	public Boolean confirmed {get; set;}
	public Boolean actionImmediately {get; set;}
	public Boolean inputComplete {get; set;}
	public Date blackoutUntilDate {get; set;}
	public Date currentBlackoutUntilDate {get; set;}
	public Boolean currentlyBlackedOut {get; set;}
	public String deactivationLabel {get; set;}
	private Id accountId;
	

	public PS_MarketingBlackoutController() {

		accountId = ApexPages.currentPage().getParameters().get('accountId');
		Account account = [SELECT Id, Blackout_Flag_Account__c, Blackout_Until_Date__c FROM Account WHERE Id = :accountId];
		currentlyBlackedOut = account.Blackout_Flag_Account__c;
		currentBlackoutUntilDate = account.Blackout_Until_Date__c;
		deactivationLabel = createDeactivationLabel();
		actionImmediately = false; // default to false
		inputComplete = false; // default to false

	}


	public void doConfirm() {

		if(valid()) {
			updateAccount();

			//if(actionImmediately) {
				Database.executeBatch(new PS_BatchMarketingBlackoutAccountUpdate(accountId));
			//}

			inputComplete = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, Label.Marketing_Blackout_Success));
		} 
	}


	private Boolean valid() {

		if(accountId == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Marketing_Blackout_No_Account));
		}

		if(!currentlyBlackedOut && blackoutUntilDate == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Marketing_Blackout_Date));
		}

		if(currentlyBlackedOut && (blackoutUntilDate == null && !actionImmediately) || (blackoutUntilDate != null && actionImmediately)) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Marketing_Blackout_Date_or_Immediate));
		}
	
		if(blackoutUntilDate != null && blackoutUntilDate <= Date.today()) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Marketing_Blackout_Future_Date));			
		}

		if(!confirmed) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Marketing_Blackout_Must_Confirm));
		}

		return !ApexPages.hasMessages();
	}


	private void updateAccount() {
		Account account = new Account();
		account.Id = accountId;


		if(currentlyBlackedOut && actionImmediately) { // immediate deactivation
			account.Blackout_Until_Date__c = null; // blank previous value
			account.Blackout_Flag_Account__c = false;

		} else if (currentlyBlackedOut && !actionImmediately) { // deactivation date ammendment - only update date
			account.Blackout_Until_Date__c = blackoutUntilDate;
		
		} else if (!currentlyBlackedOut) { // activation
			account.Blackout_Flag_Account__c = true;
			account.Blackout_Until_Date__c = blackoutUntilDate;
			//actionImmediately = true;
		}

		update account;
	}


	private String createDeactivationLabel() {

		String labelDefault = Label.Marketing_Blackout_Deactivation_Description;

		if(currentBlackoutUntilDate != null) {

			String value = currentBlackoutUntilDate.format();
			String formattedLabel = String.format(labelDefault, new List<String>{value});
			return formattedLabel;

		} else {

			String value = '\'no date specified\'';
			String formattedLabel = String.format(labelDefault, new List<String>{value});
			return formattedLabel;
		}

	}




}