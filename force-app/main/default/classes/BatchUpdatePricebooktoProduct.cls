global class BatchUpdatePricebooktoProduct implements Database.Batchable<sObject>, Database.Stateful {
    global Set<ID> productID;
    global BatchUpdatePricebooktoProduct(){
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        productID = new Set<ID>();
        return Database.getQueryLocator('Select PriceBook2.Name, Product2.Id, Product2.Name, Name From PricebookEntry WHERE  PriceBook2.Name IN (\'HE Pricebook Austria\',\'HE Pricebook Benelux\',\'HE Pricebook Germany\',\'HE Pricebook Switzerland\',\'HE Pricebook France\') ');
    }
    global void execute(Database.BatchableContext BC, List<PricebookEntry> PricebookEntryList){
        Map<ID,product2> mapProducts = new Map<ID,product2>();         
        for (PricebookEntry PBentry : PricebookEntryList){
            product2 updateProduct = [select ID,Name,Pricebook__c from Product2 where id = :PBentry.Product2.Id];
            system.debug('Pricebook Name ---> '+PBentry.PriceBook2.Name);
            system.debug('Product id ---> '+updateProduct.ID);
            If  (String.isEmpty(updateProduct.Pricebook__c)){                
                updateProduct.Pricebook__c =  PBentry.PriceBook2.Name;
                mapProducts.put(updateProduct.Id,updateProduct);
                productID.add(updateProduct.ID);
            }
            else{
                if (productID.contains(updateProduct.ID)) {
                    if (!(updateProduct.Pricebook__c.contains(PBentry.PriceBook2.Name))) {
                        updateProduct.Pricebook__c = updateProduct.Pricebook__c + ',' + PBentry.PriceBook2.Name;
                        mapProducts.put(updateProduct.Id,updateProduct);
                    }
                }
                else{
                    updateProduct.Pricebook__c =  PBentry.PriceBook2.Name;
                    productID.add(updateProduct.ID);
                    mapProducts.put(updateProduct.Id,updateProduct);
                }
            }
        }
        update mapProducts.values();
    }
    
    global void finish(Database.BatchableContext BC){

   }  

}