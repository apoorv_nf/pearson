/*******************************************************************************************************************
* Apex Class Name  : PS_leadidvalidation 
* Version          : 1.0
* Created Date     : 21 January 2016
* Function         : To check valid identification number on lead
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
* Karthik.A.S              21/01/2016              To check valid identification number on lead
* --------------------------------------------------------------------------------------------------------------------
*******************************************************************************************************************/
public class PS_leadidvalidation {
   
//Commenmted by Raja Gopalarao Bekkam as part of CR-02923
 public static void dummy(){        
    }
 /**static list < String > finalresult;
 static List < string > evenIdFromCart = new List < string > ();
 static List < string > oddIdFromCart = new List < string > ();
 
//Commenmted by Raja Gopalarao Bekkam as part of CR-02923
 public static void leadidvalidation(list < lead > idlist) {

  for (lead l: idlist) {

if(l.Identification_Number__c!=null ){

     
  
   string leadidentifiactionnumber = l.Identification_Number__c;
  if (leadidentifiactionnumber.length() > 0) {


    string odd='';
    string even='';
    Integer seq = 1;
    string leadidlastdigit = '';
    oddIdFromCart.clear();
    evenIdFromCart.clear();

    list < String > cartbreak = new list < String > ();
    cartbreak = leadidentifiactionnumber.split('');

    for (Integer i = 0; i < cartbreak.size(); i++) {
     String leadidspilt = cartbreak[i];
     leadidlastdigit = cartbreak.get(cartbreak.size() - 1);
     
     if (leadidspilt != 'null' && leadidspilt != '') {
      if ((math.mod(i, 2)) == 0) {
       odd = string.valueof(cartbreak[i]);
       oddIdFromCart.add(odd);


       //List<string> even1=new List<string>();
       // string> even =concat(productIdFromCart);

      } else {
       even = string.valueof(cartbreak[i]);
       evenIdFromCart.add(even);

       seq++;
      }

     }
    }

    String evenId = '';
    for (Integer i = 0; i < evenIdFromCart.size(); i++) {

     evenId = evenId + evenIdFromCart[i];



    }
    Integer evenspilt1 = integer.valueof(evenId);
    integer evenspilt2 = evenspilt1 * 2;
    String evenspilt3 = '';
    evenspilt3 = evenspilt3 + evenspilt2;


    list < String > leadevencombine = new list < String > ();
    leadevencombine = evenspilt3.split('');
    //list<string>int4= integer.valueof(cartbreak1);

    Integer leadevendigitadding = 0;
    for (Integer i = 0; i < leadevencombine.size(); i++) {

     leadevendigitadding = leadevendigitadding + integer.valueof(leadevencombine[i]);

    }
    Integer leadOdddigitadding = 0;
    for (Integer i = 0; i < oddIdFromCart.size(); i++) {

     leadOdddigitadding = leadOdddigitadding + integer.valueof(oddIdFromCart[i]);

    }
    string leadoddlastpostion = oddIdFromCart.get(oddIdFromCart.size() - 1);
    integer leadoddlastpostionint = integer.valueof(leadoddlastpostion);
    integer leadoddlast=leadOdddigitadding-leadoddlastpostionint;
    
    integer leadevenadd;
     leadevenadd = leadoddlast + leadevendigitadding;
   // leadevenadd = leadevendigitadding + leadOdddigitadding;
    String int9 = string.valueof(leadevenadd);

    list < String > leadevenaddresult = new list < String > ();
    leadevenaddresult = int9.split('');
    string leadevenaddresultstr = leadevenaddresult.get(leadevenaddresult.size() - 1);
    integer leadevenaddresultint = integer.valueof(leadevenaddresultstr);
    integer leadevenaddfinalresult = 10 - leadevenaddresultint;
    string leadvalidid = string.valueof(leadevenaddfinalresult);
    //list<String>fianl3 = new list<String>();
    finalresult = leadvalidid.split('');
    if (finalresult.size() > 1)
     {
     string e4 = finalresult.get(finalresult.size() - 1);
     if (e4 != leadidlastdigit) 
      {
      l.addError('Please Enter vaild National Identity Number');

      }

     } 
     else 
     {
      if ((leadvalidid != leadidlastdigit))
      {
      l.addError('Please Enter vaild National Identity Number');
      }
   
}
  
   }
  
  }
 
 }
 }**/
}