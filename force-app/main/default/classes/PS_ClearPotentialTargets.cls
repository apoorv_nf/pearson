/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER        DESCRIPTION                               
 |  ====            =========        =========== 
 |  23/10/2015        IDC              This Class is used for deletion 
                                     of potential target records
                     
 +===========================================================================*/

global class PS_ClearPotentialTargets{

    webservice static Integer clearPotentialTargets(){
    //Set for PT IDs 
    List<Id> PTlistSet = new List<Id>();
    List<Id> asyncPTlistSet = new List<Id>();
    integer ptcounter=0;
    integer result;
    Database.DeleteResult[] DelPTList;
    List<Generate_Potential_Target__C> PTlist = New List<Generate_Potential_Target__C>();
    PTlist = [Select id from Generate_Potential_Target__c where CreatedById =:UserInfo.getUserId() limit 8000];
    System.debug('PTList size  --->'+PTList.size());
    try {
         /* if(!PTlist.isEmpty()){
            for(Generate_Potential_Target__C deletePTlist : PTlist){
               if (ptcounter <= 9000){
                     ptcounter++;
                     PTlistSet.add(deletePTlist.id);
                }
                else{
                    asyncPTlistSet.add(deletePTlist.id);
                } 
                result = asyncPTlistSet.size(); 
               // system.debug('PTlistSet size -->'+PTlistSet.size());                  
               // system.debug('asyncPTlistSet size -->'+asyncPTlistSet.size());
            }
        } */
        if (PTlist!= null){  // PTlistSet
            DelPTList = Database.delete(PTlist, false);   //PTlistSet
            if(DelPTList!= null){
                List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
                for (Database.DeleteResult dr : DelPTList) {
                    String ErrMsg='';
                    if (!dr.isSuccess() || Test.isRunningTest()){
                        PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                        errlogger.InterfaceName__c='ClearPotentialTargets - delete';
                        errlogger.ApexClassName__c='PS_ClearGeneratePotentialTargets';
                        errlogger.CallingMethod__c='clearPotentialTargets';
                        errlogger.UserLogin__c=UserInfo.getUserName(); 
                        errlogger.recordid__c=dr.getId();
                        for(Database.Error err : dr.getErrors()) 
                          ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage(); 
                        errlogger.ExceptionMessage__c=  ErrMsg;  
                        errloggerlist.add(errlogger);    
                    }
                }
                if(errloggerlist.size()>0){insert errloggerlist;}
              } //DelPTList close  
          } //PTListset if close
   
         /* if(asyncPTlistSet.size()>0){
             System.enqueueJob(new PS_ClearPotentialTargetQueuable(Userinfo.getUserid(),asyncPTlistSet));
         } */
             
     }catch(Exception e){
         ExceptionFramework.LogException('ClearPotentialTarget','PS_ClearPotentialTarget','execute',e.getMessage(),UserInfo.getUserName(),'');
      }
      return DelPTList.size();  //result;  
    } //method closed

} //class closed