/* 
Name: PS_QACaseReviewController
Description:Visual Force Controller for creating QA Feedback
Test class: mcreateQAReview
 Date     Version      Author     Summary of Changes 
 ---------------------------------------------------------------------------
 7-jan-2016  1.0       Tejaswini    RD-1627 Creating QA Feedback
 30-Mar-2016	1.1		Tejasvini	Defect-4700
*/

public class PS_QACaseReviewController {

     public ApexPages.StandardController controller;
     public String retURL {get; set;}
     public String sCaseId{get;set;}
     public String caseownerid{get;set;}
     public ID rtID{get;set;}  
     Schema.DescribeSObjectResult d = Schema.SObjectType.Quality_Review__c;
     public string tabId;
         
    //constructor to initilaise CaseId 
 public PS_QACaseReviewController (ApexPages.StandardController controller){
        controller = controller;
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        sCaseId=ApexPages.currentPage().getParameters().get('caseId');
       // caseownerid=ApexPages.currentPage().getParameters().get('case.ownerid');
        
    }
    //method to populateing fields and viewing page
    public PageReference mcreateQAReview(){
        case c=[Select CaseNumber,Ownerid,Owner.Name,RecordType.Name from case where id=:sCaseId];
        PageReference returnURL= new Pagereference (Label.PS_QRTabId+'/e?');
        returnURL.getParameters().put('retURL', retURL);
        returnURL.getParameters().put(Label.PS_CaseId,c.CaseNumber);
        //User u=[Select Id,UserType from User];
        for(User u :[Select Id,UserType from User where ID=:c.Ownerid]){
        if(u.UserType =='Standard'){
            returnURL.getParameters().put(Label.PS_Agent_To_Be_Reviewed,c.Owner.Name);
        }
        else{
            returnURL.getParameters().put('',c.Owner.Name);
        }
        
        }
        returnURL.getParameters().put(Label.PS_Reviewed_By,UserInfo.getName());
        
        //Map for QA RecordtypeId
         Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
         if(c.RecordType.Name == Label.CaseCSRecordType){
             rtID =  rtMapByName.get(Label.PS_QA_Feedback_CS).RecordTypeID; 
         }else if(c.RecordType.Name == Label.CaseHeTechSupportRecordType){
             system.debug('*** '+rtMapByName );
             rtID =  rtMapByName.get(Label.PS_QA_Feedback_TS).RecordTypeID; 
         }else{
             rtID =  rtMapByName.get(Label.PS_QA_Feedback_S_N).RecordTypeID; 
         }
         returnURL.getParameters().put('RecordType',rtID);
        return returnURL;
    }
   
}