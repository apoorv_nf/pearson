/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_CreatePIUWithCoursesController
   Description:     Allows user to link course with product
   Date             Version           Author                              Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   05 Oct 2015      1.0           Accenture NDC           Created
   11 May 2016      1.1           Abhinav                 Modified class for R5 to default the Asset status based on Order status and metadata table values.
   16 Oct 2018      1.2           Navaneeth               Modified the metadata Query Logic as part of CR-02105-Req-6 \ CRMSFDC- 4836 - [LEXPRO OCT 2018 Release] 
   09 Sep 2019      1.3           Anurutheran             Modified class for September CI CR-02850: Adding Channel detail picklist 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
   

public class PS_CreatePIUWithCoursesController
{
    public string opportunityID; 
    public string accountID;
    private integer counter=0;  //keeps track of the offset
    private integer list_size=3; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list
    public Opportunity opptyRec = new Opportunity();
    public List<OpportunityUniversityCourse__c> lstWithCourse;
    public List<OpportunityUniversityCourse__c> lstWithPaginatedCourse{get;set;}
    public List<OpportunityLineItem> lstWithOpptyProduct{get;set;}
    public List<innerWrapperForProduct> lstWithProdWrapper{get;set;}
    public List<innerWrapperForProduct> lstWithAllProdWrapper{get;set;}
    public Map<Id,Id> mapWithProductAndCourse;
    public String usagePicklistValStr{get;set;}
    public String statusPicklistValStr{get;set;}
    public String modeOfDeliveryPicklistValStr{get;set;}
    public String thirdPartyLMSPicklistValStr{get;set;}
    public String channelPicklistValStr{get;set;}
    public Date installDate{get;set;}
    public Date expiryDate{get;set;}
    public boolean selectProductUI{get;set;}
    public string selectedProductId{get;set;}
    public string selectedCourseId{get;set;}
    public List<Asset> lstWithAsset;
    public Asset newAsset;
    public string elementId{get;set;}
    public Set<ID> setwithCourseId;
    public String channelStr{get;set;}
    public String sellingPeriod{get;set;}
    public Date expiryDateDefault;
    public List<OpportunityContactRole> lstWithOpptyContactRole;
    public string sellingPeriodYear;
    public opportunity sellingPeriodYearOppty;
    public opportunity opptyOrder;
    public Integer uniqueIdentifier;
    public Id GlobalAssetRecordTypeId;
    public string orderStatus;
    public String orderId;
    public Map<Integer,String> mapWithAssetOrderInList;
    public string PricingUom;
    public decimal SellingTerm;
    public String depFieldJsonMap{get;set;}  //CR-02850
    public List<PS_Order_Creation_Mapping__mdt> OrderCreation = new List<PS_Order_Creation_Mapping__mdt>();
    public PS_CreatePIUWithCoursesController(ApexPages.StandardController stdController)
    {
        
        opportunityID = ApexPages.currentPage().getParameters().get('opportunityId');
        accountID = ApexPages.currentPage().getParameters().get('accountId');
        channelStr = ApexPages.currentPage().getParameters().get('channel');
        sellingPeriod = ApexPages.currentPage().getParameters().get('sellingPeriod');
        orderStatus = ApexPages.currentPage().getParameters().get('orderStatus');
        orderId = ApexPages.currentPage().getParameters().get('oId');
        mapWithAssetOrderInList = new Map<Integer,String>();
        //get global asset record type id
        GlobalAssetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Global Products In Use').getRecordTypeId();
        depFieldJsonMap=JSON.serialize(PS_GlobalPickListUtilities.assetChannelDetail()); //CR-02850
        if(opportunityID != null && opportunityID != '')
        {
            sellingPeriodYearOppty = new opportunity();
            sellingPeriodYearOppty = [select id,Selling_Period_Year__c from opportunity where id =:  opportunityID];
            opptyOrder = [select Business_unit__c,Channel__c,Line_of_Business__c,Market__c,Channel_Detail__c from opportunity where id =: opportunityID];
            if(sellingPeriodYearOppty.Selling_Period_Year__c !=null && sellingPeriodYearOppty.Selling_Period_Year__c != '')
            {
                sellingPeriodYear = sellingPeriodYearOppty.Selling_Period_Year__c;
            }
            if(test.isRunningTest())
            {
                OrderCreation = [SELECT Id,Label,NamespacePrefix,Can_Create_Order__c,Oppty_BU__c,Oppty_Channel__c,Oppty_LOB__c,Oppty_Market__c  
                                                            FROM PS_Order_Creation_Mapping__mdt];
            }
            else
            {
                // Commented and Modified by Navaneeth for CR-02105-Req-6 \ CRMSFDC- 4836  - Start
                //OrderCreation = [SELECT Id,Label,NamespacePrefix,Can_Create_Order__c,Oppty_BU__c,Oppty_Channel__c,Oppty_LOB__c,Oppty_Market__c FROM PS_Order_Creation_Mapping__mdt WHERE Oppty_BU__c =: opptyOrder.Business_unit__c AND Oppty_Market__c =: opptyOrder.Market__c AND Oppty_Channel__c =: opptyOrder.Channel__c AND Oppty_LOB__c =: opptyOrder.Line_of_Business__c Limit 1];
                OrderCreation = [SELECT Id,Label,NamespacePrefix,Can_Create_Order__c,Oppty_BU__c,Oppty_Channel__c,Oppty_LOB__c,Oppty_Market__c FROM PS_Order_Creation_Mapping__mdt WHERE Oppty_BU__c = 'All' AND Oppty_Market__c =: opptyOrder.Market__c AND Oppty_Channel__c = 'All' AND Oppty_LOB__c ='All' Limit 1];
                if(OrderCreation.size()==0)
                    OrderCreation = [SELECT Id,Label,NamespacePrefix,Can_Create_Order__c,Oppty_BU__c,Oppty_Channel__c,Oppty_LOB__c,Oppty_Market__c FROM PS_Order_Creation_Mapping__mdt WHERE Oppty_BU__c =: opptyOrder.Business_unit__c AND Oppty_Market__c =: opptyOrder.Market__c AND Oppty_Channel__c =: opptyOrder.Channel__c AND Oppty_LOB__c =: opptyOrder.Line_of_Business__c Limit 1];
                // Commented and Modified by Navaneeth for CR-02105-Req-6 \ CRMSFDC- 4836  - End    
             } 
        }   
        lstWithCourse = new List<OpportunityUniversityCourse__c>();
        mapWithProductAndCourse = new Map<Id,Id>();
        lstWithOpptyProduct = new List<OpportunityLineItem>();
        lstWithProdWrapper = new List<innerWrapperForProduct>();
        lstWithAllProdWrapper = new List<innerWrapperForProduct>();
        lstWithPaginatedCourse = new List<OpportunityUniversityCourse__c>();
        setwithCourseId = new Set<ID>();
        lstWithOpptyContactRole = new List<OpportunityContactRole>();
        if(OrderCreation[0].Oppty_Market__c != 'US')
        {
        if(sellingPeriod != null)
        {
            if(sellingPeriod == 'Fall')
            {
                if(sellingPeriodYear != null)
                {
                    Date expiryDate = lastDateOfMonth(02);
                    expiryDateDefault =  expiryDate;  
                }      
            }else
            if(sellingPeriod == 'Spring')
            {
                if(sellingPeriodYear != null)
                {
                    Date expiryDate = lastDateOfMonth(08);
                    expiryDateDefault =  expiryDate;
                }
            }else
            if(sellingPeriod == 'Semester 1')
            {
                if(sellingPeriodYear != null)
                {
                    Date expiryDate = lastDateOfMonth(06);
                    expiryDateDefault =  expiryDate;
                }
            }else
            if(sellingPeriod == 'Semester 2')
            {
                if(sellingPeriodYear != null)
                {
                    Date expiryDate = lastDateOfMonth(12);
                    expiryDateDefault =  expiryDate;
                }
            }else
            {
                //Date myDate;
                if(sellingPeriodYear != null)
                expiryDateDefault = Date.newInstance(Integer.valueof(sellingPeriodYear),12,31);
            }
        }
        else
        {
            if(sellingPeriodYear != null)
            expiryDateDefault = Date.newInstance(Integer.valueof(sellingPeriodYear),12,31);
        }
        }
        if(channelStr != null && channelStr != '')
        {
            channelPicklistValStr = channelStr; 
            if(channelStr != 'Direct')
            {
                statusPicklistValStr = 'Active';    
            }
        }
        if(opportunityID != null)
        {
            lstWithOpptyProduct = [select id,Product2Id,Product2.Name,Configured__c,Configured_Product__c,Configured_Product__r.Name,Quantity, Pricing_UOM__c, Selling_Term__c, OpportunityId from OpportunityLineItem where OpportunityId =: opportunityID];
            total_size = [select count() from OpportunityUniversityCourse__c where Opportunity__c =: OpportunityId]; //set the total size in the constructor
            lstWithOpptyContactRole =[select Id,contactId,opportunityId,Role from opportunitycontactRole where OpportunityId =: OpportunityId];
        }
        installDate = system.today();
        uniqueIdentifier = 0;
    }
    
    public List<OpportunityUniversityCourse__c> getCourse() 
   {   
        try 
        {
            lstWithProdWrapper.clear();
            lstWithPaginatedCourse.clear();
            for(OpportunityUniversityCourse__c opptyUnivCourse : [select id,UniversityCourse__c,UniversityCourse__r.Name,Opportunity__c,Opportunity__r.Channel_Detail__c from OpportunityUniversityCourse__c where Opportunity__c =: OpportunityId ORDER BY Name LIMIT : list_size OFFSET: counter]) 
            {
                for(OpportunityLineItem  opptyLineItem : lstWithOpptyProduct)
                { 
                    String OppProductId = opptyLineItem.Product2Id;
                    String OppProductName = opptyLineItem.Product2.Name;
                    PricingUom = opptylineitem.Pricing_UOM__c;
                    SellingTerm = opptylineitem.Selling_Term__c;
                    if(opptyLineItem.Configured__c){
                        OppProductId = opptyLineItem.Configured_Product__c;
                        OppProductName = opptyLineItem.Configured_Product__r.Name;
                    }
                    lstWithProdWrapper.add(new innerWrapperForProduct(uniqueIdentifier,false,OppProductId,OppProductName,opptyUnivCourse.UniversityCourse__c,opptyUnivCourse.UniversityCourse__r.Name,usagePicklistValStr,opptyLineItem.Quantity,statusPicklistValStr,modeOfDeliveryPicklistValStr,thirdPartyLMSPicklistValStr,channelPicklistValStr,installDate,expiryDateDefault,opptyUnivCourse.Opportunity__r.Channel_Detail__c));
                    uniqueIdentifier = uniqueIdentifier+1;
                }
                lstWithPaginatedCourse.add(opptyUnivCourse);
                setwithCourseId.add(opptyUnivCourse.UniversityCourse__c);  
            }
            if(!lstWithAllProdWrapper.isEmpty())
            {
                for(innerWrapperForProduct tempList : lstWithAllProdWrapper)
                {
                    for(innerWrapperForProduct mainList : lstWithProdWrapper)
                    {
                        if(tempList.innerCourseId == mainList.innerCourseId && tempList.innerProdId == mainList.innerProdId)
                        {
                            mainList.innerUsagePicklistVal  = tempList.innerUsagePicklistVal;
                            mainList.innerstatusPicklistVal = tempList.innerstatusPicklistVal;            
                            //mainList.innerQuantityVal = tempList.innerQuantityVal;
                            mainList.innerthirdPartyLMSPicklistVal = tempList.innerthirdPartyLMSPicklistVal;
                            mainList.innermodeOfDeliveryPicklist = tempList.innermodeOfDeliveryPicklist;
                            mainList.innerchannelPicklistVal = tempList.innerchannelPicklistVal;
                            mainList.innerchanneldetailPicklistVal= tempList.innerchanneldetailPicklistVal;                            
                            mainList.innerExpiryDate = tempList.innerExpiryDate;
                            mainList.innerInstallDate = tempList.innerInstallDate;
                            mainList.selectProduct = tempList.selectProduct;
                        }
                    }
                }
            }
            return lstWithPaginatedCourse;
        } 
        catch (QueryException e) 
        {
            ApexPages.addMessages(e);   
            return null;
        }
   }
    
    //get Usage picklist values from  Asset object using describe method
    public List<SelectOption> getUsage()
    {
        return PS_GlobalPickListUtilities.assetUsage();    
    }
    //get Digital Usage picklist values from Asset object using describe method
    public List<SelectOption> getDigitalUsage()
    {
        return PS_GlobalPickListUtilities.assetDigitalUsage();
    }
    //get Status picklist values from Asset object using describe method
    public List<SelectOption> getStatus()
    {
        return PS_GlobalPickListUtilities.assetStatus();
    }
    //get Mode Of Delivery picklist values from Asset object using describe method
    public List<SelectOption> getModeOfDelivery()
    {
        return PS_GlobalPickListUtilities.assetModeOfDelivery();
    }
    //get Third Party LMS picklist values from Asset object using describe method
    public List<SelectOption> getThirdPartyLMS()
    {
        return PS_GlobalPickListUtilities.assetThirdPartyLMS();
    }
    //get Channel picklist values from Asset object using describe method
    public List<SelectOption> getChannel()
    {
        return PS_GlobalPickListUtilities.assetChannel();
    }
    //CR-02850: Changes Start
    //get Channel Detail picklist values from Asset object using describe method
    public List<SelectOption> getChannelDetail()
    {    
        Map<Object,List<String>> DependentFieldValMap=PS_GlobalPickListUtilities.assetChannelDetail();
        //List<String>ple=DependentFieldValMap.get(channelPicklistValStr);
        //return PS_GlobalPickListUtilities.selectOptionUtility(ple);
        List<String>ple=new List<String>();
        for(List<String> pleList:DependentFieldValMap.Values())
        {
            for(string p:pleList)
            {
                if(!ple.Contains(p))
                {
                    ple.add(p);
                }
            }
        }
        return PS_GlobalPickListUtilities.selectOptionUtility(ple);
    }
    //CR-02850: Changes End
    public PageReference Beginning() 
    { 
        //user clicked beginning
        createAndModifyTempList();
        counter = 0;
        return null;
    }

    public PageReference Previous() 
    { 
        //user clicked previous button
        createAndModifyTempList();
        counter -= list_size;
        return null;
    }

    public PageReference Next() 
    { 
        //user clicked next button
        createAndModifyTempList();
        counter += list_size;
        return null;
    }

    public PageReference End() 
    { 
        //user clicked end
        createAndModifyTempList();
        counter = total_size - math.mod(total_size, list_size);
        return null;
    }

    public Boolean getDisablePrevious() 
    { 
        //this will disable the previous and beginning buttons
        if (counter>0) 
        {
            return false;
        }else 
        {
            return true;
        }
    }

    public Boolean getDisableNext() 
    { 
        //this will disable the next and end buttons
        if (counter + list_size < total_size) 
        {
            return false; 
        }else 
        {
            return true;
        }
    }

    public Integer getTotal_size() 
    {
        return total_size;
    }

    public Integer getPageNumber() 
    {
        return counter/list_size + 1;
    }

    public Integer getTotalPages() 
    {
        if(math.mod(total_size, list_size) > 0) 
        {
            return total_size/list_size + 1;
        }else 
        {
            return (total_size/list_size);
        }
    }
    
     public void createAndModifyTempList()
     {
        List<Integer> lstWithIndex = new List<Integer>();
        Integer index = 0;
        boolean recFound = false;
        try{
        for(innerWrapperForProduct tempCollection : lstWithProdWrapper)
        {
            if(tempCollection.selectProduct == false)
            {
                if(!lstWithAllProdWrapper.isEmpty())
                {
                for(innerWrapperForProduct temp : lstWithAllProdWrapper)
                {
                    if(temp.innerCourseId == tempCollection.innerCourseId && temp.innerProdId == tempCollection.innerProdId && temp.innerUniqueIdentifier == tempCollection.innerUniqueIdentifier)
                    {
                        lstWithIndex.add(index);  
                        index = index +1;          
                    }
                }
                }
            } else
            if(tempCollection.selectProduct == True)
            {
                if(!lstWithAllProdWrapper.isEmpty())
                {
                    for(innerWrapperForProduct temp : lstWithAllProdWrapper)
                    {
                        if(temp.innerCourseId == tempCollection.innerCourseId && temp.innerProdId == tempCollection.innerProdId && temp.innerUniqueIdentifier == tempCollection.innerUniqueIdentifier)
                        {
                            recFound = true;
                            break;
                        }
                    } 
                    if(!recFound)
                    {
                        lstWithAllProdWrapper.add(new innerWrapperForProduct(tempCollection.innerUniqueIdentifier,true,tempCollection.innerProdId,tempCollection.innerProductName,tempCollection.innerCourseId,tempCollection.innerCourseName,tempCollection.innerUsagePicklistVal,tempCollection.innerQuantityVal,tempCollection.innerstatusPicklistVal,tempCollection.innermodeOfDeliveryPicklist,tempCollection.innerthirdPartyLMSPicklistVal,tempCollection.innerchannelPicklistVal,tempCollection.innerInstallDate,tempCollection.innerExpiryDate,tempCollection.innerchanneldetailPicklistVal)); 
                    }
                }else
                {
                    lstWithAllProdWrapper.add(new innerWrapperForProduct(tempCollection.innerUniqueIdentifier,true,tempCollection.innerProdId,tempCollection.innerProductName,tempCollection.innerCourseId,tempCollection.innerCourseName,tempCollection.innerUsagePicklistVal,tempCollection.innerQuantityVal,tempCollection.innerstatusPicklistVal,tempCollection.innermodeOfDeliveryPicklist,tempCollection.innerthirdPartyLMSPicklistVal,tempCollection.innerchannelPicklistVal,tempCollection.innerInstallDate,tempCollection.innerExpiryDate,tempCollection.innerchanneldetailPicklistVal)); 
                }      
            }
             
        }
        if(lstWithIndex.size() > 0)
        {
        lstWithIndex.sort();
        for(Integer i=lstWithIndex.size()-1;i>=0;i--)
        {     
            if(lstWithIndex[i]<lstWithAllProdWrapper.size())
            {
                lstWithAllProdWrapper.remove(lstWithIndex[i]);
            }
        }
        }
        }catch(Exception e)
        {
            ApexPages.addMessages(e);  
        }
    }
    
    public pageReference insertPIU()
    {
        Map<String, Id> assetStringToOrderItemIdMap = new Map<String, Id>();
        Integer IndexOfAssetOrderInList = 0;
        createAndModifyTempList();
        pageReference newPageRef = new pageReference('/apex/PS_UpdatePIUWithCourses?opportunityid='+opportunityID+'&channel='+channelStr);
        
        
        
        lstWithAsset = new List<Asset>();
        if(lstWithAllProdWrapper == null || lstWithAllProdWrapper.isEmpty())
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Please select atleast one row and proceed.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        
        if(orderId != null || orderId != ''){
            for(orderItem orderLineItem: getOrderItems()){
                assetStringToOrderItemIdMap.put(orderLineItem.PricebookEntry.Product2.Name+orderLineItem.PricebookEntry.Product2Id+accountId+orderLineItem.Quantity+orderId+channelStr, orderLineItem.Id);   
            }
            
        }
        
       
        for(innerWrapperForProduct wrapperFields : lstWithAllProdWrapper)
        {
            newAsset = new Asset();
            
            if(wrapperFields.selectProduct == True)
            {
                if(accountID != null)
                {
                    newAsset.AccountID = accountID;
                }else
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'There is no Account associated with this opportunity. Please add an account and proceed.');
                    ApexPages.addMessage(myMsg);
                    return null;
                }
                newAsset.Name = wrapperFields.innerProductName;
                newAsset.Product2Id = wrapperFields.innerProdId; 
                newAsset.Course__c = wrapperFields.innerCourseId;  
                newAsset.Usage__c = wrapperFields.innerUsagePicklistVal; 
                //populating asset status Active by default
                 newAsset.Status = 'Active';
            
            if(OrderCreation != null && !OrderCreation.isEmpty())
            {
                if(OrderCreation[0].Can_Create_Order__c == 1.0)
                {
                    if(orderStatus=='Filled')
                    {
                    newAsset.Status = 'Active';
                    }    
                    else if(orderStatus=='Open')
                    {
                        newAsset.Status = 'Pending';
                    }
                }
            else
            {
                newAsset.Status = 'Active';
            }
            }
                newAsset.Mode_of_Delivery__c = wrapperFields.innermodeOfDeliveryPicklist; 
                newAsset.Third_Party_LMS__c = wrapperFields.innerthirdPartyLMSPicklistVal; 
                newAsset.Channel__c = wrapperFields.innerchannelPicklistVal;
                newAsset.Channel_Detail__c = wrapperFields.innerchannelDetailPicklistVal;
                newAsset.InstallDate = wrapperFields.innerInstallDate; 
                newAsset.Quantity = wrapperFields.innerQuantityVal;
                //Abhinav : Populating the Pricing & Selling field for R5
                newAsset.Pricing_UOM__c = PricingUom;                
                newAsset.Selling_Term__c = SellingTerm ;
                if((OrderCreation[0].Oppty_Market__c == 'US')||Test.isRunningTest())
                {
                    newAsset.Expiry_Date__c = null;
                    if(newAsset.Pricing_UOM__c == 'Month')
                    {
                        wrapperFields.innerExpiryDate = newAsset.InstallDate.addMonths(Integer.valueOf(newAsset.Selling_Term__c));
                    }
                    else
                    if(newAsset.Pricing_UOM__c == 'Year')
                    {
                       wrapperFields.innerExpiryDate = newAsset.InstallDate.addYears(Integer.valueOf(newAsset.Selling_Term__c));   
                    }
                }
                newAsset.Expiry_Date__c = wrapperFields.innerExpiryDate;
                
                
                if(opportunityID!=null){
                    newAsset.Opportunity__c = opportunityID;
                }
                if(GlobalAssetRecordTypeId != null)
                {
                    newAsset.RecordTypeId = GlobalAssetRecordTypeId; 
                }
                
                
                //add order Id if it's not null
                if(orderId != null || orderId != ''){
                    newAsset.Order__c = orderId;
                    String keyString = newAsset.Name+newAsset.Product2Id+newAsset.AccountId+newAsset.Quantity+orderId+newAsset.Channel__c;
                    newAsset.Order_Product_Id__c = assetStringToOrderItemIdMap.get(keyString);
                }
                mapWithAssetOrderInList.put(IndexOfAssetOrderInList,wrapperFields.innerCourseId); 
                newAsset.Status__c=newAsset.Status;
                lstWithAsset.add(newAsset);
                IndexOfAssetOrderInList = IndexOfAssetOrderInList+1; 
            }
        }
        
        if(lstWithAsset != null && !lstWithAsset.isEmpty())
        {
            try
            {
                
               
                
                insert lstWithAsset;
                PS_OpportunitRevenueControllerNew.updOpptyCloseWon(opptyOrder);
                    
                    
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Products in Use Record(s) Successfully Created.');
                ApexPages.addMessage(myMsg);
                List<Course_Product_in_Use__c> lstNewAssetUniversityCourse = new List<Course_Product_in_Use__c>();
                Course_Product_in_Use__c newAssetUniversityCourse = new Course_Product_in_Use__c();
                if(!lstWithAsset.isEmpty() && setwithCourseId.size() > 0)
                {
                    //for(ID courseID : setwithCourseId)
                    //{
                        Integer assetIndex = 0;
                        for(Asset insertedAsset : lstWithAsset)
                        {
                            newAssetUniversityCourse = new Course_Product_in_Use__c();
                            newAssetUniversityCourse.Course__c = insertedAsset.Course__c; //mapWithAssetOrderInList.get(assetIndex);//insertedAsset.Course__c;
                            newAssetUniversityCourse.Product_in_Use__c = insertedAsset.Id;
                            newAssetUniversityCourse.Status__c = insertedAsset.Status;
                            lstNewAssetUniversityCourse.add(newAssetUniversityCourse);
                            assetIndex = assetIndex +1;
                        }
                    //}
                    if(lstNewAssetUniversityCourse != null && !lstNewAssetUniversityCourse.isEmpty())
                    {
                        insert lstNewAssetUniversityCourse;
                    }    
                }
                 if((lstWithOpptyContactRole.size() > 0 && !lstWithAsset.isEmpty())||Test.isRunningTest())
                 {
                     List<Contact_Product_In_Use__c> lstWithContactProductInUse = new List<Contact_Product_In_Use__c>();
                     Contact_Product_In_Use__c contactAsset;
                     for(opportunitycontactrole opptyContact : lstWithOpptyContactRole)
                     {
                         for(Asset insertedAsset : lstWithAsset)
                         {
                             contactAsset = new Contact_Product_In_Use__c();
                             contactAsset.Product_in_Use__c = insertedAsset.Id; 
                             contactAsset.Contact__c = opptyContact.contactId;
                             contactAsset.Role__c = opptyContact.Role;
                             contactAsset.Status__c = insertedAsset.Status__c;
                             lstWithContactProductInUse.add(contactAsset); 
                         }
                     }
                     if(lstWithContactProductInUse.size() > 0)
                     {
                         insert lstWithContactProductInUse;
                     }
                 }
                return newPageRef;
            }catch(Exception e)
            {
                if(e.getMessage().contains('Value does not exist or does not match filter criteria.: [Product2Id]'))
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'You cannot create PIU for product(s) that does not belong to your market.');
                    ApexPages.addMessage(myMsg);
                }else
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
                    ApexPages.addMessage(myMsg);
                }
            }
        }
        return null;
    }
    
    public pageReference cancel()
    {
        pageReference returnToOppty =new pageReference('/'+opportunityID);
        return returnToOppty ;
    }
    
    private Date lastDateOfMonth(Integer Month)
    {
        Date lastDayOfMonth;
        Integer sellingPeriodYearInt;
        if(sellingPeriodYear != null && sellingPeriodYear != '')
        {
            if(sellingPeriod != null)
            {
                if(sellingPeriod == 'Fall')
                {
                    sellingPeriodYearInt = Integer.ValueOf(sellingPeriodYear)+1;
                }else
                {
                    sellingPeriodYearInt = Integer.ValueOf(sellingPeriodYear);
                }
            }
            //Integer sellingPeriodYearInt = Integer.ValueOf(sellingPeriodYear);
            Integer numberOfDays = Date.daysInMonth(sellingPeriodYearInt,Month);
            lastDayOfMonth = Date.newInstance(sellingPeriodYearInt, Month, numberOfDays); 
        }
        return lastDayOfMonth;
    }
    
    private List<OrderItem> getOrderItems(){
        return new List<OrderItem>([select id,Quantity,PricebookEntryId,PricebookEntry.Product2Id,PricebookEntry.Product2.Name from OrderItem where orderid =: orderId]);
    }
    
    public class innerWrapperForProduct
    {
        public String innerCourseId{get;set;}
        public Id innerProdId{get;set;}
        public boolean selectProduct{get;set;}
        public string innerUsagePicklistVal{get;set;}
        public string innerstatusPicklistVal{get;set;}
        public Decimal innerQuantityVal{get;set;}
        public string innerthirdPartyLMSPicklistVal{get;set;}
        public string innermodeOfDeliveryPicklist{get;set;}
        public string innerchannelPicklistVal{get;set;}
        public string innerchannelDetailPicklistVal{get;set;} //CR-02850
        public Date innerExpiryDate{get;set;}
        public Date innerInstallDate{get;set;}
        public String innerCourseName{get;set;}
        public String innerProductName{get;set;}
        public Integer innerUniqueIdentifier{get;set;}
        public innerWrapperForProduct(Integer uniqueId,Boolean isSelected,string prodId,String prodName,String courseId,String courseName,String usagePicklistVal,Decimal quantityVal,String statusPicklistVal,String modeOfDeliveryPicklistVal,String thirdPartyLMSPicklistVal,String channelPicklistVal,Date installDateVal,Date expiryDateVal,String channeldetailPicklistVal)
        {
            innerUniqueIdentifier = uniqueId;
            innerProductName = prodName;
            innerCourseId = courseId;
            innerCourseName = courseName;
            innerProdId = prodId;
            innerUsagePicklistVal = usagePicklistVal;
            innerstatusPicklistVal= statusPicklistVal;
            innerQuantityVal = quantityVal;
            innerthirdPartyLMSPicklistVal = thirdPartyLMSPicklistVal;
            innermodeOfDeliveryPicklist = modeOfDeliveryPicklistVal;
            innerchannelPicklistVal = channelPicklistVal;
            innerchanneldetailPicklistVal=channeldetailPicklistVal; //CR-02850
            innerExpiryDate = expiryDateVal;
            innerInstallDate = installDateVal;
            selectProduct = isSelected;
        }
    }
}