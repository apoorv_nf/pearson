/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            OppTerrAssignDefaultLogicFilter.Cls 
   Description:     Handler class for Territory Assignment on Opportunity, it follows below logic:
                    Case 1: 0 territories in active model:
                        then set territory2Id =   null
                    Case 2: 1 territory on Account
                        then set territory2Id =   account's territory2Id
                    Case 3: 2 or more territories on Account or 0 territory on Account
                        then set territory2Id =  Territory of Opportunity Owner
                    Case 4: 2 or more territories on Account and Opportunity Owner is in both or more of these territories
                        then set territory2Id = account's territory2Id that is of highest priority.
                        But if multiple  territories have same highest priority, then set territory2Id = null
   Test Class:      OppTerrAssignDefaultLogicFilter_Test
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   20/10/2015         0.1        Accenture - Karan Khanna                Created
                                
---------------------------------------------------------------------------------------------------------------------------------------------------------- */


global class PS_OpportunityTerritoryAssignment implements TerritoryMgmt.OpportunityTerritory2AssignmentFilter {
    /**
    * No-arg constructor.
    */ 
    global PS_OpportunityTerritoryAssignment() {}

    /**
      * Get mapping of   opportunity to territory2Id. The incoming list of opportunityIds contains only those with IsExcludedFromTerritory2Filter=false.
      * If territory2Id =   null in result map, clear the opportunity.territory2Id if set.
      * If opportunity is not present in result map, its territory2Id remains intact.
    */
    global Map<Id,Id> getOpportunityTerritory2Assignments(List<Id> opportunityIds) {
        
        Map<Id, Id> OppIdTerritoryIdResult = new Map<Id, Id>();     
        List<Id> opportunityB2BIdList = new List<Id>();

        try {
            Id globalRecordTypeId;
            //Using Util class to fetch record type instead of SOQL
            globalRecordTypeId = PS_Util.fetchRecordTypeByName(Opportunity.SobjectType,PS_Constants.OPPORTUNITY_GLOBAL_RECCORD);                      
            //This is only for B2B opportunities
            for(Opportunity opty : [SELECT Id, RecordTypeId FROM Opportunity WHERE Id IN:opportunityIds]){
                
                if(opty.RecordTypeId == globalRecordTypeId){
                    
                    opportunityB2BIdList.add(opty.Id);
                }
            }
            if(opportunityB2BIdList.size() > 0){
            
                //Get the active territory model Id
                Id activeModelId = getActiveModelId();
                if(activeModelId != null){
                    List<Opportunity> opportunities = [Select Id, AccountId, Territory2Id, OwnerId from Opportunity where Id IN:opportunityB2BIdList];
                    Set<Id> accountIds = new Set<Id>();
                    Set<Id> opptyOwnerIdSet = new Set<Id>();
                    //Create   set of parent accountIds and OwnerIds
                    for(Opportunity opp:opportunities){
                        if(opp.AccountId != null){
                            accountIds.add(opp.AccountId);
                        }
                        if(opp.OwnerId != null){
                            opptyOwnerIdSet.add(opp.OwnerId);
                        }
                    }
                   
                    Map<Id, List<Id>> accountTerritoryIdMap = getAccountTerritory(activeModelId, accountIds);
                    Map<Id, List<Id>> opptyOwnerTerritoryIdMap = getOpportunityOwnerTerritory(opptyOwnerIdSet);
                    Map<Id,Territory2Priority> accountMaxPriorityTerritory = getAccountMaxPriorityTerritory(activeModelId, accountIds);
                    List<Id> accTerIdList = new List<Id>();
                    List<Id> opptyOwnerTerIdList = new List<Id>();
                  
                    for(Opportunity opp: opportunities){
                       
                        if(accountTerritoryIdMap.get(opp.AccountId) !=null){
                            accTerIdList = accountTerritoryIdMap.get(opp.AccountId);
                        }                  
                        if(opptyOwnerTerritoryIdMap.get(opp.OwnerId) !=null){
                            opptyOwnerTerIdList = opptyOwnerTerritoryIdMap.get(opp.OwnerId);
                        }
                        // logic for Case 3
                        if(opptyOwnerTerIdList.size() == 1 && (accTerIdList.size() > 1 || accTerIdList.size() == 0)) {
                            OppIdTerritoryIdResult.put(opp.Id, opptyOwnerTerIdList[0]);
                            
                        }
                        else{              
                       
                           Territory2Priority tp = accountMaxPriorityTerritory.get(opp.AccountId);
                           //assign highest priority territory if there is only 1
                          if((tp != null) && (tp.moreTerritoriesAtPriority == false) && (tp.territory2Id != opp.Territory2Id)){
                               // logic for Case 2
                               OppIdTerritoryIdResult.put(opp.Id, tp.territory2Id);
                           }else{
                               // logic for Case 1 and Case 4
                               OppIdTerritoryIdResult.put(opp.Id, null);
                           }
                        }
                    }
                }
            }
        }
        catch (exception e) {
        }
        return OppIdTerritoryIdResult;
    }
    
    /**
      * Query assigned territoryIds in active model for given Opportunity Owner
      * Create a map of opportunityOwnerId to territoryIds 
      */
    private Map<Id, List<Id>> getOpportunityOwnerTerritory(Set<Id> opptyOwnerIdSet){
        
        Map<Id, List<Id>> opptyOwnerTerritoryIdMap = new Map<Id, List<Id>>();       
        
        for(UserTerritory2Association uta : [SELECT Territory2Id, UserId FROM UserTerritory2Association WHERE UserId IN:opptyOwnerIdSet]){
            
            if(opptyOwnerTerritoryIdMap.ContainsKey(uta.UserId)){
                opptyOwnerTerritoryIdMap.get(uta.UserId).add(uta.Territory2Id);                
            }
            else {
                List<Id> territoryIdList = new List<Id>();
                territoryIdList.add(uta.Territory2Id);
                opptyOwnerTerritoryIdMap.put(uta.UserId,territoryIdList);                
            }
        }
        return opptyOwnerTerritoryIdMap;        
    }
    
    /**
      * Query assigned territoryIds in active model for given accountIds
      * Create a map of accountId to territoryIds 
      */
    private Map<Id, List<Id>> getAccountTerritory(Id activeModelId, Set<Id> accountIds){
        
        Map<Id, List<Id>> accountTerritoryIdMap = new Map<Id, List<Id>>();
        for(ObjectTerritory2Association ota:[Select ObjectId, Territory2Id from ObjectTerritory2Association where objectId IN :accountIds and Territory2.Territory2ModelId = :activeModelId]){
            
            if(accountTerritoryIdMap.ContainsKey(ota.ObjectId)){
                accountTerritoryIdMap.get(ota.ObjectId).add(ota.Territory2Id);                
            }
            else {
                List<Id> territoryIdList = new List<Id>();
                territoryIdList.add(ota.Territory2Id);
                accountTerritoryIdMap.put(ota.ObjectId,territoryIdList);                
            }
        }
        return accountTerritoryIdMap;
    }
    
    /**
      * Query assigned territoryIds in active model for given accountIds
      * Create a map of accountId to max priority territory 
      */
     private Map<Id,Territory2Priority> getAccountMaxPriorityTerritory(Id activeModelId, Set<Id> accountIds){
        
        Map<Id,Territory2Priority> accountMaxPriorityTerritory = new Map<Id,Territory2Priority>();
        for(ObjectTerritory2Association ota:[Select ObjectId, Territory2Id, Territory2.Territory2Type.Priority from ObjectTerritory2Association where objectId IN :accountIds and Territory2.Territory2ModelId = :activeModelId]){
            Territory2Priority tp = accountMaxPriorityTerritory.get(ota.ObjectId);

            if((tp == null) || (ota.Territory2.Territory2Type.Priority > tp.priority)){
                //If this is the first territory examined for account or it has greater priority than current highest priority territory, then set this as new highest priority territory.
                tp = new Territory2Priority(ota.Territory2Id,ota.Territory2.Territory2Type.priority,false);
            }else if(ota.Territory2.Territory2Type.priority == tp.priority){
                //The priority of current highest territory is same as this, so set moreTerritoriesAtPriority to indicate multiple highest priority territories seen so far.
                tp.moreTerritoriesAtPriority = true;
            }
            
            accountMaxPriorityTerritory.put(ota.ObjectId, tp);
        }
        return accountMaxPriorityTerritory;
    }

             
    /**
     * Get the Id of the Active Territory Model. 
     * If none exists, return   null;
     */
   private Id getActiveModelId() {
       List<Territory2Model>   models = [Select Id from Territory2Model where State = 'Active'];
       Id activeModelId = null;
       if(models.size() == 1){
           activeModelId = models.get(0).Id;
       }
       
       return activeModelId;
   }
   
   /**
    * Helper class to help capture territory2Id, its priority, and whether there are more territories with same priority assigned to the account
    */
    global class Territory2Priority {
       public Id territory2Id { get; set; }
       public Integer priority { get; set; }
       public Boolean moreTerritoriesAtPriority { get; set; }
       
       public Territory2Priority(Id territory2Id, Integer priority, Boolean moreTerritoriesAtPriority){
           this.territory2Id = territory2Id;
           this.priority = priority;
           this.moreTerritoriesAtPriority = moreTerritoriesAtPriority;
        }
   }
}