/************************************************************************************************************
* Apex Interface Name : PS_FeedItem_TriggerHelperTest
* Version             : 1.1 
* Created Date        : 2/2/2016  
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
  Sudhakar Navuluri        2/2/2016             Amended the code
  Abhinav Srivastava       24/06/2016           Modified for test class fix.
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_FeedItem_TriggerHelperTest{
static testMethod void validateCaseFeedItemPostive(){
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        insert lstAccount ;
        
        List<Contact> lstContact = TestDataFactory.createContact(1);
        lstContact[0].MailingState='England';
        insert lstContact; 
          
        List<case> cases = TestDataFactory.createCase(1,'School Assessment');
        cases[0].Service_Now__c ='sometext';  
        cases[0].AccountId=lstAccount[0].id;
        cases[0].Contact=lstContact[0];        
        Test.StartTest(); 
        insert cases;
        
        List<FeedItem> FeeditemlistPostive=new List<FeedItem>();
        Feeditem feedRecordPostive=new Feeditem();
        feedRecordPostive.ParentId=cases[0].id;
        feedRecordPostive.CreatedById=UserInfo.getUserId();
        feedRecordPostive.body='@ServiceNow Integration';
        feedRecordPostive.type='TextPost';
        FeeditemlistPostive.add(feedRecordPostive);
        System.runAs(new User(Id=UserInfo.getUserId()))
        {  
                
            insert FeeditemlistPostive;
            System.assertEquals(FeeditemlistPostive[0].type,'TextPost');
            Test.StopTest();
        }

}

     // negative test class scenario 3/4/2016 supriyam 
     // by sending parent id null 
static testMethod void negative_validateCaseFeedItem(){
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        insert lstAccount ;
        
        List<Contact> lstContact = TestDataFactory.createContact(1);
        lstContact[0].MailingState='England';
        insert lstContact; 
          
        List<case> cases = TestDataFactory.createCase(1,'School Assessment');
        cases[0].Service_Now__c ='sometext';  
        cases[0].AccountId=lstAccount[0].id;
        cases[0].Contact=lstContact[0];        
        Test.StartTest();  
        insert cases;
        
        List<FeedItem> FeeditemlistPostive=new List<FeedItem>();
        Feeditem feedRecordPostive=new Feeditem();
        
        feedRecordPostive.ParentId=null;
          
        feedRecordPostive.CreatedById=UserInfo.getUserId();
        feedRecordPostive.body='@ServiceNow Integration';
        feedRecordPostive.type='TextPost';
        FeeditemlistPostive.add(feedRecordPostive);
        System.runAs(new User(Id=UserInfo.getUserId()))
        {  
            
            try{    
            insert FeeditemlistPostive;
          }
          catch(DmlException e){
          
          system.debug('case exception'+e.getMessage());
           
         Boolean expectedExceptionThrown =  e.getMessage().contains('Insert failed. First exception on row 0; first error: REQUIRED_FIELD_MISSING, Required fields are missing') ? true : false;
          System.AssertEquals(expectedExceptionThrown, true);
          
     
           System.assertEquals('REQUIRED_FIELD_MISSING' ,  e.getDmlStatusCode(0));
          }
  
            Test.StopTest();
        }

}
}