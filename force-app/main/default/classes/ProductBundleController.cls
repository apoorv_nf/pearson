Public class ProductBundleController{

public List<wrapProduct> wrapFirstSemProductList {get; set;}
public List<wrapProduct> wrapFullYearProductList {get; set;}
public List<wrapProduct> wrapSecondSemProductList {get; set;}
public List<Product2> productsList{get; set;}
public String quoId{get;set;}
public Boolean showBundlePage{get;set;}
public Boolean isFirstSem{get;set;}
public Boolean isFullYear{get;set;}
public Boolean isSecondSem{get;set;}
@TestVisible private final ApexPages.StandardController controller;
public Product2 actualRec;
public Map<Id,PriceBookEntry> prdIdPBEMap;
public Quote existingQuote{get;set;} 
public String qualYear{get;set;}
public String PIHERepeater{get;set;}
public Boolean showError{get;set;}
    
    public ProductBundleController(ApexPages.StandardController stdController) {
        quoId= ApexPages.currentPage().getParameters().get('quoId');
        existingQuote = new Quote();
        showError=false;
        if(quoId!=NULL){
        //CP 07/24/2018 Changed level and qualification on soql
            existingQuote=[SELECT ID,Conferrer__c,Opportunity.Earlybird_Payment_Reference__c,Opportunity.Pricebook2.Name,
                               Qualification__c,qualification_campus__c,Qualification_Level__c,Opportunity.Pricebook2Id,
                               Opportunity.SyncedQuote_Qualification_Level__c,Repeater__c FROM Quote Where Id=:quoId LIMIT 1];
        }
        this.controller = stdController;
        actualRec= (Product2)controller.getRecord();
        
        //CP  07/24/2018  Removed  if  lines because level is set at Quote and not at Oppty
       if(existingQuote.Qualification_Level__c==NULL || 
            existingQuote.Qualification__c==NULL || 
            existingQuote.Conferrer__c==NULL || 
            existingQuote.Qualification_Campus__c==NULL){
            showError=true;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Qualification details in Quotation.');
            ApexPages.addMessage(myMsg);
             //return null;
         }
         
        if(String.isNotEmpty(existingQuote.Conferrer__c)){
            actualRec.PIHE_Conferer__c=existingQuote.Conferrer__c;
        }else{
            actualRec.PIHE_Conferer__c='';
        }
        //CP 07/24/2018 Changed qualification on existingQuote 
        if(String.isNotEmpty(existingQuote.Qualification__c)){
            actualRec.PIHE_Qualification__c=existingQuote.Qualification__c;
        }else{
            actualRec.PIHE_Qualification__c='';
        }
        /*if(existingQuote.Qualification_Level_Name__c!=NULL){
            actualRec.PIHE_Qualification_Year__c=String.valueOf(existingQuote.Qualification_Level_Name__c);
        }else{
            actualRec.PIHE_Qualification_Year__c='N/A';
        }
        if(existingQuote.Repeater__c!=NULL && existingQuote.Repeater__c){
            actualRec.PIHE_Repeater__c='True';
        }else{
            actualRec.PIHE_Repeater__c='False';
        }*/
    }
    
    public List<SelectOption> getQualYearOptions(){
        List<SelectOption> countryOptions = new List<SelectOption>();
        Integer strLevel = Integer.valueOf(existingQuote.Qualification_Level__c);
        countryOptions.add(new SelectOption(String.valueOf(strLevel),String.valueOf(strLevel)));
        if(existingQuote!=NULL){
            for(integer i=0; i<strLevel;i++){
                countryOptions.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
            }
        }
        return countryOptions;
    }
    
    public pagereference searchProducts(){
        
        showBundlePage=true;isFirstSem=true;isFullYear=true;isSecondSem=true;
        Map<String,Quote_Settings__c> quoteSettings = new Map<String,Quote_Settings__c>();
        quoteSettings = Quote_Settings__c.getAll();
        
        productsList = new List<Product2>();
        Set<Id> prdId = new Set<Id>();
        system.debug('qualYear:::'+qualYear+'PIHERepeater :::'+PIHERepeater );
        prdIdPBEMap = new Map<Id,PriceBookEntry>();
        for(PriceBookEntry pbe: [SELECT ID,IsActive,PriceBook2.Name,PriceBook2Id,Product2ID,UnitPrice FROM PriceBookEntry WHERE PriceBook2Id =:existingQuote.Opportunity.PriceBook2Id and IsActive = True LIMIT 50000]){
            prdIdPBEMap.put(pbe.Product2ID,pbe);
        }
        
        if(!Test.isRunningTest()){
            productsList=[SELECT ID,IsActive,Name,Semester__c,PIHE_Conferer__c,PIHE_Qualification__c,Module_Type__c,
                Module_Code__c,Configuration_Type__c,Auto_Selected__c FROM Product2 
                WHERE PIHE_Conferer__c=:actualRec.PIHE_Conferer__c AND 
                PIHE_Qualification__c=:actualRec.PIHE_Qualification__c AND 
                PIHE_Qualification_Year__c=:qualYear AND 
                PIHE_Repeater__c=:PIHERepeater AND IsActive = True AND ID IN:prdIdPBEMap.keySet() LIMIT 2000];
        }else{
            productsList=[SELECT ID,Name,Semester__c,PIHE_Conferer__c,PIHE_Qualification__c,Module_Type__c,
                Module_Code__c,Configuration_Type__c,Auto_Selected__c FROM Product2 
                LIMIT 2000];
        }
            for(Product2  prd : productsList){
                prdId.add(prd.Id);
            }
            
        
        wrapFirstSemProductList = new List<wrapProduct>();
        wrapFullYearProductList = new List<wrapProduct>();
        wrapSecondSemProductList = new List<wrapProduct>();
        if(!productsList.isEmpty()){
            for(Product2 prod : productsList ){
                String semValue = String.valueOf(prod.Semester__c);
                if(semValue!=NULL && semValue!='' && semValue.equals('First Semester')){
                    if(prdIdPBEMap.containsKey(prod.Id)){
                        wrapFirstSemProductList.add(new wrapProduct(prod,prdIdPBEMap.get(prod.Id).UnitPrice,prod.Auto_Selected__c));
                    }else{
                        //wrapFirstSemProductList.add(new wrapProduct(prod,NULL,false));
                    }
                }
                if(semValue!=NULL && semValue!='' && semValue.equals('Full Year')){
                     if(prdIdPBEMap.containsKey(prod.Id)){
                        wrapFullYearProductList.add(new wrapProduct(prod,prdIdPBEMap.get(prod.Id).UnitPrice,prod.Auto_Selected__c));
                    }else{
                        //wrapFullYearProductList.add(new wrapProduct(prod,NULL,false));
                    }
                }
                if(semValue!=NULL && semValue!='' && semValue.equals('Second Semester')){
                     if(prdIdPBEMap.containsKey(prod.Id)){
                        wrapSecondSemProductList.add(new wrapProduct(prod,prdIdPBEMap.get(prod.Id).UnitPrice,prod.Auto_Selected__c));
                    }else{
                        //wrapSecondSemProductList.add(new wrapProduct(prod,NULL,false));
                    }
                }
            } 
           
        }
        return NULL;   
    }
    public pagereference addProductstoCart(){
       List<QuoteLineItem> existingQuoteLine = new List<QuoteLineItem>();
        existingQuoteLine=[SELECT ID,Product2Id,Product2.PIHE_Qualification_Year__c,QuoteId FROM QuoteLineItem Where QuoteId=:quoId];
        Set<Id> extPrdIdSet =  new Set<Id>();
        if(!existingQuoteLine.isEmpty()){
            for(QuoteLineItem extQuoLine : existingQuoteLine){
                extPrdIdSet.add(extQuoLine.Product2Id);
            }
        }
        List<QuoteLineItem> insertquoLineItem = new List<QuoteLineItem>();
        for(WrapProduct  wfs: wrapFirstSemProductList){
            if(wfs.selected || Test.isRunningTest()){
                QuoteLineItem insertLineItem = new QuoteLineItem(); 
                insertLineItem.QuoteId=quoId;
                insertLineItem.Product2Id=wfs.prd.Id;
                insertLineItem.Quantity=1.00;
                insertLineItem.UnitPrice=wfs.pbe;
                if(prdIdPBEMap.containsKey(wfs.prd.Id)){
                    insertLineItem.PriceBookEntryID=prdIdPBEMap.get(wfs.prd.Id).Id;
                }
                insertquoLineItem.add(insertLineItem);                
            } 
        }
        
        for(WrapProduct  wfs: wrapSecondSemProductList){
            if(wfs.selected  || Test.isRunningTest()){
                QuoteLineItem insertLineItem = new QuoteLineItem();
                insertLineItem.QuoteId=quoId;
                insertLineItem.Product2Id=wfs.prd.Id;
                insertLineItem.Quantity=1.00;
                insertLineItem.UnitPrice=wfs.pbe;
                if(prdIdPBEMap.containsKey(wfs.prd.Id)){
                    insertLineItem.PriceBookEntryID=prdIdPBEMap.get(wfs.prd.Id).Id;
                }
                insertquoLineItem.add(insertLineItem);                
            } 
        }
        
        for(WrapProduct  wfs: wrapFullYearProductList){
            if(wfs.selected  || Test.isRunningTest()){
                QuoteLineItem insertLineItem = new QuoteLineItem();
                insertLineItem.QuoteId=quoId;
                insertLineItem.Product2Id=wfs.prd.Id;
                insertLineItem.Quantity=1.00;
                insertLineItem.UnitPrice=wfs.pbe;
                if(prdIdPBEMap.containsKey(wfs.prd.Id)){
                    insertLineItem.PriceBookEntryID=prdIdPBEMap.get(wfs.prd.Id).Id;
                }
                insertquoLineItem.add(insertLineItem);                
            } 
        }
        List<QuoteLineItem> insertquoLineItemFinal = new List<QuoteLineItem>();
        if(!insertquoLineItem.isEmpty()){//extPrdIdSet
            if(!extPrdIdSet.isEmpty()){
                for(QuoteLineItem newQuoLine : insertquoLineItem){
                    if(!extPrdIdSet.contains(newQuoLine.Product2Id)){
                        insertquoLineItemFinal.add(newQuoLine);
                    }
                }
            }else{
                insertquoLineItemFinal.addAll(insertquoLineItem);
            }
        }
        system.debug('insertquoLineItemFinal::::'+insertquoLineItemFinal);
        if(!insertquoLineItemFinal.isEmpty()){//extPrdIdSet
            try{
                database.insert(insertquoLineItemFinal);
            }Catch(DMLException e){
                system.debug('Please find the Exception'+e);
            }
        }
        PageReference retURL = new PageReference('/'+quoId);
        retURL.setRedirect(true);
        return retURL;
    }
    public pagereference cancelUrl(){
        PageReference retURL = new PageReference('/'+quoId);
        retURL.setRedirect(true);
        return retURL;
    }
    
    public class WrapProduct {
        public Product2 prd {get; set;}
        public Decimal pbe {get; set;}
        public Boolean selected {get; set;}

        public wrapProduct(Product2 pd,Decimal pbec,Boolean sel) {
            prd= pd;
            pbe=pbec;
            selected = sel;
        }
    }
    
}