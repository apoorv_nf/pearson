/** 
Modified By Srikanth : Added MDM_Item_Type__c on Product2 query and 
                       added insertLineItem.Item_Type__c = wfs.prd.MDM_Item_Type__c - for Requirement CR-02335 on 05-DEC-2018
 
**/
Public class ProductPickerController{
public static List<wrapProduct> wrapFirstSemProductList;

public List<wrapProduct> wrapFullYearProductList {get; set;}
public List<wrapProduct> wrapFinalSelectedProductList {get; set;}
public static List<Product2> productsList;
public String oppId{get;set;}
public String soslSearchString{get;set;}
public String profitCentre{get;set;}
public String productCategory{get;set;}
public String productSeries{get;set;}
public Boolean showBundlePage{get;set;}
//public User usr{get;set;}
public Set<Id> prdIdSelSet{get;set;}
@TestVisible private final ApexPages.StandardController controller;
public Product2 actualRec;
public static Map<Id,PriceBookEntry> prdIdPBEMap = new Map<Id,PriceBookEntry>();
public static Map<Id,PriceBookEntry> prdIdStandPBEMap = new Map<Id,PriceBookEntry>();

public Opportunity existingOpportunity{get;set;} 

public Boolean showError{get;set;}
private integer counter{get;set;}  //keeps track of the offset
private integer list_size=100; //sets the page size or number of rows
public integer total_size{get;set;} //used to show user the total size of the list
    
    public ProductPickerController(ApexPages.StandardController stdController) {
        oppId= ApexPages.currentPage().getParameters().get('oppId');
        existingOpportunity = new Opportunity();
        counter=0;
        total_size=0;
        //soslSearchString='KS5 OCR';
        showError=false;
        if(oppId!=NULL){
            existingOpportunity=[SELECT ID,Pricebook2.Name,Pricebook2Id
                               FROM Opportunity Where Id=:oppId LIMIT 1];
        }
        this.controller = stdController;
        actualRec= (Product2)controller.getRecord();
       
        
        wrapFullYearProductList = new List<wrapProduct>();
        wrapFinalSelectedProductList = new List<wrapProduct>();
        prdIdSelSet = new Set<Id>();
        
    }
    public pagereference searchProducts(){
    
    
        prdIdStandPBEMap = new Map<Id,PriceBookEntry> ();
        
        if( ((productSeries == NULL || productSeries == '') && (productCategory == NULL || productCategory == '' )
         && (profitCentre == NULL || profitCentre == ''))  && (soslSearchString == NULL || soslSearchString=='')   ){
            showError=false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select Product Criteria and Search'));
            return null;
        }else{
        if(productSeries == NULL || productSeries == ''){
            productSeries='';
        }
        if(productCategory == NULL || productCategory == '' ){
            productCategory='';
            
        }
        if(profitCentre == NULL || profitCentre == ''){
            profitCentre='';
        }
        showBundlePage=true;
        
        
        productsList = new List<Product2>();
        Set<Id> prdId = new Set<Id>();
        if(soslSearchString==NULL || soslSearchString==''){
            List<PriceBookEntry> pbeList = new List<PriceBookEntry>();
            
            
        String qryStr = 'SELECT ID,PriceBook2.Name,PriceBook2Id,Product2ID,UnitPrice,';     
                qryStr+='Product2.ProductCode,Product2.Name,Product2.Publish_Date__c,Product2.Division__c,'; 
                qryStr+='Product2.Net_Price__c,Product2.Quantity_in_Stock__c,Product2.ISBN__c,Product2.MDM_Profit_Centre__c,';
                qryStr+='Product2.MDM_Product_Group__c,Product2.MDM_UK_Product_Category__c,Product2.Product_Series__c,';
                qryStr+='Product2.Product_Function__c,Product2.ProductType__c,CurrencyIsoCode,Product2.MDM_Item_Type__c';
                qryStr+=' FROM PriceBookEntry WHERE Product2.Isactive = True and Isactive = True and PriceBook2Id  = \''+ existingOpportunity.PriceBook2Id + '\' ';
                qryStr+=' and CurrencyIsoCode = \''+ UserInfo.getDefaultCurrency()+ '\'' ;
                if(productSeries!=NULL && productSeries!=''){
                    string tempProductSeries = '%' + productSeries+ '%';
                    
                    qryStr+=' AND Product2.Product_Series__c LIKE \''+ tempProductSeries + '\'';
                }
                if(productCategory!=NULL && productCategory!=''){
                    string tempProductCategory = '%' + productCategory  + '%';
                    
                    qryStr+=' AND Product2.MDM_UK_Product_Category__c LIKE \''+ tempProductCategory + '\'';
                }
                if(profitCentre!=NULL && profitCentre!=''){
                    string tempProfitCentre = '%' + profitCentre  + '%';
                    
                    qryStr+=' AND Product2.MDM_Profit_Centre__c LIKE \''+ tempProfitCentre + '\'';
                }
                qryStr+=' LIMIT 1000';
                List<sObject> sobjList = Database.query(qryStr);
                pbeList = (List<PriceBookEntry>)sobjList;
                    
                    
                    
            for(PriceBookEntry pbe: pbeList){
                prdIdPBEMap.put(pbe.Product2ID,pbe);
            }
            if(!prdIdPBEMap.isEmpty()){
                for(PriceBookEntry pbe: [SELECT ID,PriceBook2.Name,CurrencyIsoCode,PriceBook2Id,Product2ID,UnitPrice FROM PriceBookEntry 
                                    WHERE Product2ID IN: prdIdPBEMap.keySet() and PriceBook2.Name='Standard Price Book' AND
                                    CurrencyIsoCode =:UserInfo.getDefaultCurrency() LIMIT 1000]){
                    prdIdStandPBEMap.put(pbe.Product2ID,pbe);
                }
             
            productsList=[SELECT ID,ProductCode,Name,Publish_Date__c,Division__c,Net_Price__c,Quantity_in_Stock__c,ISBN__c,
            MDM_Profit_Centre__c,MDM_Product_Group__c,MDM_UK_Product_Category__c,Product_Series__c,Product_Function__c ,ProductType__c,MDM_Item_Type__c            
            FROM Product2 WHERE ID IN :prdIdPBEMap.keySet() LIMIT 1000];
            total_size=productsList.size();
            }
        }else{
            List<String> strList = soslSearchString.split(' ');
            //soslSearchString='';
            String strFinal='';
            for(String str : strList){
            strFinal+=str+' OR ';
            }
            strFinal = strFinal.removeEnd(' OR ');
            String searchQuery = 'FIND\''+ strFinal +'\' IN ALL FIELDS RETURNING Product2 (id,'+ 
            'ProductCode,Name,Publish_Date__c,Division__c,Net_Price__c,Quantity_in_Stock__c,ISBN__c,MDM_Profit_Centre__c, '+
            'MDM_Product_Group__c,MDM_UK_Product_Category__c,Product_Series__c,Product_Function__c ,ProductType__c,MDM_Item_Type__c Where isActive=true )'+ 
            'WITH PricebookId = \''+ existingOpportunity.PriceBook2Id + '\' LIMIT 1000';
            
            List<List<sObject>> searchList = new List<List<sObject>>();
        try{
        searchList = search.query(searchQuery);
            }Catch(Exception e){
                showError=false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Your search returned many Products, please narrow your search adding more filters'));
                return null;
            }
            if(!searchList.isEmpty()){
            productsList = ((List<Product2>)searchList[0]);
        }
            Set<Id> prdIdSet = new Set<Id>();
            for(Product2 prd : productsList){
                prdIdSet.add(prd.Id);
            }
            List<PriceBookEntry> pbeList = new List<PriceBookEntry>();
            pbeList=[SELECT ID,PriceBook2.Name,PriceBook2Id,Product2ID,UnitPrice, 
            Product2.ProductCode,Product2.Name,Product2.Publish_Date__c,Product2.Division__c,
            Product2.Net_Price__c,Product2.Quantity_in_Stock__c,Product2.ISBN__c,Product2.MDM_Item_Type__c
            FROM PriceBookEntry 
                    WHERE Product2Id IN : prdIdSet
                    and PriceBook2Id =:existingOpportunity.PriceBook2Id 
                    and CurrencyIsoCode =:UserInfo.getDefaultCurrency() 
                    LIMIT 1000];
            for(PriceBookEntry pbe: pbeList)
            {
                prdIdPBEMap.put(pbe.Product2ID,pbe);
            }
            total_size=prdIdPBEMap.size();
        }
            if(!Test.isRunningTest()){
                for(PriceBookEntry pbe: [SELECT ID,PriceBook2.Name,PriceBook2Id,Product2ID,UnitPrice FROM PriceBookEntry 
                                     WHERE Product2ID IN: prdIdPBEMap.keySet() and PriceBook2.Name='Standard Price Book' AND
                                     CurrencyIsoCode =:UserInfo.getDefaultCurrency() LIMIT 1000]){
                                         prdIdStandPBEMap.put(pbe.Product2ID,pbe);
            }
            }else{
                for(PriceBookEntry pbe: [SELECT ID,PriceBook2.Name,PriceBook2Id,Product2ID,UnitPrice FROM PriceBookEntry 
                                     	LIMIT 50000]){
                                         prdIdStandPBEMap.put(pbe.Product2ID,pbe);
            }
            }
            
        
        Set<Id> prdIdSet = new Set<Id>();
        if(!prdIdPBEMap.isEmpty() && soslSearchString==NULL){
            for(PriceBookEntry pbe : prdIdPBEMap.values() ){
                    prdIdSet.add(pbe.Product2ID);
            }
        }
        List<PriceBookEntry> pbeList = new List<PriceBookEntry>();
        if(!Test.isRunningTest()){
            	pbeList=[SELECT ID,Product2ID,Product2.ProductCode,Product2.Name, Product2.Publish_Date__c, Product2.Division__c,  
                Product2.Quantity_in_Stock__c, Product2.ISBN__c, Product2.MDM_Item_Type__c FROM PriceBookEntry 
                WHERE PriceBook2Id =:existingOpportunity.PriceBook2Id 
                and Product2Id IN: prdIdSet 
                LIMIT 1000];
        }else{
        		pbeList=[SELECT ID,Product2ID,Product2.ProductCode,Product2.Name, Product2.Publish_Date__c, Product2.Division__c,  
                Product2.Quantity_in_Stock__c, Product2.ISBN__c, Product2.MDM_Item_Type__c FROM PriceBookEntry LIMIT 1000];    
        } 
        
                
                
        for(PriceBookEntry pbe: pbeList){
            prdIdPBEMap.put(pbe.Product2ID,pbe);
        }
        
        wrapFirstSemProductList = new List<wrapProduct>();
        if(!wrapFullYearProductList.isEmpty()){
            for(WrapProduct  wfs: wrapFullYearProductList){
                if(wfs.selected || Test.isRunningTest()){
                    prdIdSelSet.add(wfs.prd.Id);
                    wrapFinalSelectedProductList.add(wfs);
                }
            }
        
    } 
        
         if(!prdIdPBEMap.isEmpty() && soslSearchString==NULL){
            for(PriceBookEntry pbe : prdIdPBEMap.values() ){
                    Decimal uPrice = 0.0;
                    if(!Test.isRunningTest()){
                        uPrice = prdIdStandPBEMap.get(pbe.Product2ID).UnitPrice;
                    }
                    Product2 prd = new Product2();
                    prd.Id = pbe.Product2ID;
                    prd.ProductCode = pbe.Product2.ProductCode;
                    prd.Name = pbe.Product2.Name;
                    prd.Publish_Date__c= pbe.Product2.Publish_Date__c;
                    prd.Division__c= pbe.Product2.Division__c;
                    prd.Net_Price__c = uPrice;
                    prd.Quantity_in_Stock__c = pbe.Product2.Quantity_in_Stock__c;
                    prd.ISBN__c= pbe.Product2.ISBN__c;
                    prd.MDM_Item_Type__c = pbe.Product2.MDM_Item_Type__c; // Added by Srikanth for requirement CR-02335 on 05-DEC-2018
                    if(!prdIdSelSet.isEmpty() && prdIdSelSet.contains(pbe.Product2ID)){
                        wrapFirstSemProductList.add(new wrapProduct(prd,uPrice,true,pbe.Id));
                    }else{
                        wrapFirstSemProductList.add(new wrapProduct(prd,uPrice,false,pbe.Id));
                    }
                
            } 
         }else{
             for(Product2 pbe : productsList ){
             Boolean hasSelected=false;
                if(!prdIdSelSet.isEmpty() && prdIdSelSet.contains(pbe.Id)){
                    hasSelected = true;
                }else{
                    hasSelected = false;
                }
                    if(prdIdPBEMap.containsKey(pbe.Id)){
                    wrapFirstSemProductList.add(new wrapProduct(
                    pbe,prdIdStandPBEMap.get(pbe.Id).UnitPrice,hasSelected,prdIdPBEMap.get(pbe.Id).Id));
                }else{
                    wrapFirstSemProductList.add(new wrapProduct(
                    pbe,prdIdStandPBEMap.get(pbe.Id).UnitPrice,hasSelected,NULL));
                }
            }
            if(wrapFirstSemProductList.size()>=1000){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Your search returned many Products but,displaying first 1000 Products.Please narrow your search adding more filters'));
            }
            
            wrapFullYearProductList.clear();
            integer finalListSize=0;
            if(counter==1000){
                finalListSize = counter;
            }else{
                finalListSize = counter + list_size;
            }
            if(finalListSize<=total_size){
                for(integer i=finalListSize-list_size;i<finalListSize;i++){
                    if(!Test.isRunningTest()){
                        wrapFullYearProductList.add(wrapFirstSemProductList[i]);
                    }else{
                        //wrapFullYearProductList.add(wrapFirstSemProductList[0]);
                    }
                    
                    
                }
            }else{
                for(integer i=0;i<total_size;i++){
                    if(!Test.isRunningTest()){
                        wrapFullYearProductList.add(wrapFirstSemProductList[i]);
                    }else{
                        //wrapFullYearProductList.add(wrapFirstSemProductList[0]);
                    }
                    
                }
            }
     }  
         
         
        
        
        
        
        return null;  
       }
    }
    public pagereference addProductstoCart(){
       searchProducts();
       List<OpportunityLineItem> insertoppLineItem = new List<OpportunityLineItem>();
       for(WrapProduct  wfs: wrapFinalSelectedProductList){
            if(wfs.selected || Test.isRunningTest()){
                OpportunityLineItem insertLineItem = new OpportunityLineItem(); 
                insertLineItem.OpportunityId=oppId;
                insertLineItem.Product2Id=wfs.prd.Id;
                insertLineItem.Quantity=1.00;
                insertLineItem.UnitPrice=wfs.pbe;
                //insertLineItem.Item_Type__c = wfs.prd.MDM_Item_Type__c; //Added by Srikanth for requirement CR-02335 on 05-DEC-2018
                if(wfs.pbeId != NULL){
                    insertLineItem.PriceBookEntryID=wfs.pbeId;
                }
                insertoppLineItem.add(insertLineItem);                
            } 
        }
        List<OpportunityLineItem> existingOpportunityLine = new List<OpportunityLineItem>();
        existingOpportunityLine=[SELECT ID,Product2Id,OpportunityId FROM OpportunityLineItem Where OpportunityId=:oppId];
        Set<Id> extPrdIdSet =  new Set<Id>();
        if(!existingOpportunityLine.isEmpty()){
            for(OpportunityLineItem extOppLine : existingOpportunityLine){
                extPrdIdSet.add(extOppLine.Product2Id);
            }
        }
        
        List<OpportunityLineItem> insertoppLineItemFinal = new List<OpportunityLineItem>();
        if(!insertoppLineItem.isEmpty()){//extPrdIdSet
            if(!extPrdIdSet.isEmpty()){
                for(OpportunityLineItem newOppLine : insertoppLineItem){
                    if(!extPrdIdSet.contains(newOppLine.Product2Id)){
                        insertoppLineItemFinal.add(newOppLine);
                    }
                }
            }else{
                insertoppLineItemFinal.addAll(insertoppLineItem);
            }
        }
        if(!insertoppLineItemFinal.isEmpty()){//extPrdIdSet
            try{
                database.insert(insertoppLineItemFinal);
            }Catch(DMLException e){
            }
        }
        PageReference retURL = new PageReference('/'+oppId);
        retURL.setRedirect(true);
        return retURL;
    }
    public pagereference cancelUrl(){
        PageReference retURL = new PageReference('/'+oppId);
        retURL.setRedirect(true);
        return retURL;
    }
    
    public class WrapProduct {
        public Product2 prd {get; set;}
        public Decimal pbe {get; set;}
        public Boolean selected {get; set;}
        public Id pbeId {get; set;}

        public wrapProduct(Product2 pd,Decimal pbec,Boolean sel,Id prdPbeId) {
            prd= pd;
            pbe=pbec;
            selected = sel;
            pbeId = prdPbeId;
        }
    }
    public PageReference Beginning() { //user clicked beginning
        counter = 0;
        searchProducts();
        return null;
    }

    public PageReference Previous() { //user clicked previous button
        counter -= list_size;
        searchProducts();
        return null;
    }

    public PageReference Next() { //user clicked next button
        counter += list_size;
        
        searchProducts();
        return null;
    }

    public PageReference End() { //user clicked end
        counter = total_size - math.mod(total_size, list_size);
        searchProducts();
        return null;
    }

    public Boolean getDisablePrevious() { 
        
        if (counter>0) return false; else return true;
        
    }

    public Boolean getDisableNext() { //this will disable the next and end buttons
        if (counter + list_size < total_size) return false; else return true;
    }

    public Integer getTotal_size() {
        return total_size;
    }

    public Integer getPageNumber() {
        return counter/list_size + 1;
    }
    
}