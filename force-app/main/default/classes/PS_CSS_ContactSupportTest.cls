/************************************************************************************************************
* Apex Interface Name : PS_CSS_ContactSupportTest
* Version             : 1.1 
* Created Date        : 2/2/2016 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Hemangini                  2/2/2016 
* Supriyam                  3/2/2016            
-------------------------------------------------------------------------------------------------------------
* Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 40,97,202,259,365,408,464) 
** Modified by Manikanta Nagubilli on 01/12/2016 for CR-00908 / Creating Case->(Modified Lines- 64) 
************************************************************************************************************/
@isTest
public class PS_CSS_ContactSupportTest{
        
    /*
     * Method to cover the method getCase
     */  
    
        static testMethod void test_getCase(){
        String communityUserProfileName = 'Pearson Self-Service User';
        String accountRecType = 'Organisation';
        String caseRecType = 'School Assessment';
        String caseRecTypeAPIName = 'State_National';
        String contactRecType = 'Contact';
        List<User> userList = new List<User>();
        List<Account> accountList = new List<Account>();
        List<Case> caseList = new List<Case>();
        List<Product__c> prodList = new List<Product__c>();
        String successMsgStr, detailsStr;
        Product__c prod;
        user caseOwnerUsr = new user();
        User usr;
        UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', firstName='Iam', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){
            Test.StartTest();
            //user for 'Customer Community Login' license
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
            //insert userList;            
           /* accountList = TestDataFactory.createAccount(1,accountRecType);
            insert accountList; */
            caseList = TestDataFactory.createCase_SelfService(1,caseRecType);
            caseList[0].ownerId = usr.Id;
            
            //insert caseList;
            prod = new Product__c(Active_in_Case__c=true, Type__c='Product');
            prodList.add(prod);
            insert prodList;
            Test.StopTest();
        }
        detailsStr ='["Test_Fname","Test_Lname","Test_Email@test.com","10","","","Test_Email@test.com.test","Test_Email@test.com.test","'+String.valueOf(2)+'","Test Course","TestCourseID","","","Test Error Message","","Chrome","","","","","'+String.valueOf(2128425500)+'","","","","Test Account","","English","United States","","","Test Account","Online","TestOrder","USD","Dropped Class","Test Description","Account Creation Request","Technical Support","'+contactRecType+'","Test Subject","TestFileName","Test Attachment Body",".pdf","'+String.valueOf(prodList[0].id)+'","FromPhone","9854584552","'+String.valueOf(prodList[0].Name)+'","","","","","",""]';
        
         System.runAs(usr)
         {
            //Test.StartTest();
            List<Contact> ContactList = new List<Contact>();
            Account AccountRec = new account( Name='Test Account1789', Phone='+9100000' ,IsCreatedFromLead__c = True, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037');
            insert AccountRec;
            Contact contactRecord = new Contact(FirstName=TestDataFactory.generateRandomString(25), LastName=TestDataFactory.generateRandomString(25),
                                                AccountId=AccountRec.Id ,Salutation='MR.', Email=TestDataFactory.generateRandomString(30) + '@email.com',MobilePhone = '8884888472',
                                                Phone='11122233390', Preferred_Address__c='Mailing Address', MailingCity = 'Newcastle', MailingState='Northumberland', MailingCountry='United Kingdom', MailingStreet='1st Street' , MailingPostalCode='NE28 7BJ',National_Identity_Number__c = '9412055708083' , Passport_Number__c = 'ABCDE12345',Home_Phone__c = '9999',Birthdate__c = Date.newInstance(1900 , 10,10),Ethnic_Origin__c = 'Asian' , Marital_Status__c = 'Single',First_Language__c = 'English');
            //List<Contact> ContactList = TestDataFactory.createContact(1);
            ContactList.add(contactRecord);
            //ContactList[0].Email ='Test_Email@test.com';
             insert ContactList;
            successMsgStr = PS_CSS_ContactSupport.getCase(detailsStr);
            //System.assert(successMsgStr.contains('Your case has been created successfully.'));
            System.assertNotEquals(successMsgStr,null);
            //Test.StopTest();
      }
    }
    static testMethod void test_getPwdResetLink(){
        Community_Password_Reset_Link__c pwdRest = new Community_Password_Reset_Link__c(Name='Forgot Password',Target_URL__c='www.google.com');
        insert pwdRest;
        String link = PS_CSS_ContactSupport.getPwdResetLink();
        system.assertEquals('www.google.com',link);
    }
    // negative test condition 3-2 supriyam
    static testMethod void negativetest_getCase(){
        String communityUserProfileName = 'Pearson Self-Service User';
        String accountRecType = 'Organisation';
        String caseRecType = 'School Assessment';
        String caseRecTypeAPIName = 'State_National';
        String contactRecType = 'Contact';
        List<User> userList = new List<User>();
        List<Account> accountList = new List<Account>();
        List<Case> caseList = new List<Case>();
        List<Product__c> prodList = new List<Product__c>();
        String successMsgStr, detailsStr;
         try{
        Product__c prod;
        user caseOwnerUsr = new user();
        User usr;
        UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){
            Test.StartTest();
            //user for 'Customer Community Login' license
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
            insert userList;            
           accountList = TestDataFactory.createAccount(1,accountRecType);
            insert accountList; 
            
             RecordType rt = [SELECT id, Name FROM RecordType WHERE SobjectType = 'Case' AND Name =: caseRecType ];
             if(caseRecType == 'State & National'){
                Case caseRecord=new Case(Contact_Type__c='College Educator',
                   Description='description',RecordTypeId = rt.id);
                caseList.add(caseRecord);
                caseList[0].ownerId = userList[0].Id; 
            }
            
            
            insert caseList;
            prod = new Product__c(Active_in_Case__c=true, Type__c='Product');
            prodList.add(prod);
            insert prodList;
            Test.StopTest();
              detailsStr ='["Test_Fname","Test_Lname","Test_Email@test.com","10","","","","Test_Email@test.com.test","'+String.valueOf(2)+'","Test Course","TestCourseID","","","Test Error Message","","Chrome","","","","","'+String.valueOf(2128425500)+'","","","","Test Account","","English","United States","","","Test Account","Online","TestOrder","USD","Dropped Class","Test Description","Learner","'+caseRecTypeAPIName+'","'+contactRecType+'","Test Subject","TestFileName","Test Attachment Body",".pdf","'+String.valueOf(prodList[0].id)+'","FromPhone","9854584552"]';
       
            successMsgStr = PS_CSS_ContactSupport.getCase(detailsStr);
            // System.assert(false, 'Exception expected');
        }
            
       }
       catch (DmlException e) {
           //Assert Error Message            
           
         Boolean expectedExceptionThrown =  e.getMessage().contains('Insert failed. First exception on row 0; first error: REQUIRED_FIELD_MISSING, Required fields are missing: [LastName]') ? true : false;
          System.AssertEquals(expectedExceptionThrown, true);
          
     
           System.assertEquals('REQUIRED_FIELD_MISSING' ,  e.getDmlStatusCode(0));

        
  //  System.assert(e.getMessage().contains('Insert failed. First exception on row 0; first error: REQUIRED_FIELD_MISSING, Required fields are missing: [LastName]'), 'message=' + e.getMessage());
      //   System.assertEquals('REQUIRED_FIELD_MISSING'  ,   e.getDmlStatusCode(0));
        } 
    }
    //Method to cover getFooterValues()
    static testMethod void test_getFooterValues(){
        List<String> footerValues=new List<String>();
       String TermsOfUse = System.label.PS_CSS_SelfServiceFooter_TermsOfUse; 
       String PrivacyPolicy=  System.label.PS_CSS_SelfServiceFooter_PrivacyPolicy;
       String Follow = System.label.PS_CSS_SelfServiceFooter_Follow;
       footerValues.add(TermsOfUse);
       footerValues.add(PrivacyPolicy);
       footerValues.add(Follow);
       footerValues = PS_CSS_ContactSupport.getFooterValues();  
    }
    // Method to cover getRefundsLabel()
    static testMethod void test_getRefundsLabel(){
        String refundValue;
       refundValue = System.label.PS_SelfServiceRefunds;
       refundValue = PS_CSS_ContactSupport.getRefundsLabel();  
    }
     //Method to cover getAgentMessages()
    static testMethod void test_getAgentMessages(){
       List<String> agentMessages=new List<String>();
       String online = System.label.PS_AgentOnline; 
       String offline=  System.label.PS_AgentOffline;
       agentMessages.add(online);
       agentMessages.add(offline);
       agentMessages = PS_CSS_ContactSupport.getAgentMessages();  
    }
    //Method to cover getLearningStudioLabel()  
   static testMethod void test_getLearningStudioLabel()
    {
        String learningSudioProductValue;
       learningSudioProductValue = System.label.PS_SelfServiceLearningStudio; 
         learningSudioProductValue= PS_CSS_ContactSupport.getLearningStudioLabel();
    }
    /*
     * Method to cover the method getValues()
    */ 
    static testMethod void test_getValues(){
        String communityUserProfileName = 'Pearson Self-Service User';
        List<User> userList = new List<User>();
        List<Customer_Support__kav> knowledgeArtVerList;
        //List<Customer_Support__kav> knowledgeArtVerToInsertList = new List<Customer_Support__kav>();
        List<TopicAssignment> topicAssignToInsertList = new List<TopicAssignment>();
            
        String searchTitleStr, countryStr, roleStr;
        searchTitleStr = 'Test Article';
        countryStr = '';
        roleStr = '';
        User usr;
        UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', FirstName='Iam', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){      
            //user for 'Customer Community Login' license
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
        }
            
      
            Test.StartTest();
            knowledgeArtVerList = PS_CSS_ContactSupport.getValues(searchTitleStr, countryStr, roleStr, '', '');
            
            List<Customer_Support__kav> myassert;            
            System.assertEquals(knowledgeArtVerList, myassert);
            Test.StopTest();
        
    }
    
    //* Method to cover the method test_getArticles,getArticleviews,getArticleviews,getmediaArticles()
    
       static testMethod void test_getArticles(){
        String communityUserProfileName = 'Pearson Self-Service User';
        List<Customer_Support__kav> knowledgeArtVerList;
        List<Customer_Support__kav> knowledgeArtVerList_getArticles;
        List<Customer_Support__ViewStat> knowledgeArtViewStat;
        List<Customer_Support__kav> CustSupplst;
        List<FeedItem> lstFeedItm;
        List<User> userList = new List<User>();
        List<Account> accountList = new List<Account>();
        List<TopicAssignment> topicAssignToInsertList = new List<TopicAssignment>();
        List<Case> caseList = new List<Case>();
        String caseRecType = 'School Assessment';
            
           
            caseList = TestDataFactory.createCase_SelfService(1,caseRecType);
            //insert caseList;
            
            
             FeedItem feed = new FeedItem (ParentId = userinfo.getuserid(),Body = 'Hello');
            insert feed;    
            
            //Insert Topic
            Topic topc = new Topic (Name='test1');
            insert topc;
        
            //Insert Topic Assignment
            TopicAssignment topcAssgmnt = new TopicAssignment(EntityId=feed.id,TopicId=topc.id);
            insert topcAssgmnt;
            
            String searchTitleStr, countryStr, roleStr;
            searchTitleStr = 'Test Article';
            countryStr = '';
            roleStr = '';
            User usr;
        
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', FirstName='Iam', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science' );
        
        System.runAs(usr){      
            //user for 'Customer Community Login' license
            try{
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
            }catch(Exception e){}
        }
           
            Test.StartTest();
            knowledgeArtVerList = PS_CSS_ContactSupport.getValues(searchTitleStr, countryStr, roleStr, '', '');
            //knowledgeArtVerList_getArticles = PS_CSS_ContactSupport.getArticles('test');
            knowledgeArtViewStat = PS_CSS_ContactSupport.getArticleviews('test');
            lstFeedItm = PS_CSS_ContactSupport.getDiscussions('test');
            CustSupplst = PS_CSS_ContactSupport.getmediaArticles('test');
            List<Customer_Support__kav> myassert;            
            System.assertEquals(knowledgeArtVerList, myassert);
            Test.StopTest();
        
    }
    
    
    /*
     * Method to cover multiple methods
     */ 
    static testMethod void test_PS_CSS_ContactSupport(){
        String communityUserProfileName = 'Pearson Self-Service User';
        List<User> userList = new List<User>();
        List<CS_country__c> countyrList = new List<CS_country__c>();
        List<PS_CategoryGrouping__c> pickListValList = new List<PS_CategoryGrouping__c>();
        List<product__c> prodList = new List<product__c>();
        List<CS_role__c> roleList = new List<CS_role__c>();
        List<CS_role__c> roleListToInsert = new List<CS_role__c>();
        String searchTitleStr = 'Test Article';
        List<Topic> topicList = new List<Topic>();
        List<Topic> topicToInsertList = new List<Topic>();
        List<PS_Country_PhoneMapping__c> countryPhnMapToInsertList = new List<PS_Country_PhoneMapping__c>();
        String returnString, errorString, orgIdStr, schoolStr;
       List<CS_Country_ContactUs_Mapping__c> countryContactUsMapList = new List<CS_Country_ContactUs_Mapping__c>();
        List<CS_Country_ContactUs_Mapping__c> countryContactUsMapList2 = new List<CS_Country_ContactUs_Mapping__c>();
        List<CS_Country_ContactUs_Mapping__c> countryContUsMapToInsertList = new List<CS_Country_ContactUs_Mapping__c>();
        List<PS_ContactRole__c> ContactRoleList = new List<PS_ContactRole__c>();
        List<String> refundValList = new List<String>();
        List<ContactSupportLanguages__c> contSupportLangList = new List<ContactSupportLanguages__c>();
        List<PS_CountryCategryMap_PrdRole__c> countryCategoryMapPrdRoleList = new List<PS_CountryCategryMap_PrdRole__c>();
        List<String> roleCollEduAndStudentList = new List<String>();
        List<selectOption> countryValList, categoryValList, prodValList, roleValList, langOptions, stateOptions, programOptions;
        List<PS_ContactLanguage_CS__c> contLangList = new List<PS_ContactLanguage_CS__c>();
        List<PS_ContactState_CS__c> contStateList = new List<PS_ContactState_CS__c>();
        PS_CSS_ContactSupport contSupport = new PS_CSS_ContactSupport();
        List<String> lstChatButDets = new List<String>();       
        List<String> buttonIdList = new List<String>();       
        List<PS_FetchChatButton__mdt> chatButtonList = new List<PS_FetchChatButton__mdt>();
        User userRec, usr;
        Id attachId;
        UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', FirstName = 'Iam',  alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){      
            //user for 'Customer Community Login' license
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
            
            //data to cover PS_CSS_ContactSupport.getValueRole and PS_CSS_ContactSupport.getRoleValues
            roleListToInsert.add(new CS_role__c(Name='Bookstore_Rep__c', Label__c='Bookstore Rep'));
            insert roleListToInsert;
            
            //data to cover PS_CSS_ContactSupport.getTopic
            topicToInsertList.add(new Topic(Name='Test Article1', Description='Test Description'));
            insert topicToInsertList;
            
            //data to cover PS_CSS_ContactSupport.getpickvalRole
            countryPhnMapToInsertList.add(new PS_Country_PhoneMapping__c(Name='Test', Country__c='Test Country', Role__c='Test Role', Number__c='2784764'));
            insert countryPhnMapToInsertList;
            
            //data to cover PS_CSS_ContactSupport.getCountry_ContactUs_Mapping and PS_CSS_ContactSupport.getaAllData
           countryContUsMapToInsertList.add(new CS_Country_ContactUs_Mapping__c(name='test1', Country__c='Australia', Category__c='All else'));
            //countryContUsMapToInsertList.add(new CS_Country_ContactUs_Mapping__c(name='test2', Country__c='India', Category__c='All else'));
            countryContUsMapToInsertList.add(new CS_Country_ContactUs_Mapping__c(name='test2', Country__c='Germany', Category__c='All else',Country_Group__c='Lisbon')); // Added by Mayank
            ContactRoleList.add(new PS_ContactRole__c(name='College Educator',RoleDetail__c='Instructor',Role_Grouping__c='College Educator',RoleName__c='Educator'));
            insert countryContUsMapToInsertList;
            
            //data to cover PS_CSS_ContactSupport.getValueLanguage
            contSupportLangList.add(new ContactSupportLanguages__c(name='English', language__c='English'));
            contSupportLangList.add(new ContactSupportLanguages__c(name='Dutch', language__c='Dutch'));
            insert contSupportLangList;
            
            //data to cover PS_CSS_ContactSupport.getCountryValues
            countyrList.add(new CS_country__c(name='Australia__c', Label__c='Australia'));
            countyrList.add(new CS_country__c(name='Egypt__c', Label__c='Egypt'));
            insert countyrList;
            
            //data to cover getProgramValues.getProductValues and getProgramValues.getProgramValues
            prodList.add(new Product__c(name='prod1', Active_in_Case__c=true, Type__c='Product'));
            prodList.add(new Product__c(name='prod2', Active_in_Case__c=true, Type__c='Product'));
            prodList.add(new Product__c(name='prod3', Active_in_Case__c=true, Type__c='program'));
            //Added by Mayank
            prodList.add(new Product__c(name='prod4', Active_in_Case__c=true, Type__c='program',PRODUCT_GROUP__C='Germany',COUNTRY__C='Germany'));
            // prodList.add(new Product__c(name='prod5', Active_in_Case__c=true, Type__c='program'));
            insert prodList;
            
            //data to cover PS_CSS_ContactSupport.getLanguageValues
            contLangList.add(new PS_ContactLanguage_CS__c(name='English'));
            contLangList.add(new PS_ContactLanguage_CS__c(name='Dutch'));
            insert contLangList;
            
            //data to cover PS_CSS_ContactSupport.getStateValues
            contStateList.add( new PS_ContactState_CS__c(name='Florida'));
            contStateList.add( new PS_ContactState_CS__c(name='Ohio'));
            insert contStateList;
        }
                                    
        
            Test.StartTest();
            countyrList = PS_CSS_ContactSupport.getValueCtry();
            pickListValList = PS_CSS_ContactSupport.getpickvalCaseCat('Global');
            List<PS_CategoryGrouping__c> Categorypicklist = PS_CSS_ContactSupport.getpickvalCaseCat('Clinical');
            prodList = PS_CSS_ContactSupport.getValuePrd();
            roleList = PS_CSS_ContactSupport.getValueRole('Global');
            List<CS_role__c> myroleList = PS_CSS_ContactSupport.getValueRole('Clinical');
            String prodGroup = PS_CSS_ContactSupport.getprodcategory('Revel');
            //topicList = PS_CSS_ContactSupport.getTopic(searchTitleStr);
            returnString = PS_CSS_ContactSupport.getPhoneNumber('Test Country', 'Test CountryHub','Test Role','Test ProductGroup','Test Language');
            userRec = PS_CSS_ContactSupport.getCurrentUser();
            errorString = PS_CSS_ContactSupport.getChangePassword('new pswd','old pswd','new pswd');
           //attachId = PS_CSS_ContactSupport.saveAttachment(userList[0].contactId, 'Test File', 'Test Attchment Body', '.pdf');
            PS_CSS_ContactSupport.setInit('Test_Role', 'Test_Country', 'Test_Category', 'Test_Sub', 'Test_Pro', 'Test_Lang');
            countryContactUsMapList = PS_CSS_ContactSupport.getCountry_ContactUs_Mapping('Australia','All else');//to cover if
            countryContactUsMapList = PS_CSS_ContactSupport.getCountry_ContactUs_Mapping('India','All else');//to cover else
            countryContactUsMapList2 = PS_CSS_ContactSupport.getAllData();
            refundValList = PS_CSS_ContactSupport.getRefundReasonValues();
            returnString = PS_CSS_ContactSupport.getSupportToSupportPermission();
            //attachId = PS_CSS_ContactSupport.saveTheFile(userList[0].contactId, 'Test File', 'Test Attchment Body', '.pdf');
            contSupportLangList = PS_CSS_ContactSupport.getValueLanguage();
            countryCategoryMapPrdRoleList = PS_CSS_ContactSupport.getCountryCategryRole();
            roleCollEduAndStudentList = PS_CSS_ContactSupport.getRoleCollEduAndStudent();
            buttonIdList = PS_CSS_ContactSupport.getButtonId('India', 'TestCat', 'TestProd', 'TestRole', 'TestLang');
            orgIdStr = PS_CSS_ContactSupport.getOrganization();
            countryValList = contSupport.getCountryValues();
            categoryValList = contSupport.getCategoryValues();
            //prodValList = contSupport.getProductValues();
            string equalllabel  = PS_CSS_ContactSupport.getEquellaLabel();
            roleValList = contSupport.getRoleValues();
            langOptions = contSupport.getLanguageValues();
            schoolStr = contSupport.getSchoolValues();
            stateOptions = contSupport.getStateValues();
            programOptions = contSupport.getProgramValues();
            lstChatButDets = PS_CSS_ContactSupport.getChatButtonId('TestDevName1','TestDevName2','TestDevName3');
            System.assertNotEquals(roleList, null);
            Test.StopTest();
        
    }
    
    /*
     * Method to cover getter and setter methods
     */ 
    static testMethod void test_getSet(){
    
        PS_CSS_ContactSupport conSupportObj = new PS_CSS_ContactSupport();
        string getValueStr;         
        String communityUserProfileName = 'Pearson Self-Service User';      
        User usr;
        List<User> userList = new List<User>();
        UserRole tempUserRole;
        
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', firstName ='Iam', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){      
            //user for 'Customer Community Login' license
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
        }
        
        
            Test.startTest();
            conSupportObj.selectedState='TestState';
            conSupportObj.selectedCountry='TestCountry';
            conSupportObj.selectedCategory='TestCat';
            PS_CSS_ContactSupport.Subject='TestSub';
            conSupportObj.selectedProduct='TestProd';
            conSupportObj.selectedRole='TestRole';
            conSupportObj.selectedLanguage='TestLang';
            conSupportObj.selectedSchool='TestSchool';
            conSupportObj.selectedDistrict='TestDist';
            conSupportObj.selectedProgram='TestProg';
            conSupportObj.uphone='TestPhone';
            conSupportObj.sOrganization='TestOrg';
            conSupportObj.sRecType='TestRecType';
            conSupportObj.sRecTypeId='TestRecTypeId';
            conSupportObj.sAccountName='TestAccount';
            
            Test.stopTest();
        
    }
    // negative test data values
      static testMethod void negative_test_getSet(){
    
        PS_CSS_ContactSupport conSupportObj = new PS_CSS_ContactSupport();
        string getValueStr;         
        String communityUserProfileName = 'Pearson Self-Service User';      
        User usr;
        List<User> userList = new List<User>();
        UserRole tempUserRole;
        
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        try{
        //user to avoid Mixed DML error
        usr = new User( alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){      
            //user for 'Customer Community Login' license
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
        }
        
        System.runAs(userList[0]){
            Test.startTest();
            conSupportObj.selectedState='TestState';
            conSupportObj.selectedCountry='TestCountry';
            conSupportObj.selectedCategory='TestCat';
            PS_CSS_ContactSupport.Subject='TestSub';
            conSupportObj.selectedProduct='TestProd';
            conSupportObj.selectedRole='TestRole';
            conSupportObj.selectedLanguage='TestLang';
            conSupportObj.selectedSchool='TestSchool';
            conSupportObj.selectedDistrict='TestDist';
            conSupportObj.selectedProgram='TestProg';
            conSupportObj.uphone='TestPhone';
            conSupportObj.sOrganization='TestOrg';
            conSupportObj.sRecType='TestRecType';
            conSupportObj.sRecTypeId='TestRecTypeId';
            conSupportObj.sAccountName='TestAccount';
            System.assertEquals(conSupportObj.selectedCategory, 'TestCat');
            Test.stopTest();
        }
        }
         catch (DmlException e) {
           //Assert Error Message            
           
         Boolean expectedExceptionThrown =  e.getMessage().contains('Insert failed. First exception on row 0; first error: REQUIRED_FIELD_MISSING, Required fields are missing: [LastName]') ? true : false;
          System.AssertEquals(expectedExceptionThrown, true);
          
     
           System.assertEquals('REQUIRED_FIELD_MISSING' ,  e.getDmlStatusCode(0));

        
  
        } 
    }


    
    Static testmethod void checkIfRegisteredUserRegUser () {
        String communityUserProfileName = 'Pearson Self-Service User';      
        User usr;
        List<User> userList = new List<User>();
        UserRole tempUserRole;
        
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', FirstName='Iam', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){      
            //user for 'Customer Community Login' license
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
            insert userList;
        }
        
        System.runAs(userList[0]){
            Test.startTest();
                PS_CSS_ContactSupport Controller = new PS_CSS_ContactSupport();
                boolean regUserResult;
                regUserResult = PS_CSS_ContactSupport.checkIfRegisteredUser();
                System.assertEquals(regUserResult,True);
                regUserResult = Controller.getisAuthUser();
                System.assertEquals(regUserResult,True);
            Test.stopTest();
        }
    }
    /* To Cover GetPhoneNumber
     * Added by Mayank
     */ 
    static testMethod void coverGetPhoneNumber(){ 
        String returnString1,returnString2,returnString3,returnString4,returnString5,returnString6;
        String nullValue = null;
        PS_Country_PhoneMapping__c psconPhoneMap = new PS_Country_PhoneMapping__c(name='test',Number__c='123456');
        insert psconPhoneMap;
        PS_ContactRole__c psconRole = new PS_ContactRole__c(name='Test Role',RoleDetail__c='Instructor',Role_Grouping__c='College Educator',RoleName__c='Educator');
        insert psconRole;
        
        List<Community_User_Geographical_Settings__c> lstComUserGeoSetting = new List<Community_User_Geographical_Settings__c>();
        Community_User_Geographical_Settings__c comUserGeoSetting = new Community_User_Geographical_Settings__c(name='Test',Country__c='Test Country',Email_Encoding__c='UTF-8',Language__c='English',LanguageLocaleKey__c='en_GB',TimeZone__c='Europe/Lisbon');
        insert comUserGeoSetting;
        lstComUserGeoSetting = PS_CSS_ContactSupport.getcountrycodeData();
        
        List<PS_CategoryGrouping__c> lstCatGroup = new List<PS_CategoryGrouping__c>();
        PS_CategoryGrouping__c CatGroup = new PS_CategoryGrouping__c(name='Test',CountryCode__c='FR,BE,NL',Grouping__c='Access');
        lstCatGroup = PS_CSS_ContactSupport.getcountrycode('FR');
        
        returnString1 = PS_CSS_ContactSupport.getPhoneNumber('Test Country', '','Test Role','Test ProductGroup','Test Language'); 
        returnString2 = PS_CSS_ContactSupport.getPhoneNumber('Test Country', '','Test Role',nullValue,'Test Language'); 
        
        returnString3 = PS_CSS_ContactSupport.getPhoneNumber('Test Country', 'North America','Test Role','Test ProductGroup','Test Language');
        returnString4 = PS_CSS_ContactSupport.getPhoneNumber('Test Country', 'Test CountryHub','Test Role','Test Country','Test Language'); 
        returnString5 = PS_CSS_ContactSupport.getPhoneNumber('Test Country', 'Test CountryHub','Test Role1','Test Country','Test Language'); 
        returnString6 = PS_CSS_ContactSupport.getPhoneNumber('Test Country', 'Test CountryHub','Test Role','Test ProductGroup','Test Language'); 
    }
    static testMethod void test_fetchContactUsDetails(){ 
        Contact_Us_Data_Detail__c DataDetail=new Contact_Us_Data_Detail__c(Community_Type__c='UK',CustomerType__c='Customer1',IssueType__c='Issue1',Form_Type__c='Qualification');
        insert DataDetail;
        List<Contact_Us_Meta_Data_Detail__c> MetaDataList=new List<Contact_Us_Meta_Data_Detail__c>();
        Contact_Us_Meta_Data_Detail__c MetaData1=new Contact_Us_Meta_Data_Detail__c(FieldAPIName__c='CustomerType__c',ObjectAPIName__c='Contact_Us_Data_Detail__c',Form_Type_Value__c='Qualification',Community_Type__c='UK',Field_Type__c='Picklist',ParentFieldAPIName__c='',Form_Type__c='Form_Type__c',SequenceNo__c=1,IsDependent__c=False);
        MetaDataList.add(MetaData1);
        Contact_Us_Meta_Data_Detail__c MetaData2=new Contact_Us_Meta_Data_Detail__c(FieldAPIName__c='IssueType__c',ObjectAPIName__c='Contact_Us_Data_Detail__c',Form_Type_Value__c='Qualification',Community_Type__c='UK',Field_Type__c='Picklist',ParentFieldAPIName__c='CustomerType__c',Form_Type__c='Form_Type__c',SequenceNo__c=2,IsDependent__c=True);
        MetaDataList.add(MetaData2);
        insert MetaDataList;
        
        PS_CSS_ContactSupport.fetchContactUsDetails('Qualification','UK');
    }
    static testMethod void test_getDeflectionArticle(){ 
        Deflection_Articles_Fields_Data__c Articles=new Deflection_Articles_Fields_Data__c(Name='TestArticlesData1',Business_Vertical__c='Qualifications',Fields_to_add_in_Query__c='CustomerType__c');
        insert Articles;
        PS_CSS_ContactSupport.getDeflectionArticle('TestArticlesData1');
    }

}