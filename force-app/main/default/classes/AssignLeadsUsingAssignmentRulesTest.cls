@IsTest(SeeAllData=true)
public class AssignLeadsUsingAssignmentRulesTest {
    static testMethod void testmethod1() {
        test.StartTest();
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);
        lstWithTestUser[0].market__c='US';
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true);
        insert byp;
        	System.runas(lstWithTestUser[0]){
         RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
            Account acc = new Account (Name='test001',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
        acc.BillingStreet = 'Street1';    
            acc.BillingCity = 'Victoria';
            acc.BillingPostalCode = 'ABC DEF';
        acc.BillingCountry = 'Australia';
            acc.BillingState = 'Victoria';
            acc.BillingCountryCode = 'AU';
            acc.BillingStateCode = 'VIC';
            acc.ShippingStreet = 'TestStreet';
            acc.ShippingCity = 'Vns';
            acc.ShippingState = 'Delhi';
            acc.ShippingPostalCode = '234543';
            acc.ShippingCountry = 'India';        
            acc.recordtypeId=OrgAcc.Id ;
            acc.Academic_Achievement__c='High';
            acc.Market2__c = 'US';
            acc.Organisation_Type__c='School';
            acc.IsCreatedFromLead__c = true; 
            insert acc;
       /*hh Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);
        lstWithTestUser[0].market__c='US';
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true);
        insert byp;*/
        General_One_CRM_Settings__c genOneCrRM = new General_One_CRM_Settings__c();
        genOneCrRM.Name = 'Lead Automation Batch Limitt';
        genOneCrRM.Category__c = 'ReopenDays';
        genOneCrRM.Description__c = 'ReopenDays';
        genOneCrRM.Value__c = '10';     
        insert genOneCrRM;
        List<Id> leadId = new List<Id>();
        Id NARRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Account Request').getRecordTypeId();
        Lead lead = new Lead();
        lead.FirstName='Test1';
        lead.LastName='Test1';
        lead.Company='Test Company';
        lead.Run_assignment_rules__c= True;
        lead.Country_of_Origin__c='Canada';
        lead.Market__c='US';
       // lead.Record_Type__c='New_Account_Request';
        lead.recordtypeId = NARRecordTypeId;
        lead.Business_Unit__c='CTIMGI';
        lead.Geography__c='North America';
        lead.Line_of_Business__c='Higher Ed';
        lead.Channel__c ='Direct';
        lead.Organisation_Type1__c = 'Higher Education';
        lead.Role__c = 'Employee';
        lead.Enquiry_Type__c = 'Product';
        lead.PostalCode = '345678';
        lead.Country='Canada';
        lead.State = 'TORONTO';
        lead.Street = 'TestStreet';
        lead.City = 'TestCity';
        lead.OwnerId = lstWithTestUser[0].Id;
        lead.Institution_Organisation__c = acc.Id;
        
        insert lead;
        leadId.add(lead.id);
        
        AssignLeadsUsingAssignmentRules.LeadAssign(leadId); 
        test.StopTest();
    } 
    
}
}