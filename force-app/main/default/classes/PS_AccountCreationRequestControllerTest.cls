/************************************************************************************************************
* Apex Interface Name : PS_AccountCreationRequestControllerTest
* Version             : 1.1 
* Created Date        : 1/2/2016 2:44 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Rashmi Prasad         30/1/2016          
* Supriyam      3/4/16 (Negative Scenarios)       
-------------------------------------------------------------------------------------------------------------

* Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 23,51) 
************************************************************************************************************/
@isTest
public class PS_AccountCreationRequestControllerTest{
static testMethod void testPS_AccountCreationRequestController(){
    List<User> listWithUser = new List<User>();
    listWithUser  = TestDataFactory.createUser([select Id from Profile where Name = 'Pearson Service Cloud User OneCRM'].Id,1);
    insert listWithUser;
    
    List<User> listWithUser1 = new List<User>();
    //listWithUser1  = TestDataFactory.createUser([select Id from Profile where Name = 'Pearson Service User OneCRM'].Id,1);
    listWithUser1  = TestDataFactory.createUser([select Id from Profile where Name = 'CTIMGI Service User'].Id,1); //RP updated Profile name July 15 2016 to avoid confusion with "Pearson Service Cloud User OneCRM" profile
    insert listWithUser1;
    
    List<Account> lstAccount = TestDataFactory.createAccount(1,'Account_Creation_Request');
    insert lstAccount ;
    
    Test.StartTest();
    PageReference pageRef = Page.PS_AccountCreationRequest;    
    Test.setCurrentPage(pageRef);
    PS_AccountCreationRequestController  ACR = new PS_AccountCreationRequestController ();
    System.runAs(listWithUser[0]){   
    System.AssertEquals(listWithUser[0].Email ,'h@gmail.com');
    ACR.openaccountpage();
    } 
    System.runAs(listWithUser1[0]){ 
    System.AssertEquals(listWithUser1[0].Email,'h@gmail.com');  
    ACR.openaccountpage();
    }    
    Test.StopTest();
 }
 
 static testMethod void negativetestPS_AccountCreationRequestController(){
 List<User> listWithUser = new List<User>();
    listWithUser  = TestDataFactory.createUser([select Id from Profile where Name = 'Pearson Service Cloud User OneCRM'].Id,1);
    insert listWithUser;
    
    List<User> listWithUser1 = new List<User>();
    //listWithUser1  = TestDataFactory.createUser([select Id from Profile where Name = 'Pearson Service User OneCRM'].Id,1);
    listWithUser1  = TestDataFactory.createUser([select Id from Profile where Name = 'CTIMGI Service User'].Id,1);     //RP updated Profile name July 15 2016 to avoid confusion with "Pearson Service Cloud User OneCRM" profile
    insert listWithUser1;
    
    List<Account> lstAccount = TestDataFactory.createAccount(0,'Account_Creation_Request');
    insert lstAccount ;
    
    Test.StartTest();
    PageReference pageRef = Page.PS_AccountCreationRequest;    
    Test.setCurrentPage(pageRef);
    PS_AccountCreationRequestController  ACR = new PS_AccountCreationRequestController ();
    System.runAs(listWithUser[0]){   
    
    ACR.openaccountpage();
    } 
    System.runAs(listWithUser1[0]){ 
    // cehcking the negative condition
   System.assert(true, 'account is empty');
    }    
    Test.StopTest();
 }
 
}