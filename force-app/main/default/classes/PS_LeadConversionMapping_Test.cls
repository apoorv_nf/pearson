/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_LeadConversionMapping_Test 
   Description:     Test class for PS_LeadConversionMapping
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   28th Sep 2015      1.0           Accenture NDC                                         Created
   20th May 2016      1.1           Abhinav                                               Modified for test class fix.
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

@IsTest(SeeAllData=true)
public with sharing class PS_LeadConversionMapping_Test 
{
  
    static testMethod void testLeadCreationHigherEdu()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        lstWithTestUser[0].market__c='US';
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp; 
        AccountTeamMember newAccountTeamMember = new AccountTeamMember();
        System.runAs(lstWithTestUser[0]) 
        {
        RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
        Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
        acc.ShippingStreet = 'TestStreet';
        acc.ShippingCity = 'Vns';
        acc.ShippingState = 'Delhi';
        acc.ShippingPostalCode = '234543';
        acc.ShippingCountry = 'India';
        acc.recordtypeId=OrgAcc.Id ;
        acc.Academic_Achievement__c='High';
        acc.Market2__c = 'US';
        acc.Organisation_Type__c='School';
        acc.IsCreatedFromLead__c = true;
        acc.Abbreviated_Name__c='TestName';    
        insert acc;

        List<Lead> lstLeadRecords = new List<Lead>();
        Id accountId;   
        lstLeadRecords = TestDataFactory.createLead(1,'B2B');
        
        for(Lead lstLead : lstLeadRecords)
        { 
            lstLead.Channel__c ='Direct';
            lstLead.Organisation_Type1__c = 'Higher Education';
            lstLead.Role__c = 'Employee';
            lstLead.Enquiry_Type__c = 'Product';
            lstLead.PostalCode = '345678';
            lstLead.State = 'Bihar';
            lstLead.Street = 'TestStreet';
            lstLead.Institution_Organisation__c =acc.Id;
        lstLead.Market__c = 'AU';
        }
         
            test.startTest();
            Database.insert(lstLeadRecords);
            Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
            for(Lead newLead : lstLeadRecords)
            {
                mapWithLead.put(newLead.Id,newLead);
            }
            for(Lead newLead : lstLeadRecords)
            {
                Database.LeadConvert convertLead = new database.LeadConvert();
                convertLead.setLeadId(newLead.ID);
                convertLead.setDoNotCreateOpportunity(false);
                convertLead.setConvertedStatus('Qualified');
                Database.LeadConvertResult lcr = Database.convertLead(convertLead);
                System.assert(lcr.isSuccess());
                accountId = lcr.getAccountId();
            }
            List<Lead> lstWithLead = [select isConverted,Campaign__c,Do_Not_Send_Samples__c,Marketing_Opt_In__c,ownerid,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId from lead where id IN: mapWithLead.keyset()];
            PS_LeadConversionMapping.createMapping(lstWithLead,mapWithLead);
            test.stopTest();
        }
    }
    
    static testMethod void testLeadCreationProfessional()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        lstWithTestUser[0].market__c='US';
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp; 
        AccountTeamMember newAccountTeamMember = new AccountTeamMember();
        System.runAs(lstWithTestUser[0]) 
        {
        RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
        Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
        acc.ShippingStreet = 'TestStreet';
        acc.ShippingCity = 'Vns';
        acc.ShippingState = 'Delhi';
        acc.ShippingPostalCode = '234543';
        acc.ShippingCountry = 'India';
        acc.recordtypeId=OrgAcc.Id ;
        acc.Academic_Achievement__c='High';
        acc.Market2__c = 'US';
        acc.Organisation_Type__c='School';
        acc.IsCreatedFromLead__c = true;
        acc.Abbreviated_Name__c='TestName';    
        insert acc;
        List<Lead> lstLeadRecords = new List<Lead>();
        Id accountId;   
        lstLeadRecords = TestDataFactory.createLead(1,'B2B');
        for(Lead lstLead : lstLeadRecords)
        { 
            lstLead.Channel__c ='Direct';
            lstLead.Organisation_Type1__c = 'Professional';
            lstLead.Role__c = 'Employee';
            lstLead.Enquiry_Type__c = 'Product';
            lstLead.PostalCode = '345678';
            lstLead.State = 'Bihar';
            lstLead.Street = 'TestStreet';
            lstLead.Institution_Organisation__c =acc.Id;
        //lstLead.Market__c = 'AU';
        }
         
            test.startTest();
            Database.insert(lstLeadRecords);
             Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
             for(Lead newLead : lstLeadRecords)
             {
                 mapWithLead.put(newLead.Id,newLead);
             }
            for(Lead newLead : lstLeadRecords)
            {
                Database.LeadConvert convertLead = new database.LeadConvert();
                convertLead.setLeadId(newLead.ID);
                convertLead.setDoNotCreateOpportunity(false);
                convertLead.setConvertedStatus('Qualified');
                Database.LeadConvertResult lcr = Database.convertLead(convertLead);
                System.assert(lcr.isSuccess());
                accountId = lcr.getAccountId();
            }
            List<Lead> lstWithLead = [select isConverted,Campaign__c,Do_Not_Send_Samples__c,ownerid,Marketing_Opt_In__c,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId from lead where id IN: mapWithLead.keyset()];
            PS_LeadConversionMapping.createMapping(lstWithLead,mapWithLead);
            test.stopTest();
        }
    }
    
    static testMethod void testLeadCreationSchool()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        lstWithTestUser[0].market__c='US';
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp; 
        AccountTeamMember newAccountTeamMember = new AccountTeamMember();
        System.runAs(lstWithTestUser[0]) 
        { 
        RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
        Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
        acc.ShippingStreet = 'TestStreet';
        acc.ShippingCity = 'Vns';
        acc.ShippingState = 'Delhi';
        acc.ShippingPostalCode = '234543';
        acc.ShippingCountry = 'India';
        acc.recordtypeId=OrgAcc.Id ;
        acc.Academic_Achievement__c='High';
        acc.Market2__c = 'US';
        acc.Organisation_Type__c='School';
        acc.IsCreatedFromLead__c = true;
        acc.Abbreviated_Name__c='TestName';    
        insert acc;
        List<Lead> lstLeadRecords = new List<Lead>();
        Id accountId;   
        lstLeadRecords = TestDataFactory.createLead(1,'B2B');
        for(Lead lstLead : lstLeadRecords)
        { 
            lstLead.Channel__c ='Direct';
            lstLead.Organisation_Type1__c = 'School';
            lstLead.Role__c = 'Employee';
            lstLead.Enquiry_Type__c = 'Product';
            lstLead.PostalCode = '345678';
            lstLead.State = 'Bihar';
            lstLead.Street = 'TestStreet';
            lstLead.Institution_Organisation__c =acc.Id;
        }
       
            test.startTest();
            Database.insert(lstLeadRecords);
            Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
            for(Lead newLead : lstLeadRecords)
            {
                mapWithLead.put(newLead.Id,newLead);
            }
            for(Lead newLead : lstLeadRecords)
            {
                Database.LeadConvert convertLead = new database.LeadConvert();
                convertLead.setLeadId(newLead.ID);
                convertLead.setDoNotCreateOpportunity(false);
                convertLead.setConvertedStatus('Qualified');
                Database.LeadConvertResult lcr = Database.convertLead(convertLead);
                System.assert(lcr.isSuccess());
                accountId = lcr.getAccountId();
            }
            List<Lead> lstWithLead = [select isConverted,Campaign__c,ownerid,Do_Not_Send_Samples__c,Marketing_Opt_In__c,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId from lead where id IN: mapWithLead.keyset()];
            PS_LeadConversionMapping.createMapping(lstWithLead,mapWithLead);
            test.stopTest();
        }
    }
                
    static testMethod void testCreateLeadCampaignmember()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        lstWithTestUser[0].market__c='US';
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp; 
        list<Campaign> campaignList = new list<Campaign>();
        campaignList = TestDataFactory.createCampaign();
        System.runAs(lstWithTestUser[0]) 
        { 
        test.startTest();
        Database.insert(campaignList);
        
        list<lead> lstLeadRecords = new list<Lead>();
        RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
        Id B2BLeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(System.Label.PS_B2BLeadRecordType).getRecordTypeId();     
        Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
        acc.ShippingStreet = 'TestStreet';
        acc.ShippingCity = 'Vns';
        acc.ShippingState = 'Delhi';
        acc.ShippingPostalCode = '234543';
        acc.ShippingCountry = 'India';
        acc.recordtypeId=OrgAcc.Id ;
        acc.Academic_Achievement__c='High';
        acc.Market2__c = 'US';
        acc.Organisation_Type__c='School';
        acc.IsCreatedFromLead__c = true;
        acc.Abbreviated_Name__c='TestName';    
        insert acc;
        lstLeadRecords = TestDataFactory.createLead(1,'B2B');
        for(Lead leadLst : lstLeadRecords)
        {             
            leadLst.campaign_id__c = campaignList[0].id;
            leadLst.isFromIntegration__c = True;
            leadLst.PostalCode = '345678';
            leadLst.State = 'Bihar';
            leadLst.Street = 'TestStreet';
            leadLst.Record_Type__c = 'B2B';
            leadLst.Source_Details__c = 'Test';
            leadLst.Phone = '+8888888888';
            leadLst.MobilePhone = '+8888888888';
            leadLst.Other_Phone__c = '+8888888888';
            leadLst.Home_Phone__c ='+8888888888';
            leadLst.Fax = '+8888888888';
            leadLst.LeadSource = 'Webinar';
            leadLst.Sponsor_Phone__c = '+8888888888';
            leadLst.Sponsor_Home_Phone__c = '+8888888888';
            leadLst.Institution_Organisation__c =acc.Id;
        }
      
            Database.insert(lstLeadrecords);
            test.stopTest();
        }
                
    }    
    // Added by MAYANK
    static testMethod void testLeadCreationForGeneratePearsonId()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        lstWithTestUser[0].market__c='US';
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp; 
        AccountTeamMember newAccountTeamMember = new AccountTeamMember();
        List<Lead> lstLeadRecords = new List<Lead>();
        Id accountId;  
        System.runAs(lstWithTestUser[0]) 
        {
        RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Corporate'];
        Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
        acc.ShippingStreet = 'TestStreet';
        acc.ShippingCity = 'Vns';
        acc.ShippingState = 'Delhi';
        acc.ShippingPostalCode = '234543';
        acc.ShippingCountry = 'India';
        acc.recordtypeId=OrgAcc.Id ;
        acc.Academic_Achievement__c='High';
        acc.Market2__c = 'US';
        acc.Organisation_Type__c='School';
        acc.IsCreatedFromLead__c = true;
        acc.Abbreviated_Name__c='TestName';    
        insert acc;

         
        lstLeadRecords = TestDataFactory.createLead(1,'B2B');
        for(Lead lstLead : lstLeadRecords)
        { 
            lstLead.Channel__c ='Direct';
            lstLead.Organisation_Type1__c = 'Large Corporate';
            lstLead.Role__c = 'Employee';
            lstLead.Enquiry_Type__c = 'Product';
            lstLead.PostalCode = '345678';
            lstLead.State = 'Bihar';
            lstLead.Street = 'TestStreet';
            lstLead.Institution_Organisation__c =acc.Id;
        lstLead.Market__c = 'AU';
   
        }
        
            test.startTest();
            Database.insert(lstLeadRecords);
            Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
            for(Lead newLead : lstLeadRecords)
            {
                mapWithLead.put(newLead.Id,newLead);
            }
            for(Lead newLead : lstLeadRecords)
            {
                Database.LeadConvert convertLead = new database.LeadConvert();
                convertLead.setLeadId(newLead.ID);
                convertLead.setDoNotCreateOpportunity(false);
                convertLead.setConvertedStatus('Qualified');
                Database.LeadConvertResult lcr = Database.convertLead(convertLead);
                System.assert(lcr.isSuccess());
                accountId = lcr.getAccountId();
            }
            List<Lead> lstWithLead = [select isConverted,Campaign__c,ownerid,Do_Not_Send_Samples__c,Marketing_Opt_In__c,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId from lead where id IN: mapWithLead.keyset()];
            PS_LeadConversionMapping.createMapping(lstWithLead,mapWithLead);
            test.stopTest();
        }
    }
    //Added as part of CR-02295-Req6
     static testMethod void testLeadCreationWebRequest()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        lstWithTestUser[0].market__c='US';
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp; 
        AccountTeamMember newAccountTeamMember = new AccountTeamMember();
        List<Lead> lstLeadRecords = new List<Lead>();
        Id accountId;  
        System.runAs(lstWithTestUser[0]) 
        { 
            RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional' Limit 1];
            Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
            acc.ShippingStreet = 'TestStreet';
            acc.ShippingCity = 'Vns';
            acc.ShippingState = 'Delhi';
            acc.ShippingPostalCode = '234543';
            acc.ShippingCountry = 'India';
            acc.recordtypeId=OrgAcc.Id ;
            acc.Academic_Achievement__c='High';
            acc.Market2__c = 'US';
            acc.Organisation_Type__c='School';
            acc.IsCreatedFromLead__c = true;
            acc.SelfServiceCreated__c=false;
            acc.Abbreviated_Name__c='TestName';            
            insert acc;
            
            //Contact acc = new Contact (Name='testContact',
            List<Contact> lstContact = TestDataFactory.createContact(1);
            insert lstContact;
                        
            lstLeadRecords = TestDataFactory.createLead(1,'Web Request');
            
            for(Lead lstLead : lstLeadRecords)
            { 
                lstLead.Channel__c ='Direct';
                lstLead.Organisation_Type1__c = 'Large Corporate';
                lstLead.Role__c = 'Employee';
                lstLead.Role_Detail__c = 'Administrator';
                lstLead.Other_Role_Detail__c = 'Test';
                lstLead.Trust_Level__c = 'Low';
                lstLead.PostalCode = '345678';
                lstLead.State = 'Bihar';
                lstLead.Street = 'TestStreet';                
                lstLead.Institution_Organisation__c =acc.Id;
                lstLead.Contact_Lookup__c=lstContact[0].id;
                lstLead.Market__c = 'US';   
                lstLead.Enquiry_Type__c ='Digital Access';
            }
                        
            test.startTest();
            Database.insert(lstLeadRecords);
            Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
            for(Lead newLead : lstLeadRecords)
            {
                mapWithLead.put(newLead.Id,newLead);
            }
            for(Lead newLead : lstLeadRecords)
            {
                Database.LeadConvert convertLead = new database.LeadConvert();
                convertLead.setLeadId(newLead.ID);
                convertLead.setDoNotCreateOpportunity(false);
                convertLead.setConvertedStatus('Validated');
                Database.LeadConvertResult lcr = Database.convertLead(convertLead);
                System.assert(lcr.isSuccess());
                accountId = lcr.getAccountId();
            }
            List<Lead> lstWithLead = [select isConverted,Campaign__c,Marketing_Opt_In__c,Do_Not_Send_Samples__c,ownerid,LeadSource,Account_Lookup__c,Contact_Lookup__c,Comments__c,City,State,Country,CurrencyIsoCode,FirstName,Street,LastName,Phone,Email,Enquiry_Type__c,Validated_By__c,Validated_On__c,Trust_Level__c,Global_Marketing_Unsubscribe__c,Salutation,Role__c,Role_Detail__c,Other_Role_Detail__c,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,Market__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId from lead where id IN: mapWithLead.keyset()];
            PS_LeadConversionMapping.createMapping(lstWithLead,mapWithLead);
            test.stopTest();
        }
    }
    
    static testMethod void testLeadCreationWebRequest1()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        lstWithTestUser[0].market__c='US';
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp; 
        AccountTeamMember newAccountTeamMember = new AccountTeamMember();
        List<Lead> lstLeadRecords = new List<Lead>();
        Id accountId;  
        System.runAs(lstWithTestUser[0]) 
        { 
            RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional' Limit 1];
            Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
            acc.ShippingStreet = 'TestStreet';
            acc.ShippingCity = 'Vns';
            acc.ShippingState = 'Delhi';
            acc.ShippingPostalCode = '234543';
            acc.ShippingCountry = 'India';
            acc.recordtypeId=OrgAcc.Id ;
            acc.Academic_Achievement__c='High';
            acc.Market2__c = 'US';
            acc.Organisation_Type__c='School';
            acc.IsCreatedFromLead__c = true;
            acc.SelfServiceCreated__c=false;
            acc.Abbreviated_Name__c='TestName';            
            insert acc;
            
            //Contact acc = new Contact (Name='testContact',
            List<Contact> lstContact = TestDataFactory.createContact(1);
            insert lstContact;
                        
            lstLeadRecords = TestDataFactory.createLead(1,'Web Request');
            
            for(Lead lstLead : lstLeadRecords)
            { 
                lstLead.Channel__c ='Direct';
                lstLead.Organisation_Type1__c = 'Large Corporate';
                lstLead.Role__c = 'Employee';
                lstLead.Role_Detail__c = 'Administrator';
                lstLead.Other_Role_Detail__c = 'Test';
                lstLead.Trust_Level__c = 'Low';
                lstLead.PostalCode = '345678';
                lstLead.State = 'Bihar';
                lstLead.Street = 'TestStreet';                
                lstLead.Institution_Organisation__c =acc.Id;
                lstLead.Contact_Lookup__c=null;
                lstLead.Market__c = 'US';   
                lstLead.Enquiry_Type__c ='Digital Access';
            }
                        
            test.startTest();
            Database.insert(lstLeadRecords);
            Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
            for(Lead newLead : lstLeadRecords)
            {
                mapWithLead.put(newLead.Id,newLead);
            }
            for(Lead newLead : lstLeadRecords)
            {
                Database.LeadConvert convertLead = new database.LeadConvert();
                convertLead.setLeadId(newLead.ID);
                convertLead.setDoNotCreateOpportunity(false);
                convertLead.setConvertedStatus('Validated');
                Database.LeadConvertResult lcr = Database.convertLead(convertLead);
                System.assert(lcr.isSuccess());
                accountId = lcr.getAccountId();
            }
            List<Lead> lstWithLead = [select isConverted,Campaign__c,ownerid,Marketing_Opt_In__c,Do_Not_Send_Samples__c,LeadSource,Account_Lookup__c,SheerID_Verification_ID__c,Contact_Lookup__c,Comments__c,City,State,Country,CurrencyIsoCode,FirstName,Street,LastName,Phone,Email,Enquiry_Type__c,Validated_By__c,Validated_On__c,Trust_Level__c,Global_Marketing_Unsubscribe__c,PostalCode,Salutation,Role__c,Role_Detail__c,Other_Role_Detail__c,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId,Market__c from lead where id IN: mapWithLead.keyset()];
            List<Contact> lstContact2 = TestDataFactory.createContact(1);
                        
            lstContact2[0].LeadSource ='Online/Web';
            lstContact2[0].Market__c = lstWithLead[0].Market__c;
            lstContact2[0].Trust_Level__c= lstWithLead[0].Trust_Level__c;
            lstContact2[0].Validated_On__c =lstWithLead[0].Validated_On__c;
            lstContact2[0].Validated_By__c = lstWithLead[0].Validated_By__c;
            
            insert lstContact2[0];
            
            PS_LeadConversionMapping.createMapping(lstWithLead,mapWithLead);
            test.stopTest();
        }
    }
}
//testbau backup