/************************************************************************************************************
* Apex Class Name : CommonUtils_Test
* Version             : 1.0 
* Created Date        : 29/05/2019 
* Author              : Harika  
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class CommonUtils_Test {
    static testMethod void PositiveTest(){
        
        List<Account> lstacc = TestDataFactory.createAccount(1, 'Organisation');
        insert lstacc;
        CommonUtils.getAccountRecordType('Organisation', lstacc);
        List<Contact> lstcon = TestDataFactory.createContacts(1);
        insert lstcon; 
        List<Case> lstcs = TestDataFactory.createCase(1,'General');
        insert lstcs;
      
       
        CommonUtils.getContactbypassRecordType('Consumer', lstcon);
        CommonUtils.getCaseServiceRecordType('General', lstcs);
        /* List<Lead> lstLead = TestDataFactory.createLead(1, 'B2B');
insert lstLead;
createCase
CommonUtils.getLeadBypassANZRecordType('B2B', lstLead);*/
    }
}