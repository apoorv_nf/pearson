@isTest
Public class CustomProductLookupControllerTest{

static testmethod void testSearchSoql(){

    CustomProductLookupController lookupCtrl = new CustomProductLookupController();
    lookupCtrl.addNewFilter();
    lookupCtrl.addNewFilter();
    List<CustomProductLookupController.dynamicAddFilterSearch> tempList = lookupCtrl.listWithSelectOptions;
    CustomProductLookupController.dynamicAddFilterSearch filter = tempList.get(0);
    filter.searchText = 'Test';
    filter.selectedProductFilterValue = 'name';
    filter.selectedCondition = 'like %';
    
    CustomProductLookupController.dynamicAddFilterSearch filter1 = tempList.get(1);
    filter1.searchText = '23-04-2017';
    filter1.selectedProductFilterValue = 'publish_date__c';
    filter1.selectedCondition = '<';
    
   lookupCtrl.listWithSelectOptions = tempList;
    
    //tempList.add(lookupCtrl.options, lookupCtrl.condOptions, lookupCtrl.searchType, lookupCtrl.selectedSearchType, '', selectedCondition, toDisplayLookup, rowNumber);
    test.startTest();
    lookupCtrl.searchResults(true,'');    
    lookupCtrl.searchsosl();
    test.stopTest();
    }
    
    static testmethod void testSearchSoql1(){

    CustomProductLookupController lookupCtrl = new CustomProductLookupController();
    lookupCtrl.addNewFilter();
    lookupCtrl.addNewFilter();
    List<CustomProductLookupController.dynamicAddFilterSearch> tempList = lookupCtrl.listWithSelectOptions;
    CustomProductLookupController.dynamicAddFilterSearch filter = tempList.get(0);
    filter.searchText = 'Test';
    filter.selectedProductFilterValue = 'name';
    filter.selectedCondition = '=';
        
    CustomProductLookupController.dynamicAddFilterSearch filter2 = tempList.get(1);
    filter2.searchText = '23-04-2017';
    filter2.selectedProductFilterValue = 'publish_date__c';
    filter2.selectedCondition = '>';
    
    CustomProductLookupController.dynamicAddFilterSearch filter3 = tempList.get(2);
    lookupCtrl.listWithSelectOptions = tempList;
    test.startTest();
    lookupCtrl.searchResults(true,''); 
    test.stopTest();
        
    }
    
    static testmethod void testSearchSoql2(){

    CustomProductLookupController lookupCtrl = new CustomProductLookupController();
    lookupCtrl.searchText1 = '';
    lookupCtrl.searchText2 = '';
    lookupCtrl.selOptions1= '';
    lookupCtrl.selCondOptions1 = '';
    lookupCtrl.selOptions2 = '';
    lookupCtrl.selCondOptions2  = '';
    lookupCtrl.selandor = '';
    lookupCtrl.selectedValueFromPopup = '';
    
     
    lookupCtrl.addNewFilter();
    List<CustomProductLookupController.dynamicAddFilterSearch> tempList = lookupCtrl.listWithSelectOptions;
    
    test.startTest();
    CustomProductLookupController.dynamicAddFilterSearch filter = tempList.get(0);
    filter.searchText = 'Test';
    filter.selectedProductFilterValue = 'name';
    filter.selectedCondition = 'like';
    filter.selectedsearchType = '';
    filter.searchTextDate = '';
        
    CustomProductLookupController.dynamicAddFilterSearch filter2 = tempList.get(1);
    filter2.searchText = '04/23/20de17';
    filter2.selectedProductFilterValue = 'publish_date__c';
    filter2.selectedCondition = '!=';
   
    lookupCtrl.listWithSelectOptions = tempList;
    lookupCtrl.searchResults(true,''); 
    test.stopTest();
        
    }
    
     static testmethod void testSearchSoql3(){

    CustomProductLookupController lookupCtrl = new CustomProductLookupController();
    lookupCtrl.addNewFilter();
    lookupCtrl.addNewFilter();
    lookupCtrl.addNewFilter();
    lookupCtrl.addNewFilter();
    List<CustomProductLookupController.dynamicAddFilterSearch> tempList = lookupCtrl.listWithSelectOptions;
    CustomProductLookupController.dynamicAddFilterSearch filter = tempList.get(0);
    filter.searchText = '04/23/2017';
    filter.selectedProductFilterValue = 'publish_date__c';
    filter.selectedCondition = '=';
        
    CustomProductLookupController.dynamicAddFilterSearch filter2 = tempList.get(1);
    filter2.searchText = 'Test';
    filter2.selectedProductFilterValue = 'brand__c';
    filter2.selectedCondition = 'like %%';
    lookupCtrl.listWithSelectOptions = tempList;
    
    test.startTest();  
         
    lookupCtrl.searchResults(true,''); 
    lookupCtrl.convertToDate('');
    test.stopTest();
        
    }
    
    static testmethod void testSearchSosl(){

    CustomProductLookupController lookupCtrl = new CustomProductLookupController();
    lookupCtrl.gsearch = 'Test';
    List<CustomProductLookupController.dynamicAddFilterSearch> tempList = lookupCtrl.listWithSelectOptions;
    CustomProductLookupController.dynamicAddFilterSearch filter1 = tempList.get(0);
    filter1.searchText = '';
    filter1.selectedProductFilterValue = '--None--';
    filter1.selectedCondition = '<';
    lookupCtrl.searchsosl();
    }
    
    static testmethod void testSearchSoslwithSoql(){

    CustomProductLookupController lookupCtrl = new CustomProductLookupController();
    lookupCtrl.gsearch = 'Test';
    List<CustomProductLookupController.dynamicAddFilterSearch> tempList = lookupCtrl.listWithSelectOptions;
    CustomProductLookupController.dynamicAddFilterSearch filter1 = tempList.get(0);
    filter1.searchText = 'Test';
    filter1.selectedProductFilterValue = 'Name';
    filter1.selectedCondition = '=';
    lookupCtrl.searchsosl();
    }
    
    static testmethod void testSearchSoslNoData(){

    CustomProductLookupController lookupCtrl = new CustomProductLookupController();
    lookupCtrl.gsearch = '';
    List<CustomProductLookupController.dynamicAddFilterSearch> tempList = lookupCtrl.listWithSelectOptions;
    CustomProductLookupController.dynamicAddFilterSearch filter1 = tempList.get(0);
    filter1.searchText = '';
    filter1.selectedProductFilterValue = '--None--';
    filter1.selectedCondition = '';
    lookupCtrl.searchsosl();
    }

    static testmethod void testremoveFilter(){
     CustomProductLookupController lookupCtrl = new CustomProductLookupController();
     lookupCtrl.rowToRemove = 0;
     lookupCtrl.removeFilter();       
        
    }
    
    static testmethod void testchangeSerachTextType(){
    PageReference testPage = Page.customProductLookup; 
    testPage.getParameters().put('selectedRow', '0'); //Page requires an object id (Opportunity or University Course)
    
    Test.setCurrentPageReference(testPage);
    CustomProductLookupController lookupCtrl = new CustomProductLookupController();
    lookupCtrl.addNewFilter();
    lookupCtrl.addNewFilter();
    List<CustomProductLookupController.dynamicAddFilterSearch> tempList = lookupCtrl.listWithSelectOptions;
    CustomProductLookupController.dynamicAddFilterSearch filter = tempList.get(0);
    filter.searchText = 'Test';
    filter.selectedProductFilterValue = 'name';
    //filter.selectedCondition = 'like %';
    
    CustomProductLookupController.dynamicAddFilterSearch filter1 = tempList.get(1);
    filter1.searchText = '23-04-2017';
    filter1.selectedProductFilterValue = 'publish_date__c';
    //filter1.selectedCondition = '<';
    
    CustomProductLookupController.dynamicAddFilterSearch filter2 = tempList.get(2);
    filter2.searchText = 'true';
    filter2.selectedProductFilterValue = 'isactive';
    //filter2.selectedCondition = '=';
    lookupCtrl.listWithSelectOptions = tempList;
    lookupCtrl.changeSerachTextType();
    
    testPage.getParameters().put('selectedRow', '1'); //Page requires an object id (Opportunity or University Course)
    Test.setCurrentPageReference(testPage);
    lookupCtrl.changeSerachTextType();
    
    testPage.getParameters().put('selectedRow', '2'); //Page requires an object id (Opportunity or University Course)
    Test.setCurrentPageReference(testPage);
    lookupCtrl.changeSerachTextType();
     
        
    }
    
    static testmethod void testgetStatuses(){
    PageReference testPage = Page.customProductLookup; 
    testPage.getParameters().put('fieldName', 'isactive'); //Page requires an object id (Opportunity or University Course)
    Test.setCurrentPageReference(testPage);
    CustomProductLookupController lookupCtrl = new CustomProductLookupController();
    lookupCtrl.getStatuses();
        
    testPage.getParameters().put('fieldName', 'status__c'); //Page requires an object id (Opportunity or University Course)
    Test.setCurrentPageReference(testPage);
    lookupCtrl.getStatuses();   
        
        
    }
    
    static testmethod void testgetTextBox(){
    PageReference testPage = Page.customProductLookup; 
    testPage.getParameters().put('txt', 'txt'); //Page requires an object id (Opportunity or University Course)
    
    Test.setCurrentPageReference(testPage);
    CustomProductLookupController lookupCtrl = new CustomProductLookupController();
    lookupCtrl.getTextBox();
    System.assertEquals('txt', 'txt');
        
        
        
    }
    
    static testmethod void testgetFormTag(){
     PageReference testPage = Page.customProductLookup;       
        
    testPage.getParameters().put('frm', 'frm'); //Page requires an object id (Opportunity or University Course)
    Test.setCurrentPageReference(testPage);
    CustomProductLookupController lookupCtrl = new CustomProductLookupController();
    lookupCtrl.getFormTag(); 
    System.assertEquals('frm', 'frm');
        
    }
}