/*******************************************************************************************************************
* Apex Class Name  : CreateEditQuoteControllerTestClass
* Version          : 1.0 
* Created Date     : 30 March 2015
* Function         : Test Class of the CreateEditQuoteController Class
* Modification Log :
*
* Developer                                Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*   Accenture IDC                          18/04/2015              Created Initial Version
*******************************************************************************************************************/

@isTest  (SeeAllData=True)                 
public class CreateEditQuoteControllerTestClass{
 static testMethod void myTest() {
  date dateToday;        
  Account acc = new Account();
     acc.Name = 'Test';
     insert acc;
     

        List<Contact> con = new List<Contact>();          
        con = TestDataFactory.createContact(1);           
        insert con;          
      
   //  Account newacc = [Select id,Createddate from Account where Id=:acc.Id];
     
     
      Opportunity nopp = new opportunity();
      nopp.AccountId = acc.id;    
      nopp.Name = 'OppTest';
      nopp.StageName = 'Qualification';
     // nopp.Academic_Start_Date__c =system.today();
      nopp.CloseDate = system.today();
      insert nopp;
     //Asha added the below for coverage
     Opportunity Opp = [Select Id,Academic_Start_Date__c,Createddate from Opportunity where Id=:nopp.Id];
     DateTime dT = Opp.Createddate;
     Opp.Academic_Start_Date__c=date.newinstance(dT.year(), dT.month(), dT.day());
     update Opp;
    
     OpportunityContactRole ocr = new OpportunityContactRole();
     ocr.ContactId=con[0].id;
     ocr.IsPrimary = True;
     ocr.Role='Evaluator';
     ocr.OpportunityId = opp.id;     
     insert ocr;
       
       test.starttest();
        Id Pricelistid ;
      PageReference pageRef = Page.CreateEditQuote; //replace with your VF page name
         pageRef.getParameters().put('id', opp.id);
          Test.setCurrentPage(pageRef);
      
       CreateEditQuoteController ceq = new CreateEditQuoteController();
       ceq.opportunityId = opp.id;
       ceq.onLoad();
       ceq.calcuate_priceList();
      ApexPages.currentPage().getParameters().put('id',opp.id);
       test.stoptest();
 
 }
}