/************************************************************************************************************
* Apex Class Name   : PS_AssetUtilities.cls
* Version           : 1.0 
* Created Date      : 29 Jan 2016
* Function          : Asset related utility methods
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Tom Carman 				29/01/2016					Initial implementation - refactor of existing code
														that was in OrderItemTriggerHandler, with more explicit
														error handling.        												                                                      
************************************************************************************************************/
  

public class PS_AssetUtilities {

	private static final String ORDERITEM_STATUS_SHIPPED = 'Shipped';
	private static final String ORDERITEM_STATUS_CANCELLED = 'Cancelled';
	private static final String ASSET_STATUS_PENDING = 'Pending';
	private static final String ASSET_STATUS_ACTIVE = 'Active';
	private static final String ASSET_STATUS_CANCELLED = 'Cancelled';
	private static final String ASSET_PRODUCT_RELATIONSHIP = 'Product2Id';

	public static void updateRelatedAssetStatus(Map<Id, OrderItem> orderItemMap) {

		Map<Id, Asset> assetsToUpdate = getAssetsToUpdate(orderItemMap.values());
		updateAssetStatus(orderItemMap, assetsToUpdate);

	}


	private static void updateAssetStatus(Map<Id, OrderItem> orderItemMap, Map<Id, Asset> incomingAssets) {

		PS_ExceptionLogger logger = new PS_ExceptionLogger();

		List<Asset> assetsToUpdate = new List<Asset>();

		Map<Id, OrderItem> assetToOrderItemMap = new Map<Id, OrderItem>(); 

		for(Asset asset : incomingAssets.values()) {

			assetToOrderItemMap.put(asset.Id, orderItemMap.get(asset.Order_Product_Id__c)); 

			OrderItem relatedOrderItem = orderItemMap.get(asset.Order_Product_Id__c);

			if(relatedOrderItem.Status__c == ORDERITEM_STATUS_SHIPPED) {
				asset.Status__c = ASSET_STATUS_ACTIVE;
			} else if (relatedOrderItem.Status__c == ORDERITEM_STATUS_CANCELLED) {
				asset.Status__c = ASSET_STATUS_CANCELLED;
			}
			
			assetsToUpdate.add(asset);
		
		}

		try {

			update assetsToUpdate;

		} catch (DMLException dmlEx) {

			for(Integer i=0; i < dmlEx.getNumDml(); i++) {
				if(errorDueToAssetProductLookupFilter(dmlEx, i)) { 
					// Failed to update the Asset to 'Active' because the Current User violates Assset.Product2 lookup filter
					// and therefore cannot modify the record.
					// return friendly error message
					assetToOrderItemMap.get(dmlEx.getDmlId(i)).addError(Label.PS_AssetProductFilterError + ' ' + incomingAssets.get(dmlEx.getDmlId(i)).Product2.Name);// +'. ' + 'For more information contact your System Administrator with the following referece: ' + logger.getErrorCode(ex));
				} else {
					throw dmlEx;
				}
			}
		}	
	}
		


	private static Map<Id, Asset> getAssetsToUpdate (List<OrderItem> orderItemList) {

		Set<Id> orderItemIdsWhichNeedAssetsUpdating = new Set<Id>();

		// filter orders with status of Shipped or Cancelled, these need related Assets updating
		for(OrderItem orderItem : orderItemList) {

			if(orderItem.Status__c == ORDERITEM_STATUS_SHIPPED || orderItem.Status__c == ORDERITEM_STATUS_CANCELLED) {
				orderItemIdsWhichNeedAssetsUpdating.add(orderItem.Id);
			}
		}


		Map<Id, Asset> assetMap = new Map<Id, Asset>([SELECT  Id, 
																Order_Product_Id__c,
																Status__c,
																Product2.Name
														FROM Asset 
														WHERE Order_Product_Id__c IN :orderItemIdsWhichNeedAssetsUpdating
														AND Status__c = :ASSET_STATUS_PENDING]);
		
		return assetMap;

	}


	private static Boolean errorDueToAssetProductLookupFilter(DMLException dmlEx, Integer row) {

		// A User can bypass this filter if they have User.Bypass_Product_Lookup_Filter_On_Asset__c = TRUE

		return dmlEx.getDmlType(row) == System.StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION
		&& (new Set<String>(dmlEx.getDMLFieldNames(row)).contains(ASSET_PRODUCT_RELATIONSHIP));


	}







}