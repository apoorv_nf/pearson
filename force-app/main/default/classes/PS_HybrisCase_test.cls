/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_HybrisCase_test.cls 
   Description:     Test Class to cover code coverage PS_HybrisCase
   Date:            06/19/2019
   Author:          Tamizharasan Pushparaj
------------------------------------------------------------------------------------------------------------------------------------------------------------- */

   
@isTest
    public class PS_HybrisCase_test {
        Public static testmethod void Hybriscase1(){
           User u1 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = UserInfo.getProfileId(), Sample_Approval__c= true,Order_Approver__c= UserInfo.getUserId(),
                               TimeZoneSidKey='America/Los_Angeles', UserName='test1234'+Math.random()+'@pearson.com.testbau', Geography__c = 'Growth',
                               Market__c = 'US',Line_of_Business__c = 'Higher Ed');
            insert u1;
            


             set <string> MDM = new set<string>();
             MDM.add('5056773');
            
            set <string> Email = new set<string>();
            Email.add('samplee1mailaddress1112@email.com');
    
             system.runas(u1) {
                  /*Bypass_Settings__c settings = new Bypass_Settings__c();
              	  settings.Disable_Triggers__c = true;
                  settings.SetupOwnerId = u1.id;
                  insert settings;*/ 
                  Test.starttest(); 
                Account AccountRec = new account(Name='Test Account1', Phone='+9100000' ,IsCreatedFromLead__c = True, 
                                                 ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', 
                                                 ShippingPostalCode = '560037',MDM_ID__c  = '5056773');
                insert AccountRec;
                 
                    Contact contactRecord = new Contact(FirstName='TC1Firstname', LastName='TCL1astname' ,
                                                        AccountId=AccountRec.Id ,Salutation='MR.', Email='samplee1mailaddress1112@email.com', 
                                                        Phone='11122233355555', Preferred_Address__c = 'Mailing Address', MailingCountry = 'United Kingdom', 
                                                        MailingStreet = '1 Street', MailingPostalCode = 'NE27 0QQ', MailingCity  = 'City');     
                   
                 insert contactRecord;
                 
                 
                 LIST<Case> caseList=new LIST<Case>();
                 String caseRecordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
                 for (integer iCnt=0;iCnt<5; iCnt++) {
                     Case csRec=new Case(
                     RecordTypeID=caseRecordtypeid, 
                     AccountID=AccountRec.ID,
                     ContactID=contactRecord.ID,
                     Origin='Email',
                     Category__c='Access Code', 
                     Contact_Country__c='United States',
                     Contact_Type__c='K-12 Student',
                     Market__c='AU',
                     Priority='Normal',
                     Subject='Test',
                     Platform__c='ActiveLearn',
                     PS_Business_Vertical__c='Higher Education',
                     Supplied_MDM_ID__c = '5056773', 
                     SuppliedEmail = 'test@test.com',    
                     Subcategory__c='Multiple Accounts',
                     Request_Type__c='Career Colleges',
                     SuppliedName = 'test user',
                     Description='Test'+iCnt );
                     caseList.add(csRec);
                     
                 }
                 
                 insert caselist;
             
                 PS_HybrisCase.hybrisCaseCreation(caseList,MDM,Email,false);
                } 
        }
        
        
        
         Public static testmethod void Hybriscase2(){
           User u1 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = UserInfo.getProfileId(), Sample_Approval__c= true,Order_Approver__c= UserInfo.getUserId(),
                               TimeZoneSidKey='America/Los_Angeles', UserName='test1234'+Math.random()+'@pearson.com.testbau', Geography__c = 'Growth',
                               Market__c = 'US',Line_of_Business__c = 'Higher Ed');
            insert u1;
            
             set <string> MDM = new set<string>();
             MDM.add('5056773');
            
            set <string> Email = new set<string>();
            Email.add('samplee1mailaddress1112@email.com');
    
             system.runas(u1) {
              Bypass_Settings__c settings = new Bypass_Settings__c();
              settings.Disable_Triggers__c = true;
              settings.SetupOwnerId = u1.id;
              insert settings; 
              Test.starttest(); 
                Account AccountRec = new account(Name='Test Account1', Phone='+9100000' ,IsCreatedFromLead__c = True, 
                                                 ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', 
                                                 ShippingPostalCode = '560037',MDM_ID__c  = '5056773');
                insert AccountRec;
                 
                    Contact contactRecord = new Contact(FirstName='TC1Firstname', LastName='TCL1astname' ,
                                                        AccountId=AccountRec.Id ,Salutation='MR.', Email='samplee1mailaddress1112@email.com', 
                                                        Phone='11122233355555', Preferred_Address__c = 'Mailing Address', MailingCountry = 'United Kingdom', 
                                                        MailingStreet = '1 Street', MailingPostalCode = 'NE27 0QQ', MailingCity  = 'City');     
                   
                 insert contactRecord;
                 
                 
                  LIST<Case> caseList=new LIST<Case>();
                 String caseRecordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
                 for (integer iCnt=0;iCnt<5; iCnt++) {
                     Case csRec=new Case(
                     RecordTypeID=caseRecordtypeid, 
                     AccountID=AccountRec.ID,
                     ContactID=contactRecord.ID,
                     Origin='Email',
                     Category__c='Access Code', 
                     Contact_Country__c='United States',
                     Contact_Type__c='K-12 Student',
                     Market__c='AU',
                     Priority='Normal',
                     Subject='Test',
                     Platform__c='ActiveLearn',
                     PS_Business_Vertical__c='Higher Education',
                     Supplied_MDM_ID__c = '5056772', 
                     SuppliedEmail = 'samplee1mailaddress1112@email.com',    
                     Subcategory__c='Multiple Accounts',
                     Request_Type__c='Career Colleges',
                     SuppliedName = 'test user',
                     Description='Test'+iCnt );
                     caseList.add(csRec);
                     
                 }
                 
                 insert caselist;
             
                 PS_HybrisCase.hybrisCaseCreation(caseList,MDM,Email,false);
                } 
        }
        
          Public static testmethod void Hybriscase3(){
           User u1 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = UserInfo.getProfileId(), Sample_Approval__c= true,Order_Approver__c= UserInfo.getUserId(),
                               TimeZoneSidKey='America/Los_Angeles', UserName='test1234'+Math.random()+'@pearson.com.testbau', Geography__c = 'Growth',
                               Market__c = 'US',Line_of_Business__c = 'Higher Ed');
            insert u1;
            
             set <string> MDM = new set<string>();
             MDM.add('5056773');
            
            set <string> Email = new set<string>();
            Email.add('samplee1mailaddress1112@email.com');
    
             system.runas(u1) {
              Bypass_Settings__c settings = new Bypass_Settings__c();
              settings.Disable_Triggers__c = true;
              settings.SetupOwnerId = u1.id;
              insert settings; 
              Test.starttest(); 
                Account AccountRec = new account(Name='Test Account1', Phone='+9100000' ,IsCreatedFromLead__c = True, 
                                                 ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', 
                                                 ShippingPostalCode = '560037',MDM_ID__c  = '5056773');
                insert AccountRec;
                 
                    Contact contactRecord = new Contact(FirstName='TC1Firstname', LastName='TCL1astname' ,
                                                        AccountId=AccountRec.Id ,Salutation='MR.', Email='samplee1mailaddress1112@email.com', 
                                                        Phone='11122233355555', Preferred_Address__c = 'Mailing Address', MailingCountry = 'United Kingdom', 
                                                        MailingStreet = '1 Street', MailingPostalCode = 'NE27 0QQ', MailingCity  = 'City');     
                   
                 insert contactRecord;
                 
                 
                  LIST<Case> caseList=new LIST<Case>();
                 String caseRecordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
                 for (integer iCnt=0;iCnt<5; iCnt++) {
                     Case csRec=new Case(
                     RecordTypeID=caseRecordtypeid, 
                     AccountID=AccountRec.ID,
                     ContactID=contactRecord.ID,
                     Origin='Email',
                     Category__c='Access Code', 
                     Contact_Country__c='United States',
                     Contact_Type__c='K-12 Student',
                     Market__c='AU',
                     Priority='Normal',
                     Subject='Test',
                     Platform__c='ActiveLearn',
                     PS_Business_Vertical__c='Higher Education', 
                     SuppliedEmail = 'samplee1mailaddress1112@email.com',    
                     Subcategory__c='Multiple Accounts',
                     Request_Type__c='Career Colleges',
                     SuppliedName = 'test user',
                     Description='Test'+iCnt );
                     caseList.add(csRec);
                     
                 }
                 
                 insert caselist;
             
                 PS_HybrisCase.hybrisCaseCreation(caseList,MDM,Email,true);
                } 
        }
        
    }