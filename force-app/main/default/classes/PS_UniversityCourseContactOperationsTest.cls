/**
 * Name : PS_UniversityCourseContactOperationsTest 
 * Author : Accenture_Karan
 * Description : Test class used for testing PS_UniversityCourseContactOperations
 * Date : 12/10/15  
 
 * Version : <intial Draft> 
 */


@isTest
public class PS_UniversityCourseContactOperationsTest {
    
    static testMethod void testUniversityCourseContactOperations(){
    
        // Test data preparation
            
        List<Account> accountList = TestDataFactory.createAccount(1,'Organisation');    
        insert accountList ;
        
        List<Contact> contactList=TestDataFactory.createContact(1);
        insert contactList;
        //List<Contact> contactList2=TestDataFactory.createContact(2);
        //insert contactList2;
        
        list<UniversityCourse__c> courList = TestDataFactory.insertCourse();
        courList[0].Account__c = accountList[0].Id;
        courList[1].Account__c = accountList[0].Id;
        courList[2].Account__c = accountList[0].Id;
        insert courList;
        
        /*Primary_Contact_Field_Hierarchy__c instval =  new Primary_Contact_Field_Hierarchy__c();
            instval.Name = 'custSettName';
            instval.Contact_Role__c = 'Testone';
            instval.Role_level_Number__c = 1;
            insert instval;*/
            
        System.debug('courList size=' + courList.size());
        
        list<UniversityCourseContact__c> courContList = TestDataFactory.insertCourseContact (1, courList[0].Id, contactList[0].id);
        courContList[0].PS_Contact_Role__c= 'Primary Sales Contact';
         insert courContList;        
        
       System.debug('courContList size=' + courContList.size() + 'CourID='+ courList[0].Id + '------ ContactID='+ contactList[0].id);
        //09/20/2016 CP removed next lines because failure is not happening on DB to be caught by exception in class - removed to increase coverage to 46%
         //courList[0].Primary_Contact__c =courContList[0].Contact__c;  // added by Raushan Kumar for test failure .
         //update courList;
        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =: 'Chatter Free User'].Id,1);
        insert usList;
        
        UniversityCourse__c univCourse = new UniversityCourse__c();
        univCourse = [SELECT Id, Primary_Contact__c FROM UniversityCourse__c WHERE Id=:courList[0].Id LIMIT 1];
        
        
        
        test.startTest();
        
            PS_UniversityCourseContactOperations uCCOHandler = new PS_UniversityCourseContactOperations();          
            /*uCCOHandler.updatePrimaryContact(courContList);
            System.debug('univeCourseContact=' + univCourse.Primary_Contact__c + '-----courContList[0].Contact__c='+courContList[0].Contact__c);
           // System.assertEquals(univCourse.Primary_Contact__c,courContList[0].Contact__c);
            update courContList;
            delete courContList;
            undelete courContList;*/
            
            //Update the Primary Contact based on Course Contact Role CI Nov Release
            List<Contact> contactListHierarchy = TestDataFactory.createContact(1);
            insert contactListHierarchy;
            
            List<string> CustSettroleLists = new List<string>();
            CustSettroleLists.add('Testone');
            CustSettroleLists.add('Testtwo');
            CustSettroleLists.add('Testthree');
            List<Primary_Contact_Field_Hierarchy__c> primContCustSettList = new List<Primary_Contact_Field_Hierarchy__c>();
            primContCustSettList = TestDataFactory.createPrimContFieldHierarchyCustomSett(CustSettroleLists);
            //Database.SaveResult[] srList = Database.insert(primContCustSettList, false);
            system.debug('The List of CustomSetting===='+primContCustSettList);
            insert primContCustSettList;
            
            list<UniversityCourseContact__c> courContListHierarchy = TestDataFactory.insertCourseContact (1, courList[0].Id, contactListHierarchy[0].id);
            courContListHierarchy[0].PS_Contact_Role__c= 'Testone';
            insert courContListHierarchy;
            update courContListHierarchy;
            delete courContListHierarchy;
       
        test.stopTest();
        
    
    }
}