/*******************************************************************************************************************
 * Apex Class Name  : PS_InternalRequestController 
 * Version          : 1.0
 * Created Date     : 20 November 2015
 * Function         : To check validations  on Integration Request approval Process 
 * Modification Log :
 *
 * Developer                   Date                    Description
 * ------------------------------------------------------------------------------------------------------------------
 * Madhusudhan              20/11/2015             To check validations  on Integration Request approval Process
  * ------------------------------------------------------------------------------------------------------------------
 * Karan Khanna             22/01/2016             Added check of US Market before Role validation and done the exception handling
 * ------------------------------------------------------------------------------------------------------------------
 * Madhusudhan              06/06/2016             Added US BU check 
 * ------------------------------------------------------------------------------------------------------------------
 * Gayatri Keshkamat        25/08/2016             Consider "File" type of documents in addition to Attachment 
 *                            type in Submitting for Approval 
 *                          
 
 * --------------------------------------------------------------------------------------------------------------------
  *******************************************************************************************************************/

public class PS_InternalRequestController
{
    public PS_InternalRequestController() 
    {

    }

    public PageReference approveProcess()
    {
        try
        {
            String UserRole = null;
            String UserMarket = null;
            String UserBU = null;
            PageReference  returnPage = null;
            Id InterRequest = ApexPages.currentPage().getParameters().get('Id');
            
            List<Attachment> attachment = [Select id From Attachment where ParentId =:InterRequest];
            //Also Consider "File" type of Documents for IR submit for Approval- D-5788 -Gayatri
            List<ContentDocumentLink > contentDocLink = new List<ContentDocumentLink >();
            List<Internal_Request__c> combinedAtt =new List<Internal_Request__c >();            
            if(attachment.size() == 0){
                contentDocLink = [Select id,ContentDocumentId  
                                  From ContentDocumentLink where LinkedEntityId  =:InterRequest]; 
                combinedAtt=[Select Id,(SELECT Id, RecordType,ParentId FROM CombinedAttachments where ParentId = :InterRequest and RecordType='Google Doc') FROM Internal_Request__c where id = :InterRequest];
                }
            // added user BU
            List<User> loggedInUser = [select Market__c, UserRole.Name, Business_Unit__c from User where id =: UserInfo.getUserId()];   
            
            if(!loggedInUser.isEmpty())
            {
                for(User u: loggedInUser)
                {
                    UserRole = u.UserRole.Name;
                    UserMarket = u.Market__c;
                    UserBU    =  u.Business_Unit__c;
                } 
            }  
            // added user BU check 
            if(UserMarket == Label.PS_USMarket &&  UserBU == Label.PS_BU && UserRole != null && !(UserRole.contains(Label.PS_NA_Sales_DM) || UserRole.contains(Label.PS_NA_Sales_RM)))
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.DMandRMcanSubmitCaseForApproval));
                returnPage = null;
            }
            //(contentDocLink) Also Consider "File" type of Documents for IR submit for Approval- D-5788 -Gayatri
            if(attachment.size()>0 || Test.isRunningTest() || contentDocLink.size() >0 || combinedAtt[0].CombinedAttachments.size() >0){
                              
                // Create an approval request 
                Approval.ProcessSubmitRequest req =  new Approval.ProcessSubmitRequest();
                req.setComments('Submitting request for approval.');
                req.setObjectId(InterRequest );
                Approval.ProcessResult result = Approval.process(req); 
                PageReference pg = new PageReference('/'+InterRequest );
                pg.setRedirect(true);
                returnPage = pg;
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.ErrorOnCaseIfAttachmentMissing));
                returnPage = null;  
            }


            return returnPage ;
        }
        catch (exception e) 
        {
            if(e.getMessage().contains('MANAGER_NOT_DEFINED'))
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.PS_InternalRequest_NoManagerError));
                System.Debug('### Exception has occurred in method approveProcess ' + e);            
                return null;
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                System.Debug('### Exception has occurred in method approveProcess ' + e);            
                return null;
            }
                       
        }
    }

}