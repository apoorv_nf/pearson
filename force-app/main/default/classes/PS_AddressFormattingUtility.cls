/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_AddressFormattingUtility
   Description:     A Utility class for fomatting the Address
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   08 Mar 2016      1.0           Accenture NDC                                         Created
   18 April 2016    1.0            Raushan Kumar                                    Removal of Business Unit check for D-4817
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

Public class PS_AddressFormattingUtility
{
  
  //Method for formatting the order shipping address
  public Static String formatOrderShippingAddress(String accountName,String mainSellingAccount,String streetAddress,Boolean preferredHomeAddress,String preferredAddress,String market,String businessUnit)
  {
      String formattedAddressString =''; 
      if(isANZOrder(market) && preferredHomeAddress == False && (preferredAddress == Label.PS_CreateSampleOrderMailingAddress || preferredAddress == Label.PS_CreateSampleOrderOtherAddress))
      {
          formattedAddressString  = returnANZStreetAddressFormat(accountName,mainSellingAccount,streetAddress,True);
              
      }else if(isANZOrder(market) && preferredHomeAddress == True && (preferredAddress == Label.PS_CreateSampleOrderMailingAddress || preferredAddress == Label.PS_CreateSampleOrderOtherAddress))
      {
          formattedAddressString  = returnANZStreetAddressFormat(null,null,streetAddress,False);
          
      }else if(isANZOrder(market) && preferredAddress == Label.PS_CreateSampleOrderAccountAddress)
      {
          formattedAddressString  = returnANZStreetAddressFormat(accountName,mainSellingAccount,streetAddress,True);
          
      }else if(!isANZOrder(market))
      {
          formattedAddressString  = returnANZStreetAddressFormat(null,null,streetAddress,False);
      } 
      return formattedAddressString;
  }
  
  //Method to populate the street address in ANZ format
  Private static string returnANZStreetAddressFormat(String accountName,String mainSellingAccount,String streetAddress,Boolean isANZAndPreferredHomeAddress)
  {
      String formattedANZAddressString = '';
      if(accountName != null && accountName != '')
      {
          formattedANZAddressString= (accountName.length()<=30)?accountName:stringTrimmer(accountName);
      }
      if(mainSellingAccount != null && mainSellingAccount !='')
      {
          formattedANZAddressString = formattedANZAddressString+'\n'+((mainSellingAccount.length()<=30)?mainSellingAccount:stringTrimmer(mainSellingAccount));
      }
      if(streetAddress != null && streetAddress != '')
      {
          formattedANZAddressString = formattedANZAddressString+'\n'+((streetAddress.length()<=30)?streetAddress:returnStreetAddressInStandardFormat(streetAddress,isANZAndPreferredHomeAddress));
      }
      return formattedANZAddressString;
  }
  
  //Method to populate the street address in standard format which is compatible with ESB
  Private Static String returnStreetAddressInStandardFormat(String streetAddress,Boolean isANZAndPreferredHomeAddress)
  {
      String streetAddressLine1 = '';
      String streetAddressLine2 = '';
      String streetAddressLine3 = '';
      String streetAddressLine4 = '';
      String finalConcatenatedStreetAddress;
      Integer lengthOfStreetAddress = 0;
      
      if(streetAddress != '' && streetAddress != null)
      {
          lengthOfStreetAddress = streetAddress.length();
      }
      
      //If it is ANZ order and preferred home address, then populate only first two lines else populate four lines
      if(streetAddress != null && streetAddress != '' && isANZAndPreferredHomeAddress == True && streetAddress.length() > 30)
      {
          streetAddressLine1 = stringTrimmer(streetAddress);
          if(streetAddress.length() >= 60)
          {
              streetAddressLine2 = streetAddress.substring(30,60);
          }else
          {
              streetAddressLine2 = streetAddress.substring(30,lengthOfStreetAddress);
          }
          finalConcatenatedStreetAddress = streetAddressLine1+'\n'+streetAddressLine2;  
          
      }else if(streetAddress != null && streetAddress != '' && isANZAndPreferredHomeAddress == False && streetAddress.length() > 30)
      {
          streetAddressLine1 = stringTrimmer(streetAddress);
          if(streetAddress.length() >= 60)
          {
              streetAddressLine2 = streetAddress.substring(30,60);
          }else
          {
              streetAddressLine2 = streetAddress.substring(30,lengthOfStreetAddress);
          }
          if(streetAddress.length() >= 90)
          {
              streetAddressLine3 = streetAddress.substring(60,90);
          }else if(streetAddress.length() >= 60)
          {
              streetAddressLine3 = streetAddress.substring(60,lengthOfStreetAddress);
          }
          if(streetAddress.length() >= 120)
          {
              streetAddressLine4 = streetAddress.substring(90,120);
          }else if(streetAddress.length() >= 90)
          {
              streetAddressLine4 = streetAddress.substring(90,lengthOfStreetAddress);
          }
          finalConcatenatedStreetAddress = streetAddressLine1+'\n'+streetAddressLine2+'\n'+streetAddressLine3+'\n'+streetAddressLine4;
      }
      return finalConcatenatedStreetAddress;
  }
   
  //Method to check if the order belongs to ANZ market
  //Removed the business unit condition for D-4817 -18th April 2016
  Private static boolean isANZOrder(String market)
  {
      Boolean isANZOrder = False; 
      if(market != null)
      {
          if(market == Label.PS_AUMarket || market == Label.PS_NZMarket)
          {
              isANZOrder = True;
          }
      }
      return isANZOrder;
  }
  
  //Method to trim and return the string if length of the string is greater than 30 characters
  Private static string stringTrimmer(String strToBeTrimmed)
  {
      if(strToBeTrimmed != null && strToBeTrimmed != '')
      {
          if(strToBeTrimmed.length()>30)
          {
              strToBeTrimmed = strToBeTrimmed.substring(0,30);
          }else
          {
              strToBeTrimmed = strToBeTrimmed;
          }
      }
      return strToBeTrimmed;
  }
  
  //Method to return order street address in standard format which is being called from RetrieveOneCRMHandler
  Public static List<string> standardStreetAddressForGlobalOrders(String streetAddress)
  {
      List<String> streetList = new List<String>();
      Integer lengthOfStreetAddress = 0;
      if(streetAddress != null && streetAddress.length() > 30)
      {
          lengthOfStreetAddress = streetAddress.length();
          system.debug(streetAddress);
          system.debug(stringTrimmer(streetAddress));
          streetList.add(stringTrimmer(streetAddress));
          if(streetAddress.length() >= 60)
          {
              streetList.add(streetAddress.substring(30,60));
          }else
          {
              streetList.add(streetAddress.substring(30,lengthOfStreetAddress));
          }
          if(streetAddress.length() >= 90)
          {
              streetList.add(streetAddress.substring(60,90));
              
          }else if(streetAddress.length() >= 60)
          {
              streetList.add(streetAddress.substring(60,lengthOfStreetAddress));
          }
          if(streetAddress.length() >= 120)
          {
              streetList.add(streetAddress.substring(90,120));
              
          }else if(streetAddress.length() >= 90)
          {
              streetList.add(streetAddress.substring(90,lengthOfStreetAddress));
          }
      }
      return streetList;    
  }
}