public with sharing class EinsteinBotAssociateMessagingUser {

    @InvocableMethod(label='Einstein Bot - Associate Messaging User To Contact')
    public static void associateMessaginguser(List<ContactInfoInput> ContactInfoInputs){
		try{
            if(ContactInfoInputs != NULL && ContactInfoInputs.size()>0){
                Id contactId = ContactInfoInputs[0].conRec.Id;
                String msgSessionId = ContactInfoInputs[0].routableId;

                if(contactId != NULL && !String.isBlank(msgSessionId)){
                    EinsteinBotEventHandler.updateMsgUser(contactId, msgSessionId);
                }
            }
        } catch(Exception ex){}
    }

    public class ContactInfoInput {
		@InvocableVariable
		public Contact conRec;
        @InvocableVariable
		public String routableId;
	}
}