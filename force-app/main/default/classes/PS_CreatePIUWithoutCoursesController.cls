/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_CreatePIUWithoutCoursesController
   Description:     Allows user to link contact with product
   Date             Version           Author               Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   14 oct 2015      1.0           Accenture NDC           Created
   11 May 2016      1.1           Abhinav                 Modified class for R5 to default the Asset status based on Order status and metadata table values.
   16 Oct 2018      1.2           Navaneeth               Modified the metadata Query Logic as part of CR-02105-Req-6 \ CRMSFDC- 4836 - [LEXPRO OCT 2018 Release] 
   09 Sep 2019      1.3           Anurutheran             Modified class for September CI CR-02850: Adding Channel detail picklist 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
 

public class PS_CreatePIUWithoutCoursesController
{
    public string opportunityID;
    public string accountID;
    public boolean selectedContact{get;set;}
    public List<contactDetails> lstWrappercontactDetails{get;set;}
    public List<OpportunityContactRole> lstWithOpptyContactRole;
    public Map<Id,String> mapWithContactRole;
    private integer counter=0;  //keeps track of the offset
    private integer list_size=20; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list
    public List<contactDetails> lstWithAllContactDetails;
    public opportunity opportunityChannel;
    public opportunity opptyOrder;
    public Id GlobalAssetRecordTypeId;
    public Date expiryDateDefault;
    public string orderStatus;
    public String orderId;
    public List<PS_Order_Creation_Mapping__mdt> OrderCreation = new List<PS_Order_Creation_Mapping__mdt>();
    
    public PS_CreatePIUWithoutCoursesController()
    {
         opportunityID = ApexPages.currentPage().getParameters().get('opportunityId');
         accountID = ApexPages.currentPage().getParameters().get('accountId');
         orderStatus = ApexPages.currentPage().getParameters().get('orderStatus');
         orderId = ApexPages.currentPage().getParameters().get('oId');
         //get global asset record type id
         GlobalAssetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Global Products In Use').getRecordTypeId();
         lstWithOpptyContactRole = new List<OpportunityContactRole>();
         lstWrappercontactDetails = new List<contactDetails>();
         lstWithAllContactDetails = new List<contactDetails>();
         mapWithContactRole = new Map<Id,String>();
         opportunityChannel = new opportunity();
         if(opportunityID != null)
         {
             total_size = [select count() from OpportunityContactRole where OpportunityId =: opportunityID];
             opportunityChannel = [select Id,channel__c,Channel_Detail__c,Selling_Period__c,Selling_Period_Year__c from opportunity where id =: opportunityID];
             opptyOrder = [select Business_unit__c,Channel__c,Channel_Detail__c,Line_of_Business__c,Market__c from opportunity where id =: opportunityID];
            if(test.isRunningTest())
            {
                OrderCreation = [SELECT Id,Label,NamespacePrefix,Can_Create_Order__c,Oppty_BU__c,Oppty_Channel__c,Oppty_LOB__c,Oppty_Market__c  
                                                            FROM PS_Order_Creation_Mapping__mdt];
            }
            else
            {
                // Commented and Modified by Navaneeth for CR-02105-Req-6 \ CRMSFDC- 4836  - Start
                // OrderCreation = [SELECT Id,Label,NamespacePrefix,Can_Create_Order__c,Oppty_BU__c,Oppty_Channel__c,Oppty_LOB__c,Oppty_Market__c FROM PS_Order_Creation_Mapping__mdt WHERE Oppty_BU__c =: opptyOrder.Business_unit__c AND Oppty_Market__c =: opptyOrder.Market__c AND Oppty_Channel__c =: opptyOrder.Channel__c AND Oppty_LOB__c =: opptyOrder.Line_of_Business__c Limit 1];
                OrderCreation = [SELECT Id,Label,NamespacePrefix,Can_Create_Order__c,Oppty_BU__c,Oppty_Channel__c,Oppty_LOB__c,Oppty_Market__c FROM PS_Order_Creation_Mapping__mdt WHERE Oppty_BU__c = 'All' AND Oppty_Market__c =: opptyOrder.Market__c AND Oppty_Channel__c = 'All' AND Oppty_LOB__c ='All' Limit 1];
                if(OrderCreation.size()==0)
                    OrderCreation = [SELECT Id,Label,NamespacePrefix,Can_Create_Order__c,Oppty_BU__c,Oppty_Channel__c,Oppty_LOB__c,Oppty_Market__c FROM PS_Order_Creation_Mapping__mdt WHERE Oppty_BU__c =: opptyOrder.Business_unit__c AND Oppty_Market__c =: opptyOrder.Market__c AND Oppty_Channel__c =: opptyOrder.Channel__c AND Oppty_LOB__c =: opptyOrder.Line_of_Business__c Limit 1];
                // Commented and Modified by Navaneeth for CR-02105-Req-6 \ CRMSFDC- 4836  - End     
            }
         }
    }
    
    public List<contactDetails> getOpptyContactRole()
    {
        lstWrappercontactDetails.clear();
        try
        {
        for(OpportunityContactRole opptyCon : [select Id,ContactId,Contact.Name,Role from OpportunityContactRole where OpportunityId =: opportunityID ORDER BY Contact.Name LIMIT : list_size OFFSET: counter])
        {
            lstWrappercontactDetails.add(new contactDetails(opptyCon,false));
            mapWithContactRole.put(opptyCon.ContactId,opptyCon.Role);
        }
        }catch(Exception e)
        {
            ApexPages.addMessages(e);  
        }
        return lstWrappercontactDetails;
    }
    
    public PageReference Beginning() 
    { 
        //user clicked beginning
        createAndModifyTempList();
        counter = 0;
        return null;
    }

    public PageReference Previous() 
    { 
        //user clicked previous button
        createAndModifyTempList();
        counter -= list_size;
        return null;
    }

    public PageReference Next() 
    { 
        //user clicked next button
        createAndModifyTempList();
        counter += list_size;
        return null;
    }

    public PageReference End() 
    { 
        //user clicked end
        createAndModifyTempList();
        counter = total_size - math.mod(total_size, list_size);
        return null;
    }

    public Boolean getDisablePrevious() 
    { 
        //this will disable the previous and beginning buttons
        if (counter>0) 
        {
            return false;
        }else 
        {
            return true;
        }
    }

    public Boolean getDisableNext() 
    { 
        //this will disable the next and end buttons
        if (counter + list_size < total_size) 
        {
            return false; 
        }else 
        {
            return true;
        }
    }

    public Integer getTotal_size() 
    {
        return total_size;
    }

    public Integer getPageNumber() 
    {
        return counter/list_size + 1;
    }

    public Integer getTotalPages() 
    {
        if(math.mod(total_size, list_size) > 0) 
        {
            return total_size/list_size + 1;
        }else 
        {
            return (total_size/list_size);
        }
    }
    
    public pageReference cancel()
    {
        pageReference returnToOppty =new pageReference('/'+opportunityID);
        return returnToOppty ;
    }
    
    public void createAndModifyTempList()
    {
        List<Integer> lstWithIndex = new List<Integer>();
        Integer index = 0;
        boolean recFound = false;
        try{
        for(contactDetails tempCollection : lstWrappercontactDetails)
        {
            if(tempCollection.isSelected == True)
            {
                if(!lstWithAllContactDetails.isEmpty())
                {
                    for(contactDetails temp : lstWithAllContactDetails)
                    {
                        if(temp.opptyContact.contactId == tempCollection.opptyContact.contactId)
                        {
                            recFound = true;
                            break;
                        }
                    } 
                    if(!recFound)
                    {
                        lstWithAllContactDetails.add(tempCollection); 
                    }
                }else
                {
                    lstWithAllContactDetails.add(tempCollection);
                }      
            }else if(tempCollection.isSelected == false)
                {            
                for(contactDetails temp : lstWithAllContactDetails)
                {
                    if(temp.opptyContact.contactId == tempCollection.opptyContact.contactId)
                    {
                        lstWithIndex.add(index);  
                        index = index +1;          
                    }
                }
            } 
             
        }
        lstWithIndex.sort();
        for(Integer i=lstWithIndex.size()-1;i>=0;i--)
        {     
            if(lstWithIndex[i]<lstWithAllContactDetails.size())
            {
                lstWithAllContactDetails.remove(lstWithIndex[i]);
            }
        }
        }catch(Exception e)
        {
            ApexPages.addMessages(e);  
        }
    }
    
  public pageReference createPIUForSelectedContacts()
    {
        try
        {
        List<OrderItem> orderItems = new List<OrderItem>();
        Map<String, Id> assetStringToOrderItemIdMap = new Map<String, Id>();
        pageReference newPageRef = new pageReference('/apex/PS_UpdatePIUWithoutCourses?opportunityid='+opportunityID+'&accountId='+accountID);
        createAndModifyTempList();
        string assetStatus;
        
            
         if(OrderCreation != null && !OrderCreation.isEmpty())
        {
            if(OrderCreation[0].Can_Create_Order__c == 1.0)
            {
                if(orderStatus=='Filled')
                {
                    assetStatus = 'Active';
                }
                 else if(orderStatus=='Open')
                     {
                         assetStatus = 'Pending';
                     }
            }
            else
            {
                assetStatus = 'Active';
            }
        }
        List<Database.SaveResult> srList = new List<Database.SaveResult>();
        List<Contact_Product_In_Use__c> lstWithContactProductInUse = new List<Contact_Product_In_Use__c>();
        Contact_Product_In_Use__c contactAsset;
        Asset newAsset;
        List<Asset> lstWithAsset = new List<Asset>();
        List<OpportunityLineItem> lstWithOpportunityLineItem = new List<OpportunityLineItem>(); 
        lstWithOpportunityLineItem = [select Id,Product2Id,Product2.Name, Configured__c, Configured_Product__c, Configured_Product__r.Name, Quantity, Pricing_UOm__c, Selling_Term__c from OpportunityLineItem where OpportunityId =: opportunityID limit 1000];
        
              
       if(OrderCreation[0].Oppty_Market__c != 'US')
       {
         if(opportunityChannel.Selling_Period__c != null)
            {
                if(opportunityChannel.Selling_Period__c == 'Fall')
                {
                    if(opportunityChannel.Selling_Period_Year__c != null)
                    {
                        Date expiryDate = lastDateOfMonth(02);
                        expiryDateDefault =  expiryDate;  
                    }      
                }else
                    if(opportunityChannel.Selling_Period__c == 'Spring')
                    {
                        if(opportunityChannel.Selling_Period_Year__c != null)
                    {
                        Date expiryDate = lastDateOfMonth(08);
                        expiryDateDefault =  expiryDate;
                    }
                }else
                    if(opportunityChannel.Selling_Period__c == 'Semester 1')
                    {
                        if(opportunityChannel.Selling_Period_Year__c != null)
                        {
                            Date expiryDate = lastDateOfMonth(06);
                            expiryDateDefault =  expiryDate;
                    }
                }else
                    if(opportunityChannel.Selling_Period__c == 'Semester 2')
                    {
                        if(opportunityChannel.Selling_Period_Year__c != null)
                        {
                            Date expiryDate = lastDateOfMonth(12);
                            expiryDateDefault =  expiryDate;
                        }
                    }else
                    {
                //Date myDate;
                    if(opportunityChannel.Selling_Period_Year__c != null)
                    {
                        expiryDateDefault = Date.newInstance(Integer.valueof(opportunityChannel.Selling_Period_Year__c),12,31);
                    }    
                    }
        }
        else
            {
                if(opportunityChannel.Selling_Period_Year__c != null)
                {
                    expiryDateDefault = Date.newInstance(Integer.valueof(opportunityChannel.Selling_Period_Year__c),12,31);
                }
            }
        }

        if(orderId != null || orderId != ''){
            for(orderItem orderLineItem: getOrderItems()){
                assetStringToOrderItemIdMap.put(orderLineItem.PricebookEntry.Product2.Name+orderLineItem.PricebookEntry.Product2Id+accountId+orderLineItem.Quantity+orderId+assetStatus, orderLineItem.Id);   
            }
        }
        
        if(lstWithOpportunityLineItem.size()>0)
        {
            for(OpportunityLineItem opptyItem : lstWithOpportunityLineItem)
            { 
                newAsset = new Asset();
                newAsset.Name = opptyItem.Configured__c ? opptyItem.Configured_Product__r.Name : opptyItem.Product2.Name;
                newAsset.AccountId= accountID;
                newAsset.status__c = assetStatus;
                newAsset.Product2Id =  opptyItem.Configured__c ? opptyItem.Configured_Product__c : opptyItem.Product2Id;
                //Abhinav : Populating the Pricing & Selling field for R5
                newAsset.Pricing_UOM__c = opptyItem.Pricing_UOM__c;
                newAsset.Selling_Term__c = opptyItem.Selling_Term__c;
                newAsset.InstallDate = System.today();
                if(OrderCreation[0].Oppty_Market__c == 'US')                
                {
                    if(newAsset.Pricing_UOM__c == 'Month')
                    {
                        expiryDateDefault = newAsset.InstallDate.addMonths(Integer.valueOf(newAsset.Selling_Term__c));
                    }
                    else
                    if(newAsset.Pricing_UOM__c == 'Year')
                    {
                       expiryDateDefault = newAsset.InstallDate.addYears(Integer.valueOf(newAsset.Selling_Term__c));   
                    }
                }
                newAsset.Expiry_Date__c = expiryDateDefault;
                
                newAsset.Channel__c = opportunityChannel.channel__c;
                newAsset.Channel_detail__c = opportunityChannel.Channel_detail__c;//CR-02850
                newAsset.Quantity = opptyItem.Quantity;
                if(opportunityID!=null){
                    newAsset.Opportunity__c = opportunityID;
                }
                if(GlobalAssetRecordTypeId != null)
                {
                    newAsset.RecordTypeId = GlobalAssetRecordTypeId; 
                }
                
                //add order Id if it's not null
                if(orderId != null || orderId != ''){
                    newAsset.Order__c = orderId;
                    String keyString = newAsset.Name+newAsset.Product2Id+newAsset.AccountId+newAsset.Quantity+orderId+newAsset.Status__c;
                    newAsset.Order_Product_Id__c = assetStringToOrderItemIdMap.get(keyString);
                }
                
                lstWithAsset.add(newAsset);
            }
        }
        if(!lstWithAsset.isEmpty() && lstWithAsset.size()>0)
        {
            srList = database.insert(lstWithAsset,false);
            
            for (Database.SaveResult sr : srList) 
            {   
                if (!sr.isSuccess()) 
                {
                    for(Database.Error err : sr.getErrors()) 
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'The following error has occurred.'+err.getStatusCode() + ': ' + err.getMessage()+'Fields that affected this error: ' + err.getFields());
                        ApexPages.addMessage(myMsg);                 
                    }
                }
            }
        }
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) 
            {
                if (sr.isSuccess()||Test.isRunningTest()) 
                {
                    
                    for(contactDetails contactDetail : lstWithAllContactDetails)
                    {
                        contactAsset = new Contact_Product_In_Use__c();
                        contactAsset.Product_in_Use__c = sr.getId();
                        contactAsset.Contact__c = contactDetail.opptyContact.ContactId;
                        contactAsset.Status__c = assetStatus;
                        contactAsset.Role__c = mapWithContactRole.get(contactDetail.opptyContact.ContactId);
                        lstWithContactProductInUse.add(contactAsset);
                    }
                }
                else 
                {
                    // Operation failed, so get all errors 
                    for(Database.Error err : sr.getErrors()) 
                    {
                        if(err.getMessage().contains('Value does not exist or does not match filter criteria'))
                        {
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'You cannot create PIU for product(s) that does not belong to your market.');
                            ApexPages.addMessage(myMsg);
                            return null;
                        }else
                        {
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'The following error has occurred.'+err.getStatusCode() + ': ' + err.getMessage()+'Fields that affected this error: ' + err.getFields());
                            ApexPages.addMessage(myMsg); 
                            return null; 
                        }               
                    }
                }
            
        }
         boolean bSucces=false;
        if(lstWithContactProductInUse != null && !lstWithContactProductInUse.isEmpty())
        {
            srList = database.insert(lstWithContactProductInUse,false);
           
            for (database.SaveResult srItm:srList) {
                if(srItm.isSuccess()) {
                    bSucces=true;
                }
            
            }
            if(bSucces==true) {
                PS_CreatePIUWithoutCoursesController.updOrderrecord(orderID);
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Request Successfully Processed');
                ApexPages.addMessage(myMsg);
             }
        }
        PS_OpportunitRevenueControllerNew.updOpptyCloseWon(opptyOrder);
        return newPageRef;
        }catch(Exception e)
        {
             if(e.getMessage().contains('Value does not exist or does not match filter criteria.: [Product2Id]'))
             {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'You cannot create PIU for product(s) that does not belong to your market.');
                 ApexPages.addMessage(myMsg);
                 return null;
             }else
             {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
                 ApexPages.addMessage(myMsg);
                 return null;
             }  
        }
        return null;
    }
    
                 
     private Date lastDateOfMonth(Integer Month)
    {
        Date lastDayOfMonth;
        Integer sellingPeriodYearInt;
        if(opportunityChannel.Selling_Period_Year__c != null && opportunityChannel.Selling_Period_Year__c != '')
        {
            if(opportunityChannel.Selling_Period__c != null)
            {
                if(opportunityChannel.Selling_Period__c == 'Fall')
                {
                    sellingPeriodYearInt = Integer.ValueOf(opportunityChannel.Selling_Period_Year__c)+1;
                }else
                {
                    sellingPeriodYearInt = Integer.ValueOf(opportunityChannel.Selling_Period_Year__c);
                } 
            }       
            Integer numberOfDays = Date.daysInMonth(sellingPeriodYearInt,Month);
            lastDayOfMonth = Date.newInstance(sellingPeriodYearInt, Month, numberOfDays); 
        }
        return lastDayOfMonth;
    }
    
    private List<OrderItem> getOrderItems(){
        return new List<OrderItem>([select id,Quantity,PricebookEntryId,PricebookEntry.Product2Id,PricebookEntry.Product2.Name from OrderItem where orderid =: orderId]);
    }
    
    
    public static void updOrderrecord(string OrdRecID) {
    
        if(OrdRecID !=null) {
            Order orderRec=[Select Recordtype.Name,Status,EffectiveDate,Business_Unit__c, Market__c,Purchase_Order_Number__c,Sales_Channel__c from order where id =:OrdRecID];
            if(orderRec.Recordtype.Name=='Revenue Order') {
                if(orderRec.Market__c == 'UK' && (orderRec.Business_Unit__c == 'Higher Ed' || orderRec.Business_Unit__c == 'ELT'))
                {
                    orderRec.Status = 'Filled';
                }else
                {
                   orderRec.Status = 'Open';    
                } 
                orderRec.Order_Submitted__c = True;
               // orderMarket = orderRec.Market__c;
               // orderBU = orderRec.Business_Unit__c;
               // orderPurchaseOrderNum  = orderUpd.Purchase_Order_Number__c; 
               // orderChannel = orderUpd.Sales_Channel__c;
               // orderStatus = orderUpd.Status;
               // orderStartDate = orderRec.EffectiveDate;
            }
            
           else if(orderRec.Recordtype.Name=='Sample Order') {
                orderRec.Status = 'Open'; 
                orderRec.Order_Submitted__c = True;
                //orderMarket = orderUpd.Market__c;
                //orderBU = orderUpd.Business_Unit__c;
                //orderPurchaseOrderNum  = orderUpd.Purchase_Order_Number__c; 
                //orderChannel = orderUpd.Sales_Channel__c;
                //orderStatus = orderUpd.Status;
                //orderStartDate = orderUpd.EffectiveDate;
           
           
           }
           
           update orderRec;   
        }
    
    }
    
    public class contactDetails
    {
        public opportunitycontactrole opptyContact{get;set;}
        public boolean isSelected{get;set;}
        public contactDetails(opportunitycontactrole opptyConRole,boolean selected)
        {
            opptyContact = opptyConRole;
            isSelected =   selected;  
        }
    }
}