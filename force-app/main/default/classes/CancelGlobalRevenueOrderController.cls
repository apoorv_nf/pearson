/* --------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            CancelGlobalRevenueOrderController
Description:     To cancel the revenue order
Date             Version           Author                                          Summary of Changes 
-----------      ----------   -----------------     ---------------------------------------------------------------------------------------
25 july 2016      1.0           Chaitra                                        Created :R6 Cancel Revenue Order controller class -SUS629
11 Dec  2018      1.1           Navaneeth                                      Modified for IR UK Project - CR # 02337 \ CRMSFDC-5840  
14 Oct  2019      1.2           Anurutheran                                    Modified for CR-02940[Replacing Apttus with Non-Apttus]
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class CancelGlobalRevenueOrderController {
    Public Boolean displayPopUp {get;set;}
    Public Boolean ProcessedOrder {get;set;}
    Public string errormsg {get;set;} 
    Public Order mOrdobj ;
    Public String mOrderId {get;set;} 
    Public String propId ;
    Public List<orderItem> orderItmList = new list<OrderItem>();
    
    //constructor code
    public CancelGlobalRevenueOrderController(ApexPages.StandardController controller) {
        mOrderId = Apexpages.currentpage().getparameters().get('orderId');
        //CR-02940-Start
        /*propId = Apexpages.currentpage().getparameters().get('propId');
        mOrdobj = [select id,Opportunity.Finalise_Opportunity__c,Quote_Proposal__r.Apttus_Proposal__Approval_Stage__c,status,QuoteId,
                  (select id,Status__c from orderItems) from order where id =:mOrderId ];*/
        //CR-02940-End
          mOrdobj = [select id,Opportunity.Finalise_Opportunity__c,status,QuoteId,
                  (select id,Status__c from orderItems) from order where id =:mOrderId ];
        orderItmList = mOrdobj.orderItems;
    }
    //Method to pop error message on order status values
    Public pagereference mConfirm(){
        if(mOrdobj.status == 'New') {
            displayPopUp = true;
            errormsg = label.CancelOrder;
        }
        else {
            ProcessedOrder = true;
            errormsg = label.Not_a_new_order ;
        }
        return null;
    }
    //Method to cancel order
    public pageReference mCancelOrder ()
    {
        List<OrderItem> OIList = new list<OrderItem>(); 
        /*
        Apttus_Proposal__Proposal__c Prop = [select id,Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c where id=:propId ];
        */
        mOrdobj.status = 'Cancelled';
        if(!orderItmList.isempty()){
            for(OrderItem OI : orderItmList )
            {
                OI.Status__c = 'Cancelled';
                OIList.add(OI);
            }
            mOrdobj.Opportunity.Finalise_Opportunity__c = false;
            //Prop.Apttus_Proposal__Approval_Stage__c = 'Generated';
        }
        try{
            Update mOrdobj;
            Update OIList;
            Update mOrdobj.Opportunity;
            //Update prop;
            pageReference pageToRedirect = new pageReference('/'+mOrdobj.Opportunity.id);
            return pageToRedirect;
        }
        catch(Exception e){
            throw e;
        }
    }
    //Method to close popup
    public pageReference closePopup()
    {
        displayPopUp = false;
        pageReference pageToRedirect = new pageReference('/'+morderId);
        return pageToRedirect;
    } 
    
}