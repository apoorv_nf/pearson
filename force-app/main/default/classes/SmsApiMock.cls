/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 11/07/2018
 */
public class SmsApiMock implements HttpCalloutMock {
	private Boolean tokenReset = false;

	public HTTPResponse respond(HTTPRequest req) {
		HttpResponse response = new HttpResponse();

		String endpoint = req.getEndpoint();

		if(endpoint.contains('getDetails?accesscode=valid')) {
			response.setStatusCode(200);
			response.setBody('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><AccessCodeServiceResponse><status>Found</status><errorCode>0</errorCode><AccessCodeDetails><Accesscode>TTIERN-CLXII-OSMIC-VARNA-LENTO-BODES</Accesscode><AccesscodeTypeId>21153</AccesscodeTypeId><AccesscodeBatchId>43349</AccesscodeBatchId><CashedIn>N</CashedIn><CashedInDate>2018-05-22-04:00</CashedInDate><ValidFromDate>2018-03-22T00:00:00-04:00</ValidFromDate><ExpirationDate>2019-08-04T00:00:00-04:00</ExpirationDate><PoReqd>N</PoReqd><NumCashins>100</NumCashins><NumTimesCashedIn>2</NumTimesCashedIn><CreationDate>2018-03-22T00:00:00-04:00</CreationDate></AccessCodeDetails></AccessCodeServiceResponse>');
		} else if(endpoint.contains('getDetails?accesscode=invalid')) {
			response.setStatusCode(200);					
			response.setBody('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><AccessCodeServiceResponse><status>Not found</status><errorCode>4</errorCode></AccessCodeServiceResponse>');
		} else if(endpoint.contains('getDetails?accesscode=token_expired')) {
			if(this.tokenReset) {
				response.setStatusCode(200);
				response.setBody('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><AccessCodeServiceResponse><status>Found</status><errorCode>0</errorCode><AccessCodeDetails><Accesscode>TTIERN-CLXII-OSMIC-VARNA-LENTO-BODES</Accesscode><AccesscodeTypeId>21153</AccesscodeTypeId><AccesscodeBatchId>43349</AccesscodeBatchId><CashedIn>N</CashedIn><CashedInDate>2018-05-22-04:00</CashedInDate><ValidFromDate>2018-03-22T00:00:00-04:00</ValidFromDate><ExpirationDate>2019-08-04T00:00:00-04:00</ExpirationDate><PoReqd>N</PoReqd><NumCashins>100</NumCashins><NumTimesCashedIn>2</NumTimesCashedIn><CreationDate>2018-03-22T00:00:00-04:00</CreationDate></AccessCodeDetails></AccessCodeServiceResponse>');
			} else {
				this.tokenReset = true;
				response.setStatusCode(401);
				response.setBody('{"valid" : "false","reason" : "Required SSO Session Token Not Found"}');
			}
		} else if(endpoint.contains('auth/json/pearson/authenticate')) {
			response.setStatusCode(200);
			response.setBody('{"tokenId":"token1234","successUrl":"/auth/console","realm":"/pearson"}');
		} else if(endpoint.contains('accessCodes/product_token/products')) {
			response.setStatusCode(200);
			response.setBody('{"productIds":["8161","31237","31529"]}');
		} else if(endpoint.contains('courses/courseId?hdp=false')) {
			response.setStatusCode(200);
			response.setBody('{"enrollment":{"enrollable":true,"enrollBeginDate":"Nov 25, 2014","enrollEndDate":"Nov 25, 2018","errors":[]},"title":"Tiernan CERT SMS QA Course 101","kioskCoursePartner":null,"products":[{"currency":null,"id":8161,"description":"Course Compass","price":0.0,"type":"ACCESS_CODE","abbrev":"ST44"},{"currency":"USD","id":32743,"description":"MasteringNewDeisgn - Generic Student OLP with eText","price":115.95,"type":"ONLINE_PURCHASE","abbrev":"STOP"},{"currency":"USD","id":32744,"description":"MasteringNewDeisgn - Generic Student OLP without eText","price":68.95,"type":"ONLINE_PURCHASE","abbrev":"STOP"},{"currency":null,"id":34008,"description":"Generic Temp Access for Bb Integrations--does not allow linking to integrated apps","price":0.0,"type":"TEMPORARY_ACCESS","abbrev":"PLTA"}],"id":607302,"description":"Tiernan CERT SMS QA Course 101","endDate":"Nov 25, 2018","instructor":{"title":"Mr.","firstName":"Tim","lastName":"Tiernan","institution":{"name":"UPWARD MOBILITY LRNG CTR","otherStateProv":"MD","countryId":85,"stateId":21,"city":"KENSINGTON","institutionId":20311,"institutionOther":null,"zipCode":"20895","socratesId":11359,"countryCode":"US"},"email":"tim.tiernan@pearson.com"},"standaloneCourse":false,"extCourseId":"tiernan20009"}');
		} else if(endpoint.contains('courses/token_expired?hdp=false')) {
			if(this.tokenReset) {
				response.setStatusCode(200);
				response.setBody('{"enrollment":{"enrollable":true,"enrollBeginDate":"Nov 25, 2014","enrollEndDate":"Nov 25, 2018","errors":[]},"title":"Tiernan CERT SMS QA Course 101","kioskCoursePartner":null,"products":[{"currency":null,"id":8161,"description":"Course Compass","price":0.0,"type":"ACCESS_CODE","abbrev":"ST44"},{"currency":"USD","id":32743,"description":"MasteringNewDeisgn - Generic Student OLP with eText","price":115.95,"type":"ONLINE_PURCHASE","abbrev":"STOP"},{"currency":"USD","id":32744,"description":"MasteringNewDeisgn - Generic Student OLP without eText","price":68.95,"type":"ONLINE_PURCHASE","abbrev":"STOP"},{"currency":null,"id":34008,"description":"Generic Temp Access for Bb Integrations--does not allow linking to integrated apps","price":0.0,"type":"TEMPORARY_ACCESS","abbrev":"PLTA"}],"id":607302,"description":"Tiernan CERT SMS QA Course 101","endDate":"Nov 25, 2018","instructor":{"title":"Mr.","firstName":"Tim","lastName":"Tiernan","institution":{"name":"UPWARD MOBILITY LRNG CTR","otherStateProv":"MD","countryId":85,"stateId":21,"city":"KENSINGTON","institutionId":20311,"institutionOther":null,"zipCode":"20895","socratesId":11359,"countryCode":"US"},"email":"tim.tiernan@pearson.com"},"standaloneCourse":false,"extCourseId":"tiernan20009"}');
			} else {
				this.tokenReset = true;
				response.setStatusCode(401);
				response.setBody('{"valid" : "false","reason" : "Required SSO Session Token Not Found"}');
			}

		}

		return response;
	}
}