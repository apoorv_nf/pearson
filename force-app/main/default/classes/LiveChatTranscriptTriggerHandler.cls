/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 29/05/2018
 */
public with sharing class LiveChatTranscriptTriggerHandler {

	private List<Account> accounts = new List<Account>();
	private List<Contact> contacts = new List<Contact>();
	private List<Case> cases = new List<Case>();

	private User automatedProccessUser {
		get {
			if(automatedProccessUser != null) return automatedProccessUser;

			automatedProccessUser = [SELECT Id FROM User WHERE Name = 'Automated Process'];

			return automatedProccessUser;
		} set;
	}

	public void onBeforeUpdate(List<LiveChatTranscript> newTranscripts, List<LiveChatTranscript> oldTranscripts, Map<Id,LiveChatTranscript> newTranscriptsMap, Map<Id,LiveChatTranscript> oldTranscriptsMap) {
		this.swapOutDuplicateCaseAndRemoveThem(newTranscripts, oldTranscriptsMap);
	}

	public void onAfterUpdate(List<LiveChatTranscript> newTranscripts, List<LiveChatTranscript> oldTranscripts, Map<Id,LiveChatTranscript> newTranscriptsMap, Map<Id,LiveChatTranscript> oldTranscriptsMap) {
		this.deleteRelatedRecords(newTranscripts);
	}

	public void swapOutDuplicateCaseAndRemoveThem(List<LiveChatTranscript> transcripts, Map<Id,LiveChatTranscript> oldTranscriptsMap) {
		if(UserInfo.getUserId() != automatedProccessUser.Id) return;

		List<Id> deletedCaseIds = new List<Id>();

		for(LiveChatTranscript transcript : transcripts) {
			if(oldTranscriptsMap.get(transcript.Id).CaseId == null) continue;
			if(transcript.CaseId == null) continue;
			if(transcript.CaseId == oldTranscriptsMap.get(transcript.Id).CaseId) continue;

			deletedCaseIds.add(transcript.CaseId);

			transcript.CaseId = oldTranscriptsMap.get(transcript.Id).CaseId;
		}

		if(deletedCaseIds.isEmpty()) return;

		deleteCases(deletedCaseIds);
	}

	@future
	private static void deleteCases(List<Id> caseIds) {
		delete [SELECT Id FROM Case WHERE Id IN :caseIds];
	}

	public void deleteRelatedRecords(List<LiveChatTranscript> transcripts) {
		List<Id> transcriptIds = new List<Id>();

		for(LiveChatTranscript transcript : transcripts) {
			if(transcript.Status != 'Completed' && transcript.Status != 'Missed') continue;
			transcriptIds.add(transcript.Id);
		}

		if(transcriptIds.isEmpty()) return;

		Id automatedProccessUserId = this.automatedProccessUser.Id;

		for(LiveChatTranscript transcript : [
			SELECT Id, CaseId, ContactId, Contact.AccountId, Contact.OwnerId, Contact.Account.OwnerId, Contact.Account.RecordTypeId
			FROM LiveChatTranscript
			WHERE Case.OwnerId = :automatedProccessUserId
			AND Status IN ('Completed', 'Missed')
			AND Case.Origin != 'Chatbot'
			AND Id IN :transcriptIds]) {

			this.addContactAndAccounts(transcript);
			this.addCases(transcript);
		}

		this.deleteRecords(this.cases);
		this.deleteRecords(this.contacts);
		this.deleteRecords(this.accounts);
	}

	private void addContactAndAccounts(LiveChatTranscript transcript) {
		if(transcript.ContactId == null) return;
		if(transcript.Contact.AccountId == null) return;

		if(transcript.Contact.Account.RecordTypeId == Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId()) {
			this.contacts.add(new Contact(Id = transcript.ContactId));
			this.accounts.add(new Account(Id = transcript.Contact.AccountId));
		}
	}

	private void addCases(LiveChatTranscript transcript) {
		if(transcript.CaseId == null) return;
		this.cases.add(new Case(Id = transcript.CaseId));
	}

	private void deleteRecords(List<SObject> records) {
		if(records.isEmpty()) return;

		try {
			Database.delete(records);
			if(!Test.isRunningTest()) {
				Database.emptyRecycleBin(records);
			}
		} catch(Exception ex) {
			System.debug(ex.getMessage());
		}
	}
}