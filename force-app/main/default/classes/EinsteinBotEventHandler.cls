/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 02/07/2018
 * Function    : the trigger handler for the Einstein Bot Platform event. All the DML operations are carried out from a platform event.
 * Update: Added Error_Message__c - Shantanu (Neuraflash) - 27/12/2018
 * Update: Added createSMSContact(), udateMsgUser() & insertCase() specifically for Messaging 
 *         Updated logChat() & logResolution() to handle requests from both the channels (Web & SMS) - Apoorv (Neuraflash) - 28/07/2019
 *         Updated logChat() to populate Messaging Channel name - Apoorv (Neuraflash) - 07/10/2019
 *         Updated insertCase() to bypass trigger to avoid Read time out issues as a workaround 
 *         until the trigger logic is optimized - Apoorv (Neuraflash) - 11/12/2019
 */

public without sharing class EinsteinBotEventHandler { 

    //Event types
    public final static String CREATE_CONTACT = 'CREATE_CONTACT';
    public final static String UPDATE_CASE = 'UPDATE_CASE';
    public final static String UPDATE_CONTACT = 'UPDATE_CONTACT';
    public final static String LOG_CHAT = 'LOG_CHAT';
    public final static String RESET_ACCESS_TOKEN = 'RESET_ACCESS_TOKEN';
    public final static String LOG_CHAT_RESOLUTION = 'SUCCESSFUL_RESOLUTION';
    public final static String CREATE_SMS_CONTACT = 'CREATE_SMS_CONTACT';
    public final static String INSERT_CASE = 'INSERT_CASE';

    //Static values
    public final static String STUDENT = 'Student';
    public final static String EDUCATOR = 'Educator';
    public final static String ROLE_DETAIL = 'Asst. Professor';

    public void handleEvents(List<Einstein_Bot_Event__e> events) {
        for (Einstein_Bot_Event__e event : events) {
            switch on event.Type__c {
                when 'CREATE_CONTACT' {
                    this.createContact(event);
                }
                when 'UPDATE_CASE' {
                    this.updateCase(event);
                }
                when 'UPDATE_CONTACT' {
                    this.updateContact(event);
                }
                when 'LOG_CHAT' {
                    this.logChat(event);
                }
                when 'SUCCESSFUL_RESOLUTION' {
                    this.logResolution(event);
                }
                when 'RESET_ACCESS_TOKEN' {
                    this.resetAccessToken(event);
                }
                when 'CREATE_SMS_CONTACT'{
                    this.createSMSContact(event);
                }
                when 'INSERT_CASE'{ 
                    this.insertCase(event);
                }
            }
        }
    }

    public void createSMSContact(Einstein_Bot_Event__e event) {
        try{
            String firstName = event.First_Name__c.replaceAll('[^A-Za-z\\s]', '').left(40);
            String lastName = event.Last_Name__c.replaceAll('[^A-Za-z\\s]', '').left(40);
            String msgSessionId = event.Live_Agent_Session_Id__c;
            
            List<Account> accounts = [SELECT Id FROM Account WHERE Name = :event.Institution__c LIMIT 1];

            Account account = new Account();

            if(accounts.isEmpty()) {
                account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_Creation_Request').getRecordTypeId();
                account.Name = firstName + ' ' + lastName;
                account.Market__c = System.Label.SMSBot_Contact_Market;
                insert account;
            } else {
                account.Id = accounts[0].Id;
            }

            Contact contact = new Contact();
            contact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Consumer').getRecordTypeId();
            contact.AccountId = account.Id;   
            contact.LastName = lastName;
            contact.FirstName = firstName;
            contact.Email = event.Email__c;
            contact.Chat_Session_Key__c = event.Live_Agent_Session_Id__c;
            contact.Role__c = STUDENT;
            contact.Market__c = System.Label.SMSBot_Contact_Market;
            insert contact;

            if(contact.Id != NULL && !String.isBlank(msgSessionId)){
                EinsteinBotEventHandler.updateMsgUser(contact.Id, msgSessionId);
            }
        } catch(Exception ex){
        }
    }

    public static void updateMsgUser(Id contactId, String msgSessionId){    
        try{
            String msgUserId;
            List<MessagingEndUser> msgUserList = new List<MessagingEndUser>();

            MessagingSession msgSession = [SELECT Id,MessagingEndUserId, CaseId 
                                                FROM MessagingSession 
                                                    WHERE Id =: msgSessionId];
            Id accId;
            for(Contact con :[SELECT AccountId FROM Contact WHERE Id =: contactId]){
                accId = con.AccountId;
            }

            if(msgSession != NULL && msgSession.MessagingEndUserId != NULL){
                for(MessagingEndUser msgUser: [SELECT Id,ContactId, AccountId  
                                                FROM MessagingEndUser 
                                                    WHERE Id =: msgSession.MessagingEndUserId]){
                    msgUser.ContactId = contactId;
                    msgUser.AccountId = accId;
                    msgUserList.add(msgUser);
                }

                if(!msgUserList.isEmpty()){
                    update msgUserList;
                }
            }
        } catch(Exception ex){
        }
    }

    public void insertCase(Einstein_Bot_Event__e event) {
        try{
            Case newCase = new Case();
            MessagingSession msgSession = [SELECT Id, MessagingEndUser.ContactId, MessagingEndUser.MessagingPlatformKey, MessagingEndUser.MessagingChannel.DeveloperName,   
                                            CaseId, MessagingEndUser.AccountId, MessagingEndUser.Contact.Email, MessagingEndUser.MessagingChannel.TargetQueue.DeveloperName  
                                                  FROM MessagingSession 
                                                    WHERE Id =: event.Live_Agent_Session_Id__c LIMIT 1];

            newCase.RecordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Technical_Support').getRecordTypeId();
            newCase.Subject = 'SMS Chat Case';
            newCase.PS_Business_Vertical__c = 'Higher Education';
            newCase.Category__c = event.Category__c;
            newCase.Subcategory__c = event.Subcategory__c;
            newCase.Access_Code__c = event.Access_Code__c;
            newCase.Chat_Session_Key__c = event.Live_Agent_Session_Id__c;
            newCase.ContactId = msgSession.MessagingEndUser.ContactId;
            newCase.AccountId = msgSession.MessagingEndUser.AccountId;
            newCase.Course_Id__c = event.Course_Id__c;
            newCase.Contact_Email__c = msgSession.MessagingEndUser.Contact.Email;
            newCase.Contact_Organization_School__c = event.Institution__c;
            newCase.Customer_Username__c = event.Username__c;
            newCase.Origin = event.Origin__c;
            newCase.SuppliedPhone = msgSession.MessagingEndUser.MessagingPlatformKey;
            newCase.Instructor_Name__c=event.Instructor_Name__c; //CR-03144
            if(String.isNotEmpty(event.Error_Message__c)){
                newCase.Error_Message_Code__c = event.Error_Message__c;
            }
            newCase.Contact_Type__c = 'College Student';
            if([
                SELECT Id
                    FROM UserServicePresence
                        WHERE ServicePresenceStatus.DeveloperName = 'Available_for_SMS'
                            AND OwnerId IN (
                                            SELECT UserOrGroupId
                                                FROM GroupMember
                                                    WHERE Group.DeveloperName =: System.Label.PTS_SMS_Bot_Queue
                                            )
                            AND IsCurrentState = true
                            AND CreatedDate = LAST_N_DAYS:2 
                            LIMIT 1
            ].isEmpty()) {
                newCase.OwnerId = [SELECT Id FROM Group WHERE DeveloperName = 'SMSBot_Case_Queue'].Id;
                newCase.Status = 'New';
            }
            // Bypassing trigger to avoid Read time out issues as a temporary workaround until the trigger logic is optimized
            PS_SMSBot_CaseTriggerHandler.bypassTrigger = true;
            insert newCase;

            if(newCase.Id != NULL) {
                
                msgSession.CaseId = newCase.Id;
                update msgSession;

                String message = this.createFeedMessage(newCase, event, msgSession);
                if(!String.isEmpty(message)){ FeedItem feedItem = new FeedItem(Body=message, ParentId = newCase.Id, Type='TextPost');
                    insert feedItem;
                }
            }
        } catch(Exception ex){
        }
    }

    public void createContact(Einstein_Bot_Event__e event) {
        List<Account> accounts = [SELECT Id FROM Account WHERE Name = :event.Institution__c LIMIT 1];

        Account account = new Account();
        String firstName = event.First_Name__c.replaceAll('[^A-Za-z\\s]', '').left(40);
        String lastName = event.Last_Name__c.replaceAll('[^A-Za-z\\s]', '').left(40);

        if(accounts.isEmpty()) {
            account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
            account.Name = firstName + ' ' + lastName;

            insert account;
        } else {
            account.Id = accounts[0].Id;
        }

        Contact contact = new Contact();
        contact.AccountId = account.Id;   
        contact.LastName = lastName;
        contact.FirstName = firstName;
        contact.Email = event.Email__c;
        contact.Chat_Session_Key__c = event.Live_Agent_Session_Id__c;

        switch on event.Role__c {
            when 'Student' {
                contact.Role__c = STUDENT;
            }
            when 'Educator' {
                contact.Role__c = EDUCATOR;
                contact.Role_Detail__c = ROLE_DETAIL;
            }
        }

        insert contact;
    }

    public void updateCase(Einstein_Bot_Event__e event) { LiveChatTranscript transcript = [
            SELECT Id, ContactId, Contact.Email, CaseId, AccountId, Contact.AccountId, LiveChatButton.DeveloperName, Case.OwnerId, LiveChatButton.Queue.DeveloperName
            FROM LiveChatTranscript
            WHERE ChatKey = :event.Live_Agent_Session_Id__c
            AND CreatedDate = TODAY];


        if(transcript.ContactId == null) {
            List<Contact> contacts = [SELECT Id, AccountId FROM Contact WHERE Chat_Session_Key__c = :event.Live_Agent_Session_Id__c];
            transcript.ContactId = contacts.isEmpty() ? null : contacts[0].Id;
            transcript.AccountId = contacts.isEmpty() ? null : contacts[0].Id;
        } else if (transcript.AccountId == null) { transcript.AccountId = transcript.Contact.AccountId;
        }

        Case cas = new Case();
        cas.Id = transcript.CaseId; cas.Category__c = event.Category__c; 
           cas.Subcategory__c = event.Subcategory__c; cas.Access_Code__c = event.Access_Code__c;
          cas.Chat_Session_Key__c = event.Live_Agent_Session_Id__c;
        cas.ContactId = transcript.ContactId;  cas.AccountId = transcript.AccountId;
        cas.Course_Id__c = event.Course_Id__c; cas.Contact_Email__c = event.Email__c;
        cas.Instructor_Name__c = event.Instructor_Name__c;                                          
        cas.Contact_Organization_School__c = event.Institution__c; cas.Customer_Username__c = event.Username__c;

        // Start // Neuraflash Update for phase 2 chatbot optimization
        if(String.isNotEmpty(event.Error_Message__c)){ cas.Error_Message_Code__c = event.Error_Message__c;
        }
        cas.Contact_Type__c = 'College Student';

        //End //

        //Check if live agent is available
        if([
            SELECT Id
            FROM UserServicePresence
            WHERE ServicePresenceStatus.DeveloperName = 'Available_for_Chat'
            AND OwnerId IN (
                SELECT UserOrGroupId
                FROM GroupMember
                WHERE Group.DeveloperName = :transcript.LiveChatButton.Queue.DeveloperName)
            AND IsCurrentState = true
            AND CreatedDate = LAST_N_DAYS:2
                ].isEmpty()) {    cas.OwnerId = [SELECT Id FROM Group WHERE DeveloperName = 'Chatbot_Case_Queue'].Id; cas.Status = 'New';
        }

        String message = this.createFeedMessage(cas, event, transcript);
        System.debug('Updated case '+cas);
        update cas;

        if(!String.isEmpty(message)) { FeedItem feedItem = new FeedItem(Body=message, ParentId = cas.Id, Type='TextPost');  insert feedItem;
        }
    }

    private String createFeedMessage(Case cas, Einstein_Bot_Event__e event, sObject transcript) {
        if(String.isEmpty(event.Message__c)) return null;

        Schema.SObjectType sObjectType = transcript.getSObjectType();
        String qApiName;
        if(String.valueOf(sObjectType) == 'LiveChatTranscript'){
            qApiName = (String)transcript.getSobject('LiveChatButton').get('DeveloperName'); 
            System.debug(qApiName);
        } else if(String.valueOf(sObjectType) == 'MessagingSession'){
            qApiName = (String)transcript.getSobject('MessagingEndUser').getSobject('MessagingChannel').get('DeveloperName'); 
        }

        Einstein_Bot_Config__mdt config = [
            SELECT Id, Course_URL__c, Access_Code_URL__c
            FROM Einstein_Bot_Config__mdt
            WHERE QualifiedApiName = :qApiName];

        List<String> messageComponents = new List<String>();

        if(!String.isEmpty(event.Platform__c)) {   messageComponents.add(String.format(event.Message__c, new List<String>{event.Platform__c}));
        } else {
            messageComponents.add(event.Message__c);
        }

        if(!String.isEmpty(event.Access_Code__c)) { messageComponents.add(String.format(config.Access_Code_URL__c, new List<String>{event.Access_Code__c}));
        }

        if(!String.isEmpty(event.Course_Id__c)) {  SmsApi.CurrentGenCourse courseResponse = new SmsApiService().getCourse(event.Course_Id__c);
            messageComponents.add(String.format(config.Course_URL__c, new List<String>{courseResponse.id}));

            cas.Course_Id__c = courseResponse.id;
}
//CR-03144 Start
if(!String.isEmpty(event.Instructor_Name__c)) {  SmsApi.CurrentGenCourse courseResponse = new SmsApiService().getCourse(event.Instructor_Name__c);
messageComponents.add(String.format(config.Course_URL__c, new List<String>{courseResponse.id}));
cas.Instructor_Name__c = courseResponse.id;
                                                 
        } // CR-03144 End
        return String.join(messageComponents, ': ');
    }

    private void updateContact(Einstein_Bot_Event__e event) {
        Contact contact = new Contact();
        contact.Id = event.Contact_Id__c;
        contact.Chat_Session_Key__c = event.Live_Agent_Session_Id__c;
        update contact;
    }


    private void logResolution(Einstein_Bot_Event__e event) {
        
        String sessionId = event.Live_Agent_Session_Id__c;

        Einstein_Bot_Chat_Log__c chatLog = new Einstein_Bot_Chat_Log__c();
        chatLog.Live_Agent_Session_Id__c = event.Live_Agent_Session_Id__c;
        chatLog.Successful_Resolution__c = event.Successful_Resolution__c;

        if(!String.isBlank(sessionId) && sessionId.length() == 18 
                && Id.valueOf(sessionId).getSObjectType() == Schema.MessagingSession.SObjectType){ chatLog.Messaging_Session__c = sessionId;
        } else {
            LiveChatTranscript liveChat = [SELECT Id FROM LiveChatTranscript WHERE ChatKey = :event.Live_Agent_Session_Id__c];
            chatLog.Live_Chat_Transcript__c = liveChat.Id;
        }
        insert chatLog;
    }

    private void logChat(Einstein_Bot_Event__e event) {

        String sessionId = event.Live_Agent_Session_Id__c;
        Einstein_Bot_Chat_Log__c chatLog = new Einstein_Bot_Chat_Log__c();

        if(!String.isBlank(sessionId) && sessionId.length() == 18 
                && Id.valueOf(sessionId).getSObjectType() == Schema.MessagingSession.SObjectType){  chatLog.Messaging_Session__c = sessionId;
for(MessagingSession msgSession: [SELECT Id, MessagingEndUser.MessagingChannel.Developername FROM MessagingSession  WHERE Id =: sessionId]){ chatLog.Messaging_Channel__c = msgSession.MessagingEndUser.MessagingChannel.Developername;
            }
        } else {
            LiveChatTranscript liveChat = [SELECT Id FROM LiveChatTranscript WHERE ChatKey = :sessionId];
            chatLog.Live_Chat_Transcript__c = liveChat.Id;
        }

        chatLog.Live_Agent_Session_Id__c = sessionId;
        chatLog.Current_Utterance__c = event.Current_Utterance__c;
        chatLog.Current_Threshold_High__c = event.Current_Threshold_High__c;
        chatLog.Current_Confidence_For_Utterance__c = event.Current_Confidence_For_Utterance__c;
        chatLog.Current_Dialog_Id__c = event.Current_Dialog_Id__c;
        chatLog.Current_Dialog_Name__c = event.Current_Dialog_Name__c;
        chatLog.Topic_Name__c = returnDialogTopic(sessionId,event.Current_Dialog_Name__c);
        

        if(returnDialogMetric(event.Live_Agent_Session_Id__c,event.Current_Dialog_Name__c,'Business Start')) {
            chatLog.Business_Metric_Type__c = 'Start';
        }
        if(returnDialogMetric(event.Live_Agent_Session_Id__c,event.Current_Dialog_Name__c,'Business End')) {
            chatLog.Business_Metric_Type__c = 'End';
        }
        if(returnDialogMetric(event.Live_Agent_Session_Id__c,event.Current_Dialog_Name__c,'Business Direct')) {
            chatLog.Business_Metric_Type__c = 'Direct';
        }

        insert chatLog;
    }


    //finds the dialog topic name by looking at the custom metadata typelog
    private String returnDialogTopic(String trancriptSessionId,String dialogName) {
        if(!String.isBlank(trancriptSessionId) && trancriptSessionId.length() == 18 
                && Id.valueOf(trancriptSessionId).getSObjectType() == Schema.MessagingSession.SObjectType){ return NULL;
        }
        String returnString = 'NULL';
        LiveChatTranscript liveChat = [SELECT Id,LiveChatButton.DeveloperName FROM LiveChatTranscript WHERE ChatKey =:trancriptSessionId];
        String chatButtonName = liveChat.LiveChatButton.DeveloperName;
        Map<String,EinsteinBot_Report_Dialog_Name_Set__mdt> topicNameSettingMap = new Map<String,EinsteinBot_Report_Dialog_Name_Set__mdt>();
        for (EinsteinBot_Report_Dialog_Name_Set__mdt topicNameSetting : [SELECT Dialog_Name__c,Topic_Name__c FROM EinsteinBot_Report_Dialog_Name_Set__mdt WHERE Metric__c = 'Topic' AND Chat_Bot_Name__c =:chatButtonName]) {
            topicNameSettingMap.put(topicNameSetting.Dialog_Name__c,topicNameSetting);
        }
        if(topicNameSettingMap.ContainsKey(dialogName) && topicNameSettingMap.get(dialogName).Topic_Name__c != null) {
            returnString = topicNameSettingMap.get(dialogName).Topic_Name__c;
            }
        return returnString;
    }
    //finds the dialog business metric by looking at the custom metadata type
    private Boolean returnDialogMetric(String trancriptSessionId,String dialogName,String metric) {
        if(!String.isBlank(trancriptSessionId) && trancriptSessionId.length() == 18 
                && Id.valueOf(trancriptSessionId).getSObjectType() == Schema.MessagingSession.SObjectType){  return FALSE;
        }
        Boolean returnBoolean = false;
        LiveChatTranscript liveChat = [SELECT Id,LiveChatButton.DeveloperName FROM LiveChatTranscript WHERE ChatKey =:trancriptSessionId];
        String chatButtonName = liveChat.LiveChatButton.DeveloperName;
        Map<String,EinsteinBot_Report_Dialog_Name_Set__mdt> dialogNameSettingMap = new Map<String,EinsteinBot_Report_Dialog_Name_Set__mdt>();
        for (EinsteinBot_Report_Dialog_Name_Set__mdt dialogNameSetting : [SELECT Dialog_Name__c FROM EinsteinBot_Report_Dialog_Name_Set__mdt WHERE Metric__c =: metric AND Chat_Bot_Name__c =:chatButtonName]) {
            dialogNameSettingMap.put(dialogNameSetting.Dialog_Name__c,dialogNameSetting);
        }
        if(dialogNameSettingMap.ContainsKey(dialogName)) {
            returnBoolean = true;
        }
        return returnBoolean;
    }

    private void resetAccessToken(Einstein_Bot_Event__e event) {
        SMS_API_Settings__c settings = [SELECT Id, SSO_Token__c, API_Key__c, Username__c, Password__c FROM SMS_API_Settings__c];
        settings.SSO_Token__c = event.Identity_Token__c;
        update settings;
    }
}