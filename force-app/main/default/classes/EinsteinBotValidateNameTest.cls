/*
 * Author: Apoorv (Neuraflash)
 * Date: 07/08/2019
 * Description: Test Class for EinsteinBotValidateName.
*/
@isTest
private class EinsteinBotValidateNameTest {
    
    @isTest
    private static void testValidateName(){

        Test.startTest();
            List<Boolean> isValid = EinsteinBotValidateName.validateName(new List<String>{'John'});
        Test.stopTest();

        System.assertEquals(TRUE, isValid[0]);

    }

    @isTest
    private static void testValidateNameNegative(){

        Test.startTest();
            List<Boolean> isValid = EinsteinBotValidateName.validateName(new List<String>{'test2.com'});
        Test.stopTest();

        System.assertEquals(FALSE, isValid[0]);

    }

}