/************************************************************************************************************
* Apex Class Name   : PS_SingleExecution.cls
* Version           : 1.0 
* Created Date      : 17 Feb 2016
* Function          : test class for PS_SingleExecution 
* Modification Log  :
*                                                                        
  DATE            DEVELOPER        DESCRIPTION                               
*  ====            =========        =========== 
*| 02/15/2016      Sahir Ali        Singleton class to hold static variables during job execution 
/---------------------------------------------------------------------------------------------- */

global class PS_SingleExecution {

    private static Map<String,Id> singletonJobMap;
    
    global static Id hasAlreadyExecuted(String ClassNameOrExecutionName){  
        if(singletonJobMap != null) {
            return (singletonJobMap.get(ClassNameOrExecutionName)!=null)? singletonJobMap.get(ClassNameOrExecutionName): null;
            
        }
        //By default return null
        return null;
    }
    
    global static void setAlreadyExecuted(String ClassNameOrExecutionName, Id jId) {
        if(singletonJobMap == null) {
            singletonJobMap = new Map<String,Id>();
        }
        singletonJobMap.put(ClassNameOrExecutionName,jId);
    }
    
    global static void forceResetOfAlreadyExecuted() {
        singletonJobMap = null;
    }
    
}