/*******************************************************************************************************************
* Apex Class Name  : PS_contactidvalidation 
* Version          : 1.0
* Created Date     : 21 January 2016
* Function         : To check valid identification number on contact
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
* Karthik.A.S             21/01/2016              To check valid identification number on contact
* --------------------------------------------------------------------------------------------------------------------
*******************************************************************************************************************/
public class PS_contactidvalidation {

 static list < String > finalresult;
 static List < string > evenIdFromCart = new List < string > ();
 static List < string > oddIdFromCart = new List < string > ();
 

 public static void contactidvalidation(list < contact > idlist) {

  for (contact l: idlist) {

if(l.National_Identity_Number__c!=null ){

     
  
   string contactidentifiactionnumber = l.National_Identity_Number__c;
  if (contactidentifiactionnumber.length() > 0) {


    string odd='';
    string even='';
    Integer seq = 1;
    string contactidlastdigit = '';
    oddIdFromCart.clear();
    evenIdFromCart.clear();

    list < String > cartbreak = new list < String > ();
    cartbreak = contactidentifiactionnumber.split('');

    for (Integer i = 0; i < cartbreak.size(); i++) {
     String contactidspilt = cartbreak[i];
     contactidlastdigit = cartbreak.get(cartbreak.size() - 1);
     
     if (contactidspilt != 'null' && contactidspilt != '') {
      if ((math.mod(i, 2)) == 0) {
       odd = string.valueof(cartbreak[i]);
       oddIdFromCart.add(odd);


       //List<string> even1=new List<string>();
       // string> even =concat(productIdFromCart);

      } else {
       even = string.valueof(cartbreak[i]);
       evenIdFromCart.add(even);

       seq++;
      }

     }
    }

    String evenId = '';
    for (Integer i = 0; i < evenIdFromCart.size(); i++) {

     evenId = evenId + evenIdFromCart[i];



    }
    Integer evenspilt1 = integer.valueof(evenId);
    integer evenspilt2 = evenspilt1 * 2;
    String evenspilt3 = '';
    evenspilt3 = evenspilt3 + evenspilt2;


    list < String > contactevencombine = new list < String > ();
    contactevencombine = evenspilt3.split('');
    //list<string>int4= integer.valueof(cartbreak1);

    Integer contactevendigitadding = 0;
    for (Integer i = 0; i < contactevencombine.size(); i++) {

     contactevendigitadding = contactevendigitadding + integer.valueof(contactevencombine[i]);

    }
    Integer contactOdddigitadding = 0;
    for (Integer i = 0; i < oddIdFromCart.size(); i++) {

     contactOdddigitadding = contactOdddigitadding + integer.valueof(oddIdFromCart[i]);

    }
    string contactoddlastpostion = oddIdFromCart.get(oddIdFromCart.size() - 1);
    integer contactoddlastpostionint = integer.valueof(contactoddlastpostion);
    integer contactoddlast=contactOdddigitadding-contactoddlastpostionint;
    
    integer contactevenadd;
     contactevenadd = contactoddlast + contactevendigitadding;
   // contactevenadd = contactevendigitadding + contactOdddigitadding;
    String int9 = string.valueof(contactevenadd);

    list < String > contactevenaddresult = new list < String > ();
    contactevenaddresult = int9.split('');
    string contactevenaddresultstr = contactevenaddresult.get(contactevenaddresult.size() - 1);
    integer contactevenaddresultint = integer.valueof(contactevenaddresultstr);
    integer contactevenaddfinalresult = 10 - contactevenaddresultint;
    string contactvalidid = string.valueof(contactevenaddfinalresult);
    //list<String>fianl3 = new list<String>();
    finalresult = contactvalidid.split('');
    if (finalresult.size() > 1)
     {
     string e4 = finalresult.get(finalresult.size() - 1);
     if (e4 != contactidlastdigit) 
      {
      l.addError('Please Enter vaild National Identity Number');

      }

     } 
     else 
     {
      if ((contactvalidid != contactidlastdigit))
      {
      l.addError('Please Enter vaild National Identity Number');
      }
   
}
  
   }
  
  }
 
 }
 }
}