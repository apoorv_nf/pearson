/*
 * Author: Apoorv (Neuraflash)
 * Date: 29/07/2019
 * Description: Test Class for EinsteinBotContactCheck.
*/
@isTest
private class EinsteinBotContactCheckTest {
    
    @testSetup 
    static void setup() {

        Contact con = new Contact();
        con.FirstName = 'John';
        con.LastName = 'Doe';
        con.Email = 'john@salesforce.com';
        insert con;
    }
       
    @isTest
    private static void testContactExistenceOne(){
    
        EinsteinBotContactCheck.ContactDetails conDetails = new EinsteinBotContactCheck.ContactDetails();
        conDetails.firstName = 'John';
        conDetails.lastName = 'Doe';
        conDetails.email = 'john@test.com';
        conDetails.routableId = '13452';
        
        List<EinsteinBotContactCheck.ContactDetails> conDetailList = new List<EinsteinBotContactCheck.ContactDetails>();
        conDetailList.add(conDetails);
        
        Test.startTest();
            EinsteinBotContactCheck.checkContactExistence(conDetailList);
        Test.stopTest();
    }
    
    @isTest
    private static void testContactExistenceTwo(){
    
        EinsteinBotContactCheck.ContactDetails conDetails = new EinsteinBotContactCheck.ContactDetails();
        conDetails.firstName = 'John';
        conDetails.lastName = 'Doe';
        conDetails.email = 'john@salesforce.com';
        conDetails.routableId = '13452';
        
        List<EinsteinBotContactCheck.ContactDetails> conDetailList = new List<EinsteinBotContactCheck.ContactDetails>();
        conDetailList.add(conDetails);
        
        Test.startTest();
            EinsteinBotContactCheck.checkContactExistence(conDetailList);
        Test.stopTest();
    }
}