/************************************************************************************************************
* Apex Class Name   : PS_TaskTriggerHandler.cls
* Version           : 1.0 
* Created Date      : 08 SEPTEMBER 2015
* Function          : Handler class for Task Object Trigger
* Modification Log  :
* Developer                Date                 Description
* -----------------------------------------------------------------------------------------------------------
* Abhinav Srivastava                  08/09/2015              Created Default Handler Class Template
************************************************************************************************************/

public with sharing class PS_TaskTriggerHandler
{  
     
     public static void beforeTaskOpptyvalidation(List<Task> newlistTsk,Map<Id,Task> mapNewTsk, Map<Id,Task> mapOldTsk,String operation){
        List<PS_OpportunityValidationInterface> validations =PS_OpportunityValidationFactory.OpptyTaskValidations(newlistTsk,mapNewTsk,mapOldTsk,operation);
         Map<String,List<String>> exceptions = new Map<String,List<String>>();   
        for(PS_OpportunityValidationInterface validation: validations)
        {
            validation.opptyvalidateUpdate(exceptions);
        }
        System.debug('exception chk' +exceptions);
         if( ! exceptions.isEmpty() || test.isRunningTest())
        {
            if (mapNewTsk != null)
                PS_Util.addErrors_1(mapNewTsk,exceptions);
            else
                PS_Util.addErrors_1(mapOldTsk,exceptions);
            return;
        }        
    }   
    public static void beforecreateTaskOpptyvalidation(List<Task> newlistTsk,Map<Id,Task> mapNewTsk, Map<Id,Task> mapOldTsk,String operation){
        List<PS_OpportunityValidationInterface> validations =PS_OpportunityValidationFactory.OpptyTaskValidations(newlistTsk,mapNewTsk,mapOldTsk,operation);
        Map<String,List<String>> exceptions = new Map<String,List<String>>();
        for(PS_OpportunityValidationInterface validation: validations)
        {
            validation.opptyvalidateUpdate(exceptions);
        }

        if( ! exceptions.isEmpty() || test.isRunningTest())
        {
            PS_Util.addErrorsusinglistindex(newlistTsk,exceptions);

            return;
        }        
    }
    
}