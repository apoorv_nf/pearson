@isTest(seealldata=true)
private class UpdateArticleViewCountTest {
	@isTest
    private static void testBatch(){
        Customer_Support__kav cS = new Customer_Support__kav();
        cS.Title = 'abcd09876';
        cS.UrlName = 'abcd09876';
        cS.Business_Vertical__c = 'Higher Education';
        cS.Geography__c = 'North America';
        insert cS;
        
        String articleId = [select KnowledgeArticleId from Customer_Support__kav where Id =:cS.Id][0].KnowledgeArticleId;

        KbManagement.PublishingService.publishArticle(articleId, true);
        
        UpdateArticleViewCount__c uAVC = new UpdateArticleViewCount__c();
        uAVC.ArticleId__c = cS.Id;
        uAVC.Name = '999';
        uAVC.MaxViewCount__c = '10';
        insert uAVC;
        
        Database.executeBatch(new UpdateArticleViewCount());
    }
}