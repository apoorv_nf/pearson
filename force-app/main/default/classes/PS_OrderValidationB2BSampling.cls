/************************************************************************************************************
* Apex Interface Name : PS_OrderValidationB2BSampling
* Version             : 1.0 
* Created Date        : 10 Jul 2015
* Function            : Implementation of PS_OrderValidationInterface for B2B Sampling Orders
* Modification Log    :
* Developer                   *Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Davi Borges              10/Jul/2015            Initial version
* Rony Joseph              03/Dec/2015            Updating Error Handling
* Rony Joseph              29/dec/2015            Fix for permission set issue
* Rony Joseph              19/Feb/2016            Fix for issue related to Permission set conflict.
* Rony Joseph              11/Mar/2016            Fix for defect D-4459 and D-4460.
* Rony Joseph              14/Mar/2016            Fix for defect D-4459.
* Rony Joseph              16/Mar/2016            Fix for integration(ESB) failure.
* Rony Joseph              27/Apr/2016            Validation check on Order for Canada market.
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

public class PS_OrderValidationB2BSampling implements PS_OrderValidationInterface {
    
    private Map<String,Order> inOrders;
    private Map<Id, Order> oldOrders;
    private User contextUser;
    private User ologUser;
    private Map<Id, List<OrderItem>> omapOrderItem;
    
    /*************************************************************************************************************
* @Name        : initialize
* @Description : used to initiliaze the class
* 
* @Todo        : 
* @Input       : inOrders: list of order to be validated
inOldOrders: old version of the order 
contextUser: user as context for the validation process    
* @Output      : N/A
*************************************************************************************************************/
    public void initialize(Map<String,Order> inOrders, Map<Id, Order> oldOrders, User contextUser)
    {
        this.inOrders = inOrders;
        this.oldOrders = oldOrders;
        this.contextUser = contextUser;
        ologUser=[select id,market__c from user where id =: UserInfo.getUserId()];
        omapOrderItem=new Map<Id,List<OrderItem>>();
        for(OrderItem oOrderItem : [select id,OrderId,Shipping_Method__c from OrderItem where OrderId in:inOrders.keyset()]){
            if (omapOrderItem.containsKey(oOrderItem.OrderId)){
                omapOrderItem.get(oOrderItem.OrderId).add(oOrderItem);
            }
            else{
                omapOrderItem.put(oOrderItem.OrderId,new List<OrderItem>{oOrderItem});  
            }
        }        
    }
    
    /*************************************************************************************************************
* @Name        : validateInsert
* @Description : determines if the order can be inserted
* 
* @Todo        : 
* @Input       : exceptions: instance of map that will receive the exeption. The error should not be added 
to the order to allow this interface to be used outside the tigger context.
* @Output      : true if there is no error, false if there is error.
*************************************************************************************************************/
    public Boolean validateInsert(Map<String,List<String>> exceptions)
    {
        
        Map<String,Order> inOrdersToProces = new Map<String,Order>();
        
        for(String ordKey : inOrders.keySet())
        {
            Order ord = inOrders.get(ordKey);
            
            inOrdersToProces.put(ordKey,ord);
            
        }
        
        for(String ordKey : inOrdersToProces.keySet()){
            
            List<String> errors = validateInsertSingleOrder(inOrders.get(ordKey));
            
            if(! errors.isEmpty()){
                
                exceptions.put(ordKey,errors);
            }
        }
        
        return exceptions.isEmpty();
        
        
    }
    
    
    /*************************************************************************************************************
* @Name        : validateUpdate
* @Description : determines if the order can be updated
* 
* @Todo        : 
* @Input       : exceptions: instance of map that will receive the exeption. The error should not be added 
to the order to allow this interface to be used outside the tigger context.
* @Output      : true if there is no error, false if there is error.
*************************************************************************************************************/
    
    public Boolean validateUpdate(Map<String,List<String>> exceptions)
    {
        try{
            //filter order with status 
            
            
            Map<String,Order> inOrdersToProces = new Map<String,Order>();
            
            Map<String,Order> inOrdersToProcesNormalUpdate = new Map<String,Order>();
            
            for(String ordKey : inOrders.keySet())
            {
                Order ord = inOrders.get(ordKey);
                system.debug('orderstatus'+(ord.status+oldOrders.get(ord.Id).status));
                if(!((ord.status == 'New' && oldOrders.get(ord.Id).status == 'New') || 
                     (ord.status != 'New' && oldOrders.get(ord.Id).status == 'New')))
                {
                    inOrdersToProces.put(ordKey,ord);
                }else
                {
                    inOrdersToProcesNormalUpdate.put(ordKey,ord);
                }
            }
            
            if(! inOrdersToProcesNormalUpdate.isEmpty()){
                
                for(String ordKey : inOrdersToProcesNormalUpdate.keySet()){
                    
                    List<String> errors = validateAddressSingleOrder(inOrders.get(ordKey));
                    
                    if(! errors.isEmpty()){
                        
                        exceptions.put(ordKey,errors);
                    }
                }
                
            }
            
            
            Boolean userHasPermission = (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Sample Order Approver') || 
                                         PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Backend Order Creation')|| 
                                         PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Manage Orders'));
            
            System.debug('User Bypass:' + userHasPermission+inOrdersToProces);
            
            //validation for other user that don't have te permission
            for(String ordKey : inOrdersToProces.keySet()){
                
                List<String> errors = validateUpdateSingleOrder(inOrders.get(ordKey), oldOrders.get(inOrders.get(ordKey).Id), userHasPermission);
                
                system.debug('###Error'+errors);
                if(! errors.isEmpty()){
                    
                    exceptions.put(ordKey,errors);
                }
            }
            
            
            return exceptions.isEmpty();
        }catch (exception e){
            string errormessage=e.getMessage()+e.getStackTraceString();
            ExceptionFramework.LogException('Order Update','PS_OrderValidationB2BSampling','validateUpdate',errormessage,UserInfo.getUserName(),'');           
            return null;
        }   
    }
    
    private List<String> validateInsertSingleOrder(Order inOrder)
    {
        List<String> errors =  new List<String>();
        
        errors.addAll(validateAddressSingleOrder(inOrder));
        
        return errors;
        
    }
    
    private List<String> validateAddressSingleOrder(Order inOrder)
    {
        try{
            
            string shippingmethodcheck = '' ;
            List<String> errors =  new List<String>();
            shippingmethodcheck = ItemShipMethodCheck(inorder,oldOrders.get(inorder.Id));
            if(shippingmethodcheck != null){errors.add(shippingmethodcheck);} 
            if(!inOrder.isTemporary__c){
                
                if(inOrder.ShippingStreet!=null){
                    
                    if(inOrder.ShippingStreet.length() >120)
                    {
                        errors.add('The street address can only be 120 characters long!'); 
                    } 
                } 
                
            } 
            
            return errors;
        }catch (exception e){
            string errormessage=e.getMessage()+e.getStackTraceString();
            ExceptionFramework.LogException('Order Update','PS_OrderValidationB2BSampling','validateAddressSingleOrder',errormessage,UserInfo.getUserName(),'');           
            return null;
        } 
    }
    
    
    private List<String> validateUpdateSingleOrder(Order updatedOrder, Order oldOrder, Boolean userBypass)
    {
        
        try{
            List<String> errors =  new List<String>();
            string shippingmethodcheck = '' ;
            Boolean userHasorderstatuschange= (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Network Sampler'));
            Boolean userHasfrontendpermission = (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Front-End Sampling'));
            Boolean userHaSampleOrderApprove= (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Sample Order Approver'));
            
            Id approvermanager = [select Order_Approver__c from user where id = :updatedOrder.createdbyid].Order_Approver__c;
            system.debug('values:'+userHasorderstatuschange+contextUser+oldOrder.status+updatedOrder.status+approvermanager+userinfo.getuserid());                                                               
            
            if(updatedOrder.status =='New' && oldOrder.status !='New')
            {
                errors.add('You do not have permission to change the order <b><i>status</i></b> back to New!');
            }
            
            shippingmethodcheck = ItemShipMethodCheck(updatedOrder,oldOrder);
            if(shippingmethodcheck != null){errors.add(shippingmethodcheck);} 
            
            if(userBypass)
            {
                return errors;
            }
            
            if((userHaSampleOrderApprove||userHasorderstatuschange) &&oldOrder.status=='Open'&& updatedOrder.status !='Open' )//Defect fix for D-4459 and 4460
            {errors.add('Once an ordered is approved,you are not allowed to changes the status.');}
            
            if(!userHasfrontendpermission) {  
                if((updatedOrder.status !='Open' && updatedOrder.status!='Cancelled')|| (oldOrder.status !='Pending Approval')||(!userHasorderstatuschange)||(approvermanager!= userinfo.getuserid())){//Defect fix for D-4459 and 4460
                    errors.add('You do not have permission to change the order status from Pending Approval to Open or Cancelled');
                }
                else
                {PS_OrderItemValidationB2BSampling.comingfromui =true;}
                
            }                              
            
            if(updatedOrder.AccountId != oldOrder.AccountId)
            {
                errors.add('You do not have permission to change the <b><i>account</i></b> of the order!');
                
            }
            
            if(updatedOrder.OpportunityId != oldOrder.OpportunityId)
            {
                errors.add('You do not have permission to change the <b><i>Opportunity</i></b> of the order!');
            }
            
            if(updatedOrder.ShipToContactId != oldOrder.ShipToContactId)
            {
                errors.add('You do not have permission to change the <b><i>Shipping To Contact</i></b> of the order!');
            }
            
            if((updatedOrder.ShippingStreet != oldOrder.ShippingStreet) ||
               (updatedOrder.ShippingCity != oldOrder.ShippingCity) ||
               (updatedOrder.ShippingCountryCode != oldOrder.ShippingCountryCode) ||
               (updatedOrder.ShippingStateCode != oldOrder.ShippingStateCode) ||
               (updatedOrder.ShippingPostalCode != oldOrder.ShippingPostalCode) ||
               (updatedOrder.Order_Address_Type__c != oldOrder.Order_Address_Type__c))
            {
                errors.add('You do not have permission to change the <b><i>Shipping Address</i></b> of the order!');
                
            }
            
            
            if(updatedOrder.Cancel_After_Date__c != oldOrder.Cancel_After_Date__c)
            {
                errors.add('You do not have permission to change the <b><i>Cancel After Date</i></b> of the order!');
            }
            
            if(updatedOrder.POBox_Indicator__c != oldOrder.POBox_Indicator__c)
            {
                errors.add('You do not have permission to change the <b><i>POBox Indicator</i></b> of the order!');
            }
            
            if(updatedOrder.Rush_Flag__c != oldOrder.Rush_Flag__c)
            {
                errors.add('You do not have permission to change the <b><i>Rush Flag</i></b> of the order!');
            }
            
            if(updatedOrder.Do_Not_Ship_Before_Date__c != oldOrder.Do_Not_Ship_Before_Date__c)
            {
                errors.add('You do not have permission to change the <b><i>Do Not Ship Before Date</i></b> of the order!');
            }
            
            if(updatedOrder.Override_Schedule_Delivery_Date__c != oldOrder.Override_Schedule_Delivery_Date__c)
            {
                errors.add('You do not have permission to change the <b><i>Override Schedule Delivery Date</i></b> of the order!');
            }
            
            if(updatedOrder.EffectiveDate != oldOrder.EffectiveDate)
            {
                errors.add('You do not have permission to change the <b><i>Order Start Date</i></b> of the order!');
            }
            
            if(updatedOrder.Packing_Instructions__c != oldOrder.Packing_Instructions__c)
            {
                errors.add('You do not have permission to change the <b><i>Packing Instructions</i></b> of the order!');
            }
            
            if(updatedOrder.Shipping_Instructions__c != oldOrder.Shipping_Instructions__c)
            {
                errors.add('You do not have permission to change the <b><i>Shipping Instructions</i></b> of the order!');
            }
            
            
            system.debug('Value of Static Value:'+(PS_OrderItemValidationB2BSampling.comingfromui));
            if(PS_OrderItemValidationB2BSampling.comingfromui == null){
                if(updatedOrder.Status != oldOrder.Status)
                {
                    errors.add('You do not have permission to change the <b><i>status</i></b> of the order!');
                }
            }        
            
            return errors;
        }catch (exception e){
            string errormessage=e.getMessage()+e.getStackTraceString();
            ExceptionFramework.LogException('Order Update','PS_OrderValidationB2BSampling','validateUpdateSingleOrder',errormessage,UserInfo.getUserName(),'');           
            return null;
        } 
        
    }
    
    
    /*************************************************************************************************************
* @Name        : validateDelete
* @Description : determines if the order can be deleted
* 
* @Todo        : 
* @Input       : exceptions: instance of map that will receive the exeption. The error should not be added 
to the order to allow this interface to be used outside the tigger context.
* @Output      : true if there is no error, false if there is error.
*************************************************************************************************************/
    
    public Boolean validateDelete(Map<String,List<String>> exceptions)
    {
        return true;
    }
    
    public string ItemShipMethodCheck(Order updatedOrder, Order oldOrder)
    {
        List<Orderitem> itemlst;
        Boolean shipError = false;
        string shippingmethod;
        itemlst = omapOrderItem.get(updatedOrder.id);
        //user currentuser = [select id,market__c from user where id = : userinfo.getuserid() limit 1];
        //System.debug('user market is :' +currentuser.Market__c+oldOrder.status+updatedOrder.status); 
        if( (oldOrder.status != updatedOrder.status)&& (itemlst != null && itemlst.size()>1))
        { 
            
            if(ologUser.Market__c == 'CA')
            {
                
                shippingmethod =  itemlst[0].Shipping_Method__c;
                for(orderitem item:itemlst)
               {
                    if(!shippingmethod.equalsIgnoreCase(item.Shipping_Method__c))
                    {
                        shipError = true;
                    }
                }
                if(shipError)
                {
                    return 'All Order Products must have an identical shipping method.';
                }
                else
                    return null;           
                
                
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }  
}