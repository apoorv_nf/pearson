/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_BatchMarketingBlackoutAccConUpdate 
   Description:     Batch class to perform a 'marketing blackout' on AccountContacts related to a blacked out account. It is called 
                    from PS_BatchMarketingBlackoutAccountUpdate, and should never be run directly.
                
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   23/10/2015       1.0           Tom Carman - Accenture                              Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */


global class PS_BatchMarketingBlackoutAccConUpdate implements Database.Batchable<sObject>, Database.Stateful {
	
	global Set<Id> accountIds;
    global Map<Id, Id> accountToUltimateParent; // Map of account to its highest parent 
    global Map<Id, Boolean> ultimateParentToFlag; // Map of account to its current blackout flag status


	global PS_BatchMarketingBlackoutAccConUpdate(Set<Id> accountIds, Map<Id, Id> accountToUltimateParent, Map<Id, Boolean> ultimateParentToFlag) {
		
        this.accountIds = accountIds;
        this.accountToUltimateParent = accountToUltimateParent;
        this.ultimateParentToFlag = ultimateParentToFlag;

	}
	

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id, Account__c, Contact__c 
										FROM AccountContact__c 
										WHERE Account__c IN :accountIds]);
	}


   	global void execute(Database.BatchableContext BC, List<AccountContact__c> scope) {
       
        updateAccountContacts(scope);
	
    }
	
	global void finish(Database.BatchableContext BC) {
		
	}



	private void updateAccountContacts(List<AccountContact__c> accountContacts) {

        for(AccountContact__c accountContact : accountContacts) {
            if(accountToUltimateParent.containsKey(accountContact.Account__c)) {
                if(ultimateParentToFlag.containsKey(accountToUltimateParent.get(accountContact.Account__c))){
                    // set the Account Contact flag based on its ultimate parents flag
                    accountContact.Blackout_Flag_AccountContact__c = ultimateParentToFlag.get(accountToUltimateParent.get(accountContact.Account__c));
                }
            }
        }

        update accountContacts;
    }

	

	
	
}