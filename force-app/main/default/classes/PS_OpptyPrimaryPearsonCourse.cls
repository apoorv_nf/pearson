/*************************************************************************************************
*Name:   PS_OpptyPrimaryPearsonCourse
*Description : This class supports the trigger on object Opportunity_Pearson_Course_Code__c
* Created:  Sachi   
* Modified: Cristina  - Modified trigger so it also updates field to non-Apttus tables - made it without sharing to eliminate Apttus objects
*
*****************************************************************************************************/

public  without sharing class PS_OpptyPrimaryPearsonCourse{

public void updateOpptyPrimaryFlag(List<Opportunity_Pearson_Course_Code__c> newlist,Map<Id,Opportunity_Pearson_Course_Code__c> oldmap,
Boolean isinsert,Boolean isUpdate){
    Set<Id> OpptyId=new Set<Id>();
    List<Opportunity_Pearson_Course_Code__c> listToupdate= new List<Opportunity_Pearson_Course_Code__c>();

    for(Opportunity_Pearson_Course_Code__c pce: newlist){
    if(isinsert){
        if(pce.Primary__c){
            OpptyId.add(pce.Opportunity__c);}
    }//insert condition
    if(isUpdate){        
        system.debug('Oldvalue='+oldmap.get(pce.id).Primary__c);
        if(pce.Primary__c && (!oldmap.get(pce.id).Primary__c)){
            OpptyId.add(pce.Opportunity__c);}
    }//update cn
    }//for loop
    if (OpptyId.size()>0){
        List<Opportunity_Pearson_Course_Code__c> preprimlist=[select id,primary__c from Opportunity_Pearson_Course_Code__c where Opportunity__c in :OpptyId and primary__c=true];
        if (preprimlist.size()>0){
            listToupdate.clear();
            for(Opportunity_Pearson_Course_Code__c pce: preprimlist){
                pce.primary__c=false;
                listToupdate.add(pce);
            }
         }
     }
     if (listToupdate.size()>0){
         try{
             update listToupdate;
         }
         catch(DMLException e){
             throw(e);
         }              
      }
    }
    public void upsertHierarchy(List<Opportunity_Pearson_Course_Code__c>  newlist){

      //CP 12/04/2017 To add the non Apttus lookup data
      Map < id ,Hierarchy__c > allCatHierMap = new Map<id, Hierarchy__c >();
      Set<Id> newApttusCatHier = new Set<Id>();
      //we create a list of the apttus Categoryhierarchy Id's we need to map
       for (Opportunity_Pearson_Course_code__c newOPce : newlist){
           //12-07 newApttusCatHier.add(newOPce.Pearson_course_Code_Name__c);
           newApttusCatHier.add(newOPce.Pearson_course_Code_Hierarchy__c);
       }
        //we create a list of new CategoryHierarchies from the list just created of Ids
       //12-07 List<Hierarchy__c> hierLst= [Select id, CategoryHierarchy_ExternalID__c FROM Hierarchy__c WHERE CategoryHierarchy_ExternalID__c IN :newApttusCatHier];
       List<Hierarchy__c> hierLst= [Select id, CategoryHierarchy_ExternalID__c FROM Hierarchy__c WHERE id IN :newApttusCatHier];
        //System.debug('in trigger upsert - hierLst.size() = ' + hierLst.size());
       //we add them to a map so we can get the external id as the id
       for (Hierarchy__c hier : hierLst){
           allCatHierMap.put(hier.id, hier);
       }
       // we loop thru the list of new course equivalents and should check that we have this Hierarchy in our list
       for (Opportunity_Pearson_Course_Code__c upsertOPce : newList){
          //if (allCatHierMap.containsKey(upsertOPce.Pearson_Course_Code_Name__c)){
          if (allCatHierMap.containsKey(upsertOPce.Pearson_Course_Code_Hierarchy__c)){
                Hierarchy__c hierMap = (Hierarchy__c)allCatHierMap.get(upsertOPce.Pearson_Course_Code_Hierarchy__c);
                //upsertOPce.Pearson_Course_Code_Name__c = hierMap.id;
                //upsertOPce.Pearson_Course_Code_Name__c = hierMap.CategoryHierarchy_ExternalID__c; Replaced the Apttus To Non-Apttus
                //upsertOPce.Pearson_Course_Code_Hierarchy__c= hierMap.CategoryHierarchy_ExternalID__c;
            }
       }
          
    }
}