/************************************************************************************************************
* Apex Class Name   : PS_Opty_Course_Territoryhelper.Class
* Version           : 1.0 
* Created Date      : 09 Aug 2017
* Function          : Helper for handling Territory,Region and District population in Opportunity and Course object  
* Function          :          
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Abraham Daniel           09/08/2017           Helper to populate Territory,Region and District
************************************************************************************************************/

public class PS_Opty_Course_Territoryhelper {
    
        /***********************************************************************************************/
    //August CR-00641 -->Abraham
    
    //Description: New method populateterritoryID, to populate the Territory ID in opportunity
    /***********************************************************************************************/
    Public static void populateterritoryID(){
        List<String> Terrcode = new List<String>();
            for(opportunity opp :(List<opportunity>)Trigger.new)
            {
                Terrcode.add(opp.Territory_Code__c);
            }
              list<Territory2> Terr = [Select id,Territory_Code__c,ParentTerritory2.Name,ParentTerritory2.ParentTerritory2.Name from Territory2 where Territory_Code__c IN :Terrcode And (Territory2Model.ActivatedDate!=null and Territory2Model.DeactivatedDate=null)];
              Map<String,String> Mapterr = new map<string,String>();
              for(Territory2 terr2 :Terr)
              {
              Mapterr.put(terr2.Territory_Code__c,terr2.Id);
              }
              for(opportunity opp :(List<opportunity>)Trigger.new)
              {              
              if(opp.Territory_Code__c !=null)  
            {
                
                opp.Territory2Id = Mapterr.get(opp.territory_Code__c);         
            }
               }
        
          Map<String,String> MapDist1 = new map<string,String>();
              for(Territory2 terr2 :Terr)
              {
              MapDist1.put(terr2.Territory_Code__c,terr2.ParentTerritory2.Name);
              }
              for(Opportunity opp1 :(List<Opportunity>)Trigger.new)
              {              
              if(opp1.Territory_Code__c !=null)  
            {
                
                Opp1.District__c = MapDist1.get(opp1.territory_Code__c);         
            }
               }
        
        Map<String,String> MapReg1 = new map<string,String>();
              for(Territory2 terr2 :Terr)
              {
              MapReg1.put(terr2.Territory_Code__c,terr2.ParentTerritory2.ParentTerritory2.Name);
              }
              for(Opportunity Opp2 :(List<Opportunity>)Trigger.new)
              {              
              if(opp2.Territory_Code__c !=null )  
            {
                
                opp2.Region__c = MapReg1.get(opp2.territory_Code__c);         
            }
               }
            }
}
     /***********************************************************************************************/
    //August CR-00641 --> Abraham
    
    //Description: New method to populate territory,region and district in Course
    /***********************************************************************************************/
  /*  Public static void populateterritorycourse(){
        List<String> Terrcode = new List<String>();
            for(UniversityCourse__c Univc :(List<UniversityCourse__c>)Trigger.new)
            {
                Terrcode.add(Univc.Course_Territory_Code_s__c);
            }
              list<Territory2> Terr = [Select id,Territory_Code__c,Name,ParentTerritory2.Name,ParentTerritory2.ParentTerritory2.Name from Territory2 where Territory_Code__c IN :Terrcode And Territory2Model.name = 'FY16 Pearson Territory Model'];
              Map<String,String> Mapterr = new map<string,String>();
              for(Territory2 terr2 :Terr)
              {
              Mapterr.put(terr2.Territory_Code__c,terr2.Name);
              }
              for(UniversityCourse__c Univc :(List<UniversityCourse__c>)Trigger.new)
              {              
              if(Univc.Course_Territory_Code_s__c !=null && Univc.Course_Territory_Code_s__c.length()<4)  
            {
                
                Univc.Territory__c = Mapterr.get(Univc.Course_Territory_Code_s__c);         
            }
               }
        
        
        
         Map<String,String> MapDist = new map<string,String>();
              for(Territory2 terr2 :Terr)
              {
              MapDist.put(terr2.Territory_Code__c,terr2.ParentTerritory2.Name);
              }
              for(UniversityCourse__c Univc :(List<UniversityCourse__c>)Trigger.new)
              {              
              if(Univc.Course_Territory_Code_s__c !=null && Univc.Course_Territory_Code_s__c.length()<4)  
            {
                
                Univc.District__c = MapDist.get(Univc.Course_Territory_Code_s__c);         
            }
               }
        
        
         Map<String,String> MapReg = new map<string,String>();
              for(Territory2 terr2 :Terr)
              {
              MapReg.put(terr2.Territory_Code__c,terr2.ParentTerritory2.ParentTerritory2.Name);
              }
              for(UniversityCourse__c Univc :(List<UniversityCourse__c>)Trigger.new)
              {              
              if(Univc.Course_Territory_Code_s__c !=null && Univc.Course_Territory_Code_s__c.length()<4)  
            {
                
                Univc.Region__c = MapReg.get(Univc.Course_Territory_Code_s__c);         
            }
               }
        
            }
    
    
    
    //To Do dummy update when account records territory Codes are updated
   
    Public static void populateterrcoursefromAcc(List<Account> accList){
     
            List<UniversityCourse__c> Univ = [Select Id from UniversityCourse__c where Account__c IN:accList];                               
        update Univ;
            
    }          */