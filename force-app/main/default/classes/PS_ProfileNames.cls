/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:         PS_ProfileNames.cls 
   Description:  Constants for profile names
                
   Date             Version     Author                  Tag     Summary of Changes 
   -----------      -------     -----------------       ---     ------------------------------------------------------------------------
   26/08/2015       0.1         Accenture               None    Intial draft
  
  -------------------------------------------------------------------------------------------------------------------------------------------------------- */
public with sharing class PS_ProfileNames 
{
  public static final String PEARSON_ADMINISTRATOR = 'Pearson Administrator';
  public static final String SYSTEM_ADMINISTRATOR = 'System Administrator';
  public static final String PEARSON_DATA_ADMINISTRATOR = 'Pearson Data Administrator';
  public static final String PEARSON_LOCAL_ADMINISTRATOR = 'Pearson Local Administrator OneCRM';  
public PS_ProfileNames(){
    }
}