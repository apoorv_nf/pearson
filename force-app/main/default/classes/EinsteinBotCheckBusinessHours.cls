/**
* @description : Checks for Business hours.
* @unitTest    : EinsteinBotCheckBusinessHoursTest
* @Author      : Apoorv (Neuraflash)  
* @CreatedDate : Aug 29, 2019
*/
public without sharing class EinsteinBotCheckBusinessHours {
	
	@InvocableMethod(label='Einstein Bot - Check Business Hours')
    public static List<Boolean> checkBusinessHours(List<String> businessHoursName){
		try{
			Boolean isWithinBusinessHours;
            String bHourName;
            if(businessHoursName != NULL && businessHoursName.size()>0){
                bHourName = businessHoursName[0];
            }
            if(!String.isBlank(bHourName)){
                isWithinBusinessHours = new BusinessDaysUtil(bHourName).isBusinessDay(DateTime.now());
            } else { 
                isWithinBusinessHours = new BusinessDaysUtil().isBusinessDay(DateTime.now());
            }
			return new List<Boolean>{isWithinBusinessHours};
		} catch(Exception ex){}
		return NULL;
    }
}