@isTest
public class ProcessIntegrationReqFailedZABatchTest {

    public static testmethod void invokeIntegrationReqFailedZABatch(){
        Test.startTest();
        createIntReqRecords();        
        ProcessIntegrationRequestFailedZABatch intReqBatch = new ProcessIntegrationRequestFailedZABatch();
        String hour = String.valueOf(Datetime.now().hour());
		String min = String.valueOf(Datetime.now().minute() + 10);
        String ss = String.valueOf(Datetime.now().second());

		//parse to cron expression
		String scheduleTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        system.schedule('ProcessIntegrationRequestFailedZA Job Started At '+String.valueOf(Datetime.now()), scheduleTime, intReqBatch);
		Test.stopTest();
        
    }
    public static testmethod void createIntReqRecords(){
        List<Integration_Request__c> intReqList = new List<Integration_Request__c>();
        Integration_Request__c inteReq = new Integration_Request__c(Market__c='ZA', Object_Id__c = '006b000000D5GHlAAN', Object_Name__c ='Opportunity', Direction__c ='Outbound', Event__c='External Escalation', Status__c='Functional Error');
        Integration_Request__c inteReq1 = new Integration_Request__c(Market__c='ZA', Object_Id__c = '006b000000D5GHlAAN', Object_Name__c ='Opportunity', Direction__c ='Outbound', Event__c='External Escalation', Status__c='Technical Error');
        Integration_Request__c inteReq2 = new Integration_Request__c(Market__c='ZA', Object_Id__c = '006b000000D5GHlAAN', Object_Name__c ='Opportunity', Direction__c ='Outbound', Event__c='External Escalation', Status__c='Functional Error');
        intReqList.add(inteReq);
        intReqList.add(inteReq1);
        intReqList.add(inteReq2);
        Insert intReqList;
        for(Integration_Request__c intReq: intReqList){
            intReq.status__c = 'Functional Error';
        }
        intReqList[2].Status__c = 'Technical Error';
        update intReqList;
                
        List<System_Response__c> sysRespList = new List<System_Response__c>();
        System_Response__c sysResp = new System_Response__c(Name='Test1',Integration_Request__c=inteReq.id, External_System__c ='Test');
        System_Response__c sysResp1 = new System_Response__c(Name='Test2',Integration_Request__c=inteReq.id, External_System__c ='Test');
        System_Response__c sysResp2 = new System_Response__c(Name='Test3',Integration_Request__c=inteReq1.id, External_System__c ='Test');
        System_Response__c sysResp3 = new System_Response__c(Name='Test4',Integration_Request__c=inteReq2.id, External_System__c ='Test');
        sysRespList.add(sysResp);
        sysRespList.add(sysResp1);
        sysRespList.add(sysResp2);
        sysRespList.add(sysResp3);
        Insert sysRespList; 
    }
}