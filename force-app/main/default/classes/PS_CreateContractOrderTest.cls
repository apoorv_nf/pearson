@isTest
private class PS_CreateContractOrderTest
{
    static testMethod void OrderCreateUnitTest(){
    
        User usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Price_List__c= 'Math & Science');
        usr.market__c='UK';
        usr.Business_Unit__c='Schools';
        usr.Line_of_Business__c='Schools';
        usr.Group__c='Primary';
        usr.License_Pools__c ='Test User';
        insert usr;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Validation_Rules__c=true);
        insert byp;       
        System.runAs(usr){
        test.startTest(); 
        account acc = new Account();
        acc.Name='Test Account1';
        acc.IsCreatedFromLead__c = True;
        acc.Phone='+91000001';
        acc.Market2__c= 'AU';
        acc.Line_of_Business__c= 'Higher Ed';
        acc.Geography__c= 'Core';
        acc.ShippingCountry = 'India';
        acc.ShippingCity = 'Bangalore';
        acc.ShippingStreet = 'BNG1'; 
        acc.ShippingPostalCode = '5600371';
        insert acc;
        
        Contact con = new Contact(FirstName='TestContactFirstname12', LastName='TestContactLastname12',AccountId=acc.Id ,Salutation='MR.', Email='test12@gmail.com',MobilePhone='111222333' );  
        insert con;
          
        List<RecordType> rt1 = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name = 'Global Rights Sales Opportunity' LIMIT 1];  
        List<RecordType> rtB2B = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name = 'B2B' LIMIT 1];  

        test.stopTest();
        Opportunity opp = new opportunity ( Name = 'OpporName1',RecordTypeId= rt1[0].Id,AccountId = acc.Id,CloseDate = System.TODAY() + 30,StageName = 'OpporStageName', CurrencyIsoCode = userinfo.getDefaultCurrency(),Third_Party_Account__c=acc.Id, Third_Party_Contact__c =con.id,Primary_Contact__c=con.id,Market__c ='UK', Line_of_Business__c = 'Higher Ed',Business_unit__c ='Higher Ed', Geography__c = 'Core' , Ownerid = UserInfo.getUserId());
        insert opp;

        Opportunity oppB2B = new opportunity ( Name = 'OpporName1B2B',RecordTypeId= rtB2B[0].Id,AccountId = acc.Id,CloseDate = System.TODAY() + 30,StageName = 'OpporStageName', CurrencyIsoCode = userinfo.getDefaultCurrency(),Third_Party_Account__c=acc.Id, Third_Party_Contact__c =con.id,Primary_Contact__c=con.id,Market__c ='UK', Line_of_Business__c = 'Higher Ed',Business_unit__c ='Higher Ed', Geography__c = 'Core' , Ownerid = UserInfo.getUserId());
        insert oppB2B; 
  
        OpportunityContactRole oc = new OpportunityContactRole( opportunityId =opp.Id,contactId = con.id ,Role =  'Business User');
        insert oc;                     
              
        Product2 prod = new Product2();
        prod.Configuration_Type__c='Option';     // CR-02949 - Commented the Apttus changes by Vinoth
        prod.name = 'NA Test Product5';
        prod.Duration__c = 'D20';
        prod.Market__c ='UK';
        prod.Business_Unit__c='Higher Ed';
        prod.Line_of_Business__c = 'Higher Ed';           
        prod.Author__c = 'Test Author'; 
        prod.Edition__c = '10';
        prod.Medium2__c = 'Print';
        prod.Relevance_Value__c = 10;
        prod.Brand__c = null;    
        prod.Binding__c = 'Cloth';
        prod.IsActive = True;
        insert Prod; 

        Pricebook2 pb = new Pricebook2(Name ='Standard Price Book 2009', Description = 'Price Book 2009 Products', IsActive = true );
        insert pb;
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id=pricebookId, Product2Id=prod.Id, UnitPrice=1000, IsActive=true);
        insert standardPBE;
        PricebookEntry pbe2 = new PricebookEntry(Pricebook2Id=pb.Id, Product2Id=prod.Id, UnitPrice=1000, IsActive=true);
        insert pbe2;
 
 
        List<RecordType> rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Order' AND Name = 'Global Rights Sales Contract' LIMIT 1];
        order sampleorder1 = new order(Accountid=opp.accountid,RecordTypeId=rt[0].Id,Opportunityid=opp.id,EffectiveDate=system.today(),Status='New',Pricebook2Id=pb.id,CurrencyIsoCode = userinfo.getDefaultCurrency(), ShippingCountry = 'India',Type = 'Rights Contract',ShipToContactid = opp.Primary_Contact__c,ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371', ShippingState = 'Karnataka', Geography__c = 'Core',Line_of_Business__c='Higher Ed',Market__c='UK',Ownerid = opp.Ownerid);
        insert sampleorder1;
        order sampleorder2 = new order(Accountid=opp.accountid,RecordTypeId=rt[0].Id,Opportunityid=opp.id,EffectiveDate=system.today(),Status='New',Pricebook2Id=pb.id,CurrencyIsoCode = userinfo.getDefaultCurrency(), ShippingCountry = 'India',Type = 'Rights Contract',ShipToContactid = opp.Primary_Contact__c,ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371', ShippingState = 'Karnataka', Geography__c = 'Core',Line_of_Business__c='Higher Ed',Market__c='UK',Ownerid = opp.Ownerid);
        insert sampleorder2; 
       
        OpportunityLineItem Oli = new OpportunityLineItem();
        Oli.OpportunityId = opp.id;
        Oli.Product2Id = Prod.Id;
        Oli.PricebookEntryId = pbe2.Id;
        Oli.Quantity = 1;
        oli.sample__c = true;
        Oli.TotalPrice = 10;
        Oli.Contract_Term__c = 10;
        Oli.Expiry_Basis__c = 'Contract';
        Oli.First_Advance_Amount__c = 10.00;
        Oli.First_Break_Quantity__c = 10;
        Oli.Format__c = 'Print/Analog Formats>Core Materials>Core';
        Oli.Language__c = 'Arabic';
        Oli.Print_Run__c = 10;
        //Oli.Publish_Months__c = 10;
       // Oli.Rights_Channel__c = 'All Channel';
        Oli.Royalty_Accounting_Period__c = 'Annually';
        Oli.Royalty_Method__c = 'Units Sold';
        Oli.Royalty_Rate_First_Break__c = 1.00;
        Oli.Royalty_Rate_Second_Break__c = 1.00;
        Oli.Royalty_Rate_Third_Break__c = 1.00;
        Oli.Sales_Territory__c = 'Albania';
        Oli.Second_Advance_Amount__c = 1.00;
        Oli.Second_Break_Quantity__c = 10;
        Oli.Subright__c = 'Translation';
        Oli.Third_Break_Quantity__c = 10;
        Oli.Price_Per_Unit__c=3.50;
                
        Insert Oli;        

        List<orderitem> olist = new List<orderitem>();      
        Orderitem item1 = new orderitem(orderid=sampleorder1.id,Shipped_Product__c = prod.id,Quantity = 1, pricebookentryid= pbe2.id , Unitprice=0, Contract_Term__c = 10,Expiry_Basis__c = 'Contract',First_Advance_Amount__c = 10.00,First_Break_Quantity__c = 10,Format__c = 'Digital Formats>Online Courses>Core',Language__c = 'Arabic',Print_Run__c = 10,Royalty_Accounting_Period__c = 'Annually',Royalty_Method__c = 'Units Sold',Royalty_Rate_First_Break__c = 1.00,Royalty_Rate_Second_Break__c = 1.00,Royalty_Rate_Third_Break__c = 1.00,Sales_Territory__c = 'Albania',Second_Advance_Amount__c = 1.00,Second_Break_Quantity__c = 10,Subright__c = 'Translation',Third_Break_Quantity__c = 10,Price_Per_Unit__c=2.50);
        Orderitem item2 = new orderitem(orderid=sampleorder1.id,Shipped_Product__c = prod.id,Quantity = 1, pricebookentryid= pbe2.id, Unitprice=0, Contract_Term__c = 10,Expiry_Basis__c = 'Contract',First_Advance_Amount__c = 10.00,First_Break_Quantity__c = 10,Format__c = 'Digital Formats>Online Courses>Core',Language__c = 'Arabic',Print_Run__c = 10,Royalty_Accounting_Period__c = 'Annually',Royalty_Method__c = 'Units Sold',Royalty_Rate_First_Break__c = 1.00,Royalty_Rate_Second_Break__c = 1.00,Royalty_Rate_Third_Break__c = 1.00,Sales_Territory__c = 'Albania',Second_Advance_Amount__c = 1.00,Second_Break_Quantity__c = 10,Subright__c = 'Translation',Third_Break_Quantity__c = 10,Price_Per_Unit__c=5.50);
        
        olist.add(item1);
        olist.add(item2);
        insert olist; 

        sampleorder1.Status = 'open';
        try{
            update sampleorder1;
        }
        Catch(Exception ex ){
            System.Debug('### Exception has occurred  ' + ex);
        }     
            
        Order Testorder=[select id,Status from Order WHERE Status='Open' limit 1];
        update Testorder;
                  
       
        PageReference tpageRef = Page.PS_CreateContractOrder;        
        Test.setCurrentPage(tpageRef); 
        ApexPages.StandardController setCon = new ApexPages.StandardController(opp);                 
              
        PS_CreateContractOrder obj = new PS_CreateContractOrder(setCon);
        obj.Createorderitem();
            
        PageReference OrderItem= new PageReference('/'+Oli.id);
        
        OrderItem testOrderItem=[Select id from OrderItem limit 1];
        update testOrderItem;
        
          }  

    }
}