/************************************************************************************************************
* Apex Interface Name : PS_OrderValidationB2BSampling
* Version             : 1.0 
* Created Date        : 10 Jul 2015
* Function            : Implementation of PS_OrderValidationInterface for B2B Sampling Orders
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Davi Borges              10/Jul/2015            Initial version
* -----------------------------------------------------------------------------------------------------------
* Karan Khanna             04/Dec/2015            Added Market check for US
* Tom Carman               14/12/2015             Added sortOrder() method to dispatch to correct validation implementation.
* Rony Joseph              05/05/2016             Added Market Check for CA. 
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

public class PS_OrderItemValidationFactory {
  
  public static Map<String, OrderItem> usOrderItems;
  public static Map<String, OrderItem> globalOrderItems;
  
  public static List<PS_OrderItemValidationInterface> CreateValidations(Map<String,OrderItem> orderItems,Map<Id,OrderItem> oldOrderItems, User contextUser )
  {
      usOrderItems = new Map<String, OrderItem>();
      globalOrderItems = new Map<String, OrderItem>();
      sortOrderItems(orderItems);

          
      List<PS_OrderItemValidationInterface> validators = new List<PS_OrderItemValidationInterface>();


      if(!usOrderItems.isEmpty()) {
          PS_OrderItemValidationB2BSampling validatorB2B = new PS_OrderItemValidationB2BSampling();
          validatorB2B.initialize(usOrderItems, oldOrderItems, contextUser);
          validators.add(validatorB2B);
      }


      if(!globalOrderItems.isEmpty()) {
          PS_OrderItemValidationDefault validatorDefault = new PS_OrderItemValidationDefault();
          validatorDefault.initialize(globalOrderItems, oldOrderItems, contextUser);
          validators.add(validatorDefault);
      }

      return validators;

    }

  
  public static void sortOrderItems(Map<String, OrderItem> orderItems) {


    for(OrderItem orderItem : orderItems.values()) {
      if(orderItem.Market__c == Label.PS_USMarket||orderItem.Market__c == Label.PS_CAMarket) {
        usOrderItems.put(orderItem.Id, orderItem);
      } else {
        globalOrderItems.put(orderItem.Id, orderItem);
      }
    }
  }
}