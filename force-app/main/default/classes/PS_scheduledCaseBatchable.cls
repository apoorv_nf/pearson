/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_scheduledCaseBatchable .cls 
   Description:     This class is a scheduler for PS_CaseBatchDelete batch class
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  14/12/2015         1.0            Sakshi Agarwal                        Initial Release 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */

global class PS_scheduledCaseBatchable implements Schedulable {
   global void execute(SchedulableContext sc) {
      PS_CaseBatchDelete obj = new PS_CaseBatchDelete();
      database.executebatch(obj,200);
   }

}