/**
 * Apex Class Name : EinsteinBotReportingBatch
 * Version         : 1.0
 * Created On      : 12-07-2018
 * Function        : A batch class to process the LiveChatTranscript to populate all the Reporting metrics
 * Test Class      : EinsteinBotReportingBatchTest
 */

public with sharing class EinsteinBotReportingBatch implements Database.Batchable<sObject> {
    String Query;


    public EinsteinBotReportingBatch(String liveChatButtonName){
        Query='SELECT Id,Einstein_Analytics_Reporting_Process__c FROM LiveChatTranscript WHERE LiveChatButton.DeveloperName =\''+liveChatButtonName+'\' AND Einstein_Analytics_Reporting_Process__c=false';
    }

    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(Query);
    }

    public void execute(Database.BatchableContext BC,List<sObject> scope){
        Set<Id> transcriptIdSet = new Set<Id>();
        List<LiveChatTranscript> transcriptList = new List<LiveChatTranscript>();
        For(LiveChatTranscript evt : (List<LiveChatTranscript>)scope) {
            transcriptIdSet.add(evt.Id);
        }
        EinsteinBotReportingBatchHelper reportingObj = new EinsteinBotReportingBatchHelper(transcriptIdSet);
    }

    public void finish(Database.BatchableContext BC){

    }

}