/************************************************************************************************************
* Apex Interface Name : PS_OrderItemValidationDefault
* Version             : 1.0 
* Created Date        : 14 Dec 2015
* Function            : Default implementation of PS_OrderItemValidationInterface for global Orders
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Tom Carman            14/Dec/2015            Initial version
************************************************************************************************************/

public class PS_OrderItemValidationDefault implements PS_OrderItemValidationInterface {



	private Map<String, SObject> inRecords;
	private Map<Id, SObject> oldRecords;
	private User contextUser;

	private Map<Id, Order> relatedOrderMap;

	private Map<Id, OrderItem> submittedOrderItems;
	private Map<Id, OrderItem> unSumbittedOrderItems;


    public void initialize(Map<String,OrderItem> inRecords, Map<Id, OrderItem> oldRecords, User contextUser) {

		this.inRecords = inRecords;
		this.oldRecords = oldRecords;
		this.contextUser = contextUser;

		submittedOrderItems = new Map<Id, OrderItem>();
		unSumbittedOrderItems = new Map<Id, OrderItem>();

      	getOrderInfo(this.inRecords.values());

		if(oldRecords != null) {
			sortOrdersItemsByOrderSubmissionStatus(inRecords, oldRecords);
		}

		System.debug('submittedOrderItems.size(): ' + submittedOrderItems.size());

    }




    public Boolean validateInsert(Map<String, List<String>> exceptions) {

    	return true;

    }



    public Boolean validateUpdate(Map<String, List<String>> exceptions) {

    	validateSubmittedOrderItems(exceptions);
		return exceptions.isEmpty();

    }



    public Boolean validateDelete(Map<String, List<String>> exceptions) {

    	return true;

    }


    private void sortOrdersItemsByOrderSubmissionStatus(Map<String, OrderItem> inOrderItems, Map<Id, OrderItem> oldOrderItems) {

    	for(OrderItem orderItem : inOrderItems.values()) {

    		Order relatedOrder = relatedOrderMap.get(orderItem.OrderId);

    		if(PS_Constants.ORDER_SUBMITTED_STATUSES.contains(relatedOrder.Status)) {
    			// Exclude order items that related to orders currently being submitted as part of this transaction
    			if(!PS_OrderSubmissionHandler.isOrderBeingSubmitted(relatedOrder.Id)) {
					submittedOrderItems.put(orderItem.Id, orderItem);
    			} 

    		} else {
    			unSumbittedOrderItems.put(orderItem.Id, orderItem);
    		}
    	}

    }



    private void validateSubmittedOrderItems(Map<String, List<String>> exceptions) {

		//Can this user edit Orders?
		Boolean permissionToEdit = getUserPermissionToEditSubmittedOrders();

		for(String orderItemKey : submittedOrderItems.keySet()) {

			List<String> errors = new List<String>();

			if(permissionToEdit) {
				 errors = validateForInvalidFieldChanges((SObject)submittedOrderItems.get(orderItemKey), oldRecords.get(orderItemKey));
			} else {		
				 errors.add(Label.PS_ORDERITEM_EDIT_PERMISSION);  /// To label
			}

			if(!errors.isEmpty()) {
				exceptions.put(orderItemKey, errors);
			}
		}
	}


	private List<String> validateForInvalidFieldChanges(SObject newRecord, SObject oldRecord) {

		List<String> invalidFieldsChanged = PS_FieldChangeValidation.checkForInvalidFieldChanges(newRecord, oldRecord, PS_FieldChangeValidation.ORDER_ITEM_EDIT_FIELDS);
		return PS_FieldChangeValidation.createErrorMessagesFromFieldLabel(invalidFieldsChanged);


	}


	private Boolean getUserPermissionToEditSubmittedOrders() {

		return (PS_Util.hasUserPermissionSet(contextUser.Id, PS_Constants.PERMISSIONSET_BACKEND_REVENUE) || 
	    		PS_Util.hasUserPermissionSet(contextUser.Id, PS_Constants.PERMISSIONSET_BACKEND_SAMPLE));
	}





	private void getOrderInfo(List<OrderItem> items) {


		Set<Id> relatedOrderIds = new Set<Id>();

		for(OrderItem item : items) {
			relatedOrderIds.add(item.OrderId);
		}
		
		relatedOrderMap = new Map<Id, Order>([SELECT Id, Status, isTemporary__c FROM Order WHERE Id in :relatedOrderIds]);


	}


}