/***
Apex Class : CreateStandardQuote
***/

@isTest(SeeAllData = false)                   
public class CreateStandardQuoteTest{
     static testMethod void myTest() { 
         
               Profile ProfileNameadmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        List<User> u1 = TestDataFactory.createUser(ProfileNameadmin.Id,1);
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu1'; 
            u1[0].Email ='Opptyproposaltest11@pearson.com'; 
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='Opptyproposaltest12'; 
            u1[0].LanguageLocaleKey ='en_US'; 
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = ProfileNameadmin.Id; 
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName ='standarduserQuoteCreation1@testorg.com';
            u1[0].Market__c ='UK';
            u1[0].Business_Unit__c = 'Higher Ed';
            u1[0].line_of_Business__c = 'Higher Ed';
            u1[0].Product_Business_Unit__c = 'Higher Ed';
            insert u1;
        }
        
        //Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u1[0].id,Disable_Triggers__c=true,Disable_Process_Builder__c=true);
         Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u1[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true,Disable_Workflow_Rules__c=true); 

        insert byp;
       
       System.runAs(u1[0]){  
          
           Account acc1 = new Account();
          acc1.Name = 'CTI Bedfordview Campus';
          acc1.Line_of_Business__c= 'Higher Ed';
          acc1.Geography__c = 'North America';
          acc1.Market2__c = 'US';
          acc1.Pearson_Campus__c=true;
          acc1.Pearson_Account_Number__c='Campus';
          insert acc1;
             
          Account acc = new Account();
          acc.Name = 'CTI Bedfordview Campus';
          acc.Line_of_Business__c= 'Higher Ed';
          acc.Geography__c = 'North America';
          acc.Market2__c = 'US';
          acc.Pearson_Campus__c=true;
          acc.Pearson_Account_Number__c='Campus';
          acc.Primary_Selling_Account__c = acc1.id;
          insert acc;
         
          Contact ci = new Contact();
          ci.FirstName = 'Bob';
          ci.LastName  = 'Test';
          ci.AccountId = acc.id;    
          ci.HomePhone='234567890';    
          ci.Email='hdfghsg@gmail.com';
          ci.Role__c='Primary sales contact';
          ci.Mobile_Last_Updated__c = System.Today();
          insert ci;          
                    
          Contact c= new Contact();
          c.Accountid= acc.id;
          c.FirstName = 'pop';
          c.LastName  = 'star';    
          c.HomePhone='1234567891';
          c.Email='dbfhgsf@gmail.com';
          c.Role__c='Business user';  
          ci.Mobile_Last_Updated__c = System.Today();  
          insert c; 
          
         
          
          Opportunity opp = new opportunity();
          opp.AccountId = acc.id;    
          opp.Name = 'OppTest';
          opp.StageName = 'Negotiation';
          opp.CloseDate = system.today();
          opp.Type='Returning Business';
          opp.Earlybird_Payment_Reference__c= TRUE;
          opp.CreatedDate=system.today();
          opp.Primary_Contact__c=ci.Id;
          opp.Market__C = 'ZA';
          //opp.Academic_Start_Date__c = system.today();
          //opp.Business_unit__c = 'Online and Blended Learning';
          insert opp;
          
          Opportunity opp1 = new opportunity();
          opp1.AccountId = acc.id;    
          opp1.Name = 'Test';
          opp1.StageName = 'Negotiation';
          opp1.CloseDate = system.today();
          opp1.Type='New Business';
          opp1.Earlybird_Payment_Reference__c= TRUE;
          opp1.CreatedDate=system.today();
          opp1.Primary_Contact__c=ci.Id;
          opp1.Market__c = 'AU';
          opp1.CurrencyIsoCode= 'AUD';
          opp1.Business_unit__c = 'Schools';
          opp1.Line_of_Business__c = 'Schools';
          opp1.Geography__c = 'Core';  
          opp1.Academic_Start_Date__c = system.today();
          insert opp1; 



		  Opportunity opp2 = new opportunity();
          opp2.AccountId = acc.id;    
          opp2.Name = 'Test';
          opp2.StageName = 'Negotiation';
          opp2.CloseDate = system.today();
          opp2.Type='Returning Business';
          opp2.Earlybird_Payment_Reference__c= TRUE;
          opp2.CreatedDate=system.today();
          opp2.Primary_Contact__c=ci.Id;
          opp2.Market__c = 'UK';
          opp2.CurrencyIsoCode= 'AUD';
          opp2.Business_unit__c = 'Schools';
          opp2.Line_of_Business__c = 'Schools';
          opp2.Geography__c = 'Core';  
          opp2.Academic_Start_Date__c = system.today();
          insert opp2;           
                           
          OpportunityContactRole ocr = new OpportunityContactRole();
          ocr.ContactId= ci.Id;
          ocr.OpportunityId = opp1.Id;
          ocr.IsPrimary = TRUE;
          ocr.Role = 'Primary Sales Contact';
          insert ocr;  
           
          OpportunityContactRole ocr2 = new OpportunityContactRole();
          ocr2.ContactId= ci.Id;
          ocr2.OpportunityId = opp2.Id;
          ocr2.IsPrimary = TRUE;
          ocr2.Role = 'Primary Sales Contact';
          insert ocr2; 
        
          OpportunityContactRole ocr1 = new OpportunityContactRole();        
          ocr1.ContactId= c.Id;
          ocr1.OpportunityId = opp1.Id;
          ocr1.IsPrimary = FALSE;
          ocr1.Role = 'Business User';
          insert ocr1;
           
          OpportunityContactRole ocr3 = new OpportunityContactRole();        
          ocr3.ContactId= c.Id;
          ocr3.OpportunityId = opp2.Id;
          ocr3.IsPrimary = FALSE;
          ocr3.Role = 'Business User';
          insert ocr3;
          
          Quote_Settings__c qs= new Quote_Settings__c();
          qs.Early_Bird__c = system.today()-1;
          qs.Earlybird_Securing_Fee_New_Business__c= 800;
          qs.Earlybird_Securing_Fee_Return_Business__c= 1700;
          qs.Name ='System Properties';
          insert qs;  
           
          PS_Market__c ps= new PS_Market__c();
          ps.Market__c ='AU';
          ps.Name ='AU';
          insert ps; 
          PS_Market__c ps1= new PS_Market__c();
          ps1.Market__c ='NZ';
          ps1.Name ='NZ';
          insert ps1;  
          PS_Market__c ps2= new PS_Market__c();
          ps2.Market__c ='UK';
          ps2.Name ='UK';
          insert ps2;   
           
          /*Quote_Settings__c qs1 = [select Early_Bird__c,Earlybird_Securing_Fee_New_Business__c,Earlybird_Securing_Fee_Return_Business__c,Name from Quote_Settings__c limit 1]; 
          qs1.Early_Bird__c = system.today()-1;
          qs1.Earlybird_Securing_Fee_New_Business__c= 800;
          qs1.Earlybird_Securing_Fee_Return_Business__c= 1700;
          qs1.Name ='System Properties';
          update qs1;*/
          
          //Id standardPBId = Test.getStandardPricebookId();
         //System.runAs(u1[0]){
          Id standardPBId = Test.getStandardPricebookId();
          Quote quo = new Quote();
          quo.Name= 'Test Quote'; 
          quo.First_Payment_Date__c=System.today();
          quo.Payment_Period_In_Month__c='3';
          quo.Deposit__c=1000.00;
          quo.Payment_Type__c='Monthly Payment';
          quo.Opportunityid=opp.id;
          quo.Pricebook2Id=standardPBId;
          quo.Registration_Fee__c=800;
          quo.Total_Early_Bird_Securing_Fee_Payments__c=1700;
          quo.Market__c=opp1.Market__c;
          //quo.PIHE_Qualification_Year__c=2;
          
          Insert quo;
           
           Id standardPBId1 = Test.getStandardPricebookId();
          Quote quo1 = new Quote();
          quo1.Name= 'Test Quote'; 
          quo1.First_Payment_Date__c=System.today();
          quo1.Payment_Period_In_Month__c='3';
          quo1.Deposit__c=1000.00;
          quo1.Payment_Type__c='Monthly Payment';
          quo1.Opportunityid=opp.id;
          quo1.Pricebook2Id=standardPBId1;
          quo1.Registration_Fee__c=800;
          quo1.Total_Early_Bird_Securing_Fee_Payments__c=1700;
          quo1.Market__c='NZ';
          //quo.PIHE_Qualification_Year__c=2;
          
          Insert quo1;
         
         
          List<Quote> quoList = new List<Quote>();
          quoList.add(quo); 
          quoList.add(quo1);

         
          Test.StartTest();
                                     
          PageReference pageRef = Page.CreateStandardQuote; //replace with your VF page name
          pageRef.getParameters().put('oppid', opp.id);
          Test.setCurrentPage(pageRef);
          ApexPages.StandardController stdQuote = new ApexPages.StandardController(quo);
          CreateStandardQuote controller = new CreateStandardQuote(stdQuote);
          controller.quoRecTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Global Quote').getRecordTypeId();
          controller.Quote_create();
          
          PageReference pageRef1 = Page.CreateStandardQuote; //replace with your VF page name
          pageRef1.getParameters().put('oppid', opp1.id);
          Test.setCurrentPage(pageRef1);
          ApexPages.StandardController stdQuote1 = new ApexPages.StandardController(quo);
          CreateStandardQuote controller1 = new CreateStandardQuote(stdQuote1);
          controller1.Quote_create();
          
          ApexPages.StandardController stdQuote2 = new ApexPages.StandardController(quo);
          CreateStandardQuote controller2 = new CreateStandardQuote(stdQuote2);
          controller2.QuoteAlert(); 
          
          //opp1.Business_unit__c = 'Online and Blended Learning';
          opp.Type='New Business';
          update opp; 
          
          PageReference pageRef2 = Page.CreateStandardQuote; //replace with your VF page name
          pageRef2.getParameters().put('oppid', opp.id);
          Test.setCurrentPage(pageRef2);
          ApexPages.StandardController stdQuote3 = new ApexPages.StandardController(quo);
          CreateStandardQuote controller3 = new CreateStandardQuote(stdQuote3);
          controller3.Quote_create();  
          
          opp1.Type=''; 
          opp1.Market__c = 'AU';
          update opp1;
          
          ocr.IsPrimary = False;
          update ocr;
          
          ocr1.IsPrimary = False;
          update ocr1;
           
          opp.Type='Pilot';
          update opp; 
           
          PageReference pageRef3 = Page.CreateStandardQuote; //replace with your VF page name
          pageRef3.getParameters().put('oppid', opp.id);
          Test.setCurrentPage(pageRef3);
          ApexPages.StandardController stdQuote4 = new ApexPages.StandardController(quo);
          CreateStandardQuote controller4 = new CreateStandardQuote(stdQuote4);
          controller4.Quote_create();

           
          PageReference pageRef6 = Page.CreateStandardQuote; //replace with your VF page name
          pageRef6.getParameters().put('oppid', opp2.id);
          Test.setCurrentPage(pageRef6);
          ApexPages.StandardController stdQuote6 = new ApexPages.StandardController(quo1);
          CreateStandardQuote controller6 = new CreateStandardQuote(stdQuote6);
          controller6.Quote_create();
          
          Product2 prod = new Product2(Name = 'Bachelor of Commerce', 
                                        PIHE_Repeater__c='true',
                                        Semester__c ='First Semester',
                                        PIHE_Conferer__c='PIHE',
                                        PIHE_Qualification_Year__c='1',
                                        PIHE_Qualification__c='Pre-degree Foundation Programme (Social Sciences)',
                                        Module_Type__c='Core',
                                        Module_Code__c='FPIP021',
                                        Configuration_Type__c='Option',
                                        Auto_Selected__c=true,                                                                                 
                                        Family = 'Best Practices', IsActive = true);
       
       Prod.Semester__c ='Full Year';
       insert prod;
        
       PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, 
                                                            Product2Id = prod.Id,
                                                            UnitPrice = 10000, 
                                                            CurrencyIsoCode =UserInfo.getDefaultCurrency(),
                                                            IsActive = true);
       insert standardPBE;
        
       List<QuoteLineItem> qltList = new List<QuoteLineItem>();   
            QuoteLineItem qliliner = new QuoteLineItem(Product2Id=prod.id,Outside_Module__c=true,
                                                       QuoteId=quo.id,PricebookEntryId=standardPBE.id,Quantity=4, 
                                                       UnitPrice =50);
       insert qliliner;
       qliliner.Outside_Module__c=False;
       update qliliner;
         }
       Test.StopTest();
         }
     
}