@IsTest
private class Pearson_LandingFormControllerTest {
    @isTest
    public static void testSupportWrapper() {
        Pearson_LandingFormController.SupportWrapper testWrapper = new Pearson_LandingFormController.SupportWrapper(Peak_TestConstants.TESTVALUE1, Peak_TestConstants.BOOLVALUE1, Peak_TestConstants.BOOLVALUE2, Peak_TestConstants.TESTVALUE2, Peak_TestConstants.TESTVALUE3);
        String testString = testWrapper.url + testWrapper.community + testWrapper.contact + testWrapper.sfdcBusiness + testWrapper.sfdcRole;
        String assertString = Peak_TestConstants.TESTVALUE1 + Peak_TestConstants.BOOLVALUE1 + Peak_TestConstants.BOOLVALUE2 + Peak_TestConstants.TESTVALUE2 + Peak_TestConstants.TESTVALUE3;

        System.assertEquals(assertString, testString);
    }
    @isTest
    public static void testOptionWrapper() {
        Pearson_LandingFormController.OptionWrapper testWrapper = new Pearson_LandingFormController.OptionWrapper(Peak_TestConstants.TESTVALUE1, Peak_TestConstants.TESTVALUE2, Peak_TestConstants.TESTVALUE3);
        String testString = testWrapper.country + testWrapper.languages + testWrapper.roles ;
        String assertString = Peak_TestConstants.TESTVALUE1 + Peak_TestConstants.TESTVALUE2 + Peak_TestConstants.TESTVALUE3;

        System.assertEquals(assertString, testString);
    }
    @isTest
    public static void testGetBasicFields() {
        Support_Landing_Cnty_Lng_Role__c settings = Support_Landing_Cnty_Lng_Role__c.getValues('TestSetting');
        if (settings == null) {
            settings = new Support_Landing_Cnty_Lng_Role__c();
            settings.Name = Peak_TestConstants.SETTINGNAME;
            settings.Landing_Country__c = Peak_TestConstants.TESTVALUE1;
            settings.Landing_Language__c = Peak_TestConstants.TESTVALUE2;
            settings.LandingPage_Role__c = Peak_TestConstants.TESTVALUE3;

            insert settings;
        }
        List<Pearson_LandingFormController.OptionWrapper> testWrappers = Pearson_LandingFormController.getBasicFields();
        String testString = testWrappers[0].country + testWrappers[0].languages + testWrappers[0].roles ;
        String assertString =  Peak_TestConstants.TESTVALUE1 +  Peak_TestConstants.TESTVALUE2 +  Peak_TestConstants.TESTVALUE3;

        System.assertEquals(assertString, testString);
    }
    @isTest
    public static void testGetEducation() {
        Support_Landing_Role_EdType__c settings = Support_Landing_Role_EdType__c.getValues('TestSetting');
        if (settings == null) {
            settings = new Support_Landing_Role_EdType__c();
            settings.Name = Peak_TestConstants.SETTINGNAME;
            settings.Education_Type__c = Peak_TestConstants.TESTVALUE1;
            settings.LP_Country__c = Peak_TestConstants.TESTVALUE2;
            settings.LP_Role__c = Peak_TestConstants.TESTVALUE3;
            insert settings;
        }
        String testResults = Pearson_LandingFormController.getEducation(Peak_TestConstants.TESTVALUE2, Peak_TestConstants.TESTVALUE3);

        System.assertEquals(Peak_TestConstants.TESTVALUE1, testResults);
    }
    @isTest
    public static void testGetProduct() {
        Support_Landing_Website_Type__c settings = Support_Landing_Website_Type__c.getValues('TestSetting');
        if (settings == null) {
            settings = new Support_Landing_Website_Type__c();
            settings.Name = Peak_TestConstants.SETTINGNAME;
            settings.LPW_Product_Category__c = Peak_TestConstants.TESTVALUE1;
            settings.LPW_Country__c = Peak_TestConstants.TESTVALUE2;
            settings.LPW_Role__c = Peak_TestConstants.TESTVALUE3;
            settings.Education_Type__c = Peak_TestConstants.TESTVALUE4;
            insert settings;
        }
        String testResults = Pearson_LandingFormController.getProduct(Peak_TestConstants.TESTVALUE2, Peak_TestConstants.TESTVALUE4);

        System.assertEquals(Peak_TestConstants.TESTVALUE1, testResults);
    }
    @isTest
    public static void testGetSupportUrlPrinted() {
        List<Support_Website_Address__mdt> testDataList = [SELECT Id, Community_Button__c, Contact_Support_Button__c, Support_URL__c, SFDC_Business_Vertical__c, SFDC_Role__c, Web_Country__c, Education_Type__c, Product_Category__c FROM Support_Website_Address__mdt WHERE MasterLabel = 'TestPrintForApex'];
        Support_Website_Address__mdt testData;
        if (!Peak_Utils.isNullOrEmpty(testDataList)) {
            testData = testDataList[0];
        }
        Pearson_LandingFormController.SupportWrapper testResults = Pearson_LandingFormController.getSupportUrl(testData.Web_Country__c, testData.Education_Type__c, Peak_TestConstants.SUPPORTPRINT);
        String testString = String.valueOf(testResults.community) + String.valueOf(testResults.contact) + testResults.sfdcBusiness + testResults.sfdcRole + testResults.url;
        String assertString = String.valueOf(testData.Community_Button__c) + String.valueOf(testData.Contact_Support_Button__c) + testData.SFDC_Business_Vertical__c + testData.SFDC_Role__c + testData.Support_URL__c;

        System.assertEquals(assertString, testString);
    }
    @isTest
    public static void testGetSupportUrlDigital() {
        List<Support_Website_Address__mdt> testDataList = [SELECT Id, Community_Button__c, Contact_Support_Button__c, Support_URL__c, SFDC_Business_Vertical__c, SFDC_Role__c, Web_Country__c, Education_Type__c, Product_Category__c FROM Support_Website_Address__mdt WHERE MasterLabel = 'TestDigitalForApex'];
        Support_Website_Address__mdt testData;
        if (!Peak_Utils.isNullOrEmpty(testDataList)) {
            testData = testDataList[0];
        }
        Pearson_LandingFormController.SupportWrapper testResults = Pearson_LandingFormController.getSupportUrl(testData.Web_Country__c, testData.Education_Type__c, Peak_TestConstants.SUPPORTDIGITAL);
        String testString = String.valueOf(testResults.community) + String.valueOf(testResults.contact) + testResults.sfdcBusiness + testResults.sfdcRole + testResults.url;
        String assertString = String.valueOf(testData.Community_Button__c) + String.valueOf(testData.Contact_Support_Button__c) + testData.SFDC_Business_Vertical__c + testData.SFDC_Role__c + testData.Support_URL__c;

        System.assertEquals(assertString, testString);
    }
}