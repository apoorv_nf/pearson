@isTest
private class TestEinsteinBotAccessCode {

	@testSetup
	static void setup() {
		SMS_API_Settings__c settings = new SMS_API_Settings__c();
		settings.API_Key__c = 'abcdef';
		settings.Username__c = 'username123';
		settings.Password__c = 'password123';
		settings.SSO_Token__c = 'token1234';

		insert settings;
	}

	@isTest
	static void it_should_verify_access_code() {
		Test.setMock(HttpCalloutMock.class, new SmsApiMock());

		Test.startTest();
		List<EinsteinBotAccessCode.AccessCodeResponse> accessCodeResponses = EinsteinBotAccessCode.accessCodeExpired(new List<String>{'access-code'}); 
		Test.stopTest();

		System.assert(!accessCodeResponses.isEmpty(), 'Access code responses should not be empty');

		System.assertEquals(false, accessCodeResponses[0].valid);
		System.assertEquals(true, accessCodeResponses[0].success);
		System.assertEquals(false, accessCodeResponses[0].replaced);
		System.assertEquals(false, accessCodeResponses[0].expired);
	}

	@isTest
	static void it_should_verify_access_code_when_expired() {
		Test.setMock(HttpCalloutMock.class, new SmsApiMock());

		Test.startTest();
		List<EinsteinBotAccessCode.AccessCodeResponse> accessCodeResponses = EinsteinBotAccessCode.accessCodeExpired(new List<String>{'token_expired'}); 
		Test.stopTest();

		System.assert(!accessCodeResponses.isEmpty(), 'Access code responses should not be empty');
		System.assertEquals(true, accessCodeResponses[0].sessionExpired);
	}
}