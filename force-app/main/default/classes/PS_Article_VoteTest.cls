@isTest
public class PS_Article_VoteTest {

    /* static testMethod void test_getVote(){
        
        Customer_Support__kav article= new Customer_Support__kav(Title = 'Unit Test',UrlName = 'Unit-Test',Language = 'en_US');
        
            insert article;
            Customer_Support__kav a = [SELECT KnowledgeArticleId FROM Customer_Support__kav WHERE Id = :(Id) article.get('id')];
        	KbManagement.PublishingService.publishArticle(a.KnowledgeArticleId, true);
            user caseOwnerUsr = new user();
            User usr;
         
        	usr = new User(LastName = 'happy', firstName='Iam', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science');
        	
        	Integer vType = 1;
        	String title = article.Title;
        	String urlname = article.UrlName;
        	String artId = String.valueOf(article.Id);
        	String comment = 'Test the article vote';
			 System.runAs(usr)
         {
            //Test.StartTest();
            Id childId = PS_Article_Vote.createArticle(vType,artId,urlname,comment);
            Decimal recVote = [SELECT Id, Vote__c FROM Article_Votes__c where Id =: childId].Vote__c;
            System.assertEquals(recVote,vType);
            //Test.StopTest();
      }        
        	
    } */
    static testMethod void test_updVote(){
        
        Customer_Support__kav article= new Customer_Support__kav(Title = 'Unit Test',UrlName = 'Unit-Test',Language = 'en_US');
        
            insert article;
            Customer_Support__kav a = [SELECT KnowledgeArticleId FROM Customer_Support__kav WHERE Id = :(Id) article.get('id')];
        	KbManagement.PublishingService.publishArticle(a.KnowledgeArticleId, true);
            // KbManagement.PublishingService.publishArticle(a.KnowledgeArticleId, true); 
            user caseOwnerUsr = new user();
            User usr;
            
        	usr = new User(LastName = 'happy', firstName='Iam', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science');
        	
        	Integer vType = -1;
        	String title = article.Title;
        	String urlname = article.UrlName;
        	String artId = String.valueOf(article.Id);
        	String comment = 'Test the article vote';
			 System.runAs(usr)
         {
            Test.StartTest();
            Id childId = PS_Article_Vote.createArticle(vType,artId,urlname,comment);
           //Article_Votes__c recVote = [SELECT Id, Vote__c FROM Article_Votes__c where Id =: childId];
            Integer updType = 1;
            String updcomment = 'Test the article vote';
            Id chilId = PS_Article_Vote.updateChild(updcomment,String.valueOf(childId),updType);
            Article_Votes__c aVote = [SELECT Id, Vote__c FROM Article_Votes__c where Id =: chilId];
            if(aVote != null)
			System.assertEquals(aVote.Vote__c,updType);
            Test.StopTest();
      }        
        	
    }
}