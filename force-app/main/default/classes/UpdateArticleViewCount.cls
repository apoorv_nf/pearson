public class UpdateArticleViewCount implements Database.Batchable<SObject>, Database.Stateful {
    String articleId = '';
    String uAViewCountId = '';
    
    Integer limitValue = 0;
    Integer processedRecordCount = 0;
    Integer articleViewCountIndex = 0;
    
    Boolean firstTimeExecute = true;
    
    List<UpdateArticleViewCount__c> uViewCounts;
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        uViewCounts = [SELECT Id, ArticleId__c, MaxViewCount__c, CompletionStatus__c FROM UpdateArticleViewCount__c where CompletionStatus__c != 'Done'];
        Integer recordsLimit = 5000000;
        if(Test.isRunningTest()){
            recordsLimit = 10;
        }

        return Database.getQueryLocator([Select Id from Case limit :recordsLimit]);
    }
    
    public void execute(Database.BatchableContext bc, List<Case> scope){
        if(uViewCounts != null && !uViewCounts.isEmpty() && uViewCounts.size() > articleViewCountIndex){
            loadValues();
            
            for(Case c : scope){
                Customer_Support__kav cs = [SELECT Id, ArticleTotalViewCount FROM Customer_Support__kav where PublishStatus = 'Online' and id =:articleId UPDATE VIEWSTAT];
                if(firstTimeExecute){
                    processedRecordCount = cs.ArticleTotalViewCount;
                    firstTimeExecute = false;
                }
                processedRecordCount++;
                
                System.debug(processedRecordCount+' : '+limitValue);
                if(processedRecordCount == limitValue){
                    System.debug('Updation done for the articleId: '+articleId);
                    updateCompletionStatus();
                    if(uViewCounts.size() <= articleViewCountIndex){
                        firstTimeExecute = true;
                        break;
                    }
                }else if(processedRecordCount > limitValue){
                    articleViewCountIndex++;
                    processedRecordCount = 0;
                    firstTimeExecute = true;
                    if(Test.isRunningTest()){
                        updateCompletionStatus();
                    }
                    break;
                }
            }
        }
    }
    
    private void updateCompletionStatus(){
        if(String.isNotBlank(uAViewCountId)){
            UpdateArticleViewCount__c uAVC = new UpdateArticleViewCount__c();
            uAVC.Id = uAViewCountId;
            uAVC.CompletionStatus__c = 'Done';
            
            update uAVC;
            
            articleViewCountIndex++;
            processedRecordCount = 0;
            
            loadValues();    
        }
    }
    
    private void loadValues(){
        if(uViewCounts.size() > articleViewCountIndex){
            uAViewCountId = uViewCounts[articleViewCountIndex].Id;
            articleId = uViewCounts[articleViewCountIndex].ArticleId__c;
            limitValue = Integer.valueOf(uViewCounts[articleViewCountIndex].MaxViewCount__c.trim());
        }
    }
    
    public void finish(Database.BatchableContext bc){
        
    }
}