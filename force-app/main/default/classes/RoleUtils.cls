/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            RoleUtils.cls 
   Description:     Utillity controller class for getting the Sub Users for Role hierarchy
   Test class:      
   Date             Version              Author                          Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  28-Oct-2016         0.1              Saritha Singamsetty               Getting the Sub Users
  02-Nov-2016         0.2              Manikanta Nagubilli               Updated the utillity class to get the Users and create the tasks
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public with sharing class RoleUtils {
  
  /**
    *Description:this method is used to get the AllSubRoleIds
    *@param:NA
    *@return:RoleIds
    *@throws:NA
    **/
  public static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {

    Set<ID> currentRoleIds = new Set<ID>();
    for(UserRole userRole :[select Id from UserRole where ParentRoleId 
      IN :roleIds AND ParentRoleID != null])
    currentRoleIds.add(userRole.Id);
    return currentRoleIds;

  }
  
  /**
    *Description:this method is used to Create the tasks
    *@param:NA
    *@return:Count of sucess and error records
    *@throws:NA
    **/
  public static String createTask(Set<Id> UserIds,Task taskRecord)
  {
            List<Task> taskList = new List<Task>();
            List<User> userRec = [Select id,Name from user where IsActive = True AND ID IN: UserIds Limit: (Limits.getLimitQueryRows()- Limits.getQueryRows())];
            for (integer i=0;i<userRec.size(); i++) 
            {   
                Task newtaskRecord  = new Task(); 
                User usr = new User();
                usr =  userRec[i];           
                newtaskRecord.OwnerId = usr.id;
                newtaskRecord.Subject = taskRecord.Subject;
                newtaskRecord.ActivityDate = taskRecord.ActivityDate ;
                newtaskRecord.Status = taskRecord.Status;
                newtaskRecord.Priority= taskRecord.Priority;
                newtaskRecord.ReminderDateTime= taskRecord.ReminderDateTime;
                newtaskRecord.Description=taskRecord.Description;
                newtaskRecord.isreminderset=true;
                taskList.add(newtaskRecord);
            }
            Database.SaveResult[] srList = Database.insert(taskList, false);
            Integer sucessCount = 0;
            Integer errorCount = 0; 
            Integer grpErrorCount = 0;                   
            for (Database.SaveResult sr : srList)
            {
                if (sr.isSuccess())
                {
                    sucessCount++;                                           
                }           
                else 
                {   
                     String ErrMsg='';
                     String groupType = Schema.SObjectType.Group.getKeyPrefix();
                     PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                     List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();                     
                     errlogger.InterfaceName__c='MassAgent Task Notification';
                     errlogger.ApexClassName__c='PS_MassAgentNotificationController';
                     errlogger.CallingMethod__c='Execute';
                     errlogger.UserLogin__c=UserInfo.getUserName(); 
                     errlogger.recordid__c=sr.getId();    
                     for(Database.Error err : sr.getErrors()) 
                     {
                         ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage(); 
                         errlogger.ExceptionMessage__c=  ErrMsg;
                         if((errlogger.ExceptionMessage__c.right(18)).startsWithIgnoreCase(groupType)) //RG: for Defect 5507
                              grpErrorCount++;
                          else
                              errorCount++;    
                         errloggerlist.add(errlogger); 
                     } 
                     if (errloggerlist.size()>0)
                     {
                         insert errloggerlist;
                     }                  
                }                    
            }
            string Count;
            if(errorCount != null && errorCount != 0)
            {
                Count = 'error'+','+string.valueof(errorCount);
            }
            else if(sucessCount != null && sucessCount != 0)
            {
                Count = 'Sucess'+','+string.valueof(sucessCount);
            }
            else if(grpErrorCount != null && grpErrorCount != 0)
            {
                Count = 'grpError'+','+string.valueof(grpErrorCount);
            }
            return Count;
  }

}