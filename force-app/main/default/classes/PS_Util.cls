/************************************************************************************************************
* Apex Interface Name : PS_Util
* Version             : 1.0 
* Created Date        : 10 Jul 2015
* Function            : This class holds different utility methods that are used in other class for One CRM.
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Leonard Victor            10/Jul/2015            Initial version
* Kameswari                 09/2015                Updated class to add error methods from list and map using Id
* Kameswari                 09/2015                Updated class to add error methods from list and map using Id
                                                    add method to lock opportunities based on hierarchical custom setting.
                                                    Added new methods to add error based on Id and integer
* Leonard Victor            26/Aug/2015            Added New Methods for logged in user ans profile
* Karthik.A.S               10/May/2016             Added fetchRecordTypesByObject method
* Neuraflash (MM)           7/2/2018                Updated API Version to 43
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/


public without sharing class PS_Util{

    private static Map<Id,Map<String,PermissionSet>> userPermissions;
    public static Boolean isFetchedRecordType = false;
    public static Boolean isFetchedUserRecord = false;
    public static Boolean isProfileFetched = false;
    
    public static User loggedInUser;
    public static Profile loggedInUserProfile;
    
    
    
    public static Map<String, Map<String,ID>> recordTypeMap = new Map<String, Map<String,ID>>();
    
    public static void fetchAllRecordType(){
         if(!isFetchedRecordType){
             Map<String, Schema.SObjectType> sObjectMap = Schema.getGlobalDescribe() ;
             for(String objName : sObjectMap.keySet()){
                    Schema.SObjectType sObjType = sObjectMap.get(objName) ;
                    Schema.DescribeSObjectResult sObjTypeDescribe = sObjType.getDescribe() ;
                    Map<String,Schema.RecordTypeInfo> rtMapByName = sObjTypeDescribe.getRecordTypeInfosByName();
                        for(String recTypeName : rtMapByName.keyset()){
                            if(recordTypeMap.containsKey(String.valueof(sObjectMap.get(objName)))){
                                recordTypeMap.get(String.valueof(sObjectMap.get(objName))).put(recTypeName,rtMapByName.get(recTypeName).getRecordTypeId());
                            }
    
                            else{
                                Map<String,ID> tempMap = new Map<String,ID>();
                                tempMap.put(recTypeName,rtMapByName.get(recTypeName).getRecordTypeId());
                                recordTypeMap.put(String.valueof(sObjectMap.get(objName)),tempMap);
                            }
                        }
             }
             isFetchedRecordType = true;
        }
        
    }



    public static ID fetchRecordTypeByName(SObjectType sObj, String recordTypeName){

        Id recordTypeId;
        recordTypeId = sObj.getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        return recordTypeId;

    }
    
    
    public static void fetchRecordTypesByObject(Schema.SObjectType objectType) {
        if(objectType != NULL) {
        Schema.DescribeSObjectResult objectResult = objectType.getDescribe();
            if(!recordTypeMap.containsKey(objectResult.getName())) {
                recordTypeMap.put(objectResult.getName(), new Map<String,Id>());
                for(Schema.RecordTypeInfo recTypeInfo : objectResult.getRecordTypeInfos()) {
                    recordTypeMap.get(objectResult.getName()).put(recTypeInfo.getName(),recTypeInfo.getRecordTypeId());
                }             
            }
        }
    }


    public static Map<Id,Map<String,PermissionSet>> retrieveUserPermissionSets(List<Id> usersId)
    {
        if(userPermissions == null)
        {
           userPermissions = new Map<Id,Map<String,PermissionSet>>(); 
        }

        List<Id> idToSearch = new  List<Id>();
        for(Id userId : usersId)
        {
            if(! userPermissions.containsKey(userId))
            {
                idToSearch.add(userId);
            }
        }

        if(! idToSearch.isEmpty())
        {

            List<PermissionSetAssignment> perms = [SELECT Id, AssigneeId, PermissionSet.Label FROM PermissionSetAssignment WHERE AssigneeId in :idToSearch];

            for(PermissionSetAssignment perm : perms)
            {
                if( ! userPermissions.containsKey(perm.AssigneeId))
                {
                    userPermissions.put(perm.AssigneeId, new Map<String,PermissionSet>());
                }

                userPermissions.get(perm.AssigneeId).put(perm.PermissionSet.Label,perm.PermissionSet);

            }
        }
      

        return userPermissions;
    }

    public static Boolean hasUserPermissionSet(Id userId, String permissionSetName)
    {
        List<Id> userIds = new  List<Id>();
        userIds.add(userId);

        Map<Id,Map<String,PermissionSet>> permissions = retrieveUserPermissionSets(userIds);
        

        return (permissions.containsKey(userId) && permissions.get(userId).containsKey(permissionSetName));

    }
    //KP:Added method to return true if current login user is Pearson Sales User
    public static Boolean isUserPearsonSalesUser(){
        OneCRMSpecialPermissions__c user=OneCRMSpecialPermissions__c.getInstance(UserInfo.getUserId());
        OneCRMSpecialPermissions__c prof=OneCRMSpecialPermissions__c.getInstance(UserInfo.getProfileId());
        return (user.EditClosedOpportunity__c == true || prof.EditClosedOpportunity__c == true);        
        
    }    

    public static void addErrors(Map<String,SObject> objs, Map<String,List<String>> exceptions)
    {
        for( String id: objs.keyset())
        {
            if(exceptions.containsKey(id))
            {
                String errorMessage = '';

                for(String err: exceptions.get(id))
                {
                    if(!err.containsIgnoreCase('Delete'))
                        errorMessage += err + '</br>';
                    else
                        errorMessage += err;
                }


                objs.get(id).addError(errorMessage, false);
            }
        }
    }
    
    public static void addErrors_1(Map<Id,SObject> objs, Map<String,List<String>> exceptions)
    {
        for( Id id: objs.keyset())
        {
            if(exceptions.containsKey(id))
            {
                String errorMessage = '';

                for(String err: exceptions.get(id))
                {
                    if(!err.containsIgnoreCase('Delete'))
                        errorMessage += err + '</br>';
                    else
                        errorMessage += err;
                }


                objs.get(id).addError(errorMessage, false);
            }
        }
    }   

     public static void addErrorsusinglistindex(List<SObject> objs, Map<String,List<String>> exceptions){
        for (integer i=0;i<objs.size();i++){
            String key=string.valueof(i);
            if(exceptions.containsKey(key)){
                String errorMessage = '';
                for(String err: exceptions.get(key)){
                    if(!err.containsIgnoreCase('Delete'))
                        errorMessage += err + '</br>';
                    else
                        errorMessage += err;
                }
                objs[i].addError(errorMessage, false);
            }
            
        }
    }
    
    public static void getUserDetail(Id usrId)
    {
        if(!isFetchedUserRecord){
            loggedInUser = new User();
            Schema.DescribeSObjectResult dsor = User.getSObjectType().getDescribe();
            String selectSoql= 'Select ';
            Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
            List<String> lstFields = new List<String>();                
            for(String field : objectFields.keyset()){
                selectSoql+= field+','; 
            }
            String idVal = '\''+ usrId+'\'';
            selectSoql= selectSoql.removeEnd(',')+' '+'From User where id ='+idVal;
            loggedInUser = database.query(selectSoql);
            isFetchedUserRecord = true;
        }
    }
    
     public static void getProfileDetail(Id profileId)
    {
        if(!isProfileFetched){
            loggedInUserProfile = new Profile();
            Schema.DescribeSObjectResult dsor = Profile.getSObjectType().getDescribe();
            String selectSoql= 'Select ';
            Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
            List<String> lstFields = new List<String>();                
            for(String field : objectFields.keyset()){
                selectSoql+= field+','; 
            }
            String idVal = '\''+ profileId+'\'';
            selectSoql= selectSoql.removeEnd(',')+' '+'From Profile where id ='+idVal;

            //Neuraflash - If Automated Process user, profile is not visible. So we default to System Administrator
            try {
                loggedInUserProfile = database.query(selectSoql);
            } catch(QueryException ex) {
                String defaultUserName = 'System Administrator';
                selectSoql += ' OR Name = :defaultUserName';
                loggedInUserProfile = database.query(selectSoql);
            }

            isProfileFetched = true;
        }
    }
    
     public static String getDynamicSOQL(SObjectType fldsForObj , Boolean withObject){
        List<String> accessibleFields = new List<String>();
        String combinedFlds = '';
        String finalSOQL = '';
        String objectName = fldsForObj.getDescribe().getName();
        Map<String, Schema.SobjectField> fieldsMap = fldsForObj.getDescribe().fields.getMap();
        for(String objFlds :fieldsMap.keySet()){
            if(fieldsMap.get(objFlds).getDescribe().isAccessible()) {
                accessibleFields.add(objFlds);
            }
        }
        if(!accessibleFields.isEmpty()){
            combinedFlds = joinedFields(accessibleFields , ',');
        }
        if(withObject){
            finalSOQL = 'SELECT ' + combinedFlds + ' FROM '+ objectName +' WHERE ';
        }
        else{
            finalSOQL = 'SELECT ' + combinedFlds;
        }
        return finalSOQL;
    }

    public static String joinedFields(List<String> fldLst, String separator) {
        if (fldLst == null){
            return null; 
        }
        if (separator == null){
            separator = '';
        }
        String joinedFlds = '';
        Boolean firstItem = true;
        for (String item : fldLst) {
            if(item!=null) {
                if(firstItem){
                    firstItem = false;
                }
                else{
                    joinedFlds += separator;
                }
                joinedFlds += item;
         }
     }
     return joinedFlds;
    }



    public static List<String> getWriteableFieldNamesForObject(Schema.sObjectType objectType) {

        List<String> fieldNames = new List<String>();

        Map<String, Schema.SObjectField> fieldMap = objectType.getDescribe().fields.getMap();

        // filter by fields that are writeable and not unique
        for(Schema.SObjectField field : fieldMap.values()) {
            if(field.getDescribe().isUpdateable()) {
                fieldNames.add(field.getDescribe().getName());
            }
        }

        return fieldNames;

    }


    public static List<Schema.FieldSetMember> getFieldSet(SObjectType objectType, String fieldSetName) {

       //List<Schema.FieldSetMember> fieldSet = sObjectType.Order.FieldSets.FieldsEditableAfterOrderSubmission.getFields();
       return null;


    }
         


    public static List<String> getFieldSetAsListOfStrings(List<Schema.FieldSetMember> fieldSet) {

        List<String> fields = new List<String>();

        for(Schema.FieldSetMember field : fieldSet) {
            fields.add(field.getFieldPath());
        }
        return fields;
    }
    



}