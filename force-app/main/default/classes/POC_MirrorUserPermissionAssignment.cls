/*****************************************************************************************************************************/
/*Description:This class is getting called from POC_CreateOnboardingUser_New to insert Permission set and update UserRole

Date             Version              Author                          Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
01-Nov-2019			0.1				 TAMIZHARASAN		 CR-03041   
/*****************************************************************************************************************************/
Public Class POC_MirrorUserPermissionAssignment
{
    @future
    Public static void addPermissionSet(set<Id> userId,Map<Id,Id> permissionSetAssignment)
    {
       List<PermissionSetAssignment> MirrorUserPermissionList=[SELECT AssigneeId,Id,PermissionSetId FROM PermissionSetAssignment where AssigneeId IN:permissionSetAssignment.values()];
       system.debug('==============MirrorUserPermissionList'+MirrorUserPermissionList);
       system.debug('==============Permission set values'+permissionSetAssignment.values());
       
       List<UserPackageLicense> UserPackageLicenseList=[SELECT Id,PackageLicenseId,UserId FROM UserPackageLicense where UserId IN:permissionSetAssignment.values()];
       system.debug('==============UserPackageLicenseList'+UserPackageLicenseList);
        
       List<GroupMember> MirrorUserQueueList=[SELECT UserOrGroupId,Id,GroupId FROM GroupMember where UserOrGroupId IN:permissionSetAssignment.values() and Group.Type != 'queue' ];
       system.debug('==============MirrorUserQueueList'+MirrorUserQueueList);
        
        
       List<User> userList=[select User_Onboarding_Request_Id__c,id from user where id IN:userId]; 
       system.debug('==============userList'+userList);
       
       List<Id> newUserPrmssgnList=new List<Id>();
       for(PermissionSetAssignment prmSetAssgnt:[SELECT AssigneeId,Id,PermissionSetId FROM PermissionSetAssignment where AssigneeId IN:userId])
       {
            newUserPrmssgnList.add(prmSetAssgnt.PermissionSetId);       
       }
       system.debug('==============newUserPrmssgnList'+newUserPrmssgnList); 
        
        List<Id> newUserQueueList=new List<Id>();
       for(GroupMember QueueAssgnt:[SELECT UserOrGroupId,Id,GroupId FROM GroupMember where UserOrGroupId IN:userId and Group.Type != 'queue'])
       {
            newUserQueueList.add(QueueAssgnt.GroupId);       
       }
       system.debug('==============newUserQueueList'+newUserQueueList);
        
        
      // Mirror user Id and its list of permission set Ids
       Map<Id,List<Id>> permissionSetMap=new Map<Id,List<Id>>();      
       
        // Mirror user Id and its list of package license Ids
       Map<Id,List<Id>> packageLicenseIdMap=new Map<Id,List<Id>>();
        
        // Mirror user Id and its list of Queue Ids
       Map<Id,List<Id>> QueueSetMap=new Map<Id,List<Id>>();
       
       
       for(PermissionSetAssignment prmSet:MirrorUserPermissionList)
       {
           if(permissionSetMap.containsKey(prmSet.AssigneeId))             
           {               
               permissionSetMap.get(prmSet.AssigneeId).add(prmSet.PermissionSetId);                  
           }
           else
           {
               permissionSetMap.put(prmSet.AssigneeId,new List<Id>{prmSet.PermissionSetId}); 
           }
       }
                        
       for(UserPackageLicense UsrPckgLcns:UserPackageLicenseList)
       {
           if(packageLicenseIdMap.containsKey(UsrPckgLcns.UserId))
           {
               packageLicenseIdMap.get(UsrPckgLcns.UserId).add(UsrPckgLcns.PackageLicenseId);
           }
           else
           {
               packageLicenseIdMap.put(UsrPckgLcns.UserId,new List<Id>{UsrPckgLcns.PackageLicenseId}); 
           }
       
       }
        
        for(GroupMember GroupSet:MirrorUserQueueList)
       {
           if(QueueSetMap.containsKey(GroupSet.UserOrGroupId))
           {
               QueueSetMap.get(GroupSet.UserOrGroupId).add(GroupSet.GroupId);
           }
           else
           {
               QueueSetMap.put(GroupSet.UserOrGroupId,new List<Id>{GroupSet.GroupId}); 
           }
       }
       
       List<PermissionSetAssignment> permissionSetToBeInserted=new List<PermissionSetAssignment>();
       List<UserPackageLicense> packagesToBeInserted=new List<UserPackageLicense>();
       List<GroupMember> QueueToBeInserted=new List<GroupMember>();
        
       for(User newlyCreatedUser:userList)
       {
           
           if(permissionSetAssignment.containsKey(newlyCreatedUser.User_Onboarding_Request_Id__c))
           {
               
               Id mirrorUSerId=permissionSetAssignment.get(newlyCreatedUser.User_Onboarding_Request_Id__c);
               if(permissionSetMap.containsKey(mirrorUSerId))
               {
                   for(Id permissionSetId:permissionSetMap.get(mirrorUSerId))
                   {
                       system.debug('=============permission set'+permissionSetId);
                        if(!newUserPrmssgnList.contains(permissionSetId))
                        {
                            system.debug('=============permission set not exist'+permissionSetId);
                           PermissionSetAssignment objprmssnSetAssnmt=new PermissionSetAssignment();
                           objprmssnSetAssnmt.AssigneeId=newlyCreatedUser.id;
                           objprmssnSetAssnmt.PermissionSetId=permissionSetId;
                           permissionSetToBeInserted.add(objprmssnSetAssnmt);
                        }
                   }
               }
               if(packageLicenseIdMap.containsKey(mirrorUSerId))
               {
                    for(id LicenseId:packageLicenseIdMap.get(mirrorUSerId))
                    {
                        UserPackageLicense usrPckgLcns=new UserPackageLicense();
                        usrPckgLcns.PackageLicenseId=LicenseId;    
                        usrPckgLcns.UserId=newlyCreatedUser.id;
                        packagesToBeInserted.add(usrPckgLcns);
                    }   
               
               }
               if(QueueSetMap.containsKey(mirrorUSerId))
               {
                   for(Id QueueSetId:QueueSetMap.get(mirrorUSerId))
                   {
                       system.debug('=============Queue'+QueueSetId);
                        if(!newUserQueueList.contains(QueueSetId))
                        {
                            system.debug('=============Queue set not exist'+QueueSetId);
                           GroupMember objQueueSetAssnmt=new GroupMember();
                           objQueueSetAssnmt.UserOrGroupId=newlyCreatedUser.id;
                           objQueueSetAssnmt.GroupId=QueueSetId;
                           QueueToBeInserted.add(objQueueSetAssnmt);
                        }
                   }
               }
   
           }
           if(permissionSetToBeInserted.size()>0)
           insert permissionSetToBeInserted;
           
           if(packagesToBeInserted.size()>0)
           insert packagesToBeInserted;
           
           if(QueueToBeInserted.size()>0)
           insert QueueToBeInserted;
           
           
       }
    
    }
    
    @future
    Public static void Addrole(set<Id> userId,Map<Id,Id> RoleAssignment){
       id AddUserRole =[SELECT UserRoleId FROM User where Id IN:RoleAssignment.values()].UserRoleId; // get mirror user role
       list<User> usr = [select id, UserRoleId  from user where id IN: userId];
       
        for(user u: usr){
            u.userroleId = AddUserRole;
        }
        
        update usr;
        
    }
    
}