/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_RollOverCheckBoxUpdate.cls 
Description:     On insert/update/ of Asset and Product  record 
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
01/019/2018          1.0          Mayank Lal         To update the Rollover checkbox when the asset is created checks the product
family Nextedition record Relevance value is 10 or 30 also when the Next Edition 
product is updated to 10 or 30 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */

public class PS_RollOverCheckBoxUpdate {
    
    public static void Rolloverchkd(List<Asset> newRecords)
    {
        Map<Id,Boolean> mapOfProdAndRollOver = new map<Id,Boolean>();
        Map<Id,Decimal> mapofProdandRelevanceValue = new Map<Id,Decimal>();
        //Set<Id> setProdIds = new Set<Id>();
        Set<Id> setOfProdFamilyIds = new Set<Id>();
        Map<Id,Asset> mapOfProdIdFromAsst = new Map<Id,Asset>();
        List<RelatedProduct__c> lstWithRelProdFamily = new List<RelatedProduct__c>();
        List<RelatedProduct__c> lstWithProdFamily = new List<RelatedProduct__c>();
        Map<Id,RelatedProduct__c> mapOfAssetandRelatedProdFam = new Map<Id,RelatedProduct__c>();
        Set<Id> setofAssetProdIdWithOutProductFamily = new Set<Id>();
        //Get Product Ids from PIUS's
        for(Asset Asst: newRecords)
        {
            //setProdIds.add(Asst.product2Id);
            //Added if as part of INC4088932
            if(Asst.Product2Id != null){
            	mapOfProdIdFromAsst.put(Asst.Product2Id,Asst);    
            }
        }
        
        //Get Product Families
        lstWithRelProdFamily = [select id,name,Product__c,RelatedProduct__c from RelatedProduct__c 
                                where  AssociationCategory__c != null and
                                RelatedProduct__c IN :  mapOfProdIdFromAsst.keySet()];
        
        for(RelatedProduct__c relPrds : lstWithRelProdFamily) {
            if(relPrds.RelatedProduct__c != null) {
                mapOfAssetandRelatedProdFam.put(relPrds.RelatedProduct__c, relPrds);    
            }
        }
        For(Id asstProdId : mapOfProdIdFromAsst.keySet()) {
            if(mapOfAssetandRelatedProdFam.containsKey(asstProdId)){
                lstWithProdFamily.add(mapOfAssetandRelatedProdFam.get(asstProdId));
            }
            else 
            {
                setofAssetProdIdWithOutProductFamily.add(asstProdId);
            }
        }
        if(!lstWithProdFamily.isEmpty()) {
            // Get Product Family Id from Products
            for(RelatedProduct__c prodFamily: lstWithProdFamily)
            {
                setOfProdFamilyIds.add(prodFamily.Product__c);
            }  
            
            //Get Next Edition Product from Product Family and Check for Relevance value 10 or 30
            List<Product2> lstNxtEditionProducts = [select id,Next_Edition__r.Relevance_Value__c from  product2 
                                                    where Id IN :setOfProdFamilyIds];
            
            for(RelatedProduct__c prodFam: lstWithProdFamily)
            {            
                for(Product2 Prod :lstNxtEditionProducts)
                {            
                    if(prodFam.Product__c == Prod.Id && (Prod.Next_Edition__r.Relevance_Value__c == 10 || Prod.Next_Edition__r.Relevance_Value__c == 30))
                    {                    
                        mapOfProdAndRollOver.put(prodFam.RelatedProduct__c, True);
                        mapofProdandRelevanceValue.put(prodFam.RelatedProduct__c, Prod.Next_Edition__r.Relevance_Value__c);
                    }
                    else if(prodFam.Product__c == Prod.Id && (Prod.Next_Edition__r.Relevance_Value__c != 10 || Prod.Next_Edition__r.Relevance_Value__c != 30))
                    {                    
                        mapOfProdAndRollOver.put(prodFam.RelatedProduct__c, False);
                        mapofProdandRelevanceValue.put(prodFam.RelatedProduct__c, Prod.Next_Edition__r.Relevance_Value__c);
                    }
                    else if(prodFam.Product__c == Prod.Id && Prod.Next_Edition__c == null) {
                        mapOfProdAndRollOver.put(prodFam.RelatedProduct__c, False);
                    }
                }
            }
        }
        if(!setofAssetProdIdWithOutProductFamily.isEmpty()) {
            //Get List of Next Edition Products
            List<Product2> lstNextEditionProd = [select id,Next_Edition__r.Relevance_Value__c,Next_Edition__c 
                                                 from  product2 
                                                 Where Id IN :setofAssetProdIdWithOutProductFamily];
            if(!lstNextEditionProd.isEmpty()) {
                for(Product2 nxtEdiProd :lstNextEditionProd) {
                    //Check for Next Edition and Relevance Value 10/30
                    if((nxtEdiProd.Next_Edition__r.Relevance_Value__c == 10 || nxtEdiProd.Next_Edition__r.Relevance_Value__c == 30) && nxtEdiProd.Next_Edition__c != null) {
                        mapOfProdAndRollOver.put(nxtEdiProd.id, true);
                        mapofProdandRelevanceValue.put(nxtEdiProd.id, nxtEdiProd.Next_Edition__r.Relevance_Value__c);
                    }
                    //Check for Next Edition and Relevance Value NOT 10/30
                    else if((nxtEdiProd.Next_Edition__r.Relevance_Value__c != 10 || nxtEdiProd.Next_Edition__r.Relevance_Value__c != 30) && nxtEdiProd.Next_Edition__c != null) {
                        mapOfProdAndRollOver.put(nxtEdiProd.id, false);   
                        mapofProdandRelevanceValue.put(nxtEdiProd.id, nxtEdiProd.Next_Edition__r.Relevance_Value__c);
                    }
                    //Check for Next Edition Null
                    else if(nxtEdiProd.Next_Edition__c == null){
                        mapOfProdAndRollOver.put(nxtEdiProd.id, false); 
                        //mapofProdandRelevanceValue.put(nxtEdiProd.id, nxtEdiProd.Next_Edition__r.Relevance_Value__c);
                    }
                }
            }
        }   
        
        for(Asset Asst2 : newRecords)
        {              
            if (mapOfProdAndRollOver.containsKey(Asst2.Product2Id)) 
            {
                if (mapOfProdAndRollOver.get(Asst2.Product2Id))
                {
                    Asst2.Rollover2__c = mapOfProdAndRollOver.get(Asst2.Product2Id);
                    Asst2.Relevance_Value__c = mapofProdandRelevanceValue.get(Asst2.Product2Id);
                }  
                else {
                    Asst2.Rollover2__c = mapOfProdAndRollOver.get(Asst2.Product2Id);   
                    Asst2.Relevance_Value__c = mapofProdandRelevanceValue.get(Asst2.Product2Id);
                }
            }                                  
        }
        
    }
    
    // Method to check Rollover when Product Relevance is updated to 10 or 30 and unchecks Rollover 
    // when It is other than 10 or 30 
    
    public static void Rolloverchkdfromprod(List<product2> newrecords,map<Id,product2> oldprods)
    {
        Set<Id> relProdIds = new Set<Id>();
        Set<Id> irrProdIds = new Set<Id>();
        Set<Id> prFam = new Set<Id>();
        Map<Id,Asset> mapOfAssetTobeUpdated = new map<Id,Asset>();
        map<Id,List<RelatedProduct__c>> mapOfProdIdandRelatedProducts = new map<Id,List<RelatedProduct__c>>();
        
        for(product2 prod: newrecords)
        {
            if(oldprods.get(prod.id).Relevance_Value__c != prod.Relevance_Value__c)
            {
                if(prod.Relevance_Value__c == 10 ||prod.Relevance_Value__c == 30 )
                {
                    relProdIds.add(prod.id);  
                }
                else if(prod.Relevance_Value__c != 10 || prod.Relevance_Value__c != 30)
                {
                    irrProdIds.add(prod.id);                       
                }
            }
        }
        if(!relProdIds.isEmpty()){
            List<product2> prodFamily = [select id,(select id,name,Product__c,RelatedProduct__c,AssociationCategory__c from BaseProducts__r) from  product2 where Next_Edition__c !=NULL and Next_Edition__c IN :relProdIds];  //Next Edition prod
            for(product2 pr:prodfamily)
            {
                prFam.add(pr.id);
                mapOfProdIdandRelatedProducts.put(pr.id, pr.BaseProducts__r);
            }
            
            List<RelatedProduct__c> lstrelatedProduct = [select id,name,Product__c,RelatedProduct__c 
                                                         from RelatedProduct__c 
                                                         where AssociationCategory__c != null 
                                                         and Product__c IN :prFam];
            
            Set<Id> OrgProdIds = new Set<Id>();
            //Addded By Mayank on 02-22-2018 after UAT Defect for CR-415 
            for(Id productId : mapOfProdIdandRelatedProducts.keySet()) {
                if(mapOfProdIdandRelatedProducts.get(productId) == null || mapOfProdIdandRelatedProducts.get(productId).isEmpty()) {
                    OrgProdIds.add(productId);
                } 
            }
            //Addded By Mayank on 02-22-2018 after UAT Defect for CR-415  
            
            for(RelatedProduct__c relProduct : lstrelatedProduct)  {
                if(relProduct.RelatedProduct__c != null) {
                    OrgProdIds.add(relProduct.RelatedProduct__c);
                }
            }
            List<Asset> lstProdInUse = [Select Id,product2Id,AccountId,Course__r.Account__c From Asset where Product2Id IN :OrgProdIds LIMIT 1000];
            
            for(Asset Ast:lstProdInUse)
            {
                // If condition to Bypass Lookup filter present on Course Object
                //if(Ast.AccountId == Ast.Course__r.Account__c) {
                Ast.Rollover2__c = True;
                mapOfAssetTobeUpdated.put(Ast.id,Ast);    
                //}
                
            }    
        }
        
        //To uncheck the field when the Relevance value of the is not 10 or 30
        
        Set<Id> setIrrProdIds = new Set<Id>();
        if(!irrProdIds.isEmpty()){
            List<product2> lstIrelProdFamily = [select id,(select id,name,Product__c,RelatedProduct__c,AssociationCategory__c from BaseProducts__r) from  product2 where Next_Edition__c !=NULL and Next_Edition__c IN :irrProdIds];//family
            for(product2 prod: lstIrelProdFamily)
            {
                setIrrProdIds.add(prod.id);
                mapOfProdIdandRelatedProducts.put(prod.id, prod.BaseProducts__r);
            }
            
            List<RelatedProduct__c> lstIRrelatedProduct = [select id,name,Product__c,RelatedProduct__c 
                                                           from RelatedProduct__c 
                                                           where AssociationCategory__c != null 
                                                           and Product__c IN :setIrrProdIds];
            Set<Id> OrgIRProdIds = new Set<Id>();
            //Addded By Mayank on 02-22-2018 after UAT Defect for CR-415 
            for(Id productId : mapOfProdIdandRelatedProducts.keySet()) {
                if(mapOfProdIdandRelatedProducts.get(productId) == null || mapOfProdIdandRelatedProducts.get(productId).isEmpty()) {
                    OrgIRProdIds.add(productId);
                } 
            }
            //Addded By Mayank on 02-22-2018 after UAT Defect for CR-415 
            for(RelatedProduct__c IRrelProduct : lstIRrelatedProduct)  {
                if(IRrelProduct.RelatedProduct__c != null) {
                    OrgIRProdIds.add(IRrelProduct.RelatedProduct__c);
                }
            }
            List<Asset> IRPIU = [Select Id,product2Id,AccountId,Course__r.Account__c From Asset where Product2Id IN :OrgIRProdIds LIMIT 1000];   
            for(Asset Asts:IRPIU)
            {
                // If condition to Bypass Lookup filter present on Course Object
                //if(Asts.AccountId == Asts.Course__r.Account__c) {
                Asts.Rollover2__c = False;
                mapOfAssetTobeUpdated.put(Asts.id,Asts);
                //}  
            }
        }
        update  mapOfAssetTobeUpdated.values();   
}
    }