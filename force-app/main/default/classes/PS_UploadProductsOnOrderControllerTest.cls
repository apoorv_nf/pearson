@isTest (seeAllData=true)
public class PS_UploadProductsOnOrderControllerTest {
    
    static testMethod void PS_UploadProductsOnOrderMethod(){
        
        Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
        User u = new User();
        u.LastName = 'territoryuser';
        u.alias = 'terrusr'; 
        u.Email = 'territoryuser@pearson.com';  
        u.Username='territoryuser@pearson.com';
        u.LanguageLocaleKey='en_US'; 
        u.TimeZoneSidKey='America/New_York';
        u.Price_List__c='US HE All';
        u.LocaleSidKey='en_US';
        u.EmailEncodingKey='ISO-8859-1';
        u.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
        u.Geography__c = 'Growth';
        u.CurrencyIsoCode='USD';
        u.Line_of_Business__c = 'Higher Ed';
        u.Market__c = 'US';
        u.Business_Unit__c = 'US Field Sales';
        u.License_Pools__c = 'test';
        insert u;
        
        //GK
        Bypass_Settings__c settings = new Bypass_Settings__c();
        settings.Disable_Validation_Rules__c = true;
        settings.SetupOwnerId = u.id;
        insert settings;
        
        Account acc1 = new Account();   
        acc1.Name='Test Account1'; 
        acc1.Organisation_Type__c ='School';
        acc1.Phone='+9100000'; 
        acc1.ShippingCountry = 'India'; 
        acc1.ShippingCity = 'Bangalore1';
        acc1.ShippingStreet = 'BNG1';
        acc1.ShippingPostalCode = '560031';
        acc1.market2__c = 'US';
        acc1.Primary_Selling_Account_check__c = false;
        acc1.Sampling_Account_Number__c='';
        
        System.runAs(u) {   
            insert acc1;
        }
        Opportunity opp = (Opportunity)TestClassAutomation.createSObject('Opportunity');
        opp.AccountId = acc1.Id;
        opp.CurrencyIsoCode = 'GBP';
        opp.Lost_Reason_Other__c = 'XXX';
        opp.Academic_Vetting_Status__c = 'XXXXX';
        //opp.Qualification_Campus__c = acc1.Id;
        opp.Type = 'New Business';
        opp.StageName = 'Negotiation';
        System.runAs(u) {  
            insert opp;
        }    
        Product2 sProduct = (Product2)TestClassAutomation.createSObject('Product2');
        sProduct.ISBN__c = '123456 IS1';
        System.runAs(u) { 
            insert sProduct;
        }    
        Id standard1 = Test.getStandardPricebookId();   
        
        PriceBookEntry sPriceBookEntry1      = (PriceBookEntry)TestClassAutomation.createSObject('PriceBookEntry');
        sPriceBookEntry1.IsActive        = true;
        sPriceBookEntry1.Product2Id        = sProduct.Id;
        //sPriceBookEntry1.Pricebook2Id      = standard1.Id;
        sPriceBookEntry1.Pricebook2Id      = standard1;
        sPriceBookEntry1.UnitPrice        = 34.95;
        sPriceBookEntry1.CurrencyIsoCode  = 'GBP';
        System.runAs(u) { 
        insert sPriceBookEntry1;
        }
        order sampleorder = new order();
        sampleorder.OpportunityId=opp.Id;
        sampleorder.Accountid = acc1.Id;
        sampleorder.EffectiveDate = system.today();
        sampleorder.status = 'Open';
        //  sampleorder.Pricebook2Id = standard1.id;
        sampleorder.Pricebook2Id = standard1;
        sampleorder.CurrencyIsoCode = 'GBP';
        sampleorder.Packing_Instructions__c = 'Packing Instructions';
        sampleorder.Shipping_Instructions__c = 'Shipping instructions';
        sampleorder.Salesperson__c = u.Id;
        sampleorder.ShippingCity = 'Patna';
        sampleorder.ShippingCountry = 'India';
        sampleorder.ShippingState = 'Bihar';
        sampleorder.ShippingStreet = 'Test';
        sampleorder.ShippingPostalCode = '1111';
        sampleorder.Line_of_Business__c = 'Higher Ed';
        sampleorder.Geography__c = 'Growth';
        //GK
        sampleorder.RecordTypeId = PS_Util.fetchRecordTypeByName(Order.SobjectType,'Global Sample Order');
        System.runAs(u) { 
        insert sampleorder;
       // }
        /*      
Apexpages.Pagereference pg1 = new Apexpages.pagereference('/apex/PS_UploadProductsOnOrder');
ApexPages.currentPage().put('id',sampleorder.id);
//   Document doc =[Select Id,Name,Body from Document where Name='OrderItems'];
*/      
        Test.startTest();    
        PageReference pageRef = Page.PS_UploadProductsOnOrder; // Add your VF page Name here
        pageRef.getParameters().put('id', sampleorder.id);
        Test.setCurrentPage(pageRef);
        
        String csvFile = '123456 IS1,2\n';
        csvFile+='123456 IS1,2\n';
        Blob csvBlob = Blob.valueOf(csvFile);
        
        PS_UploadProductsOnOrderController upc = new PS_UploadProductsOnOrderController();
        //  upc.csvFileBody=doc.Body;
        upc.csvFileBody=csvBlob;
        upc.readFromFile();
        
        //GK
        sampleorder.RecordTypeId = PS_Util.fetchRecordTypeByName(Order.SobjectType,'Sample Order');
        update sampleorder;
        upc.readFromFile();
        Test.stopTest();    
            
     }
        
    }
}