@isTest(SeeAllData=false)
private class OpptyPrimaryContactBatchTest
{
    @isTest static void OpptyPrimaryContact_BatchTest() {
        Account acc = new Account(Name = 'AccTest1',Market2__c='UK',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
        insert acc;
        
        Contact con = new Contact(FirstName='karthik',LastName ='A.S',Phone='9999888898',Email='test@gmail.com', AccountId = acc.Id);
        insert con;
        
        UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true,Fall_Enrollment__c=10);
        insert UnivCourse;
       
        
        String OptyRecordTypeId;                        
        
        List<RecordType> RecordTypeList = new List<RecordType>([select Id,Name,SobjectType from RecordType]);
        
        for (RecordType i : RecordTypeList) {
          
          if (i.SobjectType== 'Opportunity') {
            OptyRecordTypeId = i.Id;
          }                    
        }
       
        opportunity opp = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=system.today(), RecordTypeID = OptyRecordTypeId);         
        insert opp;        

        UniversityCourseContact__c uni = new UniversityCourseContact__c(UniversityCourse__c=UnivCourse.id,Contact__c = con.Id);
        insert uni;
        
        PS_OpptyPrimaryContactBatch__c lastbatch = new PS_OpptyPrimaryContactBatch__c(Name = 'Last Batch' , LastBatchRunTime__c = System.Now().addDays(-1));
        insert lastbatch;
        
        OpportunityContactRole opprole = new OpportunityContactRole(ContactId = con.id, IsPrimary = true, OpportunityId = opp.id, Role = 'Business User');
        insert opprole;
         
        test.StartTest();
            Id BatchID = Database.executeBatch(new OpptyPrimaryContact());
        test.StopTest();
    }
    
    @isTest static void Test_OpptyPrimaryContactScheduler() {
        
         Account acc = new Account(Name = 'AccTest1',Market2__c='UK',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
        insert acc;
        
        Contact con = new Contact(FirstName='karthik',LastName ='A.S',Phone='9999888898',Email='test@gmail.com', AccountId = acc.Id);
        insert con;
        
        UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true,Fall_Enrollment__c=10);
        insert UnivCourse;
       
        String OptyRecordTypeId;                        
        
        List<RecordType> RecordTypeList = new List<RecordType>([select Id,Name,SobjectType from RecordType]);
        
        for (RecordType i : RecordTypeList) {
          
          if (i.SobjectType== 'Opportunity') {
            OptyRecordTypeId = i.Id;
          }                    
        }
       
        opportunity opp = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=system.today(), RecordTypeID = OptyRecordTypeId);         
        insert opp;       

        UniversityCourseContact__c uni = new UniversityCourseContact__c(UniversityCourse__c=UnivCourse.id,Contact__c = con.Id);
        insert uni;
        
        PS_OpptyPrimaryContactBatch__c lastbatch = new PS_OpptyPrimaryContactBatch__c(Name = 'Last Batch' , LastBatchRunTime__c = System.Now().addDays(-1));
        insert lastbatch;
        
        OpportunityContactRole opprole = new OpportunityContactRole(ContactId = con.id, IsPrimary = true, OpportunityId = opp.id, Role = 'Business User');
        insert opprole;
        
        General_One_CRM_Settings__c  setting = new General_One_CRM_Settings__c(Name = 'OpptyPrimaryContactBatchSize', Value__c = '20', Description__c = 'BatchSize for OpptyPrimaryContact', Category__c = 'BatchSize for OpptyPrimaryContact');
        insert setting;     
        
            Test.StartTest();
            OpptyPrimaryContactScheduler sh1 = new OpptyPrimaryContactScheduler();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Update Primary Contact', sch, sh1); 
            Test.stopTest(); 
    }
    
}