/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_Case_TriggerSequenceCtrlr.cls 
Description:     On insert/update/delete of Case record 
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
03/2015         1.0            Manikandan                       Initial Release 
10/2015         1.1            Rahul Boinepally       Amended not to call PS_INT_IntegrationRequestController method if Supress_integration flag is True
21/12/2015      1.2            Rahul Boinepally       1. Call PS_UserFieldUtill class to fetch the logged in user's Market, Line of Business, Geography
and Business unit and store them in the corresponding Case record.
2. Split the method 'before insert or update' to 'before insert' and 'before update'.
17/02/2016     1.3            KP                       R4 code merge                                             
17/02/2016     1.4            Rohit Kulkarni          RD-1563 method mActivateAssignmentRule is called from 'before insert' and 'before update'.
09/03/2016     1.5            AJAY                    RD-1600 Updated case fcr field status on case closure calling classes.  
3/17/2016      1.5            Rashmi                  Updated Afterupdate method for R4 code 
15/4/2016      1.7            Sudhakar Navuluri       to create AccountContact Records   
20/05/2016     1.8            Karthik.A.S             To call casesubmissionValidation method  on PS_INT_IntegrationRequestController   
12/7/2016      1.8            Payal Popat             R6 - Updated AfterInsert and AfterUpdate to fire CaseOwnerUpdateFlow 
20/07/2016     1.9            RG                      Updated code for DR-0774
26/7/2016      1.9            Payal Popat             R6- Updated BeforeInsert event to add condition for calling PS_UserFieldUtill  class                            
December,08, 2016             Jaydip B                Added logic to update ACR With Cases flag   
March 14, 2017 2.0            Saritha Singamsetty    Added new method CaseFieldsUpdate to populate market etc field for Email2Case
Jun 14,2017                   Abraham Daniel         Added new method creatingNewAccountContact, to create new account and contact record for customer service cases
September 07,2017             Abraham Daniel         Removed reference to check spam.
October 23,2017               Charitha Seelam        Added New method "updateCaseFieldsAtSurveyInvitation" for updating the fields Case Owner & Case Owner Manager At Survey Invitation for all markets when a survey invitation is sent and response not recieved
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class PS_Case_TriggerSequenceCtrlr {
    
    public  static Map<Id,String> emailMap = new Map<Id,String>();
    
    
    /**
* Description : Performing all  Before Update Operations - R4 as signatures are different with existing one
* @param NA
* @return NA
* @throws NA
**/
    public static void beforeUpdate(List<case> newCases, map<id,case> oldmap, map<id,case> newCaseMap){        
        // Below method call's has been coied from 2nd beforeUpdate(List<Case> triggeredcases) 
        // And commented the 2nd beforeUpdate(List<Case> triggeredcases) as part of CR-00018 by Raja Gopalarao B
        //Start of CR-00018
        PS_INT_IntegrationRequestController.submissionValidation(newCases);
        CloseCase.cannotCloseCase(newCases);
        // 01-05-2015 - Requirements 1177/ 1174
        // smark
        // Update the student and sponsor email fields in the case object
        UpdateEmailFieldsCase.populateEmailFieldsCase(newCases);
        
        PS_Case_D2L_Management.assignAccountToNewSponsor(newCases);  
        
        // change 1.2 starts        
        
        PS_UpdateOptOutPreferences.updateCaseStatus(newCases);
        
        // change 1.2 ends
        //R4 code merge
       // PS_caseEntitlement.UpdateCaseEntitlement(newCases);
        PS_CaseBusinessHour.mPopulateBusinessHour(newCases);
        //End of CR-00018
        //PS_UpdateCaseOwnerTracking.mEmptyRepliedtimeCase(newCases,oldmap,newCaseMap);
        PS_CaseBusinessHour.mCalculateInitialResponseTime(newCases,oldmap,newCaseMap);//Invoking to calculate Email Initial Response Time      
        PS_CaseUpdateParentCaseCtrl.mParentCaseValidation(newCases,newCaseMap,false,true,oldmap);//child case creation validation - RD-1545
        
        
        //PS_CaseTriggerOperations.mInsertAccountOnCaseUpdate(newCases);
        PS_ServiceNowIntegration.mUpdateCaseStatuswithEscalationStatus(newCases,oldmap,newCaseMap);//update case status and ownership - RD-01648
        PS_CaseFCRUpdate.mSetFCROnCaseClosure(newCases,false,true,oldmap);
        PS_Case_TriggerSequenceCtrlr.updateCaseFieldsAtSurveyInvitation(newCases,oldmap,newCaseMap);
        
    }
    
    /**
* Description : Performing all  before Insert operation -R4 as signature is different with existing one
* @param NA
* @return NA
* @throws NA
**/
    public static void beforeInsert(List<Case> triggeredcases,map<id,case> newCaseMap){ 
        PS_CaseUpdateParentCaseCtrl.mParentCaseValidation(triggeredcases,newCaseMap,true,false,null);
 //       PS_EmailtoCaseSpamPrevention.spamcheck(triggeredcases);         **Abraham** Spam Check removed as part of AugCR-01547,will be handled by proofpoint
        PS_INT_IntegrationRequestController.submissionValidation(triggeredcases);
    }
    
    /**
* Description : Performing all  before Insert operation
* @param NA
* @return NA
* @throws NA
**/
    
    public static void beforeInsert(List<Case> triggeredcases)
    {
        
        
        CloseCase.cannotCloseCase(triggeredcases);
        // 01-05-2015 - Requirements 1177/ 1174
        // smark
        // Update the student and sponsor email fields in the case object
        UpdateEmailFieldsCase.populateEmailFieldsCase(triggeredcases);
        
        PS_Case_D2L_Management.assignAccountToNewSponsor(triggeredcases);
        
        PS_UserFieldUtill.setUserFieldValues(triggeredcases);     
        
        // change 1.2 starts
        PS_HashingUtility.extractEmailHash(triggeredcases);
        // change 1.2 ends
        //R4 code merge
        PS_CaseFCRUpdate.mSetFCROnCaseClosure(triggeredcases,true,false,null);
        PS_caseEntitlement.UpdateCaseEntitlement(triggeredcases);
        PS_CaseBusinessHour.mPopulateBusinessHour(triggeredcases); 
        //Asha:5/8/2019 -CR-02478 Hybris changes begin
        List<Case> hybrisCaseList =new List<Case>();
        Set<string> suppliedMDMId = new Set<string>();  
        Set<string> suppliedEmail = new Set<string>();
        Boolean isSuppliedMDMIDNull; // Added By priya for CR-02933 Req 3
      
        for(Case caseRec:triggeredcases){
            System.debug('caseRec.SuppliedEmail' +caseRec.SuppliedEmail);
             System.debug('caseRec.Supplied_MDM_ID__c' +caseRec.Supplied_MDM_ID__c);
              System.debug('caseRec.origin:::::::' +caseRec.origin);
            if(caseRec.origin=='Hybris' && caseRec.Supplied_MDM_ID__c !=null && caseRec.SuppliedEmail !=null){
                suppliedMDMId.add(caseRec.Supplied_MDM_ID__c); 
                suppliedEmail.add(caseRec.SuppliedEmail);
                hybrisCaseList.add(caseRec);
                isSuppliedMDMIDNull=false;
            }
            // Added By priya for CR-02933 Req 3
           if(caseRec.origin=='Hybris' && caseRec.Supplied_MDM_ID__c ==null && caseRec.SuppliedEmail !=null){
                suppliedMDMId.add(caseRec.Supplied_MDM_ID__c); 
                suppliedEmail.add(caseRec.SuppliedEmail);
                hybrisCaseList.add(caseRec);
                isSuppliedMDMIDNull=true;
            }
        } 
        // PS_HybrisCase.hybrisCaseCreation(hybrisCaseList,suppliedMDMId,suppliedEmail); //Commented by Priya for CR-02933 Req 3
         PS_HybrisCase.hybrisCaseCreation(hybrisCaseList,suppliedMDMId,suppliedEmail,isSuppliedMDMIDNull); // Added by priya for CR-02933 Req 3
        //Asha:5/8/2019 -CR-02478 Hybris changes End
    }
    /**
* Description : Performing all  Before Update Operations
* @param NA
* @return NA
* @throws NA
**/
    //Below method has been commented as part of CR-00018 by Raja Gopalarao B 
    //And same we are calling from 1st beforeUpdate(List<case> newCases, map<id,case> oldmap, map<id,case> newCaseMap)
   /* public static void beforeUpdate(List<Case> triggeredcases)
    {
        
        
        PS_INT_IntegrationRequestController.submissionValidation(triggeredcases);
        CloseCase.cannotCloseCase(triggeredcases);
        // 01-05-2015 - Requirements 1177/ 1174
        // smark
        // Update the student and sponsor email fields in the case object
        UpdateEmailFieldsCase.populateEmailFieldsCase(triggeredcases);
        
        PS_Case_D2L_Management.assignAccountToNewSponsor(triggeredcases);  
        
        // change 1.2 starts        
        
        PS_UpdateOptOutPreferences.updateCaseStatus(triggeredcases);
        
        // change 1.2 ends
        //R4 code merge
       // PS_caseEntitlement.UpdateCaseEntitlement(triggeredcases);
        PS_CaseBusinessHour.mPopulateBusinessHour(triggeredcases);
    }*/
    
    
    /**
* Description : Performing all  After Insert Operations
* @param List<Case> newCases
* @return NA
* @throws NA
R4 IDC: Updated method signature by adding newmap
**/
    
    public static void afterInsert(List<Case> newCases,Map<id,Case> oldmap,Map<id,Case> newmap)
    {                
        
        
        // PS_Case_AgentHandleTime.mCalculateAgentHandleTime(newCases,oldmap,newmap);  //Added by Rahul  
        PS_UpdateCaseOwnerTracking.mCreateCaseOwnerTracking(newCases, null, newmap);       
        PS_CaseUpdateParentCaseCtrl.mProcessParentCase(newCases,true,false,null);  
        PS_CaseServiceAssignmentRuleActivation.mActivateAssignmentRule(newCases,true,false,null);
        PS_AccountContactCreation.mAccountContactCreation(newCases,true,false,null);
        PS_Case_TriggerSequenceCtrlr.PS_updateACRWithCase(newCases,null);
        //KP:2/11/2017:Added switch code for UK case assignment
        System.debug('@@afterInsert101newCases1'+newCases);
        System.debug('@@afterInsert101oldmap1'+oldmap);
        System.debug('@@afterInsert101newmap1'+newmap);
        if (Label.R6AssignmentSwitch.equalsIgnoreCase('on')){
            CallFlowforUKCaseOwnerAssignment.mInitiateFlow(newCases,true,false);
            System.debug('@@newCasesafterInsert1 '+newCases);            
            CaseUKAssignmentRuleActivation.mAssignEscalationCase(newCases,null);
            System.debug('@@newCasesafterInsert2'+newCases);               
        }
        System.debug('@@afterInsert101newCases2'+newCases);
        System.debug('@@afterInsert101oldmap2'+oldmap);
        System.debug('@@afterInsert101newmap2'+newmap);
        
    }
    
    /**
* Description : Performing all  After Update Operations
* @param NA
* @return NA
* @throws NA
R4 IDC: Updated method signature by adding newmap
**/
    public static void afterUpdate(List<Case> newCases,map<id,case> oldmap,map<id,case> newCaseMap){             
           
        PS_AccountContactCreation.mAccountContactCreation(newCases,false,true,oldmap);                    
        // call updateMarketingPref to update the opt out preferences for related Contact.
        
        // change 1.2 starts                        
        
        PS_UpdateOptOutPreferences.updateMarketingPref(newCases);
        
        // change 1.2 ends
        
        //change 1.1 starts
        List<Case> intCases = new List<Case>();
        Schema.DescribeSObjectResult describeSobjectResult = Schema.SObjectType.Case; 
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeSobjectResult.getRecordTypeInfosById();  
        System.debug('@@afterUpdate101newCases1'+newCases);
        System.debug('@@afterUpdate101oldmap1'+oldmap);
        System.debug('@@afterUpdate101newCaseMap1'+newCaseMap);         
        
        for(case caseList: newCases)
        {           
            String sCaseRTName = recordTypeMap.get(caseList.recordtypeid).getName();  
            //KP: Added method to validate R4 cases for Integration       
            if (!caseList.supress_notification__c && !Label.PS_CaseRT_TS_SN.contains(sCaseRTName))     
            {                
                intCases.add(caseList);
            }                        
        }        
        
        PS_INT_IntegrationRequestController.createIntegrationRequestCase(intCases);
        //change 1.1 ends
        //R4 code merge 
        PS_Case_AgentHandleTime.mCalculateAgentHandleTime(newCases,oldmap,newCaseMap);
        PS_UpdateCaseOwnerTracking.mCreateCaseOwnerTracking(newCases,oldmap,newCaseMap);
        PS_Case_AgentHandleTime.mCalculateAgentHandleTimeOwnerChange(newCases,oldmap,newCaseMap);
        PS_CaseUpdateParentCaseCtrl.mProcessParentCase(newCases,false,true,oldmap);
        PS_CaseServiceAssignmentRuleActivation.mActivateAssignmentRule(newCases,false,true,oldmap);
        PS_Case_TriggerSequenceCtrlr.PS_updateACRWithCase(newCases,oldmap);
        //KP:2/11/2017:Added switch code for UK case assignment
        if (Label.R6AssignmentSwitch.equalsIgnoreCase('on')){        
            CaseUKAssignmentRuleActivation.mAssignEscalationCase(newCases,oldmap); 
            System.debug('@@newCases1afterUpdate '+newCases);            
            System.debug('@@oldmap1afterUpdate '+oldmap);            

        }
        System.debug('@@afterUpdate101newCases2'+newCases);
        System.debug('@@afterUpdate101oldmap2'+oldmap);
        System.debug('@@afterUpdate101newmap2'+newCaseMap);
    }
    public static void PS_updateACRWithCase(LIST<Case> caseList, MAP<ID, Case> oldCaseMap) {
        
        string ACRRecTypeID=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
        LIST<string> accountIDList=new LIST<string>();
        LIST<string> accountOldIDList=new LIST<string>();
        LIST<Account> acctUpdList=new LIST<Account>();
        LIST<Account> acctCntList=new LIST<Account>();
        MAP<ID,string> mapAccountWithCase=new MAP<ID,string>();
        for(Case caseRec:caseList) {
            if(caseRec.AccountID!=null && (oldCaseMap==null || caseRec.AccountID!=oldCaseMap.get(caseRec.ID).AccountID)) { //if insert or Account associated with case gets changed
                accountIDList.add(caseRec.AccountID);
                if(oldCaseMap !=null) {
                    
                    accountOldIDList.add(oldCaseMap.get(caseRec.ID).AccountID);
                }
                
            }
            
        }
        
        for (Account actRec:[Select ID,RecordTypeID,ACR_With_Cases__c from Account where id in :accountIDList]) {
            if(actRec.RecordTypeID ==ACRRecTypeID && actRec.ACR_With_Cases__c==false) {
                actRec.ACR_With_Cases__c=true;
                acctUpdList.add(actRec);
                
            }
            
        } 
        
        for (Account actRec:[Select ID,RecordTypeID,ACR_With_Cases__c from Account where id in :accountOldIDList]) {
            if(actRec.RecordTypeID ==ACRRecTypeID && actRec.ACR_With_Cases__c == true) {
                acctCntList.add(actRec); //this is probable to be updated provided no ther case associated
            }
            
        }  
        
        for(Case caseRec:[Select ID,AccountID from Case where AccountID in :acctCntList]) {
            mapAccountWithCase.put(caseRec.AccountID,'Exists');     
            
        }
        
        
        for (Account acctRec:acctCntList) {
            if(!mapAccountWithCase.containsKey(acctRec.ID)) {
                acctRec.ACR_With_Cases__c=false;
                acctUpdList.add(acctRec);
            }
            
        }
        
        if(acctUpdList.size()>0) {
            update acctUpdList;
        }
        
    }
    /**
* Description : Added below code for updating case fields for E2C scenario using custom metadata types as part of CR-01124
* @param NA
* @return NA
* @throws NA
* @Author: Saritha Singamsetty
* @Modified By: Mayank Lal for INC3631155 - to prevent assignment of case back to Queue, if current owner is a Tier 3 Agent
**/
    
    public static MAP<string,string> mapUserToRole= new MAP<string,string>();
    public static boolean firstTime=true;
    public static void CaseFieldsUpdate(List<Case> newCases,Map<id,Case> oldcases) {
        List < Case > oCaseList = new List < Case > ();
        LIST<ID> ListOwnerIDs = new LIST<ID> ();
        Map<String, ANZEmailRouting_BusinessVerticalMapping__mdt> emailRoutingMap = new Map<String, ANZEmailRouting_BusinessVerticalMapping__mdt>();
        ANZEmailRouting_BusinessVerticalMapping__mdt[] ANZMetadataMappings;
        if(newCases[0].To_email_origin_address__c !=null){
            ANZMetadataMappings = [Select Business_Unit__c, BusinessVertical__c, Market__c, Request_Type__c, Routing_Address__c FROM ANZEmailRouting_BusinessVerticalMapping__mdt];
            for (ANZEmailRouting_BusinessVerticalMapping__mdt ANZMapping: ANZMetadataMappings){
                emailRoutingMap.put(ANZMapping.Routing_Address__c,ANZMapping);
            }
        }
        
        for(Case caseOwner:newCases) {
                if(string.valueof(caseOwner.OwnerID).startswith('005')) {
                    ListOwnerIDs.add(caseOwner.OwnerID);
                }
            }

        if(firstTime==true){
            for (User usrRec:[Select ID, UserRole.Name from User where id in :ListOwnerIDs]) {
                mapUserToRole.put(usrRec.Id,usrRec.UserRole.Name);    
                
            }
            firstTime=false; 
        }      
        System.debug('@@emailRoutingMap '+emailRoutingMap);
        
        boolean addedRow;
        for (Case caseObj: newCases) { 
            System.debug('@@caseObj '+caseObj);
            addedRow=false;
            if(oldcases.get(caseObj.id).To_email_origin_address__c == null){
                if(caseObj.origin == 'Email'  && caseObj.To_email_origin_address__c != null){
                    if (emailRoutingMap.containsKey(caseObj.To_email_origin_address__c )) {
                        if(emailRoutingMap.get(caseObj.To_email_origin_address__c ).Market__c !=null){
                            caseObj.Market__c = emailRoutingMap.get(caseObj.To_email_origin_address__c ).Market__c;
                        }
                        if(emailRoutingMap.get(caseObj.To_email_origin_address__c ).BusinessVertical__c !=null) {    
                            caseObj.PS_Business_Vertical__c = emailRoutingMap.get(caseObj.To_email_origin_address__c ).BusinessVertical__c;
                        }
                        if(emailRoutingMap.get(caseObj.To_email_origin_address__c ).Business_Unit__c !=null) {    
                            caseObj.Business_Unit__c = emailRoutingMap.get(caseObj.To_email_origin_address__c ).Business_Unit__c;
                        }
                        if(emailRoutingMap.get(caseObj.To_email_origin_address__c ).Request_Type__c!=null) {    
                            caseObj.Request_Type__c = emailRoutingMap.get(caseObj.To_email_origin_address__c ).Request_Type__c;
                        }
                        addedRow=true;
                    }
                } 
            }
            System.debug('@@mapUserToRole '+mapUserToRole);
            System.debug('@@emailRoutingMap12 '+emailRoutingMap);
            System.debug('@@caseObj12 '+caseObj);
            if(string.valueof(caseObj.OwnerID).startswith('005')) {
                
                //populate Owner_Role__c field
                if(mapUserToRole.containsKey(caseObj.ownerID)){
                    caseObj.Owner_Role__c=mapUserToRole.get(caseObj.ownerID);
                }
                addedRow=true;
            }
            else {
                caseObj.Owner_Role__c = ''; // Added by Mayank for INC3631155
            }
            
            if(addedRow==true){
                oCaseList.add(caseObj);
            }
        }
    }
    
    /**
* Description : Added below method of sending data to batch class to stamp parent's data to child cases
* @param NA
* @return NA
* @throws NA
* @Author: MAYANK LAL
**/
    public static void CopyParentDataOnChild(List<Case> newCases) {
        Map<id,String> mapChildParent = new Map<id,String>();
        //Added as part of 101 SOQL error after SP,Lex and CI changes deploy
        Boolean isExecuting = false;
        for(Case oCase : newCases) {
            if(oCase.ParentId == NULL && oCase.sendMassEmail__c ==true) {
                mapChildParent.put(oCase.id, oCase.Mass_Email_Body__c);
            }
            if(!isExecuting)
                isExecuting = (([SELECT COUNT() FROM AsyncApexJob WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE Name = 'PS_BatchStampParentDataOnChildCases') AND Status != 'Completed']) == 0) ? false : true ;
            System.debug('@@isExecuting '+isExecuting);
            if(isExecuting) {
                return;
            }
            else {
                if(mapChildParent.size()>0 && mapChildParent != null && !system.isFuture() && !system.isBatch()) {
                    Database.executeBatch(new PS_BatchStampParentDataOnChildCases(mapChildParent), 10); 
                    isExecuting = True;
                }
            }
        }
    }
    
    /**
* Description : Method to close Parent Case if all child under that Parent Case is closed
* @param NA
* @return NA
* @throws NA
* @Author: MAYANK LAL
**/    
   public static void CloseParentWhenAllChildClose(List<Case> lstNewCases) {

        List<Case> lstCloseParents = new list<Case>();
        Map<Id,Boolean> mapParents = new Map<Id,Boolean>();
        Set<Id> setParentIds = new Set<Id>();
        List<Case> lstChilds = new list<Case>();
        
        for(Case pCase : lstNewCases) {
            setParentIds.add(pCase.ParentId);            
        }
        
        lstChilds = [select Id,Status,ParentId from Case where ParentId IN: setParentIds];
        
        if(!lstChilds.isEmpty()) {
            for(Case eachCase : lstChilds) {
                if(eachCase.Status != 'Closed'){
                    mapParents.put(eachCase.ParentId,False);
                }
                else if (!mapParents.containsKey(eachCase.parentId)) {
                    mapParents.put(eachCase.ParentId,True);
                }
            }
        }
        
        for(Id eachParent : mapParents.keySet()) {
            if(mapParents.get(eachParent)) {
                lstCloseParents.add(new Case(Id = eachParent , Status = 'Closed'));   
            }
        }
 
        // Update Parent Status to Closed
        if( !lstCloseParents.isEmpty()) {
            try {
                update lstCloseParents;
            }
            Catch (Exception e) {
                
            }
        } 
    } 
    
/* Description : Added below method of Escalating the cases Based on the Owner of the Case for School Assessment functionality.
* @param NA
* @return NA
* @date May 2017
* @Author: Kaavya Kanna (SA Offshore)
*/
    public static void EscalatingCasesOnQueue(MAP<ID, Case> newCaseMap){
        system.debug(newCaseMap.keyset()+'****KEYSET'+newCaseMap.values());
        list <Case> CaseList = new list <case>();
        list <Case> caseObjRec = new list <case>();
        Map<Id,String> ownerQueueMap = new Map<Id,String>();
        set<string> ownerQueueSet = new set<string>();
        // Check for Queue as owner 
        for (case tempCase : newCaseMap.values()) {
            if (string.valueof(tempCase.Ownerid).startswith('00G')){
                ownerQueueSet.add(tempCase.Ownerid);
            }
        }
        
        
        if (!ownerQueueSet.isEmpty()) {
            
            // Query the Queue Name add to Map
            for (QueueSobject queueTemp : [SELECT Id, QueueId, queue.name FROM QueueSobject where QueueId IN : ownerQueueSet]) {
                ownerQueueMap.put(queueTemp.QueueId,queueTemp.queue.name);
            }    
            
            
            CaseList = [select id, owner.name,RecordType.name,IsEscalated from case where ID IN: newCaseMap.keyset()];
            for(case caseRec : CaseList){
                String CaseRecOwner= newCaseMap.get(caseRec.id).Ownerid;        
                String SACaseRTName=caseRec.RecordType.name;
                
                if(SACaseRTName !=NULL && SACaseRTName != '' && Label.State_National.contains(SACaseRTName) && CaseRecOwner!=NULL && CaseRecOwner != ''){
                    system.debug('CaseRecOwner' +CaseRecOwner);
                    //if(CaseRecOwner.startswith('00G')){
                    if(ownerQueueSet.contains(CaseRecOwner)){                   
                        //string queueName = [SELECT Id, QueueId, queue.name FROM QueueSobject where QueueId=:CaseRecOwner].queue.name;
                        system.debug('CaseRecOwner' +CaseRecOwner);
                        system.debug('ownerQueueMap' +ownerQueueMap);
                        string queueName = ownerQueueMap.get(CaseRecOwner);
                        system.debug('queueName ' +queueName);
                        if(Label.SA_Escalation_Queues.contains(queueName) || Label.SA_Escalation_Queues2.contains(queueName))
                        {                       
                            system.debug('RecordTypeName'+Label.State_National.contains(SACaseRTName));
                            system.debug('caseObjRec.ownername'+Label.SA_Escalation_Queues.contains(queueName));    
                            caseObjRec.add(caseRec); 
                        }
                    }
                    
                } 
            }                                                                                                
            if(caseObjRec!=NULL && caseObjRec.size()>0)
            {
                for(case caseUpdate : caseObjRec){
                    newCaseMap.get(caseUpdate.id).IsEscalated = true;
                }
                
            }
        }
    }     
    
    /***********************************************************************************************/
    //June CR-1362 
    
    //Description: Modified new method CreateNewAccount, to create new account record for customer service cases
    /***********************************************************************************************/
    Public static void creatingNewAccountContact(List<case> newCases){
        LIST<Contact> ContactCreateList=new LIST<Contact>();
        LIST<Account> AccountCreateList=new LIST<Account>();
        for (Case caseRec:newCases) {
            if(caseRec.Create_Account__c == True && caseRec.First_Name__c!= null && caseRec.Last_Name__c!= null && caseRec.New_Contact_Email__c!= null) {
                
                Contact cntRec=new Contact(RecordTypeId='012b000000018xA',FirstName=caseRec.First_Name__c,LastName=caseRec.Last_Name__c,Email=caseRec.New_Contact_Email__c);
                ContactCreateList.add(cntRec);
                
                Account actRec=new Account(Name = caseRec.First_Name__c+ ' ' + caseRec.Last_Name__c, RecordTypeId = '012b0000000Djbl',Is_created_from_case__c =true);
                AccountCreateList.add(actRec);
            }
            
            
            
        }
        
        if(AccountCreateList.size()>0) {
            insert AccountCreateList;
            
        }
        MAP<string,Account> mapNameAcct=new MAP<string,Account>();
        for (Account actRec:[select Name from Account where id in :AccountCreateList]) {
            mapNameAcct.put(actRec.Name,actRec);
            
        }
        LIST<Contact> contactInsertList=new LIST<Contact>();
        if(ContactCreateList.size()>0) {
            for(Contact cntRec:ContactCreateList) {
                cntRec.AccountID=mapNameAcct.get(cntRec.FirstName+ ' ' +cntRec.LastName).Id;
                contactInsertList.add(cntRec);
                
            }    
            
            insert contactInsertList;
        }
        MAP<string,contact> mapAcctToContact=new MAP<string,contact>();
        for (Contact cntRec:[Select AccountID,ID,Email from Contact where id in :contactInsertList]) {
            mapAcctToContact.put(cntRec.Email,cntRec);    
            
            
        }
        
        for (Case caseRec:newCases) {
            
            if(mapAcctToContact.containsKey(caseRec.New_Contact_Email__c) &&  caseRec.Create_Account__c==True) {
                caseRec.AccountID=mapAcctToContact.get(caseRec.New_Contact_Email__c).AccountID;
                caseRec.ContactID=mapAcctToContact.get(caseRec.New_Contact_Email__c).Id;
                caseRec.Create_Account__c=false;
                caseRec.First_Name__c =null;
                caseRec.Last_Name__c =null;
                caseRec.New_Contact_Email__c =null;
                
            }
            
            
        }
        
    }
/* Description : Added below method for ZA Market to allow only HEC & SA user that belong to same campus to allow edit on Case for CR-01064
* @param NA
* @return NA
* @date May 2017
* @Author: Saritha Singamsetty
*/
    public static void ValidateCaseUpdate(Case[] oldCaseLst, map<ID, Case> newCaseMap){
        User currUser = new user();
        String UserRole;
        Set<String> roleStr = new Set<String>();
        List<Case> lstcase = new List<Case>();
        roleStr.add('%Higher Education Consultant%');
        roleStr.add('%Student Advisor%');
        //Id RecId = [SELECT Id FROM RecordType where name = 'Update Biographic Info'and SobjectType = 'Case'].Id;
        //Id RecId = [SELECT Id FROM RecordType where name =: Label.PS_CaseUpdateBiographicInfoRecordType and SobjectType = 'Case'].Id;
        //Added as part of 101 SOQL error after SP,Lex and CI changes deploy
        Id RecId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.PS_CaseUpdateBiographicInfoRecordType).getRecordTypeId();
        for(Case oCase :oldCaseLst){
            if(oCase.RecordTypeId == RecId)
            {
                lstcase.add(oCase);
            }
        }
        if(!lstcase.isEmpty())  {   
            
            try{
                currUser = [select id,name,UserRole.Name from user where id =: Userinfo.getUserId() and market__c = 'ZA' and userrole.name like :roleStr];   
                userRole = currUser.UserRole.Name;
            }
            catch(Exception e){
                currUser = null;
            }
            if(currUser != NULL){
                for(Case oCase: lstcase){
                    if(!userRole.contains(oCase.ZA_Campus__c)){
                        newCaseMap.get(oCase.Id).addError('Only Higher Education Consultants and Student Advisors of the same campus can edit the case.');
                    }
                }
            }
            else
            {
                for(Case oCase: lstcase)
                {
                    newCaseMap.get(oCase.Id).addError('Only Higher Education Consultants and Student Advisors of the same campus can edit the case.');
                }
            }
        }
    } 
    
     /* Description : As part of CRMSFDC-518 added below method for updating the fields Case Owner & Case Owner Manager At Survey Invitation for all markets when a survey invitation is sent and response not recieved
* @param NA
* @return NA
* @date Sep 2017
* @Author: Charitha Seelam
*/
    Public static void updateCaseFieldsAtSurveyInvitation(List<Case> newCases,map<id,case> oldmap,map<id,case> newCaseMap){
    
        Schema.DescribeSObjectResult describeSobjectResult = Schema.SObjectType.Case; 
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeSobjectResult.getRecordTypeInfosById();           
        Set<Id> caseUserIds = new Set<Id>();
        List<Case> updateCasesList = new List<Case>();
        //Map<Id,User> caseOwnerUsers = new Map<Id,User>();
        System.debug('PS_Case_TriggerSequenceCtrlr : updateCaseFieldsAtSurveyInvitation :'); 
     try{
        for(Case newCase : newCases)
        {
            String sCaseRTName = recordTypeMap.get(newCase.recordtypeid).getName(); 
            System.debug('updateCaseFieldsAtSurveyInvitation : Case Market : ' + newCase.Market__c + ' sCaseRTName: '+ sCaseRTName); 
            
            if(newCase.Market__c == 'UK' && (sCaseRTName == System.Label.CaseCSRecordType || sCaseRTName == System.Label.Higher_Ed_Technical_Support) && (newCase.status != null && newCase.status != oldmap.get(newCase.id).status && newCase.status == 'Closed') && newCase.Survey_Sent__c == false && (newCase.PS_Business_Vertical__c != oldmap.get(newCase.id).PS_Business_Vertical__c && System.label.UK_Case_Business_Vertical_Values.indexOf(newCase.PS_Business_Vertical__c) != -1 || newCase.PS_Business_Vertical__c == 'Clinical')){
                                
                if(String.valueOf(newCase.ownerId).indexOf('005') != -1){
                     caseUserIds.add(newCase.ownerId);
                     updateCasesList.add(newCase);
                     }
               
            }
            
            else{          
            if((newCase.Global_Language__c == null || ( newCase.Global_Language__c != null && newCase.Global_Language__c == 'English')) && (sCaseRTName == System.Label.CaseCSRecordType || sCaseRTName == System.Label.Higher_Ed_Technical_Support) && (newCase.status != null && newCase.status == 'Closed') && newCase.Survey_Sent__c == false && newCase.Case_Resolution_Category__c != 'Customer - No Response'){ 
            
                if(String.valueOf(newCase.ownerId).indexOf('005') != -1){
                    caseUserIds.add(newCase.ownerId);
                    updateCasesList.add(newCase);
                    }
                 
              }
            }
         }  
            System.debug('updateCaseFieldsAtSurveyInvitation : caseUserIds : ' + caseUserIds);
            if(caseUserIds != null && caseUserIds.size()>0) {
                List<User> usersList = [Select id, firstname, lastname, managerId, Manager.firstname, Manager.lastname from user where id in : caseUserIds];
                if(usersList != null && usersList.size()>0){
                for(User caseUser : usersList){
                   // caseOwnerUsers.put(caseUser.id,caseuser);
                    for(Case caseObj: updateCasesList){
                        if(caseObj.ownerId == caseUser.id)
                        {
                            if(caseUser.firstname == null && caseUser.firstname == '')
                            caseObj.Case_Owner_at_Survey_Invitation__c = caseUser.lastname; 
                            else                          
                            caseObj.Case_Owner_at_Survey_Invitation__c = caseUser.firstname+' '+caseUser.lastname;
                                                        
                            if(caseUser.ManagerId != null){
                                if(caseuser.Manager.firstname != null)
                            caseObj.Case_owner_manager_at_survey_invitation__c = caseUser.Manager.firstname;
                            caseObj.Case_owner_manager_at_survey_invitation__c = caseObj.Case_owner_manager_at_survey_invitation__c+' '+caseUser.Manager.lastname;
                            }
                                                        
                        }
                    }
                    }     
                
                }               
            }       
         }catch(Exception e){
             System.debug('Exception occured while updating the case owner and case owner manager at survey invitation'+e.getMessage());
         }
    }
}