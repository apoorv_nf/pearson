/* ----------------------------------------------------------------------------------------------------------------------------------------------------------   
Name:            PS_Lead_RecordTaggingCtrlr.cls    
Description:     On After_update/ of Lead record    
Date             Version         Author                             Summary of Changes    
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------  
04/2015         1.0                Rahul Garje                       Initial Release
11/2015         1.1                Ronnie                            Duplicate check for creating AccountContact__c objects added for Defect D-2646 
11/2015         1.2                Chaitra                           Added exception framework to capture exceptions in exception logger
2/2016          1.3                Abhinav                           populating the Preferred Address and First Language fields for Learner & Sponsor Contact.
5/2016      1.4        Chaitra                   Mapping institution/organisation from lead to learner contact- D-0804
6/2018          1.5                Krishnapriya     Added as a part of CR-01908-Req-67 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class PS_lead_createContact {
  //private static boolean pasrun = false;
   

  public static Set<Id> processedLead = new Set<Id>(); //this variable is to garantee that this code is only execute once

  public static void createAccountContact(List<Lead> input, Map<id, lead> leadMap){
    
    List<Lead> CreateAccountContactList = new List<Lead>();

    for(Lead p : input)
    {
    
      if(!processedLead.contains(p.Id))
      {
        CreateAccountContactList.add(p);
      }
    }

    if(CreateAccountContactList.isEmpty()) 
    {

      return;
    }
   

    
    List<ID> leadID = new List<ID>();  // List of Leads
    List<Contact> contactList = new List<Contact>(); //List of contacts
    List<Contact>  defaultContactList = new List<Contact>(); //List of Contacts
    List<AccountContact__c> acc_cont = new List<AccountContact__c>(); //List of AccountContacs
    List<AccountContact__c> AcctConrole = new List<AccountContact__c>(); //List of Account Contacts
    List<Id> conId = new List<Id>();  //List of Ids
    List<Contact> conlist = new List<Contact>(); //List of contacts
    List<Account> Acclist = new List<Account>(); //List of Accounts
    List<Opportunity> OppList = new List<Opportunity>(); //List of Opportunities
    List<Id> oppId = new List<Id>();  //List of Ids
    String SponsorType; // sponsor type
    String strRoleDetail;
    set<Id>LeadIds = new set<id>(); // D-0804
    Map<set<Id>,List<Contact>> mapContactListCheck = new Map<set<Id>,List<Contact>>(); //Added by priya for lead conversion issue
    set<Id>contactIds = new set<Id>();  //Added by priya for lead conversion issue
    //code added to get the Record type from account ids starts
        
    Map<String, Schema.SObjectType> sObjectMap = Schema.getGlobalDescribe() ;
    
    //get the recordtype for the opportunity that will be assigned to the created opportunity.
    Schema.SObjectType oppty = sObjectMap.get('Opportunity') ; // getting Sobject Type
    Schema.DescribeSObjectResult resSchema = oppty.getDescribe() ;
    Map<String,Schema.RecordTypeInfo> recordTypeInfo = resSchema.getRecordTypeInfosByName(); //getting all Recordtype for the Sobject
    Id rtId = recordTypeInfo.get('D2L').getRecordTypeId();//particular RecordId by  Name
    Id EnterpriseopptyrtId=recordTypeInfo.get('Enterprise').getRecordTypeId();  
        
    //get the recordtype for account Learnet
    Schema.SObjectType Accnt = sObjectMap.get('Account') ; // getting Sobject Type
    Schema.DescribeSObjectResult accntresSchema = Accnt.getDescribe() ;
    Map<String,Schema.RecordTypeInfo> accntrecordTypeInfo = accntresSchema.getRecordTypeInfosByName(); //getting all Recordtype for the Sobject
    Id actrtId = accntrecordTypeInfo.get(Label.PS_LearnerRecordType).getRecordTypeId();//particular RecordId by  Name 
    Id actrtId1 = accntrecordTypeInfo.get(Label.PS_OrganisationRecordType).getRecordTypeId();//particular RecordId by  Name 
      

    //get the recordtype for the Lead that will be assigned to the created opportunity.
    Schema.SObjectType leadrt = sObjectMap.get('Lead') ; // getting Sobject Type
    Schema.DescribeSObjectResult ldresSchema = leadrt.getDescribe() ;
    Map<String,Schema.RecordTypeInfo> ldrecordTypeInfo = ldresSchema.getRecordTypeInfosByName(); //getting all Recordtype for the Sobject
    Id LeadrtId = ldrecordTypeInfo.get(Label.PS_NewAccntReq_NewAccReq).getRecordTypeId();//particular RecordId by  Name
    Id B2BLeadrtId = ldrecordTypeInfo.get('B2B').getRecordTypeId();//Kamal 28/09
    Id D2LRecordTypeId = ldrecordTypeInfo.get('D2L').getRecordTypeId(); // TC 23/01/2016 D-3584
    Id WebRequestLeadRecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Web Request').getRecordTypeId();//Added as Part of CR-02295 Req-6
    Id EnterpriseleadrtId=ldrecordTypeInfo.get('Enterprise Lead').getRecordTypeId();
      
    // list of accounts from the lead to be used in duplicate AccountContact__C    
    List<Id> accountListForDuplicateChecking = new List<Id>();
    // list of contacts from the lead to be used in duplicate AccountContact__C
    List<Id> contactListForDuplicateChecking = new List<id>();        

    // 08/05/2015 - Smark
    // Added requirement 01365
    // Get any existing sponsor contacts to avoid creating duplicates
    
    List<String> emailList = new List<String>();
    
    for(Lead l : CreateAccountContactList){
        LeadIds.add(l.id); // D-0804
        if(l.isConverted && (leadMap.get(l.id).isConverted != true)) { 
            
            if(leadMap.get(l.id).Sponsor_Type__c != 'Self') {
                
                if (l.Sponsor_Email__c != NULL) {
                    emailList.add(l.Sponsor_Email__c);
                }
            }
        }
        // add the account and contact for the lead to the lists to be used for duplicate AccountContact__c
        accountListForDuplicateChecking.add(l.ConvertedAccountId);
        contactListForDuplicateChecking.add(l.ConvertedContactId);
    } // End for
    
    // Addd the AccountContact__c objects to a map to be used when dchecking for duplicates
    // The key is a contcatination of the accountId and contactId
    Map<Id,Lead> LeadInst_orgMap = new map<Id,lead>( [select id,Institution_Organisation__r.name from lead where id IN : LeadIds]); // D-0804
    Map <String, AccountContact__c> existingAccounContacts = new Map <String, AccountContact__c>(); 
    for(AccountContact__c accCon : [SELECT Id, Account__c, Contact__c from AccountContact__c WHERE Account__c IN :accountListForDuplicateChecking and Contact__c IN :contactListForDuplicateChecking])
    {
      String key = String.valueOf(accCon.Account__c) + String.valueOf(accCon.Contact__c);
      existingAccounContacts.put(key, accCon);  
    }    
    
    Map<String, Contact> existingContactMap = new Map<String, Contact>(); // Map holding the contact details indexed by email address
    
    // Do not query for existing contacts if none of the leads have sponsor/sponsor email
    if (!emailList.isEmpty()) 
    {
        List<Contact> existingContacts = [SELECT Id, Name, Email, AccountId FROM Contact WHERE Email IN :emailList];
        
        for (Contact c : existingContacts) {
            
            existingContactMap.put(c.Email, c);
        } // End for    
    } // End email list check
    //*-------------------------------------------------------------------
    // Code added to get the Record type from account ids ends
    // if(Trigger.isAfter){
     Contact contact2;
     // This map is used to figure out the contact account Id. Contact ID cannot be used as existing contacts can be re-used
     Map<String, String> contactAccountIDMap = new Map<String, String>(); 
     
     for(Lead l:CreateAccountContactList) {
     
        
      if(l.isConverted && (leadMap.get(l.id).isConverted != true)) 
      { 

      if(leadMap.get(l.id).Sponsor_Type__c != 'Self' && leadMap.get(l.id).RecordTypeId != B2BLeadrtId && leadMap.get(l.id).RecordTypeId != EnterpriseleadrtId){ // TC 23/01/2016 D-3584
            //if(leadMap.get(l.id).Sponsor_Type__c != 'Self' && leadMap.get(l.id).RecordTypeId != B2BLeadrtId ){ // TC 23/01/2016 D-3584
          contact2 = new Contact();  
          
          contact2.Salutation = l.Sponsor_Salutation__c;  
          contact2.FirstName = l.Sponsor_Sponsor__c;
          contact2.LastName = l.Sponsor_Name__c;
          contact2.AccountId = l.ConvertedAccountId;    
          contact2.MobilePhone = String.valueOf(l.Sponsor_Mobile__c);          
          contact2.Home_Phone__c = String.valueOf(l.Sponsor_Home_Phone__c);
          contact2.Phone = String.valueOf(l.Sponsor_Phone__c);          
          contact2.Email = l.Sponsor_Email__c;    
          contact2.Role__c = 'Sponsor';
          contact2.Role_Detail__c = l.Sponsor_Type__c;
                    
          //Abhinav 2/29/2016 : populating the Preferred Address & Sponsor First Language fields
          contact2.Preferred_Address__c = 'Mailing Address';
          contact2.First_Language__c = l.Sponsor_First_Language__c;
          contact2.MailingCity = l.Sponsor_Address_City__c;          
          contact2.MailingStreet = l.Sponsor_Address_Street__c;          
          contact2.MailingState = l.Sponsor_Address_State_Province__c;          
          contact2.MailingCountry = l.Sponsor_Address_Country__c ;         
          contact2.MailingPostalcode = l.Sponsor_Address_ZIP_Postal_Code__c;
          contact2.isleadConvertedContact__c = true;
          contact2.Preferred_Method_of_Communication__c = l.Sponsor_Preferred_Method_of_Contact__c ;
          //contact2.recordTypeId = conRecordTypeIds[0].RecordType.id; 
          
          // If the contact exists already, then update it instead
          if (l.Sponsor_Email__c != NULL) {
              
              if (existingContactMap.containsKey(l.Sponsor_Email__c))
                  contact2 = existingContactMap.get(l.Sponsor_Email__c);

                  contactAccountIDMap.put(contact2.Id, l.ConvertedAccountId);
                  
          } // End email check
          
          contactList.add(contact2); 

        } 
        /* else {
          
          createTask ct = new CreateTask();
          ct.createTaskOnLead(l.ConvertedAccountId);         
        } */
         
        // To copy studentId from Lead to converted Account
        if(l.ConvertedAccountId != null){

            
            SponsorType = 'Sponsor';
            
            strRoleDetail = l.Sponsor_Type__c;
            
            if (l.Sponsor_Type__c == 'Self') {
                SponsorType = 'Learner'; 
                strRoleDetail = '';
            }
            
            /* D-2352Account acc = new Account();
            acc.Id = l.ConvertedAccountId;
            acc.Pearson_Account_Number__c = leadMap.get(l.id).Pearson_ID_Number__c;
            if(l.recordtypeid == LeadrtId){
                acc.recordTypeId = actrtId1;
            }
            else {
               if(l.recordtypeId != B2BLeadrtId) //added by kamal for bypassing 'B2B' records
               {
                   acc.recordTypeId = actrtId;
               }                             
            }
            
              /added [Stacey Walter 15062015]  to update billing address on Account if Sponsor is created
              if(leadMap.get(l.id).Sponsor_Type__c != 'Self' && leadMap.get(l.id).Sponsor_Address_Street__c!=null && leadMap.get(l.id).Sponsor_Address_ZIP_Postal_Code__c!=null && leadMap.get(l.id).Sponsor_Address_Country__c!=null){
                acc.BillingCity=l.Sponsor_Address_City__c;
                acc.BillingCountry=l.Sponsor_Address_Country__c;
                acc.BillingPostalCode=l.Sponsor_Address_ZIP_Postal_Code__c;
                acc.BillingStreet=l.Sponsor_Address_Street__c;
                acc.BillingState=l.Sponsor_Address_State_Province__c;
              }
            //Abhinav Srivastava -18/06/2015
            //Adding the functionality of mapping billing adreess of account to shipping address of account            
               if(leadMap.get(l.id).City!=null && leadMap.get(l.id).Street!=null && leadMap.get(l.id).PostalCode!=null && leadMap.get(l.id).Country!=null){
                acc.ShippingCity=l.City;
                acc.ShippingCountry =l.Country;
                acc.ShippingPostalCode=l.PostalCode;
                acc.ShippingStreet=l.Street;
                acc.ShippingState =l.State;
                acc.Email__c = l.Email;                
              } 
                         
            AccList.add(Acc); D-2352*/
        }

        // build the key to check it the AccountContact__c object already exists
        String key = String.valueOf(l.ConvertedAccountId) + String.valueOf(l.ConvertedContactId);
        // if the AccountContact__c object already exists for this account and contact combination dont create a new one
        if(!existingAccounContacts.containsKey(key))  
        {
            if(l.ConvertedContactId != null&& l.RecordTypeId!=LeadrtId && l.recordtypeId != B2BLeadrtId && l.recordtypeId != EnterpriseleadrtId && l.recordtypeId != WebRequestLeadRecordTypeId)
            // if(l.ConvertedContactId != null&& l.RecordTypeId!=LeadrtId && l.recordtypeId != B2BLeadrtId)
            {   
              //Kamal Added 'B2BLeadrtId' condition      
            
                AccountContact__c acc = new AccountContact__c();
                acc.Contact__c = l.ConvertedContactId;
                acc.Account__c = l.ConvertedAccountId; 
                acc.Decision_Making_Level__c = l.Decision_Making_Level__c;
                acc.AccountRole__c = 'Learner';
                acc.Role_Detail__c = '';
                acc.Sync_In_Progress__c = TRUE;
                acc.Financially_Responsible__c = (leadMap.get(l.id).Sponsor_Type__c == 'Self')?TRUE:FALSE;
                acc.Primary__c = true;                 
                AcctConrole.add(Acc);
            }
            /*CR-02295-Req-22-Start- Added by Srikanth */
            if(l.recordtypeId == WebRequestLeadRecordTypeId){
                AccountContact__c acc = new AccountContact__c();
                acc.Contact__c = l.ConvertedContactId;
                acc.Account__c = l.ConvertedAccountId; 
                acc.Decision_Making_Level__c = l.Decision_Making_Level__c;
                acc.AccountRole__c = l.Role__c;
                acc.Role_Detail__c = l.Role_Detail__c ;
                acc.Sync_In_Progress__c = TRUE;
                acc.Financially_Responsible__c = (leadMap.get(l.id).Sponsor_Type__c == 'Self')?TRUE:FALSE;
                acc.Primary__c = true;                 
                AcctConrole.add(Acc);
            }
            /*CR-02295-Req-22-end- Added by Srikanth */
            
            if(l.recordtypeId==LeadrtId )
            {
                AccountContact__c acc = new AccountContact__c();
                acc.Contact__c = l.ConvertedContactId;
                acc.Account__c = l.ConvertedAccountId; 
                acc.AccountRole__c = '';
                acc.Role_Detail__c = '';
                acc.Sync_In_Progress__c = TRUE;
                acc.Financially_Responsible__c = (leadMap.get(l.id).Sponsor_Type__c == 'Self')?TRUE:FALSE;
                acc.Primary__c = true;                 
                AcctConrole.add(Acc);
            }
         //added by Krishna priya
            if(l.recordtypeId==EnterpriseleadrtId )
            {
                AccountContact__c acc = new AccountContact__c();
                acc.Contact__c = l.ConvertedContactId;
                acc.Account__c = l.ConvertedAccountId; 
                acc.AccountRole__c = l.Role__c;
                acc.Role_Detail__c = l.Role_Detail__c;
                acc.Sync_In_Progress__c = TRUE;
                acc.Financially_Responsible__c = (leadMap.get(l.id).Sponsor_Type__c == 'Self')?TRUE:FALSE;
                acc.Primary__c = true;                 
                AcctConrole.add(Acc);
            }
            
            //kamal 28/09
            ContactUtils createAccCon = new ContactUtils();
            AccountContact__c AccountContact = new AccountContact__c();
            if(l.recordtypeId == B2BLeadrtId)
            {
                AccountContact = createAccCon.createAccountContact(l,leadMap);
                if(AccountContact != null)
                {
                    AcctConrole.add(AccountContact);
                }      
            }  
            //kamal end
        }                   
        //code written for passing D2L record type on Opportunity while lead conversion starts
        if(l.ConvertedOpportunityId != null  && l.recordtypeId!=EnterpriseleadrtId && l.recordtypeId != B2BLeadrtId && l.recordtypeId != WebRequestLeadRecordTypeId ){
       // if(l.ConvertedOpportunityId != null ){
            Opportunity opp = new Opportunity();
            opp.Id = l.ConvertedOpportunityId;
            oppId.add(opp.Id);       
            opp.recordTypeId = rtId;
            opp.StageName = System.Label.D2L_Opportunity_StageName_Value; //'Needs Analysis';
            opp.Type = System.Label.D2L_Opportunity_Type_Value; 
            opp.Assigned_HEC__c = l.OwnerId;
            Opplist.add(opp);
            
        }  
        //code written for passing D2L record type on Opportunity while lead conversion ends 
          
       //code added to get map the passport number and identification number on lead conversion starts
   
        if(l.ConvertedContactId!=null){
          
           Contact defaultcontact = new Contact(); 
           defaultcontact.Id = l.ConvertedContactId; 
           //defaultcontact.Passport_Number__c = l.Passport_Number__c;
           //defaultcontact.National_Identity_Number__c = l.Identification_Number__c;
           defaultcontact.OtherPhone = l.Other_Phone__c;
           defaultcontact.Marketing_Opt_In__c = l.Marketing_Opt_In__c;
            defaultcontact.POPI_Marketing_Opt_In__c = l.POPI_Marketing_Opt_In__c;
           
        if(LeadInst_orgMap.get(l.id).Institution_Organisation__r.name!= system.label.InstitutionOrganisationLead)  // D-0804
           defaultcontact.Institution_organisation__c=l.Institution_Organisation__c ;
           if(l.RecordTypeId == D2LRecordTypeId) { // TC 23/01/2016 D-3584
              defaultcontact.Role__c = 'Learner';
              //Abhinav 2/29/2016 : populating the Preferred Address field
              defaultcontact.Preferred_Address__c = 'Mailing Address';
           }
            
           defaultContactList.add(defaultcontact);
        }  
        //code added to get map the passport number and identification number on lead conversion ends 
       }//if close

       processedLead.add(l.Id);

      }// for close 
    //}//is after close
    
     if(contactList != null){                    //&& Utility.stopRecursion != True


        Database.UpsertResult[] lsr = Database.Upsert(contactList, false);
        // 18/11/2015 CP :added exception framework to capture exceptions in exception logger
         if (lsr!= null){
                List<PS_ExceptionLogger__c> errloglist=new List<PS_ExceptionLogger__c>();
                for (Database.upsertresult sr : lsr) {
                    String ErrMsg;
                    if (!sr.isSuccess() ){
                        PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
            errlogger.ApexClassName__c='PS_lead_CreateContact';
                        errlogger.CallingMethod__c='createAccountContact';
                        errlogger.UserLogin__c=UserInfo.getUserName(); 
                        errlogger.recordid__c=sr.getId();
                        for(Database.Error err : sr.getErrors()) 
                        ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage(); 
                        errlogger.ExceptionMessage__c=  ErrMsg;  
                        errloglist.add(errlogger);    
                    }
                }
                if(errloglist.size()>0)
                { 
                insert errloglist;
                }
                // exception logger ends
        }
        
        for(Integer i=0;i<lsr.size();i++){ 

          for(Database.Error dbError : lsr.get(i).getErrors()) {
          }


          if(lsr.get(i).isSuccess()){
              ConId.add(lsr.get(i).getid());
              contactIds.add(lsr.get(i).getid());
              mapContactListCheck.put(contactIds,contactList);
           }
         }
        
      }  
    
     if(ConId.size()>0){
          //Commented by priya for lead conversion issue
    /*   conlist = [SELECT Id, Name, AccountId FROM Contact WHERE Id IN :ConId];
       if(conlist.size() > 0){
          for(contact c:conlist){*/
          //  End of lead conversion issue
          for(Contact c:mapContactListCheck.get(contactIds)){    //Added by priya for lead conversion issue
            // build the key to check it the AccountContact__c object already exists
            String key = String.valueOf(c.AccountId) + String.valueOf(c.Id);
            // if the AccountContact__c object already exists for this account and contact combination dont create a new one
            if(!existingAccounContacts.containsKey(key))
            {  
                AccountContact__c acc = new AccountContact__c();
                acc.Contact__c = c.Id;
                acc.Account__c = c.AccountId; // For new contacts
                if (contactAccountIDMap.containsKey(c.Id)) 
                    acc.Account__c = contactAccountIDMap.get(c.Id); // For existing contacts
                   acc.AccountRole__c = SponsorType;
                acc.Role_Detail__c = strRoleDetail;
                acc.Sync_In_Progress__c = TRUE; // Flag the records so that the validation does not fire (primary contact and financial responsible person are inserted serially)
                acc.Financially_Responsible__c = True;
                AcctConrole.add(Acc);
            }
          }
    //   } //commented by priya for lead conversion changes
     } 
       
    if(AcctConrole!=null && AcctConrole.size()>0)
    {
     
        Insert AcctConrole;      
       
    }
      
    if(AccList.size()>0)
    {
      Update AccList;
    }
     
     //Code for update Opportunity record type starts 
    if(OppList.size() > 0 && OppList!= null)
    {
       Update OppList;

       //Davi Borges: 12 May 2015 
       List<Opportunity> opps =  [SELECT Id, Type, International_Student_Status__c FROM Opportunity WHERE Id IN: oppId];
       PS_opportunity_MapAccountcontactRole.MapAccountcontactRole(opps);
       PS_CreateAssetbasedStudentRegistration.createTask(opps);
       sendEmailToOpptyContact.OppOutboundNotification(opps,null,false);
    }      
     
     //Code for update Opportunity record type ends 
     if(defaultContactList.size()>0){
       Update defaultContactList;  
     } 

   }//method close   
} //class close