/************************************************************************************************************
* Apex Interface Name : PS_OpportunityB2BNAActionsTest
* Version             : 1.0 
* Created Date        : 26/8/2016 4:44 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Abhinav Srivastav         26/8/2016              Initial Version
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

@isTest(SeeAllData=true)
private Class PS_OpportunityB2BNAActionsTest{
    static testMethod void myUnitTest1()
    {
         List<User> listWithUser = new List<User>();
         Map<Id,opportunity> accOpp= new Map<Id,opportunity>();
         Map<Id,opportunity> oldOpps1 = new Map<Id,opportunity>();
         List<Opportunity> opptylist1 = new List<opportunity>();
         Id oppId ;
         
         List<Account> acclist = new List<Account>();
         Id accId;
         
         Set < String > newtrStr = new Set < String >{'1','byn','cjo'};
         Map < String, List < Account >> AddCodeAccountmap = new Map < String, List < Account >> ();
         Map < String, Id > territoryIDCOdeMap = new Map < String, Id > ();


        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        //listWithUser[0].UserRole = 'NA Sales and Territory Admins';
        System.runAs(listWithUser[0]){
          
        Account acc1 = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School',Market2__c='US',Phone = '9989887687');
        acc1.ShippingStreet = 'TestStreet';
        acc1.ShippingCity = 'Vns';
        acc1.ShippingState = 'Delhi';
        acc1.ShippingPostalCode = '234543';
        acc1.ShippingCountry = 'India';
        acc1.Territory_Code_s__c = '2OJ';
        insert acc1;
        acclist.add(acc1);
        AddCodeAccountmap.put('1',acclist);
        accId = acclist[0].id;
        
        opptylist1 = TestDataFactory.createOpportunity(1,'B2B');
        //Opportunity opp = new Opportunity(Name= 'OpTest', AccountId = acc1.id, StageName = 'Solutioning', Type = 'New Business', Academic_Vetting_Status__c = 'Un-Vetted', Academic_Start_Date__c = System.Today(),CloseDate = System.Today(),International_Student__c = true, Channel__c = 'Direct');
        opptylist1[0].Digital_Indicator__c = True;
        opptylist1[0].AccountId = accId;
        opptylist1[0].RecordTypeId = '012b0000000DjbzAAC';
        insert opptylist1;
        opptylist1[0].StageName = 'Closed';
        Test.StartTest();    
        update opptylist1;
        oldOpps1.put(opptylist1[0].Id,opptylist1[0]);
        accOpp.put(opptylist1[0].AccountId,opptylist1[0]);
        
        //terr =  
        //insert terr;
        Territory2 terlist = TestDataFactory.createTerritory();
        insert terlist;    
        UserTerritory2Association  UsersToTerritoryList = TestDataFactory.createUserTerritory2Association(listWithUser[0].id,terlist.id);
        insert UsersToTerritoryList;
       
        PS_OpportunityB2BNAActions.cdsOpportunityClosingNotification(opptylist1,oldOpps1);
        PS_OpportunityB2BNAActions.getOpptyOwnerTerritoryCode(opptylist1 );
        PS_OpportunityB2BNAActions.updateOpptyownerTerritoryCode(opptylist1,oldOpps1);
        Test.StopTest();
        }
}
}