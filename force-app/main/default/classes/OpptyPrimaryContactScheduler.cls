/*Scheduled Class to Call the OpptyPrimaryContact batch implemented as part of CR-0368
/*
*/
global class OpptyPrimaryContactScheduler implements Schedulable {
    global void execute(SchedulableContext SC) {
        try{
        OpptyPrimaryContact opp = new OpptyPrimaryContact();
        General_One_CRM_Settings__c  setting = General_One_CRM_Settings__c.getInstance('OpptyPrimaryContactBatchSize'); 
        Database.executebatch(opp,Integer.valueOf(setting.Value__c));
        DateTime n = datetime.now().addMinutes(60);
                    String cron = '';
                    cron += n.second();
                    cron += ' ' + n.minute();
                    cron += ' ' + n.hour();
                    cron += ' ' + n.day();
                    cron += ' ' + n.month();
                    cron += ' ' + '?';
                    cron += ' ' + n.year();
                    String jobName = 'Batch Job To Update Opportunity PrimaryContact - ' + n.format('MM-dd-yyyy-hh:mm:ss');
                    OpptyPrimaryContactScheduler nextBatch = new OpptyPrimaryContactScheduler();
                    Id scheduledJobID = System.schedule(jobName,cron,nextBatch);
            }Catch(Exception e){
            System.debug(e);
            }    
    }
}