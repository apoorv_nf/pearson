/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_OpportunityB2BNAActions .cls 
   Description:     On insert/update/delete of Lead record 
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  24/08/2015      1.2           Leonard Victor              Rd-01257   R3 Code Stream Line
  12/02/2016      2.0                Kyama                  RD-01686 Get oppty owner Territory code update territory code field 
  08/03/2016      2.0                RG                     RD-01686 Get oppty owner Territory code Modified the logic to territory code field 
  07/04/2016      2.0                RG                     D-4759  
  01/11/2017      2.1          Saritha Singamsetty          CR-01644 update SPR owners whenever the respective opportunity gets updated.
------------------------------------------------------------------------------------------------------------------------------------------------------------ */


// RD: 652 | Territory Management | Pooja | 3rd June 
public class PS_OpportunityB2BNAActions {

 // Add chatter feed on User who is assigned to Territory( as a Customer Digital Success Agent) shud be associated to Opportunity
 public static void cdsOpportunityClosingNotification(List < Opportunity > opptyList, Map < Id, Opportunity > oppOldMap) {


  // RD: 652 | Territory Management | Pooja | 3rd June
  // Add chatter feed on User who is assigned to Territory( as a Customer Digital Success Agent) shud be associated to Opportunity 
  List < Opportunity > opList = new List < Opportunity > ();
  //Using Util class to fetch record type instead of SOQL or Mutiple describe calls
  Id b2bRecordType;

  if (PS_Util.recordTypeMap.containsKey(PS_Constants.OPPORTUNITY_OBJECT) && PS_Util.recordTypeMap.get(PS_Constants.OPPORTUNITY_OBJECT).containsKey(PS_Constants.OPPORTUNITY_B2B_RECCORD)) {
   b2bRecordType = PS_Util.recordTypeMap.get(PS_Constants.OPPORTUNITY_OBJECT).get(PS_Constants.OPPORTUNITY_B2B_RECCORD);
  } else {
   b2bRecordType = PS_Util.fetchRecordTypeByName(Opportunity.SobjectType, PS_Constants.OPPORTUNITY_B2B_RECCORD);
  }
  //End of Record type fix    


  for (Opportunity op: opptyList) {
   if ((op.RecordTypeId == b2bRecordType && op.Digital_Indicator__c == true && op.StageName == 'Closed Won' && op.StageName != oppOldMap.get(op.Id).StageName) || (Test.IsRunningTest())) {
    opList.add(op);
   }
  }
  //Set<Id> accountIdSet = new Set<Id>();
  Map < Id, Opportunity > accOppMap = new Map < Id, Opportunity > ();
  for (Opportunity op: opList) {
   //accountIdSet.add(op.AccountId);
   accOppMap.put(op.AccountId, op);
  }
  List < Account > acList = [select Id, name, Territory_Code_s__c from Account where Id in : accOppMap.KeySet()];

  if (acList != null) {
   Set < String > newtrStr = new Set < String > ();
   Map < String, List < Account >> AddCodeAccountmap = new Map < String, List < Account >> ();
   Map < String, Id > territoryIDCOdeMap = new Map < String, Id > ();
   for (Account ac: acList) {
    if (ac.Territory_Code_s__c != null) {
     for (String s: ac.Territory_Code_s__c.split(',')) {
      newtrStr.add(s);
     }
    }

    if (newtrStr != null && newtrStr.size() > 0) {
     for (String ns: newtrStr) {
      if (AddCodeAccountmap.containsKey(ns)) {
       List < Account > associatedAccList = AddCodeAccountmap.get(ns);
       associatedAccList.add(ac); AddCodeAccountmap.put(ns, associatedAccList);
      }
      ELSE {
       List < Account > associatedAccList = new List < Account > ();
       associatedAccList.add(ac);
       AddCodeAccountmap.put(ns, associatedAccList);
      }
     }
    }
   }
   List < UserTerritory2Association > AssignedUsersToTerritory = new List < UserTerritory2Association > ();
   //Map of aready assigned user's record with there territory Id.
   Map < String, List < UserTerritory2Association >> AssignedUsersToTerritoryMap = new Map < String, List < UserTerritory2Association >> ();
   If(AddCodeAccountmap != null) {
   if(Test.IsRunningTest()){
   AssignedUsersToTerritory = [select id, Territory2.Territory_Code__c, Territory2Id, Territory2.Name, Territory2.Territory2ModelId, Territory2.Territory2Model.Name, UserId, User.Name, User.isActive, RoleInTerritory2 from UserTerritory2Association];
   }
    else{
    AssignedUsersToTerritory = [select id, Territory2.Territory_Code__c, Territory2Id, Territory2.Name, Territory2.Territory2ModelId, Territory2.Territory2Model.Name, UserId, User.Name, User.isActive, RoleInTerritory2 from UserTerritory2Association where RoleInTerritory2 = 'Customer Digital Success Agent'      AND Territory2.Territory_Code__c IN: AddCodeAccountmap.keyset()
    ];
   }
   }
   if (AssignedUsersToTerritory != null && AssignedUsersToTerritory.size() > 0) {
    for (UserTerritory2Association aut: AssignedUsersToTerritory) {
     if (AssignedUsersToTerritoryMap.containsKey(aut.Territory2.Territory_Code__c)) {
      List < UserTerritory2Association > utList = AssignedUsersToTerritoryMap.get(aut.Territory2.Territory_Code__c);
      utList.add(aut);
      AssignedUsersToTerritoryMap.put(aut.Territory2.Territory_Code__c, utList);
     }
     ELSE {
      List < UserTerritory2Association > utList = new List < UserTerritory2Association > ();
      utList.add(aut);
      AssignedUsersToTerritoryMap.put(aut.Territory2.Territory_Code__c, utList);
     }
    }
   }
   User useradmin;
   try {
    useradmin = [select Id, Name, Email, UserRoleId, UserRole.Name from User where UserRole.Name = 'NA Sales and Territory Admins'
     AND Market__c = 'US'
     limit 1
    ];
   } catch (Exception e) {
   }
   String msg = '';
   List < FeedItem > fiList = new List < FeedItem > ();
   if (AssignedUsersToTerritoryMap.KeySet() != null) {
    for (String st: AssignedUsersToTerritoryMap.KeySet()) {
     if (AssignedUsersToTerritoryMap.get(st) != null && AssignedUsersToTerritoryMap.get(st).size() > 0) {
      for (UserTerritory2Association uat: AssignedUsersToTerritoryMap.get(st)) {
       if (uat.User.isActive == true) {
        //@sahir: moved feeditem initialization to below loop.
        msg = Label.PS_AccountTerritory5 + uat.User.Name + ', \n' + Label.PS_AccountTerritory + ' ' + uat.RoleInTerritory2 + ' ' + Label.PS_AccountTerritory7 + ' ' + uat.Territory2.Name + '.';
        if (AddCodeAccountmap != null && AddCodeAccountmap.containsKey(st)) {
         for (Account ac: AddCodeAccountmap.get(st)) {
          String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + accOppMap.get(ac.id).Id + ' ';
          msg += '\n \n ' + ' ' + Label.PS_OpportunityTerritory + ' ' + fullRecordURL + ' ' + Label.PS_OpportunityTerritory1 + ' ';
          msg += '\n \n ' + Label.PS_OpportunityTerritory2 + ' ';  if (useradmin != null) { msg += '\n \n ' + Label.PS_OpportunityTerritory3 + ' ';
          }
          FeedItem post = new FeedItem(); post.ParentId = uat.UserId; post.Body = msg;   fiList.add(post);
         //post.CreatedDate = System.now();
        
         }
        }
       }
      }
     }
    }
   }
   if (fiList.size() > 0 && fiList.size() < 150) {
    insert fiList;
   }
  }
 }

 //Added by kyama for RD-01686.Get oppty owner Territory code.  
 public static void getOpptyOwnerTerritoryCode(List < Opportunity > opList) {
  try {
   List < Territory2 > oppOwnrTerritoryList = new List < Territory2 > ();
   List < Opportunity > opptySpecialityUpdateList = new List < Opportunity > ();
   // Opportunity opptySpecialityUpdate = new Opportunity();
   set < id > accountid = new set < id > ();
   set < id > oppOwnerId = new set < id > ();
   for (opportunity oppty: opList) {
    accountid.add(oppty.AccountId);
    oppOwnerId.add(oppty.OwnerId);
   }
   String accountTerritory = [select Territory_Code_s__c from Account where id in : accountid].Territory_Code_s__c;
   if(Test.IsRunningTest())
   {
   oppOwnrTerritoryList = [Select id, Territory2Model.ActivatedDate, Territory_Code__c from Territory2];
   }
   else{
   oppOwnrTerritoryList = [Select id, Territory2Model.ActivatedDate, Territory_Code__c from Territory2 where id in (Select Territory2Id from UserTerritory2Association where IsActive = true and UserId IN: oppOwnerId) and (Territory2Model.ActivatedDate!=null and Territory2Model.DeactivatedDate=null)];
   }
   for (opportunity oppty: opList) {
    Opportunity opptySpecialityUpdate = new Opportunity();
    opptySpecialityUpdate.id = oppty.id;
    if (!oppOwnrTerritoryList.isEmpty() && oppOwnrTerritoryList != null) {
        if (oppOwnrTerritoryList.size() > 1) {
            for (Territory2 oppOwnrterritory: oppOwnrTerritoryList) {
                if (oppOwnrterritory.Territory_Code__c != '' && accountTerritory.contains(oppOwnrterritory.Territory_Code__c)) {
                    opptySpecialityUpdate.Territory_Code__c = oppOwnrterritory.Territory_Code__c; break;
                } else { //RG:Added for D-4759  - 07/04/2016
                   opptySpecialityUpdate.Territory_Code__c = oppOwnrterritory.Territory_Code__c; break;
                }
                opptySpecialityUpdateList.add(opptySpecialityUpdate);
            }
        } else {
               opptySpecialityUpdate.Territory_Code__c = oppOwnrTerritoryList[0].Territory_Code__c; opptySpecialityUpdateList.add(opptySpecialityUpdate);
         } 
    } //end if
    else{
          opptySpecialityUpdate.Territory_Code__c = '';  opptySpecialityUpdateList.add(opptySpecialityUpdate);
     }
   } //end for
    if (!opptySpecialityUpdateList.isEmpty() && opptySpecialityUpdateList.size() > 0) {  update opptySpecialityUpdateList;
    }
   
  } catch (Exception e) {
  }
 }

 //Added by kyama for RD-01686.Get oppty owner Territory code. 
 public static void updateOpptyownerTerritoryCode(List < Opportunity > opps, Map < Id, Opportunity > oldOpportunities) {
  
   for (Opportunity newOpp: opps) {
    Opportunity oldopp = (Opportunity) oldOpportunities.get(newOpp.id);
    if (newOpp.OwnerId != null && (newOpp.OwnerId != oldopp.OwnerId)) {
     getOpptyOwnerTerritoryCode(opps);
    }
   }
  } //End Method
 // Added by Saritha Singamsetty for CR-1644 to update all Opportunity SPR's owner whenever opportunity owner is changed. 
    public static void updateOpptyowneronSPR(List < Opportunity > opps, Map < Id, Opportunity > oldOpportunities) {
        Map<Id,Id> oppOwnerMap = new Map<Id,Id>();
        List<Special_Price_Request__c> lstoppSPRs = new List<Special_Price_Request__c>();
        for (Opportunity newOpp: opps) {
            Opportunity oldopp = (Opportunity) oldOpportunities.get(newOpp.id);
            if (newOpp.OwnerId != null && (newOpp.OwnerId != oldopp.OwnerId)) {
                oppOwnerMap.put(newOpp.Id, newOpp.OwnerId);
            }
        }
        for(Special_Price_Request__c spr:[SELECT Opportunity__c, OwnerId, Id FROM Special_Price_Request__c where Opportunity__c IN: oppOwnerMap.Keyset()]){
            if(oppOwnerMap.containsKey(spr.Opportunity__c)){
                spr.OwnerId = oppOwnerMap.get(spr.Opportunity__c); lstoppSPRs.add(spr);
            }
        }
        if( !lstoppSPRs.isEmpty()) {
            try { update lstoppSPRs;
            }
            Catch (Exception e) {
                
            }
        }
    }

}