/*
 * Author: Apoorv Saxena (Neuraflash)
 * Date: 27/08/2019
 * Description: Used for getting Contact Details  Associated to MessagingEndUser - Developed for SMS Bot.
 * Update: Commented logic to look for Contact by phone number (Read Time Out Issue - needs custom indexing) - 21/10/2019 
 */
public with sharing class EinsteinBotCheckContactByPhone {
    
    @InvocableMethod(label='Einstein SMS Bot - Check Contact By Phone')
    public static List<Contact> checkContactExistenceByPhone(List<String> routableId){
        try{
            String msgSessionId = routableId[0];
            Id contactId;
            Map<String,Id> phoneConIdMap = new Map<String,Id>();

            if(!String.isBlank(msgSessionId)){
                for(MessagingSession msgSession: [SELECT Id, MessagingEndUserId, 
                                                    MessagingEndUser.ContactId, MessagingEndUser.MessagingPlatformKey 
                                                        FROM MessagingSession 
                                                            WHERE Id =: msgSessionId]){
                    phoneConIdMap.put(msgSession.MessagingEndUser.MessagingPlatformKey, msgSession.MessagingEndUser.ContactId);
                }

                if(!phoneConIdMap.keySet().isEmpty()){
                    for(String phoneNumber: phoneConIdMap.keySet()){
                        // String formattedNumber = phoneNumber;
                        // if(formattedNumber.length()==12 && formattedNumber.startsWith('+1')){
                        //     formattedNumber = formattedNumber.substring(2,formattedNumber.length());
                        // }
                        if(phoneConIdMap.get(phoneNumber) != NULL){
                            contactId = phoneConIdMap.get(phoneNumber);
                        } /*else {
                            return [SELECT Id, FirstName, LastName, Email 
                                        FROM Contact 
                                            WHERE Phone =: phoneNumber OR Phone =: formattedNumber
                                                ORDER BY LastModifiedDate LIMIT 1];
                        }*/
                    }
                    if(contactId != NULL){
                        return [SELECT Id, FirstName, LastName, Email FROM Contact WHERE Id =: contactId];
                    }
                }
            }
        } catch(Exception ex){
        }
        return NULL;
    }
}