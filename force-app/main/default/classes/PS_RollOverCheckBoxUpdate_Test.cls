@isTest
public class PS_RollOverCheckBoxUpdate_Test {
    static testmethod void PostiveScenario_Rolloverchkdfromprod() {
        //Insert Account 
         Account acc = new Account();
        //acc.RecordTypeId = '012b0000000DpIM'; 
        acc.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
        acc.Name = 'Test Account';
        acc.Geography__c = 'Growth';
        acc.Line_of_Business__c = 'Higher Ed';
        acc.Market2__c = 'Global';
        
        insert acc;
        
        List<Contact> lstCon = TestDataFactory.createContacts(1);
        lstCon[0].accountid = acc.id;
        insert lstCon;
        
        //Insert Base Product
        Product2 PIUprod = new Product2();
           PIUprod.Configuration_Type__c='Option';     //CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           PIUprod.Duration__c = 'D10';
           PIUprod.name = 'NA Territory Product1';
           PIUprod.Line_of_Business__c = 'Higher Ed';
           PIUprod.Market__c ='US';
           PIUprod.Business_Unit__c='US Field Sales';
           PIUprod.Binding__c = 'Access Code Card'; 
           PIUprod.Edition__c = '10';
           PIUprod.Platform__c = 'Plat1';
           PIUprod.Relevance_Value__c = 10;
           PIUprod.Brand__c = 'Mastering';
           PIUprod.Medium2__c= 'Digital';
           PIUprod.Category2__c='CourseSmart';
           PIUprod.CurrencyIsoCode = 'USD';   
        insert PIUprod;
        
        //Insert Product Family Next Edition
        Product2 prodFamilyNextEdition = new Product2();
           prodFamilyNextEdition.Configuration_Type__c='Option';    // CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           prodFamilyNextEdition.Market__c ='US';
           prodFamilyNextEdition.Line_of_Business__c='Higher Ed';
           prodFamilyNextEdition.Business_Unit__c   ='US Field Sales';
           prodFamilyNextEdition.name = 'NA Test Product Family';
           prodFamilyNextEdition.Author__c = 'Test Author'; 
           prodFamilyNextEdition.Edition__c = '10';
           prodFamilyNextEdition.Status__c = 'PUB';
           prodFamilyNextEdition.Relevance_Value__c = 20;
           prodFamilyNextEdition.Brand__c = 'MyLab + Note Taking Guide';
        insert prodFamilyNextEdition;
        
        //Insert Product Family
        Product2 prodFamily = new Product2();
           prodFamily.Configuration_Type__c='Option';     //CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           prodFamily.Market__c ='US';
           prodFamily.Line_of_Business__c='Higher Ed';
           prodFamily.Business_Unit__c   ='US Field Sales';
           prodFamily.name = 'NA Test Product Family';
           prodFamily.Author__c = 'Test Author'; 
           prodFamily.Edition__c = '10';
           prodFamily.Status__c = 'PUB';
           prodFamily.Relevance_Value__c = 11;
           prodFamily.Brand__c = 'MyLab + Note Taking Guide';
           prodFamily.Next_Edition__c  = prodFamilyNextEdition.id;  
        insert prodFamily;
        
        
        //Relating Product Family and Product
        RelatedProduct__c relProdandProdFamily = new RelatedProduct__c();
        relProdandProdFamily.CurrencyIsoCode = 'USD';
        relProdandProdFamily.Product__c = prodFamily.id;
        relProdandProdFamily.RelatedProduct__c = PIUprod.id;
        relProdandProdFamily.AssociationCategory__c = 'Alternate Binding';
        insert relProdandProdFamily;    
        
        //Insert Asset
       Asset asset = new Asset();
       asset.name = 'TestAsset';
       asset.Product2Id = PIUprod.id;
       asset.Primary__c = True;
       asset.Status__c = 'Active';
       asset.ContactId = lstCon[0].id;
       insert asset; 
        
       //Updating Relevance Value of Next Edition to 10
       prodFamilyNextEdition.Relevance_Value__c = 10;
       update  prodFamilyNextEdition;
        
        
    } 
    
    static testmethod void NegativeScenario_Rolloverchkdfromprod() {
        //Insert Account 
         Account acc = new Account();
        //acc.RecordTypeId = '012b0000000DpIM'; 
        acc.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
        acc.Name = 'Test Account';
        acc.Geography__c = 'Growth';
        acc.Line_of_Business__c = 'Higher Ed';
        acc.Market2__c = 'Global';
        
        insert acc;
        
        List<Contact> lstCon = TestDataFactory.createContacts(1);
        lstCon[0].accountid = acc.id;
        insert lstCon;
        
        //Insert Base Product
        Product2 PIUprod = new Product2();
           PIUprod.Configuration_Type__c='Option';     //CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           PIUprod.Duration__c = 'D10';
           PIUprod.name = 'NA Territory Product1';
           PIUprod.Line_of_Business__c = 'Higher Ed';
           PIUprod.Market__c ='US';
           PIUprod.Business_Unit__c='US Field Sales';
           PIUprod.Binding__c = 'Access Code Card'; 
           PIUprod.Edition__c = '10';
           PIUprod.Platform__c = 'Plat1';
           PIUprod.Relevance_Value__c = 10;
           PIUprod.Brand__c = 'Mastering';
           PIUprod.Medium2__c= 'Digital';
           PIUprod.Category2__c='CourseSmart';
           PIUprod.CurrencyIsoCode = 'USD';   
        insert PIUprod;
        
        //Insert Product Family Next Edition
        Product2 prodFamilyNextEdition = new Product2();
           prodFamilyNextEdition.Configuration_Type__c='Option';     //CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           prodFamilyNextEdition.Market__c ='US';
           prodFamilyNextEdition.Line_of_Business__c='Higher Ed';
           prodFamilyNextEdition.Business_Unit__c   ='US Field Sales';
           prodFamilyNextEdition.name = 'NA Test Product Family';
           prodFamilyNextEdition.Author__c = 'Test Author'; 
           prodFamilyNextEdition.Edition__c = '10';
           prodFamilyNextEdition.Status__c = 'PUB';
           prodFamilyNextEdition.Relevance_Value__c = 20;
           prodFamilyNextEdition.Brand__c = 'MyLab + Note Taking Guide';
        insert prodFamilyNextEdition;
        
        //Insert Product Family
        Product2 prodFamily = new Product2();
           prodFamily.Configuration_Type__c='Option';   //  CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           prodFamily.Market__c ='US';
           prodFamily.Line_of_Business__c='Higher Ed';
           prodFamily.Business_Unit__c   ='US Field Sales';
           prodFamily.name = 'NA Test Product Family';
           prodFamily.Author__c = 'Test Author'; 
           prodFamily.Edition__c = '10';
           prodFamily.Status__c = 'PUB';
           prodFamily.Relevance_Value__c = 11;
           prodFamily.Brand__c = 'MyLab + Note Taking Guide';
           prodFamily.Next_Edition__c  = prodFamilyNextEdition.id;  
        insert prodFamily;
        
        
        //Relating Product Family and Product
        RelatedProduct__c relProdandProdFamily = new RelatedProduct__c();
        relProdandProdFamily.CurrencyIsoCode = 'USD';
        relProdandProdFamily.Product__c = prodFamily.id;
        relProdandProdFamily.RelatedProduct__c = PIUprod.id;
        relProdandProdFamily.AssociationCategory__c = 'Alternate Binding';
        insert relProdandProdFamily;    
        
        //Insert Asset
       Asset asset = new Asset();
       asset.name = 'TestAsset';
       asset.Product2Id = PIUprod.id;
       asset.Primary__c = True;
       asset.Status__c = 'Active';
       asset.ContactId = lstCon[0].id;
       insert asset; 
        
       //Updating Relevance Value of Next Edition to 10
       prodFamilyNextEdition.Relevance_Value__c = 100;
       update  prodFamilyNextEdition;
        
        
    } 
    
    static testmethod void UpdateProductOnAssetPositive_Rolloverchkdfromprod() {
        //Insert Account 
         Account acc = new Account();
        //acc.RecordTypeId = '012b0000000DpIM'; 
        acc.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
        acc.Name = 'Test Account';
        acc.Geography__c = 'Growth';
        acc.Line_of_Business__c = 'Higher Ed';
        acc.Market2__c = 'Global';
        
        insert acc;
        
        List<Contact> lstCon = TestDataFactory.createContacts(1);
        lstCon[0].accountid = acc.id;
        insert lstCon;
        
        //Insert Base Product
        Product2 PIUprod = new Product2();
           PIUprod.Configuration_Type__c='Option';    // CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           PIUprod.Duration__c = 'D10';
           PIUprod.name = 'NA Territory Product1';
           PIUprod.Line_of_Business__c = 'Higher Ed';
           PIUprod.Market__c ='US';
           PIUprod.Business_Unit__c='US Field Sales';
           PIUprod.Binding__c = 'Access Code Card'; 
           PIUprod.Edition__c = '10';
           PIUprod.Platform__c = 'Plat1';
           PIUprod.Relevance_Value__c = 10;
           PIUprod.Brand__c = 'Mastering';
           PIUprod.Medium2__c= 'Digital';
           PIUprod.Category2__c='CourseSmart';
           PIUprod.CurrencyIsoCode = 'USD';   
        insert PIUprod;
        //Insert Base Product2
        Product2 PIUprod2 = new Product2();
           PIUprod2.Configuration_Type__c='Option';    // CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           PIUprod2.Duration__c = 'D10';
           PIUprod2.name = 'NA Territory Product1';
           PIUprod2.Line_of_Business__c = 'Higher Ed';
           PIUprod2.Market__c ='US';
           PIUprod2.Business_Unit__c='US Field Sales';
           PIUprod2.Binding__c = 'Access Code Card'; 
           PIUprod2.Edition__c = '10';
           PIUprod2.Platform__c = 'Plat1';
           PIUprod2.Relevance_Value__c = 10;
           PIUprod2.Brand__c = 'Mastering';
           PIUprod2.Medium2__c= 'Digital';
           PIUprod2.Category2__c='CourseSmart';
           PIUprod2.CurrencyIsoCode = 'USD';   
        insert PIUprod2;
        //Insert Product Family Next Edition
        Product2 prodFamilyNextEdition = new Product2();
           prodFamilyNextEdition.Configuration_Type__c='Option';    // CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           prodFamilyNextEdition.Market__c ='US';
           prodFamilyNextEdition.Line_of_Business__c='Higher Ed';
           prodFamilyNextEdition.Business_Unit__c   ='US Field Sales';
           prodFamilyNextEdition.name = 'NA Test Product Family';
           prodFamilyNextEdition.Author__c = 'Test Author'; 
           prodFamilyNextEdition.Edition__c = '10';
           prodFamilyNextEdition.Status__c = 'PUB';
           prodFamilyNextEdition.Relevance_Value__c = 10;
           prodFamilyNextEdition.Brand__c = 'MyLab + Note Taking Guide';
        insert prodFamilyNextEdition;
        
        //Insert Product Family
        Product2 prodFamily = new Product2();
           prodFamily.Configuration_Type__c='Option';    // CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           prodFamily.Market__c ='US';
           prodFamily.Line_of_Business__c='Higher Ed';
           prodFamily.Business_Unit__c   ='US Field Sales';
           prodFamily.name = 'NA Test Product Family';
           prodFamily.Author__c = 'Test Author'; 
           prodFamily.Edition__c = '10';
           prodFamily.Status__c = 'PUB';
           prodFamily.Relevance_Value__c = 11;
           prodFamily.Brand__c = 'MyLab + Note Taking Guide';
           prodFamily.Next_Edition__c  = prodFamilyNextEdition.id;  
        insert prodFamily;
        
        
        //Relating Product Family and Product
        RelatedProduct__c relProdandProdFamily = new RelatedProduct__c();
        relProdandProdFamily.CurrencyIsoCode = 'USD';
        relProdandProdFamily.Product__c = prodFamily.id;
        relProdandProdFamily.RelatedProduct__c = PIUprod2.id;
        relProdandProdFamily.AssociationCategory__c = 'Alternate Binding';
        insert relProdandProdFamily;  
        
        //Insert Asset
       Asset asset = new Asset();
       asset.name = 'TestAsset';
       asset.Product2Id = PIUprod.id;
       asset.Primary__c = True;
       asset.Status__c = 'Active';
       asset.ContactId = lstCon[0].id;
       insert asset; 
        
       //Updating Asset
       Asset astUpdate  = [select id,Product2Id from asset where id =:asset.id];
       astUpdate.Product2Id = PIUprod2.id;
        
        update  astUpdate;     
    }
    
    static testmethod void UpdateProductOnAssetNegative_Rolloverchkdfromprod() {
        //Insert Account 
         Account acc = new Account();
        //acc.RecordTypeId = '012b0000000DpIM'; 
        acc.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
        acc.Name = 'Test Account';
        acc.Geography__c = 'Growth';
        acc.Line_of_Business__c = 'Higher Ed';
        acc.Market2__c = 'Global';
        
        insert acc;
        
        List<Contact> lstCon = TestDataFactory.createContacts(1);
        lstCon[0].accountid = acc.id;
        insert lstCon;
        
        //Insert Base Product
        Product2 PIUprod = new Product2();
           PIUprod.Configuration_Type__c='Option';    // CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           PIUprod.Duration__c = 'D10';
           PIUprod.name = 'NA Territory Product1';
           PIUprod.Line_of_Business__c = 'Higher Ed';
           PIUprod.Market__c ='US';
           PIUprod.Business_Unit__c='US Field Sales';
           PIUprod.Binding__c = 'Access Code Card'; 
           PIUprod.Edition__c = '10';
           PIUprod.Platform__c = 'Plat1';
           PIUprod.Relevance_Value__c = 10;
           PIUprod.Brand__c = 'Mastering';
           PIUprod.Medium2__c= 'Digital';
           PIUprod.Category2__c='CourseSmart';
           PIUprod.CurrencyIsoCode = 'USD';   
        insert PIUprod;
        
        //Insert Base Product2
        Product2 PIUprod2 = new Product2();
           PIUprod2.Configuration_Type__c='Option';    // CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           PIUprod2.Duration__c = 'D10';
           PIUprod2.name = 'NA Territory Product1';
           PIUprod2.Line_of_Business__c = 'Higher Ed';
           PIUprod2.Market__c ='US';
           PIUprod2.Business_Unit__c='US Field Sales';
           PIUprod2.Binding__c = 'Access Code Card'; 
           PIUprod2.Edition__c = '10';
           PIUprod2.Platform__c = 'Plat1';
           PIUprod2.Relevance_Value__c = 10;
           PIUprod2.Brand__c = 'Mastering';
           PIUprod2.Medium2__c= 'Digital';
           PIUprod2.Category2__c='CourseSmart';
           PIUprod2.CurrencyIsoCode = 'USD';   
        insert PIUprod2;
        
        //Insert Product Family Next Edition
        Product2 prodFamilyNextEdition = new Product2();
           prodFamilyNextEdition.Configuration_Type__c='Option';    // CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           prodFamilyNextEdition.Market__c ='US';
           prodFamilyNextEdition.Line_of_Business__c='Higher Ed';
           prodFamilyNextEdition.Business_Unit__c   ='US Field Sales';
           prodFamilyNextEdition.name = 'NA Test Product Family';
           prodFamilyNextEdition.Author__c = 'Test Author'; 
           prodFamilyNextEdition.Edition__c = '10';
           prodFamilyNextEdition.Status__c = 'PUB';
           prodFamilyNextEdition.Relevance_Value__c = 100;
           prodFamilyNextEdition.Brand__c = 'MyLab + Note Taking Guide';
        insert prodFamilyNextEdition;
        
        //Insert Product Family
        Product2 prodFamily = new Product2();
           prodFamily.Configuration_Type__c='Option';   //  CR-02949 - Replaced the Apttus to Non-Apttus changes by Vinoth
           prodFamily.Market__c ='US';
           prodFamily.Line_of_Business__c='Higher Ed';
           prodFamily.Business_Unit__c   ='US Field Sales';
           prodFamily.name = 'NA Test Product Family';
           prodFamily.Author__c = 'Test Author'; 
           prodFamily.Edition__c = '10';
           prodFamily.Status__c = 'PUB';
           prodFamily.Relevance_Value__c = 11;
           prodFamily.Brand__c = 'MyLab + Note Taking Guide';
           prodFamily.Next_Edition__c  = prodFamilyNextEdition.id;  
        insert prodFamily;
        
        
        //Relating Product Family and Product
        RelatedProduct__c relProdandProdFamily = new RelatedProduct__c();
        relProdandProdFamily.CurrencyIsoCode = 'USD';
        relProdandProdFamily.Product__c = prodFamily.id;
        relProdandProdFamily.RelatedProduct__c = PIUprod2.id;
        relProdandProdFamily.AssociationCategory__c = 'Alternate Binding';
        insert relProdandProdFamily;    
        
        //Insert Asset
       Asset asset = new Asset();
       asset.name = 'TestAsset';
       asset.Product2Id = PIUprod.id;
       asset.Primary__c = True;
       asset.Status__c = 'Active';
       asset.ContactId = lstCon[0].id;
       insert asset; 
        
       //Updating Asset
       asset.Product2Id = PIUprod2.id;
       update  asset;
        
        
    }
    
}