/*******************************************************************************************************************
* Apex Class Name  : RetrieveOneCRMTestTest
* Version          : 1.0 
* Created Date     : 30 April 2015
* Function         : Test Class of the RetrieveOneCRMTest
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*                         24/04/2015              Created Initial Version of AccountContactSyncTestClass
* Karan Khanna            24/12/2015              Updated test data to meet R3 validation rules
  Rajesh Paleti           17/10/2019              Take out Apttus related objects&Fields and replaced with standard objects&Fields 
*******************************************************************************************************************/
@isTest
public class RetrieveOneCRMTest_Clone
{
  /*************************************************************************************************************
  * Name        : generateTestCases
  * Description : Generate Cases records
  * Input       : NumOfCases - Number of case records to generate
  * Output      : List of the Case records generated
  *************************************************************************************************************/
  private static List<Case> generateTestCases(Integer numOfCases, Boolean setExternalId)
  { 
      //GK
     
    List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId(),1);
    usrLst[0].Market__c = 'US';
    insert usrLst;
    
    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Workflow_Rules__c=true,Disable_Process_Builder__c=true);
    insert byp; 
    List<Case> casesToInsert = new List<Case>();
    system.runas(usrLst[0])  {  
        
        //test.startTest();  
        //Generate an account
        Account acc = new Account(Name = 'Account 1',Pearson_Campus__c=true);
        if(setExternalId)
        {
          acc.External_Account_Number__c = 'External';  
        }
        insert acc;
            
        //Generate contact
        Contact con = new Contact(LastName = 'Contact 1', FirstName = 'fn', Email = 'test@test.com.demo', AccountId = acc.Id,Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',Salutation='Mr.',MobilePhone='+914566',First_Language__c='English');
        insert con;
        
        AccountContact__c accCon = new  AccountContact__c(Account__c = acc.Id, Contact__c = con.Id, AccountRole__c = 'Role', Primary__c = true, Financially_Responsible__c = True);
        insert accCon;
        
        Account_Correlation__C ac = new Account_Correlation__C(Account__C = acc.Id, External_ID_Name__c = 'eVision Learner Number', External_ID__c = 'External ID');
        insert ac;
         
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Case' and DeveloperName = 'Loan_Bursary_Request'];                        
        for(Integer i=0; i<numOfCases; i++)
        {    
          Case caseToInsert = new Case( AccountId = acc.Id, RecordTypeid = rt.id, Type ='General', ContactId = con.Id, Sponsor_name__c = con.Id, Reason_if_Other__c = 'Other');          
          casesToInsert.add(caseToInsert);
        }
            
        
        if(casesToInsert.size()>0)
        {
          insert casesToInsert;
        }
        //test.stopTest(); 
        
    }//End RunAS  
      return casesToInsert;
      
  }
  
  static testMethod void verifyRetrieveCaseById1()
  {     
      
    List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
    usrLst[0].Market__c = 'US';
    insert usrLst;
    
    //List<Case> generatedCases = generateTestCases(1, false);
    List<Case> generatedCases = generateTestCases(1, true);
    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Workflow_Rules__c=true,Disable_Process_Builder__c=true);
    insert byp;
    
    System.runas(usrLst[0])
    {  
    test.startTest();
    RetrieveOneCRM.RetrieveCaseResult result = RetrieveOneCRM.RetrieveCaseById(generatedCases.get(0).Id);
    System.assertNotEquals(result, null, 'Returned Result is null');
    test.stopTest();
    }
  }
}