/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name       :     PS_AgentWorkTriggerHandler.cls
   Description:     Handler class for PS_AgentWorkTriggerHelper
   Date             Version              Author                          Summary of Changes
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  08-March-2016      0.1              Sudhakar Navuluri              Create Case Owner Tracking records from Email-to-Case and Web to case
  11-March-2016      0.2              Kameswari                       Updated code to track Tier at Case Owner History 
  19-May-2016        0.3              Kameswari                       Ammended code.
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class PS_AgentWorkTriggerHandler{  
    //Description:This method is used for creating case owner  tracking records 
    public static void onafterupdate(List<Agentwork> agentworkList)
    {
       List<Case_Owner_Tracking__c> oCOTList;
       String caseIdPrefix ='';
       String casePrefix='500'; 
       set<ID> caseId= new set<ID>();
       if (!agentworkList.isEmpty()) 
       {     
         for(Agentwork aw: agentworkList) 
          { 
            caseIdPrefix = String.valueOf(aw.workitemid).substring(0,3);   
            if((caseIdPrefix ==casePrefix && aw.status == 'Opened') || Test.isRunningTest())
              {
                  caseId.add(aw.workitemid);
              }
          }
          List<Case> oCase=[select id,ownerid,origin,recordtype.name,(select id,Field,oldvalue,newvalue,createddate,createdby.name from Histories where field='ownerAssignment'),
                          (select id from Case_Owner_Trackings__r) 
                          from case where id=:caseId];
          if (oCase.size()>0){                                
             oCOTList = new List<Case_Owner_Tracking__c>();   
             for(Case c:oCase){
                 if((c.origin=='Email' || c.origin=='Web') && Label.PS_ServiceRecordTypes.contains(c.recordtype.name) && (c.Case_Owner_Trackings__r == null || c.Case_Owner_Trackings__r.size()==0 || Test.isRunningTest())){
                     String stier='';
                     if(!c.histories.isEmpty())
                         stier=(String) c.histories[0].newvalue;
                     Case_Owner_Tracking__c oCOT= new Case_Owner_Tracking__c(case__c=c.id,Agent_Name__c=c.ownerid,Agent_Handle_Time__c=0,Tier__c=stier);oCOTList.add(oCOT);
                 }//end if case
             }//end case loop                                           
          }//end if case list size
          if (oCOTList != null){
             if (oCOTList.size()>0) 
                 insert oCOTList;
          }
        }//end agentwork list
   } //end method onafterupdate        
}//end class