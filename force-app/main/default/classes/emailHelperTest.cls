/************************************************************************************************************
* Apex Interface Name : emailHelperTest
* Version             : 1.0 
* Created Date        : 3/27/2017
* Modification Log    : 
* Developer              CP     Date                
* -----------------------------------------------------------------------------------------------------------
* Cristina Perez        3/27/2017      Class to send email with templates to user, specialy to support catching errors     
*   
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class emailHelperTest {
    static testMethod void validateSendTemplateEmailToUser(){
       
        OrgWideEmailAddress owa = [select id, DisplayName, Address from OrgWideEmailAddress limit 1];
        //create Contact
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].email = 'cperez@ourstuffconsulting.com';
       
        insert lstContact;
 
        String contactURL = ' ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + lstContact[0].ID;       
        String emailBody  = Label.Physical_Address_Error_Email1  + contactURL + Label.Physical_Address_Error_Email2;
 
        Test.StartTest();
        
        emailHelper.sendEmailToUser(EmailBody, lstContact[0].ID,owa.ID);
        
        Test.StopTest();
    }
}