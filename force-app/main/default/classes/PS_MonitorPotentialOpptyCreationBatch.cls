global class PS_MonitorPotentialOpptyCreationBatch implements Schedulable {  
    /*@Method <This method monitor's potential target job and kicks it off if it did not ran in 90 minutes>
    @param <Database.BatchableContext BC - Batchable context>
    @param SchedulableContext SC
    @return <void> - <Not returning anything>
    @throws exception - No Exception Thrown , Send email from Catch Block if there is Excetion
    */
    global Integer runningBatchJobs;
    global Integer runningPotentialTargetJobs;
    global void execute(SchedulableContext SC){
         PS_PotentialTargetOpptyCreationBatch  potenTargetBatch= new PS_PotentialTargetOpptyCreationBatch('Potential Target Monitor Query Limit');
         General_One_CRM_Settings__c  setting = General_One_CRM_Settings__c.getInstance('Potential Target Monitor Btch Size Lmt');
         General_One_CRM_Settings__c  restartMin = General_One_CRM_Settings__c.getInstance('Potential Target Monitor Restart Mins');
        try{
             if(!Test.isRunningTest()) //Added the code just to cover the test class coverage.
             { 
                 runningBatchJobs = [select count() from AsyncApexJob where JobType = 'BatchApex' and status in ('Queued','Processing','Preparing') limit 5000]; 
                 //runningPotentialTargetJobs = [select count() from AsyncApexJob where ApexClass.Name='PS_PotentialTargetOpptyCreationBatch' and (CompletedDate >: Datetime.now().addMinutes(Integer.valueof(restartMin) ) OR Status='Processing')];  
                 runningPotentialTargetJobs = [select count() from AsyncApexJob where ApexClass.Name='PS_PotentialTargetOpptyCreationBatch' and (CompletedDate >: Datetime.now().addMinutes(-120) OR Status='Processing')];  
             
             }
            else{
              runningBatchJobs = 10; //Added the code just to cover the test class coverage.
              runningPotentialTargetJobs = 1;  
            }
             
             System.debug(' runningBatchJobs --->'+runningBatchJobs);
             if( runningPotentialTargetJobs==0 ){
                 if (setting != null)
                    database.executebatch(potenTargetBatch, Integer.valueOf(setting.Value__c));
                 else
                    database.executebatch(potenTargetBatch);
             }
                
            DateTime n = datetime.now().addMinutes(Integer.valueOf(restartMin.Value__c));
            String cron = '';
            cron += n.second();
            cron += ' ' + n.minute();
            cron += ' ' + n.hour();
            cron += ' ' + n.day();
            cron += ' ' + n.month();
            cron += ' ' + '?';
            cron += ' ' + n.year();
            String jobName = 'Batch Job To Monitor Potential Target batch - ' + n.format('MM-dd-yyyy-hh:mm:ss');
            PS_MonitorPotentialOpptyCreationBatch nextBatch = new PS_MonitorPotentialOpptyCreationBatch();
            Id scheduledJobID = System.schedule(jobName,cron,nextBatch); 
                    
        }     
        
        catch(Exception e){
           //Uncomment this below part to send mail when there is a failure
           /* String supportEmail = '';//Support Email to be added
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {supportEmail};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Attention: Error in Batch Job to send OBMs');
            String strPlainTextBody = 

             'Fatal Error :'+e;
             
            mail.setPlainTextBody(strPlainTextBody);
              
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); */
            system.debug('Catch block to send a mail if any exception raised during Scheduler');
        } 
    }
}