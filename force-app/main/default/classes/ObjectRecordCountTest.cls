@isTest
public class ObjectRecordCountTest{
    /*static testMethod void myTest() {
       ObjectRecordCount orc = new ObjectRecordCount();
       orc.updateRecordCounts();
     }*/
    
    
    static testMethod void myTest(){
        test.startTest();
        ObjectRecordCount orc = new ObjectRecordCount();
        ObjectRecordCountClient__c obj = new ObjectRecordCountClient__c();
        obj.Name = 'Credential';
        obj.ClientId__c='adaacacaacac';
        obj.Client_Secret__c= '1236584656';
        insert obj;
        ObjectRecordUserCredentials__c objRecCre = new ObjectRecordUserCredentials__c();
        objRecCre.Name = 'UserCredentials';
        objRecCre.UserName__c = 'testuser';
        objRecCre.UserPassword__c = 'test1224';
        insert objRecCre;
        
        ObjectRecordCounts__c orr = new ObjectRecordCounts__c();
        Test.SetMock(HttpCallOutMock.class, new ObjectRecordMock());

        ObjectRecordCount.updateRecordCounts();
        
        test.stopTest();
    }
    
}