/************************************************************************************************************
* Apex Interface Name : ComplaintFeedbackSupportTest
* Version             : 1.0 
* Created Date        : 9/19/2016 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Supriyam                 9/19/2016          
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class ComplaintFeedbackSupportTest {
 /*
     * Method to cover multiple methods
     */ 
     static testMethod void test_ComplaintFeedbackSupportTest(){
         String communityUserProfileName = 'Pearson Self-Service User';
        List<User> userList = new List<User>();
          UserRole tempUserRole;
          User userRec, usr;
                   //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
         //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
         System.runAs(usr){      
            //user for 'Customer Community Login' license
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
     }
           Test.StartTest();
         ComplaintFeedbackSupport.getCurrentUser();
      }
    /*
     * Method to cover test_getValues methods
     */
     static testMethod void test_getValues()
     {
         String communityUserProfileName = 'Pearson Self-Service User';
       // String accountRecType = 'Organisation';
        //String caseRecType = 'Customer Service';
        String caseRecTypeAPIName = System.Label.CaseComplaintRecordType;
        String contactRecType = System.Label.ContactGlobalRecordTypeName;
        List<User> userList = new List<User>();
        List<Account> accountList = new List<Account>();
        List<Case> caseList = new List<Case>();
         String FileId='';
         String fileName='word';
         String base64Data='';
         String contentType='';
         String Title='';
         
         caseList = TestDataFactory.createCase_SelfService(1,caseRecTypeAPIName);
            insert caseList;
        List<Contact_Roles_UK__c> roleList = new List<Contact_Roles_UK__c>();
        Contact_Roles_UK__c role;
         List<Request_Type__c> Request_TypeList = new List<Request_Type__c>();
        Request_Type__c Request_Type;
        String successMsgStr, detailsStr;
        user caseOwnerUsr = new user();
        User usr;
        UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        insert usr;
        System.runAs(usr){
            Test.StartTest();

            role = new Contact_Roles_UK__c(name='Head_Principal_CurriculumManager__c',Contact_Role_Mapp__c='Educator', Role__c='Student, Parent, or Carer');
            roleList.add(role);
            insert roleList;
           
             Request_Type = new Request_Type__c(name='Complaint__c',Field_on_case__c='Complaint', 	Request_Type__c='TESTComplaint');
            Request_TypeList.add(Request_Type);
            insert Request_TypeList;
            Test.StopTest();
        }
         System.runAs(usr)
         {
           // Test.StartTest();
            successMsgStr = ComplaintFeedbackSupport.getValues(FileId,'01p7E000000AMN3QAO',fileName,base64Data,contentType,String.valueOf(Request_TypeList[0].Request_Type__c),String.valueOf(roleList[0].Role__c),'Test','Test_FIrstName','Test_LastName','TestCenter','1234567890','263153','123456','test@gmail.com','1234567890','CaseCReation');

              System.assert(successMsgStr.contains('Your case has been created successfully.'));
            //     Test.StopTest();
      }
     }
     /*
     * Method to cover getRequestType methods
     */
     static testMethod void test_getRequestType()
     {
          List<Contact_Roles_UK__c> roleList = new List<Contact_Roles_UK__c>();
        Contact_Roles_UK__c role;
          List<Request_Type__c> Request_TypeList = new List<Request_Type__c>();
        Request_Type__c Request_Type;
          User usr;
        UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
         
              //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        insert usr;
         System.runAs(usr){
            Test.StartTest();

            role = new Contact_Roles_UK__c(name='Head_Principal_CurriculumManager__c',Contact_Role_Mapp__c='Educator', Role__c='Student, Parent, or Carer');
            roleList.add(role);
            insert roleList;
           
             Request_Type = new Request_Type__c(name='Complaint__c',Field_on_case__c='Complaint', 	Request_Type__c='TESTComplaint');
            Request_TypeList.add(Request_Type);
            insert Request_TypeList;
            Test.StopTest();
        }
          System.runAs(usr)
         {
             ComplaintFeedbackSupport.getRequestType();
             ComplaintFeedbackSupport.getRoleMapping();
         }
     }
     /*
     * Method to cover saveTheFile methods
     */
     static testMethod void test_saveTheFile(){
          User usr;
          UserRole tempUserRole;
         String FileId='01p7E0000009ygBQAQ';
         String fileName='';
         String base64Data='';
         String contentType='';
         String Title='';
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
         
              //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        insert usr;
          System.runAs(usr){
            Test.StartTest();
              ComplaintFeedbackSupport.saveTheFile('01p7E000000AMN3QAO',fileName,base64Data,contentType);
                Test.StopTest();
        }
         
     }
     /*
     * Method to cover appendToFile methods
     */
     static testMethod void test_appendToFile()
     {
          User usr;
          UserRole tempUserRole;
         String FileId='';
         String base64Data='';
         Id parentId;
        String caseRecTypeAPIName = System.Label.CaseComplaintRecordType;
        String contactRecType = System.Label.ContactGlobalRecordTypeName;
       
              //user to avoid Mixed DML error
        //usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', U);
        //insert usr;
        List<User> usrlst =TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id);
         RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND Name =: caseRecTypeAPIName ];
          Case caseDetails;
          caseDetails = new Case(Status ='New',Priority='Low',Origin ='Phone',
                                           RecordTypeId = rt.id);
        insert caseDetails;
         System.runAs(usrlst[0]){
             Test.StartTest();
        Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
        attach.parentId=caseDetails.Id;
             system.debug('case id'+caseDetails.Id);
        insert attach;
             try {
          ComplaintFeedbackSupport.appendToFile(attach.Id,base64Data);
        } catch (Exception e) {
           System.debug('An error occurred');
        }
                Test.StopTest();
        }
     }
 
}