global with sharing class CommunityTypePicklist extends VisualEditor.DynamicPickList {
    
    global override VisualEditor.DataRow getDefaultValue() {
        return getValues().get(0);
    }
    
    global override VisualEditor.DynamicPickListRows getValues() {
        Schema.DescribeFieldResult fieldResult = Community_Content__c.Community_Type__c.getDescribe();
        VisualEditor.DynamicPickListRows contentTypeValues = new VisualEditor.DynamicPickListRows();
        for(Schema.PicklistEntry pLE : fieldResult.getPicklistValues())
        {
            contentTypeValues.addRow(new VisualEditor.DataRow(pLE.getLabel(), pLE.getValue()));
        }  
        return contentTypeValues;
    }
}