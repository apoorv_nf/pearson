/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER        DESCRIPTION                               
 |  ====            =========        =========== 
 | 
     24/11/2015      Rahul Garje     		 for change in the subject line 
     1/12/2015       Rahul Garje    		 modified to add pearson account number
     2/12/2015       Rahul Garje    		 modified to add pearson account number and account name
     3/12/2015       kyama             		 modified class definition for D-2892. 
     5/23/2017       Jaydip B                Modified teh code not to blank out Account number field
     11/10/2017      Saritha Singamsetty     Modified for CR-01668 for sending emails to SPG and HE CS Contract Team. 
     30/07/2017      Tamizharasan	     	 Added a condition to check the business unit
 +===========================================================================*/
public class SpecialPriceRequestTriggerHandler {
 
    private boolean m_isExecuting = false;
    private integer iBatchSize = 0;
 
    public SpecialPriceRequestTriggerHandler(boolean isExecuting, integer iSize){
        m_isExecuting = isExecuting;
        iBatchSize = iSize;
    }
 
    public void OnBeforeInsert(Special_Price_Request__c[] updatedObjects){
        // EXECUTE BEFORE INSERT LOGIC
       
        set<Id> setOppIds = new set<Id>();
        map<id,list<OpportunityTeamMember>> mapOppIdOppteam = new map<Id,list<OpportunityTeamMember>>();// stores Opportunity Id and List of Opp team Members.
        map<id,list<Relationship__c>> mapAccRelation = new map<Id,list<Relationship__c>>();
        Map<Id,Id> mapOppIdOUC = new Map<Id,Id>();
        Map<Id,Id> mapOppSpl= new Map<Id,Id>();
        Map<Id,Opportunity> lstOpp;// = new list<Opportunity>(); // Stores list of opportunity which satisfy the condition of status = 'completed'
        set<Id> setAccountId = new set<Id>();
        system.debug('Before Insert trigger Special Pricing Request');
        for(Special_Price_Request__c objspecial:updatedObjects){
                setOppIds.add(objspecial.Opportunity__c);
                mapOppSpl.put(objspecial.Id,objspecial.Opportunity__c);
        }
       
        if(setOppIds != null && !setOppIds.isEmpty()){
            //lstOpp = new Map<Id,Opportunity>([select id,Name ,AccountId,Special_Price_Request_Open__c,(Select UserId, Name, TeamMemberRole, Job_Function__c From OpportunityTeamMembers WHERE Job_Function__c = 'Learning Technology Specialist'),(Select id, Opportunity__c, UniversityCourse__c from OpportunityUniversityCourses__r) from Opportunity WHERE Id IN: setOppIds]);  //commented by Rahul
            lstOpp = new Map<Id,Opportunity>([select id,Name ,AccountId,Account.Primary_Selling_Account__c,Special_Price_Request_Open__c,(Select UserId, Name, TeamMemberRole, Job_Function__c From OpportunityTeamMembers WHERE Job_Function__c = 'Learning Technology Specialist'),(Select id, Opportunity__c, UniversityCourse__c from OpportunityUniversityCourses__r) from Opportunity WHERE Id IN: setOppIds]);
    //code added for Special_Price_Request_Open__c checkbox
        //List<Opportunity> oppToUpdate= new List<Opportunity>();
            for(Special_Price_Request__c objspecial:updatedObjects){
             if(lstOpp.get(mapOppSpl.get(objspecial.Id)) != null){ 
                 
            Opportunity op =  lstOpp.get(mapOppSpl.get(objspecial.Id));
            system.debug('Opportunity :'+op);
           // oppToUpdate.add(lstOpp.Opportunity__r)
            if(op.Special_Price_Request_Open__c){
                objspecial.addError('A Special Price Request is already open');
            }
            else{
               op.Special_Price_Request_Open__c = true;
            }
           
                //setAccountId.add(op.AccountId); //commented by Rahul
                setAccountId.add(op.Account.Primary_Selling_Account__c);
                System.debug(LoggingLevel.INFO,'@@OpportunityTeamMembers:'+op.OpportunityTeamMembers); 
                 System.debug(LoggingLevel.INFO,'@@OpportunityTeamMembers:'+op.OpportunityTeamMembers.size()); 
                if(op.OpportunityTeamMembers.size()>0){
                    for(OpportunityTeamMember objteam:op.OpportunityTeamMembers){
                        list<OpportunityTeamMember> tempTeam = new list<OpportunityTeamMember>();
                      if(mapOppIdOppteam.get(op.Id) == null){
                          tempTeam.add(objteam);
                          mapOppIdOppteam.put(op.Id,tempTeam);
                      }else{
                          tempTeam = mapOppIdOppteam.get(op.Id);
                          tempTeam.add(objteam);
                         mapOppIdOppteam.put(op.Id,tempTeam);
                      }
                    }                  
                }
                // Assignee first courses on the opportunity to the special price request.
                if(op.OpportunityUniversityCourses__r.size()>0){
                    for(OpportunityUniversityCourse__c OUC:op.OpportunityUniversityCourses__r){
                        //List<OpportunityUniversityCourse__c> OUCtemp = new List<OpportunityUniversityCourse__c>();
                        if(mapOppIdOUC.get(OUC.Opportunity__c) == null && OUC.UniversityCourse__c != null){
                            mapOppIdOUC.put(OUC.Opportunity__c, OUC.UniversityCourse__c);
                        }
                    }
                }
            }
            }
       /* for(Opportunity op: lstOpp){
           // oppToUpdate.add(lstOpp.Opportunity__r)
            if(op.Special_Price_Request_Open__c= true){
                this.addError('A Special Price Request is already open');
            }
            else{
           op.Special_Price_Request_Open__c= true;
            }
 
       }
*/
           /* for(Opportunity obj:lstOpp){
                setAccountId.add(obj.AccountId);
                if(obj.OpportunityTeamMembers.size()>0){
                    for(OpportunityTeamMember objteam:obj.OpportunityTeamMembers){
                        list<OpportunityTeamMember> tempTeam = new list<OpportunityTeamMember>();
                      if(mapOppIdOppteam.get(obj.Id) == null){
                          tempTeam.add(objteam);
                          mapOppIdOppteam.put(obj.Id,tempTeam);
                      }else{
                          tempTeam = mapOppIdOppteam.get(obj.Id);
                          tempTeam.add(objteam);
                          mapOppIdOppteam.put(obj.Id,tempTeam);
                      }
                    }                  
                }
                // Assignee first courses on the opportunity to the special price request.
                if(obj.OpportunityUniversityCourses__r.size()>0){
                    for(OpportunityUniversityCourse__c OUC:obj.OpportunityUniversityCourses__r){
                        //List<OpportunityUniversityCourse__c> OUCtemp = new List<OpportunityUniversityCourse__c>();
                        if(mapOppIdOUC.get(OUC.Opportunity__c) == null && OUC.UniversityCourse__c != null){
                            mapOppIdOUC.put(OUC.Opportunity__c, OUC.UniversityCourse__c);
                        }
                    }
                }
            }*/
           
            for(Account objAcc:[Select id,Name, (Select Id, Name, Related_To__c,Related_To__r.Name,Related_To__r.Pearson_Account_Number__c From Relationships__r) From Account WHERE Id IN: setAccountId]){
                if(objacc.Relationships__r.size()>0){
                    for(Relationship__c objr:objacc.Relationships__r){
                        list<Relationship__c> tempRelation = new list<Relationship__c>();
                      if(mapAccRelation.get(objAcc.Id) == null){
                          tempRelation.add(objr);
                          mapAccRelation.put(objAcc.Id,tempRelation);
                      }else{
                          tempRelation = mapAccRelation.get(objAcc.Id);
                          tempRelation.add(objr);
                          mapAccRelation.put(objAcc.Id,tempRelation);
                      }
                    }
                }
            }
           
            
            for(Special_Price_Request__c objspecial:updatedObjects){
                String objAccountNum = '';
                //objspecial.Account_Numbers__c = '';
                if(mapOppIdOppteam.containsKey(objspecial.Opportunity__c) && mapOppIdOppteam.get(objspecial.Opportunity__c) != null ){
                    objspecial.LTS_Name__c = mapOppIdOppteam.get(objspecial.Opportunity__c)[0].UserId;
                }
                System.debug('objspecial-->'+objspecial);
                System.debug('objspecial Relation_Pearson_Account_Number__c-->'+objspecial.Relation_Pearson_Account_Number__c);
                System.debug('mapAccRelation outside -->'+mapAccRelation);
                if(mapAccRelation.containsKey(objspecial.Relation_Pearson_Account_Number__c) && mapAccRelation.get(objspecial.Relation_Pearson_Account_Number__c) != null){   //removed this condition by Rahul for D-2813   
                   System.debug('mapAccRelation -->'+mapAccRelation);
                    for(Relationship__c objr : mapAccRelation.get(objspecial.Relation_Pearson_Account_Number__c)){
                        //code commented by Rahul for Defect D-2813
                        /*if(objr.Related_To__r.Pearson_Account_Number__c != null){
                            objAccountNum += objr.Related_To__r.Pearson_Account_Number__c +'\r\n';
                        }*/
            if(objr.Related_To__r.Pearson_Account_Number__c != null && objr.Related_To__r.Name != null){
                            objAccountNum += objr.Related_To__r.Name+' - '+objr.Related_To__r.Pearson_Account_Number__c +'\r\n';
                        }
                    }
                    //Jaydip B added if clause on 5/23/2017
                    if(objAccountNum <> '') {
                        objspecial.Account_Numbers__c = objAccountNum;
                    }
                }
                if(mapOppIdOUC.get(objspecial.Opportunity__c) != null){
                    objspecial.Course__c = mapOppIdOUC.get(objspecial.Opportunity__c);
                }
            }
   
                if(lstOpp != null && !lstOpp.isEmpty()){
                   
                    try{
                        update lstOpp.values();
                    }catch (exception E){
                        System.debug('Erron in updating Opportunity :'+ E );
                    }
                }
        }
    }
 
    public void OnAfterInsert(Special_Price_Request__c[] newObjects){
        // EXECUTE AFTER INSERT LOGIC
        List<Id> oppty_Id = new List<Id>();
        Map<Id,List<OpportunityLineItem>> MapOpptyLineitem = new Map<Id,List<OpportunityLineItem>>();
        List<Special_Price_Products__c> Specialproduct = new List<Special_Price_Products__c>();
        system.debug ('in SpecialPriceRequestTriggerHandler OnAfterInsert method');
        for(Special_Price_Request__c eachspecialprice :newObjects){
            if(eachspecialprice.Opportunity__c != null){
               oppty_Id.add(eachspecialprice.Opportunity__c);
            }
        }
       
        if(oppty_Id != null && !oppty_Id.isEmpty()){
          List<OpportunityLineItem> OpptyLineItem = [Select Id, OpportunityId, Product_Family_HE__c,PricebookEntry.Product2.Product_Family__c, OptionId__c, Quantity, UnitPrice, Product2Id, CurrencyIsoCode from OpportunityLineItem where OpportunityId in: oppty_Id];
          if(OpptyLineItem != null && !OpptyLineItem.isEmpty()){
              for(OpportunityLineItem eachopptylineitem :OpptyLineItem){
                  List<OpportunityLineItem> tempopptyline = new List<OpportunityLineItem>();
                  if(MapOpptyLineitem.get(eachopptylineitem.OpportunityId) == null){
                      tempopptyline.add(eachopptylineitem);
                      MapOpptyLineitem.put(eachopptylineitem.OpportunityId,tempopptyline);
                  }else{
                      tempopptyline = MapOpptyLineitem.get(eachopptylineitem.OpportunityId);
                      tempopptyline.add(eachopptylineitem);
                      MapOpptyLineitem.put(eachopptylineitem.OpportunityId,tempopptyline);
                  }
                      
                  }
                for(Special_Price_Request__c eachspecialprice :newObjects){
                    if(eachspecialprice.Opportunity__c != null && MapOpptyLineitem.get(eachspecialprice.Opportunity__c) != null){
                        for(OpportunityLineItem eachopptylineitem :MapOpptyLineitem.get(eachspecialprice.Opportunity__c)){
                            Special_Price_Products__c tempspecialproduct = new Special_Price_Products__c();
                            tempspecialproduct.Special_Price_Request__c = eachspecialprice.Id;
                            tempspecialproduct.Opportunity__c = eachopptylineitem.OpportunityId;
                            tempspecialproduct.Product__c = eachopptylineitem.Product2Id;
                            tempspecialproduct.Quantity__c = eachopptylineitem.Quantity;
                            tempspecialproduct.UnitPrice__c = eachopptylineitem.UnitPrice;
                            tempspecialproduct.Standard_Product__c = eachopptylineitem.Product_Family_HE__c;
                            tempspecialproduct.Standard_Product_Family__c = eachopptylineitem.PricebookEntry.Product2.Product_Family__c;
                            tempspecialproduct.CurrencyIsoCode = eachopptylineitem.CurrencyIsoCode;
                            tempspecialproduct.Opportunity_Line_Item__c = eachopptylineitem.Id;
                                                     
                            Specialproduct.add(tempspecialproduct);
                        }
                       
                        
                    }
                }
               
                if(Specialproduct != null && !Specialproduct.isEmpty()){
                   
                    try{
                        insert Specialproduct;
                    }catch (exception E){
                        System.debug('Erron in inserting Special_Price_Products :'+ E );
                    }
                }
                 
              }
            
              
          }
         
        }
       
    public void OnBeforeUpdate(Special_Price_Request__c[] oldObjects, Special_Price_Request__c[] updatedObjects, map<ID, Special_Price_Request__c> MapObjectMap){
        //BEFORE UPDATE LOGIC
        
        System.debug('in before update trigger');
        
        List<Special_Price_Request__c>updatedRequests= new List<Special_Price_Request__c>();
        for(Special_Price_Request__c eachspecialprice: updatedObjects){
            
            if(eachspecialprice.Status__c == 'Completed' && MapObjectMap.get(eachspecialprice.Id).Status__c !='Completed')
                updatedRequests.add(eachspecialprice);
        }
        if(updatedRequests != null && ! updatedRequests.isEmpty()){
            User Usr = new User();
            Usr = [SELECT  Id FROM User WHERE Id = : UserInfo.getUserId()];
            system.debug('Usr  :'+Usr);
            // Added by Saritha Singamsetty for CR-01668 starts here                          
            //Group Queue= [Select Id from Group where type ='Queue' and DeveloperName = 'Special_Pricing_Monitoring_Group' Limit 1];
            //List<GroupMember> QueueMembers = [Select UserOrGroupId From GroupMember where GroupId =: Queue.Id and UserOrGroupId =: Usr.Id];
            List<Group> Queue= [Select Id from Group where type ='Queue' and DeveloperName IN ('Special_Pricing_Monitoring_Group','HE_CS_Contract_Team')];
            system.debug('@@Queue'+Queue);
            List<GroupMember> QueueMembers = [Select UserOrGroupId From GroupMember where GroupId IN: Queue and UserOrGroupId =: Usr.Id];
            List<Id> userIds = new List<Id>();
            List<Id> GroupIds = new List<Id>();
            List<Id> UserId = new List<Id>();
            system.debug('QueueMembers :'+QueueMembers);
            // Added by Saritha Singamsetty for CR-01668 ends here 
            if(QueueMembers == null || QueueMembers.isEmpty()){
                for(Special_Price_Request__c updatePrice: updatedRequests){
                    system.debug('throwing error');
                    string errormsg = 'Only a Special Pricing Approver can complete approval request';
                    updatePrice.addError(errormsg);
                }
            }
            Group spgQueue= [Select Id from Group where type ='Queue' and DeveloperName = 'Special_Pricing_Monitoring_Group' LIMIT 1];
            List<GroupMember> spgQueueMembers = [Select UserOrGroupId From GroupMember where GroupId =: spgQueue.Id];
            for(GroupMember g: spgQueueMembers)
            {
                If(String.valueOf(g.UserOrGroupId).substring(0,3) == '005'){
                    userIds.add(g.UserOrGroupId);   
                }
                else if(String.valueOf(g.UserOrGroupId).substring(0,3) == '00G'){
                    system.debug('@@ GroupIds' +g.UserOrGroupId);
                    GroupIds.add(g.UserOrGroupId);   
                }
                
            }
            List<Group> gRelatedId = [SELECT RelatedId FROM Group where Id IN:GroupIds];
            List<id> relatedId = new List<id>();
            for(Group r: gRelatedId){
                relatedId.add(r.RelatedId);
            }
            system.debug('@@gRelatedId'+gRelatedId);
            List<User> roleUIds = [SELECT Id FROM User where UserRoleId IN: relatedId];
            for(User u: roleUIds){
                userIds.add(u.Id);
            }
             system.debug('@@groupuserIds' +userIds);
            for(id uid:userIds ){
                if(uid == Usr.Id){
                    UserId.add(uid);
                }
            }
            system.debug('@@UserId' +UserId);
            if(userId == null || userId.isEmpty()){
                system.debug('@@UserId entered loop' );
                List<Special_Price_Products__c> lstspp = [Select Id,Requested_Price__c, Special_Price_Request__c,Customer_Service_Contract_ID__c From Special_Price_Products__c where Special_Price_Request__c in: updatedRequests]; 
                for(Special_Price_Products__c spp :lstspp){
                    for(Special_Price_Request__c s: updatedRequests){
                        if(spp.Customer_Service_Contract_ID__c == null && s.Id == spp.Special_Price_Request__c && spp.Requested_Price__c > 0){
                            String errormesg = 'You must enter a value in Customer Service Contract ID.'; 
                            s.addError(errormesg);
                        }
                    } 
                }
                
            }  
        }
    }
    public void OnAfterUpdate(Special_Price_Request__c[] oldObjects, Special_Price_Request__c[] updatedObjects, map<ID, Special_Price_Request__c> MapObjectMap){
        // AFTER UPDATE LOGIC
        system.debug('in SpecialPriceRequestTriggerHandler OnAfterUpdate method');
        List<Special_Price_Request__c>completedspecialrequest = new List<Special_Price_Request__c>();
       // List < Messaging.SingleEmailMessage > mails = new List < Messaging.SingleEmailMessage > (); 
        List <Messaging.SingleEmailMessage> mMails = new List < Messaging.SingleEmailMessage > ();
        List<Special_Price_Request__c> specialRequests = new List<Special_Price_Request__c>();
        List<string> opptylineitemid = new List<string>();
        Map<Id,Opportunity>Optty;
        Set<Id>OpttyID= New Set<Id>();
        List<Special_Price_Products__c> specialproducts = new List<Special_Price_Products__c>();
        Map<Id,OpportunityLineItem> opptylineitem = new Map<Id,OpportunityLineItem>();
        string baseURL = URL.getSalesforceBaseUrl().toExternalForm();// building Base URL.
       // list < string > teamEmail = new list < string > {System.label.SpecialPricingEmail};
        //Added by Saritha for CR-1686
        List<Special_Price_Request__c> lstCompletedSPR = new List<Special_Price_Request__c>(); 
        List<Special_Price_Request__c> lstcontractPendingSPR = new List<Special_Price_Request__c>();
        List<Special_Price_Products__c> lstToUpdateSPP = new List<Special_Price_Products__c>();
        Map<ID, List<Special_Price_Products__c>> mapOfContractPendingSPP = new Map<ID, List<Special_Price_Products__c>>();
        Group Queue= [Select Id from Group where type ='Queue' and DeveloperName = 'Special_Pricing_Monitoring_Group' LIMIT 1];
        List<GroupMember> QueueMembers = [Select UserOrGroupId From GroupMember where GroupId =: Queue.Id];  
        List<Id> userIds = new List<Id>();
        List<Id> GroupIds = new List<Id>();
        //string body;
        for(GroupMember g: QueueMembers)
        {
            If(String.valueOf(g.UserOrGroupId).substring(0,3) == '005'){
                userIds.add(g.UserOrGroupId);   
            }
            else if(String.valueOf(g.UserOrGroupId).substring(0,3) == '00G'){
                system.debug('@@ GroupIds' +g.UserOrGroupId);
                GroupIds.add(g.UserOrGroupId);   
            }
            
        }
        List<Group> gRelatedId = [SELECT RelatedId FROM Group where Id IN:GroupIds];
        List<id> relatedId = new List<id>();
        for(Group r: gRelatedId){
            relatedId.add(r.RelatedId);
        }
        system.debug('@@gRelatedId'+gRelatedId);
        List<User> roleUIds = [SELECT Id FROM User where UserRoleId IN: relatedId];
        for(User u: roleUIds){
            userIds.add(u.Id);
        }
        //Added by Saritha for CR-1686 - Ends 
        
        for(Special_Price_Request__c eachspecialprice :updatedObjects){
            if(eachspecialprice.Status__c == 'Completed' && MapObjectMap.get(eachspecialprice.Id).Status__c !='Completed'||
               eachspecialprice.Status__c == 'Rejected' && MapObjectMap.get(eachspecialprice.Id).Status__c !='Rejected'||
               eachspecialprice.Status__c == 'Recalled' && MapObjectMap.get(eachspecialprice.Id).Status__c !='Recalled'){
                   
               completedspecialrequest.add(eachspecialprice);
               OpttyID.add(eachspecialprice.Opportunity__c);
               specialRequests.add(eachspecialprice);
              /* if(eachspecialprice.Status__c == 'Completed' && MapObjectMap.get(eachspecialprice.Id).Status__c !='Completed'){
                   string body = ' A Special Price Request has been approved and is ready for your review.' + '<br/><br/>';
                    body += 'The Request can be viewed at the following link: <br/>' + ' ';
                    body += baseURL + '/' +eachspecialprice.id ;
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(teamEmail);
                    mail.setSubject('Special Price Request Approved for '+eachspecialprice.AccountName__c+' :'+eachspecialprice.Name);  //changed by Rahul for D-2737  (24/11/2015)
                    mail.setHtmlBody(body);
                    mails.add(mail);
               }*/
            }
             //Added by Saritha Singamsetty for CR-1668 -- starts
            if(eachspecialprice.Status__c == 'Contract Setup Pending' && MapObjectMap.get(eachspecialprice.Id).Status__c !='Contract Setup Pending'){
                lstcontractPendingSPR.add(eachspecialprice);
                system.debug('@@@lstcontractPendingSPR' +lstcontractPendingSPR);
            }
            if(eachspecialprice.Status__c == 'Completed' && MapObjectMap.get(eachspecialprice.Id).Status__c !='Completed'){
                lstCompletedSPR.add(eachspecialprice); 
            }
        }
        for(Special_Price_Products__c completedspp :[Select Id, Primary_Account__c, Special_Price_Request__c,Special_Price_Request__r.CreatedById, Special_Price_Request__r.Name From Special_Price_Products__c 
                                                     where Special_Price_Request__c in: lstCompletedSPR]){
           string sbody = '';
           sbody = 'Dear Special Pricing Monitoring Group,' + '<br/><br/>';
           sbody += 'Special pricing request ' + completedspp.Special_Price_Request__r.Name + ' for ' + completedspp.Primary_Account__c + ' has been completed.' + '<br/>' ;
           sbody += 'The request can be viewed at the following link:' + '<br/>';
           sbody +=  baseURL + '/' +completedspp.Special_Price_Request__c +'<br/><br/>'; 
           sbody += 'Thank you,' + '<br/>';
           sbody += 'Customer Service Contract team';
		   userIds.add(completedspp.Special_Price_Request__r.CreatedById);
           system.debug('@@@@useridscreatedbyid'+userIds);                                         
           Messaging.SingleEmailMessage m = new Messaging.SingleEmailMessage();
           m.setToAddresses(userIds);
           m.setSubject('Special Price Request Completed for '+completedspp.Primary_Account__c+': ' +completedspp.Special_Price_Request__r.Name);
           m.setHtmlBody(sbody);
           mMails.add(m);                                              
		}
        
        /*for(Special_Price_Products__c spp : [Select Id,Requested_Price__c, Primary_Account__c, Special_Price_Request__c,
                                             Estimated_Contract_Start_Date__c From Special_Price_Products__c 
                                             where Special_Price_Request__c in: lstcontractPendingSPR])*/
        for(Special_Price_Products__c spp : [Select Id,Estimated_Contract_Start_Date__c,Requested_Price__c From Special_Price_Products__c 
                                             where Special_Price_Request__c in: lstcontractPendingSPR]){
        /*List<Special_Price_Products__c> lstSPP = mapOfContractPendingSPP.get(spp.Special_Price_Request__c);
		if (lstSPP == null) {
			lstSPP = new List<Special_Price_Products__c>();
            lstSPP.add(spp); 
			mapOfContractPendingSPP.put(spp.Special_Price_Request__c, lstSPP);
		}
		else
        {
          mapOfContractPendingSPP.get(spp.Special_Price_Request__c).add(spp);
        }*/
		if(spp.Requested_Price__c>0)
        {
            spp.Estimated_Contract_Start_Date__c = system.today(); 
			lstToUpdateSPP.add(spp);  
        }
	  }
        system.debug('@@@lstpopulatemapspp' +lstToUpdateSPP.size());
     //   system.debug('@@@fullmapspp' +mapOfContractPendingSPP.size());
     /* for(Special_Price_Request__c spr:lstcontractPendingSPR){
            if(mapOfContractPendingSPP.containsKey(spr.Id)){
                string sbody = '';
                string sPrimaryAccount = '';
                List<Special_Price_Products__c> rlstSPP = new List<Special_Price_Products__c>(mapOfContractPendingSPP.get(spr.Id));
                system.debug('@@@lstspp' +rlstSPP);
                for(Special_Price_Products__c spp:rlstSPP){
                    system.debug('@@@spp' +spp); 
                    sPrimaryAccount = spp.Primary_Account__c; 
                    if(spp.Requested_Price__c > 0){
                        sbody += baseURL + '/' +spp.id +'<br/>';
                    }				
                }
                body = 'Dear Customer Service,' + '<br/><br/>';
                body += 'Special pricing request ' + spr.Name + ' for ' + sPrimaryAccount +' has been approved and is ready for your review.<br/>';
                body += 'The request can be viewed at the following link:<br/>';
                body += sbody + '<br/><br/>';
                body += 'Thank you,<br/>'+ 'Special Pricing Monitoring Group';
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(teamEmail);
                mail.setSubject('Special Pricing Request '+ spr.Name +' has been approved');
                mail.setHtmlBody(body);
                mails.add(mail); 
            }
        }*/
        try{
            update lstToUpdateSPP;
        } 
        catch (exception E){
            System.debug('Error in updating Special Price Products :'+ E );
        }
        //Added by Saritha Singamsetty for CR-1668 -- ends here
        system.debug('completedspecialrequest :'+completedspecialrequest);
        system.debug('OpttyID :'+OpttyID);
		
        
        if(OpttyID != null && !OpttyID.isEmpty()){
           Optty = new Map <Id, Opportunity> ( [Select Id,Special_Pricing_Approval__c,Special_Price_Request_Open__c,Business_unit__c From Opportunity where ID in: OpttyID]);
          
           }
        
           
           for(Special_Price_Request__c eachspecialprice :specialRequests){
            //Added a condition to check the business unit in the below line for CR-2402
             if (eachspecialprice.Status__c == 'Completed' &&  Optty.get(eachspecialprice.Opportunity__c)!= null && Optty.get(eachspecialprice.Opportunity__c).Business_unit__c != 'Pearson Assessment'){
                 Optty.get(eachspecialprice.Opportunity__c).Special_Price_Request_Open__c = false;
                 Optty.get(eachspecialprice.Opportunity__c).Special_Pricing_Approval__c = true;
                 system.debug('Optty.get(eachspecialprice.Opportunity__c).Special_Price_Request_Open__c :'+Optty.get(eachspecialprice.Opportunity__c).Special_Price_Request_Open__c);
                 system.debug('Optty.get(eachspecialprice.Opportunity__c).Special_Pricing_Approval__c :'+Optty.get(eachspecialprice.Opportunity__c).Special_Pricing_Approval__c);
            }
            else if (eachspecialprice.Status__c== 'Recalled' || eachspecialprice.Status__c== 'Rejected' &&  Optty.get(eachspecialprice.Opportunity__c)!= null){
            Optty.get(eachspecialprice.Opportunity__c).Special_Price_Request_Open__c= false;
            } 
           }
      
        if(specialRequests != null && !specialRequests.isEmpty()){
            specialproducts = [Select Id,Requested_Price__c,Special_Price_Request__r.Status__c, Approved_Price__c, Opportunity_Line_Item__c From Special_Price_Products__c where Special_Price_Request__c in: specialRequests];
           
        }
        if(specialproducts != null && !specialproducts.isEmpty()){
            for(Special_Price_Products__c eachspecialprice :specialproducts){
                //Saritha Singamsetty: 05/14/2019: added this for INC4948331.
                if(eachspecialprice.Approved_Price__c != null && eachspecialprice.Special_Price_Request__r.Status__c == 'Completed')
                    //eachspecialprice.Approved_Price__c = eachspecialprice.Requested_Price__c;
                opptylineitemid.add(eachspecialprice.Opportunity_Line_Item__c);
                system.debug('opptylineitemid'+opptylineitemid);
            }
            for(OpportunityLineItem eachopptylineitem : [select id, Discount, UnitPrice  from OpportunityLineItem where Id in:opptylineitemid]){
                opptylineitem.put(eachopptylineitem.Id, eachopptylineitem);
            }
           
            if(opptylineitem != null && !opptylineitem.isEmpty()){
               for(Special_Price_Products__c eachspecialprice :specialproducts){
                    if(opptylineitem.get(eachspecialprice.Opportunity_Line_Item__c) != null){
                        OpportunityLineItem tempopptyline = new OpportunityLineItem();
                        tempopptyline = opptylineitem.get(eachspecialprice.Opportunity_Line_Item__c);
                        system.debug('tempopptyline.UnitPrice :'+tempopptyline.UnitPrice);
                        system.debug('eachspecialprice.Approved_Price__c :'+eachspecialprice.Approved_Price__c);
                        system.debug('eachspecialprice.Requested_Price__c :'+eachspecialprice.Requested_Price__c);
                        if(tempopptyline.UnitPrice != 0.00 && eachspecialprice.Approved_Price__c != null )
                            tempopptyline.Discount = ((tempopptyline.UnitPrice - eachspecialprice.Approved_Price__c) * 100 )/tempopptyline.UnitPrice;
                            system.debug('tempopptyline.Discount  :'+tempopptyline.Discount);
                    }
                }
            }
               try{
                   update specialproducts;
                   update opptylineitem.values();
                  
               } catch (exception E){
               System.debug('Error in updateing Special Price Products :'+ E );
            }
        }
       
        try{
            if(Optty!= null && !Optty.isEmpty()){
                       update Optty.values();
                   }
        }catch (exception E){
               System.debug('Error in updating Opportunity:'+ E );
        }
      //  list<Messaging.SendEmailResult> results = Messaging.sendEmail(mails);
        list<Messaging.SendEmailResult> results1 =  Messaging.sendEmail(mMails);
            system.debug('results --->'+results1);
    }
   
        
 
    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }
}