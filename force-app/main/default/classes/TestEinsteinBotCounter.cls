/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 30/05/2018
 */
@isTest
private class TestEinsteinBotCounter {

	@isTest
	static void it_should_increment_the_bot_counter_if_null() {
		List<Integer> returnedValues = EinsteinBotCounter.increment(new List<Integer>{null});

		System.assert(!returnedValues.isEmpty());
		System.assertEquals(1, returnedValues[0]);
	}

	@isTest
	static void it_should_increment_the_bot_counter() {
		List<Integer> returnedValues = EinsteinBotCounter.increment(new List<Integer>{1});

		System.assert(!returnedValues.isEmpty());
		System.assertEquals(2, returnedValues[0]);
	}

	@isTest
	static void it_should_increment_the_bot_counter_twice() {
		List<Integer> returnedValues = EinsteinBotCounter.increment(new List<Integer>{1});
		returnedValues = EinsteinBotCounter.increment(returnedValues);

		System.assert(!returnedValues.isEmpty());
		System.assertEquals(3, returnedValues[0]);
	}
}