@isTest
private class TestLiveChatTranscriptTriggerHandler
{
    private final static String CHAT_KEY = 'abc';

    @testSetup
    static void setup() {
        Bypass_Settings__c bypass = new Bypass_Settings__c(
            SetupOwnerId = UserInfo.getUserId(),
            Disable_Triggers__c = true,
            Disable_Validation_Rules__c = true);
        insert bypass;

        Account account = new Account(
            Name = 'Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId()
        );
        insert account;

        Contact contact = new Contact(
            FirstName = 'Rick',
            LastName = 'Sanchez',
            Email = 'rick.sanchez@email.com',
            AccountId = account.Id
        );
        insert contact;


        User testUser = [SELECT Id FROM User WHERE Name = 'Automated Process'];

        Case cas = new Case();
        cas.OwnerId = testUser.Id;
        insert cas;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

        LiveChatTranscript transcript = new LiveChatTranscript(
            ContactId = contact.Id,
            Status = 'Completed',
            CaseId = cas.Id,
            ChatKey = CHAT_KEY,
            LiveChatVisitorId = vistor.Id);
        insert transcript;
    }

    @isTest
    static void it_should_delete_related_records_after_completion()
    {
        User testUser = [SELECT Id FROM User WHERE Name = 'Automated Process'];

        System.runAs(testUser) {
            update [SELECT Id, Status FROM LiveChatTranscript];
        }

        System.assertEquals(0, [
            SELECT COUNT()
            FROM Account]);

        System.assertEquals(0, [
            SELECT COUNT()
            FROM Contact]);

        System.assertEquals(0, [
            SELECT COUNT()
            FROM Case]);
    }

    @isTest
    static void it_should_remove_new_case_when_updated_by_automated_process() {
        User testUser = [SELECT Id FROM User WHERE Name = 'Automated Process'];
        LiveChatTranscript transcript = [SELECT Id, CaseId, Status FROM LiveChatTranscript];

        Id oldCaseId = transcript.CaseId;

        System.assertNotEquals(null, transcript.CaseId);

        Case cas = new Case();
        cas.OwnerId = testUser.Id;
        insert cas;

        Test.startTest();
        System.runAs(testUser) {
            transcript.CaseId = cas.Id;
            update transcript;
        }
        Test.stopTest();

        System.assertEquals(0, [
            SELECT COUNT()
            FROM Case
            WHERE Id = :cas.Id
        ]);
    }
}