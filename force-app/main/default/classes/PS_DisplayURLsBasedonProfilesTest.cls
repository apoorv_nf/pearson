/************************************************************************************************************
* Apex Interface Name : PS_AppLauncherControllerTest
* Version             : 1.1 
* Created Date        : 16-Feb-2016 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Dalia                  16-Feb-2016             
* Rohit Kulkarni	 04/03/2016
-------------------------------------------------------------------------------------------------------------
* Test Name                   :Display Links BY Application Catalog
* Test Description            :Display Links BY Application Catalog
* Expected Inputs             :Pass parameters like Application name,static parameters,
* Expected Outputs            :Dipaly QuickLinks and GuidedFlow links as per profile
************************************************************************************************************/
@isTest
public class PS_DisplayURLsBasedonProfilesTest{
    
    static testMethod void DisplayURLs(){
        Boolean bRenderGuidedList =false;
        String sPageName = '';
        
        List<AppCatalog__c> appCatalogList = new List<AppCatalog__c>(); 
        appCatalogList.add( new AppCatalog__c(name='First', Catalog__c='Catalog', APPLICATIONNAME__c='Application Catalog', PS_ApplicationCatalogController__c='PS_ApplicationCatalogController', fetchApplicationcatalogUrls__c='fetchApplicationcatalogUrls'));
        insert appCatalogList; 
        
        String APPLICATIONNAME = appCatalogList[0].APPLICATIONNAME__c;
        String CLASSNAME = appCatalogList[0].PS_DisplayURLsBasedONProfile__c;
        String METHODNAME = appCatalogList[0].getLinksBasedOnProfile__c; 
        String MYAPPS = appCatalogList[0].MyApps__c;
        
        boolean VISIBLEONGF=FALSE;
        boolean VISIBLEONQL=FALSE;
        Integer urlLimit = 10;
        
        //Pearson Service Cloud User OneCRM start
        List<User> listWithPearsonServiceCloudUser = new List<User>();
        listWithPearsonServiceCloudUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,1);
        //Pearson Service Cloud User OneCRM start
        //System Administrator start
        List<User> listWithSystemAdministrator = new List<User>();
        listWithSystemAdministrator  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        //System Administrator start End


        //ACURLLimit__c start
        List<ACURLLimit__c> accurlLimit = new List<ACURLLimit__c>();
        accurlLimit.add(new ACURLLimit__c(name='MyApps',Limit__c = 50));
        insert accurlLimit;
        //ACURLLimit__c End
        
        //Application__c Start
        List<Application__c> listApplication = new List<Application__c>();
        listApplication.add(new Application__c(App_Label__c='Access Code Guide',App_Type__c='Console Tab',
            URL__c = 'apex/PS_Access_Code_Guide', Use_Sharing_Rules_to_display_records__c = true, 
            Visible_On_application_Catalog__c = true, Visible_On_MyApp__c = true, Visible_On_Quicklinks__c = false));
        listApplication.add(new Application__c(App_Label__c='ClassLive',App_Type__c='Console Tab',
            URL__c = 'apex/PS_ClassLive', Use_Sharing_Rules_to_display_records__c = true, 
            Visible_On_application_Catalog__c = true, Visible_On_MyApp__c = true, Visible_On_Quicklinks__c = false));
        insert listApplication;
        //Application__c End
        
        //App_Profile__c Start
        List<App_Profile__c> listAppProfile = new List<App_Profile__c>();
        listAppProfile.add(new App_Profile__c(Application__c = listApplication[0].Id, 
            Profile_Name__c = 'Pearson Service Cloud User OneCRM', Ranking__c=1));
        listAppProfile.add(new App_Profile__c(Application__c = listApplication[0].Id, 
            Profile_Name__c = 'System Administrator', Ranking__c=2));
        listAppProfile.add(new App_Profile__c(Application__c = listApplication[0].Id, 
            Profile_Name__c = 'Pearson Service Cloud User OneCRM', Ranking__c=3));
        listAppProfile.add(new App_Profile__c(Application__c = listApplication[0].Id, 
            Profile_Name__c = 'System Administrator', Ranking__c=4));
        insert listAppProfile;
        //App_Profile__c End
        Test.StartTest();
        
        System.runAs(listWithPearsonServiceCloudUser[0]){
        
            // call the constructor
            PS_DisplayURLsBasedonProfilesController controller = new PS_DisplayURLsBasedonProfilesController();
            
            List<App_Profile__c> listAppProfiles1 = controller.GuidedFlow;
            List<AggregateResult> listAppProfiles = controller.GuidedCategories;
            List<App_Profile__c> listAppProfiles2 = controller.QuickLinks;
            String PERMISSIONSETNAME = controller.PermissionSetCheck();
            PageReference pageRef = controller.mRenderList();
            PS_DisplayURLsBasedonProfilesController.getGuidedFlowForCategories();
            
        }
        
        System.runAs(listWithSystemAdministrator[0]){
            // call the constructor
            PS_DisplayURLsBasedonProfilesController controller = new PS_DisplayURLsBasedonProfilesController();
            
            List<App_Profile__c> listAppProfiles1 = controller.GuidedFlow;
            List<AggregateResult> listAppProfiles = controller.GuidedCategories;
            List<App_Profile__c> listAppProfiles2 = controller.QuickLinks;
            String PERMISSIONSETNAME = controller.PermissionSetCheck();
            PageReference pageRef1 = controller.mRenderList();
        }
        System.assertEquals(appCatalogList[0].Catalog__c,'Catalog');
        
        Test.StopTest(); 
    }
    
    /*
     * Method to cover Negative scenario where Exception occurs for getApps() method
     */
    
     static testMethod void Negative_DisplayURLs(){
        try{
            Boolean bRenderGuidedList =false;
            String sPageName = '';
            
            List<AppCatalog__c> appCatalogList = new List<AppCatalog__c>(); 
            appCatalogList.add( new AppCatalog__c(Catalog__c='Catalog', APPLICATIONNAME__c='Application Catalog', PS_ApplicationCatalogController__c='PS_ApplicationCatalogController', fetchApplicationcatalogUrls__c='fetchApplicationcatalogUrls'));
            insert appCatalogList; 
            
            String APPLICATIONNAME = appCatalogList[0].APPLICATIONNAME__c;
            String CLASSNAME = appCatalogList[0].PS_DisplayURLsBasedONProfile__c;
            String METHODNAME = appCatalogList[0].getLinksBasedOnProfile__c; 
            String MYAPPS = appCatalogList[0].MyApps__c;
            
            boolean VISIBLEONGF=FALSE;
            boolean VISIBLEONQL=FALSE;
            Integer urlLimit = 10;
            
            //Pearson Service Cloud User OneCRM start
            List<User> listWithPearsonServiceCloudUser = new List<User>();
            listWithPearsonServiceCloudUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,1);
            //Pearson Service Cloud User OneCRM start
            //System Administrator start
            List<User> listWithSystemAdministrator = new List<User>();
            listWithSystemAdministrator  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
            //System Administrator start End


            //ACURLLimit__c start
            List<ACURLLimit__c> accurlLimit = new List<ACURLLimit__c>();
            accurlLimit.add(new ACURLLimit__c(name='MyApps',Limit__c = 50));
            insert accurlLimit;
            //ACURLLimit__c End
            
            //Application__c Start
            List<Application__c> listApplication = new List<Application__c>();
            listApplication.add(new Application__c(App_Label__c='Access Code Guide',App_Type__c='Console Tab',
                URL__c = 'apex/PS_Access_Code_Guide', Use_Sharing_Rules_to_display_records__c = true, 
                Visible_On_application_Catalog__c = true, Visible_On_MyApp__c = true, Visible_On_Quicklinks__c = false));
            listApplication.add(new Application__c(App_Label__c='ClassLive',App_Type__c='Console Tab',
                URL__c = 'apex/PS_ClassLive', Use_Sharing_Rules_to_display_records__c = true, 
                Visible_On_application_Catalog__c = true, Visible_On_MyApp__c = true, Visible_On_Quicklinks__c = false));
            insert listApplication;
            //Application__c End
            
            //App_Profile__c Start
            List<App_Profile__c> listAppProfile = new List<App_Profile__c>();
            listAppProfile.add(new App_Profile__c(Application__c = listApplication[0].Id, 
                Profile_Name__c = 'Pearson Service Cloud User OneCRM', Ranking__c=1));
            listAppProfile.add(new App_Profile__c(Application__c = listApplication[0].Id, 
                Profile_Name__c = 'System Administrator', Ranking__c=2));
            listAppProfile.add(new App_Profile__c(Application__c = listApplication[0].Id, 
                Profile_Name__c = 'Pearson Service Cloud User OneCRM', Ranking__c=3));
            listAppProfile.add(new App_Profile__c(Application__c = listApplication[0].Id, 
                Profile_Name__c = 'System Administrator', Ranking__c=4));
            insert listAppProfile;
            //App_Profile__c End
            Test.StartTest();
            
            System.runAs(listWithPearsonServiceCloudUser[0]){
            
                // call the constructor
                PS_DisplayURLsBasedonProfilesController controller = new PS_DisplayURLsBasedonProfilesController();
                
                List<App_Profile__c> listAppProfiles1 = controller.GuidedFlow;
                List<AggregateResult> listAppProfiles = controller.GuidedCategories;
                List<App_Profile__c> listAppProfiles2 = controller.QuickLinks;
                String PERMISSIONSETNAME = controller.PermissionSetCheck();
                PageReference pageRef = controller.mRenderList();
                
            }
            
            System.runAs(listWithSystemAdministrator[0]){
                // call the constructor
                PS_DisplayURLsBasedonProfilesController controller = new PS_DisplayURLsBasedonProfilesController();
                
                List<App_Profile__c> listAppProfiles1 = controller.GuidedFlow;
                List<AggregateResult> listAppProfiles = controller.GuidedCategories;
                List<App_Profile__c> listAppProfiles2 = controller.QuickLinks;
                String PERMISSIONSETNAME = controller.PermissionSetCheck();
                PageReference pageRef1 = controller.mRenderList();
            }
            System.assertEquals(appCatalogList[0].Catalog__c,'Catalog');
            
            Test.StopTest(); 
        }
         catch (DmlException e) {
           //Assert Error Message            
            Boolean expectedExceptionThrown =  e.getMessage().contains('Insert failed. First exception on row 0; first error: REQUIRED_FIELD_MISSING, Required fields are missing: [Name]: [Name]') ? true : false;
            System.AssertEquals(expectedExceptionThrown,true);
            System.assertEquals('REQUIRED_FIELD_MISSING' , e.getDmlStatusCode(0));
            
        }
    }
}