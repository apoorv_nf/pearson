/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
    Name:            PS_opportunity_UpdateAccountTeamAdmin.cls 
   Description:     TO Update Account Team Adminstator Based On Opportunity Values
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
     04/2015         1.0            Karthik.A.S                       Initial Release 
  03/04/2015         1.1           Davi Borges           Change in the process of selecting the campus account
  14/01/2016         1.2            Karthik.A.S           Added condtion on query to retrive active users only
  27/08/2018         1.3           Navaneetha Perumal.A  Modified as part of ZA Non-Apttus On-Board Activities
------------------------------------------------------------------------------------------------------------------------------------------------------------ */

public without sharing class PS_opportunity_UpdateAccountTeamAdmin
{
  public static void updateadmin(List<Opportunity>oppty) 
  { 
    
    List<Opportunity> toProcessOpportunties =  new List<Opportunity>();

    for(Opportunity o:oppty)  
    {
      
      if((o.Type=='New Business') &&(o.StageName == 'Closed' || o.StageName == 'Negotiation')){
        
        toProcessOpportunties.add(o);
        
      }
    }

    if(toProcessOpportunties.size() == 0){

      return;
    }

    Map<Id, Opportunity> opportunities = new Map<Id, Opportunity> (toProcessOpportunties);

         
    // maps opportunities id to account ids
    Map<Id,Id> opportunityIdToAccountId = new Map<Id, Id>();
    
    // for each opportunity add the account id to the map
    for(Opportunity o:opportunities.values())  
    {      
        opportunityIdToAccountId.put(o.Id,  o.AccountId );
    }

        
    // account id to list of current team members
    List<AccountTeamMember> currentAccountTeamMembers  = [SELECT a.Id, a.UserId,a.User.Name,a.TeamMemberRole, a.AccountId
                                                FROM AccountTeamMember a
                                                WHERE  a.TeamMemberRole IN ('Account Administrator') AND 
                                                a.AccountId  in :opportunityIdToAccountId.values()and a.User.IsActive = true];
    

    // fetching child  account team members
    // maps preferred campus to opportunity id
    Map<Id , List<Id>> CampusAccountToListOpportunityId = new Map<Id ,List<Id>>();
    Map<Id,Id> oppSyncQuoteId = new Map<Id,Id>(); // Added by Navaneeth
    for(Opportunity o:opportunities.values()){  
     //CP 07/25/2017 changed qualification_campus to look at SyncedQuote.Qualification_Campus
     // Commented and Modified by Navaneeth - Start 
     /*
           if( ! CampusAccountToListOpportunityId.containsKey(o.SyncedQuote_Qualification_Campus__c))
           {
             CampusAccountToListOpportunityId.put(o.SyncedQuote_Qualification_Campus__c, new List<Id>());
           }

           CampusAccountToListOpportunityId.get(o.SyncedQuote_Qualification_Campus__c).add(o.id);
     */ 
         if(o.SyncedQuoteId!=null)
         {
             oppSyncQuoteId.put(o.id,o.SyncedQuoteId);
         }
     // Commented and Modified by Navaneeth - End
    }
    for(Quote quot : [SELECT Id, OpportunityId, Qualification_Campus__c, IsSyncing FROM Quote where Id IN:oppSyncQuoteId.values()])
    { if( ! CampusAccountToListOpportunityId.containsKey(quot.Qualification_Campus__c)) { CampusAccountToListOpportunityId.put(quot.Qualification_Campus__c, new List<Id>());
           }

           CampusAccountToListOpportunityId.get(quot.Qualification_Campus__c).add(quot.OpportunityId);
    }
        
    AccountTeamMember newAccountTeamMember;
    // the account team members to insert
    List<AccountTeamMember> listWithAccountTeamMember = new List<AccountTeamMember >();
    AccountShare[] newShare = new AccountShare[]{};
    for(AccountTeamMember accountTeamMember1 : [SELECT a.UserId, a.User.Name, a.TeamMemberRole, a.Id, a.AccountId, a.Account.Name FROM AccountTeamMember a 
                                                WHERE  a.TeamMemberRole IN ('Account Administrator') AND a.AccountId IN :CampusAccountToListOpportunityId.KeySet() and a.User.IsActive = true])  
    { List<Id> idOpp = CampusAccountToListOpportunityId.get(accountTeamMember1.AccountId);
      
      // Updating  parent account team members with child account team member
      for(Id oppId : idOpp) { newAccountTeamMember = new AccountTeamMember(); newAccountTeamMember.AccountID = opportunities.get(oppId).AccountId;
 newAccountTeamMember.TeamMemberRole = accountTeamMember1.TeamMemberRole; newAccountTeamMember.UserId = accountTeamMember1.UserId; listWithAccountTeamMember.add(newAccountTeamMember);
        
newShare.add(new AccountShare(UserOrGroupId=accountTeamMember1.UserId, AccountId=opportunities.get(oppId).AccountId, AccountAccessLevel='Edit', OpportunityAccessLevel='Edit')); 
        }       
    }
    
    if(currentAccountTeamMembers.size() >0){
      delete currentAccountTeamMembers;
    }
    
    
    if(listWithAccountTeamMember.size() >0){
      
      insert listWithAccountTeamMember; insert(newShare); return;
    }

  }
   
}