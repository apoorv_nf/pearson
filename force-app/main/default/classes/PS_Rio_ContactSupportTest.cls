@isTest
public class PS_Rio_ContactSupportTest {
    @TestSetup
    public static void testData()
    {
        Product__c rioProduct = new Product__C (Active_in_Case__c=true,Active_in_Self_Service__c=false,Business_Vertical__c='Higher Education',Line_of_Bussiness__c='Higher Education',Name='Rio Project');
        PS_CategoryGrouping__c catGroup = new PS_CategoryGrouping__c(name='Assignments' , grouping__c='Courses',CountryCode__c='US');
        CS_role__c csRole = new CS_role__c(name='College_Educator__c',Label__c='College Educator',Category__c='All');
        insert rioProduct;
        insert catGroup;
        insert csRole;
    }
	public testMethod static void testGetpickvalCaseCat()
    {
        List<PS_CategoryGrouping__c> categoryGrouping = PS_Rio_ContactSupport.getpickvalCaseCat();
    }
    public testMethod static void testGetValueRole()
    {
        list<CS_role__c> rolelist = PS_Rio_ContactSupport.getValueRole();
    }
    public testMethod static void testGetCaseData()
    {
        String result = PS_Rio_ContactSupport.getCaseData('NAUS_HETS_Rio_Pilot', 'TechnicalSupport', 'Technical_Support', 'Rio Project');
    }
}