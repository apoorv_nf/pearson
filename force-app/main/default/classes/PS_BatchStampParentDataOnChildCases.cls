/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_BatchStampParentDataOnChildCases.cls 
   Description:     Runs from Trigger to Update send mass flag and Parent's mail body to Child Records. 
   CR Info:         CR-00275
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  03/04/2017         1.0            MAYANK LAL                       Initial Design 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */

global class PS_BatchStampParentDataOnChildCases implements Database.Batchable<sObject>,Database.stateful {
    
    //String parentCaseId;
    //List<Case> parCase = new List<Case>();
    List<Database.SaveResult> lstDBSaveResult = null;
    List<Case> lstCasetoBeUpdated = null;
    Map<id,String> mapOfParent = new Map<id,String>();
    
  
    
    public PS_BatchStampParentDataOnChildCases(Map<id,String> parentMap) {
        mapOfParent = parentMap;
        
    }

    
    // To get all the child Cases from Parent Case
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([select id,casenumber,Mass_Email_Body__c,ContactEmail,Thread_ID__c,Subject,contact.Name,sendMassEmail__c,parentid from Case where parentid  IN : mapOfParent.keySet()]);
    }
    
    // Method to stamp parent's Data on child records
    global void execute(Database.BatchableContext BC, List<Case> lstCases){
        lstCasetoBeUpdated = new List<Case>();
        
            for(Case cschild: lstCases) {
                cschild.sendMassEmail__c = true;
          cschild.Mass_Email_Body__c = mapOfParent.get(cschild.ParentId);
                lstCasetoBeUpdated.add(cschild);
            }
               
        lstDBSaveResult = Database.Update(lstCasetoBeUpdated,false);
        List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
        try{            
            for (integer i=0;i<lstCasetoBeUpdated.size();i++){
                Case cases=lstCasetoBeUpdated[i];
                Database.SaveResult res=lstDBSaveResult[i];
                String ErrMsg='';
                if (!res.isSuccess() || Test.isRunningtest()){
                    PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                    errlogger.InterfaceName__c='StampParentDataOnChildCases';
                    errlogger.ApexClassName__c='PS_StampParentDataOnChildCases';
                    errlogger.CallingMethod__c='finish';
                    errlogger.UserLogin__c=UserInfo.getUserName(); 
                    errlogger.RecordId__c=cases.id;   
                    for(Database.Error err : res.getErrors()) {
                        ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage();
                    }   
                    errlogger.ExceptionMessage__c=ErrMsg;
                    errloggerlist.add(errlogger);                     
                }
            }
            if(errloggerlist.size()>0){insert errloggerlist;}
            
        }
        catch(DMLException e){
            throw(e);
        }
        catch(Exception e){
            ExceptionFramework.LogException('BatchStampParentDataOnChildCases','PS_BatchStampParentDataOnChildCases','execute',e.getMessage(),UserInfo.getUserName(),'');
        }
    }
    
    
    // finish method will send the complete status of batch to configured email address.
    global void finish(Database.BatchableContext BC){
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email, ExtendedStatus
                          from AsyncApexJob where Id = :bc.getJobId()];        
        if(a.Status == 'Completed') {
            system.debug('@@Batch Completed');
            
            /*
            // 1. Send Email (CSV created in execute method)
            
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            // Send the email to the job submitter
            String[] toAddresses = new String[] {'mayank.lal@cognizant.com'};
            mail.setToAddresses(toAddresses);
            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('x70RecordExtract  Status: ' + a.Status);
            mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
                                  ' batches with '+ a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus);
            
            // Add your attachment to the email.
            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
            
            // 2. database.executebatch(new chain_batch(),200); */
            
        }
    }
}