// Project: Convert Assets from Product Info.
// Created by: Comity Designs [Developer: Rashi Girdhar]
// Class: AssetWrapper.cls     
// Description: Wrapper class for the Asset Object used by ProductsToAssets.cls 
// Other Package Classes: ProductsToAssets.cls
// VisualForce Page: ProductsToAssets.page
// Date: 8/3/2009

/**
 ** Class: AssetWrapper
**/
public with sharing class AssetWrapper {
    //Variables
    
    @AuraEnabled
    public Boolean isAssetSelected = false;
    
    @AuraEnabled
    public Asset asset{get;set;}
    
    @AuraEnabled
    public String oliID{get; set;}
    
    /**
    ** Constructor
    **/ 
    public AssetWrapper(Asset a, String id) {
        this.setAsset(a);   
        this.oliID=id;
    }
    
    //Getters
    
    public Boolean getIsAssetSelected(){
        return this.isAssetSelected;
    }
    
    public Asset getAsset(){
        return this.asset;
    }           
    
    //Setters
    
    public void setIsAssetSelected(Boolean a){
        this.isAssetSelected = a;
    }
    
    private void setAsset(Asset a){
        this.asset=a;
    }
    
    @AuraEnabled
    public Asset getTheAsset(){
    return this.asset;
    }
    
    @AuraEnabled
    public boolean getIsSelected(){
    return this.isAssetSelected;
    }    
    
}