/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_CaseServiceAssignRuleActivationTest.cls 
   Description:     Test Class to cover code coverage PS_CaseServiceAssignRuleActivation
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
    10/2/2016         1.0            Payal Popat                       Initial Release 
    18/02/2016        1.1            Rohit Kulkarni                    Added Method for update and insert
    04/03/2016        1.2        Rohit Kulkarni            Negative test
------------------------------------------------------------------------------------------------------------------------------------------------------------- 
* Test Name                   :Call Assignment Rules
* Test Description            :Call Assignment Rules
* Expected Inputs             :Create case with Web,Emai.. and Escalation_Point__c is not tier 1
* Expected Outputs            :Depending on criteria tow different Assignments rules must call
------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class PS_CaseServiceAssignRuleActivationTest{
    static testMethod void mScenarioWebToCase(){
        //User Creation start
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Support Profile'].Id,1);
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Validation_Rules__c=true);
        insert byp; 
        //User Creation Ends
        //Account Creation start
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        insert lstAccount ;
        //Account Creation Ends
        //Contact Creation start
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].MailingState='England';
        insert lstContact;
        //Contact Creation Ends
        //Case Creation start
        System.runAs(listWithUser[0]){
            List<case> cases = TestDataFactory.createCase(1,'CustomerSupport');
            cases[0].Account=lstAccount[0];
            cases[0].Contact=lstContact[0];
            cases[0].Origin = 'Web';
            Test.StartTest();
            insert cases;
           System.assertEquals(cases[0].Origin,'Web');
            Test.StopTest();
        //Case Creation Ends
        }
    }
    static testMethod void mScenarioEmailToCase(){
        //Account Creation start
         List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Support Profile'].Id,1);
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Validation_Rules__c=true);
        insert byp; 
        System.runAs(listWithUser[0]){
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        insert lstAccount ;
        //Account Creation 
        //Contact Creation start
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].MailingState='England';
        lstContact[0].accountid = lstAccount[0].id;
        insert lstContact;
        //Contact Creation 
        //Case Creation start
       // System.runAs(listWithUser[0]){
        List<case> cases = TestDataFactory.createCase(1,'CustomerSupport');
        cases[0].Account=lstAccount[0];
        cases[0].Contact=lstContact[0];
        cases[0].Origin = 'Email';
        Test.StartTest();
        insert cases;
        System.assertEquals(cases[0].Origin,'Email');
        Test.StopTest();
        }
        //Case Creation Ends
    }
    static testMethod void mScenarioInsertCase(){
        //Account Creation start
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        insert lstAccount ;
        //Account Creation 
        //Contact Creation start
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].MailingState='England';
        insert lstContact;
        //Contact Creation 
        //Case Creation start
        List<case> cases = TestDataFactory.createCase(1,'CustomerSupport');
        cases[0].Account=lstAccount[0];
        cases[0].Contact=lstContact[0];
        cases[0].Escalation_Point__c = 'Tier 2';
        cases[0].Global_Country__c = 'United States';
        cases[0].Escalation_Reason__c='Test';
        Test.StartTest();
        insert cases;
        System.assertEquals(cases[0].Escalation_Point__c,'Tier 2');
        Test.StopTest();
        //Case Creation Ends
    }
    static testMethod void mScenarioInsertEmailCase(){
         List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Support Profile'].Id,1);
        //Account Creation start
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Validation_Rules__c=true);
        insert byp; 
        System.runAs(listWithUser[0]){
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        insert lstAccount ;
        //Account Creation 
       
        //Contact Creation start
         
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].MailingState='England';
        lstContact[0].Accountid = lstAccount[0].id;
        insert lstContact;
        //Contact Creation 
        //Case Creation start
        
        List<case> cases = TestDataFactory.createCase(1,'CustomerSupport');
        cases[0].Account=lstAccount[0];
        cases[0].Contact=lstContact[0];
        cases[0].Escalation_Point__c = 'Tier 2';
        cases[0].Global_Country__c = 'United States';
        cases[0].Escalation_Reason__c='Test';
        cases[0].Origin = 'Email';
       Test.StartTest();
        insert cases;
        System.assertEquals(cases[0].Escalation_Point__c,'Tier 2');
        Test.StopTest();
        }
        //Case Creation Ends
    }
    
    //Negative
    static testMethod void Negativesc_mScenarioWebToCase(){
        //User Creation start
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Support Profile'].Id,1);
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Validation_Rules__c=true);
        insert byp; 
        //User Creation Ends
        //Account Creation start
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        insert lstAccount ;
        //Account Creation Ends
        //Contact Creation start
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].MailingState='England';
        lstContact[0].AccountId=lstAccount[0].Id; 
        insert lstContact;
        //Contact Creation Ends
        //Case Creation start
        System.runAs(listWithUser[0]){
            List<case> cases = TestDataFactory.createCase(1,'CustomerSupport');
            cases[0].Account=lstAccount[0];
            cases[0].Contact=lstContact[0];
            cases[0].Origin = 'Chat';
            Test.StartTest();
            insert cases;
           System.assertEquals(cases[0].Origin,'Chat');
            Test.StopTest();
        //Case Creation Ends
        }
    }
    
    static testMethod void mCallAssignmentRuleTest(){
        //Account Creation start
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        lstAccount[0].Abbreviated_Name__c='Testacc132434';
        insert lstAccount ;
        //Account Creation 
        //Contact Creation start
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].MailingState='England';
        insert lstContact;
        //Contact Creation 
        //Case Creation start
        List<case> cases = TestDataFactory.createCase(1,'CustomerSupport');
        cases[0].Account=lstAccount[0];
        cases[0].Contact=lstContact[0];
        //cases[0].Origin = 'Web';        
        Test.StartTest();
        insert cases;
        cases[0].Escalation_Point__c = 'Tier 2';
        cases[0].Origin = 'Web-Phone';
        update cases;
        list<id> caseID= new list<id>();
        caseID.add(cases[0].id);
        Map<Id, Case> caseMap = new Map<Id, Case>();
        PS_CaseServiceAssignmentRuleActivation.mCallAssignmentRule(caseID,true,true);
        PS_CaseServiceAssignmentRuleActivation.mActivateAssignmentRule(cases,true,true,caseMap);
        Test.StopTest();
        //Case Creation Ends
    }
       
}