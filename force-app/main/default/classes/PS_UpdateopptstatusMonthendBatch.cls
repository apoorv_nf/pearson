global class PS_UpdateopptstatusMonthendBatch implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {    
        String query = 'SELECT Id,Name,Chased__c,Chased_On__c,CloseDate,IsClosed,StageName,Auto_Expire__c,isChange_Chased_on_Date__c,Updated_Last_On__c FROM Opportunity WHERE (Chased__c=True OR Auto_Expire__c=True) AND StageName != \'Assumed Declined\' AND CloseDate!=9999-12-31';  
        return Database.getQueryLocator(query);    
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        Integer currentMonth = System.Today().Month();
        Integer currentYear = System.Today().year();
        List<opportunity> lstOppty = new List<opportunity>();
        
        for(Opportunity oppt : scope){
            if(oppt.Auto_Expire__c == True && (oppt.CloseDate.Month() == currentMonth && oppt.CloseDate.year() == currentYear) && oppt.isClosed == False){
                oppt.StageName = 'Assumed Declined';  
            }   
            
            if(oppt.Chased__c == True && oppt.Chased_On__c.Month() ==currentMonth && (oppt.isChange_Chased_on_Date__c <= oppt.Chased_On__c ) && oppt.isClosed == False){
                oppt.StageName = 'Assumed Declined';  
            }
            
            if(oppt.StageName == 'Assumed Declined'){
                lstOppty.add(oppt);	
            }					
        }
        update lstOppty;    
    }
    
    global void finish(Database.BatchableContext BC){
        
    }   
}