/************************************************************************************************************
* Apex Interface Name : PS_AgentWorkTriggerHandlerTest
* Version             : 1.1 
* Created Date        : 8/04/2016  
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
 Sudhakar Navuluri        8/04/2016       Amended Code      
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest(SeeAllData=true)
public class PS_AgentWorkTriggerHandlerTest
{
 static testMethod void validateAgentWorkPostive()
    {

    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=UserInfo.getUserId(),Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
    //insert byp;
    List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');        
    insert lstAccount ;  
        
    List<Contact> lstContact = TestDataFactory.createContacts(1);
    lstContact[0].MailingState='England';
    lstContact[0].AccountId = lstAccount[0].id;       
    insert lstContact;  
    
    List<case> cases = TestDataFactory.createCase(1,'School Assessment'); 
    cases[0].AccountId=lstAccount[0].id;
    cases[0].Contact=lstContact[0];
    cases[0].origin='Email';
    
    System.runAs(new User(Id=UserInfo.getUserId()))
    { 
        Test.StartTest();
        insert cases; 
        
        AgentWork awlist = [select id,CurrencyIsoCode from Agentwork limit 1];
        awlist.CurrencyIsoCode = 'USD';  
        update awlist;
        Test.StopTest();  
    }     
    }
}