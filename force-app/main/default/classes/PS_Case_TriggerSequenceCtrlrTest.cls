@isTest (seeAllData=false)
public with sharing class PS_Case_TriggerSequenceCtrlrTest 
{    
    
    /*************************************************************************************************************
* Name        : verifySubmisionValidationForCases
* Description : Verify the submissionValidationForCases method   
* Input       : 
* Output      : 
*************************************************************************************************************/
    
    private static List<Case> generateTestCases(Integer numOfCases, Boolean setExternalId)
    {
        List<Case> casesToInsert = new List<Case>();
        
        
        //Generate an account
        Account acc = new Account(Name = 'Account 1');
        if(setExternalId)
        {
            acc.External_Account_Number__c = 'External';  
        }
        insert acc;
        test.startTest();//Ab
        //Generate contact
        Contact con = new Contact(LastName = 'Contact 1', FirstName = 'fn', Email = 'test@test.com.demo', AccountId = acc.Id,First_Language__c  = 'English',MobilePhone  = '498763425',Salutation = 'MR.',Preferred_Address__c = 'Other Address' ,OtherCountry  = 'India',OtherStreet = 'Test',OtherCity  = 'Test',OtherPostalCode  = '123456');
        insert con;
        
        AccountContact__c accCon = new  AccountContact__c(Account__c = acc.Id, Contact__c = con.Id, AccountRole__c = 'Role', Primary__c = true, Financially_Responsible__c = True);
        insert accCon;
        
        Account_Correlation__C ac = new Account_Correlation__C(Account__C = acc.Id, External_ID_Name__c = 'eVision Learner Number', External_ID__c = 'External ID');
        insert ac;
        test.stopTest();//Ab
        // RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Case' and DeveloperName = 'Loan_Bursary_Request'];                        
        
        
        Id rt = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
        
        for(Integer i=0; i<numOfCases; i++)
        {    
            Case caseToInsert = new Case( Reopen_Reason__c = 'test',AccountId = acc.Id, RecordTypeid = rt, Type ='General', ContactId = con.Id, Sponsor_name__c = con.Id, Reason_if_Other__c = 'Other', Email_Initial_Response_Date__c = system.now(),ownerid=userinfo.getuserid());          
            casesToInsert.add(caseToInsert);
        }
        
        if(casesToInsert.size()>0)
        {
            insert casesToInsert;
        }
        
        return casesToInsert;
    }
    
    static testMethod void verifySubmisionValidationForCases()
    {  
            User usrRec=[Select ID,Name from User where isActive=true and name='Deployment User' Limit 1];
            system.runas(usrRec){
            LIST<Account> acctList=new LIST<Account>();
            acctList=TestDataUtility_Class.createAccounts(2);
            insert acctList;
            
            LIST<Contact> ConList=new LIST<Contact>();
            ConList=TestDataUtility_Class.createContacts(1,acctList[0]);
            ConList[0].Mobile_Last_Updated__c = system.now();
            LIST<Contact> ConList2=new LIST<Contact>();
            ConList2=TestDataUtility_Class.createContacts(1,acctList[1]);
            ConList2[0].Mobile_Last_Updated__c = system.now();
            ConList.addAll(ConList2);
            insert ConList;
            test.startTest();
            // Saritha Singamsetty: Added the below line for code coverage as part of CHG0171048 
            ANZEmailRouting_BusinessVerticalMapping__mdt emailMapping=[Select Market__c,Routing_Address__c,Business_Unit__c,BusinessVertical__c from ANZEmailRouting_BusinessVerticalMapping__mdt where Routing_Address__c = 'collectionsonecrm@pearson.com' Limit 1];
            LIST<Case> CaseList=new LIST<Case>();
            CaseList=TestDataUtility_Class.createCases(1,acctList[0],ConList[0]);
            insert CaseList;
            
            Case caseRec=[Select AccountID,ContactID from Case where id=:CaseList[0].Id]; 
            
            caseRec.AccountID=acctList[1].Id;
            caseRec.ContactId=ConList[1].Id;
            // Saritha Singamsetty: Added the below line for code coverage as part of CHG0171048 
            caseRec.To_email_origin_address__c = 'collectionsonecrm@pearson.com';  
            update caseRec;
            
            Case caseRec1=[Select AccountID,ContactID from Case where id=:CaseList[0].Id];   
            caseRec1.AccountID=acctList[1].Id;
            caseRec1.ContactId=ConList[1].Id; 
            caseRec1.Create_Account__c = True;  
            caseRec1.First_name__c = 'Test';
            caseRec1.Last_Name__c = 'Last';
            caseRec1.New_Contact_Email__c = 'test@abc.com' ;   
            update caseRec1;           
            
            
            checkRecurssionAfter.executedTriggerName = new Set<String>();
            checkRecurssionBefore.executedTriggerName = new Set<String>();
            
            caseRec=[Select Status,Case_Resolution_category__c,OwnerId,ParentId,sendMassEmail__c from Case where id=:CaseList[0].Id]; 
            
            System.debug('@@Case Owner '+caseRec.OwnerId);
            System.debug('@@Parent Case '+caseRec.ParentId);
            System.debug('@@sendMassEmail__c Case '+caseRec.sendMassEmail__c);    
            caseRec.Status='Closed';
            caseRec.Case_Resolution_category__c='Done';
            caseRec.Escalation_External_Status__c = 'Success';
            caseRec.sendMassEmail__c=true;
            caseRec.Mass_Email_Body__c = 'Test Email Body';   
            //update caseRec;    
            test.stopTest();
            
            checkRecurssionAfter.runOnce('testTrigger', 'Insert');
            checkRecurssionAfter.runOnce('testTrigger', 'Insert');
            checkRecurssionBefore.runOnce('testTrigger', 'Insert');
            checkRecurssionBefore.runOnce('testTrigger', 'Insert');
            Set<Id> caseIds=new Set<Id>();
            caseIds.add(caseRec.id);
            checkRecurssionAfter.runServiceIntOnce(caseIds, 'Update');
        }
    }
    
    /*Description: Test Method added by SA Offshore for SA escalation Case*/
    
    static testMethod void verifyEscalationOfCases()
    {
        User usrRec=[Select ID,Name from User where isActive=true and name='Deployment User' Limit 1];
        system.runas(usrRec){
            LIST<Account> acctList=new LIST<Account>();
            acctList=TestDataUtility_Class.createAccounts(2);
            insert acctList;
            Test.startTest();
            LIST<Contact> ConList=new LIST<Contact>();
            ConList=TestDataUtility_Class.createContacts(1,acctList[0]);               
            ConList[0].Mobile_Last_Updated__c = system.now();     
             
            LIST<Contact> ConList2=new LIST<Contact>();
            ConList2=TestDataUtility_Class.createContacts(1,acctList[1]);            
            ConList2[0].Mobile_Last_Updated__c = system.now();     
            ConList.addAll(ConList2);
            insert ConList;
            
            group grpObj = new group();
            grpObj.type='Queue';
            grpObj.Name = 'NAUS-SA: Accuplacer';    
            insert grpObj;
            
            QueueSobject mappingObject = new QueueSobject(QueueId = grpObj.Id, SobjectType = 'Case');
            insert mappingObject;
            
            String caseRecType = 'School Assessment';
            String caseRecTypeAPIName = 'State_National';
            List<Case> caseList = new List<Case>();
            caseList = TestDataUtility_Class.createCases(1,acctList[0],ConList[0]);
            caseList[0].Escalation_Reason__c = 'test'; 
            caseList[0].Status='Closed';
            caseList[0].Case_Resolution_category__c='Done';
            caseList[0].Escalation_External_Status__c = 'Success';            
            insert caseList;          
            
            
            caseList[0].ownerid=grpObj.id;        
            update caseList;
            Test.stopTest();
            MAP<ID, Case> newCaseMap = new MAP<ID, Case>();
            newCaseMap.put(caseList[0].id,caseList[0]);
            system.debug('TestCCaseRecOwner' +caseList[0].ownerid);
            PS_Case_TriggerSequenceCtrlr.EscalatingCasesOnQueue(newCaseMap);
        } 
    } 
    static testMethod void verifyEscalationOfCases_V1()
    {
        User usrRec=[Select ID,Name from User where isActive=true and name='Deployment User' Limit 1];
         Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrRec.id,Disable_Validation_Rules__c=true);
        insert byp; 
        system.runas(usrRec){
            LIST<Account> acctList=new LIST<Account>();
            acctList=TestDataUtility_Class.createAccounts(2);   
            insert acctList;
            
            LIST<Contact> ConList=new LIST<Contact>();
           
            Contact con1 = new Contact(LastName = 'Contact 1', FirstName = 'fn1', Email = 'test@test.com.demo1',First_Language__c  = 'English',MobilePhone  = '498763425',Salutation = 'MR.',Preferred_Address__c = 'Other Address' ,OtherCountry  = 'India',OtherStreet = 'Test',OtherCity  = 'Test',OtherPostalCode  = '123456');
            //insert con1;
            
            Contact con2 = new Contact(LastName = 'Contact 2', FirstName = 'fn2', Email = 'test@test.com.demo2',First_Language__c  = 'English',MobilePhone  = '498763425',Salutation = 'MR.',Preferred_Address__c = 'Other Address' ,OtherCountry  = 'India',OtherStreet = 'Test',OtherCity  = 'Test',OtherPostalCode  = '123456');
            //insert con2;
            
            ConList.add(con1);
            ConList.add(con2);
            
            group grpObj = new group();
            grpObj.type='Queue';
            grpObj.Name = 'NAUS-SA: Accuplacer';    
            insert grpObj;
            
            QueueSobject mappingObject = new QueueSobject(QueueId = grpObj.Id, SobjectType = 'Case');
            insert mappingObject;
            
            String caseRecType = 'School Assessment';
            String caseRecTypeAPIName = 'State_National';
            List<Case> caseList = new List<Case>();
            caseList = TestDataFactory.createCase_SelfService(1,caseRecType);
            caseList[0].Escalation_Reason__c = 'test';
            caseList[0].PS_Product__c ='N/A';
            insert caseList;    
            
            caseList[0].ownerid=grpObj.id; 
            //update caseList;
            MAP<ID, Case> newCaseMap = new MAP<ID, Case>();
            newCaseMap.put(caseList[0].id,caseList[0]);
            MAP<ID, Case> oldCaseMap = new MAP<ID, Case>();
            oldCaseMap.put(caseList[0].id,caseList[0]); 
            system.debug('TestCCaseRecOwner' +caseList[0].ownerid);
            test.starttest();
            PS_Case_TriggerSequenceCtrlr.afterUpdate(caseList,newCaseMap,newCaseMap);
             PS_Case_TriggerSequenceCtrlr.CopyParentDataOnChild(caseList);
            PS_Case_TriggerSequenceCtrlr.CloseParentWhenAllChildClose(caseList);
            //PS_Case_TriggerSequenceCtrlr.beforeUpdate(caseList);
            PS_Case_TriggerSequenceCtrlr.ValidateCaseUpdate(caseList,newCaseMap);
            List<User> listWithUser = new List<User>();
            listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,2);
            insert listWithUser;
            listWithUser[1].managerId = listWithUser[0].id;
            update listWithUser;
            caseList[0].ownerid=listWithUser[1].id;
            RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND Name = 'Customer Service'];
            caseList[0].recordtypeId = rt.id;
            caseList[0].Status = 'Closed';
            newCaseMap.put(caseList[0].id,caseList[0]);   
            System.debug('updateCaseFieldsAtSurveyInvitation : Case Market : '+ caseList[0]); 
            PS_Case_TriggerSequenceCtrlr.updateCaseFieldsAtSurveyInvitation(caseList,oldCaseMap,newCaseMap); 
            test.stoptest();
        } 
    } 
    
    static testmethod void mergeAccount()
    {
        User usrRec=[Select ID,Name from User where isActive=true and name='Deployment User' Limit 1];
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrRec.id,Disable_Validation_Rules__c=true);
        insert byp; 
        system.runas(usrRec){
            LIST<Account> acctList=new LIST<Account>();
            String accountRecordtypeid =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
            Account act = new Account(Name='AccountUtility_1',
                                      Recordtypeid = accountRecordtypeid , Line_of_Business__c = 'Higher Ed',Geography__c = 'Core');  
            
            Account act2 = new Account(Name='AccountUtility_2',
                                       Recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Learner').getRecordTypeId(), Line_of_Business__c = 'Higher Ed',Geography__c = 'Core');    
            acctList.add(act);
            acctList.add(act2);
          //  Test.startTest();
            
            insert acctList;
            system.debug('@@acctList'+acctList);
            LIST<Contact> ConList=new LIST<Contact>();
            ConList=TestDataUtility_Class.createContacts(1,acctList[0]);
            
            insert ConList;
            
           // Test.startTest(); //Added by Mayank 
            List<case> caseList = new List<case>();
            //caseList =TestDataUtility_Class.createCases(1,acctList[0],ConList[0]);
            caseList =TestDataUtility_Class.createCases(1,acctList[0],ConList[0]);
            caseList[0].AccountId = acctList[0].Id;
            insert caseList;
            //Commented by Mayank
           /* Map<Id,Case> caseOldMap = new Map<Id,Case>();
            caseOldMap.put(caseList[0].id,caseList[0]);*/
            
            caseList[0].accountId=acctList[1].id;
          //  update caseList[0];
            system.debug('@@tempcaseList[0]'+caseList[0]);
            
            Test.startTest();
            Map<Id, Case> nCase = new Map<Id,Case>();
            nCase.put(caseList[0].id, new case(id=caseList[0].id, AccountId=act.id, ContactId=ConList[0].Id));
            DeleteAcrAccount.DeleteAcr(caseList,nCase);
            
            //Commented by Mayank
          /*  Case ca = [Select id,accountid from case where id=:caseList[0].id];
            ca.accountId = acctList[1].id;
            
            update ca;*/
            Test.StopTest();  
            
            
        }
    }   
    static testmethod void deleteCases()
    {
        LIST<Account> acctList=new LIST<Account>();
        acctList=TestDataUtility_Class.createAccounts(2);   
        insert acctList;
        
        LIST<Contact> ConList=new LIST<Contact>();
        ConList=TestDataUtility_Class.createContacts(1,acctList[0]);
        insert ConList;
        
        List<case> casesToDelete = new List<case>();
        //caseList =TestDataUtility_Class.createCases(1,acctList[0],ConList[0]);
        casesToDelete =TestDataUtility_Class.createCases(1,acctList[0],ConList[0]);
        casesToDelete[0].AccountId = acctList[0].Id;
        insert casesToDelete;
         
        Test.startTest();
        Delete casesToDelete;
        Test.stopTest();
    }   
    
}