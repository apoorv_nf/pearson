/*************************************************************
@Author     : Accenture IDC
@Description: Controller to 'SendEmailInstructor' page - sends email to contact(s) that has been associated to opportunity
@Date       : 19/03/2015
**************************************************************/


/* --------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            SendEmailInstructor.cls
Description:      
Date             Version           Author           Tag                                Summary of Changes 
-----------      ----------   -----------------   --------     ---------------------------------------------------------------------------------------
7th Sep 2015      0.2         Rahul Boinepally     T1              Removed the hard coded error messages/headers and replaced with Custom labels.
25th April 2016   0.3           Rony Joseph        T1              Updated code for R5/Rd-01739 .
3rd May 2016      0.4            Abhinav           T1              Added user special permission metadata table for market specific functionalities.
17th May 2016     0.5           Rony Joseph        T1              Updated logic to display print offer for CA market.
18th May 2016     0.5           Rony Joseph        T1              Updated logic to Suggested Retial Price.
19th May 2016     0.5           Rony Joseph        T1              Defect fix for D-5306.
22th June 2016    0.6           Rony Joseph        T1              Added method to inlcude Canada Signature.
10/12/2019        0.7           Manimekalai                        Replaced Apttus objects with Non Apttus Object
--------------------------------------------------------------------------------------------------------------------------------------------------------- */


public class SendEmailInstructor{
    
    public Map<Id,Pearson_Choice__c> mapWithPearsonChoice{get;set;}
    public List<Pearson_Choice__c> listWithPearsonChoices{get;set;}
    public Id productFamilyIdFromURL{get;set;}
    public List<InnerWrapperClass> listWithContactValues{get;set;}
    public List<Contact> selectedContacts{get;set;}
    public Id opportunityIdFromURL;
    public Id productIdFromURL ;
    public Id primaryContactId{get;set;}
    public String primaryContact{get;set;}
    public List<OpportunityContactRole> primaryContactList{get;set;}
    public String subject{get;set;}
    public String emailBody{get;set;}
    public String emailBody1{get;set;}
    public blob documentbody{get;set;}
    public String documentname{get;set;}
    public boolean notifySender{get;set;}
    public String additionalEmailAddress{get;set;}
    public Map<Id,String> mapWithContactRoleAndID{get;set;}
    private String author='',title='',edition='',name='',iSBN='',pcStr='',selectedPrice='',iRCLink='';
    public List<CatalogPrice__c>  suggestedPrice{get; set;}
    public Map<Id,CatalogPrice__c> suggestedPricemap{get; set;}
    public List<PS_MarketSpecialPermission__mdt> ulist = new List<PS_MarketSpecialPermission__mdt>();
    public Decimal suggestedRetailprice {get;set;} 
    public  boolean mailsent {get;set;}
    Set<Id> prodIdset=new Set<Id>();
    public Map<String ,String> mapStr=null;
    //Wrapper class to allow user to select multiple contact(s)
    public class InnerWrapperClass{
        public boolean selectBox{get;set;}
        public Contact contactList{get;set;} 
        public boolean primaryContact{get;set;}
        public InnerWrapperClass(Contact contactRec)//constructor for warapper class
        {
            contactList =  contactRec;
            selectBox = false;   
        }
    }
    
    //Constructor
    public SendEmailInstructor()
    {
        suggestedRetailprice=0.00;
        mapWithContactRoleAndID = new Map<Id,String>();
        opportunityIdFromURL = ApexPages.currentPage().getParameters().get('opportunityId');
        productIdFromURL = ApexPages.currentPage().getParameters().get('productId');
        pcStr =ApexPages.currentPage().getParameters().get('pc');
        selectedPrice =ApexPages.currentPage().getParameters().get('col');
        List<Id> ids = pcStr.split(',');
        List<String> priceList =  selectedPrice.split(',');
        listWithContactValues = new List<InnerWrapperClass>();  
        primaryContactList = new List<OpportunityContactRole>();
        additionalEmailAddress = UserInfo.getUserEmail()+';'+' ';    
        retrieveContactDetails(); //call to pull opportunity contacts
        retrieveProductDetails(); //call to pull product details
        //List<Pearson_Choice__c> listWithPearsonChoices1 = getPearsonChoices(ids); 
        getPearsonChoices(ids);
        subject  = System.Label.Email_Subject+' '+name+'"';
        Messaging.SingleEmailMessage sendEmailMsg= new Messaging.SingleEmailMessage();
        //Header used in Subject
        String headerStr1 = '<br>'+System.Label.EmailBody+'</br>'  +'<br>'+System.Label.EmailBody0+
            '</br><br><b><font size="4">'+name+'</font></b></br><br>'+System.Label.EmailBody1+iRCLink+
            '</br><br> <table border="2" style="width:100%"> <tr><td>';
        //Header used in email body                 
        String headerStr = '</td><td> Includes Pearson eText?</td><td>Access Length</td><td>Bookstore ISBN(s)</td>';
        mapStr = new Map<String,String>();
        if(priceList.size() >0){
            for(String s : priceList){
                if(s.equalsIgnoreCase('Bookstore_Price__c')){        
                    headerStr = headerStr + '<td> Price to Bookstore</td>' ;
                }else if(s.equalsIgnoreCase('Suggested Retail Price')){
                    headerStr = headerStr + '<td> Suggested Retail Price</td>';
                }
                else if(s.equalsIgnoreCase('Instant_Access_Price__c')){
                    headerStr = headerStr + '<td> Instant Access</td>';
                }else if(s.equalsIgnoreCase('eText_Offer_Price__c')){
                    headerStr = headerStr + '<td> eText Offer</td>';
                }
                //else if(s.equalsIgnoreCase('Print_Offer_Price__c')){
                else{
                    headerStr = headerStr + '<td> Print Offer</td>';
                }
                mapStr.put(s,s);
            }
            headerStr = headerStr + '</tr>';
        }else{
            headerStr = headerStr + '</tr>';
        }
        String conCat=ConstructingEmailBody();
        sendEmailMsg.setHtmlBody(headerStr1+headerStr + conCat + '</table></br><br>'+System.Label.EmailBody2+'</br>');
        List<String> lstEmail = new List<String>();
        lstEmail.add(String.valueof('s.reddy.bathula@accenture.com'));
        sendEmailMsg.setToAddresses(lstEmail);
        List<Messaging.SingleEmailMessage> lstMAils = new List<Messaging.SingleEmailMessage>();
        lstMAils.add(sendEmailMsg);  
        emailBody1 = sendEmailMsg.htmlbody;
        Messaging.sendEmail(lstMAils); 
    }
    
    public String ConstructingEmailBody(){
        try{
            String concat='';
            String loggedUserpriceList;
            list<id> listofprintofferids = new list<id>();
            map<id,product2> printofferlistprocemap;
            List<User> loggedInUserpriceListName = [select Price_List__c,market__c,Business_Unit__c from User where id =: UserInfo.getUserId() limit 1];
            ulist = [Select Market__c,Bookstore_Net_price__c,PC_PrintOffer__c,Business_unit__c from PS_MarketSpecialPermission__mdt where market__c =:loggedInUserpriceListName[0].market__c AND Bookstore_Net_price__c=:1 AND Business_unit__c =:loggedInUserpriceListName[0].Business_Unit__c]; 
            if(!loggedInUserpriceListName.isEmpty())
            {
                loggedUserpriceList = String.ValueOf(loggedInUserpriceListName[0].Price_List__c);
            }
            if (prodIdset.size()>0){
                suggestedPrice = [select Product__c,ListPrice__c from CatalogPrice__c 
                                  where Product__c =:prodIdset  and 
                                  Catalog__r.name=:loggedUserpriceList and 
                                  isActive__c = true order by lastmodifieddate desc limit 1000];
                if (!suggestedPrice.isEmpty()){
                    if (suggestedPricemap == null)
                        suggestedPricemap =new Map<Id,CatalogPrice__c>();
                    for(CatalogPrice__c acp:suggestedPrice){
                        if (!suggestedPricemap.containsKey(acp.Product__c)){
                            suggestedPricemap.put(acp.Product__c,acp);
                        }
                    }
                }              
            }
            for(Pearson_Choice__c pc : listWithPearsonChoices){
                if(pc.Print_Offer__c!=null){listofprintofferids.add(pc.Print_Offer__c);}
            }  
            if (!listofprintofferids.isempty()){ printofferlistprocemap = new map<id,product2>([select id,origin__c from product2 where id in : listofprintofferids]);}
            if(printofferlistprocemap == null){printofferlistprocemap = new map<id,product2>();}  
            if (!listWithPearsonChoices.isEmpty()){
                for(Pearson_Choice__c pc : listWithPearsonChoices){   
                    if(pc.Bookstore_ISBN_s__c == null){pc.Bookstore_ISBN_s__c = '0.00';}
                    if(pc.Access_Length__c == null) {pc.Access_Length__c = ' ';}
                    if(pc.Instant_Access_Price__c == null) {pc.Instant_Access_Price__c = 0.00;}
                    if(pc.eText_Offer_Price__c == null) {pc.eText_Offer_Price__c = 0.00;}
                    if(pc.Print_Offer_Price__c == null) {pc.Print_Offer_Price__c = 0.00;}
                    if(pc.Includes_Pearon_eText__c == null) {pc.Includes_Pearon_eText__c = ' ';}
                    if(conCat == ''){
                        if(null != pc.Bookstore_ISBN_s__c){
                            conCat = '<tr><td>'+pc.Brand__c+'</td><td>'+pc.Includes_Pearon_eText__c+
                                '</td><td>'+pc.Access_Length__c+'</td><td>'+pc.Bookstore_ISBN_s__c+'</td>';
                        }
                    }
                    else{
                        if(null != pc.Brand__c ){
                            conCat = conCat + '<tr><td>'+pc.Brand__c+'</td><td>'+pc.Includes_Pearon_eText__c+
                                '</td><td>'+pc.Access_Length__c+'</td><td>'+pc.Bookstore_ISBN_s__c+'</td>';
                        }
                    }
                    
                    if(mapStr.containsKey('Bookstore_Price__c')){
                        if(pc.Bookstore_Price__c == null){
                            conCat = conCat + '<td>'+' '+'</td>';
                        }
                        else{
                            conCat = conCat + '<td>'+ pc.Bookstore_Price__c.setScale(2)+'</td>';
                        }            
                    }
                    if(mapStr.containsKey('Suggested Retail Price') ){
                        if(suggestedPricemap != null && !suggestedPricemap.isEmpty()){
                            
                            if(ulist.isempty()){    
                                if(pc.Bookstore__c != null){    
                                    if(suggestedPricemap.containsKey(pc.Bookstore__c)){
                                        suggestedRetailprice  = suggestedPricemap.get(pc.Bookstore__c).ListPrice__c ;
                                        suggestedRetailprice  = suggestedRetailprice.setScale(2);
                                    }
                                }
                                else if (pc.Instant_Access__c != null){
                                    if(suggestedPricemap.containsKey(pc.Instant_Access__c)){
                                        suggestedRetailprice  = suggestedPricemap.get(pc.Instant_Access__c).ListPrice__c ;
                                        suggestedRetailprice  = suggestedRetailprice.setScale(2);
                                    }
                                }
                            }  
                            else
                            {
                                if(pc.Bookstore__c != null){    
                                    if(suggestedPricemap.containsKey(pc.Bookstore__c)){
                                        suggestedRetailprice  = suggestedPricemap.get(pc.Bookstore__c).ListPrice__c ;
                                        suggestedRetailprice  = suggestedRetailprice.setScale(2);
                                    }
                                    
                                }
                                else
                                {
                                    suggestedRetailprice  = 0.00;
                                    
                                }
                                
                            }  
                        }
                        else{ 
                            
                            suggestedRetailprice  = 0.00;
                        } 
                        
                        
                        if(suggestedRetailprice  == 0.00){
                            conCat = conCat + '<td>'+' '+'</td>'; 
                        } else {
                            conCat = conCat + '<td>'+suggestedRetailprice+'</td>';
                        }      
                    } 
                    if(mapStr.containsKey('Instant_Access_Price__c')){
                        if(ulist.isempty()){ 
                            if(pc.Instant_Access_Price__c == 0.00)
                                conCat = conCat + '<td>'+' ' +'</td>';
                            else 
                            {conCat = conCat + '<td>'+pc.Instant_Access_Price__c.setScale(2) +'</td>';}
                        }   
                        else  
                        {
                            if((suggestedPricemap.containskey(pc.Instant_Access__c))){
                                if(suggestedPricemap.get(pc.Instant_Access__c).ListPrice__c == 0.00)
                                {conCat = conCat + '<td>'+' ' +'</td>';}
                                else 
                                {conCat = conCat + '<td>'+suggestedPricemap.get(pc.Instant_Access__c).ListPrice__c.setScale(2) +'</td>';} 
                            }else 
                            {conCat = conCat + '<td>'+' ' +'</td>';}
                        } 
                    } 
                    if(mapStr.containsKey('eText_Offer_Price__c') && null != pc.eText_Offer_Price__c ){
                        if(pc.eText_Offer_Price__c == 0.00)
                            conCat = conCat + '<td>'+' '+'</td>';
                        else 
                            conCat = conCat + '<td>'+pc.eText_Offer_Price__c.setScale(2) +'</td>';
                    }        
                    if(mapStr.containsKey('Print_Offer_Price__c') && null!= pc.Print_Offer_Price__c ){
                        if(!printofferlistprocemap.isempty() && printofferlistprocemap.containskey(pc.print_offer__c)&& loggedInUserpriceListName[0].market__c =='CA') 
                        {
                            if( printofferlistprocemap.get(pc.print_offer__c).origin__C == 'CA' && pc.Bookstore__c != null && suggestedPricemap.containsKey(pc.Bookstore__c) )//logic to display print offer for CA market
                                conCat = conCat + '<td>'+ (suggestedPricemap.get(pc.Bookstore__c).ListPrice__c*0.57).setScale(2)+'</td>';
                            else     
                                conCat = conCat + '<td>'+' '+'</td>';  
                        }       
                        else{       
                            if(pc.Print_Offer_Price__c == 0.00)
                                conCat = conCat + '<td>'+' '+'</td>';
                            else
                                conCat = conCat + '<td>'+ pc.Print_Offer_Price__c.setScale(2)+'</td>';
                        }    
                    }     
                    conCat = conCat + '</tr>';    
                }
            }
            return conCat;
        }catch(exception e){
            return null;}
    }
    public void retrieveProductDetails()
    {
        Product2 product = [select Name, Author__c, Full_Title__c,  Edition__c, ISBN__c,IRC_Link__c from Product2 
                            where Id =:productIdFromURL limit 1000];    
        if(product.Author__c!=null){ author= product.Author__c ; }
        if(product.Full_Title__c !=null){  title =product.Full_Title__c;  } 
        if(product.Edition__c  !=null){  edition =product.Edition__c ;  }
        if(product.Name  !=null){ name =product.Name ;  }
        if(product.ISBN__c !=null){    iSBN =product.ISBN__c;  }
        if(product.IRC_Link__c !=null){    iRCLink =product.IRC_Link__c;  }
        else {  iRCLink ='';  }
    }
    
    //Method to retrieve the contact details from opportunity
    public void retrieveContactDetails()
    {
        for(OpportunityContactRole contId : [select ContactId,Role from OpportunityContactRole where OpportunityId =: opportunityIdFromURL AND IsDeleted = False limit 1000])
        {    
            //listWithOpptyContactId.add(contId.ContactId); 
            if(contId.Role!=null){
                mapWithContactRoleAndID.put(contId.ContactId,contId.Role);
            }
            else{
                mapWithContactRoleAndID.put(contId.ContactId,'');
            }
        }    
        if(!(mapWithContactRoleAndID.keyset()).isEmpty() && (mapWithContactRoleAndID.keyset()).size()>0)
        {    
            for(Contact conVal :  getContacts(mapWithContactRoleAndID.keyset()))
            {
                listWithContactValues.add(new InnerWrapperClass(conVal));
            }   
        } 
        
        //get primary contact
        primaryContactList = [select ContactId from OpportunityContactRole where OpportunityId =: opportunityIdFromURL AND IsDeleted = False AND IsPrimary=true limit 1000];
        if(!primaryContactList.isEmpty() && primaryContactList.size()>0) 
        {
            List<Contact> listWithPrimaryContact = new List<Contact>();
            listWithPrimaryContact = getPrimaryContact(primaryContactList[0].ContactId);
            primaryContact = listWithPrimaryContact[0].Email;       
        }  
        if(listWithContactValues.isEmpty())
        {    
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,label.PS_HE_Leavebehind_OppConList);
            ApexPages.addMessage(myMsg); 
        }  
    }
    
    //get bulk contact utility
    public List<Contact> getContacts(Set<Id> contactId)
    {
        List<Contact> listWithContact = new List<Contact>();
        listWithContact = [select Id,Name,Email,MailingCity,MailingCountry,MailingState,MailingStreet,MailingPostalCode from Contact where Id IN : contactId limit 1000];
        return listWithContact;
    }
    
    //get single contact utility
    public List<Contact> getPrimaryContact(Id contactId)
    {
        List<Contact> listWithContact = new List<Contact>();
        listWithContact = [select Id,Name,Email,MailingCity,MailingCountry,MailingState,MailingStreet,MailingPostalCode from Contact where Id = : contactId limit 1000];
        return listWithContact;
    }
    //Return list of contact
    public List<InnerWrapperClass> getContactValues()
    {
        if(listWithContactValues.isEmpty())
        {    
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,label.PS_HE_Leavebehind_OppConList);
            ApexPages.addMessage(myMsg); 
        }   
        return listWithContactValues;
    }
    
    //Add the selected contacts to a list and return to next page
    public pageReference addContactToList()
    {
        pageReference nextPage = page.Email_Choice;
        nextPage.setRedirect(false);
        return nextPage;   
    }
    
    //Add the selected contacts to a list and return to next page
    public pageReference addContactToList0()
    {
        
        pageReference nextPage = page.PS_SendEmail_Contacts;
        nextPage.setRedirect(false);
        return nextPage;   
    }
    
    //Add the selected contacts to a list and return to next page
    public pageReference addContactToList1()
    {
        selectedContacts = new List<Contact>();    
        pageReference nextPage = page.Pearson_Choice_SendEmail;
        for(InnerWrapperClass cCon: getContactValues()) 
        {
            if(cCon.selectBox == true) 
            {
                selectedContacts.add(cCon.contactList);
            }
        }
        if(selectedContacts != null && !selectedContacts.isEmpty())
        { 
            if(primaryContact == null)
                primaryContact = selectedContacts[0].Email;
            for(Contact contEmail: selectedContacts)    
            {
                if(contEmail.Email != null && contEmail.Email != primaryContact)
                    additionalEmailAddress=additionalEmailAddress+contEmail.Email+';'+' ';
            }    
        }        
        nextPage.setRedirect(false);
        return nextPage;   
    }
    
    //Add the selected contacts to a list and return to next page
    public pageReference addContactToListRM()
    {
        selectedContacts = new List<Contact>(); 
        
        for(innerWrapperClass cCon: getContactValues()) 
        {   
            if(cCon.selectBox == true && cCon.contactList.Id != primaryContactId )
            {
                selectedContacts.add(cCon.contactList);
            }   
        }
        
        String urlParameters = '';
        if(primaryContactId != null)
        {    
            urlParameters = '?p2_lkid=' + primaryContactId;  
        } 
        else
        {
            urlParameters = '?p2_lkid=';
        }
        
        if(selectedContacts != null && !selectedContacts.isEmpty())
        {
            urlParameters += '&p24=';
            for(Contact cont: selectedContacts)    
            {
                if(cont.Email != null)
                {
                    urlParameters += cont.Email + '; ';  
                }
            } 
        }
        
        /*
if(opportunityIdFromURL != null)
{
urlParameters += '&p3_lkid=' + opportunityIdFromURL;   
}

if(subject != null)
{
urlParameters += '&p6=' + EncodingUtil.urlEncode(subject.left(100), 'UTF-8');
}
if(emailBody1 != null)
{
urlParameters += '&p7=' + EncodingUtil.urlEncode(emailBody1, 'UTF-8');
}*/
        
        Outbound_Notification__c message = new Outbound_Notification__c();
        message.Subject__c = subject.left(100);
        emailBody1 =addCanadaSignature(selectedContacts,emailBody1);
        message.Rich_Body__c = emailBody1;
        insert message;
        
        List<EmailTemplate> templates = [SELECT id FROM EmailTemplate WHERE NAME = 'Email Body Pearson Choices HTML'];
        Id templateId = null;
        if(templates != null && templates.size() > 0)
        {
            templateId = templates.get(0).Id;
            urlParameters += '&p3_lkid=' + message.Id;    
            //urlParameters += '&template_id=00Xg0000000NIwF';
            urlParameters += '&template_id=' + templateId;
        }
        
        urlParameters += '&retURL=/' + opportunityIdFromURL;
        PageReference nextPage = new PageReference('/_ui/core/email/author/EmailAuthor'+urlParameters);
        
        nextPage.setRedirect(false);
        return nextPage;   
    }
    
    //Set email fields in page
    public List<Contact> getselectedContacts()
    {
        return selectedContacts;
    }
    
    //Send email 
    public void sendEmail()
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
        //List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        //Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
        List<String> toAddress = new List<String>();
        
        try
        { 
            toAddress.add(primaryContact );
            List<String> addEmails = additionalEmailAddress.split('; ');
            if(addEmails.size()>0){
                for(String addEmail : addEmails )
                {
                    if(addEmail.length()>0 && addEmail!=null)
                        toAddress.add(addEmail.trim());
                }
            }
            email.setToAddresses(toAddress);    
            email.setHtmlBody(emailBody1); 
            email.setSubject(subject);      
            if(toAddress!=null)
            {         
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,label.PS_HE_Leavebehind_EmailSuccess);
                ApexPages.addMessage(myMsg);
                mailsent = true;
            }          
        }  
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,label.PS_CURRED_Leavebehind_Error);
            ApexPages.addMessage(myMsg);
        }  
    }
    
    //  Method to build the pearson choices tab
    public void getPearsonChoices(List<Id> ids)
    {
        mapWithPearsonChoice = new Map<Id,Pearson_Choice__c>();
        listWithPearsonChoices = new List<Pearson_Choice__c>();
        //if (listWithPearsonChoices == null){ 
        listWithPearsonChoices = [select Id,Access_Length__c,Bookstore__c,Bookstore__r.Name,Bookstore__r.Author__c,
                                  Bookstore__r.Copyright_Year__c,Bookstore__r.Edition__c,Bookstore__r.ISBN__c,Bookstore__r.InstockDate__c,
                                  Bookstore__r.Status__c,Bookstore__r.Sub_Brand__c,Bookstore_ISBN_s__c,Bookstore_Price__c,Brand__c,
                                  eText_Offer__c,eText_Offer_Price__c,Includes_Pearon_eText__c,Instant_Access__r.Name,
                                  Instant_Access__r.Author__c,Instant_Access__r.Copyright_Year__c,Instant_Access__r.Edition__c,
                                  Instant_Access__r.ISBN__c,Instant_Access__r.InstockDate__c,Instant_Access__r.Status__c,
                                  Instant_Access__r.Sub_Brand__c,Instant_Access__c,Instant_Access_Price__c,Master_Product__c,
                                  Offer_Type__c,Platform__c,Print_Offer__c,Print_Offer__r.Name,Print_Offer__r.Author__c,
                                  Print_Offer__r.Copyright_Year__c,Print_Offer__r.Edition__c,Print_Offer__r.ISBN__c,
                                  Print_Offer__r.InstockDate__c,Print_Offer__r.Status__c,Print_Offer__r.Sub_Brand__c,Print_Offer_Price__c,
                                  eText_Offer__r.Name,eText_Offer__r.Author__c,eText_Offer__r.Copyright_Year__c,eText_Offer__r.Edition__c,eText_Offer__r.ISBN__c,eText_Offer__r.InstockDate__c,eText_Offer__r.Status__c,eText_Offer__r.Sub_Brand__c,Product_Family__c,Sequence__c 
                                  from Pearson_Choice__c where  id in: ids Order By Sequence__c,Bookstore_Price__c,Instant_Access_Price__c ASC limit 1000];
        //}   
        if(listWithPearsonChoices.size()>0){
            //prodIdset=new Set<Id>();
            for(Pearson_Choice__c pearsonCho : listWithPearsonChoices ) 
            {
                if (pearsonCho.Bookstore__c != null){
                    prodIdset.add(pearsonCho.Bookstore__c);
                }
                if(pearsonCho.Instant_Access__c != null){
                    prodIdset.add(pearsonCho.Instant_Access__c);
                }
                mapWithPearsonChoice.put(pearsonCho.Id,pearsonCho);
            }        
        }
        //return listWithPearsonChoices;
    } 
    
    //Redirect to opportunity
    public pageReference cancel()
    {
        pageReference productPage; 
        productPage = new  pageReference(System.Label.onecrmFamilyVF+'productFamilyId='+productIdFromURL+'&oppId='+opportunityIdFromURL);
        productPage .setRedirect(true);   
        return productPage;  
    }
    
    //Adds the email signature if atleast one of the email reciepts belongs to Canda Market
    public string addCanadaSignature(list<contact> chosencontact,string htmlbody)
    {
         try{ 
             String idcov=String.valueof(primaryContactId);
             if( idcov != ''){
              contact temp = [select id,mailingcountrycode from contact where id = :primaryContactId limit 1]; 
              chosencontact.add(temp);
              }
            if(!chosencontact.isempty() && htmlbody!=null)
             {
                  map<id,contact> contactlist= new map<id,contact>(chosencontact);
                  list<contact> canadacontact = [select id,mailingcountrycode from contact where id in :contactlist.keyset() and mailingcountrycode = 'CA'];
                  if(!canadacontact.isempty() && canadacontact.size()>0)
                     {
                        user loginuser=[select id,name,email,phone from user where id =: userinfo.getuserid() ];
                        htmlbody+='<br/><br/>'+loginuser.name+'<br/>'+'Pearson Canada<br/>26 Prince Andrew Place<br/>Don Mills,Ontario<br/>M3C 2T8<br/>'
                        +loginuser.phone+'<br/>'+loginuser.email+'<br/>'+'<a href="http://www.pearsoncanada.ca/email-preferences/unsubscribe">Unsubscribe</a>'
                        +' from communications<br/>'+'<a href="http://www.pearsoncanada.ca/email-preferences/unsubscribe">Me désabonner</a>'+' des communications<br/>';
                     }
             }
            return htmlbody;
            }catch(exception e){
                return htmlbody;}  
    }
    
}