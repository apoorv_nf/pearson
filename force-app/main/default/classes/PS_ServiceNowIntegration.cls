/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_ServiceNowIntegration.cls
   Description:     Visual force controller class for PS_AccountCreationRequest page
   Date             Version              Author                          Summary of Changes
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  29/1/2016         0.1              Rashmi Prasad                 RD-01645-Intial escalation,RD-1646-Repeat Escalation
  26/2/2016         0.2              Rohit                         RD-01648-ServiceNow Integration - RESOLUTION from ServiceNow 
  17/3/2016         0.3              Rashmi Prasad                 Updated Method : mgetEscalationQueues
  4/5/2016          0.4              KP                            Amended code
  4/6/2016          0.5              KP                            Integration Fixes - D-4730
  11/08/2016        0.6              RG                            Added for DR-863
  15/7/2016         0.7              Payal                         Updated mUpdateCaseStatuswithEscalationStatus to add new business vertical value
  7/12/2016         0.8              Payal                         Merged CI and R6 changes
  5/5/2017          0.9              Christwin                     Modified Record Type Name for School Assesment
  21/11/2019        0.10             Shiva Nampally                Added the 'Customer Service' Record Type for #CR-03093
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public with sharing class PS_ServiceNowIntegration {
    static Set<Id> oupId=new Set<Id>();
    /**
    * Description : To retrive all queues from Custom setting
    * @param NA
    * @return List of cases
    * @throws NA
    **/

  public static Map<Id,Group> mgetEscalationQueues(){
      Map <String, PS_Escalation_Queues__c> oServiceNowQs = PS_Escalation_Queues__c.getAll();  //to fetch escalation queues from custom setting
      Map<Id,Group> oServiceNowGroups = new Map<Id,Group>();  
      if(Test.isRunningTest()){
        oServiceNowGroups = new Map<Id,Group>([SELECT Id, Name FROM group limit 1]);
      }
      //if(!Test.isRunningTest()){
      else{
        oServiceNowGroups = new Map<Id,Group>([SELECT Id, Name FROM group WHERE Name IN : oServiceNowQs.keySet()]);  //fetch queue names 
      }              
      return  oServiceNowGroups;   
   }
 
 
    /**
    * Description : To perform Intial/repeat escalation
    * @param NA
    * @return List of cases
    * @throws NA
    **/
    
  public static list<Case> mCaseEscalation(List<case> newCases, map<id,case> oldmap, map<id,case> newCaseMap) {
      Map<Id,Group> oServiceNowGroups = mgetEscalationQueues();
      List<Case> oIntCases = new List<Case>();            
      Schema.DescribeSObjectResult describeSobjectResult = Schema.SObjectType.Case; 
      Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeSobjectResult.getRecordTypeInfosById(); 
      for(Case ca: newCases){      
         String sCaseRTName = recordTypeMap.get(ca.recordtypeid).getName();         
         if (Label.PS_CaseRT_TS_SN.contains(sCaseRTName)){      
             if(ca.OwnerId != oldmap.get(ca.id).get('OwnerId') && oServiceNowGroups.containsKey(ca.OwnerId)){
                 oIntCases.add(ca);  
                 oupId.add(ca.Id);
             }      
         }
      }  //end for loop        
      return oIntCases;
  }
  
  /**
    * Description : To perform RESOLUTION from ServiceNow
    * @param NA
    * @return NA
    * @throws NA
    **/
  
  public static void mUpdateCaseStatuswithEscalationStatus(List<case> oNewCaseList ,Map<Id,Case> oOldmap,Map<Id,Case> oNewCaseMap){
        Map<Id,Group> oServiceNowGroups = mgetEscalationQueues();  
        Set<Id> sCaseidList = new set<ID>();
        List<Case> oCaseList = new List<Case>();
        List<case> sUpdateClist = new list<case>();
        oCaseList = CommonUtils.getCaseServiceRecordType(System.Label.PS_ServiceRecordTypes, oNewCaseList );
        Id TSRecordType = Schema.SObjectType.case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
        Id CSRecordType = Schema.SObjectType.case.getRecordTypeInfosByName().get('Customer Service').getRecordTypeId(); //Added by for #CR-3093-Shiva Nampally
        // Modified for School Assesment to Change Record Type
        Id SNRecordType = Schema.SObjectType.case.getRecordTypeInfosByName().get(Label.School_Assessment).getRecordTypeId();
        // Modified for School Assesment to Change Record Type End
        Boolean isAwaited = false;        
        For(case c:oCaseList ){ 
            if(c.Escalation_External_Status__c !=null && 
               c.Escalation_External_Status__c != oOldmap.get(c.id).Escalation_External_Status__c && 
               c.Status ==Label.PS_Case_onholdstatus){
               // Added the CSRecordType to below if condation for the #CR-03093
               if((c.RecordTypeid == TSRecordType || c.RecordTypeid == CSRecordType ) && c.Status != 'Closed' && (c.PS_Business_Vertical__c == 'Higher Education' || c.PS_Business_Vertical__c == 'Digital Support')){
                    if(c.Escalation_External_Status__c == 'Resolved' || c.Escalation_External_Status__c == 'Cancelled' || c.Escalation_External_Status__c == 'Canceled' 
                       || c.Escalation_External_Status__c == 'Deliver' ||
                      (c.Escalation_External_Status__c == 'Closed' && oOldmap.get(c.id).Escalation_External_Status__c != 'Resolved')){                                         
                       //SSaritha:CR-00914: Updated to Status to Additional Information Received
                       // c.Status = Label.PS_CaseInProgressStatus;
                       system.debug('entering here for cs');
                       c.Status = Label.PS_Case_Additional_Information_Received;
                        if (c.Escalation_External_Status__c != 'Closed'){
                        c.Escalation_Point__c='';}
                        if(c.User_Who_Escalted_Case__c != null && c.User_Who_Escalted_Case__c != ''){
                            c.OwnerId= c.User_Who_Escalted_Case__c;
                        }

                     }
                    else if(c.Escalation_External_Status__c == 'Pending' && (c.Escalation_External_Status__c != oOldmap.get(c.id).Escalation_External_Status__c)){
                        if(c.User_Who_Escalted_Case__c != null && c.User_Who_Escalted_Case__c != ''){
                            c.OwnerId= c.User_Who_Escalted_Case__c;
                        }
                        c.Escalation_Point__c='';
                    }
                }
                else if(c.RecordTypeid == SNRecordType){
                    if(c.Escalation_External_Status__c == 'Resolved' || 
                        (c.Escalation_External_Status__c == 'Closed' && oOldmap.get(c.id).Escalation_External_Status__c != 'Resolved')){    
                            c.Status = Label.PS_CaseClosureStatus;
                    }
                }
            }//end if escaltion status change
         }//end for loop
    }//end method definition
}//end class