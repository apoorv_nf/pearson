/************************************************************************************************************
* Apex Interface Name : PS_Pearson_SupportFormControllerTest
* Test Description    : Test Class for  PS_Pearson_SupportFormController
* Test Inputs         : Insert Country and product route number 
* Test Outputs        : Check whether the page is redirected to link based on product route number        
* Version             : 1.1 
* Created Date        : 12/4/2016 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Payal Popat         12/4/2016             
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

@isTest (SeeAllData = false)
Public class PS_Pearson_SupportFormControllerTest{
    public static testMethod void checkWeblink(){
        PS_SupportSignPost_Country__c oCountry = new PS_SupportSignPost_Country__c();
        oCountry.Name = 'Albania';
        
        PS_SupportSignPost_Weblinks__c oWeblinks = new PS_SupportSignPost_Weblinks__c();
        oWeblinks.Name = '1';
        oWeblinks.Weblinks__c = 'https://247pearsoned.custhelp.com/';
        
        Test.StartTest();
        insert oCountry;
        insert oWeblinks;
        Test.StopTest();
        
        PS_Pearson_SupportFormController sc = new PS_Pearson_SupportFormController();
        sc.getCountries();
        sc.SelectProdRoute = '1';
        Pagereference pf = sc.getWeblinks();
        
        System.assert(pf!=NULL,'Error message');
    }
}