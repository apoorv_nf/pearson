@isTest
public class DeleteRecordsBatchTest{

    static testMethod void myTest(){
    
        test.startTest();        
        //---product creation--//             
        List<Product2> sProduct = TestDataFactory.createProduct(1);
        sProduct[0].Market__c = 'AU';
        sProduct[0].Line_of_Business__c= 'Higher Ed';
        sProduct[0].Business_Unit__c = 'Higher Ed';
        sProduct[0].Author__c = 'Talent'; 
        sProduct[0].Copyright_Year__c = '2005';
        sProduct[0].Competitor_Product__c = true;
        insert sProduct;
        system.debug('product details---'+sProduct);
                    
        PriceBookEntry newPriceBookEntry = new PriceBookEntry();
        newPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();//listWithPriceBook[0].Id;
        newPriceBookEntry.UnitPrice = 10.0;
        newPriceBookEntry.Product2Id = sProduct[0].Id;
        newPriceBookEntry.CurrencyIsoCode= 'GBP';
        newPriceBookEntry.IsActive = true;
        insert newPriceBookEntry;
        //--product creation completed--//
        
        List<ProductCategory__c> appClassList = new List<ProductCategory__c>();
        for(Integer i=1;i<=10;i++){
            ProductCategory__c appClass = new ProductCategory__c();
            appClass.CurrencyISOCode = 'USD';
            //appClass.Apttus_Config2__Type__c = 'Offering';
            appClass.Name = 'MASTERBUNDLE';
            appClass.Market__c='AU';
            appClass.Category_ExternalID__c='UA12345'+i;
            appClass.MDMSegment7Type__c='Super Discipline';
            appClass.MDMSegment8Type__c='Discipline';
            appClass.MDMSegment9Type__c='Cluster';
            appClass.MDMSegment10Type__c='Sub Cluster';
            appClass.MDMSegment11Type__c='Super Discipline';
            appClass.MDMSegment12Type__c='Discipline';
            appClass.MDMSegment13Type__c='Cluster';
            appClass.MDMSegment14Type__c='Sub Cluster';
            appClass.MDMSegment15Type__c='Super Discipline';
            appClass.MDMSegment16Type__c='Discipline';
            appClass.MDMSegment17Type__c='Cluster';
            appClass.MDMSegment18Type__c='Sub Cluster';
            appClass.MDMSegment19Type__c='Super Discipline';
            appClass.MDMSegment20Type__c='Discipline';
            appClass.HierarchyLabel__c='Super Parent';
            appClassList.add(appClass);
        }
        insert appClassList;
        
        ProductCategory__c pcat = new ProductCategory__c();
        pcat.Market__c = 'US';
        insert pcat;
        
        Hierarchy__c csHirer = new Hierarchy__c();
        csHirer.ProductCategory__c=appClassList[1].id;
        csHirer.CategoryHierarchy_ExternalId__c = csHirer.id;
        //csHirer.Apttus_Config2__PrimordialId__c = csHirer.id;
        csHirer.Market__c='AU';
        csHirer.CurrencyISOCode ='AUD';
        csHirer.Name='Hierarchy__c';
        csHirer.Label__c='ABCD';
        insert csHirer;
        
        Hierarchy__c hier = new Hierarchy__c();
        hier.ProductCategory__c = pcat.id;
        hier.Label__c = 'Career Placement and Job Success';
        insert hier; 
        
        HierarchyProduct__c prodClass = new HierarchyProduct__c();
        prodClass.ProductCategory__c = csHirer.id;
        /*prodClass.Apttus_Config2__Default__c =true;
        prodClass.Apttus_Config2__DefaultQuantity__c=2;     
        prodClass.Apttus_Config2__MaxQuantity__c = 12;
        prodClass.Apttus_Config2__MinQuantity__c = 4;
 		prodClass.Apttus_Config2__Sequence__c=54;
        prodClass.Apttus_Config2__Modifiable__c =False;
*/
        prodClass.Product__c = sProduct[0].id;
        prodClass.SubHead__c = 'blue';
        //prodClass.SubhHeadSequence__c = 'dark';  
        prodClass.CurrencyISOCode ='AUD';
        prodClass.Delete__c = true;
        insert prodClass;
        
        HierarchyProduct__c hierPrd = new HierarchyProduct__c();
        hierPrd.Product__c = sProduct[0].id;
        hierPrd.Delete__c = true;
        hierPrd.ProductCategory__c = hier.id;
        insert hierPrd;          
        
        RelatedProduct__c relPrd = new RelatedProduct__c();
        relPrd.Product__c = sProduct[0].id;
        relPrd.RelatedProduct__c = sProduct[0].id;
        relPrd.Rank__c = 12;
        //relPrd.Apttus_Config2__RelationType__c = 'Up Sell';
        relPrd.Delete__c = true;
        insert relPrd;
        
        RelatedProduct__c rp = new RelatedProduct__c();
        rp.Product__c = sProduct[1].id;
        rp.RelatedProduct__c = sProduct[1].id;
        rp.Delete__c = true;
        insert rp;
        
        DeleteRecordsBatch drb= new DeleteRecordsBatch ();
        database.executebatch(drb);
        
        Test.StopTest();
    }
    
   static testMethod void myTest1(){
        
        test.startTest();
        Rep_Locator_Result__c reploc = new Rep_Locator_Result__c();
        reploc.Disciplines__c = 'Accounting & Taxation';
        reploc.Delete__c = True;
        insert reploc;
        
        DeleteRecordsBatch drb= new DeleteRecordsBatch ();
        database.executebatch(drb);
                    
        Test.StopTest();
    }
    
}