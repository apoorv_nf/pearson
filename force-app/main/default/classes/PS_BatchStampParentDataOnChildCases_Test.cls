/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_BatchStampParentDataOnChildCases_Test.cls 
   Description:     Test Class for PS_BatchStampParentDataOnChildCases. 
   CR Info:         CR-00275
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  03/04/2017         1.0            MAYANK LAL                       Initial Design 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */

@isTest
public class PS_BatchStampParentDataOnChildCases_Test {
    
    
    static testmethod void testStampParentOnChild() {
         List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
        insert usrLst;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp;         
        System.runas(usrLst[0]){
        Account acc = new Account();
        acc.RecordTypeId = '012b0000000DpIM'; 
        acc.Name = 'Test Account';
        acc.Geography__c = 'Growth';
        acc.Line_of_Business__c = 'Higher Ed';
        acc.Market2__c = 'US';
        
        insert acc;
        System.debug('@@acc '+acc.Name);
        
        List<Contact> lstCon = TestDataFactory.createContacts(1);
        lstCon[0].AccountId = acc.Id;
        insert lstCon;
        System.debug('@@lstCon '+lstCon);
        /*Contact con = new Contact();
        con.LastName = 'Test Con Last Name';
        con.RecordTypeId = '012b0000000Djbt';
        con.Role__c = 'Educator';
        con.AccountId = acc.id;
        con.Phone = '1122334455';
        con.MobilePhone = '9988776655'; 
        con.Email = 'mayank.lal@cognizant.com';
        con.Secondary_Email__c ='mayank.lal@cognizant.com';
        con.Role_Detail__c = 'Associate Dean';
        insert con;
        System.debug('@@con '+con.Name);*/
        
        
        Product__c  pro = new Product__c ();
        pro.name = 'Test Product';
        //pro.RecordTypeId = '012b0000000DcVl';
        pro.CurrencyIsoCode = 'GBP';
        insert pro;
        System.debug('@@pro '+pro.Name);
        //List<Product2> lstProduct = TestDataFactory.createProduct(1);
        //insert lstProduct;
        
        //Creating Parent Case
        Case cs = new Case();
        Map<id,String> parentmap = new Map<id,string>();
        cs.Contact_Type__c = 'College Educator';
        cs.AccountId = acc.Id;
     //   for(Contact ct : lstCon) {
            cs.ContactId = lstCon[0].id;
     //   }
        cs.Status = 'New';
        cs.Origin = 'Email';
        cs.Priority = 'Medium';
        cs.Customer_Username__c = 'test';
        cs.Access_Code__c = 'test';
        cs.Error_Message_Code__c = 'test';
        cs.URL__c = 'www.test.com';
        cs.PS_Business_Vertical__c = 'Clinical';
        cs.Request_Type__c = 'Technical Support';
        cs.Platform__c = 'Q-interactive';
        //for(Product2 pro :lstProduct ) {
            cs.Products__c = pro.id;    
        //}
        cs.Category__c = 'Sign in/ Password';
        cs.Subcategory__c = 'Wrong site';
        cs.Subject = 'test';
        cs.Description = 'test';
        cs.sendMassEmail__c = true;
        cs.Mass_Email_Body__c = 'Test Mail CR 275';
        Test.startTest();
        insert cs;
        
        System.debug('@@cs '+cs.CaseNumber);
        System.debug('@@cssendMassEmail__c '+cs.sendMassEmail__c);
        System.debug('@@csMass_Email_Body__c '+cs.Mass_Email_Body__c);
        parentmap.put(cs.Id, cs.Mass_Email_Body__c);
        
        //Creating Child Cases
        List<Case> lstCase = new List<Case>();
        for(Integer i=0;i<2;i++) {
            Case childcase = new Case();
            childcase.ParentId = cs.Id;
            childcase.AccountId = acc.Id;
           // for(  Contact ct : lstCon) {
                    childcase.ContactId = lstCon[0].id;
            //  }
            childcase.Contact_Type__c = 'College Educator';
            childcase.Status = 'New';
            childcase.Origin = 'Email';
            childcase.Priority = 'Medium';
            childcase.Customer_Username__c = 'test';
            childcase.Access_Code__c = 'test';
            childcase.Error_Message_Code__c = 'test';
            childcase.URL__c = 'www.test.com';
            childcase.PS_Business_Vertical__c = 'Clinical';
            childcase.Request_Type__c = 'Technical Support';
            childcase.Platform__c = 'Q-interactive';
            //for(Product2 pro :lstProduct ) {
            childcase.Products__c = pro.id;    
            //  }
            childcase.Category__c = 'Sign in/ Password';
            childcase.Subcategory__c = 'Wrong site';
            childcase.Subject = 'test';
            childcase.Description = 'test';
            lstCase.add(childcase);
        }
        Database.insert(lstCase, false);
        
    PS_BatchStampParentDataOnChildCases psbatchstamptest = new PS_BatchStampParentDataOnChildCases(parentmap);
    
    Database.executeBatch(psbatchstamptest);
    Test.stopTest();
    }
    
    }
}