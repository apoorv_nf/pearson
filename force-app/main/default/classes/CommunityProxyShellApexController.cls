/************************************************************************************************************
* Apex Interface Name : CommunityProxyShellApexController
* Version             : 1 
* Created Date        : 7/8/2016 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Christwin                  7/8/2016 
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
public class CommunityProxyShellApexController {
    
    @AuraEnabled
    public static string getUserPermissions (boolean RolePresent,boolean ProfilePresent,string permissionSets) {
        try {
            string roleType = 'RoleNotSet';
            string profileType = 'profileNotSet';
            string hasPermissionSet = 'NoInput';
            
            if (RolePresent) {
                List<contact> conList = new List<contact>();
                conList = [SELECT Role__c FROM Contact WHERE ID IN (select contactid from user where ID = :Userinfo.getuserId())];
                if (conList.size() > 0) {
                    roleType = conList[0].Role__c;
                }
            }
			
            if (ProfilePresent) {
                List<profile> profList = new List<profile>();
                profList = [SELECT Name FROM profile WHERE ID IN (select ProfileId from user where ID = :Userinfo.getuserId())];
                if (profList.size() > 0) {
                    profileType = profList[0].Name;
                }
            }
            
            if (permissionSets != 'NoInput') {
                List<string> permissionSetsList = permissionSets.split(',');
                List<PermissionSetAssignment> PermSetAssList = new List<PermissionSetAssignment>();
                PermSetAssList = [SELECT Id 
                                    FROM PermissionSetAssignment 
                                  WHERE PermissionSetId IN (select id from PermissionSet where name in : permissionSetsList) 
                                  AND AssigneeId = : Userinfo.getUserId()];
                if (PermSetAssList.size() > 0) {
                    hasPermissionSet = 'true';
                } else {
                    hasPermissionSet = 'false';
                }
            }
            string retStr = roleType + ':' + profileType + ':' + hasPermissionSet;
        	return retStr;
        } catch (exception ex) {
            system.debug('Error='+ex.getMessage());
        }
        return null;        
    }
    
}