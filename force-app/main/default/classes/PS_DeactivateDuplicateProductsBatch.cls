/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_DeactivateDuplicateProductsBatch
   Description:     Batch class that finds duplicate product in the system and de-activates it.
                    The batch can be run using a dynamic date by passing it as parameter or the batch takes the date from 'PS_DuplicateProductBatchLastRun'
                    custom setting which holds the last run date of this class. This date is used to filter the products in the query locator.
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   15 feb 2016      1.0           Accenture NDC                                         Created
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   24 feb 2016      1.1           Davi Borges                            Add referece to Job Execution Rqequest
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

global class PS_DeactivateDuplicateProductsBatch implements Database.Batchable<sObject>
{
 //Variable(s) declaration
 Date lastModifiedProductDate;
 General_One_CRM_Settings__c lastRunDateFromCS;
 List<String> listWithProductISBN;
 List<Product2> listWithDuplicateProducts; 
 Set<ID> setWithProductId;
 Set<Product2> duplicateProductListForDeactivation;
 List<PS_ExceptionLogger__c> lstWithExceptionErrors;
 Set<Product2> setWithOriginalProdUpdateWithDupRef;
 List<Id> job_execution_requests;
  
 //Constructor 
 global PS_DeactivateduplicateProductsBatch(Date userInputDate, List<Id> job_execution_requests)
 {
     this.job_execution_requests = job_execution_requests;

     //Retrieve the last run date from custom setting if the user input date is null
     lastRunDateFromCS = General_One_CRM_Settings__c.getValues('Duplicate Product Batch Last Run Date');
     if(userInputDate != null)
     {
         lastModifiedProductDate = userInputDate;
     }else if(lastRunDateFromCS != null && lastRunDateFromCS.Duplicate_Product_Batch_Last_Run_Date__c != null)
     {
         lastModifiedProductDate = lastRunDateFromCS.Duplicate_Product_Batch_Last_Run_Date__c; 
     }else
     {
         lastModifiedProductDate = System.Today() - 1;
     }
     
 } 
 
 global Database.QueryLocator start(Database.BatchableContext BC) 
 {
      //Query the product(s)
      if(Test.isRunningtest())
      {
          return Database.getQueryLocator([select id,ISBN__c,Market__c,Line_of_Business__c,Business_Unit__c,IsActive,DM_Identifier__c from Product2 where IsActive = True AND 
                                           Configuration_Type__c = 'Bundle' AND Line_of_Business__c =: Label.PS_HigherEdLOB AND (Market__c =: Label.PS_UKMarket OR Market__c =: Label.PS_AUMarket OR Market__c =: Label.PS_NZMarket) AND
                                           (Business_Unit__c =: Label.PS_HigherEdBU OR Business_Unit__c =: Label.PS_EdifyBU) AND SystemModStamp >=: lastModifiedProductDate]);
      }else
      {
          return Database.getQueryLocator([select id,ISBN__c,Market__c,Line_of_Business__c,Business_Unit__c,IsActive,DM_Identifier__c from Product2 where IsActive = True AND 
                                          (Market__c =: Label.PS_UKMarket OR Market__c =: Label.PS_AUMarket OR Market__c =: Label.PS_NZMarket) AND Line_of_Business__c =: Label.PS_HigherEdLOB AND 
                                          (Business_Unit__c =: Label.PS_HigherEdBU OR Business_Unit__c =: Label.PS_EdifyBU) AND SystemModStamp >=: lastModifiedProductDate]);
      }                                
     
 }
 
 global void execute(Database.BatchableContext BC,List<Product2> productList) 
 {
     listWithDuplicateProducts = new List<Product2>();
     listWithProductISBN = new List<String>();
     setWithProductId = new Set<ID>();
     product2 duplicateProductIdentifier;
     duplicateProductListForDeactivation = new Set<Product2>();
     lstWithExceptionErrors = new List<PS_ExceptionLogger__c>(); 
     PS_ExceptionLogger__c errlogger;
     setWithOriginalProdUpdateWithDupRef = new Set<Product2>();
     product2 productUpdateWithDupRef;
     
     //Iterate over product list and add the ISBN to list
     for(Product2 product : productList)
     {
         listWithProductISBN.add(product.ISBN__c);
         setWithProductId.add(product.Id);
     }
     
     //Query the product table with the ISBN list to check for duplicates
     listWithDuplicateProducts = [Select id,ISBN__c,Market__c,Line_of_Business__c,Business_Unit__c,IsActive,DM_Identifier__c from Product2 where IsActive = True AND 
                                  (Market__c =: Label.PS_UKMarket OR Market__c =: Label.PS_AUMarket OR Market__c =: Label.PS_NZMarket) AND Line_of_Business__c =: Label.PS_HigherEdLOB AND 
                                  (Business_Unit__c =: Label.PS_HigherEdBU OR Business_Unit__c =: Label.PS_EdifyBU) AND ISBN__c IN : listWithProductISBN AND Id NOT IN : setWithProductId];
     system.debug('####1234'+ listWithDuplicateProducts);
     /*Iterate over the duplicate product list and segregate based on Market 
     And also identify which product's DMIdentifier starts with EPM for UK products and 'BM' for AU and NZ products to de-activate it.
     Also, if product with DMIdentifier is not found which starts with 'EPM' or 'BM', then add it to exception logger*/
     
     for(Product2 duplicateProduct : listWithDuplicateProducts)  
     {
         for(Product2 recentModProd : productList)
         {
             duplicateProductIdentifier = new Product2();
             productUpdateWithDupRef = new Product2();
             errlogger=new PS_ExceptionLogger__c();
             if(duplicateProduct.ISBN__c == recentModProd.ISBN__c)
             {
                 //Identify product with 'EPM' for UK market
                 if(duplicateProduct.Market__c == Label.PS_UKMarket && duplicateProduct.Line_of_Business__c == Label.PS_HigherEdLOB && duplicateProduct.Business_Unit__c == Label.PS_HigherEdBU && recentModProd.Market__c == Label.PS_UKMarket && recentModProd.Line_of_Business__c == Label.PS_HigherEdLOB && recentModProd.Business_Unit__c == Label.PS_HigherEdBU && duplicateProduct.DM_Identifier__c != null && recentModProd.DM_Identifier__c != null)
                 {
                     if((duplicateProduct.DM_Identifier__c).startsWithIgnoreCase('EPM'))
                     {
                         duplicateProductIdentifier.Id = duplicateProduct.Id;
                         duplicateProductIdentifier.IsActive = False;
                         duplicateProductListForDeactivation.add(duplicateProductIdentifier);
                         //Update the original product with duplicate product Id
                         productUpdateWithDupRef.Id = recentModProd.Id;
                         productUpdateWithDupRef.Ref_Product__c = duplicateProduct.Id;
                         setWithOriginalProdUpdateWithDupRef.add(productUpdateWithDupRef); 
                     }else if((recentModProd.DM_Identifier__c).startsWithIgnoreCase('EPM'))
                     {
                         duplicateProductIdentifier.Id = recentModProd.Id;
                         duplicateProductIdentifier.IsActive = False;
                         duplicateProductListForDeactivation.add(duplicateProductIdentifier);
                         //Update the original product with duplicate product Id
                         productUpdateWithDupRef.Id = recentModProd.Id;
                         productUpdateWithDupRef.Ref_Product__c = duplicateProduct.Id;
                         setWithOriginalProdUpdateWithDupRef.add(productUpdateWithDupRef); 
                     }else
                     {
                         errlogger.InterfaceName__c='DeactivateDuplicateProductsBatch';
                         errlogger.ApexClassName__c='PS_DeactivateDuplicateProductsBatch';
                         errlogger.CallingMethod__c='execute';
                         errlogger.UserLogin__c = UserInfo.getUserName();   
                         errlogger.ExceptionMessage__c = Label.PS_DuplicateProduct;
                         errlogger.RecordId__c = duplicateProduct.Id;
                         lstWithExceptionErrors.add(errlogger);
                     }
                 } //Identify product with 'BM' for AU and NZ market
                 else if(((duplicateProduct.Market__c == Label.PS_AUMarket && duplicateProduct.Line_of_Business__c == Label.PS_HigherEdLOB && duplicateProduct.Business_Unit__c == Label.PS_HigherEdBU && recentModProd.Market__c == Label.PS_AUMarket && recentModProd.Line_of_Business__c == Label.PS_HigherEdLOB && recentModProd.Business_Unit__c == Label.PS_HigherEdBU) || (duplicateProduct.Market__c == Label.PS_NZMarket && duplicateProduct.Line_of_Business__c == Label.PS_HigherEdLOB && duplicateProduct.Business_Unit__c == Label.PS_EdifyBU && recentModProd.Market__c == Label.PS_NZMarket && recentModProd.Line_of_Business__c == Label.PS_HigherEdLOB && recentModProd.Business_Unit__c == Label.PS_EdifyBU)) && duplicateProduct.DM_Identifier__c != null && recentModProd.DM_Identifier__c != null)
                 {
                     if((duplicateProduct.DM_Identifier__c).startsWithIgnoreCase('BM'))
                     {
                         duplicateProductIdentifier.Id = duplicateProduct.Id;
                         duplicateProductIdentifier.IsActive = False;
                         duplicateProductListForDeactivation.add(duplicateProductIdentifier); 
                         //Update the original product with duplicate product Id
                         productUpdateWithDupRef.Id = recentModProd.Id;
                         productUpdateWithDupRef.Ref_Product__c = duplicateProduct.Id;
                         setWithOriginalProdUpdateWithDupRef.add(productUpdateWithDupRef); 
                     }else if((recentModProd.DM_Identifier__c).startsWithIgnoreCase('BM'))
                     {
                         duplicateProductIdentifier.Id = recentModProd.Id;
                         duplicateProductIdentifier.IsActive = False;
                         duplicateProductListForDeactivation.add(duplicateProductIdentifier);
                         //Update the original product with duplicate product Id
                         productUpdateWithDupRef.Id = recentModProd.Id;
                         productUpdateWithDupRef.Ref_Product__c = duplicateProduct.Id;
                         setWithOriginalProdUpdateWithDupRef.add(productUpdateWithDupRef);  
                     }else
                     {
                         errlogger.InterfaceName__c='DeactivateDuplicateProductsBatch';
                         errlogger.ApexClassName__c='PS_DeactivateDuplicateProductsBatch';
                         errlogger.CallingMethod__c='execute';
                         errlogger.UserLogin__c = UserInfo.getUserName();   
                         errlogger.ExceptionMessage__c = Label.PS_DuplicateProduct;
                         errlogger.RecordId__c = duplicateProduct.Id;
                         lstWithExceptionErrors.add(errlogger);
                     }        
                 }
             }
         }
     }  
     //Update the duplicate products to de-activate it
     if(duplicateProductListForDeactivation.size() > 0)
     {
         List<Product2> listWithDuplicateProductsForDeactivation = new List<Product2>();
         listWithDuplicateProductsForDeactivation.addAll(duplicateProductListForDeactivation);
         update listWithDuplicateProductsForDeactivation;
     }   
     //Insert exception logger 
     if(lstWithExceptionErrors.size() > 0)
     {
         insert lstWithExceptionErrors;
     }  
     //Update the Original product with duplicate product Id
     if(setWithOriginalProdUpdateWithDupRef.size() > 0)
     {
         List<Product2> listWithOriginalProdUpdateWithDupRef = new List<Product2>();
         listWithOriginalProdUpdateWithDupRef.addAll(setWithOriginalProdUpdateWithDupRef);
         update listWithOriginalProdUpdateWithDupRef;
     }                    
 }
 
 global void finish(Database.BatchableContext BC) 
 {
     //Update the custom setting value to today, so that when the batch runs next time, it retrieves the last batch run date from custom setting.
     General_One_CRM_Settings__c lastRunDate = General_One_CRM_Settings__c.getInstance('Duplicate Product Batch Last Run Date');
     lastRunDate.Duplicate_Product_Batch_Last_Run_Date__c = System.Today();
     update lastRunDate;

     if(this.job_execution_requests!= null)
     {

        List<Job_Execution_Request__c> requests = new List<Job_Execution_Request__c>();

        for(String job_id : this.job_execution_requests )
        {
            Job_Execution_Request__c request = new Job_Execution_Request__c();
            request.Id = job_id ;
             request.Job_Product_Deduplication_Completion__c = Datetime.now();
             requests.add(request);

        }

        update requests;

     }

 }
 
}