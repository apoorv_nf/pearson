@isTest
public class PS_MonitorPotentialOpptyBatchTest {

    //Method to cover the test class
    static testMethod void myUnitTest() {
        createCustomSettingData();
        PS_MonitorPotentialOpptyCreationBatch  ptoc = new PS_MonitorPotentialOpptyCreationBatch();
        //ptoc.runningBatchJobs = 5;
        //code for running the scheduler
                Test.startTest();
                    DateTime n = datetime.now().addMinutes(60);
                    String cron = '';
                    cron += n.second();
                    cron += ' ' + n.minute();
                    cron += ' ' + n.hour();
                    cron += ' ' + n.day();
                    cron += ' ' + n.month();
                    cron += ' ' + '?';
                    cron += ' ' + n.year();
                    String jobName = 'Batch Job To Create Opportunity - ' + n.format('MM-dd-yyyy-hh:mm:ss');
                    String jobId = System.schedule(jobName, cron, new PS_MonitorPotentialOpptyCreationBatch());

                    CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];

                    System.assertEquals(cron, ct.CronExpression); 
                    System.assertEquals(0, ct.TimesTriggered);
                    //System.assertEquals('2015-05-27 02:27:07', String.valueOf(ct.NextFireTime));

            Test.stopTest();
    }
    
   static void createCustomSettingData() {
        List<General_One_CRM_Settings__c> generalSettingList = new List<General_One_CRM_Settings__c>();
        General_One_CRM_Settings__c generalSetting = new General_One_CRM_Settings__c();
        generalSetting.Category__c = 'Batch Processing';
        generalSetting.Description__c = 'Limit of Potential Target that will be executed per cycle';
        generalSetting.Value__c = '15';
        generalSetting.name = 'Potential Target Monitor Restart Mins';
        generalSettingList.add(generalSetting);
        insert generalSettingList;
        
   }
}