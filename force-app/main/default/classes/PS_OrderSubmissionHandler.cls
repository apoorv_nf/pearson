/************************************************************************************************************
* Apex Interface Name : PS_OrderSubmissionHandler
* Version             : 1.0 
* Created Date        : 14 Dec 2015
* Function            : Utility to determine if Orders are CURRENTLY in a submission execution, so that validation can be temporarily supressed
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Tom Carman            14/Dec/2015            Initial version
************************************************************************************************************/

public class PS_OrderSubmissionHandler { 

	private static Map<Id, Boolean> orderSubmissionMap = new Map<Id, Boolean>();

	public static void setOrderCurrentlyBeingSubmittedMap(Map<Id, Order> newOrders, Map<Id, Order> oldOrders) {

		for(Order newOrder : newOrders.values()) {
			if(oldOrders.containsKey(newOrder.Id)) {
				orderSubmissionMap.put(newOrder.Id, checkIfOrderBeingSubmitted(newOrder, oldOrders.get(newOrder.Id)));
			}
		}
	}

	private static Boolean checkIfOrderBeingSubmitted(Order newOrder, Order oldOrder) {

		return PS_Constants.ORDER_SUBMITTED_STATUSES.contains(newOrder.Status) && 
			  !PS_Constants.ORDER_SUBMITTED_STATUSES.contains(oldOrder.Status);

	}


	public static Boolean isOrderBeingSubmitted(Id orderId) {

		
		if(!orderSubmissionMap.containsKey(orderId)) {
			return false;
		} else {
			return orderSubmissionMap.get(orderId);
		}
	} 



}