/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 08/07/2018
 */
global without sharing class EinsteinBotGetPrechatData {
	private static final String CHROME = 'Chrome';
	private static final String SAFARI = 'Safari';
	private static final String FIREFOX = 'Firefox';
	private static final String INTERNET_EXPLORER = 'Internet Explorer';
	private static final String MICROSOFT_EDGE = 'Microsoft Edge';
	private static final String UNKNOWN = 'Unknown';

    @InvocableMethod(label='Einstein Bot - Get Prechat Data')
    global static List<PreChatDataResponse> getPreChatData(List<String> liveAgentSessionIds) 
    {
		List<PreChatDataResponse> responses = new List<PreChatDataResponse>();

		try {
			for(LiveChatTranscript transcript : [
				SELECT Id, ContactId, Contact.Email, CaseId, Case.CaseNumber, Browser, Contact.Account.RecordType.Name
				FROM LiveChatTranscript
				WHERE ChatKey IN :liveAgentSessionIds]) {

				PreChatDataResponse response = new PreChatDataResponse();
			
				if(String.isEmpty(transcript.ContactId)) {
					response.matchFound = false;
				} else {
					response.matchFound = true;
					try {
						response.newAccount = transcript.Contact.Account.RecordType.Name == 'Account Creation Request' ? true : false;
					} catch(Exception ex) {}
					response.contact = new Contact(
						Id = transcript.ContactId,
						Email = transcript.Contact.Email
					);
				}

				response.cas = [SELECT Id, CaseNumber FROM Case WHERE Id = :transcript.CaseId];

				response.browser = formatBrowser(transcript.Browser);

				responses.add(response);
			}
		} catch(Exception ex) {
			return responses;
		}

		return responses;
	}

	private static String formatBrowser(String browser) {
		if(browser.containsIgnoreCase('chrome')) {
			return CHROME;
		} else if(browser.containsIgnoreCase('safari')) {
			return SAFARI;
		} else if(browser.containsIgnoreCase('firefox')) {
			return FIREFOX;
		} else if(browser.containsIgnoreCase('explorer')) {
		return INTERNET_EXPLORER;
		} else if(browser.containsIgnoreCase('edge')) {
			return MICROSOFT_EDGE;
		}

		return UNKNOWN;
	}

    global class PreChatDataResponse
    {
        @InvocableVariable
        global Contact contact;

        @InvocableVariable
        global Boolean matchFound;

        @InvocableVariable
        global Case cas;

		@InvocableVariable
		global String browser;

		@InvocableVariable
		global Boolean newAccount;
    }
}