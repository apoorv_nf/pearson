/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_AccountCreationRequestController.cls
   Description:     Visual force controller class for PS_AccountCreationRequest page
   Date             Version              Author                          Summary of Changes
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  11-Dec-2015         0.1              Rashmi Prasad                 RD-01482-Orphan cases
  8-Apr-2016          0.2              KP                            updated record type query using dynamic apex
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public with sharing class PS_AccountCreationRequestController {
 
  /**
   * Description : To redirect to Account request creation page based on Profile of the user
   * @param NA
   * @return PageReference
   * @throws NA
  **/

 public PageReference openaccountpage() {
  String pname = [select id, name from Profile where id = : userInfo.getProfileId()].name;
  Id accRecordTypeId= Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.PS_AccountCreationRequest_ID).getRecordTypeId();       

  if (Label.PS_AccountCreationRequest_Profile.contains(pname)) {
   PageReference pref = new PageReference('/001/e?retURL=%2F001%2Fo&RecordType='+ accRecordTypeId +' &ent=Account');
   pref.setRedirect(true);
   return pref;
  }
  else {
   PageReference pref = new PageReference('/apex/PS_NewAccountRequest');
   pref.setRedirect(true);
   return pref;
  }
 }

}