/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_Account_RecordTagging_Ctrlr.cls 
   Description:     On insert/update/ of Account record 
   Date             Version         Author                              Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  05/2015         1.0               Davi Borges                       Initial Release 
  05/2017         2.0               Abraham Daniel                   To update the region from current user and also the record type to Organisation for a specific condition
                  3.0               Sasiraja                            CR-01868-Req-23 , commented line 67 to remove validation for TEPNA project
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class PS_Account_RecordTagging_Ctrlr
{  
  public static void accountRecordTagging(List<Account> accountUpdateList)
  {

   List<User> userLst = new List<User>();

         String user_id = UserInfo.getUserId();

         List <User> userdetails = [select Market__c , Line_of_Business__c , Geography__c,Region__c, Name from User where id =:user_id limit 1];

       if(userdetails[0].Market__c != null && userdetails[0].Line_of_Business__c != null && userdetails[0].Geography__c != null && !userdetails.isEmpty())
        { 
        for(Account newAcc: accountUpdateList)
        {
           
        //lead l = new lead();
        if(newAcc.Market2__c == null || newAcc.Market2__c == '')
        {
            newAcc.Market2__c = userdetails[0].Market__c;
        }
        if(newAcc.Line_of_Business__c == null || newAcc.Line_of_Business__c == '')
        {
            newAcc.Line_of_Business__c = userdetails[0].Line_of_Business__c;
        }
        if(newAcc.Geography__c == null || newAcc.Geography__c == '')
        {
            newAcc.Geography__c = userdetails[0].Geography__c;
        }
            //Abraham For CR-1527 to add region and record type
            
        if(newAcc.Region__c == null || newAcc.Region__c == '')
        {
            newAcc.Region__c = userdetails[0].Region__c;
        }
        /*if(newAcc.Is_created_from_case__c != True && userdetails[0].Geography__c == 'North America' && userdetails[0].Line_of_Business__c == 'Higher Ed' && userdetails[0].Market__c == 'US')
        {
            newAcc.RecordtypeId = Label.Record_type_of_Account_Organisation;
        }*/
            //Abraham Changes For CR-1527 Ends
            }
            //leadLst.add(newlead);
            

         }
          //Abraham:-  Commenting below market so MDM will be able to create account without market
         else if(/*accountUpdateList[0].Market__c != null*/ accountUpdateList[0].Line_of_Business__c != null && accountUpdateList[0].Geography__c != null )
         {
         }
         else
         {
         // accountUpdateList[0].addError('Record can’t be created without the Market, Line of Business and Geography information in Current User');
         }

         //if(!leadLst.isEmpty())
         //{
             //update leadLst;
             
        // }           
    }
}