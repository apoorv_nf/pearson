/* Test Class to Cover PS_TerritoryMovedAddeddeleted Trigger & its Handler, PS_UserAdderorRemovedNotificationTrigger and its handler
and cdsOpportunityClosingNotification() of Opportunity Trigger and Handler.
Author : Accenture IDC */ 


@isTest
public class PS_TerritoryMovedAddeddeletedTest
{
public static List<User> userLSt = new List<User>();
public static void createUser(){

    userLSt = TestDataFactory.createUser(Userinfo.getProfileid());
    userLSt[0].Market__c = 'US'; 
    userLSt[1].Market__c = 'US';
    userLSt[2].Market__c = 'UK';
    userLSt[3].Market__c = 'UK';
    userLSt[4].Market__c = 'AU';
    userLSt[5].Market__c = 'AU';
    userLSt[6].Market__c = 'NZ';
    userLSt[7].Market__c = 'NZ';
    insert userLSt;

}
public static void createGroupMemeber(String groupName){

   system.debug('Dont remove this system debug otherwise you will get cpu limitation error: '+[Select id from Group limit 10]); 
   List<Group> grpObj = [Select id from Group where name=:groupName limit 1];
   GroupMember grpMemeberObj = new GroupMember();
   grpMemeberObj.GroupId = grpObj[0].id;
   grpMemeberObj.UserOrGroupId = userLSt[10].id;
   insert grpMemeberObj;

}


static testMethod void myTerritoryOpertaionUK(){
        createUser();  
        createGroupMemeber('UK Territory Administrator');
        
        test.StartTest();
             //Run AS UK User 
             System.runas(userLSt[3]){
                  Territory2 tr0 = TestDataFactory.createTerritory();
                  tr0.Territory_Code__c = 'AAA';
                  insert tr0;
      
                  Territory2 tr = new Territory2();
                  tr.DeveloperName = 'TestTerritoryUK';
                  tr.Name = 'TestTerritory';
                  tr.Territory2ModelId = tr0.Territory2ModelId;
                  tr.Territory2TypeId = tr0.Territory2TypeId;
                  tr.ParentTerritory2Id = tr0.id;
                  tr.Territory_Code__c = 'XXX';
                  insert tr;
                  
                  Territory2 tr1 = new Territory2();
                  tr1.DeveloperName = 'TestTerritory1UK';
                  tr1.Name = 'TestTerritory1';
                  tr1.Territory2ModelId = tr0.Territory2ModelId;
                  tr1.Territory2TypeId = tr0.Territory2TypeId;
                  tr1.ParentTerritory2Id = tr0.Id;
                  tr1.Territory_Code__c = 'YYY';
                  insert tr1;
                 
                  tr1.ParentTerritory2Id = tr.Id;
                  update tr1;

                  Territory2 tr2 = new Territory2();
                  tr2.DeveloperName = 'TestTerritory2UK';
                  tr2.Name = 'TestTerritory2';
                  tr2.Territory2ModelId = tr0.Territory2ModelId;
                  tr2.Territory2TypeId = tr0.Territory2TypeId;
                  tr2.ParentTerritory2Id = tr.Id;
                  tr2.Territory_Code__c = 'ZZZ';
                  insert tr2;

            }
        test.StopTest();

 }


 static testMethod void myTerritoryOpertaionAU(){
  
        createUser(); 
        createGroupMemeber('ANZ Territory Administrator');
        test.StartTest();
            
            //Run AS AU User 
             System.runas(userLSt[5]){
                  PS_Util.isFetchedUserRecord = false;
                  Territory2 tr0 = TestDataFactory.createTerritory();
                  tr0.Territory_Code__c = 'AAA';
                  insert tr0;
                  Territory2 tr = new Territory2();
                  tr.DeveloperName = 'TestTerritoryAU';
                  tr.Name = 'TestTerritory';
                  tr.Territory2ModelId = tr0.Territory2ModelId;
                  tr.Territory2TypeId = tr0.Territory2TypeId;
                  tr.ParentTerritory2Id = tr0.id;
                  tr.Territory_Code__c = 'XXX';
                  insert tr;
                  
                  Territory2 tr1 = new Territory2();
                  tr1.DeveloperName = 'TestTerritory1AU';
                  tr1.Name = 'TestTerritory1';
                  tr1.Territory2ModelId = tr0.Territory2ModelId;
                  tr1.Territory2TypeId = tr0.Territory2TypeId;
                  tr1.ParentTerritory2Id = tr0.Id;
                  tr1.Territory_Code__c = 'YYY';
                  insert tr1;
                 
                  tr1.ParentTerritory2Id = tr.Id;
                  update tr1;

                  Territory2 tr2 = new Territory2();
                  tr2.DeveloperName = 'TestTerritory2AU';
                  tr2.Name = 'TestTerritory2';
                  tr2.Territory2ModelId = tr0.Territory2ModelId;
                  tr2.Territory2TypeId = tr0.Territory2TypeId;
                  tr2.ParentTerritory2Id = tr.Id;
                  tr2.Territory_Code__c = 'ZZZ';
                  insert tr2;

            }
        test.StopTest();

 }

 
 static testMethod void myTerritoryOpertaionNZ(){
        createUser(); 
         
        createGroupMemeber('ANZ Territory Administrator');
        test.StartTest();
            
            //Run AS NZ User 
             System.runas(userLSt[6]){
                  PS_Util.isFetchedUserRecord = false;
                  Territory2 tr0 = TestDataFactory.createTerritory();
                  tr0.Territory_Code__c = 'AAA';
                  insert tr0;
                  Territory2 tr = new Territory2();
                  tr.DeveloperName = 'TestTerritoryNZ';
                  tr.Name = 'TestTerritory';
                  tr.Territory2ModelId = tr0.Territory2ModelId;
                  tr.Territory2TypeId = tr0.Territory2TypeId;
                  tr.ParentTerritory2Id = tr0.id;
                  tr.Territory_Code__c = 'XXX';
                  insert tr;
                  
                  Territory2 tr1 = new Territory2();
                  tr1.DeveloperName = 'TestTerritory1NZ';
                  tr1.Name = 'TestTerritory1';
                  tr1.Territory2ModelId = tr0.Territory2ModelId;
                  tr1.Territory2TypeId = tr0.Territory2TypeId;
                  tr1.ParentTerritory2Id = tr0.Id;
                  tr1.Territory_Code__c = 'YYY';
                  insert tr1;
                 
                  tr1.ParentTerritory2Id = tr.Id;
                  update tr1;

                  Territory2 tr2 = new Territory2();
                  tr2.DeveloperName = 'TestTerritory2NZ';
                  tr2.Name = 'TestTerritory2';
                  tr2.Territory2ModelId = tr0.Territory2ModelId;
                  tr2.Territory2TypeId = tr0.Territory2TypeId;
                  tr2.ParentTerritory2Id = tr.Id;
                  tr2.Territory_Code__c = 'ZZZ';
                  insert tr2;

            }
        test.StopTest();

 }






static testMethod void myUnitTest()
{
  createUser();    
  test.StartTest();
  Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=userLSt[1].id,Disable_Triggers__c=true,Disable_Validation_Rules__c =true,Disable_Process_Builder__c =true);
    insert byp; 
//Run AS US User 
 System.runas(userLSt[1]){
 
        PS_Util.isFetchedUserRecord = false;
        Territory2 tr0 = TestDataFactory.createTerritory();
        tr0.Territory_Code__c = 'AAA';
        insert tr0;
       
        
 
     // List<Territory2> childTerritoryList = new List<Territory2>();
      Territory2 tr = new Territory2();
      tr.DeveloperName = 'TestTerritoryUS';
      tr.Name = 'TestTerritory';
     tr.Territory2ModelId = tr0.Territory2ModelId;
     tr.Territory2TypeId = tr0.Territory2TypeId;
      tr.ParentTerritory2Id = tr0.id;
      tr.Territory_Code__c = 'XXX';
      insert tr;
      
      Territory2 tr1 = new Territory2();
      tr1.DeveloperName = 'TestTerritory1US';
      tr1.Name = 'TestTerritory1';
     tr1.Territory2ModelId = tr0.Territory2ModelId;
     tr1.Territory2TypeId = tr0.Territory2TypeId;
      tr1.ParentTerritory2Id = tr0.Id;
      tr1.Territory_Code__c = 'YYY';
      insert tr1;
     
      tr1.ParentTerritory2Id = tr.Id;
      update tr1;

      Territory2 tr2 = new Territory2();
      tr2.DeveloperName = 'TestTerritory2US';
       tr2.Name = 'TestTerritory2';
      tr2.Territory2ModelId = tr0.Territory2ModelId;
     tr2.Territory2TypeId = tr0.Territory2TypeId;
      tr2.ParentTerritory2Id = tr.Id;
      tr2.Territory_Code__c = 'ZZZ';
      insert tr2;



 UserTerritory2Association ut2aP = new UserTerritory2Association();
  ut2aP.Territory2Id = tr.Id;   
  ut2aP.UserId = userLSt[0].id;
  ut2aP.RoleInTerritory2 = 'District Manager';
  insert ut2aP;
  UserTerritory2Association ut2a = new UserTerritory2Association();
  ut2a.Territory2Id = tr1.Id;   
  ut2a.UserId = userLSt[0].id;
  ut2a.RoleInTerritory2 = 'Customer Digital Success Agent';
  insert ut2a;
  UserTerritory2Association ut2a2 = new UserTerritory2Association();
  ut2a2.Territory2Id = tr2.Id;   
  ut2a2.UserId = userLSt[0].id;
  ut2a2.RoleInTerritory2 = 'Sales Representative';
  insert ut2a2;
   //delete ut2a;
 
   // Code to Cover cdsOpportunityClosingNotification in Opportunity Trigger.
  createAcc();
  
  delete ut2a;
  }
 test.StopTest();
 
 
  
}
@testSetup static void setup() {
    // Create common test accounts
    Account testAcct = new Account(Name = 'TestAcct', Geography__c = 'North America', Region__c = 'United States',Line_of_Business__c ='Schools', 
                                   Market2__c = 'US',BillingCountry = 'Australia',BillingState = 'Victoria',BillingCountryCode = 'AU',BillingStateCode = 'VIC',
                                   ShippingCountry = 'Australia',ShippingState = 'Victoria',ShippingCountryCode = 'AU',ShippingStateCode = 'VIC',Territory_Code_s__c = 'YYY',
                                   ShippingCity ='Shipping City', ShippingStreet = 'Shipping Street', ShippingPostalCode = 'NE27 0QQ',IsCreatedFromLead__c = true);
   
    insert testAcct;   
    List<PS_TerritoryMovedAddeddeleted__c> lstWithTerritoryCustomSetting = new List<PS_TerritoryMovedAddeddeleted__c>();
          lstWithTerritoryCustomSetting = TestDataFactory.createTerritoryCustomSettingRecord();
          insert lstWithTerritoryCustomSetting;    
}
public static void createAcc(){
List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
    insert usrLst;
    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
    insert byp;         
    System.runas(usrLst[0]){    
Account acct = [SELECT Id,Territory_Code_s__c FROM Account WHERE Name='TestAcct' LIMIT 1];
  

Opportunity sOpportunity          = (Opportunity)TestClassAutomation.createSObject('Opportunity');
sOpportunity.AccountId            = acct.Id;

sOpportunity.RecordTypeId         = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
sOpportunity.Digital_Indicator__c = true;
insert sOpportunity;

sOpportunity.StageName            = 'Closed';
sOpportunity.Decision_Date__c = system.today();
sOpportunity.TotalOpportunityQuantity=10;
sOpportunity.Enrolments__c=30;
sOpportunity.Lost_Reason__c='Other';
update sOpportunity;
  acct.Territory_Code_s__c = 'ZZZ';
update acct; 
}
}
}