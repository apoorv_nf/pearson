/*******************************************************************************************************************
 * Apex Class Name  : PS_AmendmentOpportunityMain
 * Version          : 1.0
 * Created Date     : 17 November 2015
 * Function         : To check integration validations while creating  AmendmentOpportunity 
 * Modification Log :
 *
 * Developer                   Date                    Description
 * ------------------------------------------------------------------------------------------------------------------
 * Karthik.A.S               17/11/2015              To check integration validations while creating AmendmentOpportunity-RD-01594
 * --------------------------------------------------------------------------------------------------------------------
  *******************************************************************************************************************/
public with sharing class PS_AmendmentOpportunityMain {
    @TestVisible ID  oid;

    public pageReference openPage() 
    {
      
       Integer currentYear = Date.Today().Year();
       oid = ApexPages.currentPage().getParameters().get('id');

       opportunity paroppty=[select id,stagename,External_Opportunity_Number__c,External_ID__c,Product_Count__c,Academic_Start_Date__c,
       PS_OppotunityIntegartionfield__c,CreatedById, RecordType.Name from opportunity where id=:oid];
       
       list<opportunity> opp = [select id,StageName from opportunity where External_Opportunity_Number__c =:oid and (StageName != 'Closed' and StageName != 'Lost' and RecordType.Name != 'B2B') limit 1];

       
       system.debug('opptttt'+opp);
       date opptyyear =paroppty.Academic_Start_Date__c;
       String link ='<a href=/'+oid+' >Click here to return</a>';
      
      if(paroppty.stagename =='Lost' || (paroppty.RecordType.Name == 'B2B' && paroppty.stagename == 'Closed Lost')){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.PS_AmendmentLost_stage +link));
           return null;  
       }
          
    /*  else if(paroppty.External_ID__c==''||paroppty.External_ID__c==null){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.PS_IntegrationVetted+link));
           return null;  
      }
      else if(paroppty.PS_OppotunityIntegartionfield__c==false){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.PS_IntegrationFailed +link));
           return null;  
      }*/
      
      else if(Opp.size()>0){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.Ps_AmendmentDuplicateopp +link));
           return null;
      }
      else if(opptyyear != null && opptyyear.year() !=currentYear){
     
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.PS_Opportunitycurrentyear+link));
           return null; 
      }
      else if(paroppty.stagename !='closed' && paroppty.RecordType.Name != 'B2B' &&(paroppty.Product_Count__c <= 0 ||paroppty.CreatedById !=System.Label.DataMigrationUserForAmendment)){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.Ps_AmendmentClose+link));
           return null;  
       }
       pageReference pg = new pageReference('/apex/Create_Amendment_Chain?id='+oid);
       pg.setRedirect(true);
       return pg;
       
    }
}