@istest
public class PS_BatchDeleteDocEmailAttachments_Test {
    
    @testsetup
    public static void testdata(){
        List<Folder> folders = [SELECT id FROM Folder WHERE NAME = 'Temporary Attachment Folder'];
        Id folderId = null;
        folderId = folders.get(0).Id;
        System.debug('@@folderId '+folderId);
        Blob b = null;
        b = Blob.valueof('Test String');
        Document d = new Document();
        d.FolderId = folderId;
        d.Body = b;
        d.Name = 'TestDocument';
        d.ContentType = 'application/pdf';
        d.Type = 'pdf';
        insert d;
    }
    
    /*public static testMethod void testMethod1(){
        System.debug('@@testmethod');
        List<Folder> folders = [SELECT id FROM Folder WHERE NAME = 'Email Attachments'];
        Id folderId = null;
        folderId = folders.get(0).Id;
        
        Database.executeBatch(new PS_BatchDeleteDocFromEmailAttachments(folderId));
        
        
    }*/
    public static testMethod void testScheduler(){
        Test.startTest();
        PS_DeleteDocFromEmailAttachScheduler emailSchduler = new PS_DeleteDocFromEmailAttachScheduler();
        String sch = '0 0 23 * * ?';
        system.schedule('Email Attachment Deletion', sch, emailSchduler); 
        Test.stopTest();
        
    }
}