/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_HashingUtility_Test.cls 
   Description:     Test class for PS_HasingUtility.
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  18/01/2016         1.0            Rahul Boinepally                       Initial Release 
  ---------------------------------------------------------------------------------------------------------------------------------------------------------- */


@isTest
private class Ps_HashingUtility_Test
{
    
    static testmethod void successHash_OptOut()
    {
        
        List<Case> cases = TestDataFactory.createCase(1, 'General');
        for(Case cc: cases)
        {           
            cc.Type = 'Opt Out';
        }
        insert cases;   

        List<Case> caseList = [select Email_Hash__c from Case where ID in :cases];
        for(case cc: caseList)
        {
            
            system.assertEquals(False,string.isBlank(cc.Email_Hash__c),'expect emial hash to be populated - Opt out case');
        }
    
    }

        static testmethod void failureHash_OptOut()
    {
                
        List<Case> cases = TestDataFactory.createCase(1, 'General');
        for(Case cc: cases)
        {           
            cc.Type = 'Cancellation request';
        }
        insert cases;   

        List<Case> caseList = [select Email_Hash__c from Case where ID in :cases];
        for(case cc: caseList)
        {           
            system.assertEquals(True,string.isBlank(cc.Email_Hash__c),'expect email hash to be blank - not Opt Out case');
        }
    
    }

}