/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_ExceptionLogger.cls
   Description:     Utility to create exception logs
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  03/2015         1.0            Tom Carman                       Initial Release 
   
------------------------------------------------------------------------------------------------------------------------------------------------------------ */


public class PS_ExceptionLogger{


    private Map<Exception, String> exceptionToAutonumber = new Map<Exception, String>();
    private List<PS_ExceptionLogger__c> logs = new List<PS_ExceptionLogger__c>();
    private Map<PS_ExceptionLogger__c, Exception> logToExceptionMap = new Map<PS_ExceptionLogger__c, Exception>();
    private Map<Exception, PS_ExceptionLogger__c> exceptionToLogMap = new Map<Exception, PS_ExceptionLogger__c>();



    public void addLog(Exception ex) {

        PS_ExceptionLogger__c log = new PS_ExceptionLogger__c();

        log.Type__c = ex.getTypeName();
        log.Message__c = ex.getMessage();
        log.Stack_Trace__c = ex.getStackTraceString();
        log.Line_Number__c = String.valueOf(ex.getLineNumber());
        log.UserLogin__c = UserInfo.getUserId();

        logs.add(log);
        logToExceptionMap.put(log, ex);


    }
    
    public void addLog(Database.UpsertResult result, String className) {

        PS_ExceptionLogger__c errlogger = new PS_ExceptionLogger__c();
        String ErrMsg='';
        errlogger.ApexClassName__c = className;
        errlogger.UserLogin__c = UserInfo.getUserName(); 
        errlogger.recordid__c = result.getId();
        for(Database.Error err : result.getErrors()) 
            ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage() + ': '+err.getFields(); 
        errlogger.ExceptionMessage__c =  ErrMsg;  
        
        logs.add(errlogger); 

    }

    public void writeLogs() {

        insert logs;

        //requery to get autonumbers
        logs = [SELECT Id, Name FROM PS_ExceptionLogger__c WHERE Id IN :logs];

        //put in map where exception is key
        for(PS_ExceptionLogger__c log : logs) {
            if(logToExceptionMap.get(log) != null)
            exceptionToLogMap.put(logToExceptionMap.get(log), log);
        }


    }


    public String getErrorCode(Exception ex) {
        return exceptionToLogMap.get(ex).Name;
    }


}