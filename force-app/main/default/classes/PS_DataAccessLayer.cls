/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_ServiceAccessLayer.cls 
Description:     Class to hold all SOQL's at Service Layer and prevent them from getting called recurrsively. 
Incident Info:   Lead Conversion Issue
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
25/10/2017         1.0            MAYANK LAL                       Initial Design
17/10/2019                        Rajesh Paleti                    Apttus Decommission changes  
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class PS_DataAccessLayer {
    public static boolean acccontrecurrsionctrlForContactUtil = false;
    public static boolean acccontrecurrsionctrlForOpportunityAccConRole = false;
    public static boolean contractrecurrsionctrlForPSOpptyOpeartion = false;
    public static boolean userrecurrsionctrlForAccRecTaggCtrl = false;
    public static boolean userOwnerrecurrsionctrlForPreventOwner = false;
    public static boolean opptyConRolerecurrsionctrlForSendMailOpptyCont = false;
    public static boolean recordtyperecurrsionctrlForOpptyTrgHndlr = false;
    public static boolean recordtyperecurrsionctrlForEventTrggr = false;
    public static boolean OpptyrecurrsionctrlForOpptyMapAccContRole = false;
    public static boolean OpptyrecurrsionctrlForopportunityD2L = false;
    public static boolean OpptyrecurrsionctrlForSendMailOpptyCont = false;
    public static boolean contractrecurrsionctrlForSendMailOpptyCont = false;
    public static boolean accountrecurrsionctrlForSendMailOpptyCont = false;
    public static boolean profilerecurrsionctrlForAccountOperations = false;
    public static boolean proposalrecurrsionctrlForConTrggHndlr = false;
    public static boolean TerritoryrecurrsionctrlForOpptyCourseTrrHlpr = false; 
    public static boolean eventrecurrsionctrlForSendMailOpptyCont = false; 
    public static boolean proposalrecurrsionctrlForIntegrationReqCtrl = false;
    public static list<AccountContact__c> lstAccContactData1 = new List<AccountContact__c>();
    public static list<AccountContact__c> lstAccContactData2 = new List<AccountContact__c>();
    public static list<Contract> lstContract = new List<Contract>();
    public static list<User> lstUser= new List<User>();
    public static User OwnerUser= new User();
    public static List<OpportunityContactRole> lstOpptyConRole = new List<OpportunityContactRole>();
    public static RecordType recTypevar = new RecordType();
    public static RecordType eventRecTypevar = new RecordType();
    public static List<Opportunity> lstOpportunities = new List<Opportunity>();
    public static List<Opportunity> lstD2LOpportunities = new List<Opportunity>();
    public static List<Opportunity> lstEmailOpptyContOpportunities = new List<Opportunity>();
    public static List<Contract> lstsendEmailToOpptyContract = new List<Contract>();
    public static List<Account> lstAccount = new List<Account>();
    public static Profile profdata = new Profile();
    // Commented for CR-2949 Apttus(Changes) by Rajesh Paleti
    //public static List<Apttus_Proposal__Proposal__c> lstApttusProposal = new List<Apttus_Proposal__Proposal__c>();
    public static List<Territory2> lstTerritory = new List<Territory2>();
    public static List<Event> lstEvent = new List<Event>();
    // Commented for CR-2949 Apttus(Changes) by Rajesh Paleti
    //public static Map<Id,Apttus_Proposal__Proposal__c> mapApptusProposal;
    
    /*Method to be called from Class: ContactUtils and PS_opportunity_MapAccountcontactRole*/
    /*public static list<AccountContact__c> AccountContactData(String className,Set<Id> accountSet,Map<id,id> mapAccOpptyIds){
           
        if(ClassName=='ContactUtils'){
            if(acccontrecurrsionctrlForContactUtil==false && !accountSet.isEmpty()){
                lstAccContactData1 = [SELECT Contact__c, Account__c
                                 FROM AccountContact__c
                                 where Account__c = :accountSet];
                acccontrecurrsionctrlForContactUtil = true;
            }
            return lstAccContactData1;
        }
        else if(ClassName=='PS_opportunity_MapAccountcontactRole') {
            if(acccontrecurrsionctrlForOpportunityAccConRole==false && !mapAccOpptyIds.isEmpty()){
                 lstAccContactData2 = [SELECT Contact__c,Account__c,Primary__c,Financially_Responsible__c 
                  FROM AccountContact__c 
                  where Account__c IN: mapAccOpptyIds.keySet()];
                acccontrecurrsionctrlForOpportunityAccConRole = true;
            }
            return lstAccContactData2;
        } else {
            return null;
        }
    }*/
    
    /*Method to be called from Class: PS_OpportunityOperations*/
    /*public static List<Contract> ContractData(String className, Set<Id> setAccountIds){
        if(ClassName=='PS_OpportunityOperations' && !setAccountIds.isEmpty() && contractrecurrsionctrlForPSOpptyOpeartion==false ){
            lstContract = [select id,Opportunity__c,AccountId 
                               from Contract 
                               where Accountid in: setAccountIds];
                contractrecurrsionctrlForPSOpptyOpeartion = true;
         }
        return lstContract;
    }*/
    
    /*Method to be called from class: PS_Account_RecordTagging_Ctrlr*/ 
    /*public static List<User> UserData(String className, String userId){
        
        if(ClassName=='PS_Account_RecordTagging_Ctrlr' && userId!='' && userrecurrsionctrlForAccRecTaggCtrl==false ){
            lstUser =  [select Market__c , Line_of_Business__c , Geography__c,Region__c 
                        from User 
                        where id =:userId limit 1];
                userrecurrsionctrlForAccRecTaggCtrl = true;
         }
        return lstUser;
    }*/
    
    /*Method to be called from Class: PS_PreventChangingOwner*/
    /*public static User UserOwnerData(String className, String username){
        
        if(ClassName=='PS_PreventChangingOwner' && username!='' && userOwnerrecurrsionctrlForPreventOwner==false ){
            OwnerUser =  [select id 
                        from user 
                        where name=: username AND UserType= 'Standard'];
                userOwnerrecurrsionctrlForPreventOwner = true;
         }
        return OwnerUser;
    }*/
    
    /*Method to be called from Class: sendEmailToOpptyContact*/
    public static List<OpportunityContactRole> OpptyContactRoleData(String className, List<Opportunity> lstOppty){
        
        if(ClassName=='sendEmailToOpptyContact' && !lstOppty.isEmpty()  && opptyConRolerecurrsionctrlForSendMailOpptyCont==false ){
            lstOpptyConRole =  [SELECT ContactId, Contact.Name, Contact.Firstname, Contact.lastname, Contact.Email, Role, OpportunityId  
                                                                FROM OpportunityContactRole 
                                                                WHERE OpportunityId IN :lstOppty
                                                                AND (Role ='Business User' OR Role = 'Primary Sales Contact')]; 
                opptyConRolerecurrsionctrlForSendMailOpptyCont = true;
         }
        return lstOpptyConRole;
    }
    
    /*Method to be called from Class: OpportunityTriggerHandler*/
    /*public static Recordtype RecordTypeData(String className){
        
        if(ClassName=='OpportunityTriggerHandler' && recordtyperecurrsionctrlForOpptyTrgHndlr==false ){
            recTypevar = [SELECT Id FROM RecordType where name = 'D2L'and SobjectType = 'Opportunity']; 
                recordtyperecurrsionctrlForOpptyTrgHndlr = true;
         }
        return recTypevar;
    }*/
    
    /*Method to be called from Trigger: Event*/
    /*public static Recordtype EventRecordTypeData(String className){
        
        if(ClassName=='EventTrigger' && recordtyperecurrsionctrlForEventTrggr==false ){
            eventRecTypevar = [Select id from RecordType where sObjectType = 'Event' AND RecordType.Name='Interview']; 
                recordtyperecurrsionctrlForEventTrggr = true;
         }
        return eventRecTypevar;
    }*/
    
    /*Method to be called from Class: PS_opportunity_MapAccountcontactRole*/
    /*public static List<Opportunity> opportunityData(String className, List<Opportunity> lstOppty){
        
        if(ClassName=='PS_opportunity_MapAccountcontactRole' && !lstOppty.isEmpty()  && OpptyrecurrsionctrlForOpptyMapAccContRole==false ){
            lstOpportunities =  [select Id ,AccountId 
                                 from Opportunity
                                 where id in:lstOppty and Mass_Opportunity_Generation_Indicator__c=false];
                OpptyrecurrsionctrlForOpptyMapAccContRole = true;
         }
        return lstOpportunities;
    }*/
  
    /*Method to be called from Class: PS_opportunityD2L*/
    /*public static List<Opportunity> opportunityD2LData(String className, Set<Id> setOpptyIds){
        
        if(ClassName=='PS_opportunityD2L' && !setOpptyIds.isEmpty()  && OpptyrecurrsionctrlForopportunityD2L==false ){
            lstD2LOpportunities =  [select id,isClosed,stagename,recordtypeId,recordtype.name 
                                    from opportunity 
                                    where id in : setOpptyIds];
                OpptyrecurrsionctrlForopportunityD2L = true;
         }
        return lstD2LOpportunities;
    }*/
    
    /*Method to be called from Class: sendEmailToOpptyContact*/
    public static List<Opportunity> opptysendEmailToOpptyContactData(String className, List<Opportunity> lstOpptys){
        
        if(ClassName=='sendEmailToOpptyContact' && !lstOpptys.isEmpty()  && OpptyrecurrsionctrlForSendMailOpptyCont==false ){
            lstEmailOpptyContOpportunities =   [SELECT Id,Name, AccountId, Primary_Contact__c, Geography__c, Market__c, Type, Student_Registered__c, Enrolment_Status__c, isConvertedFromLead__c, StageName,Line_of_Business__c, RecordType.DeveloperName, Registration_Payment_Reference__c, Preferred_Campus__c 
                                           FROM Opportunity 
                                          WHERE Id IN :lstOpptys];
                OpptyrecurrsionctrlForSendMailOpptyCont = true;
         }
        return lstEmailOpptyContOpportunities;
    }
    
    /*Method to be called from Class: sendEmailToOpptyContact*/
    public static List<Contract> ContractsendEmailToOpptyContactData(String className, List<Id> lstAccountIds){
        if(ClassName=='sendEmailToOpptyContact' && !lstAccountIds.isEmpty() && contractrecurrsionctrlForSendMailOpptyCont==false ){
            lstsendEmailToOpptyContract = [SELECT Id, AccountId, Opportunity__c
                                                    FROM Contract
                                                    WHERE AccountId IN :lstAccountIds];
                contractrecurrsionctrlForSendMailOpptyCont = true;
         }
        return lstsendEmailToOpptyContract;
    }
    
    /*Method to be called from Class: sendEmailToOpptyContact*/
    public static List<Account> AccountData(String className){
        if(ClassName=='sendEmailToOpptyContact' && accountrecurrsionctrlForSendMailOpptyCont==false ){
            lstAccount = [SELECT Id, Pearson_Campus_Picklist__c, Phone, Fax, Bank_Account_Number__c
                                                FROM Account
                                                WHERE Pearson_Account_Number__c = 'Campus'
                                                AND Pearson_Campus__c = true
                                                AND Pearson_Institution__c = 'CTI'];
                accountrecurrsionctrlForSendMailOpptyCont = true;
         }
        return lstAccount;
    }
    
    /*Method to be called from Class: PS_AccountOperations*/
    /*public static Profile ProfileData(String className){
        if(ClassName=='PS_AccountOperations' && profilerecurrsionctrlForAccountOperations==false ){
            profdata =  [select Name from profile where id = :userinfo.getProfileId()];
                profilerecurrsionctrlForAccountOperations = true;
         }
        return profdata;
    }*/
    // Commented for CR-2949 Apttus(Changes) by Rajesh Paleti
    /*Method to be called from Class: ContactTriggerHandler*/
    /*public static List<Apttus_Proposal__Proposal__c> ApttusProposalData(String className, List<Id> lstContactIds){
        if(ClassName=='ContactTriggerHandler' && !lstContactIds.isEmpty() && proposalrecurrsionctrlForConTrggHndlr==false ){
            lstApttusProposal =  [select id,Apttus_Proposal__Primary_Contact__c,Opportunity_International_Student__c,Apptus_Id_Passport__c,Apttus_Proposal__Opportunity__r.IsClosed
                     from Apttus_Proposal__Proposal__c 
                                 where Apttus_Proposal__Primary_Contact__c IN : lstContactIds 
                                 and Apttus_Proposal__Opportunity__c !=null];
                proposalrecurrsionctrlForConTrggHndlr = true;
         }
        return lstApttusProposal;
    }*/
    
    /*Method to be called from Class: PS_Opty_Course_Territoryhelper*/
    /*public static List<Territory2> TerritoryData(String className, List<String> lstTerrCode){
        if(ClassName=='PS_Opty_Course_Territoryhelper' && !lstTerrCode.isEmpty() && TerritoryrecurrsionctrlForOpptyCourseTrrHlpr==false ){
            lstTerritory =  [Select id,Territory_Code__c,ParentTerritory2.Name,ParentTerritory2.ParentTerritory2.Name 
                             from Territory2 
                             where Territory_Code__c IN :lstTerrCode 
                             And (Territory2Model.ActivatedDate!=null and Territory2Model.DeactivatedDate=null)];
                TerritoryrecurrsionctrlForOpptyCourseTrrHlpr = true;
         }
        return lstTerritory;
    }*/
    
    /*Method to be called from Class: sendEmailToOpptyContact*/
    public static List<Event> EventOpptyData(String className, List<Opportunity> lstOppty){
        if(ClassName=='sendEmailToOpptyContact' && !lstOppty.isEmpty() && eventrecurrsionctrlForSendMailOpptyCont==false ){
            lstEvent =   [SELECT Id, WhatId, Sponsor_Attended__c 
                                        FROM Event
                                        WHERE WhatId IN :lstOppty
                                        AND subject__c = 'Lead Interview'
                                        AND Status__c = 'Completed'];
                eventrecurrsionctrlForSendMailOpptyCont = true;
         }
        return lstEvent;
    }
    // Commented for CR-2949 Apttus(Changes) by Rajesh Paleti
    /*Method to be called from Class: PS_INT_IntegrationRequestController */
    /*public static Map<Id,Apttus_Proposal__Proposal__c> ApttusIntegrationProposalData(String className, Map<Id,SObject> mapSobjects){
        if(ClassName=='PS_INT_IntegrationRequestController' && !mapSobjects.isEmpty() && proposalrecurrsionctrlForIntegrationReqCtrl==false ){
            mapApptusProposal =  new Map<Id,Apttus_Proposal__Proposal__c>([SELECT id,Qualification_Campus__c,Apttus_Proposal__Opportunity__c,RecordType.Name 
                          FROM Apttus_Proposal__Proposal__c 
                          WHERE Apttus_Proposal__Opportunity__c =: mapSobjects.keyset() 
                          and Apttus_Proposal__Primary__c =:true]) ;
                proposalrecurrsionctrlForIntegrationReqCtrl = true;
         }
        return mapApptusProposal;
    }*/
}