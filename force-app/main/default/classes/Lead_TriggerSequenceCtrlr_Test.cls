/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            Lead_TriggerSequenceCtrlr_Test.cls 
   Description:     Test Class to cover code coverage Lead_TriggerSequenceCtrlr
   Date:            09/18/2017
   Author:          Vishista Madati
------------------------------------------------------------------------------------------------------------------------------------------------------------- */


@isTest(SeeAllData=true)
public class Lead_TriggerSequenceCtrlr_Test 
{
    
    static void methodName1()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);
        lstWithTestUser[0].market__c='Global';
        insert lstWithTestUser;
        System.runAs(lstWithTestUser[0]) 
            {
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c    =true);
        insert byp; 
        List<Lead> lstLeadRecords = new List<Lead>();
        Id accountId;
        General_One_CRM_Settings__c genOneCrRM = new General_One_CRM_Settings__c();
        genOneCrRM.Name = 'Lead Automation Batch Limit';
        genOneCrRM.Category__c = 'ReopenDays';
        genOneCrRM.Description__c = 'ReopenDays';
        genOneCrRM.Value__c = '0';     
        insert genOneCrRM;
        RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
        RecordType Leadrec= [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Lead' AND Name = 'D2L'];
        Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
        acc.ShippingStreet = 'TestStreet';
        acc.ShippingCity = 'Vns';
        acc.ShippingState = 'Delhi';
        acc.ShippingPostalCode = '234543';
        acc.ShippingCountry = 'India';
        acc.recordtypeId=OrgAcc.Id ;
        acc.Academic_Achievement__c='High';
        acc.Market2__c = 'Global';
        acc.Organisation_Type__c='School';
        acc.IsCreatedFromLead__c = true; 
        insert acc;
        
        lstLeadRecords = TestDataFactory.createLead(1,'B2B');
        for(Lead lstLead : lstLeadRecords)
        {   lstLead.recordtypeId=Leadrec.id;
            lstLead.Channel__c ='Direct';
            lstLead.Organisation_Type1__c = 'Large Corporate';
            lstLead.Role__c = 'Employee';
            lstLead.Enquiry_Type__c = 'Product';
            lstLead.PostalCode = '345678';
            lstLead.State = 'Bihar';
            lstLead.Street = 'TestStreet';
            lstLead.Institution_Organisation__c =acc.Id;
            lstLead.Market__c = 'Global'; 
            lstLead.Email ='leadtestmail12343@gmail.com';
            
        }
        
            
                try{
                    lstLeadRecords[0].OwnerId = lstWithTestUser[1].Id;
                    insert lstLeadRecords;
                                         
                }
                catch(Exception e){
                    System.debug('Exceptoin'+e.getMessage());
                }
            }
          
    }
    static testMethod void methodName2()
    {
      
        List<User> users = [Select Id from User where Name = :Label.Deployment_User];
        users[0].Market__c = 'US';
        update users;
        
       // Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=users[0].id,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true);
        //insert byp;
        Profile ProfileNameadmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        List<User> u1 = TestDataFactory.createUser(ProfileNameadmin.Id,1);
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu12'; 
            u1[0].Email ='Opptyproposaltest1122@pearson.com'; 
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='Opptyproposaltest122'; 
            u1[0].LanguageLocaleKey ='en_US'; 
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = ProfileNameadmin.Id; 
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName ='standarduserQuoteCreation1@testorg.com';
            u1[0].Market__c ='US';
            u1[0].Business_Unit__c = 'Higher Ed';
            u1[0].line_of_Business__c = 'Higher Ed';
            u1[0].Product_Business_Unit__c = 'Higher Ed';
            insert u1;
        }
        
       // user usr = [select id from user where id =: Userinfo.getUserId()];
        System.runAs(u1[0]){
            Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u1[0].id,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true,Disable_Triggers__c=true);
            insert byp;
            test.startTest();
                                   
         //System.runAs(usr){
      
            List<Lead> lstLeadRecords = new List<Lead>(); 
            List<Lead> lstLeadUpdRecords = new List<Lead>(); 
            List<ID> lstLeadID = new List<ID>(); 
            List<Lead> lsLead = new List<Lead>(); 
            RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
            //RecordType Leadrectype= [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Lead' AND Name = 'D2L'];
            Account acc = new Account (Name='test001',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
            acc.ShippingStreet = 'TestStreet';
            acc.ShippingCity = 'Vns';
            acc.ShippingState = 'Delhi';
            acc.ShippingPostalCode = '234543';
            acc.ShippingCountry = 'India';
            acc.recordtypeId=OrgAcc.Id ;
            acc.Academic_Achievement__c='High';
            acc.Market2__c = 'US';
            acc.Organisation_Type__c='School';
            acc.IsCreatedFromLead__c = true; 
            acc.BillingStreet ='Teststeetb';
            acc.BillingCity='hyd';
            acc.BillingStateCode='';
            acc.BillingPostalCode='352456';
            insert acc;
            //System.runAs(lstWithTestUser[0]){
            //lstLeadRecords = TestDataFactory.createLead(1,'B2B');
            //}
            User usr1 = [select id from user where Profile.Name = 'System Administrator' limit 1];
            RecordType rtye = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Lead' AND Name ='New Account Request' LIMIT 1]; 
            lead leadRec = new lead(LastName='Abraham', FirstName='John', RecordTypeId =  rtye.Id, Status='Open', Market__c ='US', Country_of_Origin__c = 'India',
                                   Channel__c ='Direct',Organisation_Type1__c = 'Large Corporate',Role__c = 'Employee',Enquiry_Type__c = 'Product',PostalCode = '345678',
                                   Country = 'India',State = 'Bihar',Street = 'TestStreet',Company = 'test',Email ='leadtestmail123445@gmail.com',OwnerId=usr1.Id,
                                   Run_assignment_rules__c =True,Institution_Organisation__c =null);
        
            //insert leadRec;
            lsLead.add(leadRec);
            Database.insert(lsLead);      
            System.debug('After Insert:'+lsLead);          
            
                   
         test.stopTest();
            //lsLead[0].Email = 'ddddd@gmail.com';
           // lsLead[0].OwnerId = Userinfo.getUserId();
            //update lsLead;
         
            try{
                   
                    System.debug('After Update:'+leadRec);
                    //lstLeadRecords[0].OwnerId = u1[1].Id;
                    //insert lstLeadRecords;
                                         
                }
                catch(Exception e){
                    System.debug('Exceptoin'+e.getMessage());
                }
        }
    }
    
    
     public static testMethod void methodName3()
    {
      
        List<User> users = [Select Id from User where Name = :Label.Deployment_User];
        users[0].Market__c = 'US';
        update users;
        
       // Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=users[0].id,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true);
        //insert byp;
        Profile ProfileNameadmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        List<User> u1 = TestDataFactory.createUser(ProfileNameadmin.Id,1);
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu12'; 
            u1[0].Email ='Opptyproposaltest1122@pearson.com'; 
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='Opptyproposaltest122'; 
            u1[0].LanguageLocaleKey ='en_US'; 
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = ProfileNameadmin.Id; 
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName ='standarduserQuoteCreation1@testorg.com';
            u1[0].Market__c ='US';
            u1[0].Business_Unit__c = 'Higher Ed';
            u1[0].line_of_Business__c = 'Higher Ed';
            u1[0].Product_Business_Unit__c = 'Higher Ed';
            insert u1;
        }
        
       // user usr = [select id from user where id =: Userinfo.getUserId()];
        System.runAs(u1[0]){
            Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u1[0].id,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true,Disable_Triggers__c=true);
            insert byp;
            test.startTest();
                                   
         //System.runAs(usr){
      
            List<Lead> lstLeadRecords = new List<Lead>(); 
            List<Lead> lstLeadUpdRecords = new List<Lead>(); 
            List<ID> lstLeadID = new List<ID>(); 
            List<Lead> lsLead = new List<Lead>(); 
            RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
            Account acc = new Account (Name='test001',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
            acc.ShippingStreet = 'TestStreet';
            acc.ShippingCity = 'Vns';
            acc.ShippingState = 'Delhi';
            acc.ShippingPostalCode = '234543';
            acc.ShippingCountry = 'India';
            acc.recordtypeId=OrgAcc.Id ;
            acc.Academic_Achievement__c='High';
            acc.Market2__c = 'US';
            acc.Organisation_Type__c='School';
            acc.IsCreatedFromLead__c = true; 
            acc.BillingStreet ='Teststeetb';
            acc.BillingCity='hyd';
            acc.BillingStateCode='';
            acc.BillingPostalCode='352456';
            insert acc;
            //System.runAs(lstWithTestUser[0]){
            //lstLeadRecords = TestDataFactory.createLead(1,'B2B');
            //}
            
            List<Contact> lstContact = TestDataFactory.createContact(1);
            insert lstContact;
            
            User usr1 = [select id from user where Profile.Name = 'System Administrator' limit 1];
            RecordType rtye = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Lead' AND Name ='B2B' LIMIT 1]; 
            lead leadRec = new lead(LastName='Abraham', FirstName='John', RecordTypeId =  rtye.Id, Status='Open', Market__c ='US', Country_of_Origin__c = 'India',
                                   Channel__c ='Direct',Organisation_Type1__c = 'Large Corporate',Role__c = 'Employee',Enquiry_Type__c = 'Product',PostalCode = '345678',
                                   Country = 'India',State = 'Bihar',Street = 'TestStreet',Company = 'test',Email =lstContact[0].Email ,OwnerId=usr1.Id,
                                   Run_assignment_rules__c =True);
        
            insert leadRec;
            lsLead.add(leadRec);
            
            leadRec.Email = 'ddddd@gmail.com';
            leadRec.OwnerId = usr1.Id;          
            
            
            Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
            mapWithLead.put(leadRec.Id,leadRec); 
            PS_Lead_RecordTagging_Ctrlr conts = new PS_Lead_RecordTagging_Ctrlr();  
            conts.updateDateforQualifiedLead(lstLeadRecords,mapWithLead);
        
                   
         test.stopTest();
            try{
                  
                    update leadRec;
                    lstLeadRecords[0].OwnerId = u1[1].Id;
                    insert lstLeadRecords;
                                         
                }
                catch(Exception e){
                    System.debug('Exceptoin'+e.getMessage());
                }
        }
    }
    
    public static testMethod void methodName4()
    {
      
        List<User> users = [Select Id from User where Name = :Label.Deployment_User];
        users[0].Market__c = 'US';
        update users;
        
       // Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=users[0].id,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true);
        //insert byp;
        Profile ProfileNameadmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        List<User> u1 = TestDataFactory.createUser(ProfileNameadmin.Id,1);
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu12'; 
            u1[0].Email ='Opptyproposaltest1122@pearson.com'; 
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='Opptyproposaltest122'; 
            u1[0].LanguageLocaleKey ='en_US'; 
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = ProfileNameadmin.Id; 
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName ='standarduserQuoteCreation1@testorg.com';
            u1[0].Market__c ='US';
            u1[0].Business_Unit__c = 'Higher Ed';
            u1[0].line_of_Business__c = 'Higher Ed';
            u1[0].Product_Business_Unit__c = 'Higher Ed';
            insert u1;
        }
        
       // user usr = [select id from user where id =: Userinfo.getUserId()];
        System.runAs(u1[0]){
            Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u1[0].id,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true,Disable_Triggers__c=true);
            insert byp;
            test.startTest();
                                   
         //System.runAs(usr){
      
            List<Lead> lstLeadRecords = new List<Lead>(); 
            List<Lead> lstLeadUpdRecords = new List<Lead>(); 
            List<ID> lstLeadID = new List<ID>(); 
            List<Lead> lsLead = new List<Lead>(); 
            RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
            Account acc = new Account (Name='test001',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
            acc.ShippingStreet = 'TestStreet';
            acc.ShippingCity = 'Vns';
            acc.ShippingState = 'Delhi';
            acc.ShippingPostalCode = '234543';
            acc.ShippingCountry = 'India';
            acc.recordtypeId=OrgAcc.Id ;
            acc.Academic_Achievement__c='High';
            acc.Market2__c = 'US';
            acc.Organisation_Type__c='School';
            acc.IsCreatedFromLead__c = true; 
            acc.BillingStreet ='Teststeetb';
            acc.BillingCity='hyd';
            acc.BillingStateCode='';
            acc.BillingPostalCode='352456';
            insert acc;
            //System.runAs(lstWithTestUser[0]){
            //lstLeadRecords = TestDataFactory.createLead(1,'B2B');
            //}
            User usr1 = [select id from user where Profile.Name = 'System Administrator' limit 1];
            RecordType rtye = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Lead' AND Name ='D2L' LIMIT 1]; 
            lead leadRec = new lead(LastName='Abraham', FirstName='John', RecordTypeId =  rtye.Id, Status='Open', Market__c ='US', Country_of_Origin__c = 'India',
                                   Channel__c ='Direct',Organisation_Type1__c = 'Large Corporate',Role__c = 'Employee',Enquiry_Type__c = 'Product',PostalCode = '345678',
                                   Country = 'India',State = 'Bihar',Street = 'TestStreet',Company = 'test',Email ='leadtestmail123445@gmail.com',OwnerId=usr1.Id,
                                   Run_assignment_rules__c =True);
        
            insert leadRec;
            lsLead.add(leadRec);
            
            leadRec.Email = 'ddddd@gmail.com';
            leadRec.OwnerId = usr1.Id;
            //update leadRec;
            
            
            Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
            mapWithLead.put(leadRec.Id,leadRec); 
            PS_Lead_RecordTagging_Ctrlr conts = new PS_Lead_RecordTagging_Ctrlr();  
            conts.updateDateforQualifiedLead(lstLeadRecords,mapWithLead);
        
                   
         test.stopTest();
            try{
                    update leadRec;
                    lstLeadRecords[0].OwnerId = u1[1].Id;
                    insert lstLeadRecords;
                                         
                }
                catch(Exception e){
                    System.debug('Exceptoin'+e.getMessage());
                }
        }
    }
    
}