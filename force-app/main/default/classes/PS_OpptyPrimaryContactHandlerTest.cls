/************************************************************************************************************
* Apex Class Name : PS_OpptyPrimaryContactHandlerTest
* Version             : 1.0 
* Created Date        : 6/6/2017 
* Author              : MAYANK LAL  
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_OpptyPrimaryContactHandlerTest {

    static testmethod void OppwithSingleOppConRolePrimaryUncheck(){
       
        //Create Account,Contact and Opportunity
        List<Account> lstacc = TestDataFactory.createAccount(1, 'Learner');
        insert lstacc;
		
        List<Contact> lstcon = TestDataFactory.createContacts(1);
        lstcon[0].accountid = lstacc[0].id;
        insert lstcon;
	        
        List<Opportunity> lstOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        insert lstOppty;
        
        
        //Create OpportunityContactRole 
        OpportunityContactRole opptyConRole = new OpportunityContactRole();
        opptyConRole.OpportunityId = lstOppty[0].id;
        opptyConRole.contactId = lstcon[0].id;
        opptyConRole.Role  = 'Decision Maker';
        opptyConRole.IsPrimary  = false;
        insert opptyConRole;
       
        //opptyConRole.ContactId = lstOppty[0].
        lstOppty[0].NextStep = 'Test'; 
      test.startTest();
      update lstOppty;
       PS_OpptyPrimaryContactHandler.updatePrimary(lstOppty);  
       test.stopTest(); 
       
    }
    
    static testmethod void OppwithSingleOppConRolePrimaryCheck(){
      
        //Create Account,Contact and Opportunity
        List<Account> lstacc = TestDataFactory.createAccount(1, 'Learner');
        insert lstacc;
		
        List<Contact> lstcon = TestDataFactory.createContacts(1);
        lstcon[0].accountid = lstacc[0].id;
        insert lstcon;
	        
        List<Opportunity> lstOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        insert lstOppty;
        
        
        //Create OpportunityContactRole 
        OpportunityContactRole opptyConRole = new OpportunityContactRole();
        opptyConRole.OpportunityId = lstOppty[0].id;
        opptyConRole.contactId = lstcon[0].id;
        opptyConRole.Role  = 'Decision Maker';
        opptyConRole.IsPrimary  = true;
        insert opptyConRole;
        //opptyConRole.ContactId = lstOppty[0].
        lstOppty[0].NextStep = 'Test';
		test.startTest();
        update lstOppty;
       PS_OpptyPrimaryContactHandler.updatePrimary(lstOppty); 
        test.stopTest(); 
    }
    
    static testmethod void OppwithMultipleOppConRolePrimaryUnCheck(){
       
        //Create Account,Contact and Opportunity
        List<Account> lstacc = TestDataFactory.createAccount(1, 'Learner');
        insert lstacc;
		
        List<Contact> lstcon = TestDataFactory.createContacts(1);
        lstcon[0].accountid = lstacc[0].id;
        insert lstcon;
	        
        List<Opportunity> lstOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        insert lstOppty;
        
        
        List<OpportunityContactRole> lstopptyConRole = new list<OpportunityContactRole>();
        //Create OpportunityContactRole 
        for(Integer i=0;i<=2;i++){
            OpportunityContactRole opptyConRole = new OpportunityContactRole();
            opptyConRole.OpportunityId = lstOppty[0].id;
            opptyConRole.contactId = lstcon[0].id;
            //opptyConRole.Role  = 'Decision Maker';
            opptyConRole.IsPrimary  = false;
            lstopptyConRole.add(opptyConRole);
        }
        lstopptyConRole[0].Role = 'Decision Maker';
        lstopptyConRole[1].Role = 'Influencer';
        lstopptyConRole[2].Role = 'Evaluator';
        insert lstopptyConRole;
        
        //Creating Data for Custom Setting
        Primary_Contact_Field_Hierarchy__c priContFieldHier = new Primary_Contact_Field_Hierarchy__c();
        priContFieldHier.name = 'test';
        priContFieldHier.Contact_Role__c = 'Decision Maker';
        priContFieldHier.IsActive__c = true;
        priContFieldHier.Object_API_Name__c = 'Opportunity';
        priContFieldHier.Role_level_Number__c = 1;
        insert priContFieldHier;
        
        
        //opptyConRole.ContactId = lstOppty[0].
        lstOppty[0].NextStep = 'Test';
        test.startTest();
        update lstOppty;
       PS_OpptyPrimaryContactHandler.updatePrimary(lstOppty); 
        test.stopTest();  
    }
    static testmethod void OppwithMultipleOppConRolePrimaryCheck(){
       
        //Create Account,Contact and Opportunity
        List<Account> lstacc = TestDataFactory.createAccount(1, 'Learner');
        insert lstacc;
		
        List<Contact> lstcon = TestDataFactory.createContacts(1);
        lstcon[0].accountid = lstacc[0].id;
        insert lstcon;
	        
        List<Opportunity> lstOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        insert lstOppty;
        
        
        List<OpportunityContactRole> lstopptyConRole = new list<OpportunityContactRole>();
        //Create OpportunityContactRole 
        for(Integer i=0;i<=2;i++){
            OpportunityContactRole opptyConRole = new OpportunityContactRole();
            opptyConRole.OpportunityId = lstOppty[0].id;
            opptyConRole.contactId = lstcon[0].id;
            //opptyConRole.Role  = 'Decision Maker';
            opptyConRole.IsPrimary  = false;
            lstopptyConRole.add(opptyConRole);
        }
        lstopptyConRole[0].Role = 'Decision Maker';
        lstopptyConRole[1].Role = 'Influencer';
        lstopptyConRole[2].Role = 'Evaluator';
        lstopptyConRole[2].isPrimary = true;
        insert lstopptyConRole;
        
        //Creating Data for Custom Setting
        Primary_Contact_Field_Hierarchy__c priContFieldHier = new Primary_Contact_Field_Hierarchy__c();
        priContFieldHier.name = 'test';
        priContFieldHier.Contact_Role__c = 'Decision Maker';
        priContFieldHier.IsActive__c = true;
        priContFieldHier.Object_API_Name__c = 'Opportunity';
        priContFieldHier.Role_level_Number__c = 1;
        insert priContFieldHier;
        
        
        //opptyConRole.ContactId = lstOppty[0].
        lstOppty[0].NextStep = 'Test';
        test.startTest();
        update lstOppty;
       PS_OpptyPrimaryContactHandler.updatePrimary(lstOppty); 
      test.stopTest();   
    }
}