/**
* @description : Test class for EinsteinBotCheckOpenCases.
* @Author      : Apoorv (Neuraflash)  
* @CreatedDate : Aug 30, 2019
*/
@isTest
private class EinsteinBotCheckOpenCasesTest {
    @TestSetup
	static void setupData(){
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {

			String firstName = 'John';
            String lastName = 'Doe';

            Account acc = new Account();
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_Creation_Request').getRecordTypeId();
            acc.Name = firstName + ' ' + lastName;

            insert acc;
            
            Contact con = new Contact();
            con.FirstName = firstName;
            con.LastName = lastName;
            con.Email = 'john@salesforce.com';
            con.Phone = '+12345678901';
            con.AccountId = acc.Id;
            insert con;
        
			// Create a queue
			Group testGroup = new Group(Name='NAUS HETS SMS ACR', Type='Queue');
			insert testGroup;

			// Assign user to queue
			GroupMember testGrpMember = new GroupMember(GroupId = testGroup.Id, UserOrGroupId = UserInfo.getUserId());
			insert testGrpMember;

            Case newCase = new Case();
            newCase.Subject = 'SMS Chat Case';
            newCase.ContactId = con.Id;
            newCase.Origin = 'SMS';
            newCase.Status = 'In Progress';
            newCase.Case_Resolution_Category__c = 'Training';
            newCase.PS_Business_Vertical__c = 'Higher Education';
            insert newCase;

			MessagingChannel msgChannnel = new MessagingChannel(MasterLabel = '+11234567890', IsActive = TRUE, 
																MessageType = 'Text', TargetQueueId = testGroup.Id,
																MessagingPlatformKey = '+11234567890', DeveloperName = 'Text_11234567890'
																);
			insert msgChannnel;

			// Create a test MessagingEndUser
			MessagingEndUser msgUser = new MessagingEndUser(MessagingPlatformKey = '+12345678901', Name = '+12345678901',
                                                            IsOptedOut = FALSE, MessagingChannelId = msgChannnel.Id,
                                                            MessageType = 'Text',AccountId = acc.Id, ContactId = con.Id);
           
			insert msgUser;

            MessagingSession msgSessionOne = new MessagingSession(MessagingEndUserId = msgUser.Id, MessagingChannelId = msgChannnel.Id, 
                                                                  Status = 'Ended', CaseId = newCase.Id);
            insert msgSessionOne;

		}
    }
    
    @IsTest
    static void testCheckOpenCasesNegative(){
       
        Id msgUserId = [SELECT Id FROM MessagingEndUser WHERE MessagingPlatformKey = '+12345678901' LIMIT 1].Id;
        String msgSessionId = [SELECT Id FROM MessagingSession LIMIT 1].Id;
        
        Test.startTest();
            List<Boolean> openCasesBoolList = EinsteinBotCheckOpenCases.checkOpenCases(new List<String>{msgSessionId});
        Test.stopTest();
        System.assertEquals(FALSE, openCasesBoolList[0]); 
    }

    @IsTest
    static void testCheckOpenCases(){
       
        Id msgChannnelId = [SELECT Id FROM MessagingChannel LIMIT 1].Id;
        Id msgUserId = [SELECT Id FROM MessagingEndUser WHERE MessagingPlatformKey = '+12345678901' LIMIT 1].Id;
        
        Test.startTest();
            MessagingSession msgSessionTwo = new MessagingSession(MessagingEndUserId = msgUserId, MessagingChannelId = msgChannnelId, 
                                                                  Status = 'Active');
            insert msgSessionTwo;

            List<Boolean> openCasesBoolList = EinsteinBotCheckOpenCases.checkOpenCases(new List<String>{msgSessionTwo.Id});
        Test.stopTest();

        System.assertEquals(TRUE, openCasesBoolList[0]); 
    }
    
}