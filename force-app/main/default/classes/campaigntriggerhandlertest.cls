/************************************************************************************************************
* Apex Class Name  : campaigntriggerhandlertest.cls
* Version          : 1.0 
* Created Date     : 23 MAY 2018
* Function         : Test class for campaigntriggerhandler class
* Modification Log :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Darshan MS                  23/05/2018              CR-01459-Req-13, CR-01459-Req-20
************************************************************************************************************/

@isTest
public class campaigntriggerhandlertest 
{ 
     //Constat variables hold record type ID
     // Commented and Modified as part of US-HE:SP - Global Record Type - Renamed. By Navaneeth - Start
     //public static final String STR_GLOBAL_CAMPAIGN = 'Global Campaign';
     public static final String STR_GLOBAL_CAMPAIGN = Label.CampaignGlobalRecordType;
     // Commented and Modified as part of US-HE:SP - Global Record Type - Renamed. By Navaneeth - End
     public static final String STR_SAMPLING_CAMPAIGN = 'Sampling Campaign';
     public static final String STR_ERPI =  'ERPI';
     public static final ID GLOBAL_CAMPAIGN_RECORDTYPE_ID = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get(STR_GLOBAL_CAMPAIGN).getRecordTypeId();
     public static final ID SAMPLING_CAMPAIGN_RECORDTYPE_ID = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get(STR_SAMPLING_CAMPAIGN).getRecordTypeId();
    
     public static testmethod void handlermethod()
       
       {
          Profile P = [  SELECT Id 
                         FROM Profile 
                         WHERE Name='System Administrator'];
          User u1 = new User( Alias = 'newUser', Email='newuser@testorg.com', Business_unit__c = 'ERPI',
                              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US', ProfileId = P.Id,
                              TimeZoneSidKey='America/Los_Angeles', UserName='newuser05252018@testorg.com');
      
         insert u1;
         System.runAs(u1)
           
           {
             List<campaign> listcamp=new list<campaign>();
             Campaign campobj = new Campaign(Name='Test',Campaign_Objectives__c='test1',RecordTypeId=GLOBAL_CAMPAIGN_RECORDTYPE_ID ,Type='Mass Campaign',StartDate = system.today(),EndDate = system.today(),Line_of_Business__c = 'Schools');
             listcamp.add(campobj);
    
             Campaign campobj1 = new Campaign(Name='Test1',Campaign_Objectives__c='test2',RecordTypeId=SAMPLING_CAMPAIGN_RECORDTYPE_ID ,Type='Mass Campaign',StartDate = system.today(),EndDate = system.today(),Line_of_Business__c = 'Schools');
             listcamp.add(campobj1);
    
             insert listcamp;

             campaigntriggerhandler.OnBeforeInsertupdate(listcamp);
    
          } 
    }
}