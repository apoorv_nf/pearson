/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            ComplaintFeedbackSupport.cls
Description:     ComplaintFeedbackSupport Class
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
7/07/2016         1.0            Supriyam                 Added method runServiceIntOnce for R4 servicenow Integration
22/8/2016         1.1            Payal                    Updated documentation, removed system debug and unused code.
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class ComplaintFeedbackSupport {
    /**
    * Description : Method to get logged in user details
    * @Profile value to check the user profile
    * @return String
    * @throws NA
    **/
     @AuraEnabled
    public static User getCurrentUser() 
    {
        String message;
        User toReturn ;
        User newUser  = new User();
        
        String Profile = 'Pearson UK Support Profile';
        try{
           toReturn = [SELECT Id, Name, FirstName, MobilePhone, LastName, Email, City, Country, State , UserName , Profile.Name ,
           Phone,accountid FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
           //Phone,accountid,account.Pearson_Customer_Number__c  FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];  For Apttus changes
          if( toReturn.Profile.Name != Profile )
           {     
               return toReturn;  
           }
          else{
             return newUser;
          }
             
        }
        Catch(System.Exception e)
        {
            message=e.getMessage();
            return null;
        }
    }
     /**
    * Description : Method to create contact and cases
    * @param Id, filename,and string values to map to case object and feeditem object
    * @return String
    * @throws NA
    **/
    @AuraEnabled
    public Static String getValues(String fileId,Id parentID,String fileName, String base64Data, String contentType,String type,String Role,String Title,String firstName,String lastName,String CenterName,String CenterNumber,String CenterPostCode,String AssessmentNumber,String Email,String PhoneNumber,String Message) 
    {

        //String roleName;
        //String roleDetail;
       
        Contact_Roles_UK__c roleList = [select Contact_Role_Mapp__c,Role_Details_mapp__c from Contact_Roles_UK__c where Role__c =:Role];

        Contact con=new Contact();
        String Success_Message='';
        String errorMsg;
        String recordType_case = System.Label.CaseComplaintRecordType;
        String recordType_contact = System.Label.ContactGlobalRecordTypeName;
        List<RecordType> RecordTypeID_contact = [Select Id FROM RecordType WHERE SobjectType = 'Contact' and DeveloperName =: recordType_contact LIMIT 1] ;
        List<RecordType> RecordTypeID_case = [Select Id FROM RecordType WHERE SobjectType = 'Case' and DeveloperName =: recordType_case LIMIT 1] ;
        String query = 'SELECT Id FROM Contact WHERE Email =' + '\'' + Email + '\'' + '';
        LIST <Contact> conn = Database.query(query);
        Case Obj=new Case();
        try{
            if(conn.isEmpty())
            {
                con.LastName=lastName;
                con.FirstName=firstName; 
                con.Role__c=roleList.Contact_Role_Mapp__c;
                con.Role_Detail__c=roleList.Role_Details_mapp__c;
                con.Email=Email;
                con.Title=Title;
                con.Phone=PhoneNumber;
                con.Market__c=System.label.PS_UKMarket;
                RecordType RecordTypeID_contactobj=RecordTypeID_contact.get(0);
                con.RecordTypeId=RecordTypeID_contactobj.Id;

                //Changes as per Defect 4757- to bypass Duplicate Checker
                Database.DMLOptions dml = new Database.DMLOptions(); 
                dml.DuplicateRuleHeader.AllowSave = true;
                Database.insert(con, dml);

                Obj.ContactId=con.Id;
            }
            else{
                Contact existingcontactobj=conn.get(0);
                Obj.ContactId=existingcontactobj.Id;
            }
            Obj.Account_Primary_Contact_Email__c = email;
            RecordType RecordTypeID_caseobj=RecordTypeID_case.get(0);
            Obj.RecordTypeId = RecordTypeID_caseobj.Id;
            obj.Type=type;
            obj.Subject=type;
            obj.PS_Business_Vertical__c='Qualifications';
            obj.Category__c='Complaint';
            obj.SuppliedCompany=CenterName;
            obj.Status='new';
            obj.Origin='web';
            obj.Market__c=System.label.PS_UKMarket;
            obj.Contact_Email__c=Email;
            obj.SuppliedEmail=Email;
            obj.Priority='Medium';
            obj.SuppliedPhone=PhoneNumber;
            obj.Contact_Phone__c=PhoneNumber;
            obj.Description='-> '+ Role + '\n' +'-> '+ CenterName + '\n' +'-> '+ CenterPostCode + '\n' + '-> '+ AssessmentNumber + '\n' + '-> ' + Message;
            insert obj;
            Case Casenum = [select CaseNumber from Case where id =:Obj.Id Limit 1];
            String CaseNumber = Casenum.CaseNumber;
            parentId = Obj.Id;
            if(fileName != '' && fileName != null)
            {
               
            saveTheChunk(parentId, fileName,base64Data, contentType, fileId);    
               // saveTheFile(parentID,fileName, base64Data, contentType);
            }
            Success_Message=System.label.PS_SelfService_SuccessMessage+' '+CaseNumber+'.';
            }
        catch(dmlexception e) {
            errorMsg = e.getMessage();
         
            if(!String.ISBLANK(errorMsg)){
                Success_Message = System.label.PS_SelfService_ErrorMessage;
            }
        }
        return Success_Message;
    }

    /**
    * Description : Method to upload file in chatterfeed
    * @param Id parentID ,String fileName, String base64Data, String contentType
    * @return Id
    * @throws NA
    **/
    @AuraEnabled
    public static Id saveTheFile( Id parentID ,String fileName, String base64Data, String contentType) { 
        Attachment a = new Attachment();
        try{
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            a.parentId = parentID;
            a.Body = EncodingUtil.base64Decode(base64Data);
            a.Name = fileName;
            a.ContentType = contentType;
            insert a;
         }catch(dmlexception e) {
        
         }
         return a.id;
    } 
    
    @AuraEnabled
    public static Id saveTheChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) { 
        
        
        if (fileId == '' || fileId == null) {
            fileId = saveTheFile(parentId, fileName, base64Data, contentType);
        } else {
            appendToFile(fileId, base64Data);
        }
        
        return Id.valueOf(fileId);
    }
    
    @TestVisible private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Attachment a = [
            SELECT Id, Body
            FROM Attachment
            WHERE Id = :fileId
        ];
        
        String existingBody = EncodingUtil.base64Encode(a.Body);
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data); 
        
        update a;
    }

    /**
    * Description : Method to fetch all the request type from the Custom setting - Request_Type__c
    * @param NA
    * @return List<Request_Type__c>
    * @throws NA
    **/
    @AuraEnabled
    public static List<Request_Type__c> getRequestType() 
    {        
        List<Request_Type__c> requestTypeList = Request_Type__c.getall().values();
        requestTypeList.sort();
        return requestTypeList;
    }
    /**
    * Description : Method to fetch all the role mapping from the Custom setting - Contact_Roles_UK__c
    * @param NA
    * @return List<Contact_Roles_UK__c>
    * @throws NA
    **/
    @AuraEnabled
    public static List<Contact_Roles_UK__c> getRoleMapping() 
    {        
        List<Contact_Roles_UK__c> roleMappingList = Contact_Roles_UK__c.getall().values();
        roleMappingList.sort();
        return roleMappingList;
    }

}