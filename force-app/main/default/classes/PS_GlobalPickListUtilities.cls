/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_CreatePIUWithCoursesController
   Description:     Allows user to link course with product
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   13 oct 2015      1.0           Accenture NDC                                         Created
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   09 sep 2019       --            Cognizant           [Changes]September CI CR-02850: get Channel detail picklist values from Asset object using describe method
---------------------------------------------------------------------------------------------------------------------------------------------------------- */


public class PS_GlobalPickListUtilities
{
    //get Usage picklist values from  Asset object using describe method
    public static List<SelectOption> assetUsage()
    {
        List<SelectOption> usagePicklistValues = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Asset.Usage__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        usagePicklistValues  = selectOptionUtility(ple);    
        return usagePicklistValues;
    }
    //get Digital Usage picklist values from Asset object using describe method
    public static List<SelectOption> assetDigitalUsage()
    {
        List<SelectOption> digitalUsagePicklistValues = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Asset.Digital_Usage__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        digitalUsagePicklistValues = selectOptionUtility(ple);     
        return digitalUsagePicklistValues;
    }
    //get Status picklist values from Asset object using describe method
    public static List<SelectOption> assetStatus()
    {
        List<SelectOption> statusPicklistValues = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Asset.Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        statusPicklistValues = selectOptionUtility(ple);
        return statusPicklistValues;
    }
    //get Mode Of Delivery picklist values from Asset object using describe method
    public static List<SelectOption> assetModeOfDelivery()
    {
        List<SelectOption> modeofDeliveryPicklistValues = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Asset.Mode_of_Delivery__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        modeofDeliveryPicklistValues = selectOptionUtility(ple);
        return modeofDeliveryPicklistValues;
    }
    //get Third Party LMS picklist values from Asset object using describe method
    public static List<SelectOption> assetThirdPartyLMS()
    {
        List<SelectOption> thirdPartyLMSPicklistValues = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Asset.Third_Party_LMS__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        thirdPartyLMSPicklistValues = selectOptionUtility(ple);
        return thirdPartyLMSPicklistValues;
    }
    //get Channel picklist values from Asset object using describe method
    public static List<SelectOption> assetChannel()
    {
        List<SelectOption> channelPicklistValues = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Asset.Channel__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        channelPicklistValues = selectOptionUtility(ple);
        return channelPicklistValues;
    }
    //CR-02850: Changes Start
    //get Channel detail picklist values from Asset object using describe method
    public static Map<Object,List<String>> assetChannelDetail()
    {
        Map<Object,List<String>> dependentFieldMap=getDependentPicklistValues('Asset','Channel_Detail__c');
        return dependentFieldMap;
    }
    //CR-02850: Changes End
    public static List<SelectOption> selectOptionUtility(List<Schema.PicklistEntry> picklistValues)
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('--None--','--None--'));
        for( Schema.PicklistEntry f : picklistValues)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    //CR-02850: Changes Start
    public static List<SelectOption> selectOptionUtility(List<String> picklistValues)
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for( String p : picklistValues)
        {
            options.add(new SelectOption(p, p));
        }       
        return options;
    }

   public static Map<Object,List<String>> getDependentPicklistValues( String sObjectName, String fieldName )
    {
        return getDependentPicklistValues
        (Schema.getGlobalDescribe().get( sObjectName ).getDescribe().fields.getMap().get( fieldName )
        );
    }
    
    public static Map<Object,List<String>> getDependentPicklistValues( Schema.sObjectField dependToken )
    {
        Schema.DescribeFieldResult depend = dependToken.getDescribe();
        Schema.sObjectField controlToken = depend.getController();
        if ( controlToken == null ) return null;
        Schema.DescribeFieldResult control = controlToken.getDescribe();
        List<Schema.PicklistEntry> controlEntries =
        (   control.getType() == Schema.DisplayType.Boolean
        ?   null
        :   control.getPicklistValues()
        );
     
        String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        Map<Object,List<String>> dependentPicklistValues = new Map<Object,List<String>>();
        for ( Schema.PicklistEntry entry : depend.getPicklistValues() ) if ( entry.isActive() )
        {
            List<String> base64chars =
                String.valueOf
                (   ((Map<String,Object>) JSON.deserializeUntyped( JSON.serialize( entry ) )).get( 'validFor' )
                ).split( '' );
            for ( Integer index = 0; index < (controlEntries != null ? controlEntries.size() : 2); index++ )
            {
                Object controlValue =
                (   controlEntries == null
                ?   (Object) (index == 1)
                :   (Object) (controlEntries[ index ].isActive() ? controlEntries[ index ].getLabel() : null)
                );
                Integer bitIndex = index / 6, bitShift = 5 - Math.mod( index, 6 );
                if  (   controlValue == null
                    ||  (base64map.indexOf( base64chars[ bitIndex ] ) & (1 << bitShift)) == 0
                    ) continue;
                if ( !dependentPicklistValues.containsKey( controlValue ) )
                {
                    dependentPicklistValues.put( controlValue, new List<String>() );
                }
                dependentPicklistValues.get( controlValue ).add( entry.getLabel() );
            }
        }
        return dependentPicklistValues;
    }
    //CR-02850: Changes End
}