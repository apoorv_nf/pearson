/**********************************************************************************************************************************************************
 * Class Name   : UKTEPPricingXMLController
 * Author       : Aman Sawhney
 * Date Created : 13 / Nov / 2018
 * Description  : This class handles the pricing xml request creation and response parsing.
 ***********************************************************************************************************************************************************
    Date            Version         ModifiedBy                      Description
 31-Dec-2018          1.1           Navaneeth                       Modified the OtherInformationDetails in Order and OrderItem XML Construct
 05-Feb-2019          1.2           Navaneeth                       Modified the Account - ERPID to Account - Primary_Selling_Account_External_Number__c
 07-Feb-2019          1.3           Navaneeth                       Added details for OneTime Ship to Flag - Y \ N and Shipping Address Details
 07-Mar-2019          1.4           Navaneeth                       Replacing the '&' with '&amp;'Special Character in Quote Acconunt Name & Quote Shipping Address Details
 ***********************************************************************************************************************************************************/

public with sharing class UKTEPPricingXMLController {

    //singleton getInstance to maintain one instance of this class
    private static final UKTEPPricingXMLController pricingXMLHandler = new UKTEPPricingXMLController();
    public static UKTEPPricingXMLController getInstance() {
        return pricingXMLHandler;
    }
    
    //instantiations
    public Id quoteId{get;set;}
    
    //This method makes the pricing request using quote data in xml string form and returns it.    
    public QuoteRequestDetail getQuoteRequestDetail() {

        //final response
        QuoteRequestDetail finalResponse = new QuoteRequestDetail();

        //target quote call       
        Quote targetQuote = [Select Id, Name, Status, Opportunity.Market__c, Opportunity.CurrencyIsoCode, Account.Name, ERP_Billing_Site_Use_ID__c, Account.ERP_ID__c,Account.Primary_Selling_Account_External_Number__c,
            Opportunity.Primary_Contact__r.Email,
            Sec_School_Discount__c,
            Ship_To_Address__r.ERP_Shipping_Site_Use_ID__c,
            Ship_To_Address__r.Address_1__c, Ship_To_Address__r.Address_2__c, Ship_To_Address__r.Address_3__c,
            Ship_To_Address__r.City__c, Ship_To_Address__r.Country__c, Ship_To_Address__r.Country_Code__c, Ship_To_Address__r.Zip_Code__c, Ship_To_Address__r.State__c, Ship_To_Address__r.State_Code__c,
            (Select Id, Product2.ISBN__c, ListPrice, Quantity, Sample__c,Promo_Discount_Code__c from QuoteLineItems)

            from Quote where Id =: this.quoteId
        ];

        //some constants
        final String SOURCE_SYSTEM = 'ONECRM';
        final String ORDER_FLAG = 'Order Simulation';
        final String CART_ID = '0';
        final String STORE_ID = '0';
        // final String CREATION_TIME = String.valueOf(System.Now());
        final String MARKET_REST_FLAG = 'Y';
        final String PRODUCT_SUBS_FLAG = 'Y';
        final String ITEM_STATUS_FLAG = 'Y';
        final String AUTHOR_VALUE = 'Author';
        
        
        //nullable values
        String primaryContactEmail = ((targetQuote.Opportunity.Primary_Contact__r==null || targetQuote.Opportunity.Primary_Contact__r.Email==null)?'noemail@noemail.com':targetQuote.Opportunity.Primary_Contact__r.Email);
        
        // Commented and Modifed by Navaneeth - Start
        // 5th Feb 2019 - Confirmation From Grace , BA , OneCRM & QA Team
        // String customerNumber = ((targetQuote.Account.ERP_ID__c==null)?'':targetQuote.Account.ERP_ID__c);    
        String customerNumber = ((targetQuote.Account.Primary_Selling_Account_External_Number__c==null)?'':targetQuote.Account.Primary_Selling_Account_External_Number__c);
        // Commented and Modifed by Navaneeth - End
        
        // Added by Navaneeth - Start
        // OneCRM Market is UK - TEP will accept as GB - Hence Transforming
        String marketChange = 'No Value';
        if(targetQuote.Opportunity.Market__c == Label.PS_UKMarket){
            marketChange = 'GB';
        }
        // Added by Navaneeth - End
        
        // Added by Navaneeth - Start
        String oneTimeShipToFlag = 'N';
        String addressShipLine1 = '';
        String addressShipLine2 = '';
        String addressShipLine3 = '';
        String addressShipCountry = '';
        String addressShipState = '';
        String addressShipCity = '';
        String addressShipZipCode = '';
        String shipaddressERPSiteUseId = ((targetQuote.Ship_To_Address__r.ERP_Shipping_Site_Use_ID__c==null)?'':targetQuote.Ship_To_Address__r.ERP_Shipping_Site_Use_ID__c);
        if(targetQuote.Ship_To_Address__r.ERP_Shipping_Site_Use_ID__c == null)
            oneTimeShipToFlag = 'Y';
        if(oneTimeShipToFlag == 'Y')
        {
            addressShipLine1 = ((targetQuote.Ship_To_Address__r.Address_1__c==null)?'':targetQuote.Ship_To_Address__r.Address_1__c);
            addressShipLine2 = ((targetQuote.Ship_To_Address__r.Address_2__c==null)?'':targetQuote.Ship_To_Address__r.Address_2__c);
            addressShipLine3 = ((targetQuote.Ship_To_Address__r.Address_3__c==null)?'':targetQuote.Ship_To_Address__r.Address_3__c);
            addressShipCountry = ((targetQuote.Ship_To_Address__r.Country_Code__c==null)?'':targetQuote.Ship_To_Address__r.Country_Code__c);
            addressShipState = ((targetQuote.Ship_To_Address__r.State_Code__c==null)?'':targetQuote.Ship_To_Address__r.State_Code__c);
            addressShipCity = ((targetQuote.Ship_To_Address__r.City__c==null)?'':targetQuote.Ship_To_Address__r.City__c);
            addressShipZipCode = ((targetQuote.Ship_To_Address__r.Zip_Code__c==null)?'':targetQuote.Ship_To_Address__r.Zip_Code__c);
        }            
        // Added by Navaneeth - End
        
        
        //xml request start
        String quoteListStart = '<orders>';
        //String quoteDetailStart = '<order>'+
        String quoteDetailStart = '<order creationTime="' + DateTime.Now().format('YYYY-MM-dd') + 'T' + DateTime.Now().format('HH:mm:ss') +'" currencyIsoCode="' + targetQuote.Opportunity.CurrencyIsoCode + '" >' +
            '<sourceOrderNumber>' + targetQuote.Id + '</sourceOrderNumber>' +
            '<sourceSystem>' + SOURCE_SYSTEM + '</sourceSystem>' +
            '<country>' + marketChange + '</country>' +
            '<currency>' + targetQuote.Opportunity.CurrencyIsoCode + '</currency>' +
            '<orderFlag>' + ORDER_FLAG + '</orderFlag>' +
            '<cartId>' + CART_ID + '</cartId>' +
            '<storeId>' + STORE_ID + '</storeId>' +
            '<customerNumber>' + customerNumber + '</customerNumber>' +
            '<marketRestrictionsFlag>' + MARKET_REST_FLAG + '</marketRestrictionsFlag>' +
            '<productSubstitutionFlag>' + PRODUCT_SUBS_FLAG + '</productSubstitutionFlag>' +
            '<itemStatusFlag>' + ITEM_STATUS_FLAG + '</itemStatusFlag>' +
            '<sourceCatalog>' + marketChange + '</sourceCatalog>' +
            '<name>' + targetQuote.Account.Name.replace('<','&lt;').replace('>','&agt;').replace('&','&amp;') + '</name>' +
            '<primaryEmail>' + primaryContactEmail + '</primaryEmail>' + 
            '<weight unit="Kg">0.00</weight>'+ //need this
            '<addresses>' +
            '<address type="BILL_TO">' +
            '<billToSiteNumber>' + targetQuote.ERP_Billing_Site_Use_ID__c + '</billToSiteNumber>' +
            '</address>' +
            '<address type="SHIP_TO">' +
            '<shipToSiteNumber>' + shipaddressERPSiteUseId + '</shipToSiteNumber>' +
            '<oneTimeShipFlag>' + oneTimeShipToFlag + '</oneTimeShipFlag>' + //need this logic for OneTime Ship to Flag
            '<addressLine1>'+ addressShipLine1.replace('<','&lt;').replace('>','&agt;').replace('&','&amp;') +'</addressLine1>' +
            '<addressLine2>'+ addressShipLine2.replace('<','&lt;').replace('>','&agt;').replace('&','&amp;') +'</addressLine2>' +
            '<addressLine3>'+ addressShipLine3.replace('<','&lt;').replace('>','&agt;').replace('&','&amp;') +'</addressLine3>' +
            '<region>'+ addressShipState +'</region>' +
            '<country>'+ addressShipCountry +'</country>' +
            '<city>'+ addressShipCity +'</city>' +
            '<postalCode>'+ addressShipZipCode + '</postalCode>' +
            '</address>' +
            '</addresses>' +
            '<otherInformation>' +
            '<entry>' +
            '<name>SecondarySchoolDiscount</name>' +
            '<value>' + targetQuote.Sec_School_Discount__c + '</value>' +
            '</entry>' +
            '</otherInformation>';

        // Updated the above String Population references from Opportunity.Market__c to String marketChange - by Navaneeth
        
        //make the xml for line items
        String quoteLineItemListStart = '<orderLines>';

        String quoteLineItemDetail = '';
        if (targetQuote.QuoteLineItems != null && !targetQuote.QuoteLineItems.isEmpty()) {

            for (QuoteLineItem li: targetQuote.QuoteLineItems) {
                
                //nullable values
                String promoCode = ((li.Promo_Discount_Code__c==null)?'':li.Promo_Discount_Code__c);
                
                //set the values to specific format
                Integer quantity = (li.Quantity==null?0:Integer.valueOf(li.Quantity));
                
                //if not sample quote line item
                if (!li.Sample__c) {
                    quoteLineItemDetail += '<line>' +
                        '<sourceLineId>' + li.Id + '</sourceLineId>' +
                        '<itemCode>' + li.Product2.ISBN__c + '</itemCode>' +
                        '<quantity>' + quantity + '</quantity>' +
                        '<cost></cost>' +
                        '<otherInformation>' +
                        '<entry>' +
                        '<name>linePromoCode</name>' +
                        '<value>' + promoCode + '</value>' + //need this
                        '</entry>' +
                        '</otherInformation>' +
                        '</line>';
                } else {

                    //collect the records of sample quote line items for 0 price updates
                    if (finalResponse.sampleQuoteLineItemList == null) {
                        finalResponse.sampleQuoteLineItemList = new List < QuoteLineItem > ();
                    }
                    finalResponse.sampleQuoteLineItemList.add(li);

                }

            }

        }


        String quoteLineItemListEnd = '</orderLines>';

        String quoteDetailEnd = '</order>';

        String quoteListEnd = '</orders>';


        //make final reponse, remove spaces and return

        //if no line items are added, return default xml request as null
        if (quoteLineItemDetail == '') {
            finalResponse.tepXMLRequest = null;
        } else {
            finalResponse.tepXMLRequest = quoteListStart + quoteDetailStart + quoteLineItemListStart + quoteLineItemDetail + quoteLineItemListEnd + quoteDetailEnd + quoteListEnd;
        }

        return finalResponse;
    }
    
    // Commented by Navaneeth - Start
    /*
    //This method parses the xml response and returns the respective quote line items for update
    public List < QuoteLineItem > getLineItemsFromXMLResponse(String xmlResponse) {

        //final quote line items to be returned
        List < QuoteLineItem > finalQuoteLineItemList = null;

        //enclosure of try catch as any parsing can result in node not found
        try{
            
            //XML parsing from response
            Dom.Document doc = new Dom.Document();
            doc.load(xmlResponse);
    
            //dom xml list of line items
            List < Dom.XMLNode > lineItemLevelList = null;
            Dom.XMLNode orderLevel = doc.getRootElement().getChildElement('OrderLevel', null);
            
            //check the proper heirarchy of xml
            if(orderLevel!=null){
            
                Dom.XMLNode lineLevel = orderLevel.getChildElement('LineLevel', null);
                
                if(lineLevel!=null){
                    lineItemLevelList = lineLevel.getChildren();
                }
        
            }
        
            //if line items are not null and not empty
            if (lineItemLevelList != null && !lineItemLevelList.isEmpty()) {

                finalQuoteLineItemList = new List < QuoteLineItem > ();

                //prepare quote line items
                for (Dom.XMLNode lineItemNode: lineItemLevelList) {

                    String apiReturnStatus = lineItemNode.getChildElement('API_RETURN_STATUS', null).getText();
                    String apiReturnMessage = lineItemNode.getChildElement('API_RETURN_MESSAGE', null).getText();
                    String isbn = lineItemNode.getChildElement('ISBN', null).getText();
                    String productDetails = lineItemNode.getChildElement('productDetails', null).getText();
                    Double unitListPrice = UKTEPPricingCustomUtility.getDouble(lineItemNode.getChildElement('unitListPrice', null).getText());
                    
                    Double discountPercentage = UKTEPPricingCustomUtility.getDouble(lineItemNode.getChildElement('discountPercentage', null).getText());
                    String discountCode = lineItemNode.getChildElement('discountCode', null).getText();

                    //if the response message for line item is success, convert xml to quote line item sOBject
                    if (apiReturnMessage == 'SUCCESS') {
                        
                        //check the Id of line item
                        try{
                            Id lineItemId = Id.valueOf(lineItemNode.getChildElement('LineRefNumOfOasis', null).getText());
                            finalQuoteLineItemList.add(new QuoteLineItem(
                                Id  = lineItemId,
                                UnitPrice = UKTEPPricingCustomUtility.getDouble(lineItemNode.getChildElement('unitListPrice', null).getText()),
                                Quantity = UKTEPPricingCustomUtility.getInteger(lineItemNode.getChildElement('orderQuantity', null).getText()),
                                Pre_Approved_Discount__c = 100 * UKTEPPricingCustomUtility.getDouble(lineItemNode.getChildElement('discountPercentage', null).getText()),
                                Pre_Approved_Discount_Type__c = lineItemNode.getChildElement('discountCode', null).getText()
                            ));
                        }
                        catch(Exception e){
                        }
                        
                        
                    }

                }

            }
        
        }
        
        catch(Exception e){
            
            
        }
        
        //Consoled debugs
        
        return finalQuoteLineItemList;


    }
    
    */
    // Commented by Navaneeth - End
    
    //SubClass to handle request maker response
    public class QuoteRequestDetail {

        public List<QuoteLineItem> sampleQuoteLineItemList{get;set;}
        public String tepXMLRequest{get;set;}

    }

    // Added by Navaneeth - Start
    
    
    //This method parses the xml response and returns the wrapperClass to Process Updated Price and Display Actual Error Message
    public UKTEPWrapperResponseClass getLineItemsFromXMLResponseReturnWrapper(String xmlResponse) {

        // Final WrapperClass to be Returned 
        UKTEPWrapperResponseClass finalWrapper = new UKTEPWrapperResponseClass();
        
        // Final Status & Message to be added to the WrapperClass
        String finalStatus = '';
        String finalMessage = '';
        
        //final quote line items to be added to the wrapper class QuoteLineItem List
        List < QuoteLineItem > finalQuoteLineItemList = null;

        //enclosure of try catch as any parsing can result in node not found
        try{
            
            //XML parsing from response
            Dom.Document doc = new Dom.Document();
            doc.load(xmlResponse);
    
            //dom xml list of line items
            List < Dom.XMLNode > lineItemLevelList = null;
            Dom.XMLNode orderLevel = doc.getRootElement().getChildElement('OrderLevel', null);
            
            //check the proper heirarchy of xml
            if(orderLevel!=null){
            
                Dom.XMLNode lineLevel = orderLevel.getChildElement('LineLevel', null);
                
                if(lineLevel!=null){
                    lineItemLevelList = lineLevel.getChildren();
                }
        
            }
            
            // Reading the Header Level Status & Message
            
            finalStatus = orderLevel.getChildElement('API_RETURN_STATUS', null).getText();
            finalMessage = orderLevel.getChildElement('API_RETURN_MESSAGE', null).getText();
        
            //if line items are not null and not empty
            if (lineItemLevelList != null && !lineItemLevelList.isEmpty()) {

                finalQuoteLineItemList = new List < QuoteLineItem > ();

                //prepare quote line items
                for (Dom.XMLNode lineItemNode: lineItemLevelList) {

                    String apiReturnStatus = lineItemNode.getChildElement('API_RETURN_STATUS', null).getText();
                    String apiReturnMessage = lineItemNode.getChildElement('API_RETURN_MESSAGE', null).getText();
                    String isbn = lineItemNode.getChildElement('ISBN', null).getText();
                    String productDetails = lineItemNode.getChildElement('productDetails', null).getText();
                    Double unitListPrice = UKTEPPricingCustomUtility.getDouble(lineItemNode.getChildElement('unitListPrice', null).getText());
                    //if the response message for line item is success, convert xml to quote line item sOBject
                    if (apiReturnMessage == 'SUCCESS') {
                        
                        //check the Id of line item
                        try{
                            Id lineItemId = Id.valueOf(lineItemNode.getChildElement('LineRefNumOfOasis', null).getText());
                            finalQuoteLineItemList.add(new QuoteLineItem(
                                Id  = lineItemId,
                                UnitPrice = UKTEPPricingCustomUtility.getDouble(lineItemNode.getChildElement('unitListPrice', null).getText()),
                                Quantity = UKTEPPricingCustomUtility.getInteger(lineItemNode.getChildElement('orderQuantity', null).getText()),
                                Pre_Approved_Discount__c = 100 * UKTEPPricingCustomUtility.getDouble(lineItemNode.getChildElement('discountPercentage', null).getText()),
                                Pre_Approved_Discount_Type__c = lineItemNode.getChildElement('discountCode', null).getText()
                            ));
                        }
                        catch(Exception e){
                        }
                        
                        
                    }
                    

                }

            }
        
        }
        
        catch(Exception e){
            
            
        }
        
        //Consoled debugs
        
        finalWrapper.processResponseStatus = finalStatus;
        finalWrapper.processResponseMessage = finalMessage;
        finalWrapper.processResponseQuoteLineItemList = finalQuoteLineItemList;
        
        return finalWrapper;


    }
    // Added by Navaneeth - End
}