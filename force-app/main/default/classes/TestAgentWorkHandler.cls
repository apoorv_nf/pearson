/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 10/07/2018
 */
@isTest
private class TestAgentWorkHandler {


    @isTest(SeeAllData=true)
    static void trigger_test_coverage() {
        // This is for trigger test coverage only. It is not currenly possible to create an AgentWork record from a unit test
        // without making the test user availabe in Omnichannel.
        List<AgentWork> agentWork = [SELECT Id FROM AgentWork LIMIT 1];
        System.assert(!agentWork.isEmpty(), 'Agent Work results are empty');
        update agentWork;
    }

    @isTest
    static void it_should_update_a_case_with_new_owner_and_market() {
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];

        Bypass_settings__C settings = new Bypass_settings__C();
        settings.Disable_Workflow_Rules__c = false;
        settings.Disable_Process_Builder__c = false;
        settings.Disable_Validation_Rules__c = false;
        settings.Disable_Triggers__c = false;
        settings.Disable_Integration_Requests__c = false;

        insert settings;

        Case cas = new Case();

        System.runAs(automatedProcess) {
            cas.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
            insert cas;

            cas.OwnerId = [SELECT Id FROM Group WHERE DeveloperName = 'Chatbot_Case_Queue'].Id;
            update cas;
        }

        User testUser = [SELECT Id, Market__c FROM User WHERE Market__c != null AND IsActive = true AND UserPermissionsLiveAgentUser = true AND UserPermissionsSupportUser = true LIMIT 1];

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor; 

        LiveChatTranscript transcript = new LiveChatTranscript(CaseId = cas.Id, LiveChatVisitorId = vistor.Id);
        insert transcript;

        AgentWork agentWork = new AgentWork(
            UserId = testUser.Id,
            WorkItemId = transcript.Id
        );

        // System.runAs(automatedProcess) {
            new AgentWorkHandler().afterInsert(new List<AgentWork>{agentWork});
        // }

        System.assertEquals(1, [
            SELECT Count()
            FROM Case
            WHERE OwnerId = :testUser.Id
            AND Market__c = :testUser.Market__c
            AND IsEscalated = false
        ]);
    }

    @isTest
    static void testSMSCaseOwnerishipChange() {
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];

        Case newCase = new Case();

        System.runAs(automatedProcess) {
            newCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Technical_Support').getRecordTypeId();
            insert newCase;
        }

        Id msgSessionId = TestAgentWorkHandler.getMsgSessionId(newCase.Id);
        AgentWork agentWork = new AgentWork(
                    UserId = UserInfo.getUserId(),
                    WorkItemId = msgSessionId
        );
        
        Test.startTest();
            new AgentWorkHandler().afterInsert(new List<AgentWork>{agentWork});
        Test.stopTest();

        System.assertEquals(1, [
            SELECT Count()
            FROM Case
            WHERE OwnerId = :UserInfo.getUserId()
        ]);
    }

    private static Id getMsgSessionId(Id caseId){
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {

            MessagingChannel msgChannnel = new MessagingChannel(MasterLabel = '+11234567890', IsActive = TRUE, 
                                                                MessageType = 'Text', MessagingPlatformKey = '+11234567890', 
                                                                DeveloperName = 'Text_11234567890'
                                                                );
            insert msgChannnel;
    
            // Create a test MessagingEndUser
            MessagingEndUser msgUser =  new MessagingEndUser(MessagingPlatformKey = '+12345678901', Name = '+12345678901',
                                                            IsOptedOut = FALSE, MessagingChannelId = msgChannnel.Id,
                                                            MessageType = 'Text');
            insert msgUser;
    
            MessagingSession msgSession = new MessagingSession(MessagingEndUserId = msgUser.Id, MessagingChannelId = msgChannnel.Id, 
                                              Status = 'Active', CaseId = caseId);
            insert msgSession;
        
            return msgSession.Id;
        }
        return NULL;
    }
}