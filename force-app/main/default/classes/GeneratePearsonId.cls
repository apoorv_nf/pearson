/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            CalculateAccountFee.cls
   Description:     This logic is not used anymore so commenting the whole class
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  21/08/2015         1.1            Leonard Vicotr                   Code Strramline as part of R3 - Removed unsued and commented code
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class GeneratePearsonId 
{  
    
    public static void GenerateRandomPearsonID(List<Account> listAccount)
    {
        Set<Id> UserIds = new Set<Id>();
         List<Id> UserId = new List<Id>();
         List<Account> AccList = new List<Account>();
         List<Account> AccountList = new List<Account>();
         List<Account> AccountList1 = new List<Account>();
         Set<id> AccountId = new Set<id>();
         List<id> AccountIds = new List<id>();
         Map<id,List<Account>> UserAccountmap = new Map<Id,List<Account>>();
         List<User> Userlist = new List<User>();
         List<User> Userlist1 = new List<User>();
         //List<Account> accstudentIdlist = [Select Pearson_Account_Number__c from Account];
         //List<Lead> leadstudentIdlist = [Select Pearson_ID_Number__c from Lead];
         List<String> StudentIdlist = new List<String>();
         Set<String> ExistingIds = new Set<String>();
          
        //create a hash set of all student Ids
        //String[] ANZRecordTypes = System.Label.ANZ_Account_Record_Type.split(',');
        //String LearnerRecordType = 'Learner';
        Id LearnerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.PS_LearnerRecordType).getRecordTypeId();
        
 
            for(Account acc : listAccount) 
                AccountIds.add(acc.id);
        
          if(AccountIds.size()>0){
              //if(!Test.isRunningTest()) //commented by Mayank
                Accountlist = [Select id,Pearson_Account_Number__c from Account where Id In:AccountIds and RecordTypeId=:LearnerRecordTypeId];
          } 
            
          
          //For StudentId generation
          if(AccountList.size()>0){
             for( Account act: [select Pearson_Account_Number__c from Account where RecordTypeId =: LearnerRecordTypeId and Pearson_Account_Number__c<> NULL and account.market2__c =: 'ZA' order by lastmodifieddate desc limit 999])
             {
                 if( ! (String.isEmpty(act.Pearson_Account_Number__c) || String.isBlank(act.Pearson_Account_Number__c) ) )
                     ExistingIds.add(act.Pearson_Account_Number__c);
             } 
              
             for(Account a:AccountList){
               Account a1 = new Account();
               a1.id = a.Id;
               
           
            String str = GeneratePearsonId.GenerateRandId();
            //check if generated Id already exists, if yes genearte until unique found
            while(ExistingIds.contains(str))
            {
                str = GeneratePearsonId.GenerateRandId();
            }
            Boolean flag= false;
                             
             for(string s:StudentIdlist){
              if(s==str){
                flag = true;
                break;
               }}
              if(flag!=true){
                a1.Pearson_Account_Number__c = str;
                StudentIdlist.add(str);
                }
              
                
              AccountList1.add(a1); 
               
          }}
          
          if(UserList1.size()>0){  
            Update UserList1;  }
          
          if(AccountList1.size()>0){
                if(!Test.isRunningTest()) Update AccountList1;
             }
       }
    
    private static String GenerateRandId()
    {
        final String chars = PS_Constants.Account_GENERATE_PSID1; 
            String randStr = '';
             while (randStr.length() < 8) {
               Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 30);
               randStr += chars.substring(idx, idx+1);
             }  
             
            final String chars2 = PS_Constants.Account_GENERATE_PSID2;
             String randStr2 = '';
             while (randStr2.length() < 1) {
               Integer idx2 = Math.mod(Math.abs(Crypto.getRandomInteger()), 9);
               randStr2 += chars2.substring(idx2, idx2+1);
             }
            return randstr+randStr2;
                   
              

    }
}