/* -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            ScheduledCaseDeletionWithACR.cls 
Description:    This class is a scheduler for CaseDeletionWithACR batch class.

Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ------------------------------------------------------------------------------------------------------------------------------------
May 13 2019       1.0                Priya                For CR-01780 : Batch Apex For Pre-Call Form Closed Case Records Deletion and Related Contact & ACR Record Deletion as well.                                                    
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
global class ScheduledCaseDeletionWithACR implements Schedulable {
 global void execute(SchedulableContext sc) {
      CaseDeletionWithACR obj = new CaseDeletionWithACR();
      database.executebatch(obj,200);
   }
}