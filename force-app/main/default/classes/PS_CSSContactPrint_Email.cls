/*
.....................................................................................................................................
   Name:            PS_CSSContactPrint_Email.apxc 
   Description:     Apex controller for Article Detail Page for Community Self Service
   Test class:      NA 
   Date             Version              Author                          Summary of Changes 
   ...........      ........      .......................    ...........................................................................
  26-Nov-2015         0.1              Neha Karpe                 Self Service Article Detail Page - Community Builder
  12-Jan-2016		  0.2				KP						  Comments to method definition	
.......................................................................................................................................
*/  
public with sharing class PS_CSSContactPrint_Email {
    
    //Method definition to send articles through emails to customers
    @AuraEnabled
    public static void articlesendEmail(String yourEmail, string url, string articleName, string recEmail, string firstName, 
                                          string recName) {       
        Outbound_Notification__c outnot = new Outbound_Notification__c();
        outnot.Event__c = 'Article';
        outnot.Method__c = 'Email';
        outnot.Type__c = 'Article Outbound';
        outnot.PS_ArticleRecipient__c=recEmail;
        outnot.PS_ArticleCCRecipient__c=yourEmail;
        outnot.PS_ArticleName__c=articleName;
        outnot.PS_ArticleURL__c=url;
        outnot.PS_RecipientName__c=recName;
        outnot.PS_Sender_Name__c=firstName;                                      
        try{
            insert outnot;}
      	catch(Exception e){
        	throw(e);                                          
        }                                     
    }
    
    //Method to get current logged in user
	@AuraEnabled
    public static User getCurrentUser() 
    {
         String message;
        User toReturn;
        String Profile = System.Label.Pearson_Support_Profile;
        try{
            //Defect 4942 - Making this change to avoid List has no rows exception for chat to case.
            //toReturn = [SELECT Id, Name, FirstName, MobilePhone, LastName, Email, City, Country, State , UserName FROM User WHERE Id = :UserInfo.getUserId() and Profile.Name !=: Profile LIMIT 1];
           toReturn = [SELECT Id, Name, FirstName, MobilePhone, LastName, Email, City, Country, State , UserName , Profile.Name   FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
       
            
            if( toReturn.Profile.Name != Profile)
           {
               
            return toReturn;  
           }
            else return null;
        }
        Catch(System.Exception e)
        {
            message=e.getMessage();
            return null;
        }
    }
    
}