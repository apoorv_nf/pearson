/*******************************************************************************************************************
 * Apex Class Name  : PS_ReturningOpportunityMainTest
 * Version          : 1.0
 * Created Date     : 24 November 2015
 * Function         : Test Class for PS_ReturningOpportunityMain 
 * Modification Log :
 *
 * Developer                   Date                    Description
 * ------------------------------------------------------------------------------------------------------------------
 * sneha.a.sinha                24/11/2015              Test Class for PS_ReturningOpportunityMain 
 * --------------------------------------------------------------------------------------------------------------------
  *******************************************************************************************************************/

@isTest
Public class PS_ReturningOpportunityMainTest
{
                
Static testmethod void myUnitTest()
 { 
   ID oid;
   PS_ReturningOpportunityMain OppMainPage = new PS_ReturningOpportunityMain();
   List<opportunity>opp = Testdatafactory.createOpportunity(2,'D2L');
   opp[0].stagename='lost';
   opp[0].Lost_Reason__c='Price\\Value';
   insert opp;
   oid=opp[0].ID;
   
   PageReference pageRef = new PageReference('/apex/?id=' + opp[0].Id); 
   Test.setCurrentPageReference(pageRef);
   OppMainPage.openPage();
   PageReference pageRef1 = new PageReference('/apex/?id=' + opp[1].Id); 
   Test.setCurrentPageReference(pageRef1);
   OppMainPage.openPage();
 }
 
  Static testmethod void myUnitTest2()
 { 
 
  ID oid;
  PS_ReturningOpportunityMain OppMainPage1 = new PS_ReturningOpportunityMain();
  List<opportunity>opp = Testdatafactory.createOpportunity(1,'D2L');
  
  opp[0].stagename='closed';
  opp[0].Signed_Enrolement_Declaration_Recieved__c=true;
  opp[0].Lost_Reason__c='Price\\Value';
  insert opp;
  oid=opp[0].id;
   
   list<opportunity>opp1=[select id,stagename,External_Opportunity_Number__c,External_ID__c,Product_Count__c,Academic_Start_Date__c,
       PS_OppotunityIntegartionfield__c,CreatedById from opportunity where id=:oid];
      
   apexpages.currentpage().getParameters().put('id',oid);
   OppMainPage1.openpage();
 
 }
}