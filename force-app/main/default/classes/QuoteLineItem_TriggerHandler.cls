/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            QuoteLineItem_TriggerHandler
Description:     On insert/update of QuoteLineItem 
Code Coverage:   CteateStandardQuotetest   
Market:          ZA
Date             Version         Author                              Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
08/03/2018          1.0          Mani                 To update the Outside_Module__c checkbox on QuoteLineItems when modules of two different Qualification Levels are selected
------------------------------------------------------------------------------------------------------------------------------------------------------------ */


Public class QuoteLineItem_TriggerHandler{
    /*public static void LineItemUpsert(List<QuoteLineItem> newQuoLineMap,Boolean isDelete){
        Set<Integer> levelSet = new Set<Integer>();
        for(QuoteLineItem quoLine : newQuoLineMap){
            levelSet.add(Integer.valueOf(quoLine.Product2Level__c));
        }
        List<Integer> tempList = new List<Integer>(levelSet);
        Integer maxLevel = tempList[0];
        for (integer i =0;i<tempList.size();i++){            
            if( tempList[i] > maxLevel)
                maxLevel = tempList[i];             
        }    
        for(QuoteLineItem quoLine : newQuoLineMap){
            if(Integer.valueOf(quoLine.Product2Level__c)<maxLevel){
                quoLine.Outside_Module__c=true;
            }else{
                quoLine.Outside_Module__c=false;
            }
        }
        if(isDelete && !newQuoLineMap.isEmpty()){
            Update newQuoLineMap;
        }
    }*/
    public static void LineItemtoQuoteUpdate(Map<Id,QuoteLineItem> newQuoLineMap){
        Set<Integer> levelSet = new Set<Integer>();
        Set<Id> quoteIdSet = new Set<Id>();
        for(QuoteLineItem quoLine : newQuoLineMap.values()){
            
            quoteIdSet.add(quoLine.QuoteId);
        }
        
        List<Quote> quoteUpdateList = new List<Quote>();
        List<QuoteLineItem> allLinesList = new List<QuoteLineItem>();
        allLinesList =[SELECT ID,Product2Level__c,Outside_Module__c FROM QuoteLineItem WHERE QuoteId IN:quoteIdSet AND Quote.Market__c='ZA' AND IsDeleted=false LIMIT 50000];
        for(QuoteLineItem quoLine : allLinesList){
            levelSet.add(Integer.valueOf(quoLine.Product2Level__c));
            //quoteIdSet.add(quoLine.QuoteId);
        }
        List<Integer> tempList = new List<Integer>(levelSet);
        Integer maxLevel;
        if(!tempList.isEmpty()){
            maxLevel = tempList[0];
            for (integer i =0;i<tempList.size();i++){            
                if( tempList[i] > maxLevel)
                    maxLevel = tempList[i];             
            }
                
            
            for(QuoteLineItem quoLine : allLinesList){
                if(Integer.valueOf(quoLine.Product2Level__c)<maxLevel){
                    quoLine.Outside_Module__c=true;
                }else{
                    quoLine.Outside_Module__c=false;
                }
                if(Integer.valueOf(quoLine.Product2Level__c)==maxLevel){
                    quoLine.Outside_Module__c=false;
                }
            }
            if(!allLinesList.isEmpty()){
                database.update(allLinesList,false);
            }
            for(ID quoId : quoteIdSet){
                Quote quoUpdate = new Quote();
                quoUpdate.ID=quoId;
                //quoUpdate.Qualification_Level_Name__c=maxLevel;
            quoteUpdateList.add(quoUpdate); 
            }
            if(!quoteUpdateList.isEmpty()){
                Update quoteUpdateList;
            }
        }
    }
}