public class CommunityContentController {
    @AuraEnabled
    public static Map<String, List<Community_Content_Detail__c>> getData(String communityType, String communityContentType){
        List<Community_Content__c> cContentList = new List<Community_Content__c>();
        cContentList = [select Community_Content_Detail_Group__c, Order__c, (select Name, Display_Name_URL__c, Google_Tracking_Id__c 
                                                                             from Community_Content_Details__r 
                                                                             where Visible_in_Community__c = true ORDER BY Order__c ASC) 
                        from Community_Content__c where Community_Content_Type__c = :communityContentType 
                        and Community_Type__c = :communityType ORDER BY Order__c ASC];
        
        Map<String, List<Community_Content_Detail__c>> results = new Map<String, List<Community_Content_Detail__c>>();
        Integer i = 1;
        for(Community_Content__c cC : cContentList){
            List<Community_Content_Detail__c> cCDetails = new List<Community_Content_Detail__c>();
            
            for(Community_Content_Detail__c cCDetail : cC.Community_Content_Details__r){
                if(cCDetail != null){
                    cCDetails.add(cCDetail);
                }
            }
            
            if(!cCDetails.isEmpty() && ((cC.Community_Content_Detail_Group__c == null) || (cC.Community_Content_Detail_Group__c == ''))){
                List<Community_Content_Detail__c> detail = results.get('notitle'+i);
                if(detail != null){
                    detail.addAll(cCDetails);
                } else {
                    List<Community_Content_Detail__c> detailNoTitle = new List<Community_Content_Detail__c>();
                    detailNoTitle.addAll(cCDetails);
                    results.put('notitle'+i, detailNoTitle);
                }
            } else if(!cCDetails.isEmpty()){
                List<Community_Content_Detail__c> detail = results.get(cC.Community_Content_Detail_Group__c);
                if(detail != null){
                    detail.addAll(cCDetails);
                } else {
                    List<Community_Content_Detail__c> details = new List<Community_Content_Detail__c>();
                    details.addAll(cCDetails);
                    results.put(cC.Community_Content_Detail_Group__c, details);
                }
            }
            i++;
        }
        
        return results;
    }
}