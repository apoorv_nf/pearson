/*******************************************************************************************************************
* Apex Class Name  : PS_CaseUpdateParentCaseCtrlTest
* Version          : 1.0 
* Created Date     : 19 Nov 2015
* Function         : Test Class of the PS_CaseUpdateParentCaseCtrl Class
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
* Payal Popat               19/11/2015              Created Initial Version of PS_CaseUpdateParentCaseCtrlTest
* Rohit Kulkarni            04/03/2016              Negative scenario
*******************************************************************************************************************/
@isTest(SeeAllData = False)
public with Sharing class PS_CaseUpdateParentCaseCtrlTest{

    public static testMethod void test_mParentCaseValidation(){
        
        User usr;
        UserRole tempUserRole;
        List<Account> accList = new List<Account>();
        List<Case> parentCaseList = new List<Case>(); 
        List<Case> childCaseList = new List<Case>();
        Map<id,Case> caseOldMap = new Map<id,Case>();
        Case caseRecord = new Case();
        
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        
        
        //user to avoid Mixed DML error and User validation errors
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){              
            Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c=true);
            insert byp;
            
            PS_Batch_Size_for_Child_Cases_Closure__c btch = new PS_Batch_Size_for_Child_Cases_Closure__c();
        	btch.name = 'ChildBatchSize';
        	btch.Batch_Size__c = 10.0;
        	btch.Threshold_No_Of_Childs__c = 20;
        	insert btch;
            
            // parent case            
            parentCaseList = TestDataFactory.createCase(1, 'School Assessment');
            parentCaseList[0].status = Label.PS_CaseClosureStatus;            
            parentCaseList[0].createddate = system.Today() -8;
            //parentCaseList.add(caseRecord1);          
            insert parentCaseList;  
            
            
            
            Test.startTest();
            //child cases
            childCaseList = TestDataFactory.createCase(3, 'School Assessment');
            childCaseList[0].ParentId=parentCaseList[0].id;
            childCaseList[0].createddate = system.Today() -8;
            
            childCaseList[1].Status=Label.PS_CaseClosureStatus;
            childCaseList[1].ParentId=parentCaseList[0].id;
            childCaseList[1].createddate = system.Today() -9;
            childCaseList[1].closedDate = system.Today() -8;
            
            //alternate parent case. added in list to insert.
            childCaseList[2].Status=Label.PS_CaseClosureStatus;
            childCaseList[2].closedDate = system.Today() -8;
            childCaseList[2].createddate = system.Today() -8;
            //childCaseList.add(caseRecord);  
            
            
            insert childCaseList;
            
            caseRecord = childCaseList.remove(2);
            
            //iterate child cases to re-map parent
            for(case cs : childCaseList){
                cs.ParentId = caseRecord.id;
                caseOldMap.put(cs.id, cs);
            }
            
            PS_CaseUpdateParentCaseCtrl.mParentCaseValidation(childCaseList,null,true,false,null);//insert
            PS_CaseUpdateParentCaseCtrl.mParentCaseValidation(childCaseList,null,false,true,caseOldMap);//update
            System.assertNotEquals(childCaseList, null);
            Test.StopTest();
        }
    }
    
    public static testMethod void test_ParentChildCase(){
        
        User usr;
        UserRole tempUserRole;
        Map<id,Case> mapOldParent = new map<id,Case>();
        Id sysAdminProfileId = [select Id from Profile where Name =: 'System Administrator'].Id;
        List<Case> parentCaseList = new List<Case>(); 
        List<Case> childCaseList = new List<Case>();
        set<id> parentIdSet =new set<id>();
        List<Case_Owner_Tracking__c> caseOwnerTrackList = new List<Case_Owner_Tracking__c>();
        
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        
        
        //user to avoid Mixed DML error and User validation errors
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = sysAdminProfileId, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c=true);
        insert byp;
            
            PS_Batch_Size_for_Child_Cases_Closure__c btch = new PS_Batch_Size_for_Child_Cases_Closure__c();
        	btch.name = 'ChildBatchSize';
        	btch.Batch_Size__c = 10.0;
        	btch.Threshold_No_Of_Childs__c = 20;
        	insert btch;
            
            //'closed' parent case
            parentCaseList = TestDataFactory.createCase(1, 'School Assessment');
            parentCaseList[0].Status=Label.PS_CaseClosureStatus;
            parentCaseList[0].closedDate = system.Today() -7;
            parentCaseList[0].createddate = system.Today() -8;
            insert parentCaseList; 
            parentIdSet.add(parentCaseList[0].id);
            
            for(Case parCase :parentCaseList) {
            	mapOldParent.put(parCase.id,parCase);    
            }
            
            
            caseOwnerTrackList.add(new Case_Owner_Tracking__c(Resolved__c = true, Agent_Name__c = [select ownerid from case where id=:parentCaseList[0].id].ownerid, tier__c='test', case__c=parentCaseList[0].id));
            
            Test.startTest();            
            //child cases
            childCaseList = TestDataFactory.createCase(2, 'School Assessment');
            childCaseList[0].ParentId=parentCaseList[0].id;
            childCaseList[0].createddate = system.Today() -7;
            
            childCaseList[1].ParentId=parentCaseList[0].id;
            childCaseList[1].createddate = system.Today() -6;
            insert childCaseList;
            
            /*caseOwnerTrackList.add(new Case_Owner_Tracking__c(Resolved__c = true, Agent_Name__c = [select ownerid from case where id=:childCaseList[0].id].ownerid, tier__c='test', case__c=childCaseList[0].id));
            caseOwnerTrackList.add(new Case_Owner_Tracking__c(Resolved__c = true, Agent_Name__c = [select ownerid from case where id=:childCaseList[1].id].ownerid, tier__c='test', case__c=childCaseList[1].id));
            insert caseOwnerTrackList;*/
            
            PS_CaseUpdateParentCaseCtrl.mUpdateChildCase(parentIdSet, parentCaseList,mapOldParent);
            //System.assertNotEquals(caseOwnerTrackList, null);
            Test.StopTest();
        }
    }
    
    
    public static testMethod void test_mUpdateParentCase(){
        
        User usr;
        UserRole tempUserRole;
        Id sysAdminProfileId = [select Id from Profile where Name =: 'System Administrator'].Id;
        List<Case> caseList = new List<Case>(); 
        set<id> oCaseIdSet = new set<id>();
        
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        
        
        //user to avoid Mixed DML error and User validation errors
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = sysAdminProfileId, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){
           Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c=true);
           insert byp;
            
            PS_Batch_Size_for_Child_Cases_Closure__c btch = new PS_Batch_Size_for_Child_Cases_Closure__c();
        	btch.name = 'ChildBatchSize';
        	btch.Batch_Size__c = 10.0;
        	btch.Threshold_No_Of_Childs__c = 20;
        	insert btch;
           
           caseList = TestDataFactory.createCase(1, 'School Assessment');
            caseList[0].Status=Label.PS_CaseClosureStatus;
            caseList[0].closedDate = system.Today() -3;
            caseList[0].createddate = system.Today() -4;
            caseList[0].Reopen_Reason__c='Not Resolved';
            Test.startTest();
            insert caseList; 
            
            oCaseIdSet.add(caseList[0].id);
                
                PS_CaseUpdateParentCaseCtrl.mUpdateParentCase(oCaseIdSet,true);
                System.assertNotEquals(caseList, null);
                Test.StopTest();
            }
        }
        //Negative test
        
    public static testMethod void Negative_test_mUpdateParentCase(){
            
            User usr;
            UserRole tempUserRole;
            Id sysAdminProfileId = [select Id from Profile where Name =: 'System Administrator'].Id;
            List<Case> caseList = new List<Case>(); 
            set<id> oCaseIdSet = new set<id>();
            
            //user role to avoid 'portal account owner must have a role' error
            tempUserRole = new UserRole(Name='CEO');
            insert tempUserRole;
        
        	
        	
            
            //user to avoid Mixed DML error and User validation errors
            usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = sysAdminProfileId, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
            
            System.runAs(usr){
                 
                PS_Batch_Size_for_Child_Cases_Closure__c btch = new PS_Batch_Size_for_Child_Cases_Closure__c();
        	btch.name = 'ChildBatchSize';
        	btch.Batch_Size__c = 10.0;
        	btch.Threshold_No_Of_Childs__c = 0;
        	insert btch;
                
                caseList = TestDataFactory.createCase(1, 'School Assessment');
                //Passing wring data
                caseList[0].Status='';
                caseList[0].closedDate = system.Today();
                caseList[0].createddate = system.Today();
                Test.startTest();
                insert caseList; 
                // Added by Mayank for CR-01159
                caseList[0].status = 'Closed';
                caseList[0].Reopen_Reason__c = 'Reopen';   
                update caseList;
                // Added by Mayank for CR-01159
                System.Assert(true,'Invalid Status & close & created date'); 
                oCaseIdSet.add(caseList[0].id);
                
              
                PS_CaseUpdateParentCaseCtrl.mUpdateParentCase(oCaseIdSet,true);
                System.assertNotEquals(caseList, null);
                Test.StopTest();
            }
            
        }
    
    public static testMethod void Negative_test_mUpdateParentCase2(){
            
            User usr;
            UserRole tempUserRole;
            Id sysAdminProfileId = [select Id from Profile where Name =: 'System Administrator'].Id;
            List<Case> caseList = new List<Case>(); 
            set<id> oCaseIdSet = new set<id>();
            
            //user role to avoid 'portal account owner must have a role' error
            tempUserRole = new UserRole(Name='CEO');
            insert tempUserRole;
        
        	

            
            //user to avoid Mixed DML error and User validation errors
            usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = sysAdminProfileId, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
            
            System.runAs(usr){
                
                PS_Batch_Size_for_Child_Cases_Closure__c btch = new PS_Batch_Size_for_Child_Cases_Closure__c();
        	btch.name = 'ChildBatchSize';
        	btch.Batch_Size__c = 10.0;
        	btch.Threshold_No_Of_Childs__c = 0;
        	insert btch;
                //Creating Parent Case 
                caseList = TestDataFactory.createCase(1, 'School Assessment');
                //Passing wring data
                caseList[0].Status='New';
                caseList[0].closedDate = system.Today();
                caseList[0].createddate = system.Today();
                Test.startTest();
                insert caseList; 
                
                System.Assert(true,'Invalid Status & close & created date'); 
                oCaseIdSet.add(caseList[0].id);
                
                //Creating Child Cases
        List<Case> lstCase = new List<Case>();
        for(Integer i=0;i<10;i++) {
            Case childcase = new Case();
            childcase.ParentId = caseList[0].Id;
            childcase.AccountId = caseList[0].AccountId;
            childcase.ContactId = caseList[0].ContactId;
            childcase.Contact_Type__c = 'College Educator';
            childcase.Status = 'New';
            childcase.Origin = 'Email';
            childcase.Priority = 'Medium';
            childcase.Customer_Username__c = 'test';
            childcase.Access_Code__c = 'test';
            childcase.Error_Message_Code__c = 'test';
            childcase.URL__c = 'www.test.com';
            childcase.PS_Business_Vertical__c = 'Clinical';
            childcase.Request_Type__c = 'Technical Support';
            childcase.Platform__c = 'Q-interactive';
            //for(Product2 pro :lstProduct ) {
            childcase.Products__c = caseList[0].productid;    
            //  }
            childcase.Category__c = 'Sign in/ Password';
            childcase.Subcategory__c = 'Wrong site';
            childcase.Subject = 'test';
            childcase.Description = 'test';
            lstCase.add(childcase);
        }
        Database.insert(lstCase, false);
                // Added by Mayank for CR-01159
                //public static void mProcessParentCase(List<Case> triggeredcases,boolean isInsert,boolean isUpdate,Map<id,Case> oldMap){  }
         Map<id,Case> parmap = new Map<id,Case>();
                for(Case cs : lstCase) {
                    parmap.put(cs.ParentId, cs);
                }
                
               /* List<case> childcaselst = new List<Case>();
                for(Case cs : caseList) {
                    cs.status = 'Closed';
                    childcaselst.add(cs);
                }
                update childcaselst; */
                
                caseList[0].Status='Closed';
                update caseList;
                
                // Added by Mayank for CR-01159
                PS_CaseUpdateParentCaseCtrl.mProcessParentCase(caseList,false,true,parmap);
                //PS_CaseUpdateParentCaseCtrl.mUpdateParentCase(oCaseIdSet,true);
                System.assertNotEquals(caseList, null);
                Test.StopTest();
            }
            
        }
    
    }