/*******************************************************************************************************************
* Apex Class Name   : PS_AddAccountTeamMember_Test 
* Created Date      : 11th Sep 2015
* Description       : Test class for AddAccountTeamMember class
*******************************************************************************************************************/

@isTest//(SeeAllData=True)
public Class PS_AddAccountTeamMember_Test {
    static testMethod void myUnitTest() {
        //Test.StartTest();
        List<Account> accList = new List<Account>();
        id pro =[select id, Name from profile where Name=:'Pearson Sales User OneCRM'].id;
        ID orgid  = [SELECT Id FROM RecordType WHERE SobjectType='Account' and Name= 'Organisation'].Id;
        List<User> userLst = TestDataFactory.createUser(pro , 1);
        insert userLst;
       // Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c    =true);
           Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=userLst[0].id,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true);

       insert byp; 
         system.runAs(userLst[0]){
        Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School',Market2__c='US'); 
       // acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Organisation').getRecordTypeId();
        acc.ShippingCountry = 'India';
        acc.ShippingCity  = 'Gurgaon';
        acc.ShippingStreet  = 'DLF';
        acc.ShippingPostalCode = '213456';
        acc.phone             ='+910000000';
        acc.ownerid=userinfo.getuserid();
        acc.RecordTYpeId = orgid;
        system.debug('AccRecord'+acc.RecordTYpeId);
        //system.debug('AccRecordorg'+orgid);
        accList.add(acc);
      //hhh  system.runAs(userLst[0]){
        insert accList;
        //AddAccountTeamMember.intialize(null,null,orgid);
        //User u = [select Id, firstname from user where id=:userinfo.getuserid()];
        //Profile pr;
        //ID d2lId;
        //ID orgid;
       
        //AddAccountTeamMember.intialize(pr, d2lId, orgid);
        System.debug('accList...'+accList);
        AddAccountTeamMember.onInsert(accList);

   }
}}