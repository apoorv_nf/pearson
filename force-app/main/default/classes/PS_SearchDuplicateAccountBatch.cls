/*
if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5){ 
PS_SearchDuplicateAccountBatch b = new PS_SearchDuplicateAccountBatch();
database.executeBatch(b,20);
}
*/
global class PS_SearchDuplicateAccountBatch implements Database.Batchable < sObject > {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //String query = 'SELECT Id,Name,Sequence_No__c FROM Account where Master_Account__c = TRUE';
        String query = System.Label.PS_DuplicateQuery_Master;
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Account > scope) {


        for (Account a: scope) {  
            
            List < Account > accUpdate = new List < Account > ();
            

            String accscopeId = a.Id;
            String accName = a.Name;
            Integer seqNo = Integer.ValueOf(a.Sequence_No__c);
            String strSOSL = System.Label.PS_Duplicate_Query_SOSL;

            //Search.SearchResults searchResults = Search.find('FIND :accName IN Name Fields RETURNING Account (Id,Name,Master_Account__c,Master_Account_No__c,Merge_Account__c,Sequence_No__c,DeDupe_By__c) LIMIT 2000');
            Search.SearchResults searchResults = Search.find(strSOSL);
            List < Search.SearchResult > accList = searchResults.get('Account');
            system.debug('SOSL result:'+accList);
            for (Search.SearchResult searchResult: accList) {
                Account acc = (Account) searchResult.getSObject();
                System.debug(acc.Name);
                String accobjId = acc.Id;
                
                if (accobjId.equals(accscopeId)) {
                    //acc.Master_Account__c = TRUE;
                    acc.Merge_Account__c = FALSE;
                    //acc.Master_Account_No__c = seqNo;
                    acc.DeDupe_By__c = accscopeId;
                } else {
                    //acc.Master_Account__c = FALSE;
                    acc.Merge_Account__c = FALSE;
                    acc.Master_Account_No__c = seqNo;
                    acc.DeDupe_By__c = accscopeId;
                }
                accUpdate.add(acc);
            }
                update accUpdate;
        } //for loop

                //update accUpdate;


    }

    global void finish(Database.BatchableContext BC) {
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                              TotalJobItems, CreatedBy.Email
                              FROM AsyncApexJob WHERE Id =
                              :BC.getJobId()];
      
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('Batch Process Status -- ' + a.Status);
        mail.setPlainTextBody('Batch Process Status -- ' + a.Status);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
            mail
        });
    }
}