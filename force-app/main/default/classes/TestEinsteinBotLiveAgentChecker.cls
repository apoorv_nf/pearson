/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 12/05/2018
 * Update: added testLiveAgentForSMS() to increase coverage - Apoorv (Neuraflash) 29/07/2018
 */
@isTest
private class TestEinsteinBotLiveAgentChecker {

    private static final String CHAT_KEY = 'abcdef';

    @testSetup
    static void setup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {

            LiveChatVisitor vistor = new LiveChatVisitor();
            insert vistor;

		    LiveChatTranscript transcript = new LiveChatTranscript(
			ChatKey = CHAT_KEY,
			LiveChatVisitorId = vistor.Id,
            LiveChatDeploymentId = [SELECT Id FROM LiveChatDeployment LIMIT 1].Id,
            LiveChatButtonId = [SELECT Id FROM LiveChatButton WHERE DeveloperName = 'NAUS_HETS_ACR_STU'].Id
            );
		    insert transcript;

            // Create a queue
            Group testGroup = new Group(Name='NAUS HETS SMS ACR', Type='Queue');
            insert testGroup;

            // Assign user to queue
            GroupMember testGrpMember = new GroupMember(GroupId = testGroup.Id, UserOrGroupId = UserInfo.getUserId());
            insert testGrpMember;

            MessagingChannel channnel = new MessagingChannel(MasterLabel = '+11234567890', IsActive = TRUE, 
                                                            MessageType = 'Text', TargetQueueId = testGroup.Id,
                                                            MessagingPlatformKey = '+11234567890', DeveloperName = 'Text_11234567890');
            insert channnel;
            
            // Create a test MessagingEndUser
            MessagingEndUser msgUser =  new MessagingEndUser(MessagingPlatformKey = '+12345678901', Name = '+12345678901',
                                                            IsOptedOut = FALSE, MessagingChannelId = channnel.Id,
                                                            MessageType = 'Text');
            insert msgUser;

            MessagingSession msgSession = new MessagingSession(MessagingEndUserId = msgUser.Id, MessagingChannelId = channnel.Id, 
                                                               Status = 'Active');
            insert msgSession;
        }
        
    }

    @isTest
    static void it_should_provide_status_of_live_agent_when_agent_is_not_available() {
        List<Boolean> liveAgentStatuses = EinsteinBotLiveAgentChecker.checkLiveAgent(new List<String>{CHAT_KEY});

        System.assert(!liveAgentStatuses.isEmpty());
        System.assertEquals(false, liveAgentStatuses[0]);
    }

    @isTest
    static void it_should_provide_status_of_live_agent_when_chatkey_does_not_exist() {
        List<Boolean> liveAgentStatuses = EinsteinBotLiveAgentChecker.checkLiveAgent(new List<String>{'asdfasdf'});

        System.assert(!liveAgentStatuses.isEmpty());
        System.assertEquals(false, liveAgentStatuses[0]);
    }

    @isTest
    static void testLiveAgentForSMS(){
        String sessionId = [SELECT Id FROM MessagingSession LIMIT 1].Id;
        List<Boolean> liveAgentStatuses = EinsteinBotLiveAgentChecker.checkLiveAgent(new List<String>{sessionId});

        System.assert(!liveAgentStatuses.isEmpty());
        System.assertEquals(false, liveAgentStatuses[0]);
    }
}