public class RepaymentCalculatorUpdate{
    /** method to Update Repayment Calculator records  **/
    
    @InvocableMethod
    public Static void updateRepaymentCalculator(List<Quote> proposals){
        
        Set<Id> quoIdSet = new Set<Id>();
        for(Quote quo : proposals){
                quoIdSet.add(quo.Id);
        }
        
        List <Repayment_Calculator__c> delRC = new List <Repayment_Calculator__c>();
        if(!proposals.isEmpty() && !quoIdSet.isEmpty() ){
           delRC = [select Id from Repayment_Calculator__c where Quote__c IN:quoIdSet];

        }
       
        if(!delRC.isEmpty() && !quoIdSet.isEmpty() ){
            try{
                delete delRC;
            }catch (DMLException e) {
                System.debug('Exception: Repayment_Calculator deleting records'+e);      
                throw e;     
            }
        }
        
       
        
        
        List<Repayment_Calculator__c> UpdateRCList = new  List<Repayment_Calculator__c>();
        Quote_Settings__c qsvalue = Quote_Settings__c.getValues('System Properties');
        Decimal lastbeginning_Balance = 0;
        Decimal lastEndingBalance = 0;
        for(Quote eachpropsal : proposals){
            if(eachpropsal.Course_Fee_Rollup__c!=0.00){
                if(String.isNotEmpty(eachpropsal.Payment_Period_In_Month__c) && Integer.valueOf(eachpropsal.Payment_Period_In_Month__c) >=1 ){
                   for(integer count = 1 ; count <= Integer.valueof(eachpropsal.Payment_Period_In_Month__c); count++){
                        Repayment_Calculator__c Calculator = new Repayment_Calculator__c();
                        Calculator.Quote__c = eachpropsal.ID;
                        //Laxman-Start- CR-02959 September 2019//
                        if(eachpropsal.Payment_Period_In_Month__c.equalsIgnoreCase('2') &&  eachpropsal.Payment_Method__c.equalsIgnoreCase('Semester') && Count==2){
                            Calculator.Payment_Date__c = eachpropsal.Next_Payment_Date__c;                            
                        }else{
                            Calculator.Payment_Date__c = eachpropsal.First_Payment_Date__c.addMonths(count-1);
                        } //Laxman-End- CR-02959 September 2019//
                        Calculator.Total_Payment__c = eachpropsal.Scheduled_Monthly_Payments_f__c;// - Calculator.Extra_Payment__c;
                                       
                        if(Count==1){
                             system.debug('Calculator.Beginning_Balance__c>>>'+eachpropsal.Remaining_Payment_Amount_f__c);    
                            Calculator.Beginning_Balance__c = eachpropsal.Remaining_Payment_Amount_f__c;                                                  
                            Calculator.Discount_Reversed__c = (Calculator.Beginning_Balance__c) * (qsvalue.Administration_Fee_Rate__c / 100) / 12;
                          system.debug('@@@@'+Calculator.Discount_Reversed__c);
                            Calculator.Principal__c = Calculator.Total_Payment__c-Calculator.Discount_Reversed__c ;
                            Calculator.Ending_Balance__c = eachpropsal.Remaining_Payment_Amount_f__c - Calculator.Principal__c;
                            lastbeginning_Balance = Calculator.Beginning_Balance__c;
                            lastEndingBalance = Calculator.Ending_Balance__c;
                            
                        } 
                        else
                        {
                            Calculator.Discount_Reversed__c = (lastEndingBalance) * (qsvalue.Administration_Fee_Rate__c / 100) / 12;
                            Calculator.Principal__c = Calculator.Total_Payment__c - Calculator.Discount_Reversed__c ;
                            Calculator.Beginning_Balance__c = lastEndingBalance;
                            Calculator.Ending_Balance__c = Calculator.Beginning_Balance__c - Calculator.Principal__c;
                            lastbeginning_Balance = Calculator.Beginning_Balance__c;
                            lastEndingBalance = Calculator.Ending_Balance__c;
                        }
                      
                        Calculator.Scheduled_Payment__c = eachpropsal.Scheduled_Monthly_Payments_f__c;
                                            
                        Calculator.Interest_Rate__c = qsvalue.Administration_Fee_Rate__c ;
                       
                        //Calculator.Extra_Payment__c = null;

                        Calculator.Sequence_No__c = count;
                        //TempBalance= Calculator.Ending_Balance__c;
                       
                       
                        
                        UpdateRCList.add(Calculator);
                        System.debug('UpdateRCList:'+UpdateRCList);
                          
                    }
              
                }
            }
           system.debug('UpdateRCList  :'+UpdateRCList);
        }
        
        if(!UpdateRCList.isEmpty()){
            try{
                insert UpdateRCList;
            }catch (DMLException e) {
                System.debug('Exception: Repayment_Calculator deleting records'+e);   
                throw e;        
            }
        }
    }
    /*
    @InvocableMethod
    public Static void updateRepaymentCalculator(List<Quote> proposals){
    }
    */
}