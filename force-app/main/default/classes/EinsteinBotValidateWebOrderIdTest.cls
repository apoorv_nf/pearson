/*
 * Author: Apoorv (Neuraflash)
 * Date: 03/09/2019
 * Description: Test Class for EinsteinBotValidateWebOrderId.
*/
@isTest
private class EinsteinBotValidateWebOrderIdTest {
    
    @isTest
    private static void testVerifyWebOrderIdFormat(){

        Test.startTest();
            List<Boolean> isValid = EinsteinBotValidateWebOrderId.verifyWebOrderIdFormat(new List<String>{'WEB70000015492854'});
        Test.stopTest();

        System.assertEquals(TRUE, isValid[0]);

    }

    @isTest
    private static void testVerifyWebOrderIdFormatNegative(){

        Test.startTest();
            List<Boolean> isValid = EinsteinBotValidateWebOrderId.verifyWebOrderIdFormat(new List<String>{'TEST70000015492854'});
        Test.stopTest();

        System.assertEquals(FALSE, isValid[0]);

    }

}