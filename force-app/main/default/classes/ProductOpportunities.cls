public class ProductOpportunities {
    @AuraEnabled
    public static List<OpportunityLineItem> getProductOpportunities(String productId){
        Map<Id, OpportunityLineItem> oLineItemsMap = new Map<Id, OpportunityLineItem>();
        List<OpportunityLineItem> oLineItemsList = [SELECT Id, OpportunityId, Opportunity.Name, Opportunity.StageName FROM OpportunityLineItem 
                                                    WHERE Opportunity.RecordType.Name = 'Global Rights Sales Opportunity'
                                                    AND PricebookEntryId in (SELECT Id FROM PricebookEntry WHERE Product2Id = :productId)];
        for(OpportunityLineItem oLT : oLineItemsList){
            if(oLineItemsMap.get(oLT.OpportunityId) == null){
                oLineItemsMap.put(oLT.OpportunityId, oLT);
            }
        }
        
        return oLineItemsMap.values();
    }
}