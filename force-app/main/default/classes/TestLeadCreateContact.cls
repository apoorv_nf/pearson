/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:         TestLeadCreateContact.cls 
   Description:  Test Class For Lead
                
   Date             Version     Author                  Tag     Summary of Changes 
   -----------      -------     -----------------       ---     ------------------------------------------------------------------------
   26/08/2015       0.1         Accenture               None    Intial draft
   08/03/2016       0.2         Abhinav                 None    Populating the mandatory fields on D2L Lead record.
   28/09/2016       0.3         Supriyam                None    Class updation for coverage
  -------------------------------------------------------------------------------------------------------------------------------------------------------- */
@isTest(seealldata =True)
private class TestLeadCreateContact
{  
  static testMethod void convertLeadWithSponsorEmailTest()  
  {
      UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' errora
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
      User usr;
      usr = new User(LastName = 'ETL Integration', alias = 'happy', Email = 'h@pearson.com', Username='test1234'+Math.random()+'@pearson.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
                     ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK',
                     Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science',
                       // UserRoleId=[select Id from UserRole Limit 1].Id,License_Pools__c='Test user');
                     UserRoleId=tempUserRole.Id,License_Pools__c='Test user');
      insert usr;
       /* Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c =true,Disable_Process_Builder__c =true,Disable_Validation_Rules__c =true);
        insert byp;
       Bypass_Settings__c byp1 = [select id,Disable_Triggers__c from Bypass_Settings__c where SetupOwnerId=:usr.Id Limit 1];
        byp1.Disable_Triggers__c = false;
        byp1.Disable_Validation_Rules__c = true;
        byp1.Disable_Process_Builder__c =true;
        upsert byp1;*/
     System.runAs(usr){  
    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c =true,Disable_Process_Builder__c =true,Disable_Validation_Rules__c =true);
    insert byp;
    List<Lead> testLeads = TestDataFactory.createLead(1,'D2L');  
    Lead testLead = testLeads[0];
    RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
    // Set sponsor email
    testLead.Sponsor_Type__c = 'Other';
    //Abhinav : 3/2016 - Populating the mandatory fields on Lead record.
    testLead.Sponsor_Salutation__c='Mr.';
    testLead.Sponsor_Name__c  ='Test';
    testLead.Sponsor_Sponsor__c = 'TestclassSponsor';   
    testLead.Sponsor_Preferred_Method_of_Contact__c = 'Email';
    testLead.Sponsor_Mobile__c= '87265873';
    testLead.Sponsor_First_Language__c = 'English';
    testLead.Sponsor_Address_City__c = 'Bangalore';
    testLead.Sponsor_Address_Country__c = 'India';
    testLead.Sponsor_Address_Street__c = 'Civil Lines';
    testLead.Sponsor_Address_ZIP_Postal_Code__c = '211011';
    testLead.Sponsor_Email__c = 'test@example.com';
    testLead.LeadSource  = 'Online/Web';
    testLead.phone = '9384756456';
    testLead.Home_Phone__c = '8192829282';
    testLead.Primary_Phone__c = '8394857565';
    testLead.International_Student__c ='Yes';     
    testLead.Interview_Attendance__c = True;
   // testLead.Identification_Number__c='Test1234';     
    //testLead.Passport_Number__c='Test1234';     
         
         
    Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
    acc.ShippingStreet = 'TestStreet';
    acc.ShippingCity = 'Vns';
    acc.ShippingState = 'Delhi';
    acc.ShippingPostalCode = '234543';
    acc.ShippingCountry = 'India';
    acc.recordtypeId=OrgAcc.Id ;
    acc.Academic_Achievement__c='High';
    acc.Market2__c = 'UK';
    acc.Organisation_Type__c='School';
    acc.IsCreatedFromLead__c = true;
    acc.Abbreviated_Name__c='TestName123568456';
    insert acc;
    testLead.Institution_Organisation__c =acc.Id;
    testLead.Market__c = 'AU';
    Database.insert(testLead);
     
    Test.StartTest();   
    Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
    mapWithLead.put(testLead.Id,testLead);
    Database.LeadConvert lc = new database.LeadConvert();  
    lc.setLeadId(testLead.Id); 
   
    lc.setDoNotCreateOpportunity(false);
    lc.setConvertedStatus('Qualified');
        
    PS_lead_createContact.processedLead.clear();
       //  try{
    Database.LeadConvertResult lcr = Database.ConvertLead(lc);
        /* }
         catch(Exception ex)
         {
             System.assert(true, 'National ID number is compulsory when student/sponsor is not International. Passport number is compulsory when student/sponsor is International. Please verify contact information.');
         }*/
   System.assert(lcr.isSuccess(), 'The lead was not converted successfully');
    List<Lead> lstWithLead = [select isConverted,City,Country,PostalCode,POPI_Marketing_Opt_In__c,Marketing_Opt_In__c,Street,Other_Phone__c,State,Email,Sponsor_Salutation__c,Sponsor_Sponsor__c,Sponsor_Name__c,Sponsor_Mobile__c,Sponsor_Home_Phone__c,Sponsor_First_Language__c,Sponsor_Phone__c,Sponsor_Email__c,Sponsor_Preferred_Method_of_Contact__c,Sponsor_Address_State_Province__c,Sponsor_Address_City__c,Sponsor_Address_Country__c,Sponsor_Address_ZIP_Postal_Code__c,Sponsor_Address_Street__c,Sponsor_Type__c,Campaign__c,ownerid,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId,Institution_Organisation__c from lead where id IN: mapWithLead.keyset()];
    PS_lead_createContact.createAccountContact(lstWithLead,mapWithLead);     
    Test.StopTest();
  }
  } 
 
    static testMethod void myCreateContactTest() 
  {
       UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
      User usr;
      usr = new User(LastName = 'ETL Integration', alias = 'happy', Email = 'h@pearson.com', License_Pools__c='Test user', Username='test1234'+Math.random()+'@pearson.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
    insert usr;
      System.runAs(usr){ 
    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c =true,Disable_Process_Builder__c =true,Disable_Validation_Rules__c =true);
    insert byp; 
    list<Lead> leadList = TestDataFactory.createLead(1,'D2L');                  
    RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
    Account acc1 = new Account (Name='test123',geography__c='growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
    acc1.ShippingStreet = 'TestStreet123';
    acc1.ShippingCity = 'Vns123';
    acc1.ShippingState = 'Delhi';
    acc1.ShippingPostalCode = '23454323';
    acc1.ShippingCountry = 'India'; 
    acc1.recordtypeId=OrgAcc.Id ;
    acc1.Academic_Achievement__c='High';
    acc1.Market2__c = 'UK';
    acc1.Organisation_Type__c='School';    
    acc1.IsCreatedFromLead__c = true;
    acc1.Abbreviated_Name__c='TestName12356890';
    insert acc1;
    
    for(lead l : leadList )
    {
      l.Institution_Organisation__c =acc1.Id;
      l.Market__c = 'AU';      
    }
    
    Test.Starttest();   
    Insert leadList;
    Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
    for(lead l : leadList )
    {
        mapWithLead.put(l.Id,l); 
    } 
    for(Lead newLead : leadList)
    {
      if(newLead.Sponsor_Type__c !='Self')
      {
        Database.LeadConvert lc = new database.LeadConvert();  
        lc.setLeadId(newLead.id);    
        lc.setDoNotCreateOpportunity(false);
        lc.setConvertedStatus('Qualified');
        PS_lead_createContact.processedLead.clear();
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess(), 'The lead was not converted successfully');
      }
    }
    List<Lead> lstWithLead = [select isConverted,Marketing_Opt_In__c,City,Country,PostalCode,Street,Other_Phone__c,State,Email,Sponsor_Salutation__c,Sponsor_Sponsor__c,Sponsor_Name__c,Sponsor_Mobile__c,Sponsor_Home_Phone__c,Sponsor_Phone__c,Sponsor_Email__c,Sponsor_Preferred_Method_of_Contact__c,Sponsor_Address_State_Province__c,Sponsor_Address_City__c,Sponsor_Address_Country__c,Sponsor_Address_ZIP_Postal_Code__c,Sponsor_Address_Street__c,Sponsor_Type__c,Campaign__c,ownerid,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId,Institution_Organisation__c from lead where id IN: mapWithLead.keyset()];
    PS_lead_createContact.createAccountContact(lstWithLead,mapWithLead); 
    Test.Stoptest();
        
   /* List<Lead> testLeads1 = TestDataFactory.createLead(1,'B2B');
    testLeads1[0].Institution_Organisation__c=acc1.id;
    testLeads1[0].Role__c = 'Learner';
    testLeads1[0].Sponsor_Type__c = 'Self';*/
 //   Insert testLeads1;
        
   /*    
       
     Database.LeadConvert lc1 = new database.LeadConvert();  
        lc1.setLeadId(testLeads1[0].id); 
        lc1.setDoNotCreateOpportunity(False);   
        lc1.setConvertedStatus('Qualified');
        PS_lead_createContact.processedLead.clear();      
        Database.LeadConvertResult lcr = Database.convertLead(lc1); 
        System.assert(lcr.isSuccess(), 'The lead was not converted successfully');
    //test
    Map<Id,Lead> mapWithLead1 = new Map<Id,Lead>();
    for(lead l : testLeads1 )
    {
        mapWithLead1.put(l.Id,l); 
    } 
    List<Lead> lstWithLead1 = [select Sub_Discipline_of_Interest__c,Discipline_Of_Interest_Multi_Select__c,Additional_Responsibilities__c,Other_Role_Detail__c,Role_Detail__c,Role__c,Market__c,isConverted,City,Country,PostalCode,Street,Identification_Number__c,Other_Phone__c,State,Email,Sponsor_Salutation__c,Sponsor_Sponsor__c,Sponsor_Name__c,Sponsor_Mobile__c,Sponsor_Home_Phone__c,Sponsor_Phone__c,Sponsor_Email__c,Sponsor_Preferred_Method_of_Contact__c,Sponsor_Address_State_Province__c,Sponsor_Address_City__c,Sponsor_Address_Country__c,Sponsor_Address_ZIP_Postal_Code__c,Sponsor_Address_Street__c,Sponsor_Type__c,Campaign__c,ownerid,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId,Institution_Organisation__c from lead where id IN: mapWithLead1.keyset()];
    PS_lead_createContact.createAccountContact(lstWithLead1,mapWithLead1);     
    ContactUtils cu = new ContactUtils();
   // cu.createAccountContact(testLeads1[0],mapWithLead1); 
   cu.createAccountContact(lstWithLead1[0],mapWithLead1);      
       */
  }          
  }
  static testMethod void myCreateContactTest_1() 
  {
       UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
      User usr;
      usr = new User(LastName = 'ETL Integration', alias = 'happy', Email = 'h@pearson.com', License_Pools__c='Test user', Username='test1234'+Math.random()+'@pearson.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
   insert usr;
      System.runAs(usr){ 
    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c =true,Disable_Process_Builder__c =true,Disable_Validation_Rules__c =true);
    insert byp;
    RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
    Account acc1 = new Account (Name='test',geography__c='growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
    acc1.ShippingStreet = 'TestStreet';
    acc1.ShippingCity = 'Vns';
    acc1.ShippingState = 'Delhi';
    acc1.ShippingPostalCode = '234543';
    acc1.ShippingCountry = 'India'; 
    acc1.recordtypeId=OrgAcc.Id ;
    acc1.Academic_Achievement__c='High';
    acc1.Market2__c = 'UK';
    acc1.Organisation_Type__c='School';    
    acc1.IsCreatedFromLead__c = true;
    acc1.Abbreviated_Name__c='TestName123568478';
    insert acc1;
    list<Lead> leadList = TestDataFactory.createLead(1,'D2L'); 
    list<lead>leadLst1 = new list<lead>();
   
        
    for(lead l : leadList )
    {
         l.phone = '9384756453';
    l.Home_Phone__c = '8192829283';
    l.Primary_Phone__c = '3192829282';
      l.Institution_Organisation__c =acc1.Id;
      l.Market__c = 'AU'; 
         l.International_Student__c ='Yes';     
    l.Interview_Attendance__c = True;
   // testLead.Identification_Number__c='Test1234';     
   // l.Passport_Number__c='Test1234';     
      leadLst1 .add(l);
    }                
    Insert leadLst1;  
    PS_Lead_UpdateStudentId.beforelead1(leadLst1);
    Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
    for(lead l : leadLst1)
    {
        mapWithLead.put(l.Id,l); 
    } 
    Test.Starttest();   
        
    for(Lead newLead : leadList)
    {
      if(newLead.Sponsor_Type__c !='Parent')
      {
        Database.LeadConvert lc1 = new database.LeadConvert();     
        lc1.setLeadId(newLead.id);    
        lc1.setDoNotCreateOpportunity(false);
        lc1.setConvertedStatus('Qualified');
        PS_lead_createContact.processedLead.clear();
        Database.LeadConvertResult lcr = Database.convertLead(lc1);
        System.assert(lcr.isSuccess(), 'The lead was not converted successfully');
      }
    }
    List<Lead> lstWithLead = [select isConverted,City,Country,POPI_Marketing_Opt_In__c,Marketing_Opt_In__c,PostalCode,Street,Other_Phone__c,State,Email,Sponsor_Salutation__c,Sponsor_Sponsor__c,Sponsor_Name__c,Sponsor_Mobile__c,Sponsor_Home_Phone__c,Sponsor_Phone__c,Sponsor_Email__c,Sponsor_Preferred_Method_of_Contact__c,Sponsor_Address_State_Province__c,Sponsor_Address_City__c,Sponsor_Address_Country__c,Sponsor_Address_ZIP_Postal_Code__c,Sponsor_Address_Street__c,Sponsor_Type__c,Campaign__c,ownerid,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId,Institution_Organisation__c from lead where id IN: mapWithLead.keyset()];
    PS_lead_createContact.createAccountContact(lstWithLead,mapWithLead);  
    Test.Stoptest();    
  }
  }
   
  static testMethod void convertLeadWithSponsorEmailTest2()  
  {
      UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
      User usr;
      usr = new User(LastName = 'ETL Integration', alias = 'happy', Email = 'h@pearson.com',License_Pools__c='Test user', Username='test1234'+Math.random()+'@pearson.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Schools', Business_Unit__c = 'Schools', UserRoleId=tempUserRole.Id);
     insert usr;
      System.runAs(usr){
     Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c =true,Disable_Process_Builder__c =true,Disable_Validation_Rules__c =true);
    insert byp;  
    List<Lead> testLeads = TestDataFactory.createLead(1,' B2B');  
    Lead testLead = testLeads[0];
    RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'School'];
    // Set sponsor email
    testLead.Sponsor_Type__c = 'Other';
    //Abhinav : 3/2016 - Populating the mandatory fields on Lead record.
    testLead.Sponsor_Salutation__c='Mr.';
    testLead.Sponsor_Name__c  ='Test';
    testLead.Sponsor_Sponsor__c = 'TestclassSponsor';   
    testLead.Sponsor_Preferred_Method_of_Contact__c = 'Email';
    testLead.Sponsor_Mobile__c= '87265873';
    testLead.Sponsor_First_Language__c = 'English';
    testLead.Sponsor_Address_City__c = 'Bangalore';
    testLead.Sponsor_Address_Country__c = 'India';
    testLead.Sponsor_Address_Street__c = 'Civil Lines';
    testLead.Sponsor_Address_ZIP_Postal_Code__c = '211011';
    testLead.Sponsor_Email__c = 'test@example.com';
    testLead.LeadSource  = 'Online/Web';
    testlead.city = 'chennai';
    testlead.postalcode = '600119';
    testlead.phone = '9989876767';
    testlead.Home_Phone__c = '8374657465';
    testlead.mobilePhone = '9828384858';
    testlead.Primary_Phone__c = '827363635444';
    testlead.Email = 'test12@example2.com';
    testLead.International_Student__c ='Yes';     
    testLead.Interview_Attendance__c = True;
   // testLead.Identification_Number__c='Test1234';     
   //  testLead.Passport_Number__c='Test1234';         

         
    Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
    acc.ShippingStreet = 'TestStreet';
    acc.ShippingCity = 'Vns';
    acc.ShippingState = 'Delhi';
    acc.ShippingPostalCode = '234543';
    acc.ShippingCountry = 'India';
    acc.recordtypeId=OrgAcc.Id ;
    acc.Academic_Achievement__c='High';
    acc.Market2__c = 'UK';
    acc.Organisation_Type__c='School';
    acc.IsCreatedFromLead__c = true;
    acc.Abbreviated_Name__c='TestName123568';
    insert acc;
    testLead.Institution_Organisation__c =acc.Id;
    testLead.Market__c = 'UK';
    testLead.Role__c = 'Employee';
    Database.insert(testLead);
    ContactUtils createAccCon = new ContactUtils();
    AccountContact__c AccountContact = new AccountContact__c(); 
    Test.StartTest();   
    Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
    mapWithLead.put(testLead.Id,testLead);
    Database.LeadConvert lc = new database.LeadConvert();  
    lc.setLeadId(testLead.Id); 
   
    lc.setDoNotCreateOpportunity(false);
    lc.setConvertedStatus('Qualified');
        
    PS_lead_createContact.processedLead.clear();
     //  try{
    Database.LeadConvertResult lcr = Database.ConvertLead(lc);
       /*  }
         catch(Exception ex)
         {
             System.assert(true, 'National ID number is compulsory when student/sponsor is not International. Passport number is compulsory when student/sponsor is International. Please verify contact information.');
         }*/
   System.assert(lcr.isSuccess(), 'The lead was not converted successfully');
    List<Lead> lstWithLead = [select isConverted,City,phone,POPI_Marketing_Opt_In__c,Marketing_Opt_In__c,Home_Phone__c,Primary_Phone__c,mobilePhone,Market__c,Country,PostalCode,Street,Other_Phone__c,State,Email,Sponsor_Salutation__c,Sponsor_Sponsor__c,Sponsor_Name__c,Sponsor_Mobile__c,Sponsor_Home_Phone__c,Sponsor_First_Language__c,Sponsor_Phone__c,Sponsor_Email__c,Sponsor_Preferred_Method_of_Contact__c,Sponsor_Address_State_Province__c,Sponsor_Address_City__c,Sponsor_Address_Country__c,Sponsor_Address_ZIP_Postal_Code__c,Sponsor_Address_Street__c,Sponsor_Type__c,Campaign__c,ownerid,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId,Institution_Organisation__c from lead where id IN: mapWithLead.keyset()];
    PS_lead_createContact.createAccountContact(lstWithLead,mapWithLead);     
    Test.StopTest();
  }
  } 
   
    static testMethod void myCreateContactTestEnterprise() 
  {
       UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
      User usr;
      usr = new User(LastName = 'ETL Integration', alias = 'happy',License_Pools__c='Test user', Email = 'h@pearson.com', Username='test1234'+Math.random()+'@pearson.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
   insert usr;
      System.runAs(usr){
    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c =true,Disable_Process_Builder__c =true,Disable_Validation_Rules__c =true);
    insert byp;  
    list<Lead> leadList = TestDataFactory.createLead(1,'Enterprise');                  
    RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
    Account acc1 = new Account (Name='test123',geography__c='growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
    acc1.ShippingStreet = 'TestStreet123';
    acc1.ShippingCity = 'Vns123';
    acc1.ShippingState = 'Delhi';
    acc1.ShippingPostalCode = '23454323';
    acc1.ShippingCountry = 'India'; 
    acc1.recordtypeId=OrgAcc.Id ;
    acc1.Academic_Achievement__c='High';
    acc1.Market2__c = 'UK';
    acc1.Organisation_Type__c='School';    
    acc1.IsCreatedFromLead__c = true;
    acc1.Abbreviated_Name__c='TestName123568';
    insert acc1;
    
    for(lead l : leadList )
    {
      l.Institution_Organisation__c =acc1.Id;
      l.Market__c = 'AU';      
    }
    
    Test.Starttest();   
    Insert leadList;
    Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
    for(lead l : leadList )
    {
        mapWithLead.put(l.Id,l); 
    } 
    for(Lead newLead : leadList)
    {
      if(newLead.Sponsor_Type__c !='Self')
      {
        Database.LeadConvert lc = new database.LeadConvert();  
        lc.setLeadId(newLead.id);    
        lc.setDoNotCreateOpportunity(false);  
        lc.setConvertedStatus('Qualified');
        PS_lead_createContact.processedLead.clear();
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess(), 'The lead was not converted successfully');
      }
    }
    List<Lead> lstWithLead = [select isConverted,City,Country,Marketing_Opt_In__c,PostalCode,Street,Other_Phone__c,State,Email,Sponsor_Salutation__c,Sponsor_Sponsor__c,Sponsor_Name__c,Sponsor_Mobile__c,Sponsor_Home_Phone__c,Sponsor_Phone__c,Sponsor_Email__c,Sponsor_Preferred_Method_of_Contact__c,Sponsor_Address_State_Province__c,Sponsor_Address_City__c,Sponsor_Address_Country__c,Sponsor_Address_ZIP_Postal_Code__c,Sponsor_Address_Street__c,Sponsor_Type__c,Campaign__c,ownerid,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId,Institution_Organisation__c from lead where id IN: mapWithLead.keyset()];
    PS_lead_createContact.createAccountContact(lstWithLead,mapWithLead); 
    Test.Stoptest();
        
   /* List<Lead> testLeads1 = TestDataFactory.createLead(1,'B2B');
    testLeads1[0].Institution_Organisation__c=acc1.id;
    testLeads1[0].Role__c = 'Learner';
    testLeads1[0].Sponsor_Type__c = 'Self';*/
 //   Insert testLeads1;
        
   /*    
       
     Database.LeadConvert lc1 = new database.LeadConvert();  
        lc1.setLeadId(testLeads1[0].id); 
        lc1.setDoNotCreateOpportunity(False);   
        lc1.setConvertedStatus('Qualified');
        PS_lead_createContact.processedLead.clear();      
        Database.LeadConvertResult lcr = Database.convertLead(lc1); 
        System.assert(lcr.isSuccess(), 'The lead was not converted successfully');
    //test
    Map<Id,Lead> mapWithLead1 = new Map<Id,Lead>();
    for(lead l : testLeads1 )
    {
        mapWithLead1.put(l.Id,l); 
    } 
    List<Lead> lstWithLead1 = [select Sub_Discipline_of_Interest__c,Discipline_Of_Interest_Multi_Select__c,Additional_Responsibilities__c,Other_Role_Detail__c,Role_Detail__c,Role__c,Market__c,isConverted,City,Country,PostalCode,Street,Passport_Number__c,Identification_Number__c,Other_Phone__c,State,Email,Sponsor_Salutation__c,Sponsor_Sponsor__c,Sponsor_Name__c,Sponsor_Mobile__c,Sponsor_Home_Phone__c,Sponsor_Phone__c,Sponsor_Email__c,Sponsor_Preferred_Method_of_Contact__c,Sponsor_Address_State_Province__c,Sponsor_Address_City__c,Sponsor_Address_Country__c,Sponsor_Address_ZIP_Postal_Code__c,Sponsor_Address_Street__c,Sponsor_Type__c,Campaign__c,ownerid,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId,Institution_Organisation__c from lead where id IN: mapWithLead1.keyset()];
    PS_lead_createContact.createAccountContact(lstWithLead1,mapWithLead1);     
    ContactUtils cu = new ContactUtils();
   // cu.createAccountContact(testLeads1[0],mapWithLead1); 
   cu.createAccountContact(lstWithLead1[0],mapWithLead1);      
       */
  }          
  }
    
    static testMethod void convertLeadEnterprise2()  
  {
      UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
      User usr;
      usr = new User(LastName = 'ETL Integration', License_Pools__c = 'Test User', alias = 'happy', Email = 'h@pearson.com', Username='test1234'+Math.random()+'@pearson.com',
                     CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', 
                     LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
   insert usr;
      /*
      List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
       insert usrLst;*/
      System.runAs(usr){ 
      Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c =true,Disable_Process_Builder__c =true,Disable_Validation_Rules__c =true);
    insert byp; 
    List<Lead> testLeads = TestDataFactory.createLead(1,'Enterprise');  
    Lead testLead = testLeads[0];
     Map<String, Schema.SObjectType> sObjectMap = Schema.getGlobalDescribe() ;
     Schema.SObjectType leadrt = sObjectMap.get('Lead') ; // getting Sobject Type
    Schema.DescribeSObjectResult ldresSchema = leadrt.getDescribe() ;
         Map<String,Schema.RecordTypeInfo> ldrecordTypeInfo = ldresSchema.getRecordTypeInfosByName();
       Id EnterpriseleadrtId=ldrecordTypeInfo.get('Enterprise Lead').getRecordTypeId();   
    RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
    // Set sponsor email
   /* testLead.Sponsor_Type__c = 'Other';
    //Abhinav : 3/2016 - Populating the mandatory fields on Lead record.
    testLead.Sponsor_Salutation__c='Mr.';
    testLead.Sponsor_Name__c  ='Test';
    testLead.Sponsor_Sponsor__c = 'TestclassSponsor';   
    testLead.Sponsor_Preferred_Method_of_Contact__c = 'Email';
    testLead.Sponsor_Mobile__c= '87265873';
    testLead.Sponsor_First_Language__c = 'English';
    testLead.Sponsor_Address_City__c = 'Bangalore';
    testLead.Sponsor_Address_Country__c = 'India';
    testLead.Sponsor_Address_Street__c = 'Civil Lines';
    testLead.Sponsor_Address_ZIP_Postal_Code__c = '211011';
    testLead.Sponsor_Email__c = 'test@example.com';
    testLead.LeadSource  = 'Online/Web';
    testLead.phone = '9384756456';
    testLead.Home_Phone__c = '8192829282';
    testLead.Primary_Phone__c = '8394857565';
    testLead.International_Student__c ='Yes';     
    testLead.Interview_Attendance__c = True;
   // testLead.Identification_Number__c='Test1234';     
    testLead.Passport_Number__c='Test1234';   */  
    testLead.RecordTypeId=EnterpriseleadrtId;    
    
    Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
    acc.ShippingStreet = 'TestStreet';
    acc.ShippingCity = 'Vns';
    acc.ShippingState = 'Delhi';
    acc.ShippingPostalCode = '234543';
    acc.ShippingCountry = 'India';
    acc.recordtypeId=OrgAcc.Id ;
    acc.Academic_Achievement__c='High';
    acc.Market2__c = 'UK';
    acc.Organisation_Type__c='School';
    acc.IsCreatedFromLead__c = true;
    acc.Abbreviated_Name__c='TestName123568123';
    insert acc;
    testLead.Account_Lookup__c=acc.Id;
    testLead.Company=acc.name;     
  //testLead.Institution_Organisation__c =acc.Id;
  testLead.Institution_Organisation__c =null;
 testLead.Market__c = 'AU';
    testLead.Role__c='Learner';
    testLead.Role_Detail__c= 'Student';
    Database.insert(testLead);
    
    List<Contact> lstContact = TestDataFactory.createContact(1);
    insert lstContact;
    
    
    AccountContact__c AccntContt = new AccountContact__c(); 
    AccntContt.Account__c = acc.id;
    AccntContt.Contact__c = lstContact[0].id;
    AccntContt.AccountRole__c='Learner';
    AccntContt.Role_Detail__c= 'Student';
    insert AccntContt;
    
        
    Test.StartTest();   
    Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
    mapWithLead.put(testLead.Id,testLead);
    Database.LeadConvert lc = new database.LeadConvert();  
    AccountContact__c acc12 = new AccountContact__c();
      acc12.Contact__c = lc.contactid;
         
    lc.setLeadId(testLead.Id); 
   
    lc.setDoNotCreateOpportunity(false);
    lc.setConvertedStatus('Qualified');
        
    PS_lead_createContact.processedLead.clear();
       //  try{
    Database.LeadConvertResult lcr = Database.ConvertLead(lc);
        /* }
         catch(Exception ex)
         {
             System.assert(true, 'National ID number is compulsory when student/sponsor is not International. Passport number is compulsory when student/sponsor is International. Please verify contact information.');
         }*/
   System.assert(lcr.isSuccess(), 'The lead was not converted successfully');
    List<Lead> lstWithLead = [select isConverted,City,Country,PostalCode,Street,Role__c,POPI_Marketing_Opt_In__c,Marketing_Opt_In__c,Role_Detail__c,Other_Phone__c,State,Email,Sponsor_Salutation__c,Sponsor_Sponsor__c,Sponsor_Name__c,Sponsor_Mobile__c,Sponsor_Home_Phone__c,Sponsor_First_Language__c,Sponsor_Phone__c,Sponsor_Email__c,Sponsor_Preferred_Method_of_Contact__c,Sponsor_Address_State_Province__c,Sponsor_Address_City__c,Sponsor_Address_Country__c,Sponsor_Address_ZIP_Postal_Code__c,Sponsor_Address_Street__c,Sponsor_Type__c,Campaign__c,ownerid,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId,Institution_Organisation__c from lead where id IN: mapWithLead.keyset()];
    PS_lead_createContact.createAccountContact(lstWithLead,mapWithLead);     
    Test.StopTest();
  }
  } 
  
  static testMethod void convertLeadWebrequest2()  
  {
      UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
      User usr;
      usr = new User(LastName = 'ETL Integration', alias = 'happy',License_Pools__c='Test user', Email = 'h@pearson.com', Username='test1234'+Math.random()+'@pearson.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
      insert usr;
       System.runAs(usr){  
      Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c =true,Disable_Process_Builder__c =true,Disable_Validation_Rules__c =true);
      insert byp; 
     //System.runAs(usr){  
    List<Lead> testLeads = TestDataFactory.createLead(1,'Enterprise');  
    Lead testLead = testLeads[0];
     Map<String, Schema.SObjectType> sObjectMap = Schema.getGlobalDescribe() ;
     Schema.SObjectType leadrt = sObjectMap.get('Lead') ; // getting Sobject Type
    Schema.DescribeSObjectResult ldresSchema = leadrt.getDescribe() ;
         Map<String,Schema.RecordTypeInfo> ldrecordTypeInfo = ldresSchema.getRecordTypeInfosByName();
       Id WebreqleadrtId=ldrecordTypeInfo.get('Web Request').getRecordTypeId();   
    RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
    // Set sponsor email
   /* testLead.Sponsor_Type__c = 'Other';
    //Abhinav : 3/2016 - Populating the mandatory fields on Lead record.
    testLead.Sponsor_Salutation__c='Mr.';
    testLead.Sponsor_Name__c  ='Test';
    testLead.Sponsor_Sponsor__c = 'TestclassSponsor';   
    testLead.Sponsor_Preferred_Method_of_Contact__c = 'Email';
    testLead.Sponsor_Mobile__c= '87265873';
    testLead.Sponsor_First_Language__c = 'English';
    testLead.Sponsor_Address_City__c = 'Bangalore';
    testLead.Sponsor_Address_Country__c = 'India';
    testLead.Sponsor_Address_Street__c = 'Civil Lines';
    testLead.Sponsor_Address_ZIP_Postal_Code__c = '211011';
    testLead.Sponsor_Email__c = 'test@example.com';
    testLead.LeadSource  = 'Online/Web';
    testLead.phone = '9384756456';
    testLead.Home_Phone__c = '8192829282';
    testLead.Primary_Phone__c = '8394857565';
    testLead.International_Student__c ='Yes';     
    testLead.Interview_Attendance__c = True;
   // testLead.Identification_Number__c='Test1234';     
    testLead.Passport_Number__c='Test1234';   */  
    testLead.RecordTypeId=WebreqleadrtId;    
      
    Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
    acc.ShippingStreet = 'TestStreet';
    acc.ShippingCity = 'Vns';
    acc.ShippingState = 'Delhi';
    acc.ShippingPostalCode = '234543';
    acc.ShippingCountry = 'India';
    acc.recordtypeId=OrgAcc.Id ;
    acc.Academic_Achievement__c='High';
    acc.Market2__c = 'UK';
    acc.Organisation_Type__c='School';
    acc.IsCreatedFromLead__c = true;
    acc.Abbreviated_Name__c='TestName123568123';
    insert acc;
    testLead.Account_Lookup__c=acc.Id;
    testLead.Company=acc.name;     
    testLead.Institution_Organisation__c =acc.Id;
    testLead.Market__c = 'AU';
    testLead.Role__c='Learner';
    testLead.Role_Detail__c= 'Student';
    Database.insert(testLead);
    
    List<Contact> lstContact = TestDataFactory.createContact(1);
    insert lstContact;
    
    
    AccountContact__c AccntContt = new AccountContact__c(); 
    AccntContt.Account__c = acc.id;
    AccntContt.Contact__c = lstContact[0].id;
    AccntContt.AccountRole__c='Learner';
    AccntContt.Role_Detail__c= 'Student';
    insert AccntContt;
    
        
    Test.StartTest();   
    Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
    mapWithLead.put(testLead.Id,testLead);
    Database.LeadConvert lc = new database.LeadConvert();  
    AccountContact__c acc12 = new AccountContact__c();
    acc12.Contact__c = lc.contactid;
         
    lc.setLeadId(testLead.Id); 
   
    lc.setDoNotCreateOpportunity(false);
    lc.setConvertedStatus('Validated');
        
    PS_lead_createContact.processedLead.clear();
       //  try{
    Database.LeadConvertResult lcr = Database.ConvertLead(lc);
        /* }
         catch(Exception ex)
         {
             System.assert(true, 'National ID number is compulsory when student/sponsor is not International. Passport number is compulsory when student/sponsor is International. Please verify contact information.');
         }*/
   System.assert(lcr.isSuccess(), 'The lead was not converted successfully');
    List<Lead> lstWithLead = [select isConverted,City,Country,POPI_Marketing_Opt_In__c,Marketing_Opt_In__c,PostalCode,Street,Role__c,Role_Detail__c,Other_Phone__c,State,Email,Sponsor_Salutation__c,Sponsor_Sponsor__c,Sponsor_Name__c,Sponsor_Mobile__c,Sponsor_Home_Phone__c,Sponsor_First_Language__c,Sponsor_Phone__c,Sponsor_Email__c,Sponsor_Preferred_Method_of_Contact__c,Sponsor_Address_State_Province__c,Sponsor_Address_City__c,Sponsor_Address_Country__c,Sponsor_Address_ZIP_Postal_Code__c,Sponsor_Address_Street__c,Sponsor_Type__c,Campaign__c,ownerid,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId,Institution_Organisation__c from lead where id IN: mapWithLead.keyset()];
    PS_lead_createContact.createAccountContact(lstWithLead,mapWithLead);     
    Test.StopTest();
  }
  } 
    
}