/************************************************************************************************************
* Apex Class Name   : StandardOrderTriggerHandler.cls 
* Version           : 1.0 
* Created Date      : 12 May 2015
* Function          : Handler class for StandarOrder Object Trigger
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Davi Borges              01/Jul/2015            Removal of delete related operation, removal of Address 
                                                  replication to address override that is no longer necessary, 
                                                  since it is performed by web service RetrieveOneCRMHandler
-------------------------------------------------------------------------------------------------------------
  Davi Borges              10/Jul/2015            1) Introduction to validation to prevent changes from inconrect
                                                     people
                                                  2) Replication of shippingToContactID to Contact_c to allow 
                                                  use of related list in contact. 
-------------------------------------------------------------------------------------------------------------
  Karan Khanna              03/Dec/2015           Addition of PS_SendEmailAlertsForOrder for sending email alerts and PS_UpdateOrderItemStatus for updating OrderItems. And PS_UpdateOrderItemStatus for updating OrderItem Status depending on change in Order. 
-------------------------------------------------------------------------------------------------------------
  Dinesh Acharya            12/Dec/2015           Addition of extra logic to check if SalesPerson field is populated then
                                                  tagging Geo, Market, Business Unit, Line of business values Else
                                                  populate CompanyAuthorisedBy user values for Geo, Market, Business Unit, 
                                                  Line of business. This is done so when user is changed to a different market or 
                                                  BU or LOB it won't impact Order history.   
--------------------------------------------------------------------------------------------------------------
  Kamal Chandran            21/Dec/2015           Commented Dinesh Acharya's code and called 'PS_UserFieldUtill' Class to
                                                  auto populate Geo, Market, Business Unit, Line Of Business based on sales user
                                                  or logged in user            
                                                  -------------------------------------------------------------------------------------------------------------
Rony Joseph              10/Nov/2015           Addition of Error Handling Code.
-------------------------------------------------------------------------------------------------------------
Rony Joseph              03/Dec/2015          Fix for Company Authorising issue.
-------------------------------------------------------------------------------------------------------------
Rony Joseph              12/Dec/2015          Fix for Removal of Special Characters.
-------------------------------------------------------------------------------------------------------------
Rony Joseph              29/dec/2015          Fix for special character, including #
-------------------------------------------------------------------------------------------------------------
Karan Khanna              08/Jan/2016         Addition of PS_PopulateFieldsOnOrder for populating Pricebook
-------------------------------------------------------------------------------------------------------------
Karan Khanna              20/Jan/2016         Addition a condition to check if any of the Market, BU or LOB is null then only stamp it with Salesperson or current user field's values
-------------------------------------------------------------------------------------------------------------
Davi Borges               30/Jan/2016         Throw exception onAfterUpdateOperation
-------------------------------------------------------------------------------------------------------------
Rony Joseph               03/Mar/2016         Defect fix for D-4488.
-------------------------------------------------------------------------------------------------------------
Gregory Hardin            13/May/2016         Check Order Market for value before setting Shipping_address_formatted__c
-------------------------------------------------------------------------------------------------------------
Rony Joseph               08/Sep/2016         Added check for Contact field value fixing.
---------------------------------------------------------------------------------------------------------
Chaitra                   14/sep/2016         Modified the null check as revenueOrder Creation was failling
---------------------------------------------------------------------------------------------------------
Abhishek Goel             06/Apr/2018          CR-01868-Req-14: Create new field on User and Order objects
--------------------------------------------------------------------------------------------------------------
Asha G                    11/04/2018          CR-2023 Req 3 :Added new condition for order line item status --CR-2023 Req 3 and 5
************************************************************************************************************/

public without sharing class StdOrderTriggerHandler
{

    //Added by Abhishek
    public String userDefaultERPType;
    public String userMarket; //TEPNA: 4th May - Manimala -  Added Market validation to retrict US & CA for setting ERP Order Type value 
    // EXECUTE BEFORE INSERT LOGIC
    //
    public void OnBeforeInsert(List<Order> newOrders)
    {
        List<Order> lstWithSalesRepOrder = new List<Order>();
        List<Order> lstWithNonSalesRepOrder = new List<Order>();
        List<User> userfields = [Select Market__c,Default_ERP_Order_Type__c From User Where Id = :UserInfo.getUserId()];
        String userDefaultERPType = userfields[0].Default_ERP_Order_Type__c; //added by abhishek
        String userMarket = userfields[0].Market__c;//TEPNA: 4th May - Manimala -  Added Market validation to retrict US & CA for setting ERP Order Type value 
       for(Order ordr: newOrders)
        {   
        //TEPNA: 4th May - Manimala -  Added Market validation to retrict US & CA for setting ERP Order Type value 
            if((ordr.ERP_Order_Type__c == null || ordr.ERP_Order_Type__c == '')&& (userMarket =='US' ||userMarket =='CA' )){
                if(userfields[0].Default_ERP_Order_Type__c == null || userfields[0].Default_ERP_Order_Type__c == ''){
                    ordr.ERP_Order_Type__c.addError('There is no ERP Type for the User or not populated at the time of order creation');
                }else{
                    ordr.ERP_Order_Type__c = userfields[0].Default_ERP_Order_Type__c;
                }
            }
          if(ordr.CompanyAuthorizedById ==NULL){
            ordr.CompanyAuthorizedById = UserInfo.getUserId();}            
            if(ordr.Contact__c == null){ordr.Contact__c =  ordr.ShipToContactId;} //CP : Modified the null check as revenueOrder Creation was failling
            if((ordr.Market__c == null || ordr.Market__c == '') || (ordr.Line_of_Business__c == null || ordr.Line_of_Business__c == '') || (ordr.Business_Unit__c ==  null || ordr.Business_Unit__c ==  '')) {
                if(ordr.Salesperson__c != null){
                    lstWithSalesRepOrder.add(ordr);
                }else{
                    lstWithNonSalesRepOrder.add(ordr); 
                }
            }
            //ordr = removespecialcharacters(ordr);
            if(String.isNotBlank(ordr.Market__c))
            {
              if(ordr.Market__c != Label.PS_USMarket)
              {
                  ordr.Shipping_address_formatted__c = True;
              }
            } else {
              
            }
        }
        
        if(!lstWithSalesRepOrder.isEmpty() && lstWithSalesRepOrder.size() > 0)
        {
            PS_UserFieldUtill.setUserFieldValues(lstWithSalesRepOrder, 'Salesperson__c');
        }
        
        if(!lstWithNonSalesRepOrder.isEmpty() && lstWithNonSalesRepOrder.size() > 0)
        {
            PS_UserFieldUtill.setUserFieldValues(lstWithNonSalesRepOrder, 'CompanyAuthorizedById');
        }
         // Class for populating fields on Order
        PS_PopulateFieldsOnOrder populatefields = new PS_PopulateFieldsOnOrder(newOrders);
        // Method for populating Pricebook
        populatefields.PopulatePriceBook();
    }

    // EXECUTE AFTER INSERT LOGIC
    //
    public void OnAfterInsert(Map<Id,Order> newOrders)
    {
       PS_INT_IntegrationRequestController.createIntegrationRequestOrder(newOrders.values(),null);
    }
    
    // BEFORE UPDATE LOGIC
    //
    public void OnBeforeUpdate( Map<Id,Order> newOrders, Map<Id,Order> oldOrders)
    {
        try{
        // Create static map of orders being submitted (eg. status is being changed to 'Open')
       // This is used to supress validation in the OrderItem trigger for this transaction
        PS_OrderSubmissionHandler.setOrderCurrentlyBeingSubmittedMap(newOrders, oldOrders);        
        Map<String, Order> mapNewOrder = createListMap(newOrders);
        List<PS_OrderValidationInterface> validations = PS_OrderValidationFactory.CreateValidations(mapNewOrder, oldOrders, new User(Id=UserInfo.getUserId()));
            
        Map<String,List<String>> exceptions = new Map<String,List<String>>();

        for(PS_OrderValidationInterface validation: validations)
        {
            validation.validateUpdate(exceptions);
        }

        if( ! exceptions.isEmpty())
        {
            PS_Util.addErrors(mapNewOrder, exceptions);

            return;
        }


       //List<Order> lstWithAddressChangedOrders = new List<Order>();
        
       for(Order ordr: newOrders.values())
       {           
         //Below line commneted by Abhishek - CR-01868-Req-14
        //String userDefaultERPType = [Select Default_ERP_Order_Type__c From User Where Id = :UserInfo.getUserId()][0].Default_ERP_Order_Type__c;
          //TEPNA: 4th May - Manimala -  Added Market validation to retrict US & CA for setting ERP Order Type value 
           if((ordr.ERP_Order_Type__c == null || ordr.ERP_Order_Type__c == '')&& (userMarket =='US' ||userMarket =='CA')){
                if(userDefaultERPType == null || userDefaultERPType == ''){
                     ordr.ERP_Order_Type__c.addError('There is no ERP Type for the User or not populated at the time of order creation');
                }else{
                    ordr.ERP_Order_Type__c = userDefaultERPType;
                }
            }
            // CR-01868-Req-14 code changes ends
            if(ordr.Contact__c == null ){ordr.Contact__c = ordr.ShipToContactId;} ////CP : Modified the null check as revenueOrder Creation was failling
            ordr = removespecialcharacters(ordr);
            if(String.isNotBlank(ordr.Market__c))
            {
             
              if(ordr.Market__c != Label.PS_USMarket)
              {
                  ordr.Shipping_address_formatted__c = True;
              }
            } else {
            }
       }

       OrderApproval approvalprocess = new OrderApproval(newOrders,oldOrders);
       approvalprocess.submitOrder(); 


       
       }catch(exception e){string errormessage=e.getMessage()+e.getStackTraceString();ExceptionFramework.LogException('Order Update','StdOrderTriggerHandler','OnBeforeUpdate',errormessage,UserInfo.getUserName(),'');           
        }
    }
    
    // AFTER UPDATE LOGIC
    //
    public void OnAfterUpdate(Map<Id,Order> newOrders, Map<Id,Order> oldOrders)
    {    
        try
        {
          PS_INT_IntegrationRequestController.createIntegrationRequestOrder(newOrders.values(),oldOrders);
          // Class for updating Order items when Order Status is changed to Filled, applicable for Manually processed Orders
          // calling this class before approvalprocess because there is recursion check on OrderItemTriggerHandler.OnAfterUpdate() hence it gets called only once in a transaction and approvalprocess.saveOrderItems() updates OrderItems even when there is no update required
          PS_UpdateOrderItemStatus updateOrderItems = new PS_UpdateOrderItemStatus(newOrders, oldOrders);
          updateOrderItems.StatusChangedToFilled();
          //KP - to update opportunity stage for R6
          GlobalRevenueUpdateOppty.mUpdateOptyStage(newOrders,oldOrders);
          Map<Id,Order> ordlist = new Map<Id,order>();
          for(Order ord:newOrders.values()){
              if (ord.Status == 'Pending Approval' || ord.Status == 'Open' || ord.Status == 'Cancelled') //Added new condition for Order line item status update for CR-2023 Req 3
                  ordlist.put(ord.Id,ord);
          }
          if (ordlist.size() > 0){
          OrderApproval approvalprocess = new OrderApproval(ordlist,oldOrders);
          approvalprocess.submitApprovalChatterPost();
          approvalprocess.updatelorderlineitemstatus();
          approvalprocess.saveOrderItems();
          }
          // Class for sending Email alerts
          PS_SendEmailAlertsForOrder sendEmail = new PS_SendEmailAlertsForOrder(newOrders, oldOrders);
          sendEmail.StatusChangedToOpen();
        
         
        }catch(exception e){string errormessage=e.getMessage()+e.getStackTraceString();ExceptionFramework.LogException('Order Update','StdOrderTriggerHandler','OnAfterUpdate',errormessage,UserInfo.getUserName(),'');throw e;  
        } 
    }


    public static Map<String,Order> createListMap(List<Order> objs)
    {
        Map<String,Order> output = new  Map<String,Order>();

        for(Integer i =0 ; i< objs.size(); i++)
        {
            output.put(String.valueOf(i), objs[i]);
        }

        return output;
    }

    public static Map<String,Order> createListMap(Map<Id,Order> objs)
    {
        Map<String,Order> output = new  Map<String,Order>();

        for(Id id: objs.keySet())
        {
            output.put(id,objs.get(id));
        }

        return output;
    }

    
    private order removespecialcharacters(order inOrder)
    {
        try{
            /*if(inOrder.ShippingStreet!=null) {inOrder.ShippingStreet=String.valueof(inOrder.ShippingStreet).replaceall('[^a-zA-Z0-9\\.\\#\\,\\-]',' ');} 
            if(inOrder.Shippingcity!=null) {inOrder.Shippingcity=String.valueof(inOrder.Shippingcity).replaceall('[^a-zA-Z0-9\\.\\#\\,\\-]',' ');} 
            if(inOrder.Shippingpostalcode!=null) {inOrder.ShippingPostalcode=String.valueof(inOrder.Shippingpostalcode).replaceall('[^a-zA-Z0-9\\.\\#\\,\\-]',' ');} */
            if(inOrder.Shipping_Instructions__c!=null) {inOrder.Shipping_Instructions__c=String.valueof(inOrder.Shipping_Instructions__c).replaceAll('\r|\n|\r\n|\n\r', '');} // Defect fix for D-4488.
            if(inOrder.Packing_Instructions__c!=null) {inOrder.Packing_Instructions__c=String.valueof(inOrder.Packing_Instructions__c).replaceAll('\r|\n|\r\n|\n\r', '');} // Defect fix for D-4488.
            if(inOrder.Order_Notes__c!=null) {inOrder.Order_Notes__c=String.valueof(inOrder.Order_Notes__c).replaceAll('\r|\n|\r\n|\n\r', '');} // Defect fix for D-4488.
            return inOrder;
        }catch(exception e){string errormessage=e.getMessage()+e.getStackTraceString();ExceptionFramework.LogException('Order Update','StdOrderTriggerHandler','removespecialcharacters',errormessage,UserInfo.getUserName(),'');return null;         
        } 
    } 
    
}