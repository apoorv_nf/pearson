@isTest
public class MobileCacheWarmerController_Test {
    public static testMethod void testController(){
    List<Opportunity> listWithOpportunity = new List<Opportunity>();
    Product2 newProduct2 = new Product2();
   
    
  
    List<Product2> listWithProduct2 = new List<Product2>();
    List<User> listWithUser = new List<User>();
    listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
   
    if(listWithUser != null && !listWithUser.isEmpty())
    {
        listWithUser[0].Price_List__c = 'Math & Science';
        insert listWithUser;
    }
    Bypass_Settings__c bypass = new Bypass_Settings__c();
    bypass.Disable_Triggers__c = true;
    bypass.disable_Validation_Rules__c = true;
    bypass.setupownerId = listWithUser[0].id;  
    insert bypass;
        
   System.runAs(listWithUser[0])
   {
    PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'Pearson_Network_Sampler'];
    insert new PermissionSetAssignment(AssigneeId = listWithUser[0].id, PermissionSetId = ps.Id);
    
    PermissionSet ps1 = [SELECT ID From PermissionSet WHERE Name = 'Pearson_Front_End_Sampling'];
    insert new PermissionSetAssignment(AssigneeId = listWithUser[0].id, PermissionSetId = ps1.Id);
    
   
     String OpprectypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('B2B').getRecordTypeid();
      Account acc =  new Account();
        acc.Name = 'Mani';
         acc.Line_of_Business__c = 'Higher Ed';
          acc.Geography__c = 'Core';
          acc.IsCreatedFromLead__c = True;  
          acc.ShippingCity ='Shipping City';
          acc.ShippingCountry = 'United Kingdom';
          acc.ShippingStreet = 'Shipping Street';
          acc.ShippingPostalCode = 'NE27 0QQ';          
          acc.Market2__c = 'Global';
        insert acc;
        
      Contact conObj1 = new Contact();                  
         conObj1.FirstName = 'FName';
         conObj1.LastName = 'LName';
         conObj1.AccountId = acc.Id;            
         conObj1.email =  'test30@gmail.com';
         conObj1.Preferred_Address__c = 'Mailing Address';
         conObj1.MailingCountry = 'United Kingdom';
         conObj1.MailingStreet = '11 Street';
         conObj1.MailingPostalCode = 'NE27 0QQ';
         conObj1.MailingCity = 'City';
         //Abhinav on 3/2016 : Populating Mobile,Salutation and First Language on Contact.
         conObj1.MobilePhone = '43685634789';
         conObj1.First_Language__c = 'English';
          conObj1.Salutation = 'Mr.';    
                              
        insert conObj1; 
        
        Opportunity oppObj2 = new Opportunity();       
         oppObj2.Name = 'OpporName';
         oppObj2.AccountId = acc.Id;
         oppObj2.CloseDate = System.TODAY() + 30;
         oppObj2.StageName = 'OpporStageName';     
         oppObj2.Registration_Payment_Reference__c = 'A12B'; 
         oppObj2.Spring__c = 10;
         oppObj2.Fall__c = 10;
         oppObj2.recordtypeid = OpprectypeId;
         oppObj2.Selling_Period__c = '2015 - Spring';    
         
          insert oppObj2;  
          listWithOpportunity.add(oppObj2);   
    
    List<ProductCategory__c> listWithCategory = new List<ProductCategory__c>();
    ProductCategory__c newCategory = new ProductCategory__c();
    newCategory.HierarchyLabel__c = 'Professional & Career Services';
    newCategory.Name = 'Professional & Career Services';
    listWithCategory.add(newCategory);
    insert listWithCategory;
    
    List<Hierarchy__c> listWithHierarchy = new List<Hierarchy__c>();
    Hierarchy__c newHierarchy = new Hierarchy__c();
    newHierarchy.ProductCategory__c =  listWithCategory[0].Id;
    newHierarchy.Label__c = '(BIM) Building Information Modeling';
    newHierarchy.Name = '(BIM) Building Information Modeling';
    newHierarchy.Type__c = 'Course';
    listWithHierarchy.add(newHierarchy);
    insert listWithHierarchy;
    
    List<Opportunity_Pearson_Course_Code__c> listWithOpportunityPearsonCourseCode = new List<Opportunity_Pearson_Course_Code__c>();
    Opportunity_Pearson_Course_Code__c newOpportunityPearsonCourseCode;
    newOpportunityPearsonCourseCode = new Opportunity_Pearson_Course_Code__c();
    newOpportunityPearsonCourseCode.Opportunity__c = listWithOpportunity[0].Id;
    newOpportunityPearsonCourseCode.Pearson_Course_Code_Hierarchy__c = listWithHierarchy[0].Id;
    newOpportunityPearsonCourseCode.Primary__c = true;
    listWithOpportunityPearsonCourseCode.add(newOpportunityPearsonCourseCode);
    insert listWithOpportunityPearsonCourseCode;
    
    newProduct2.Name = 'X-FULWIL & RIO SALADO - COLLEGE WRITERS REFERENCE:2009 MLA UPD EDTN, 2/e';
    newProduct2.Configuration_Type__c = 'Bundle';
    newProduct2.Author__c = 'FULWIL & RIO SALADO';
    newProduct2.Origin__c = 'CA';
    newProduct2.Copyright_Year__c = '2010';
    listWithProduct2.add(newProduct2);
    insert listWithProduct2;
  
    List<RelatedProduct__c> listWithRelatedProduct = new List<RelatedProduct__c>();
    RelatedProduct__c newRelatedProduct = new RelatedProduct__c();
    newRelatedProduct.Product__c = listWithProduct2[0].Id;
    newRelatedProduct.RelatedProduct__c = listWithProduct2[0].Id;
    newRelatedProduct.InstructorResource__c = true;
    newRelatedProduct.StudentResource__c = true;
    newRelatedProduct.RelationPackage__c = true;
    newRelatedProduct.Component_Package__c = true;
    newRelatedProduct.PSAM__c = true;
    listWithRelatedProduct.add(newRelatedProduct);
    insert listWithRelatedProduct; 
    
    List<ProductCategory__c> listWithnewProductCategory = new List<ProductCategory__c>();
    ProductCategory__c newProductCategory = new ProductCategory__c();
    //newProductCategory.Apttus_Config2__Active__c = true;
    newProductCategory.Name = 'Test';
    //newProductCategory.DM_Identifier__c = '1234';
    newProductCategory.HierarchyLabel__c = 'Test';
    listWithnewProductCategory.add(newProductCategory);
    insert listWithnewProductCategory;
    
    List<Catalog__c> listWithCatalog = new List<Catalog__c>();
    Catalog__c newCatalog = new Catalog__c();
    newCatalog.Name = 'Math & Science';
    //newCatalog.isActive__c = true;
    //newCatalog.Type__c = 'Standard';
    listWithCatalog.add(newCatalog);
    insert listWithCatalog;   
       
    List<CatalogPrice__c> listWithCatalogPrice = new List<CatalogPrice__c>();
    CatalogPrice__c newCatalogPrice = new CatalogPrice__c();
    newCatalogPrice.Product__c = listWithProduct2[0].Id;
    newCatalogPrice.isActive__c = true;
    newCatalogPrice.Catalog__c = listWithCatalog[0].Id;
    newCatalogPrice.ValidForMobile__c ='1';
    listWithCatalogPrice.add(newCatalogPrice);
    insert listWithCatalogPrice;   
       
    List<Marketing_Information__c> listWithMarketingInformation = new List<Marketing_Information__c>();
    Marketing_Information__c newMarketingInformation = new Marketing_Information__c();
    newMarketingInformation.Type__c = 'Rep Tools';
    newMarketingInformation.Family__c = listWithProduct2[0].Id;
    newMarketingInformation.Sub_Type__c = 'Features';
    listWithMarketingInformation.add(newMarketingInformation);
    insert listWithMarketingInformation;
    
   /* List<Apttus_Config2__ProductClassification__c> listWithProductClassification = new List<Apttus_Config2__ProductClassification__c>();
    Apttus_Config2__ProductClassification__c newApttusConfig2ProductClassification = new Apttus_Config2__ProductClassification__c();
    newApttusConfig2ProductClassification.Apttus_Config2__ClassificationId__c = listWithApttusConfig2ClassificationHierarchy[0].Id;
    newApttusConfig2ProductClassification.Apttus_Config2__ProductId__c = listWithProduct2[0].Id;
    listWithProductClassification.add(newApttusConfig2ProductClassification);
    insert listWithProductClassification;*/
    
    List<PriceBook2> listWithPriceBook = new List<PriceBook2>();
    PriceBook2 newPriceBook = new PriceBook2();
    newPriceBook.Name = 'Standard Price Book';
    listWithPriceBook.add(newPriceBook);
    insert listWithPriceBook;
        
    List<PriceBookEntry> listWithPriceBookEntry = new List<PriceBookEntry>();
    PriceBookEntry newPriceBookEntry = new PriceBookEntry();
    newPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();//listWithPriceBook[0].Id;
    newPriceBookEntry.UnitPrice = 10.0;
    newPriceBookEntry.Product2Id = listWithProduct2[0].Id; 
    newPriceBookEntry.IsActive = true;
    listWithPriceBookEntry.add(newPriceBookEntry);
    insert listWithPriceBookEntry;
    
    List<OpportunityLineItem> listWithOpportunityProduct = new List<OpportunityLineItem>();
    OpportunityLineItem newOpportunityProduct = new OpportunityLineItem();
    newOpportunityProduct.Quantity = 12;
    newOpportunityProduct.UnitPrice = 10.0;
    newOpportunityProduct.PricebookEntryId = listWithPriceBookEntry[0].Id;
    newOpportunityProduct.OpportunityId = listWithOpportunity[0].Id;
    listWithOpportunityProduct.add(newOpportunityProduct);
    insert listWithOpportunityProduct;
    listWithOpportunityProduct = new list<OpportunityLineItem>();
    newOpportunityProduct.Quantity = 24;
    listWithOpportunityProduct.add(newOpportunityProduct);
    update listWithOpportunityProduct;
   
        
    System.assertEquals( MobileCacheWarmerController.getMI(listWithCatalog[0].Id), 1 );
    System.assertEquals( MobileCacheWarmerController.getPC(listWithCatalog[0].Id), 0 );
    System.assertEquals( MobileCacheWarmerController.getPCl(listWithCatalog[0].Id),0 );
    System.assertEquals( MobileCacheWarmerController.getRP(listWithCatalog[0].Id),1 );
       
   }
    
    }


}