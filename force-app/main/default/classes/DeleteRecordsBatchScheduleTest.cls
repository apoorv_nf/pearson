/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            DeleteRecordsBatchScheduleTest.cls 
   Description:     Test class for DeleteRecordsBatchSchedule class
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
  08/13/2019        0.1        Mani Kagithoju                Created
                                
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
@istest
public class DeleteRecordsBatchScheduleTest{
    public static testMethod void testschedule() {
        Test.StartTest();
        General_One_CRM_Settings__c myCS1 = new General_One_CRM_Settings__c();
        myCS1.Value__c = '50';
        myCS1.Category__c = 'Batch processing';
        myCS1.Name = 'Delete Records Batch Size Limit';
        myCS1.Description__c= 'Delete Records Batch Size Limit';
        insert myCS1;
        
        DeleteRecordsBatchSchedule testsche = new DeleteRecordsBatchSchedule();
        String sch = '0 0 6 ? * 6 *';
        system.schedule('Test status Check', sch, testsche );
        Test.stopTest();
    }
}