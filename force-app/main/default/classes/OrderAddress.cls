public class OrderAddress {
    
    public void OrderAddresshandler(list<Order> orderLSt){
        List<Order> OrderAddressprocess = new List<Order>();
        List<ID> ContactIds = new List<Id>();
        // for each order in the new list
        for(Order eachorder: orderLSt)
        {
            System.debug('Davi-->Order_Address_Type__c:[' + eachorder.Order_Address_Type__c + '];');
            
            // if the address type is not null and not custom type and not account type add it to the list
            if(eachorder.Order_Address_Type__c != null && eachorder.Order_Address_Type__c != 'Custom' && eachorder.Order_Address_Type__c != 'Account')
            {
               OrderAddressprocess.add(eachorder);
            } 

            // if the address type is contact mailing or contact other and contact not null add to contacts list
            if((eachorder.Order_Address_Type__c == 'Contact Mailing' || eachorder.Order_Address_Type__c == 'Contact Other') && eachorder.ShipToContactId != null)
            {
                 ContactIds.add(eachorder.ShipToContactId);
            }
        }
        
        // if there are some contacts - type was contact mailing or contact other 
        if(ContactIds != null && !ContactIds.isEmpty())
        {
           // get the address details for all contacts
           map<Id,Contact> ContactAddress = new map<Id,Contact>([Select Id, MailingStreet, MailingCity , MailingState, MailingStateCode, MailingCountry, MailingCountryCode, MailingPostalCode,
                                         OtherStreet, OtherCity, OtherState, OtherStateCode, OtherCountry, OtherCountryCode, OtherPostalCode from Contact where Id in:ContactIds]);
                                         
           // for each order                              
            for(Order eachorder : OrderAddressprocess)
            {
                // if the type is contact mailing and the addess was retrieved from the map
                if(eachorder.Order_Address_Type__c == 'Contact Mailing' && ContactAddress.get(eachorder.ShipToContactId) != null)
                {
                    // if part of the address is not null
                    if(ContactAddress.get(eachorder.ShipToContactId).MailingStreet != null)
                    {
                      // populate the override address 
                        string[] addresslist = ContactAddress.get(eachorder.ShipToContactId).MailingStreet.split('\r\n');
                        if(addresslist != null && !addresslist.isEmpty())
                        {
                            eachorder.Override_Delivery_Address_Line1__c = addresslist[0];
                        }
                        if(addresslist.size() >= 2)
                        {
                            eachorder.Override_Delivery_Address_Line2__c = addresslist[1];
                        }
                        if(addresslist.size() >= 3)
                        {
                            eachorder.Override_Delivery_Address_Line3__c = addresslist[2];
                        }
                        if(addresslist.size() >= 4) 
                        {
                            eachorder.Override_Delivery_Address_Line4__c = addresslist[3];
                        }
                    }
                    
                    eachorder.Override_Delivery_City__c = ContactAddress.get(eachorder.ShipToContactId).MailingCity;
                    eachorder.Override_Delivery_State__c = ContactAddress.get(eachorder.ShipToContactId).MailingState;
                    eachorder.Override_Delivery_Country__c = ContactAddress.get(eachorder.ShipToContactId).MailingCountry;
                    eachorder.Override_Delivery_Postal_Code__c = ContactAddress.get(eachorder.ShipToContactId).MailingPostalCode;
                    
                } 
                // if the address type is Contact Other populae it from the contact other fields
                else if (eachorder.Order_Address_Type__c == 'Contact Other' && ContactAddress.get(eachorder.ShipToContactId) != null)
                {
                     if(ContactAddress.get(eachorder.ShipToContactId).OtherStreet != null)
                     {
                          string[] addresslist = ContactAddress.get(eachorder.ShipToContactId).OtherStreet.split('\r\n');
                          if(addresslist != null && !addresslist.isEmpty())
                          {
                              eachorder.Override_Delivery_Address_Line1__c = addresslist[0];
                          }
                          if(addresslist.size() >= 2)
                          {
                              eachorder.Override_Delivery_Address_Line2__c = addresslist[1];
                          }
                          if(addresslist.size() >= 3)
                          {
                             eachorder.Override_Delivery_Address_Line3__c = addresslist[2];
                          }
                          if(addresslist.size() >= 4)
                          {
                              eachorder.Override_Delivery_Address_Line4__c = addresslist[3];
                          }
                     }
                         
                     eachorder.Override_Delivery_City__c = ContactAddress.get(eachorder.ShipToContactId).OtherCity;
                     eachorder.Override_Delivery_State__c = ContactAddress.get(eachorder.ShipToContactId).OtherState;
                     eachorder.Override_Delivery_Country__c = ContactAddress.get(eachorder.ShipToContactId).OtherCountry;
                     eachorder.Override_Delivery_Postal_Code__c = ContactAddress.get(eachorder.ShipToContactId).OtherPostalCode;
                }
                
            }
        }        
    }
}