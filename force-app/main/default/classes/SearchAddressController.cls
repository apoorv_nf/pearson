/*****************************************************************************************************************************************************
Author          Version        Date                           Summary of Changes 
-----------      ----------      -----------------       -----------------------------------------------------------------------------------------------
KP                    v1.0    6/24/2016                    Controller for Address Search Page Validation
RG                            8/23/2016                    Redirect condtion check
KP                            9/6/2016                     Integration defect fixes
Manimala                      11/1/2018                    Address_type__c removed and Billing__c,Shipping__c fields referred
Vinoth                        30-Nov-2018                  Add Physical Address(Primary_Physical__c,Physical__c ) as part of CR-02397
Manimala                      01/03/2019                   Add County field for CR-2483
Sekhar Malli                  06/06/2019                   Add Address Line4 Validation Rule for CR-2680
Manimekalai					  07/30/2019				   Assign User's TEP OU to account address TEP OU
******************************************************************************************************************************************************************/

public class SearchAddressController{

    //Static Variable declaration to capture user input
    public SampleOrderContacts oOrderCont{get; set;}
    public String sCountry {get; set;}
    public String sAddressLine1 {get; set;}
    public String sAddressLine2 {get; set;}
    public String sAddressLine3 {get; set;}
    public String sAddressLine4 {get; set;}
    public String sCity {get; set;}
    public String sState {get; set;}
    public string sCounty{get; set;} //Variable added for County
    public String sZipCode {get; set;}
    public String sAddValStatus {get; set;}
    public String sAddressType {get;set;}
    public boolean billingtype {get;set;}  // Variable added for Billing_c checkbox
    public boolean shippingtype {get;set;} // Variable added for Shipping_c checkbox
    public boolean Physicaltype {get;set;} // Variable added for Physical_c checkbox   
    
    
    public Boolean bShowSearchResult {get; set;}   
    public Boolean bShowMatchCode {get; set;}   
    public Boolean bPrimaryFlg {get; set;}
    public Boolean bPrimaryShippingFlg{get; set;}
    
    public Boolean bPrimaryphyFlg{get; set;}    //CR-02397 Primary Physical values
    
    public Boolean bShowPrimaryBilling{get; set;}   
    public static List<AddressWrapper> oAddrResponseList{get; set;}
    public String sObjectType {get;set;}
    public Id recordId {get;set;}
    public Id accRecordId{get;set;}
    public Account oAcc{get; set;}
    public transient Lead oLead{get; set;}
    public transient Map<String,String> objCountryCodeMap;
    public transient Map<String,String> objStateCodeMap;
    public string sQuoteId{get;set;}
    public String ERPId{get;set;} //Sekhar added CR-2680 Refering the ERP_Id from Account 
    public String sFinAccStatus{get;set;}
    public  String userProfile{get;set;}
    public String AccountTepLevelValue{get; set;}
    public string statedescription{get;set;}
    public string countrydescription{get;set;}
    Account_Addresses__c objAccaddr;
    public string county{get;set;}
    public Boolean UserMarket {get;set;} //Sekhar added:CR-2680 for AddressLine4 Validation June-06-2019
    Public List<User> loggedInUserList{get;set;}// Added for CR-02863 by Manimekalai to pull the loggedIn user TEP Operating Unit
   

    //Constructor method definition - to decide page called from Lead, Contact or Account
    public SearchAddressController(){
       try{
        //Retrieve Page Parameters        
        userProfile = [select Name from profile where id = :userinfo.getProfileId()].Name;
        //CR-02863 - Start
        loggedInUserList = new List<User>();
        loggedInUserList= [select id,Market__c,TEP_Operating_Unit__c from User WHERE id=:userinfo.getUserId() AND  Market__c IN('US','CA','UK')]; 
        //CR-02863 - End
           if(test.isRunningTest())   
        {
            userProfile = System.Label.R6_Pearson_Sales_User_OneCRM;
        }
        sObjectType = ApexPages.currentPage().getParameters().get('sObjType'); 
        sAddressType = ApexPages.currentPage().getParameters().get('sAddressType'); 
        recordId = ApexPages.currentPage().getParameters().get('sId');
        sQuoteId= ApexPages.currentPage().getParameters().get('qId');
        bShowSearchResult = false;
        sAddValStatus = 'Invalidated';
        sState = '';
        sCounty=''; // intializing the variable 
        sCountry = '';
        bShowPrimaryBilling=true;

        if(userProfile==System.Label.R6_Pearson_Sales_User_OneCRM){
        bPrimaryFlg =false;
        bShowPrimaryBilling=false;
        }
        if (oAcc== null)
            oAcc= new Account();      
        oOrderCont=new SampleOrderContacts(null,null,'','','','');
        //Account and Quote to create address from Financial account
        if(sObjectType == 'Account' || sObjectType == 'Quote'){
          Account objFinAccount = [select Id,ERP_Status__c,TEP_Level__c,ERP_ID__c from Account where id =:recordId limit 1 ];   
            //   Financial_Account__c objFinAccount = [select Account__c,ERP_Status__c from Financial_Account__c where id =:recordId limit 1 ];
            accRecordId=objFinAccount.Id;                                                                      //   accRecordId=objFinAccount.Account__c;
            sFinAccStatus=objFinAccount.ERP_Status__c;   
            AccountTepLevelValue=objFinAccount.TEP_Level__c;
            ERPId =objFinAccount.ERP_ID__c;
                                                                  //   sFinAccStatus=objFinAccount.ERP_Status__c;                                                  
        }
        //Lead or contact to default address in manage address page.
        if(sObjectType == 'Lead'){
            oLead = [Select Id,Country,Countrycode,City,PostalCode,Street,State,statecode,county__c from Lead where Id =: recordId limit 1];
            if(oLead != null){
                if (oLead.Country != null)
                {
                    if(oLead.Country =='United Kingdom')
                         mPopulateCurrentAddrForUK(oLead.Country,oLead.CountryCode,oLead.City,oLead.PostalCode,oLead.Street,oLead.county__C);
                    else
                         mPopulateCurrentAddr(oLead.Country,oLead.CountryCode,oLead.City,oLead.PostalCode,oLead.Street,oLead.State,oLead.StateCode);
                }
                //    mPopulateCurrentAddr(oLead.Country,oLead.CountryCode,oLead.City,oLead.PostalCode,oLead.Street,oLead.State,oLead.StateCode);
            }
        }
        else if(sObjectType == 'Contact'){
            if(sAddressType =='Mailing'){
                Contact oCon = [Select MailingCountry,MailingCountryCode,MailingCity,MailingPostalCode,MailingStreet,MailingState,MailingStateCode,Mailing_County__c from Contact where Id =: recordId];
                if(oCon != null){
                    if (oCon.MailingCountry != null)
                    {
                        if(oCon.MailingCountry=='United Kingdom')
                            mPopulateCurrentAddrForUK(oCon.MailingCountry,oCon.MailingCountryCode,oCon.MailingCity,oCon.MailingPostalCode,oCon.MailingStreet,oCon.Mailing_County__c);
                        else
                            mPopulateCurrentAddr(oCon.MailingCountry,oCon.MailingCountryCode,oCon.MailingCity,oCon.MailingPostalCode,oCon.MailingStreet,oCon.MailingState,oCon.MailingStateCode);
                    }
                        //mPopulateCurrentAddr(oCon.MailingCountry,oCon.MailingCountryCode,oCon.MailingCity,oCon.MailingPostalCode,oCon.MailingStreet,oCon.MailingState,oCon.MailingStateCode);
                }
            }else if (sAddressType =='Other'){
                Contact oCon = [Select OtherCountry,OtherCountryCode,OtherCity,OtherPostalCode,OtherStreet,OtherState,OtherStateCode,Other_County__c from Contact where Id =: recordId];
                if(oCon != null){
                    if (oCon.OtherCountry != null)
                    {
                        if(oCon.OtherCountry=='United Kingdom')
                             mPopulateCurrentAddrForUK(oCon.OtherCountry,oCon.OtherCountryCode,oCon.OtherCity,oCon.OtherPostalCode,oCon.OtherStreet,oCon.Other_County__c);   
                        else
                             mPopulateCurrentAddr(oCon.OtherCountry,oCon.OtherCountryCode,oCon.OtherCity,oCon.OtherPostalCode,oCon.OtherStreet,oCon.OtherState,oCon.OtherStateCode);   
                    }
                       // mPopulateCurrentAddr(oCon.OtherCountry,oCon.OtherCountryCode,oCon.OtherCity,oCon.OtherPostalCode,oCon.OtherStreet,oCon.OtherState,oCon.OtherStateCode);   
                }
            }
        }
      }
      catch(exception e){
      }  
    }
  
    //Predefault record address on address search page for Lead or Contact
    public void mPopulateCurrentAddr(String sCurrentCountry,String sCurrentCountryCode,String sCurrentCity,String sCurrentPostalCode,String sCurrentStreet,String sCurrentState,String sCurrentStateCode){
        sCountry = sCurrentCountryCode;
        oAcc.BillingCountryCode = sCurrentCountryCode;
        oAcc.BillingCountry = sCurrentCountry;
        sCity = sCurrentCity;
        
        if (sCurrentState != null){
            oAcc.BillingStateCode = sCurrentStateCode;
            oAcc.BillingState = sCurrentState;
            sState = sCurrentStateCode;  
        }
        sZipCode = sCurrentPostalCode;
        if(sCurrentStreet!= null || sCurrentStreet!=''){
            List<String> sStreetAddr = sCurrentStreet.split('\n');
            for(Integer i=0;i<sStreetAddr.size();i++){
               if(i==0) sAddressLine1 = sStreetAddr[i];
               if(i==1) sAddressLine2 = sStreetAddr[i];
               if(i==2) sAddressLine3 = sStreetAddr[i];
               if(i==3) sAddressLine4 = sStreetAddr[i];
             }
        }
    }    
    
    //Predefault record address on address search page for Lead or Contact
    public void mPopulateCurrentAddrForUK(String sCurrentCountry,String sCurrentCountryCode,String sCurrentCity,String sCurrentPostalCode,String sCurrentStreet,String sCurrentCounty){
        sCountry = sCurrentCountryCode;
        oAcc.BillingCountryCode = sCurrentCountryCode;
        oAcc.BillingCountry = sCurrentCountry;
        sCity = sCurrentCity;
        oAcc.County__c = sCurrentCounty;
        sCounty = sCurrentCounty; 
        sState=sCurrentCounty;
        sZipCode = sCurrentPostalCode;
        if(sCurrentStreet!= null || sCurrentStreet!=''){
            List<String> sStreetAddr = sCurrentStreet.split('\n');
            for(Integer i=0;i<sStreetAddr.size();i++){
               if(i==0) sAddressLine1 = sStreetAddr[i];
               if(i==1) sAddressLine2 = sStreetAddr[i];
               if(i==2) sAddressLine3 = sStreetAddr[i];
               if(i==3) sAddressLine4 = sStreetAddr[i];
             }
        }
    }    
    //Methods to init Country and state values from page to controller
    public void mInitCountry(){
        oAcc.BillingStateCode='';
        oAcc.BillingState='';
    }
    //Method to init state values from page to controller
    public void mInitState(){
    }
    //Method to init state values from page to controller
    public void mInitCounty(){
    }
    
     //Method to init city values from page to controller
    public void mInitCity(){
    }    
    
    //Method to init Addr type values from page to controller           
    public void mInitAddr(){
    }

    //Method for creating address type picklist values in page
    public List<SelectOption> getAddressTypes() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None','--None--'));
        options.add(new SelectOption('Shipping','Shipping'));
        if(userProfile!=System.Label.R6_Pearson_Sales_User_OneCRM){
            options.add(new SelectOption('Billing','Billing'));
            options.add(new SelectOption('Billing and Shipping','Billing and Shipping'));
           
        }
        return options;
        
    }
    //Method for address validation
    public boolean mAddressValidation(Boolean frmSrch){
        ApexPages.Message myMsg;
        if (frmSrch){//validation on search
            if (sCountry == '' || sCountry == 'None' || sCity == '' || sCity == null || sCountry == null){
                myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.AddressValError);
                ApexPages.addMessage(myMsg);
                return true;
            }            
        }
        else{//validation on save
            if (accRecordId != null){
               // if (sCountry == '' || sCountry == 'None' || sCity == '' || sCity == null || sCountry == null || sAddressType =='' || sAddressType == null || 
                   // sAddressType == 'None'){
                   if (sCountry == '' || sCountry == 'None' || sCity == '' || sCity == null || sCountry == null || (!billingtype && !shippingtype && !Physicaltype)){
                    myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Country, City and Address type must be populated to save the address record.');
                    ApexPages.addMessage(myMsg);
                    return true;
                }
                //if(bPrimaryFlg==true && !sAddressType.contains('Billing')){
                  if(bPrimaryFlg==true && !billingtype==true){
                    myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.PrimaryAddressVal);
                    ApexPages.addMessage(myMsg);
                    return true;
                }
                //if(bPrimaryShippingFlg==true && !sAddressType.contains('Shipping')){
                //  if(bPrimaryFlg==true && !shippingtype==true){
                if(bPrimaryShippingFlg==true && !shippingtype==true){
                    myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.PrimaryAddressVal);
                    ApexPages.addMessage(myMsg);
                    return true;
                }
                
                //CR-02397
                if(bPrimaryphyFlg==true && !Physicaltype==true){
                    myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.PrimaryAddressVal);
                    ApexPages.addMessage(myMsg);
                    return true;
                }
                
                //Sekhar Malli Added:CR-2680 :June-06-2019 for AddressLine4 Validation -Start
                   list<User> CurrentUser=[select id,Market__c from User WHERE id=:userinfo.getUserId() AND  Market__c IN('US','CA')];                   
                   UserMarket=CurrentUser.size()>0 ? TRUE :FALSE;
                
                     if(UserMarket){
                     if((sAddressLine1 == '' && sAddressLine2!= '' && sAddressLine3!= '' && sAddressLine4!='' ) ||
                     (sAddressLine1 == '' && sAddressLine2!= '' )  ||
                     ((sAddressLine1 == '' || sAddressLine2== '') && sAddressLine3!= '' ) ||
                     ((sAddressLine1 == '' || sAddressLine2== '' || sAddressLine3== '') && sAddressLine4!= '' ))     
                     {
                      myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.Do_Not_Skip_Address_Lines);
                      ApexPages.addMessage(myMsg);
                      return true;
                     }
                  }
                   if(UserMarket){
                   //if((sCountry == 'United States' || sCountry=='Canada') && (billingtype == True || shippingtype==True )){
                   //if((sCountry =='US' || sCountry =='CA') && ( (shippingtype == True) || ( billingtype == True && shippingtype == True ) || ( billingtype == True && shippingtype == True && Physicaltype == TRUE  ) || (shippingtype == TRUE && Physicaltype == TRUE ) ) && sAddressLine4!= '') {
                   if( (ERPId != '' && ERPId != NULL ) && ( (shippingtype == True) || ( billingtype == True && shippingtype == True ) || ( billingtype == True && shippingtype == True && Physicaltype == TRUE  ) || (shippingtype == TRUE && Physicaltype == TRUE ) ) && sAddressLine4!= '') {
                   myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.Address4ValError);
                   ApexPages.addMessage(myMsg);
                   return true;
                   }
                 } 
                //CR-02680-end
                  
                //Added To get existing Accountaddress Address_type ="Billing"/"Billing and Shipping"
                //if (sAddressType.contains('Billing')){
                  if (billingtype==true){
                    List<Account_Addresses__c> existingAccaddr = new List<Account_Addresses__c>();
                    //existingAccaddr = [select id,name,Address_Type__c,Primary__c,PrimaryShipping__c from Account_Addresses__c where 
                      existingAccaddr = [select id,name,Billing__c,Shipping__c,Physical__c,Primary_Physical__c,Primary__c,PrimaryShipping__c from Account_Addresses__c where 
                             //Account__c=:accRecordId AND (Address_Type__c='Billing' OR Address_Type__c='Billing and Shipping')]; 
                               Account__c=:accRecordId AND (Billing__c=true)];                           
                             // Financial_Account__c=: recordId and (Address_Type__c='Billing' OR Address_Type__c='Billing and Shipping')];
                             //Above line alread commented
                    if(!existingAccaddr.isEmpty()){
                        myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.AddressTypeError);
                        ApexPages.addMessage(myMsg);
                        return true;
                    }
                }     
                
                
                //CR-02397 - Start
                if (Physicaltype ==true && bPrimaryphyFlg==true){
                    List<Account_Addresses__c> existingAccaddr = new List<Account_Addresses__c>();
                    //existingAccaddr = [select id,name,Address_Type__c,Primary__c,PrimaryShipping__c from Account_Addresses__c where 
                      existingAccaddr = [select id,name,Billing__c,Shipping__c,Physical__c,Primary__c,PrimaryShipping__c,Primary_Physical__c from Account_Addresses__c where 
                             //Account__c=:accRecordId AND (Address_Type__c='Billing' OR Address_Type__c='Billing and Shipping')]; 
                               Account__c=:accRecordId AND (Physical__c=true)];                           
                             // Financial_Account__c=: recordId and (Address_Type__c='Billing' OR Address_Type__c='Billing and Shipping')];
                             //Above line alread commented
                   /*
                    if(!existingAccaddr.isEmpty()){
                        myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.PS_PhysicalAddrDupicate);
                        ApexPages.addMessage(myMsg);
                        return true;
                    }
                    */
                }                                   
                //CR-02397 - End
                
               // if (sAddressType.contains('Shipping') && bPrimaryShippingFlg){
                  if (shippingtype==true && bPrimaryShippingFlg){
                    List<Account_Addresses__c> existingAccaddr = new List<Account_Addresses__c>();
                   // for(Account_Addresses__c objAccaddr : [select id,name,Address_Type__c,Primary__c,PrimaryShipping__c from Account_Addresses__c where 
                      for(Account_Addresses__c objAccaddr : [select id,name,Billing__c,Shipping__c,Physical__c,Primary_Physical__c,Primary__c,PrimaryShipping__c from Account_Addresses__c where
                          // Account__c=:accRecordId AND (Address_Type__c='Billing' OR Address_Type__c='Billing and Shipping')])   { 
                             Account__c=:accRecordId AND (billing__C=true)])   {
                           //       Financial_Account__c=: recordId and (Address_Type__c='Shipping' OR Address_Type__c='Billing and Shipping')]){
                           //Above line already commented
                        if(objAccaddr.PrimaryShipping__c){
                            objAccaddr.PrimaryShipping__c = false;
                            existingAccaddr.add(objAccaddr);
                        }
                     }
                     if (existingAccaddr.size()>0){
                         try{
                             update existingAccaddr;
                         }
                         catch(Exception e){
                            myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error in updating existing shipping address: '+e.getMessage());
                            ApexPages.addMessage(myMsg);
                            return true;                         
                         }
                     }
                         
                }  //to flip primary shipping address                           
            }
            else{
                if (sCountry == '' || sCountry == 'None' || sCity == ''){
                    myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Country and City must be populated to save the address record.');
                    ApexPages.addMessage(myMsg);
                    return true;
                }
            }
            
        }
        
        return false;
    }
    //Method to call search address service
    public void mSearchAddress() {
        if (sObjectType=='SampleOrder'){
                    if (oOrderCont.addLine1== null){sAddressLine1 = '';}else{sAddressLine1 = oOrderCont.addLine1;}
                    if (oOrderCont.addLine2 == null){sAddressLine2 = '';}else{sAddressLine2 = oOrderCont.addLine2;}
                    if (oOrderCont.addLine3 == null){sAddressLine3 = '';}else{sAddressLine3 = oOrderCont.addLine3;}
                    if (oOrderCont.addLine4 == null){sAddressLine4 = '';}else{sAddressLine4 = oOrderCont.addLine4;}
                    if (oOrderCont.tempOrder.ShippingCountryCode == null){sCountry = '';}else{sCountry = oOrderCont.tempOrder.ShippingCountryCode;}
                    if (oOrderCont.tempOrder.ShippingStateCode == null){sState= '';}else{sState = oOrderCont.tempOrder.ShippingStateCode;}
                    if (oOrderCont.addCity == null){sCity= '';}else{sCity= oOrderCont.addCity;}
                    if (oOrderCont.addPostalCode == null){sZipCode = '';}else{sZipCode  = oOrderCont.addPostalCode;}                                                                                                                        
        }       
        Boolean bValErr = mAddressValidation(true);
        bShowMatchCode = false;                
        if (!bValErr){   
            oAddrResponseList = new List<AddressWrapper>();
            oAddrResponseList.clear();
            try{
                oAddrResponseList = AddressSearchHelper.mCallSearchService(sAddressLine1,sAddressLine2,sAddressLine3,
                                                                           sAddressLine4,sCity,sState,sCounty,sCountry,sZipCode);
                bShowMatchCode  = OneCRMSpecialPermissions__c.getInstance(UserInfo.getUserId()).ViewAddressMatchCode__c;                                                                          
                if (oAddrResponseList.size() > 0){
                    bShowSearchResult = true;
                }
                else{
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,Label.InvalidAdderssSrchMsg);
                    ApexPages.addMessage(myMsg);    
                    bShowSearchResult = true;  
                }                                                                          
                     
            }
            catch(Exception e){
            if(e.getMessage().contains('IO Exception')){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.IO_Exception);
                ApexPages.addMessage(myMsg); 
                }           
            }                                                                                    
        }                                                                                        
    }    
    //Method for cancel operation
    public PageReference mCancel(){
        PageReference onewRef;
        //RG:23/04/2016 added condition for sobject check
        if(sObjectType == 'Quote'){
           // onewRef = new PageReference('/apex/FinancialAccountSelection');
            onewRef = new PageReference('/apex/FinancialAccountSelection?&Id='+sQuoteId);
        }else{
            onewRef = new PageReference('/'+recordId);
        }
        onewRef.setRedirect(true);
        return onewRef;
    }   
     
    //Method to save address and return back to parent page
    public PageReference mSaveAddress(){    
        PageReference onewRef;
        Boolean bValErr = mAddressValidation(false);
        if (!bValErr){
          try
            {
                Lead oUpLead;
                Contact oUpContact;
                
                String sStreet = sAddressLine1;
                if (sAddressLine2  != ''){sStreet  = sStreet  + '\n' + sAddressLine2;}
                if (sAddressLine3  != ''){sStreet  = sStreet  + '\n'+ sAddressLine3;}
                if (sAddressLine4  != ''){sStreet  = sStreet  + '\n' +sAddressLine4;}

                if (sObjectType == 'Lead'){
                    oUpLead = new Lead(Id=recordId);
                    oUpLead.Street =  sStreet;
                    oUpLead.City=sCity;
                    if (sCountry != null)
                        oUpLead.CountryCode = sCountry;
                    else
                        oUpLead.CountryCode = oAcc.BillingCountryCode;
                        
                    // Manimala :IRUK - Added for CR-2483 - code starts
                    if(sCountry =='United Kingdom')
                      oUpLead.county__C=sCounty;
                    else
                        oUpLead.StateCode=sState;
                     
                     // Manimala :IRUK - Added for CR-2483 - code ends
                    oUpLead.PostalCode=sZipCode;
                    oUpLead.Validated__c=sAddValStatus;
                    update oUpLead;
                }//end Lead
                else if (sObjectType == 'Contact'){
                    oUpContact= new Contact(Id=recordId);
                    if (sAddressType == 'Mailing'){
                        oUpContact.MailingStreet =  sStreet;
                        oUpContact.MailingCity=sCity;
                        if (sCountry != null)
                            oUpContact.MailingCountryCode = sCountry;
                        else
                            oUpContact.MailingCountryCode = oAcc.BillingCountryCode;
                        
                        // Manimala :IRUK - Added for CR-2483 - code starts
                        if(sCountry =='United Kingdom')
                        oUpContact.Mailing_County__c=sCounty;
                        else
                          oUpContact.MailingStateCode=sState;
                          
                         // Manimala :IRUK - Added for CR-2483 - code ends
                        //oUpContact.MailingState=sState;
                        oUpContact.MailingPostalCode=sZipCode;
                        oUpContact.MailingAddressValidated__c=sAddValStatus;}
                    else{
                        oUpContact.OtherStreet =  sStreet;
                        oUpContact.OtherCity=sCity;
                        if (sCountry != null)
                            oUpContact.OtherCountryCode = sCountry;
                        else 
                            oUpContact.OtherCountryCode = oAcc.BillingCountryCode;
                         if(sCountry =='United Kingdom')
                        oUpContact.Other_County__c=sCounty;
                        else   
                          oUpContact.OtherStateCode=sState;
                        oUpContact.OtherPostalCode=sZipCode;
                        oUpContact.OtherAddressValidated__c=sAddValStatus;}  
                    
                     update oUpContact;              
                 }  //end contact     
                 else{
                     //Fetching country and state name from response codes

                     Schema.DescribeFieldResult ctryfieldResult = Account.BillingCountrycode.getDescribe();
                     List<Schema.PicklistEntry> objCtry = ctryfieldResult.getPicklistValues();
                     Schema.DescribeFieldResult statefieldResult = Account.BillingStatecode.getDescribe();
                     List<Schema.PicklistEntry> objState = statefieldResult.getPicklistValues();                 
                     if (sCountry != null)
                     {
                        String sctryValue='';
                        String sstateValue='';
          
           Map<String,String> mapCountry = StateandCountryMap.CountryDescCodeMap();
           Map<String,String> mapState = StateandCountryMap.StateDescCodeMap();
           
          sctryValue= countrydescription;
                    sstateValue=statedescription;
          
                      /*  for( Schema.PicklistEntry f : objCtry){
                            if(f.getValue() == sCountry){
                                sctryValue=f.getLabel();
                                break;
                            }                            
                        }     
                        for( Schema.PicklistEntry f : objState){
                            if(f.getValue() == sState){
                                sstateValue=f.getLabel();
                                break;
                            }                            
                        }  */
                         // Manimala :IRUK - Added county field for CR-2483 - code starts
                         if(sctryValue=='United Kingdom')  {
                          objAccaddr=new Account_Addresses__c(/*Financial_Account__c=recordId,*/Account__c=accRecordId,Address_1__c=sAddressLine1,Address_2__c=sAddressLine2,Address_3__c=sAddressLine3,
                                                    Address_4__c=sAddressLine4,City__c=sCity,Country__c=sctryValue,County__c=sCounty,Zip_Code__c=sZipCode,Physical__c=Physicaltype,Primary_Physical__c=bPrimaryphyFlg,
                                                    //Validated__c=sAddValStatus,Address_Type__c=sAddressType,Source__c='One 
                                                    Validated__c=sAddValStatus,billing__c=billingtype,shipping__c=shippingtype,Source__c='One  CRM',Primary__c=bPrimaryFlg,PrimaryShipping__c=bPrimaryShippingFlg,Country_Code__c=sCountry);   
                          
                         }
                         else{
                        objAccaddr=new Account_Addresses__c(/*Financial_Account__c=recordId,*/Account__c=accRecordId,Address_1__c=sAddressLine1,Address_2__c=sAddressLine2,Address_3__c=sAddressLine3,
                                                    Address_4__c=sAddressLine4,City__c=sCity,Country__c=sctryValue,State__c=sstateValue,Zip_Code__c=sZipCode,Physical__c=Physicaltype,Primary_Physical__c=bPrimaryphyFlg,
                                                    //Validated__c=sAddValStatus,Address_Type__c=sAddressType,Source__c='One 
                                                    Validated__c=sAddValStatus,billing__c=billingtype,shipping__c=shippingtype,Source__c='One  CRM',Primary__c=bPrimaryFlg,PrimaryShipping__c=bPrimaryShippingFlg,State_Code__c=sState,Country_Code__c=sCountry);   
                         }
                       if(AccountTepLevelValue=='Site' )
                       {
                           if( sctryValue=='United Kingdom'){
                            objAccaddr=new Account_Addresses__c(/*Financial_Account__c=recordId,*/Account__c=accRecordId,Address_1__c=sAddressLine1,Address_2__c=sAddressLine2,Address_3__c=sAddressLine3,
                                                    Address_4__c=sAddressLine4,City__c=sCity,Country__c=sctryValue,County__c=sCounty,Zip_Code__c=sZipCode,Physical__c=Physicaltype,Primary_Physical__c=bPrimaryphyFlg,
                                                   // Validated__c=sAddValStatus,Address_Type__c=sAddressType,Source__c='One
                                                      Validated__c=sAddValStatus,billing__c=billingtype,shipping__c=shippingtype,Source__c='One  CRM',Primary__c=bPrimaryFlg,PrimaryShipping__c=bPrimaryShippingFlg,Country_Code__c=sCountry);   
                               
                        }
                        else{
                           objAccaddr=new Account_Addresses__c(/*Financial_Account__c=recordId,*/Account__c=accRecordId,Address_1__c=sAddressLine1,Address_2__c=sAddressLine2,Address_3__c=sAddressLine3,
                                                    Address_4__c=sAddressLine4,City__c=sCity,Country__c=sctryValue,State__c=sstateValue,Zip_Code__c=sZipCode,Physical__c=Physicaltype,Primary_Physical__c=bPrimaryphyFlg,
                                                   // Validated__c=sAddValStatus,Address_Type__c=sAddressType,Source__c='One
                                                      Validated__c=sAddValStatus,billing__c=billingtype,shipping__c=shippingtype,Source__c='One  CRM',Primary__c=bPrimaryFlg,PrimaryShipping__c=bPrimaryShippingFlg,State_Code__c=sState,Country_Code__c=sCountry);   
                           }
                       }                             
                                                    
                    }                                                                
                    else    
                        objAccaddr=new Account_Addresses__c(/*Financial_Account__c=recordId,*/Account__c=accRecordId,Address_1__c=sAddressLine1,Address_2__c=sAddressLine2,Address_3__c=sAddressLine3,Address_4__c=sAddressLine4,
                        City__c=sCity,Country__c=oAcc.BillingCountryCode,State__c=sState,Zip_Code__c=sZipCode,Validated__c=sAddValStatus,billing__c=billingtype,shipping__c=shippingtype,Source__c='One CRM' 
                                                   ,Primary__c=bPrimaryFlg,PrimaryShipping__c=bPrimaryShippingFlg);            
                  // Validated__c=sAddValStatus,Address_Type__c=sAddressType,Source__c='One
                   // Manimala :IRUK - Added county field for CR-2483 - code starts
                    if (sFinAccStatus == 'Active' || sFinAccStatus == 'Pending' )
                        objAccaddr.ERP_Address_Status__c = 'Pending';
                    else if(sFinAccStatus == 'New')
                        objAccaddr.ERP_Address_Status__c = 'New';
                    else
                        objAccaddr.ERP_Address_Status__c = 'Inactive';
                     //CR-02863- Start - to update account address TEP OU with loggedIn user TEP OU
                     if(loggedInUserList.size() >0){
                         objAccaddr.TEP_Operating_Unit__c = loggedInUserList[0].TEP_Operating_Unit__c;
                     }
                     //CR-02863- End
                    insert objAccaddr;        
                                          
                 }   //end Account and Quote
       
                 if(sObjectType == 'Quote')
                    onewRef = new PageReference('/apex/FinancialAccountSelection?Id='+sQuoteId+'&FAId='+recordId+'&AddId='+objAccaddr.id);         
                 else   
                    onewRef = new pagereference('/'+recordId); 
                 onewRef.setRedirect(true);
                 return onewRef; 
                
            }//end try
           catch(Exception e)
            {
            
            if(AccountTepLevelValue=='Site' && objAccaddr.Address_4__c!='' && e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Address Line 4 only cannot be populated for top level accounts.');
                ApexPages.addMessage(myMsg); 
            }
                else
                //Adding the Start of code for CR-2502 Laxmanrao
                if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') && e.getMessage().contains('Please select a picklist value for State/Province'))
                {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a picklist value for State/Province');
                ApexPages.addMessage(myMsg);
                }
                //Adding the End of code for CR-2502 Laxmanrao
                else if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                {
                
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Full Address Must be Populated.');
                        ApexPages.addMessage(myMsg); 
                }
                
                else{
                       ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,Label.DuplicateDetector);
                       ApexPages.addMessage(msg);
                }
                //throw(e);
                return null;
            }       
        }   //end not valid error
        else
            return null;  

    }
    
}