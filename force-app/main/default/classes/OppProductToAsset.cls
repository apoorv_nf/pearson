/***************************************************************************************************************************************************************************************************
 Name:            OppProductToAsset.cls
 Description:     Class to convert opportunity line items to Product in use.
 Date             Version              Author                          Summary of Changes
****************************************************************************************************************************************************************************************************
 01/06/2016                            Sneha                   DR-0389: Method added to convert Competitor Products to PIU when Opportunity stage is lost.      
 16/06/2016                            Sowmya                  DR-0639: Change sort order for Opportunity Products in "Convert to PIU" custom page      
 20/06/2016                            Pooja                   DR-0381: Method added to make old piu inactive and convert to piu.  
 10/04/2017                            Cristina                INC3336943 - Error 101 SOQL Queries - we had a query within a for loop that was not necessary                                                                     
 28/06/2017                            Charitha                CRMSFDC-368 - Modified SetOldPiu method inorder to create Competitor Product records for the inactivated PIU records
                                                                             Updating the usageEndDate field on Competitor Product object and Asset object as well.
****************************************************************************************************************************************************************************************************/


global class OppProductToAsset {
    //Class to convert opportunity line items to Product in use.
    private Static Opportunity opportunity;
    private static Account account;
    //Public Id opptyID;
    private static List <OpportunityLineItem> olis {get;set;}
    private Static List<Asset> assets = new List<Asset>();
    // List of Opportunity University Courses
    public static list<OpportunityUniversityCourse__c> oucs {get;set;} 
    public static list<OpportunityUniversityCourse__c> oldpiu{get;set;}  
    private static List<OpportunityContactRole> ocrs {get;set;}
    
     //method called from button click to convert opportunity line items to assets 
    webservice static void convertToAsset(Id oppId,Id accId) {
         try {
            opportunity = new Opportunity(id=oppId);
            account = new Account(id=accId);
            if(opportunity.ID == null) {
            }
            setOLIS();
            setOUCS();
            convert();
         } catch (Exception e) {
            throw e;
         }
            
    }
    // Method added by Pooja for DR-381
     webservice static void markInactiveconvertToAsset(Id oppId,Id accId) {
         try {
            opportunity = new Opportunity(id=oppId);
            account = new Account(id=accId);
            if(opportunity.ID == null) {
            }
            setoldPiu();   
            setOUCS();
            setOLIS();
            convertInactive();
        } catch (Exception e) {
            throw e;
         }
        
    }
    // Method added by Pooja for DR-381
    public static void setoldPiu() {
        try {
            setContactRoles();
            List<Asset> updateAsset = new List<Asset>();
            Set<Asset> updateAssetSet = new Set<Asset>();
            List<Asset> updateAllAssest = new List<Asset>();  
            Set<Id> assetProdIds = new Set<Id>();
            //CP 04/10/2017 To eliminate 101 soql queries we removed the query in the for loop
             List<Asset> oldAssets=[select id,name,status__c,Primary__c,Contact.Name,product2Id,ContactId from asset where Status__c='Active' and Course__c IN (Select o.UniversityCourse__c from    OpportunityUniversityCourse__c o  where   o.Opportunity__c=:opportunity.ID)  ];
            //oldpiu=[Select o.UniversityCourse__c from    OpportunityUniversityCourse__c o  where   o.Opportunity__c=:opportunity.ID];  
            //for(OpportunityUniversityCourse__c o : oldpiu){
                //if(o.UniversityCourse__c!=null){
                    //List<Asset> oldAssets=[select id,name,status__c,Primary__c,Contact.Name,ContactId from asset where Course__c =:o.UniversityCourse__c and Status__c='Active' ];
            if(oldAssets!=null && oldAssets.size()>0){
                for(Asset ur : oldAssets){
                    ur.Primary__c = false ;
                    if(ocrs.size()>0){
                      for(OpportunityContactRole ocr : ocrs){
                            if(ur.ContactId == ocr.ContactId){
                                ur.Status__c ='InActive';
                                ur.UsageEndDate = system.today();
                                assetProdIds.add(ur.product2Id);
                            } 
                             updateAsset.add(ur);
                            //update oldAssets;

                        }   
                           
                      }
                }
            }
                

                       
                //}
            //}
            if(updateAsset.size()>0 && updateAsset!=null){
                updateAssetSet.addAll(updateAsset);
                updateAllAssest.addAll(updateAssetSet);
                Update updateAllAssest;
                
                if(assetProdIds.size()>0){
                if(!PS_Util.isFetchedUserRecord)
                PS_Util.getUserDetail(Userinfo.getUserid());
                User loggedInUser = PS_Util.loggedInUser;
                List<Product2> productList = [Select id,name from Product2 where id in :assetProdIds and Competitor_Product__c=true and Market__c=:loggedInUser.Market__c and Line_of_Business__c=:loggedInUser.Line_of_Business__c and Business_Unit__c=:loggedInUser.Product_Business_Unit__c];
                if(productList!=null){
                 List<Competitor_Product__c > compProductList = new List<Competitor_Product__c >();
                 Competitor_Product__c compProduct;
                    for(Product2 assetProdId : productList){
                    compProduct = new Competitor_Product__c();
                    compProduct.Opportunity__c = opportunity.Id;
                    compProduct.Product__c = assetProdId.id;
                    compProduct.Usage_End_Date__c = System.today();
                    compProductList.add(compProduct);
                    }
                 insert compProductList;
                 }
                }

            } 
        }
        catch (Exception e) {
            throw e;
        }
    }
    
    //Method definition to get opportunity line items.
    public List<OpportunityLineItem> getOlis() {
       return olis;
    }
    
     /**
    ** setOUCS: runs query on Opportunity University Course table to retrieve University Courses on opportunity 
    **/
    public static void setOUCS() {
        try {
            oucs=[Select    o.Id, 
                            o.Opportunity__c, 
                            o.UniversityCourse__c,
                            o.name, //Added
                            o.Opportunity__r.Primary_Contact__c //added
                            
                    from    OpportunityUniversityCourse__c o  
                    where   o.Opportunity__c=:opportunity.ID];        
        } catch (DmlException e) {
            for(Integer i = 0; i < e.getNumDml(); i++)
                System.debug(e.getDmlMessage(i));
        } catch (Exception e) {
            throw e;
        }
    }
    //Method definition to pull opportunity line items
      public static void setOLIS() {
        try {
            olis=[Select    o.Id, 
                            o.IsDeleted, 
                            o.ListPrice,
                            o.OpportunityId, 
                            o.Opportunity.CloseDate,
                            o.Opportunity.Name, //added Opportunity Name for concatenation in Asset Name
                            o.PricebookEntry.Product2Id,
                            o.PricebookEntryId, 
                            o.PricebookEntry.Name, 
                            o.Quantity, 
                            o.ServiceDate, //Relabeled to the Forecasted Install Date
                            o.TotalPrice, 
                            o.UnitPrice, 
                            //o.Forecasted_Install_Date__c, //Forecasted Install Date field; Displaying in Visualforce page
                            o.Sales_Channel__c, //Sales Channel Picklist field; Displaying in Visualforce page
                            o.Bookshop__c,  //Bookshop Lookup field
                            o.Expected_Sales_Quantity__c,
                            o.Required_by_Date__c, 
                            o.Suggested_Order_Quantity__c,
                            o.Discount,
                            o.Description,
                            o.OppProductCreateddate__c,
                            o.createddate,
                            o.Mode_of_Delivery__c  //Mode of Delivery Picklist field; Displaying in Visualforce page
                    from    OpportunityLineItem o  
                    where   o.IsDeleted=false and 
                            o.OpportunityId=:opportunity.ID ];
        
        
        } catch (DmlException e) {
            for(Integer i = 0; i < e.getNumDml(); i++)
               System.debug(e.getDmlMessage(i));
        } catch (Exception e) {
            throw e;
        }
    }
    //Method definition to pull contact details along with their role definitions.
    private static void setContactRoles(){
        try {
            ocrs = [select ContactId,Contact.Name from OpportunityContactRole where OpportunityId =:opportunity.id];
        }
        catch (DmlException e) {
            for(Integer i = 0; i < e.getNumDml(); i++)
               System.debug(e.getDmlMessage(i));
        } catch (Exception e) {
            throw e;
        }
    }
    //Method definition to create Product In Use
    public static void setAssetList() {
        try {
            setContactRoles();
            Integer counter=0;
            Asset a1, b1;
            string combinednames;
            if(!olis.isEmpty()) {
                for(OpportunityLineItem o: olis) {
                    if(ocrs.size()>0){
                        for(OpportunityContactRole ocr : ocrs){
                            a1 = new Asset();
                            a1.Name                     = o.PriceBookEntry.Name;
                            a1.Product2Id               = o.PriceBookEntry.Product2Id;
                            a1.AccountId                = account.Id;
                            a1.Purchase_Opportunity__c  = opportunity.Id;
                            a1.ContactId                = ocr.ContactId;
                            a1.InstallDate              = system.today();
                            if(o.OppProductCreateddate__c!= null)
                            a1.OpportunityAssetCreateDate__c = o.OppProductCreateddate__c;
                            else
                             a1.OpportunityAssetCreateDate__c = o.Createddate ;
                            a1.Converted_Asset__c       = true;
                          if(o.Discount!=null){
                            a1.Discount__c              =o.Discount;
                             }
                     //       if(o.TotalPrice == maxoli.TotalPrice) 
                     //       a1.Primary__c = true;
                            assets.add(a1);
                        }
                    }
                    else{
                        a1 = new Asset();
                            a1.Name                     = o.PriceBookEntry.Name;
                            a1.Product2Id               = o.PriceBookEntry.Product2Id;
                            a1.AccountId                = account.Id;
                            a1.Purchase_Opportunity__c  = opportunity.Id;
                            a1.Contact                  = null;
                            a1.InstallDate              = system.today();
                            a1.Converted_Asset__c       = true;
                           if(o.OppProductCreateddate__c!= null)
                            a1.OpportunityAssetCreateDate__c = o.OppProductCreateddate__c;
                            else
                             a1.OpportunityAssetCreateDate__c = o.Createddate ;
                            if(o.Discount!=null){
                            a1.Discount__c              =o.Discount;
                             }
                         //   if(o.TotalPrice == maxoli.TotalPrice) 
                         //   a1.Primary__c = true;
                             assets.add(a1);
                    }
                } 
            //insert assets;
            }
        }catch (Exception e){
            throw e;
        }
        
    }
    public static void setAssetListInactive() {
        try {
            setContactRoles();
            Integer counter=0;
            Asset a1, b1;
            string combinednames;
            OpportunityLineItem maxoli = [SELECT id,TotalPrice from opportunitylineitem where opportunityid =:opportunity.Id order by totalprice desc limit 1];
            if(!olis.isEmpty()) {
            
                for(OpportunityLineItem o: olis) {
                    if(ocrs.size()>0){
                        for(OpportunityContactRole ocr : ocrs){
                            a1 = new Asset();
                            a1.Name                     = o.PriceBookEntry.Name;
                            a1.Product2Id               = o.PriceBookEntry.Product2Id;
                            a1.AccountId                = account.Id;
                            a1.Purchase_Opportunity__c  = opportunity.Id;
                            a1.ContactId                = ocr.ContactId;
                            a1.InstallDate              = system.today();
                            if(o.OppProductCreateddate__c!= null)
                            a1.OpportunityAssetCreateDate__c = o.OppProductCreateddate__c;
                            else
                             a1.OpportunityAssetCreateDate__c = o.Createddate ;
                            a1.Converted_Asset__c       = true;
                          if(o.Discount!=null){
                            a1.Discount__c              =o.Discount;
                             }
                            if(o.TotalPrice == maxoli.TotalPrice) 
                            a1.Primary__c = true;
                            assets.add(a1);
                        }
                    }
                    else{
                        a1 = new Asset();
                            a1.Name                     = o.PriceBookEntry.Name;
                            a1.Product2Id               = o.PriceBookEntry.Product2Id;
                            a1.AccountId                = account.Id;
                            a1.Purchase_Opportunity__c  = opportunity.Id;
                            a1.Contact                  = null;
                            a1.InstallDate              = system.today();
                            a1.Converted_Asset__c       = true;
                           if(o.OppProductCreateddate__c!= null)
                            a1.OpportunityAssetCreateDate__c = o.OppProductCreateddate__c;
                            else
                             a1.OpportunityAssetCreateDate__c = o.Createddate ;
                            if(o.Discount!=null){
                            a1.Discount__c              =o.Discount;
                             }
                            if(o.TotalPrice == maxoli.TotalPrice) 
                            a1.Primary__c = true;
                             assets.add(a1);
                    }
                } 
            //insert assets;
            }
        }catch (Exception e){
            throw e;
        }
        
    }
    
    //Sneha - DR-0389: Method added to convert Competitor Products to PIU when Opportunity stage is lost.
    
    webservice static void CompProdConvertToAsset(Id oppId,Id accId) {
    try{
        opportunity = new Opportunity(id=oppId);
        account = new Account(id=accId);
        List<Competitor_Product__c> competitorProd = [select id,name,Opportunity__c,Product__c,Author__c,Edition__c,Copyright_Year__c from Competitor_Product__c where Opportunity__c =:opportunity.Id];
        List<OpportunityUniversityCourse__c> opuc = [Select Id,Opportunity__c, UniversityCourse__c from OpportunityUniversityCourse__c where Opportunity__c=:opportunity.ID ]; 
        Asset a1;
        setContactRoles();
        if(!competitorProd.isEmpty()) { 
            for(Competitor_Product__c cp: competitorProd) {
              for(OpportunityUniversityCourse__c ouc:opuc){
                if(ocrs.size()>0){
                    for(OpportunityContactRole ocr : ocrs){
                   
                                  a1 = new Asset();
                                a1.Name                     = cp.Product__c;
                                a1.Product_Author__c        = cp.Author__c;
                                a1.Product_Edition__c       = cp.Edition__c;
                                a1.Copyright_Year__c        = cp.Copyright_Year__c;
                                a1.Product2Id               = cp.Product__c;
                                a1.AccountId                = account.Id;
                                a1.Purchase_Opportunity__c  = opportunity.Id;
                                a1.ContactId                = ocr.ContactId;     
                                a1.Course__c =                ouc.UniversityCourse__c;
                                a1.InstallDate              = system.today();
                                a1.Converted_Asset__c       = true;
                                a1.Discount__c              = null;
                                a1.Status__c                = 'Active';            
                                a1.Is_Competitor_Product__c = true;
                                assets.add(a1); 
                    }
                                
                }
                else{
                                a1 = new Asset();
                                a1.Name                     = cp.Product__c;
                                a1.Product_Author__c        = cp.Author__c;
                                a1.Product_Edition__c       = cp.Edition__c;
                                a1.Copyright_Year__c        = cp.Copyright_Year__c;
                                a1.Product2Id               = cp.Product__c;
                                a1.AccountId                = account.Id;
                                a1.Purchase_Opportunity__c  = opportunity.Id;
                                a1.ContactId                = null;
                                a1.Course__c                = ouc.UniversityCourse__c;
                                a1.InstallDate              = system.today();
                                a1.Converted_Asset__c       = true;
                                a1.Discount__c              = null;
                                a1.Status__c                = 'Active';
                                a1.Is_Competitor_Product__c = true;
                                 assets.add(a1);
                                
                }   
               }                
            } 
                 
        }
        if(!assets.isEmpty()){
        insert assets;
       
        }
        opportunity.Product_Converted__c = true;
                update opportunity;
    }
    
    catch(Exception e){
            throw e;
        }
    }
    // Method added by Pooja for DR-381
    webservice static void MarkInactiveCompProdConvertToAsset(Id oppId,Id accId) {
        
    try{
        opportunity = new Opportunity(id=oppId);
        account = new Account(id=accId);
        List<Competitor_Product__c> competitorProd = [select id,name,Opportunity__c,Product__c,Author__c,Edition__c,Copyright_Year__c from Competitor_Product__c where Opportunity__c =:opportunity.Id];
        List<OpportunityUniversityCourse__c> opuc = [Select Id,Opportunity__c, UniversityCourse__c from OpportunityUniversityCourse__c where Opportunity__c=:opportunity.ID ]; 
        List<Competitor_Product__c> competitorProdInactive = [select id,name from Competitor_Product__c where Opportunity__c =:opportunity.Id LIMIT 1];
        Asset a1;
        setContactRoles();
        if(!competitorProd.isEmpty()) { 
            setoldPiu();
            for(Competitor_Product__c cp: competitorProd) {
              for(OpportunityUniversityCourse__c ouc:opuc){
                if(ocrs.size()>0){
                    for(OpportunityContactRole ocr : ocrs){
                   
                                  a1 = new Asset();
                                a1.Name                     = cp.Product__c;
                                a1.Product_Author__c        = cp.Author__c;
                                a1.Product_Edition__c       = cp.Edition__c;
                                a1.Copyright_Year__c        = cp.Copyright_Year__c;
                                a1.Product2Id               = cp.Product__c;
                                a1.AccountId                = account.Id;
                                a1.Purchase_Opportunity__c  = opportunity.Id;
                                a1.ContactId                = ocr.ContactId;     
                                a1.Course__c =                ouc.UniversityCourse__c;
                                a1.InstallDate              = system.today();
                                a1.Converted_Asset__c       = true;
                                a1.Discount__c              = null;
                                a1.Status__c                = 'Active';            
                                a1.Is_Competitor_Product__c = true;
                                if(!competitorProd.isEmpty()){
                                if((cp.id).equals(competitorProdInactive[0].id))
                                a1.primary__c = true ;
                                }
                                assets.add(a1); 
                    }
                                
                }
                else{
                                a1 = new Asset();
                                a1.Name                     = cp.Product__c;
                                a1.Product_Author__c        = cp.Author__c;
                                a1.Product_Edition__c       = cp.Edition__c;
                                a1.Copyright_Year__c        = cp.Copyright_Year__c;
                                a1.Product2Id               = cp.Product__c;
                                a1.AccountId                = account.Id;
                                a1.Purchase_Opportunity__c  = opportunity.Id;
                                a1.ContactId                = null;
                                a1.Course__c                = ouc.UniversityCourse__c;
                                a1.InstallDate              = system.today();
                                a1.Converted_Asset__c       = true;
                                a1.Discount__c              = null;
                                a1.Status__c                = 'Active';
                                a1.Is_Competitor_Product__c = true;
                                if(!competitorProd.isEmpty()){
                                if((cp.id).equals(competitorProdInactive[0].id))
                                a1.primary__c = true ;
                                }
                                 assets.add(a1);
                                
                }   
               }                
            } 
                 
        }
        if(!assets.isEmpty()){
        insert assets;
       
        }
       opportunity.Product_Converted__c = true;
                update opportunity;
    }
     
    
    catch(Exception e){
            throw e;
        }
    }
    
    
   // public PageReference convert() {
      public static void convert() {
        
        try {
            Integer num_of_assets = 0;
     
            // Setup the assets to be upserted
            setAssetList();
            
            num_of_assets = assets.size(); 
            
            if(num_of_assets > 0) {
                // Insert or Update Assets [Upsert]
                
                 Database.UpsertResult[] lur = Database.upsert(assets);
                if(oucs.size()>0){ //don't act unless there are university courses associated with this opportunity
                    id assetid;
                    id universitycourseid;
                    Integer count = 0;
                    List<Asset> Alist = new List<Asset>();
                    for(Database.UpsertResult ur : lur){ //loop through assets that were just upserted
                        assetid= ur.Id;
                        Asset assetrec = new Asset(Id=assetid);
                            count=0; 
                            for(OpportunityUniversityCourse__c ouc : oucs){  //loop through all university courses associated with this opportunity
                                if(count==0)
                                {
                                    OpportunityUniversityCourse__c oucRec = [select name,Opportunity__c, Opportunity__r.Primary_Contact__c,UniversityCourse__c from OpportunityUniversityCourse__c where id =: ouc.id];
                                    //assetrec.ContactId =oucRec.Opportunity__r.Primary_Contact__c;
                                    assetrec.Course__c = oucRec.UniversityCourse__c;
                                    assetrec.Status__c = 'Active';
                                    Alist.add(assetrec);
                                    
                                }
                                count+=1;
                                //universitycourseid = ouc.UniversityCourse__c;                     
                            }
                        
                    }
                    update Alist;
                } 
                opportunity.Product_Converted__c = true;
                update opportunity;
                                              
            }
        } catch (Exception e) {
           throw e;
        }
    }

public static void convertInactive() {
        
        try {
            Integer num_of_assets = 0;
     
            // Setup the assets to be upserted
            setAssetListInactive();
            
            num_of_assets = assets.size(); 
            
            if(num_of_assets > 0) {
                // Insert or Update Assets [Upsert]
                
                 Database.UpsertResult[] lur = Database.upsert(assets);
                if(oucs.size()>0){ //don't act unless there are university courses associated with this opportunity
                    id assetid;
                    id universitycourseid;
                    Integer count = 0;
                    List<Asset> Alist = new List<Asset>();
                    for(Database.UpsertResult ur : lur){ //loop through assets that were just upserted
                        assetid= ur.Id;
                        Asset assetrec = new Asset(Id=assetid);
                            count=0; 
                            for(OpportunityUniversityCourse__c ouc : oucs){  //loop through all university courses associated with this opportunity
                                if(count==0)
                                {
                                    //CP 04/10/2017 To eliminate 101 soql queries we removed the query in the for loop and we use the query from line 113
                                    //OpportunityUniversityCourse__c oucRec = [select name,Opportunity__c, Opportunity__r.Primary_Contact__c,UniversityCourse__c from OpportunityUniversityCourse__c where id =: ouc.id];
                                    //assetrec.ContactId =oucRec.Opportunity__r.Primary_Contact__c;
                                    //assetrec.Course__c = oucRec.UniversityCourse__c;
                                    assetrec.Course__c = ouc.UniversityCourse__c;
                                    assetrec.Status__c = 'Active';
                                    Alist.add(assetrec);
                                    
                                }
                                count+=1;
                                //universitycourseid = ouc.UniversityCourse__c;                     
                           }
                        
                    }
                    update Alist;
                } 
                opportunity.Product_Converted__c = true;
                update opportunity;
            }
        } catch (Exception e) {
           throw e;
        }
    }
}