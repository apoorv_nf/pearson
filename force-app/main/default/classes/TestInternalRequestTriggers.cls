/*******************************************************************************************************************
* Apex Class Name  : TestInternalRequestTriggers 
* Version          : 1.0 
* Created Date     : 17 July 2015
* Function         : Test Class for TestInternalRequestTrigger 
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*                      17/07/2015              Created Initial Version of TestInternalRequestTriggers Class
*******************************************************************************************************************/

@isTest
private class TestInternalRequestTriggers
{
    @testsetup
    public static void buildTestData(){
        Profile pf= [Select Id from profile where Name='System Administrator']; 
        User admnUser=new User(firstname = 'ABC', 
                         lastName = 'XYZ', 
                         email = 'testadmnuser@pearson.com' , 
                         Username = 'testadmnuser@pearson1.com' , 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = 'testUser', 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         ProfileId = pf.Id,isactive=false);
                        insert admnUser;
    }
    static testMethod void myUnitTest()
    {
        // Create Test Record      
        Internal_Request__c  sIRequest   = new Internal_Request__c();
        User uTest1 = new User();
         String AcctRecordTypeId;
         String irRecordTypeId;
         List<RecordType> RecordTypeList = new List<RecordType>([select Id,Name from RecordType]);
         
        for (RecordType i : RecordTypeList) {
          if (i.Name == 'Bookstore') {
            AcctRecordTypeId = i.Id;
          }
          if(i.Name == 'Sales Crediting & Territory Management') {
              irRecordTypeId = i.Id;
          }
        }     
        Account bkshop1  = new Account (name = 'Test Bookshop1', BillingStreet = 'Street1',
                   BillingCity = 'Sydney', BillingPostalCode = 'ABC DEF', BillingCountry = 'Australia',
                    //Vista_Account_Number__c = 'xyz',
                    RecordTypeId= AcctRecordTypeId );
        insert bkshop1;     
        utest1 = [Select Id, IsActive FROM User where IsActive = true and Profile.Name='System Administrator' limit 1    ];
        //a = [Select Id, Name FROM Account limit 1    ];
        sIRequest.Adjustment_Type__c     = 'Registration';
        sIRequest.Sales_Person_Name__c   = utest1.Id; //'005g0000002CEEP'; 
        sIRequest.Account_Name__c        = bkshop1.id; // a.Id;
        sIRequest.Subject__c             = 'Australia';
        sIRequest.Description__c         = 'Victoria';
        sIRequest.ISBN__c                = '12345';
        sIRequest.Requested_Unit_Count__c= 1;
        sIRequest.RecordTypeId = irRecordTypeId;
           
        // Insert Test Record
        insert sIRequest;
       
        // Start Test
        Test.startTest();
        // Recall inserted Record
            siRequest = [SELECT OwnerId, Sales_Person_Name__c FROM Internal_Request__c WHERE Id =:sIRequest.Id];
        // Simulate Blank Update (No value is changed)
            sIRequest.Sales_Person_Name__c = sIRequest.Sales_Person_Name__c;
        // Update Record
            update sIRequest;
        // Validate BeforeUpdate Trigger fired
           //system.assertEquals(sIRequest.OwnerId, sIRequest.Sales_Person_Name__c,'Owner Update');
        // Negative test - Sales Person Name is Inactive, Owner Update Fails
        User uTest= new User();
            utest = [Select Id, IsActive FROM User where IsActive = false and Profile.Name='System Administrator' limit 1   ];
            siRequest.Sales_Person_Name__c  = uTest.Id;
            update sIRequest;
            //system.assertNotEquals(sIRequest.OwnerId, sIRequest.Sales_Person_Name__c);
            //system.assertEquals(uTest.Id, sIRequest.Sales_Person_Name__c);
            sIRequest.Requested_Unit_Count__c = null;
        try{
            update sIRequest;
        }catch (Exception e){
           // Assert DML Exception to cover catch block System.Assert(e.getTypeName() == 'DmlException');
        }
        Test.stopTest();
    }
    
    static testMethod void myUnitTest1()
    {
        // Create Test Record      
        Internal_Request__c  sIRequest   = new Internal_Request__c();
        User uTest1 = new User();
         String AcctRecordTypeId;
         String irRecordTypeId1;
                 List<RecordType> RecordTypeList = new List<RecordType>([select Id,Name from RecordType]);
        
        for (RecordType i : RecordTypeList) {
          if (i.Name == 'Bookstore') {
            AcctRecordTypeId = i.Id;
          }
          if(i.Name == 'Contact Request') {
              irRecordTypeId1 = i.Id;
          }
        } 

        Account bkshop1  = new Account (name = 'Test Bookshop1', BillingStreet = 'Street1',
                   BillingCity = 'Sydney', BillingPostalCode = 'ABC DEF', BillingCountry = 'Australia',
                    //Vista_Account_Number__c = 'xyz',
                    RecordTypeId= AcctRecordTypeId );
        insert bkshop1;     
        utest1 = [Select Id, IsActive FROM User where IsActive = true and Profile.Name='System Administrator' limit 1    ];
        //a = [Select Id, Name FROM Account limit 1    ];
        sIRequest.Adjustment_Type__c     = 'Registration';
        sIRequest.Sales_Person_Name__c   = utest1.Id; //'005g0000002CEEP'; 
        sIRequest.Account_Name__c        = bkshop1.id; // a.Id;
        sIRequest.Subject__c             = 'Australia';
        sIRequest.Description__c         = 'Victoria';
        sIRequest.ISBN__c                = '12345';
        sIRequest.Requested_Unit_Count__c= 1;
        sIRequest.RecordTypeId = irRecordTypeId1;
           
        // Insert Test Record
        insert sIRequest;
       
        // Start Test
        Test.startTest();
        // Recall inserted Record
            siRequest = [SELECT OwnerId, Sales_Person_Name__c FROM Internal_Request__c WHERE Id =:sIRequest.Id];
        // Simulate Blank Update (No value is changed)
            sIRequest.Sales_Person_Name__c = sIRequest.Sales_Person_Name__c;
        // Update Record
            update sIRequest;
        // Validate BeforeUpdate Trigger fired
           // system.assertEquals(sIRequest.OwnerId, sIRequest.Sales_Person_Name__c);
        // Negative test - Sales Person Name is Inactive, Owner Update Fails
        User uTest= new User();
            utest = [Select Id, IsActive FROM User where IsActive = false and Profile.Name='System Administrator' limit 1 ];
            siRequest.Sales_Person_Name__c  = uTest.Id;
            update sIRequest;
            //system.assertNotEquals(sIRequest.OwnerId, sIRequest.Sales_Person_Name__c);
            //system.assertEquals(uTest.Id, sIRequest.Sales_Person_Name__c);
            sIRequest.Requested_Unit_Count__c = null;
        try{
            update sIRequest;
        }catch (Exception e){
           // Assert DML Exception to cover catch block System.Assert(e.getTypeName() == 'DmlException');
        }
        Test.stopTest();
    }
}