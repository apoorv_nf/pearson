/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_CloseParentCaseWithChildCases.cls 
Description:     Class for closing childs along with Parent Cases. 
CR Info:         CR-01159
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
25/07/2017         1.0            MAYANK LAL                       Initial Design 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */


global class PS_CloseParentCaseWithChildCases implements Database.Batchable<sObject>,Database.stateful{ 
    list<case> updateParent = new list<Case>();
    transient List<Database.SaveResult> lstSaveResult = null;
    List<Case> lstCasetoUpdated = null;
    Map<Id, Case> parCaseMap = new map<id,Case>();
    Map<Id, Case> OldCaseMap = new map<id,Case>();
    public list<Case> openChildCases = new List<Case>();
    public  list<Case> lstFailedChilds = new list<Case>();
    map<id,list<Case>> mapofBatchParentandChilds = new map<id,list<Case>>();
    map<String,list<String>> mapParentIdFailedChilds= new map<String,list<String>>();
	String ChildCaseNumbers;
    
    
    public PS_CloseParentCaseWithChildCases(Map<Id, Case> pMap,map<id,Case> oldCase){
        parCaseMap = pMap;
        OldCaseMap =  oldCase;
        System.debug('@@OldCaseMap batch: '+OldCaseMap);  
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator([SELECT Id,Casenumber,Status,parentid,parent.casenumber,Case_Resolution_Category__c,Case_Resolution_Sub_Category__c 
                                         FROM CASE where ParentId IN: parCaseMap.keySet() and  Status !=:Label.PS_CaseClosureStatus ]);
    }
    global void execute(Database.BatchableContext BC, List<Case> lstCases){
        lstCasetoUpdated = new List<Case>();
        for(Case casetoUpdate : lstCases ){
            casetoUpdate.Status = Label.PS_CaseClosureStatus;
            casetoUpdate.Case_Resolution_Category__c=parCaseMap.get(casetoUpdate.parentid).Case_Resolution_Category__c;
            casetoUpdate.Case_Resolution_Sub_Category__c=parCaseMap.get(casetoUpdate.parentid).Case_Resolution_Sub_Category__c;
            lstCasetoUpdated.add(casetoUpdate);
        }
        
        lstSaveResult = Database.update(lstCasetoUpdated, false);
        System.debug('@@lstSaveResult '+lstSaveResult);
        List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
        for(Integer i=0;i<lstSaveResult.size();i++){
            if (!lstSaveResult.get(i).isSuccess()){
                // DML operation failed
                //Database.Error error = lstSaveResult.get(i).getErrors().get(0);
                //String failedDML = error.getMessage();
                //system.debug('Failed ID'+lstCasetoUpdated.get(i).Id);
                lstFailedChilds.add(lstCasetoUpdated.get(i));
            }
        }
        
        try{            
            for (integer i=0;i<lstCasetoUpdated.size();i++){
                System.debug('@@lstCasetoUpdated');
                Case cases=lstCasetoUpdated[i];
                Database.SaveResult res=lstSaveResult[i];
                String ErrMsg='';
                if (!res.isSuccess() || Test.isRunningtest()){
                    PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                    errlogger.InterfaceName__c='PS_CloseParentCaseWithChildCases';
                    errlogger.ApexClassName__c='PS_CloseParentCaseWithChildCases';
                    errlogger.CallingMethod__c='finish';
                    errlogger.UserLogin__c=UserInfo.getUserName(); 
                    errlogger.RecordId__c=cases.id;   
                    for(Database.Error err : res.getErrors()) {
                        ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage();
                    }   
                    errlogger.ExceptionMessage__c=ErrMsg;
                    errloggerlist.add(errlogger);                     
                }
            }
            if(errloggerlist.size()>0){insert errloggerlist;}
            
        }
        catch(DMLException e){throw(e);}
        
        catch(Exception e){ExceptionFramework.LogException('PS_CloseParentCaseWithChildCases','PS_CloseParentCaseWithChildCases','execute',e.getMessage(),UserInfo.getUserName(),'');}
        
    }
    global void finish(Database.BatchableContext BC){
        boolean flag;
        openChildCases= [select id,status,Casenumber,parentid from Case where parentid IN: parCaseMap.keySet() ];
        System.debug('@@openChildCases size '+openChildCases.size());
        System.debug('@@openChildCases '+openChildCases);
        Integer fstIterate=1;
        for(Case childCase : openChildCases) {
            list<Case> lstChildCase = new List<Case>();
            if(fstIterate==1){
                lstChildCase.add(childCase);
                fstIterate = fstIterate + 1;
                mapofBatchParentandChilds.put(childCase.parentId, lstChildCase);
            }
            else {
                if(mapofBatchParentandChilds.containsKey(childCase.ParentId)) {
                    mapofBatchParentandChilds.get(childCase.ParentId).add(childCase);
                }
                else {
                    lstChildCase.add(childCase);
                    mapofBatchParentandChilds.put(childCase.ParentId, lstChildCase);
                }
            }
        }
        System.debug('@@mapofBatchParentandChilds: '+mapofBatchParentandChilds);
        System.debug('@@mapofBatchParentandChilds size: '+mapofBatchParentandChilds.size());
        for(id parRecid :mapofBatchParentandChilds.keySet() ){
            flag = true;
            for(Case chdCase :mapofBatchParentandChilds.get(parRecid)) {
                if(chdCase.status != 'Closed'){
                    System.debug('@@chdCase.status != Closed');
                    flag = false;
                    updateParent.add(new Case(Id = parRecid , Status = OldCaseMap.get(parRecid).Status,Reopen_Reason__c = 'All child cases could not be closed'));
                    break;
                }
            }
            if(flag==true) {
                System.debug('Closing Parent');
            }
        }
        if(updateParent.size()>0 && updateParent !=null && !updateParent.isEmpty()) {
            try{
                update updateParent;
                String[] CaseNumbers = new list<String>();
                Integer failedChildIterate = 1;
                for(Case csFail : lstFailedChilds) {
                    List<String> lstChildCaseNumber = new List<String>();
                    if(failedChildIterate==1){
                        lstChildCaseNumber.add(csFail.casenumber);
                        failedChildIterate = failedChildIterate +1;
                        mapParentIdFailedChilds.put(csFail.Parent.CaseNumber, lstChildCaseNumber);
                        
                    }
                    else {
                        if(mapParentIdFailedChilds.containsKey(csFail.Parent.CaseNumber)) {
                            mapParentIdFailedChilds.get(csFail.Parent.CaseNumber).add(csFail.CaseNumber);
                        }
                        else {
                            lstChildCaseNumber.add(csFail.casenumber);
                            mapParentIdFailedChilds.put(csFail.Parent.CaseNumber, lstChildCaseNumber);
                        }
                    }
                }
                List<Messaging.SingleEmailMessage> lstallMails = new List<Messaging.SingleEmailMessage>();
                for(String parRecId : mapParentIdFailedChilds.keySet()){
                    System.debug('@@parRecId: '+parRecId);
                    System.debug('@@mapParentIdFailedChilds.keySet(): '+mapParentIdFailedChilds.keySet());
                    // Send Mail notification for Non Closure of Child Cases
                    String emailAddress = UserInfo.getUserEmail();
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    // Send the email to the job submitter
                    String[] toAddresses = new String[] {emailAddress};
                        system.debug('@@toAddresses '+toAddresses);
                    mail.setToAddresses(toAddresses);
                    mail.setSenderDisplayName('Pearson Customer Support');
                    mail.setSubject('All child cases cannot be closed for Parent Case: '+parRecId);
                    //mail.setPlainTextBody('Following child Cases cannot be closed, hence Parent Case has been reopened. \n' +'Failed Case Numbers are:  '+mapParentIdFailedChilds.get(parRecId)); 
					ChildCaseNumbers = '';
					for (String sChildCaseNumber : mapParentIdFailedChilds.get(parRecId)) {
						ChildCaseNumbers = ChildCaseNumbers + sChildCaseNumber + ' ,';
					}
					ChildCaseNumbers = ChildCaseNumbers.substring(0, ChildCaseNumbers.length() - 1);
                    mail.setPlainTextBody('Following child Cases cannot be closed, hence Parent Case: '+parRecId +' has been reopened. \n' +'Failed Case Numbers are:  ' + ChildCaseNumbers);
                    lstallMails.add(mail);
                    //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
                System.debug('@@You have made: ' + Limits.getEmailInvocations() + ' email calls out of :' + Limits.getLimitEmailInvocations() + ' allowed');
                Messaging.sendEmail(lstallMails); 
            }
            catch(Exception e) {
                System.debug('@@Exception Occured '+e.getStackTraceString());
                System.debug('@@Exception Occured '+e.getMessage());
            }
        }
        // Logic to send mail for batch completion status- this will be commented in production
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email, ExtendedStatus
                          from AsyncApexJob where Id = :bc.getJobId()];        
        if(a.Status == 'Completed') {
            system.debug('@@Batch Completed');
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            // Send the email to the job submitter
            String[] toAddresses = new String[] {'mayank.lal@cognizant.com'};
                mail.setToAddresses(toAddresses);
            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('PS_CloseParentCaseWithChildCases  Status: ' + a.Status);
            mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
                                  ' batches with '+ a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
        }
    }
    
}