/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            JobExecutionRequestScheduler_Test.cls 
   Description:     Test Class covering JobExecutionRequest_Scheduler.cls and CreateJobExecutionRequest.cls
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
   5/18/2016		1.0				Gregory Hardin			Created                                  
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
@isTest
private class JobExecutionRequestScheduler_Test {

	@isTest static void scheduleExecutionRequest_test_method() {
		String testJobId;		
		String testJobName = 'TestJobScheduler ' + Datetime.now();
		String testJobCRON = '0 0 ' + Datetime.now().hour() + ' * * ?';
		String testType = 'Product Synchronization';
		String testSource = 'All';

		Test.startTest();
			JobExecutionRequest_Scheduler testScheduler = new JobExecutionRequest_Scheduler();
			testJobId = System.schedule(testJobName, testJobCRON, testScheduler);
        Test.stopTest();
		for(CronTrigger testCron : [SELECT Id, CronJobDetail.Name, CronExpression, CreatedDate, CreatedById
									FROM CronTrigger
									WHERE CronJobDetail.JobType = '7'
                                		AND CronJobDetailId =: testJobId]) {
			if(Date.valueOf(testCron.CreatedDate).isSameDay(Date.today())) {
				if(testCron.CreatedById == UserInfo.getUserId()) {
					System.assertEquals(testCron.Id, testJobId, 'Job Id = ' + testCron.Id);
					System.assertEquals(String.valueOf(CronJobDetail.Name), testJobName, 'JobName = ' + testCron.CronJobDetail.Name);
					System.assertEquals(testCron.CronExpression, testJobCRON, 'JobName = ' + testCron.CronExpression);
				}			
			}
		}
	}

	@isTest static void createJobExecutionRequest_test_method() {
		String testType = 'Product Synchronization';
		String testSource = 'All';
		Test.startTest();
			CreateJobExecutionRequest.createRequest('Product Synchronization','All');
		Test.stopTest();
		for(Job_Execution_Request__c testRequest : [SELECT Id, Type__c, Source__c, CreatedDate
													FROM Job_Execution_Request__c
													WHERE CreatedById = : UserInfo.getUserId()
														AND CreatedDate = TODAY
													ORDER BY CreatedDate Desc
													LIMIT 1]) {
			System.assertEquals(testRequest.Type__c, testType, 'Type__c = ' + testRequest.Type__c);
			System.assertEquals(testRequest.Source__c, testSource, 'Source__c ' + testRequest.Source__c);
		}
	}	
}