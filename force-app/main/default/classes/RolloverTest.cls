/*
     *@name myUnitTest() 
     *@return void 
     *@description This method for Insert Update and delete In Trigger OpportunityTeamMemberTrigger
     */

@isTest(SeeAllData=True)
private Class RolloverTest {
    static testMethod void myUnitTest() {
        Id usrId = UserInfo.getUserId();
       /* Bypass_Settings__c byp = new Bypass_Settings__c();
        byp.SetupOwnerId=usrid;
        byp.Disable_Triggers__c=true;
        byp.Disable_Validation_Rules__c=true;
        insert byp;*/
        
        //Test method for Rollover Class
        Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School',Market2__c='US',Phone = '9989887687');
        acc.ShippingStreet = 'TestStreet';
        acc.ShippingCity = 'Vns';
        acc.ShippingState = 'Delhi';
        acc.ShippingPostalCode = '234543';
        acc.ShippingCountry = 'India';
        insert acc;
        User u = [select Id, firstname from user where id=:userinfo.getuserid() limit 1];
        
        Opportunity op = new Opportunity(Name= 'OpTest1', AccountId = acc.id, StageName = 'Solutioning', Type = 'New Business', Academic_Vetting_Status__c = 'Un-Vetted', Academic_Start_Date__c = System.Today(),CloseDate = System.Today(),International_Student__c = true, Qualification__c = 'Test Bundle', Campus__c ='Durbanville', Level__c=1,Conferer__c='CTI', Preferred_Campus__c = 'Bedfordview');
        insert op;
        
        OpportunityTeamMember  OTeam = new OpportunityTeamMember(UserId = U.Id, TeamMemberRole ='Sales', OpportunityId = op.Id );
        insert OTeam;
        
        Contact con = new Contact(FirstName='Rollover',LastName ='Rollovertest',Phone='9999888898',Email='Rollover.Rollovertest@test.com', AccountId = acc.Id,
                                  Preferred_Address__c = 'Mailing Address', MailingCountry = 'United Kingdom', MailingStreet = '1 Street', MailingPostalCode = 'NE27 0QQ', MailingCity  = 'City');
        insert con;
        
        OpportunityContactRole ocr  = new OpportunityContactRole(OpportunityId = op.Id, Role = 'Decision Maker', IsPrimary= true);
        ocr.ContactId = con.Id;
        insert ocr;
        Test.StartTest();
        Product2 p = new Product2();
        p.Name = 'Test Bundle Level 2';
        p.Configuration_Type__c = 'Bundle' ;    // CR-02949 - Replacement the Apttus to Non-Apttus changes by Vinoth
        p.ISBN__c = '12345678901';
        p.IsActive = true;
        P.Qualification_Name__c = 'Test Bundle';
        p.Campus__c='Durbanville';
        p.Qualification_Level_Name__c= 2;
        p.Conferer__c = 'CTI';
        insert p;
        
        Product2 p1 = new Product2();
        p1.Name = 'Test Option';
        p1.Configuration_Type__c = 'Option' ;    // CR-02949 - Replacement the Apttus to Non-Apttus changes by Vinoth
        p1.ISBN__c = '123456 IS';
        p1.IsActive = true;
        insert p1;
        
        ProductCategory__c CN = new ProductCategory__c(Name ='Test CN',HierarchyLabel__c = 'Test CN'/*, Apttus_Config2__Active__c = true*/);
        insert CN;
        
        Hierarchy__c CH = new Hierarchy__c(Name ='Test CH', Label__c = 'Test CH', ProductCategory__c = CN.Id);
        insert CH;
        
        Bundle__c POG = new Bundle__c(RootBundle__c = CH.Id, BundleOption__c = CH.Id, /*Apttus_Config2__ProductId__c = P.Id,*/MaxOptions__c = 999, Sequence__c = 1);
        insert POG;
        
        BundleItem__c poc = new BundleItem__c(Sequence__c = 1,Product__c = p1.id, RelationshipType__c = 'Option', ParentProduct__c = p.id/*, Apttus_Config2__ProductOptionGroupId__c = POG.id, Apttus_Config2__Modifiable__c = true*/);
        insert poc;

        //ApexPages.StandardController std = new ApexPages.StandardController(op);
        Opportunity op1 = new Opportunity(Name= 'Test Bundle 1', AccountId = acc.id, StageName = 'Solutioning', Type = 'New Business', Academic_Vetting_Status__c = 'Un-Vetted', Academic_Start_Date__c = System.Today(),CloseDate = System.Today(),International_Student__c = true, Qualification__c = 'Test Bundle', Campus__c ='Durbanville', Level__c=1, Preferred_Campus__c = 'Bedfordview');
        insert op1;
        
        ApexPages.currentPage().getParameters().put('id',op1.id);
        Rollover roll2 = new Rollover();
        roll2.createOppty();
        //roll2.proposal_new = null;
        //roll2.routeToQuote();
        System.runAs(u){
        ApexPages.currentPage().getParameters().put('id',null);
        Rollover roll1 = new Rollover();
        
        //roll1.routeToQuote();
        ApexPages.currentPage().getParameters().put('id',op.id);
        Rollover roll = new Rollover();
        //roll.Create_ContactRoles();
        //roll.opportunityId = op.id;
        //roll.onLoad_dorollover();
        roll.createOppty();
        roll.Create_Quote();
      //  roll.routeToQuote();
        //roll.Create_ContactRoles();
        roll.Create_OpportunityTeam();
        roll.Create_Quote();
      //  roll.cretaeApttusobjects();
      //  roll.routeToQuote();
        }
        Update OTeam;
        
        Delete OTeam;
        
        //UnDelete OTeam; 
               
        /*WrapperProductController wrap= new WrapperProductController();
        wrap.ProductId = p.id;
        WrapperProductController.WrapperProductOption wrapOption = new WrapperProductController.WrapperProductOption();
        wrapOption.RecordId = poc.id;
        wrapOption.ProductId = p1.id;
        wrapOption.ProductOptionId = poc.Name;
        wrap.productoptions.add(wrapOption);
        roll.cpq.CreateBundle(wrap,roll.cartID);*/
        Test.stopTest();
    }
}