/* --------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_LeadConversionMapping 
Description:     Helper class for 'Lead_TriggerSequenceCtrlr' class, which is fired from lead trigger on after update event. 
Basically,the class updates the record type of Account,Contact and Opportunity and maps the field from lead to AccountContact
Date             Version           Author                                          Summary of Changes 
-----------      ----------   -----------------     ---------------------------------------------------------------------------------------
25th Sep 2015      1.0           Accenture NDC                                         Created
22nd April 2016    1.1           Modified By Kyama To map group to Opportunity,for R5
---------------------------------------------------------------------------------------------------------------------------------------------------------- 
* Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 323) 
*/

public without sharing class PS_LeadConversionMapping  
{
    public static List<Account> lstAccountToBeUpdated = new List<Account>();
    public static List<Contact> lstContactToBeUpdated = new List<Contact>();
    public static List<Opportunity> lstOpportunityToBeUpdated = new List<Opportunity>();
    public static Opportunity leadOppty;
    public static Contact leadContact;
    public static Account leadAccount;
    public static Set<Id> processedLead = new Set<Id>(); //this variable is to garantee that this code is only execute once
    public static List<AccountTeamMember> accountTeamMembers = new List<AccountTeamMember>();
    public static List<Id> accountsforSharingUpdate = new List<Id>();
    public static List<Id> userIds = new List<Id>();
    public static List<Lead> leadsRequiringChatterPost = new List<Lead>();
    public static Set<Id> setWithOpptyId = new Set<Id>();
    public static List<OpportunityContactRole> lstWithOpptyConRole = new List<OpportunityContactRole>();
    public static String leadDecisionMakingLevel;
    public static list<Lead> lstOfLeads = new list<Lead>();
    public static Lead leads;
    public static boolean isWebRequestLead  = false;   //Added by priya for lead conversion issue
    public static boolean isNotD2LLead  = false;//Added by priya for lead conversion issue
    // Map user and record id
    public static Map<Id,Id> mapUsrObj = new Map<Id,Id>();
    
    // List of users ids 
    private static List<Id> users = new List<Id>();
    
    // List of Account ids
    private static List<Id> accounts = new List<Id>();
    
    // List of Account ids
    private static List<Id> contacts = new List<Id>();                               
    
    // Map of users ids 
    private static Map<Id, User> usersMap; 
    
    // Map of accounts ids 
    private static Map<Id, Account> accountMap;                            
    
    // Map of accounts ids 
    private static Map<Id, Contact> contactMap;                            
    
    public static void createMapping(List<Lead> newLstLead,Map<Id,Lead> oldMapLead)
    {   
        //Added by priya for lead conversion issue   
      
        Id D2LLeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('D2L').getRecordTypeId();
        //  try
        //  {        
        List<Lead> lstLeadsToBeProcessed = new List<Lead>();
        
        for(Lead processLead : newLstLead)
        {
            
            if(!processedLead.contains(processLead.Id))
            {
                lstLeadsToBeProcessed.add(processLead);
            }
            
            users.add(processLead.ownerId);
            accounts.add(processLead.convertedAccountId);
            contacts.add(processLead.convertedContactId);
             //Added by priya for lead conversion issue
            if(processLead.RecordTypeId != D2LLeadRecordTypeId){
               isNotD2LLead = true; 
            }
            //end of lead conversion issue
        }
        
        if(lstLeadsToBeProcessed.isEmpty()) 
        {
            return;
        }
        //Added to fetch  Group in the existing query for RD-01731 for R5
        if(isNotD2LLead == true){    //Added by priya for lead conversion issue
            usersMap =  new Map<Id, User>([SELECT Id, Market__c, Line_Of_Business__c, Geography__C, Business_Unit__c, Group__c from User where id IN :users]);
            
            accountMap = new Map<Id, Account>([SELECT Id, Market2__c, Line_Of_Business__c, Geography__C, Business_Unit__c from Account where id IN :accounts]);
              
            contactMap = new Map<Id, Contact>([SELECT Id, Market__c,Contact_Status__c,Trust_Level__c from Contact where id IN :contacts]);        
        } 
        //get B2B lead record type id(s)
        Id B2BLeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
        Id NewAcctReqLeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(Label.PS_NewAccntReq_NewAccReq).getRecordTypeId();  
        Id EnterpriseLeadRecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Enterprise Lead').getRecordTypeId();
        Id WebRequestLeadRecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Web Request').getRecordTypeId();
        /*get account record type id(s) - Commented as we move the logic to Account before insert trigger
Id CorporateAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate').getRecordTypeId();
Id HigherEducationAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Higher Education').getRecordTypeId();
Id SchoolAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('School').getRecordTypeId();
Id ProfessionalAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Professional').getRecordTypeId();
*/
        //get contact record type id(s)
        Id GlobalContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Label.PS_Global_Contact).getRecordTypeId();
        
        //get opportunity record type is(s)
        Id B2BOpportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
        //Id B2B2LOpportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('B2B2L').getRecordTypeId();
        Id globalOpportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Global_Opportunity).getRecordTypeId();
        Id EnterpriseOpportunityRecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Enterprise').getRecordTypeId();
        
        /*Added by Srikanth -- INC5082765-Start*/
        List<String> leadEmail = new List<String>();
        List<Contact> existingCons = new List<Contact>();
        for(Lead Lead : newLstLead){
          if(lead.RecordTypeId == WebRequestLeadRecordTypeId && lead.Email !=null && lead.Contact_Lookup__c == null){
            leadEmail.add(lead.Email);
          }
        }

        if(leadEmail.size()>0 && !leadEmail.isEmpty()){
           existingCons = [Select Id,Name,Email from contact where Email =:leadEmail and CreatedDate<:Datetime.now().addMinutes(-1)];
        }
        /*Added by Srikanth -- INC5082765-End*/
        
        for(Lead newLead : newLstLead)
        {
            
            if(newLead.RecordTypeId == EnterpriseLeadRecordTypeId )
            {
                
                opportunityMappingUtility(newLead,EnterpriseOpportunityRecordTypeId);
            }
            if(newLead.ConvertedContactId != null && newLead.RecordTypeId == WebRequestLeadRecordTypeId )
            {
                
                leadContactMappingUtility(newLead,GlobalContactRecordTypeId,existingCons);
                leadOpportunityMappingUtility(newLead,B2BOpportunityRecordTypeId);
                
            }
            if((newLead.RecordTypeId == B2BLeadRecordTypeId) || (newLead.RecordTypeId == NewAcctReqLeadRecordTypeId  && !isNorthAmericaOrSouthAfricaLead(newLead)))  
            {
                if(newLead.isConverted && (oldMapLead.get(newLead.id).isConverted != true))
                { 
                    processedLead.add(newLead.Id);
                    
                    if(newLead.Organisation_Type1__c == Label.PS_LeadOrgTypeGovernment_Public || newLead.Organisation_Type1__c == Label.PS_LeadOrgTypeInstitution || newLead.Organisation_Type1__c == Label.PS_LeadOrgTypePurchasing || newLead.Organisation_Type1__c == Label.PS_LeadOrgTypeNGO_Association || newLead.Organisation_Type1__c == Label.PS_LeadOrgTypeLargeCorporate || newLead.Organisation_Type1__c == Label.PS_LeadOrgTypeSmallCorporate)
                    {
                        /*if(CorporateAccountRecordTypeId != null && newLead.ConvertedAccountId != null)
{
accountMappingUtility(newLead.ConvertedAccountId,CorporateAccountRecordTypeId, newLead);
}  D-2352*/
                        if(newLead.ConvertedContactId != null && GlobalContactRecordTypeId != null)
                        {
                            contactMappingUtility(newLead.ConvertedContactId,GlobalContactRecordTypeId, newLead.ownerId, newLead);
                        }
                        /*if(newLead.Channel__c == Label.PS_LeadChannelIndirect ||newLead.Channel__c == Label.PS_LeadChannelDirect)
{
if(globalOpportunityRecordTypeId != null && newLead.ConvertedOpportunityId != null)
{
opportunityMappingUtility(newLead,globalOpportunityRecordTypeId);

}
}*/
                        opportunityMappingUtility(newLead,globalOpportunityRecordTypeId);
                        
                    }
                    
                    if(newLead.Organisation_Type1__c == Label.PS_LeadOrgTypeHigherEducation)
                    {
                        /*if(newLead.ConvertedAccountId != null && HigherEducationAccountRecordTypeId != null)
{
accountMappingUtility(newLead.ConvertedAccountId,HigherEducationAccountRecordTypeId, NewLead);
}D-2352*/
                        if(newLead.ConvertedContactId != null && GlobalContactRecordTypeId != null)
                        {
                            contactMappingUtility(newLead.ConvertedContactId,GlobalContactRecordTypeId, newLead.ownerId, newLead);
                        }                       
                        /*if(newLead.Channel__c == Label.PS_LeadChannelStudentDriven)
{
if(newLead.ConvertedOpportunityId !=null && globalOpportunityRecordTypeId !=null)
{
opportunityMappingUtility(newLead,globalOpportunityRecordTypeId);
}
}else 
if(newLead.Channel__c == Label.PS_LeadChannelIndirect ||newLead.Channel__c == Label.PS_LeadChannelDirect)
{
if(newLead.ConvertedOpportunityId != null && globalOpportunityRecordTypeId !=null)
{
opportunityMappingUtility(newLead,globalOpportunityRecordTypeId);
}
}*/
                        opportunityMappingUtility(newLead,globalOpportunityRecordTypeId);
                        
                    }
                    if(newLead.Organisation_Type1__c == Label.PS_LeadOrgTypeSchool)
                    {
                        /*if(newLead.ConvertedAccountId != null && SchoolAccountRecordTypeId !=null)
{
accountMappingUtility(newLead.ConvertedAccountId,SchoolAccountRecordTypeId, NewLead);
}D-2352*/  
                        if(newLead.ConvertedContactId != null && GlobalContactRecordTypeId != null)
                        {
                            contactMappingUtility(newLead.ConvertedContactId,GlobalContactRecordTypeId, newLead.ownerId, newLead);
                        }
                        /*if(newLead.Channel__c == Label.PS_LeadChannelIndirect ||newLead.Channel__c == Label.PS_LeadChannelDirect)
{
if(newLead.ConvertedOpportunityId!= null && globalOpportunityRecordTypeId != null)
{
opportunityMappingUtility(newLead,globalOpportunityRecordTypeId);
}   
}*/
                        opportunityMappingUtility(newLead,globalOpportunityRecordTypeId);
                    }
                    if(newLead.Organisation_Type1__c == Label.PS_LeadOrgTypeProfessional)
                    {
                        /*if(newLead.ConvertedAccountId != null && ProfessionalAccountRecordTypeId != null)
{
accountMappingUtility(newLead.ConvertedAccountId,ProfessionalAccountRecordTypeId, NewLead);
}D-2352*/
                        if(newLead.ConvertedContactId != null && GlobalContactRecordTypeId != null)
                        {
                            contactMappingUtility(newLead.ConvertedContactId,GlobalContactRecordTypeId, newLead.ownerId, newLead);
                        }   
                        
                        /*if(newLead.Channel__c == Label.PS_LeadChannelIndirect ||newLead.Channel__c == Label.PS_LeadChannelDirect)
{
if(newLead.ConvertedOpportunityId != null && globalOpportunityRecordTypeId != null)
{
opportunityMappingUtility(newLead,globalOpportunityRecordTypeId);
}   
}*/
                        opportunityMappingUtility(newLead,globalOpportunityRecordTypeId);
                    }
                    
                    addTeamMember(newLead);
                    updateAccountKeyFields(newLead);
                    if(newLead.Decision_Making_Level__c != null)
                    {
                        leadDecisionMakingLevel = newLead.Decision_Making_Level__c;   
                    }                      
                }
            }   
        }
        
        
        
        //Utility.stopRecursion = True;
        if(lstAccountToBeUpdated != null && !lstAccountToBeUpdated.isEmpty())
        {
            database.update(lstAccountToBeUpdated,false);
        }
        
        if(lstContactToBeUpdated !=null && !lstContactToBeUpdated.isEmpty())
        {
            database.update(lstContactToBeUpdated,false);
        }
        if(lstOpportunityToBeUpdated != null && !lstOpportunityToBeUpdated.isEmpty())
        {
            database.update(lstOpportunityToBeUpdated,false);
            for(Opportunity oppty : lstOpportunityToBeUpdated)
            {
                setWithOpptyId.add(oppty.Id);
            }   
            if(setWithOpptyId != null && !setWithOpptyId.isEmpty())
            {
                for(OpportunityContactRole OpptyConRole : [select Id,Role,OpportunityId from OpportunityContactRole where OpportunityId IN : setWithOpptyId])
                {
                    if(leadDecisionMakingLevel != null && leadDecisionMakingLevel != '')
                    {
                        OpptyConRole.Role = leadDecisionMakingLevel;
                        lstWithOpptyConRole.add(OpptyConRole);
                    }
                }
                if(!lstWithOpptyConRole.isEmpty() && lstWithOpptyConRole.size()>0)
                {
                    update lstWithOpptyConRole;
                }
            }
        }  
        if(accountTeamMembers != null && !accountTeamMembers.isEmpty())
        {
            database.insert(accountTeamMembers);
            updateAccountTeamShare();
        } 
        
        if(!mapUsrObj.isEmpty()){
            //call the method to chatter post on newly created account
            PS_ChatterPostUtilities.createPostOnAccount(mapUsrObj);
        }
        
        
        //}catch(Exception e)
        // {
        //  }
    }  
    /*public static void accountMappingUtility(ID accountID,ID accountRecTypeID, Lead newLead)
{
//Instantiate account and assign the record type to corporate 
leadAccount = new Account();
leadAccount.Id = accountID;
leadAccount.RecordTypeId = accountRecTypeID;
lstAccountToBeUpdated.add(leadAccount);
leadsRequiringChatterPost.add(newLead);
leads = new Lead();
leads.id = newLead.Id;
lstOfLeads.add(leads);
}D-2352*/
    //As part CR-03115 Included the newLead as a Input variable for the below method by Raja Gopalarao B to map the Marketing_Opt_In__c B2b lead Lead field to Conatact Marketing_Opt_In__c field. 
    public static void contactMappingUtility(ID contactID,ID contactRecTypeID, ID ownerId, Lead newLead)
    {   
        Id B2BLeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
        //Instantiate contact and assign the record type to Global Contact 
        leadContact = new Contact();
        leadContact = contactMap.get(contactID);
        leadContact.RecordTypeId = contactRecTypeID;
        //Added the below if condition as part of CR-03115 by Raja Gopalarao B
        if(newLead.RecordTypeId == B2BLeadRecordTypeId){
            leadContact.Marketing_Opt_In__c = newLead.Marketing_Opt_In__c;
            //Added by Uma Maheswari B as part of CR-02898-req4
            leadContact.Do_Not_Send_Samples__c = newLead.Do_Not_Send_Samples__c;
           
        }
        // update the market value from the user that owns the lead 
        User u = usersMap.get(ownerId);
        if(u != null)
        {
            if(String.isBlank(leadContact.Market__c))
            { 
                leadContact.Market__c = u.Market__c;
            }
        }   
        lstContactToBeUpdated.add(leadContact);
    }
    public static void opportunityMappingUtility(Lead convertedLead,ID opptyRecTypeID)
    {
        Id EnterpriseOpportunityRecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Enterprise').getRecordTypeId();
        //Instantiate Opportunity and assign the record type to either B2B/B2B2L  
        leadOppty = new Opportunity();
        leadOppty.Id = convertedLead.ConvertedOpportunityId;
        leadOppty.Campaign__c = convertedLead.Campaign__c;
        leadOppty.Amount = convertedLead.Estimated_Value__c;
        
        // update the Market, LOB, BU and geography with the coresponding values
        // from the user that owns the lead
        User u = usersMap.get(convertedLead.OwnerId);
        if(u != null)
        {
            leadOppty.Business_unit__c = u.Business_unit__c;
            leadOppty.Market__c = u.Market__c;
            leadOppty.Line_Of_Business__c = u.Line_Of_Business__c;
            leadOppty.Geography__c = u.Geography__c;
            //Added To map user group to optty as per RD-01731 for R5
            leadOppty.Group__c = u.Group__c;         
        } 
        leadOppty.ownerId = convertedLead.OwnerId;
        if(opptyRecTypeID == EnterpriseOpportunityRecordTypeId)
        {
            leadOppty.StageName=System.Label.D2L_Opportunity_StageName_Value;
        }
        leadOppty.RecordTypeId = opptyRecTypeID;
        lstOpportunityToBeUpdated.add(leadOppty);
    }
    
    //This method is to identify if the lead belongs to R1/R2/R3. if it belongs to R1/R2, then it returns true else false
    private static Boolean isNorthAmericaOrSouthAfricaLead(Lead leadRec)
    {
        Boolean isNorthAmericaOrSouthAfricaLeadFlag = false;
        if((leadRec.Market__c == 'US' || leadRec.Market__c == 'ZA') && (leadRec.Business_Unit__c == 'CTIPIHE' || leadRec.Business_Unit__c == 'US Field Sales'))
        {
            isNorthAmericaOrSouthAfricaLeadFlag = true;
        }               
        return isNorthAmericaOrSouthAfricaLeadFlag;
    }
    
    public static void addTeamMember(Lead convertedLead)
    {   
        AccountTeamMember accTeamMember = new AccountTeamMember();
        accTeamMember.AccountId = convertedLead.ConvertedAccountId;
        accTeamMember.TeamMemberRole = 'Team Member';
        accTeamMember.userid = convertedLead.ownerid;
        accountTeamMembers.add(accTeamMember);
        accountsforSharingUpdate.add(convertedLead.ConvertedAccountId);
        userIds.add(accTeamMember.userid);
        mapUsrObj.put(convertedLead.ConvertedAccountId,convertedLead.ownerid);
    }
    
    public static void updateAccountTeamShare()
    {
        List<AccountShare> accountSharesToUpdate = new List<AccountShare>();
        //Retrieving and updating Access Level Permissions
        //04/22/2019: Saritha Singamsetty modified the where clause to filter the query with RowCause= Team.
        List<AccountShare> addAccountShare = [select Accountid, userorgroupid,AccountAccessLevel,rowcause,OpportunityAccessLevel, CaseAccessLevel,ContactAccessLevel from AccountShare where Accountid IN : accountsforSharingUpdate and UserOrGroupId IN : UserIds and rowcause = 'Team'];
        for(AccountShare aas : addAccountShare)
        {
            aas.AccountAccessLevel = 'Edit';
            aas.OpportunityAccessLevel = 'Edit';
            aas.CaseAccessLevel = 'Edit';
            aas.ContactAccessLevel = 'Edit';               
            accountSharesToUpdate.add(aas);          
        }
        if(!test.isRunningTest() && accountSharesToUpdate.size()>0)
        {
            update accountSharesToUpdate;
        }
    }
    
    /*
* Updates the market, LOB, BU and Geography to the corresponding
* values from the user that is the owner of the lead
*/ 
    private static void updateAccountKeyFields(Lead newLead)
    {
        User u = usersMap.get(newLead.OwnerId);
        
        if(u != null)
        { 
            leadAccount = accountMap.get(newLead.convertedAccountId);
            
            // only update if the value is not already set
            if(String.isBlank(leadAccount.Market2__c))
            {
                leadAccount.Market2__c = u.Market__c;
            }
            
            if(String.isBlank(leadAccount.Line_Of_Business__c))
            {
                leadAccount.Line_Of_Business__c = u.Line_Of_Business__c;
            }
            
            if(String.isBlank(leadAccount.Geography__c))
            {
                leadAccount.Geography__c = u.Geography__c;
            }
            
            // for Business Unit we append the value
            if(u.Business_Unit__c != null)
            {
                if(leadAccount.Business_Unit__c == null || leadAccount.Business_Unit__c == '') 
                {               
                    leadAccount.Business_Unit__c = u.Business_Unit__c; 
                }
                else // Business_unit__c already exist, so append the values.  
                {                                                                                    
                    if(!leadAccount.Business_Unit__c.contains(u.Business_Unit__c))
                    {
                        leadAccount.Business_Unit__c += ';'+ u.Business_Unit__c;                                              
                    }                                               
                }
            }        
            
            lstAccountToBeUpdated.add(leadAccount);     
        } 
    } 
    //This method convert Lead record of record type of 'Web Request' to Contact of record type of 'Global Request' (CR-02295-Req6)
    public static void leadContactMappingUtility(Lead ConvertedLead,Id GlobalContactRecordTypeId,List<Contact> Contacts){   
        User userdetails = [select Market__c from User where id =:UserInfo.getUserId()];
        Id UserprofileId= userinfo.getProfileId();
        if(ConvertedLead.Contact_Lookup__c == null && Contacts.isEmpty()){
            
            leadContact = new Contact();
            leadContact = contactMap.get(ConvertedLead.ConvertedContactId);
            //Instantiate contact and assign the record type to Global Contact
            leadContact.RecordTypeId = GlobalContactRecordTypeId;
            leadContact.LeadSource ='Online/Web';
            leadContact.Market__c = ConvertedLead.Market__c;
            leadContact.Trust_Level__c= ConvertedLead.Trust_Level__c;
            leadContact.Validated_On__c =ConvertedLead.Validated_On__c;
            leadContact.Validated_By__c = ConvertedLead.Validated_By__c;
            if(ConvertedLead.Account_Lookup__c!=NULL && ConvertedLead.Parent_Account__c!=NULL){             
                leadContact.AccountID = ConvertedLead.Account_Lookup__c;
            }
            leadContact.Salutation = ConvertedLead.Salutation;
            leadContact.FirstName = ConvertedLead.FirstName ;
            leadContact.LastName = ConvertedLead.LastName ;
            leadContact.Email = ConvertedLead.Email ;
            leadContact.MailingStreet = ConvertedLead.Street;
            leadContact.MailingCity = ConvertedLead.City;
            leadContact.MailingState = ConvertedLead.State;
            leadContact.MailingCountry = ConvertedLead.Country;
            leadContact.MailingPostalCode = ConvertedLead.PostalCode; 
            leadContact.CurrencyIsoCode =  ConvertedLead.CurrencyIsoCode;
            leadContact.Description= ConvertedLead.Comments__c;
            leadContact.Global_Marketing_Unsubscribe__c = ConvertedLead.Global_Marketing_Unsubscribe__c;
            leadContact.Phone = ConvertedLead.Phone;
            leadContact.SheerID_Verification_ID__c = ConvertedLead.SheerID_Verification_ID__c;
            leadContact.Contact_Status__c = True;
            //Asha changes for Req 11
            if((UserprofileId==System.Label.Pearson_InsideSales_User_OneCRM_Profile || UserprofileId==System.Label.Pearson_Sales_User_OneCRM_Profile) && userdetails.Market__c == 'US' && (ConvertedLead.Trust_Level__c == 'Unknown' || ConvertedLead.Trust_Level__c == null)){ 
                leadContact.Trust_Level__c = 'Low';
                   leadContact.Validated_On__c =Date.today();
                   leadContact.Validated_By__c = 'Pearson User';
               } //Asha changes end
            lstContactToBeUpdated.add(leadContact);
        }else if(ConvertedLead.Contact_Lookup__c != null || !Contacts.isEmpty()){
            leadContact = new Contact();
            leadContact = contactMap.get(ConvertedLead.ConvertedContactId);
            leadContact.RecordTypeId = GlobalContactRecordTypeId;
            leadContact.LeadSource ='Online/Web';
            leadContact.Trust_Level__c= ConvertedLead.Trust_Level__c;
            leadContact.Validated_On__c =ConvertedLead.Validated_On__c;
            leadContact.Validated_By__c = ConvertedLead.Validated_By__c;
            leadContact.Role__c = ConvertedLead.Role__c;
            leadContact.Role_Detail__c = ConvertedLead.Role_Detail__c;
            leadContact.Other_Role_Detail__c = ConvertedLead.Other_Role_Detail__c;
            leadContact.Global_Marketing_Unsubscribe__c = ConvertedLead.Global_Marketing_Unsubscribe__c;
            lstContactToBeUpdated.add(leadContact);
        }
    }
    
    //This method convert Lead record of record type of 'Web Request' to Opportunity of record type of 'B2B' (CR-02295-Req6)
    public static void leadOpportunityMappingUtility(Lead convertedLead,ID B2BOppRecordTypeId)
    {
        
        //Instantiate Opportunity and assign the record type to either B2B
        leadOppty = new Opportunity();
        leadOppty.Id = convertedLead.ConvertedOpportunityId;
        leadOppty.RecordTypeId = B2BOppRecordTypeId;
        lstOpportunityToBeUpdated.add(leadOppty);
    }  
}