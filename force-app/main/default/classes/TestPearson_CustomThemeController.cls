/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 02/07/2018
 */
@isTest
private class TestPearson_CustomThemeController {

	@isTest
	static void it_should_get_snap_in_settings() {
		SnapIn_Settings__c settings = new SnapIn_Settings__c();
		settings.Enable_SnapIn__c = true;
		insert settings;

		settings = Pearson_CustomThemeController.getSnapInSettings();
		System.assertEquals(true, settings.Enable_SnapIn__c);
	}
}