/*******************************************************************************************************************
* Apex Class Name  : PS_CaseChatTranscript.cls
* Version          : 1.0
* Created Date     : 10 Dec 2015
* Function         : RD-1524 - Attached Case Chat transcript in Email template 
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
* Payal Popat               10/12/2015              Initial documentation of the class. 
* 
*******************************************************************************************************************/

public  class PS_CaseChatTranscript{
    public Id caseId {get;set;}
    /**
    * Description : Method to query Chat Transcript for Case 
    * @param NA
    * @return List<LiveChatTranscript>
    * @throws NA
    **/
    public List<LiveChatTranscript> getChatTranscript()
    {
        List<LiveChatTranscript> chatTranscript;
        chatTranscript = [SELECT Body FROM LiveChatTranscript WHERE CaseId =: caseId order by CreatedDate Desc Limit 1];
        return chatTranscript;
    }
}