@isTest
public class BatchSystemAccountIndicatorTest {
    static testMethod void testAccountPrimarySellingAccount(){
        
        list<Account> accountdatalist = TestDataFactory.createAccount(10,PS_Constants.ACCOUNT_LEARNER_RECCORD); 
        Insert accountdatalist;
        
        accountdatalist[0].System_Account_Indicator__c = True;
        accountdatalist[1].Primary_Selling_Account_check__c = True;
        accountdatalist[2].Primary_Selling_Account_check__c = True;
        accountdatalist[2].ParentID = accountdatalist[1].ID;
        accountdatalist[3].ParentID = accountdatalist[2].ID;
        //accountdatalist[3].Primary_Selling_Account_check__c = True;
        accountdatalist[4].ParentID = accountdatalist[3].ID;
        //accountdatalist[4].Primary_Selling_Account_check__c = True;
        accountdatalist[5].ParentID = accountdatalist[4].ID;
        //accountdatalist[5].Primary_Selling_Account_check__c = True;
        accountdatalist[6].ParentID = accountdatalist[5].ID;
        accountdatalist[6].Primary_Selling_Account_check__c = False;
        accountdatalist[7].ParentID = accountdatalist[6].ID;
        accountdatalist[7].Primary_Selling_Account_check__c = True;
        accountdatalist[8].ParentID = accountdatalist[7].ID;
        accountdatalist[8].Primary_Selling_Account_check__c = True;
        accountdatalist[9].ParentID = accountdatalist[8].ID;
        accountdatalist[9].Primary_Selling_Account_check__c = False;
        Update accountdatalist;
        
        test.startTest();
        for(account acc1 : accountdatalist){
        	Map<Id,Account> oldMapAccount =  new Map<Id,Account>();
        	List<Id> accID = new  List<ID>();
            oldMapAccount.put(acc1.Id,acc1);
            accID.add(acc1.Id);
            Database.executeBatch(new BatchSystemAccountIndicator(accID,True,False,oldMapAccount));
        }

        test.stopTest();		

    }
    
    static testMethod void testAccountPrimarySellingAccount2(){
    
        list<Account> accountdatalist1 = TestDataFactory.createAccount(6,PS_Constants.ACCOUNT_LEARNER_RECCORD); 
        Insert accountdatalist1;
        
        accountdatalist1[0].Primary_Selling_Account_check__c = True;
        accountdatalist1[1].Primary_Selling_Account_check__c = False;
        accountdatalist1[1].System_Account__c = accountdatalist1[0].ID;
        accountdatalist1[1].ParentID = accountdatalist1[0].ID;
        accountdatalist1[2].Primary_Selling_Account_check__c = False;
        accountdatalist1[2].ParentID = accountdatalist1[1].ID;
        accountdatalist1[3].ParentID = accountdatalist1[2].ID;
        accountdatalist1[3].Primary_Selling_Account_check__c = False;
        accountdatalist1[4].ParentID = accountdatalist1[3].ID;
        accountdatalist1[4].Primary_Selling_Account_check__c = False;
        accountdatalist1[5].ParentID = accountdatalist1[4].ID;
        accountdatalist1[5].Primary_Selling_Account_check__c = True;
        Update accountdatalist1;
        
        
        /*
        list<Account> accountdatalist2 = TestDataFactory.createAccount(4,PS_Constants.ACCOUNT_LEARNER_RECCORD); 
        Insert accountdatalist2;
        
        Account accParent = [select id,name from account where id = :accountdatalist1[3].id];
        
        accountdatalist2[0].ParentID = accParent.ID;
        accountdatalist2[0].Primary_Selling_Account_check__c = True;
        accountdatalist2[1].ParentID = accountdatalist2[0].ID;
        accountdatalist2[1].Primary_Selling_Account_check__c = True;
        accountdatalist2[2].ParentID = accountdatalist2[1].ID;
        accountdatalist2[2].Primary_Selling_Account_check__c = True;
        accountdatalist2[3].ParentID = accountdatalist2[2].ID;
        accountdatalist2[3].Primary_Selling_Account_check__c = True;
        Update accountdatalist2;
        
        
        list<Account> accountdatalist3 =new list<Account>();
        
        for(account acc : accountdatalist1){
            accountdatalist3.add(acc);
        }
        for(account acc1 : accountdatalist2){
            accountdatalist3.add(acc1);
        }
        /*
        accountdatalist[1].System_Account__c = accountdatalist[0].ID;
        accountdatalist[1].ParentID = accountdatalist[0].ID;
        Update accountdatalist;
        */
        
        test.startTest();

        Map<Id,Account> oldMapAccount2 =  new Map<Id,Account>();
        List<Id> accID2 = new  List<ID>();
        oldMapAccount2.put(accountdatalist1[5].Id,accountdatalist1[5]);
        accID2.add(accountdatalist1[5].Id);
        
        Database.executeBatch(new BatchSystemAccountIndicator(accID2,True,False,oldMapAccount2));
        
		//for(account acc2 : accID2){
        	//Map<Id,Account> oldMapAccount2 =  new Map<Id,Account>();
        	//List<Id> accID2 = new  List<ID>();
            //oldMapAccount2.put(acc2.Id,acc2);
            //accID2.add(acc2.Id);
            //Database.executeBatch(new BatchSystemAccountIndicator(accID2,True,False,oldMapAccount2));
        //}
        
        test.stopTest();		

    }
    /*
    static testMethod void testAccountPrimarySellingAccount3(){
    		list<Account> accountdatalist  = new List<Account>();
            For(Integer i=1 ;i<=5 ; i++)
            {
                Account acc = new Account();
                accountdatalist.add(acc);
            }
        insert accountdatalist;
        
        accountdatalist[0].System_Account_Indicator__c = True;
        accountdatalist[0].ParentID = accountdatalist[0].ID;
        accountdatalist[1].Primary_Selling_Account_check__c = True;
        accountdatalist[2].ParentID = accountdatalist[1].ID;
        accountdatalist[2].Primary_Selling_Account_check__c = False;
        accountdatalist[3].ParentID = accountdatalist[2].ID;
        accountdatalist[3].Primary_Selling_Account_check__c = False;
        accountdatalist[4].ParentID = accountdatalist[3].ID;
        accountdatalist[4].Primary_Selling_Account_check__c = False;
        accountdatalist[5].ParentID = accountdatalist[4].ID;
        accountdatalist[5].Primary_Selling_Account_check__c = True;
        
        for(account acc2 : accountdatalist){
        	Map<Id,Account> oldMapAccount2 =  new Map<Id,Account>();
        	List<Id> accID2 = new  List<ID>();
            oldMapAccount2.put(acc2.Id,acc2);
            accID2.add(acc2.Id);
            Database.executeBatch(new BatchSystemAccountIndicator(accID2,True,False,oldMapAccount2));
        }
        
        test.stopTest();
        
    }
    */
    static testMethod void testSystemAccountIndicator2(){
        
        list<Account> accountdatalist = TestDataFactory.createAccount(10,PS_Constants.ACCOUNT_LEARNER_RECCORD); 
        Insert accountdatalist;
        
        accountdatalist[0].System_Account_Indicator__c = True;
        accountdatalist[1].System_Account_Indicator__c = True;
        accountdatalist[2].System_Account_Indicator__c = True;
        accountdatalist[2].ParentID = accountdatalist[1].ID;
        accountdatalist[3].ParentID = accountdatalist[2].ID;
        //accountdatalist[3].System_Account_Indicator__c = True;
        accountdatalist[4].ParentID = accountdatalist[3].ID;
        //accountdatalist[4].System_Account_Indicator__c = True;
        accountdatalist[5].ParentID = accountdatalist[4].ID;
        accountdatalist[5].System_Account_Indicator__c = True;
        accountdatalist[6].ParentID = accountdatalist[5].ID;
        accountdatalist[6].System_Account_Indicator__c = True;
        accountdatalist[7].ParentID = accountdatalist[6].ID;
        accountdatalist[7].System_Account_Indicator__c = True;
        accountdatalist[8].ParentID = accountdatalist[7].ID;
        accountdatalist[8].System_Account_Indicator__c = False;
        accountdatalist[9].ParentID = accountdatalist[8].ID;
        accountdatalist[9].System_Account_Indicator__c = False;
        Update accountdatalist;
        
        
        
        /*
        accountdatalist[1].System_Account__c = accountdatalist[0].ID;
        accountdatalist[1].ParentID = accountdatalist[0].ID;
        Update accountdatalist;
        */
        
        test.startTest();
        for(account acc1 : accountdatalist){
        	Map<Id,Account> oldMapAccount =  new Map<Id,Account>();
        	List<Id> accID = new  List<ID>();
            oldMapAccount.put(acc1.Id,acc1);
            accID.add(acc1.Id);
            Database.executeBatch(new BatchSystemAccountIndicator(accID,True,False,oldMapAccount));
        }
        
        test.stopTest();		

    }
}