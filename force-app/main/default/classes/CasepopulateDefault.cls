public class CasepopulateDefault{
    
    public CasepopulateDefault(ApexPages.StandardController controller){}
                  
    public pagereference Redirect()
    {
        PageReference pageRef;
        //To get the logged in userId and line of business value
        User usr = [select Id, username, Business_Vertical__c from User where Id = :UserInfo.getUserId()];
        
                
        //  Query the Tech Support Record Type Id
        string recTypeID=Apexpages.currentPage().getparameters().get('RecordType');
        recordtype recordtypeInst;
        if(recTypeID !=null) {
            recordtypeInst = [select Id,Name  from recordtype where ID=:recTypeID and sobjecttype = 'Case'];
         }
         
       string contactID=Apexpages.currentPage().getparameters().get('def_contact_id');
       string  acctID=Apexpages.currentPage().getparameters().get('def_account_id');
        
       string pgRef;
       
        if(recTypeID !=null && recordtypeInst.Name=='Technical Support' && usr.Business_Vertical__c  !=null ) {
                 
             pgRef='/500/e?retURL=%2F500%2Fo&RecordType='+recordtypeInst.ID+'&ent=Case&00Nb000000AGUYm='+ usr.Business_Vertical__c + '&nooverride=1';
             
             if(contactId!=null) {
                 pgRef +='&def_contact_id='+contactID;
                 
             }
             
             if(acctID !=null) {
                 pgRef +='&def_account_id='+acctID;   
             }
             
             pageRef = new PageReference(pgRef);
        
        }
       
        else if(recTypeID !=null && usr.Business_Vertical__c  ==null ) {
             
             pgRef='/500/e?retURL=%2F500%2Fo&RecordType='+recTypeID+'&ent=Case&nooverride=1';
             
             if(contactId!=null) {
                 pgRef +='&def_contact_id='+contactID;
                 
             }
             
             if(acctID !=null) {
                 pgRef +='&def_account_id='+acctID;   
             }
             
             pageRef = new PageReference(pgRef);
             
             
        
        }
         else if(recTypeID !=null && recordtypeInst.Name !='Technical Support' ) {
             
             pgRef='/500/e?retURL=%2F500%2Fo&RecordType='+recTypeID+'&ent=Case&nooverride=1';
             
             if(contactId!=null) {
                 pgRef +='&def_contact_id='+contactID;
                 
             }
             
             if(acctID !=null) {
                 pgRef +='&def_account_id='+acctID;   
             }
             
             /***
             Change desc: Added the below conditiion in order to default Business Vertical for clinical Users on CS Case record based on 
                          LoggedInUser's Business Vertical
             Added By: Charitha Seelam
             Added On: 09/25/2017
             ***/
             
             if(usr.Business_Vertical__c  !=null && usr.Business_Vertical__c  == 'Clinical')
             {
                 pgRef +='&00Nb000000AGUYm='+ usr.Business_Vertical__c;
             }
             
             pageRef = new PageReference(pgRef);
             
             
            
         }
        
        else if(recTypeID==null && usr.Business_Vertical__c  !=null) {
            
            pgRef='/500/e?retURL=%2F500%2Fo&ent=Case&nooverride=1&00Nb000000AGUYm='+ usr.Business_Vertical__c;
             
             if(contactId!=null) {
                 pgRef +='&def_contact_id='+contactID;
                 
             }
             
             if(acctID !=null) {
                 pgRef +='&def_account_id='+acctID;   
             }
             
             
             pageRef = new PageReference(pgRef);
            
            
          }
          else if(recTypeID ==null && usr.Business_Vertical__c  ==null ) {
             
             pgRef='/500/e?retURL=%2F500%2Fo&ent=Case&nooverride=1';
             
             if(contactId!=null) {
                 pgRef +='&def_contact_id='+contactID;
                 
             }
             
             if(acctID !=null) {
                 pgRef +='&def_account_id='+acctID;   
             }
             
             pageRef = new PageReference(pgRef); 
             
          }  
        pageRef.setRedirect(True);
        return pageRef;
    }   
}
        //if case record type is technical support
       /*
        if((id.contains(recordtypeId) || recordtypeId.contains(id)) && u.Line_of_Business__c== Null ){
            pageRef = new PageReference('/500/e?retURL=%2F500%2Fo&RecordType='+recordtypeId+'&ent=Case&nooverride=1');
        }
        else if((id.contains(recordtypeId) || recordtypeId.contains(id)) && u.Line_of_Business__c=='Professional'){
            pageRef = new PageReference('/500/e?retURL=%2F500%2Fo&RecordType='+recordtypeId+'&ent=Case&00Nb000000AGUYm=Professional&nooverride=1');
        }
        else if((id.contains(recordtypeId) || recordtypeId.contains(id)) && u.Line_of_Business__c == 'Higher Ed'){
            pageRef = new PageReference('/500/e?retURL=%2F500%2Fo&RecordType='+recordtypeId+'&ent=Case&00Nb000000AGUYm=Higher%20Education&nooverride=1');
        }
        else {pageRef = new PageReference('/500/e?retURL=%2F500%2Fo&RecordType='+recordtypeId+'&ent=Case&nooverride=1');} */