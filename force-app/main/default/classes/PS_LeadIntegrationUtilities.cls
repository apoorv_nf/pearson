/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_LeadIntegrationUtilities.cls
   Description:      
   Date             Version                  Author           Tag                                Summary of Changes 
   -----------      ----------         -----------------   --------     ---------------------------------------------------------------------------------------
   15th Oct 2015      0.1                Rahul Boinepally     T1              Integration handler for Lead generation from Web to Lead Forms.  
                  (Initial version)                                          (Updates the record type and creates Campaign Member records ) 
   09/11/2015         0.2                Kamal Chandran                       Added 'assignIntegrationLeads' method for firing assignment rules 
                                                                              when the lead is created via soap request                
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public  class PS_LeadIntegrationUtilities 
{
    


    public static void mapRecordType(List<Lead> lstLead)
    {

        List<Lead> leadsToProcess = filterIntegrationLeads(lstLead);
        

        for(Lead leadObj : leadsToProcess)
        {    
            if (leadObj.Record_Type__c!= null && PS_Util.recordTypeMap.get('Lead').containsKey(leadObj.Record_Type__c))
            {               
                leadObj.recordtypeiD = PS_Util.recordTypeMap.get('Lead').get(leadObj.Record_Type__c);
            }
        }
    }   

    public static void createCampaignMember(List<Lead> lstLead)
    {        
        

        List<Lead> leadsToProcess = filterIntegrationLeads(lstLead);

        List<CampaignMember> newCampaignMember = new List<CampaignMember>();   
        //String campaignIdTemp; 

        for(Lead leadObj : leadsToProcess) 
        {
            try
            {            
                //campaignIdTemp = string.valueOf(LeadObj.campaign_id__c);
                if(leadObj.campaign_id__c != null) 
                {   
                    //Id leadCampaignId = leadObj.campaign_id__c;
                
                    
                    newCampaignMember.add(new CampaignMember(CampaignId = (Id)leadObj.campaign_id__c, LeadId = leadObj.Id));
                }
            }
            catch(Exception e)
            {
            }
        }
            insert newCampaignMember;                               
    }

    public static void assignIntegrationLeads(List<Lead> lstLead)
    {        
        List<Lead> leadsToProcess = filterIntegrationLeads(lstLead);
        List<Lead> leadsToUpdate = new List<Lead>();
        Lead leadUpdate;
        for(Lead leadObj : leadsToProcess) 
        {
            leadUpdate = new Lead();
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
            leadUpdate.Id = leadObj.Id;
            leadUpdate.setOptions(dmo); 
            leadsToUpdate.add(leadUpdate);
        }
        update leadsToUpdate;
    }    

    /* Filters Leads which have isFromIntegration set to TRUE*/

    private static List<Lead> filterIntegrationLeads(List<Lead> allLeads) {

        List<Lead> integrationLeads = new List<Lead>();

        for(Lead lead : allLeads) {

            if(lead.isFromIntegration__c) {
                integrationLeads.add(lead);
            }
        }

        return integrationLeads;
    }



}