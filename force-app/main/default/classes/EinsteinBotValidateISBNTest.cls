/*
 * Author: Apoorv (Neuraflash)
 * Date: 03/09/2019
 * Description: Test Class for EinsteinBotValidateISBN.
*/
@isTest
private class EinsteinBotValidateISBNTest {
    
    @isTest
    private static void testVerifyISBNFormat(){

        Test.startTest();
            List<Boolean> isValid = EinsteinBotValidateISBN.verifyISBNFormat(new List<String>{'9780205977789'});
        Test.stopTest();

        System.assertEquals(TRUE, isValid[0]);

    }

    @isTest
    private static void testVerifyISBNFormatNegative(){

        Test.startTest();
            List<Boolean> isValid = EinsteinBotValidateISBN.verifyISBNFormat(new List<String>{'1230205977789'});
        Test.stopTest();

        System.assertEquals(FALSE, isValid[0]);

    }

}