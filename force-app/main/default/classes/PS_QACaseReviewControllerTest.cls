/************************************************************************************************************
* Apex Interface Name : PS_QACaseReviewControllerTest
* Version             : 1.1 
* Created Date        : 2/2/2016  
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Sneha Sinha         2/2/2016          
* Abhinav Srivastava  19/5/2016  modified for test class fix.    
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_QACaseReviewControllerTest
{
 static  testMethod void validateCaseReviewController()
 { 
  Quality_Review__c QA=new Quality_Review__c();
  ApexPages.StandardController sc = new ApexPages.standardController(QA);
  PS_QACaseReviewController sample =new PS_QACaseReviewController(sc);
 }
 public static testmethod void validateCaseReviewController1()
 {
   List<UserRole> userRoldeId = [Select Id from UserRole where DeveloperName=: 'NA_S_N_Service_Manager'];
   List<User> listWithUser = new List<User>();
   listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,1);
   listWithUser[0].UserRoleId = userRoldeId[0].Id;
   insert listWithUser;
   list<Permissionset> ps=new list<Permissionset>();
   ps=[select id,name from permissionset where name =:'Pearson_S_N_Agents'];     
   PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = ps[0].id, AssigneeId = listWithUser[0].id );
   insert psa; 
   System.runAs(listWithUser[0])  
   { 
     Test.StartTest();
     list <case> case2=testDatafactory.createCase_SelfService(1,'School Assessment');
     
     Case2[0].OwnerId = listWithUser[0].Id;
     Case2[0].origin = 'Mail';
     insert case2;
     PageReference pageRef = page.PS_OverideQualityReviewNewButton;//Adding VF page Name here
     pageRef.getParameters().put('caseId',case2[0].id);
     pageRef.getParameters().put('retURL',case2[0].id);
     Test.setCurrentPage(pageRef);
     ApexPages.StandardController sc = new ApexPages.standardController(case2[0]);
     PS_QACaseReviewController sample1 = new PS_QACaseReviewController(sc);
     Pagereference pf = sample1.mcreateQAReview();
     System.assert( pf != null,'Error message');
     Test.stoptest(); 
   }
  }
}