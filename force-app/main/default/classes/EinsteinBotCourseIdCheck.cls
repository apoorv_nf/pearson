/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 14/05/2018
 */
global with sharing class EinsteinBotCourseIdCheck
{
	@InvocableMethod(label='Einstein Bot - Check Course Id')
	global static List<CourseIdResponse> checkCourseId(List<String> courseIds) {
		List<CourseIdResponse> courseIdResponse = new List<CourseIdResponse>();

		for(String courseId : courseIds) {
			CourseIdResponse response = new CourseIdResponse();
			response.success = false;
			try {
				SmsApi.CurrentGenCourse courseDetails = new SmsApiService(8000, false).getCourse(courseId);
				response.success = true;
				response.valid = courseDetails.id == null ? false : true;
				response.sessionExpired = courseDetails.valid == 'false' && courseDetails.reason.contains('SSO Session') ? true : false;

				if(response.sessionExpired && [SELECT Id FROM AsyncApexJob WHERE JobType = 'Future' AND MethodName = 'resetAccessToken' AND Status NOT IN ('Completed','Failed')].isEmpty()) {
					resetAccessToken(courseId);
				}
			} catch(CalloutException ex) {
				//Fail gracefully if a timeout of 5000 is reached
				response.timedOut = ex.getMessage() == 'Read timed out' ? true : false;
			} catch(Exception ex) {}

			courseIdResponse.add(response);
		}

		return courseIdResponse;
	}

	@future(callout=true)
	private static void resetAccessToken(String courseId) {
		new SmsApiService().getCourse(courseId);
	}

    global class CourseIdResponse
    {
        @InvocableVariable
        global Boolean success;

        @InvocableVariable
        global Boolean valid;

        @InvocableVariable
        global Boolean timedOut;

		@InvocableVariable
		global Boolean sessionExpired;
	}
}