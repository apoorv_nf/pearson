@isTest(SeeAllData = False)                   
public class ProductPickerControllerTest{
     
     static testMethod void myTest1() { 
 
            Account acc = new Account();
            acc.Name = 'CTI Bedfordview Campus';
            acc.Line_of_Business__c= 'Higher Ed';
            acc.Geography__c = 'Core';
            acc.Market2__c = 'UK';
            acc.Pearson_Campus__c=true;
            acc.Pearson_Account_Number__c='Campus';
            insert acc;
            
            Id standardPBId = Test.getStandardPricebookId();// Pricebook2Id = standardPBId
            Opportunity opp = new opportunity();
            opp.AccountId = acc.id;    
            opp.Name = 'OppTest';
            opp.StageName = 'Need Analysis';
            opp.PriceBook2Id = standardPBId;
            opp.CloseDate = system.today();
            insert opp;
            
            /*Quote quote = new quote();
            PageReference pageRef = Page.CreateStandardQuote;
            pageRef.getParameters().put('oppid', opp.id);
            Id recordtypeid =  Schema.SObjectType.Quote.getRecordTypeInfosByName().get('HE Quote').getRecordTypeId();
            pageRef.getParameters().put('RecordType',recordtypeid);
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController stdQuote = new ApexPages.StandardController(quote);
            CreateStandardQuote controller = new CreateStandardQuote(stdQuote);
            controller.Quote_create();
           
            Test.StartTest();
            Quote quo = [SELECT ID,Name,OpportunityId From Quote LIMIT 1];
            quo.ERP_Operating_Unit__c = 'GB Pearson Education OU';
            quo.Primary_Selling_Account__c = Opp.Account.Primary_Selling_Account__c;
            quo.Primary_Contact__c = opp.Primary_Contact__c;
            quo.Email = opp.Primary_Contact__r.Email;
            quo.Ship_To_Contact__c = opp.Primary_Contact__c; 
            quo.Subscription_Contact__c = opp.Primary_Contact__c;
            quo.Subscription_Contact_Email__c = opp.Primary_Contact__r.Email;
            quo.Market__c = opp.Market__c;
            quo.Name= opp.Name;
            quo.Business_unit__c = opp.Business_unit__c;
            quo.Line_of_Business__c = opp.Line_of_Business__c;
            quo.Geography__c = opp.Geography__c;
            quo.ERP_Operating_Unit__c = 'GB Pearson Education OU';
            quo.Freight_Override__c=true;
            quo.ExpirationDate=System.today();
            update quo;*/
            
            Test.StartTest();          
            List<Product2> listProduct = new List<Product2>();
            Product2 prod = new Product2(Name = 'Companion Website for Law Express: Jurisprudence', 
                                        MDM_Profit_Centre__c = 'Law',
                                        MDM_UK_Product_Category__c = '36 months amortisation plant',
                                        Product_Series__c ='Law Express',
                                        ISBN__c='9781292210339',  
                                        Net_Price__c =  0.01, 
                                        Quantity_in_Stock__c = 0,  
                                        Publish_Date__c = system.today(),
                                        MDM_Product_Group__c = 'Law',
                                        Division__c = 'Higher Education',
                                        Imprint__c ='Pearson',
                                        MDM_UK_Product_Function__c = 'Companion Website',
                                        Product_Function__c = 'Companion Website',
                                        Family = 'Best Practices', IsActive = true);
            insert prod;
        
            
            PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, 
                                                            Product2Id = prod.Id,
                                                            UnitPrice = 10000,
                                                            CurrencyIsoCode =UserInfo.getDefaultCurrency(), 
                                                            IsActive = true);
                                                            //PriceBook2.Name='Standard Price Book' AND
                                
            insert standardPBE;
                     
            List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();   
            OpportunityLineItem oliliner = new OpportunityLineItem(Product2Id=prod.id,
                                                       OpportunityId=opp.id,PriceBookEntryID=standardPBE.id,Quantity=4, 
                                                       UnitPrice =50);
            //oliList.add(oliliner);                                                        
            insert oliliner;
          
            ApexPages.currentPage().getParameters().put('oppId',opp.Id);
            ApexPages.currentPage().getParameters().put('prd2Id',prod.Id);
            PageReference pageRef1 = Page.ProductPickerPage;
            pageRef1.getParameters().put('oppId', opp.id);
            ApexPages.StandardController stdQuote1 = new ApexPages.StandardController(prod);
            ProductPickerController prdSearch = new ProductPickerController(stdQuote1);
            prdSearch.soslSearchString = NULL;
            prdSearch.productSeries = 'Law';
         	//prdSearch.productCategory = 'Law';
         	//prdSearch.profitCentre = 'Law';
            prdSearch.searchProducts();
            prdSearch.addProductstoCart();
            prdSearch.Beginning();
            prdSearch.Previous();
            prdSearch.Next();
            prdSearch.End();
            prdSearch.getDisablePrevious();
            prdSearch.getDisableNext();
            prdSearch.getTotal_size();
            prdSearch.getPageNumber();
            prdSearch.cancelUrl();         
            Test.StopTest();
     }
     static testMethod void myTest2() { 
 
            Account acc = new Account();
            acc.Name = 'CTI Bedfordview Campus';
            acc.Line_of_Business__c= 'Higher Ed';
            acc.Geography__c = 'Core';
            acc.Market2__c = 'UK';
            acc.Pearson_Campus__c=true;
            acc.Pearson_Account_Number__c='Campus';
            insert acc;
            
            Id standardPBId = Test.getStandardPricebookId();// Pricebook2Id = standardPBId
            Opportunity opp = new opportunity();
            opp.AccountId = acc.id;    
            opp.Name = 'OppTest';
            opp.StageName = 'Need Analysis';
            opp.PriceBook2Id = standardPBId;
            opp.CloseDate = system.today();
            insert opp;
            
            /*Quote quote = new quote();
            PageReference pageRef = Page.CreateStandardQuote;
            pageRef.getParameters().put('oppid', opp.id);
            Id recordtypeid =  Schema.SObjectType.Quote.getRecordTypeInfosByName().get('HE Quote').getRecordTypeId();
            pageRef.getParameters().put('RecordType',recordtypeid);
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController stdQuote = new ApexPages.StandardController(quote);
            CreateStandardQuote controller = new CreateStandardQuote(stdQuote);
            controller.Quote_create();
           
            Test.StartTest();
            Quote quo = [SELECT ID,Name,OpportunityId From Quote LIMIT 1];
            quo.ERP_Operating_Unit__c = 'GB Pearson Education OU';
            quo.Primary_Selling_Account__c = Opp.Account.Primary_Selling_Account__c;
            quo.Primary_Contact__c = opp.Primary_Contact__c;
            quo.Email = opp.Primary_Contact__r.Email;
            quo.Ship_To_Contact__c = opp.Primary_Contact__c; 
            quo.Subscription_Contact__c = opp.Primary_Contact__c;
            quo.Subscription_Contact_Email__c = opp.Primary_Contact__r.Email;
            quo.Market__c = opp.Market__c;
            quo.Name= opp.Name;
            quo.Business_unit__c = opp.Business_unit__c;
            quo.Line_of_Business__c = opp.Line_of_Business__c;
            quo.Geography__c = opp.Geography__c;
            quo.ERP_Operating_Unit__c = 'GB Pearson Education OU';
            quo.Freight_Override__c=true;
            quo.ExpirationDate=System.today();
            update quo;*/
            
                     
            List<Product2> listProduct = new List<Product2>();
            Product2 prod = new Product2(Name = 'Companion Website for Law Express: Jurisprudence', 
                                        ISBN__c='9781292210339',  
                                        Net_Price__c =  0.01, 
                                        Quantity_in_Stock__c = 0,  
                                        Publish_Date__c = system.today(),
                                        MDM_Product_Group__c = 'Law',
                                        Division__c = 'Higher Education',
                                        Imprint__c ='Pearson',
                                        MDM_UK_Product_Function__c = 'Companion Website',
                                        Product_Function__c = 'Companion Website',
                                        Family = 'Best Practices', IsActive = true);
            insert prod;
        
            
            PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, 
                                                            Product2Id = prod.Id,
                                                            UnitPrice = 10000,
                                                            CurrencyIsoCode =UserInfo.getDefaultCurrency(), 
                                                            IsActive = true);
                                                            //PriceBook2.Name='Standard Price Book' AND
                                
            insert standardPBE;
                     
            List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();   
            OpportunityLineItem oliliner = new OpportunityLineItem(Product2Id=prod.id,
                                                       OpportunityId=opp.id,PriceBookEntryID=standardPBE.id,Quantity=4, 
                                                       UnitPrice =50);
            //oliList.add(oliliner);                                                        
            insert oliliner;
            Test.StartTest(); 
            ApexPages.currentPage().getParameters().put('oppId',opp.Id);
            ApexPages.currentPage().getParameters().put('prd2Id',prod.Id);
            PageReference pageRef1 = Page.ProductPickerPage;
            pageRef1.getParameters().put('oppId', opp.id);
            ApexPages.StandardController stdQuote1 = new ApexPages.StandardController(prod);
            ProductPickerController prdSearch = new ProductPickerController(stdQuote1);
            prdSearch.soslSearchString ='Law';
            prdSearch.searchProducts();
            prdSearch.addProductstoCart();
            prdSearch.Beginning();
            prdSearch.Previous();
            prdSearch.Next();
            prdSearch.End();
            prdSearch.getDisablePrevious();
            prdSearch.getDisableNext();
            prdSearch.getTotal_size();
            prdSearch.getPageNumber();
            prdSearch.cancelUrl();         
            Test.StopTest();
     }
     
}