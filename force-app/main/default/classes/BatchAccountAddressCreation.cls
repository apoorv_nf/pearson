global class BatchAccountAddressCreation implements Database.Batchable < sObject > , Database.Stateful {

public string batchQuery;

public BatchAccountAddressCreation (integer limitrecords, Boolean excludeAccountsWithAccountAddress) {
    batchQuery = 'select id,name,billingstreet,billingcity,billingstate,billingcountry,billingpostalcode,shippingstreet,'+
    'shippingcity,shippingstate,shippingcountry,shippingpostalcode,Account_Shipping_Addresses_id__c,PrimaryBillingAddressId__c from Account where '+
    'recordtype.developername != \'Account_Creation_Request\'';
    
    if(excludeAccountsWithAccountAddress)
    {
        batchQuery = batchQuery  + ' and (Account_Shipping_Addresses_id__c = null or PrimaryBillingAddressId__c = null)';
    }

     if(limitrecords > 0)
     {
         batchQuery  = batchQuery   + ' limit '+limitrecords;
     }
}
global Database.QueryLocator start(Database.BatchableContext BC) {
      return Database.getQueryLocator(batchQuery);
}
global void execute(Database.BatchableContext BC, List <Account> scope) {
    system.debug('Account to process:'+scope);
    List<Account_Addresses__c> oAccaddrList = new List<Account_Addresses__c>();
    for(Account acc: scope){
        string[] streetlist=new String[]{};
        
        if(acc.billingstreet != null || acc.billingcity !=null  || acc.BillingState != null || acc.BillingPostalCode != null || acc.BillingCountry !=null)
        {
        
            if (acc.billingstreet!= null && acc.billingstreet != ''){
                streetlist = RetrieveOneCRMHandler.splitStreetAddress(acc.billingstreet);
            }
            Account_Addresses__c oBillAccaddr = new Account_Addresses__c();
            oBillAccaddr.Address_1__c= (streetlist.size()>=1) ? streetlist[0] : null;
            oBillAccaddr.Address_2__c= (streetlist.size()>=2) ? streetlist[1] : null;
            oBillAccaddr.Address_3__c= (streetlist.size()>=3) ? streetlist[2] : null;
            oBillAccaddr.Address_4__c= (streetlist.size()>=4) ? streetlist[3] : null;  
            oBillAccaddr.City__c=acc.billingcity;
            oBillAccaddr.State__c=acc.BillingState;
            oBillAccaddr.Country__c=acc.BillingCountry;
            oBillAccaddr.Zip_Code__c=acc.BillingPostalCode;                         
            oBillAccaddr.Source__c = 'One CRM';    
            oBillAccaddr.Address_Type__c='Billing';
            oBillAccaddr.Primary__c=true;
            oBillAccaddr.Update_Sold_To_Address__c= true;
            oBillAccaddr.Account__c= acc.id;
            
            if(acc.PrimaryBillingAddressId__c != null)
            {
                oBillAccaddr.Id = acc.PrimaryBillingAddressId__c ;
            }
                         
            oAccaddrList.add(oBillAccaddr);
        }
                    
        streetlist.clear();
        
        if(acc.shippingstreet != null || acc.shippingcity !=null  || acc.shippingState !=null || acc.shippingPostalCode != null || acc.shippingCountry !=null)
        {
        
            if (acc.shippingstreet!= null && acc.shippingstreet != ''){
                streetlist = RetrieveOneCRMHandler.splitStreetAddress(acc.shippingstreet);
            }
           
            Account_Addresses__c oShipAccaddr = new Account_Addresses__c();
            oShipAccaddr.Address_1__c= (streetlist.size()>=1) ? streetlist[0] : null;
            oShipAccaddr.Address_2__c= (streetlist.size()>=2) ? streetlist[1] : null;
            oShipAccaddr.Address_3__c= (streetlist.size()>=3) ? streetlist[2] : null;
            oShipAccaddr.Address_4__c= (streetlist.size()>=4) ? streetlist[3] : null;  
            oShipAccaddr.City__c=acc.shippingcity;
            oShipAccaddr.State__c=acc.shippingState;
            oShipAccaddr.Country__c=acc.shippingCountry;
            oShipAccaddr.Zip_Code__c=acc.shippingPostalCode;                        
            oShipAccaddr.Source__c = 'One CRM';  
            oShipAccaddr.Address_Type__c='Shipping';
            oShipAccaddr.PrimaryShipping__c=true;
            oShipAccaddr.Update_Sold_To_Address__c= true;   
            oShipAccaddr.Account__c= acc.id; 
            
            if(acc.Account_Shipping_Addresses_id__c != null)
            {
                oShipAccaddr.Id = acc.Account_Shipping_Addresses_id__c ;
            }
                                     
            oAccaddrList.add(oShipAccaddr);
         }
    }//end for loop
    
    if (oAccaddrList.size()>0){
    
        System.debug('Upsert Account Addresses -->' + oAccaddrList);
        upsert oAccaddrList;
        
        Map<Id,Account> accounts =  new Map<Id,Account>();
        
        for(Account_Addresses__c  address:  oAccaddrList)
        {
            if(! accounts.containsKey(address.Account__c))
            {
                accounts.put(address.Account__c, new Account(Id = address.Account__c));
            }
            
            if(address.Address_Type__c== 'Billing')
            {
                 accounts.get(address.Account__c).PrimaryBillingAddressId__c=address.Id;
            }
            else
            {
                accounts.get(address.Account__c).Account_Shipping_Addresses_id__c=address.Id;
            }
        
        }
        
        if(accounts.size() >0)
        {
           System.debug('Updated Accounts -->' + accounts);
            update accounts.values();
        }
        
        
      }
        
}
global void finish(Database.BatchableContext BC) {
}

}