@isTest
public class SurveytoCasetest {
    static testMethod void test1(){
        List<User> listWithUser = new List<User>();
        String caseRecType = 'School Assessment';
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        System.runAs(listWithUser[0]){
        List<Case> caseList = new List<Case>();
        caseList = TestDataFactory.createCase_SelfService(1,caseRecType);    
        caseList[0].Escalation_Reason__c = 'test';
        insert caseList;    
                
        MaritzCX__Survey_Response__c msr = new MaritzCX__Survey_Response__c();
        msr.MaritzCX__Metric_8__c = caseList[0].id;
        insert msr;
        }
    }
}