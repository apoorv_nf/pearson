/**********************************************************************************************************

* Apex Class Name : GlobalRevenueUpdateOppty
* Version             : 1.0 
* Created Date        : 9/29/2016 
* Function            :  R6 - To update opportunity stage
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------

        KP                    9/29/2016            R6 - To update opportunity stage
***********************************************************************************************************/
public class GlobalRevenueUpdateOppty{    
    //Method to update oppty stage based on revenue order status
    public static void mUpdateOptyStage(Map<Id,Order> newOrdersMap, Map<Id,Order> oldOrdersMap) {
        Id revenueOrderRTID = PS_Util.fetchRecordTypeByName(Order.SobjectType,Label.PS_OrderGlobalRevenueRecordType);
        Map<Id,String> oOpptyStageMap;
        for(Order ord : newOrdersMap.Values()) {
            if(ord.Status != oldOrdersMap.get(ord.Id).Status && ord.recordtypeid == revenueOrderRTID){// to identify status change
                if ((ord.Status == System.Label.Booked || ord.Status == System.Label.Closed) && ord.OpportunityId != null){ //Booked or closed
                    if (oOpptyStageMap== null)
                        oOpptyStageMap = new Map<Id,String>();
                    oOpptyStageMap.put(ord.OpportunityId,'Closed Won');                    
                }
                if (ord.Status == System.Label.Cancelled && ord.OpportunityId != null){ //cancelled status
                    if (oOpptyStageMap== null)
                        oOpptyStageMap = new Map<Id,String>();
                    oOpptyStageMap.put(ord.OpportunityId,'Awarded');                    
                }
            }
        }//end for loop
        if (oOpptyStageMap != null){
            List<Opportunity> objOpptyList = new List<Opportunity>();
            try{
                for(Opportunity objOpp : [select id,stagename,Finalise_Opportunity__c,RecordType.Name from Opportunity where id in :oOpptyStageMap.keyset()]){
                    objOpp.stagename=oOpptyStageMap.get(objOpp.Id);
                    if (oOpptyStageMap.get(objOpp.Id) == 'Awarded' && objOpp.RecordType.Name != 'B2B')
                        objOpp.Finalise_Opportunity__c = false;
                    objOpptyList.add(objOpp);
                }
                Database.Update(objOpptyList, false);
            }//end try
            catch(Exception e){system.debug('Inside exception');}
        }//end oppty process
    }//end method
}//end class