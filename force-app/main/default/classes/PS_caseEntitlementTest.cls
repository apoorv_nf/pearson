/************************************************************************************************************
* Apex Interface Name : PS_caseEntitlementTest
* Version             : 1.1 
* Created Date        : 2/2/2016  
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Sneha Sinha         2/2/2016  
* supriyam srivastava  3/4/16           
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_caseEntitlementTest
{

 static testMethod void validatecaseEntitlement()
 {
  List<User> listWithUser = new List<User>();
  listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,1);
  insert listWithUser;
  list<Permissionset> ps=new list<Permissionset>();
  ps=[select id,name from permissionset where name =:'Pearson_S_N_Agents'];
  PermissionSetAssignment psa = new PermissionSetAssignment
  (PermissionSetId = ps[0].id, AssigneeId = listWithUser[0].id );
  insert psa; 
     
  System.runAs(listWithUser[0])
  {
    List<case> cases = TestDataFactory.createCase_SelfService(1,'School Assessment'); 
   Test.StartTest();
   List<Entitlement> listent=TestDataFactory.CreateEntitlement();
   insert listent;
   cases[0].accountid=listent[0].accountid;
   cases[0].PS_Business_Vertical__c=listent[0].Business_Vertical__c;
   cases[0].PS_Product__c ='N/A';
   insert cases;   
   Test.StopTest();
  }
 }
 // negative condition with null value
  static testMethod void negative_validatecaseEntitlement()
 {
  List<User> listWithUser = new List<User>();
  listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,1);
  insert listWithUser;
  list<Permissionset> ps=new list<Permissionset>();
  ps=[select id,name from permissionset where name =:'Pearson_S_N_Agents'];
  PermissionSetAssignment psa = new PermissionSetAssignment
  (PermissionSetId = ps[0].id, AssigneeId = listWithUser[0].id );
  insert psa; 
   
  System.runAs(listWithUser[0])
  {
   List<case> cases = TestDataFactory.createCase_SelfService(1,'School Assessment');     
   Test.StartTest();
   List<Entitlement> listent=TestDataFactory.CreateEntitlement();
   insert listent;
   //passing the wrong data
   listent[0].Id = null;
   listent[0].Business_Vertical__c='';
   cases[0].accountid=listent[0].accountid;
   cases[0].PS_Business_Vertical__c=listent[0].Business_Vertical__c;
   cases[0].PS_Product__c ='N/A';
   insert cases;
   //checking with null id 
      System.Assert(true,'Invalid id'); 
   Test.StopTest();
  }
 }
 
}