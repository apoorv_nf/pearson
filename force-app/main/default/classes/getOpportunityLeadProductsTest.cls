@IsTest
public class getOpportunityLeadProductsTest {
       
    static testMethod void testLeadCreation()
    {
        List< Opportunity> oppList = new List <Opportunity>();
        oppList=TestDataFactory.createnewopportnity(1);
        getOpportunityLeadProducts obj = new getOpportunityLeadProducts( new ApexPages.StandardController( OppList[0] ) );  

         Lead_Product__c leadProd= new Lead_Product__c();
         leadProd.Opportunity__c =oppList[0].Id;
         leadProd.Lead__c =oppList[0].Converted_Lead_Id__c;
         insert leadProd;
         obj.getLeadProducts();
    }
}