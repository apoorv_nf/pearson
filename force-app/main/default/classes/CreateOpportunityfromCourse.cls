/************************************************************************************************************
* Apex Class Name   : CreateOpportunityfromCourse.cls
* Version           : 1.0
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Raushan                   12/2/2015                Changes for defect id- 2903  
* Leonard Victor            17/12/2015               Changes User Key fields API
* Rahul Garje               16/02/2016               for mapping Speciality,TerritoryCode fields on the oppty
  Rahul Garje               23/02/2016               for mapping Activefields on the oppty PCE from Course PCE 
  Rahul Garje               28/03/2016               for checking if the user is attached to multiple terriory and course terriotry field is empty.
 Kyama                     25/04/2016               Null check for Defect raised for RD-01744
  Sasiraja                  11/04/2017                   CR-01244
*************************************************************************************************************/

public class CreateOpportunityfromCourse {
    
    public ID courseId;
    public String command;
    public Id oppId;
    public Opportunity Oppty_new;
    public List<OpportunityContactRole> Opptycontact_new;
    public OpportunityUniversityCourse__c new_OpportunityUniversityCourse;
    public UniversityCourse__c UnivCourse;
    public Selling_Period__c Fall_Season;
    public Selling_Period__c Spring_Season;
    public Date today_date;
    public Date Fall_start_date;
    public Date Fall_end_date;
    public Date Spring_start_date;
    public Date Spring_end_date;

    //Constructor
    public CreateOpportunityfromCourse(){
        courseId =  ApexPages.currentPage().getParameters().get('id');
        oppId =  ApexPages.currentPage().getParameters().get('opptyid');
        command = ApexPages.currentPage().getParameters().get('command');
        if(courseId == null && command == null) {  
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: No course Id ');
                        ApexPages.addMessage(myMsg);
        } else {

            }
     }

    public PageReference onLoad_CourseOpptyEdit(){
        if(command == 'Opportunitycreate'){
           String redirectURL = createOppty();
           if(redirectURL != null){
               PageReference pg = new PageReference(redirectURL);
               pg.setRedirect(true);
               return pg;
           }
        }
        
        if(command =='DeleteOpportunity'){
            String redirectURL = DeleteOppty();
            if(redirectURL != null){
                PageReference pg = new PageReference(redirectURL);
                pg.setRedirect(true);
                return pg;
            }
        }
        
        /*if(command == 'CreateOpportunityUniversityCourse'){
            String redirectURL = CreateOpportunityUniversityCourse();
            if(redirectURL != null){
                PageReference pg = new PageReference(redirectURL);
                pg.setRedirect(true);
                return pg;
            }
        }*/
        
     /*   if(command == 'CreatePearsonCourse'){
            String redirectURL = CreatePearsonCourse();
            if(redirectURL != null){
                PageReference pg = new PageReference(redirectURL);
                pg.setRedirect(true);
                return pg;
            }
        }*/
        
        return null;
    }
    
    public String createOppty(){
        UniversityCourse__c UnivCourse = [select Id, Name, CreatedBy.Id, Account__c,CourseSpecialty__c,Course_Territory_Code_s__c, Fall_Enrollment__c, Spring_Enrollment__c, Winter_Enrollment__c, Summer_Enrollment__c, Adoption_Type__c,LMS_Access_Method__c
                                          from UniversityCourse__c where Id =:courseId ];
        List<UniversityCourseContact__c>  UniversityCourseContact = [select Id, Contact__c, Contact_Role__c from UniversityCourseContact__c where UniversityCourse__c =:courseId and Active__c = true];
        if(UnivCourse != Null){
            Oppty_new = new Opportunity();
            //code added by Rahul starts
            
            List<Territory2> oppOwnrTerritoryLSt = new List<Territory2>();
            List<Territory2> oppOwnrTerrList = new List<Territory2>();
            oppOwnrTerritoryLSt = [Select id,Territory_Code__c,Territory2Model.ActivatedDate from Territory2 where id in (Select Territory2Id from UserTerritory2Association where IsActive=true and UserId=:userinfo.getuserid()) and (Territory2Model.ActivatedDate!=null and Territory2Model.DeactivatedDate=null)];
            if(oppOwnrTerritoryLSt.size() > 0 && oppOwnrTerritoryLSt != null){
                for(Territory2 oppOwnrTerrObj:oppOwnrTerritoryLSt)
                    oppOwnrTerrList.add(oppOwnrTerrObj);
                }
             if(oppOwnrTerrList != null && !oppOwnrTerrList.isEmpty()){
                if(oppOwnrTerrList.size() > 1 && UnivCourse.Course_Territory_Code_s__c!=null){//And condition added by RG:28/03/2016
                     for(Territory2 oppOwnrTerrcode : oppOwnrTerrList){
                       if(oppOwnrTerrcode.Territory_Code__c!=null){
                        if(UnivCourse.Course_Territory_Code_s__c.contains(oppOwnrTerrcode.Territory_Code__c)){
                            Oppty_new.Territory_Code__c= oppOwnrTerrcode.Territory_Code__c;
                            break;
                        }
                     }
                   } 
                }else{
                    Oppty_new.Territory_Code__c = oppOwnrTerrList[0].Territory_Code__c;
                }
             }  
             else{
                    Oppty_new.Territory_Code__c = '';
                } 
            //code added by Rahul Ends
            Oppty_new.LMS_Access_Method__c = UnivCourse.LMS_Access_Method__c;
            Oppty_new.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
            Oppty_new.Name = UnivCourse.Name;
            Oppty_new.AccountId = UnivCourse.Account__c;
            Oppty_new.StageName = Label.TA_Prequalification_Stage;
            Oppty_new.Amount = 0;
            Oppty_new.Fall__c = UnivCourse.Fall_Enrollment__c;
            Oppty_new.Spring__c = UnivCourse.Spring_Enrollment__c;
            Oppty_new.Winter__c = UnivCourse.Winter_Enrollment__c;
            Oppty_new.Summer__c = UnivCourse.Summer_Enrollment__c;
            Oppty_new.Adoption_Type__c = UnivCourse.Adoption_Type__c;
            Oppty_new.Speciality__c= UnivCourse.CourseSpecialty__c; //added by Rahul for new DR - 12/02/106
            Oppty_new.Market__c = 'US';
            
             //if(today_date >= Fall_start_date && today_date <= Fall_end_date){
                    //Oppty_new.CloseDate = Date.newInstance(2015, 10, 15);
                /*if(system.Date.today().month() == 12){
                    Oppty_new.CloseDate = Date.newInstance (system.Date.today().year()+1, 10, 15);
                }else if (system.Date.today().month() >=1 && system.Date.today().month() <= 5){
                    Oppty_new.CloseDate = Date.newInstance (system.Date.today().year(), 10, 15);
                }else if (system.Date.today().month() >= 6 && system.Date.today().month() <= 11){
                    Oppty_new.CloseDate = Date.newInstance (system.Date.today().year()+1, 4, 15);
                //}else if(today_date >= Spring_start_date && today_date <= Spring_end_date){
                    //Oppty_new.CloseDate = Date.newInstance(2015, 4, 15);
                } else {
                    //Oppty_new.CloseDate = system.today();
                }*/
                
            if(system.Date.today().month() == 12 || (system.Date.today().month() >=1 && system.Date.today().month() <= 5)){
                if(system.Date.today().month() == 12){
                Oppty_new.Selling_Period__c = string.valueof(system.Date.today().year()+1)+' - Fall';
                //By Raushan added for defect iD-2903
                 Oppty_new.CloseDate = Date.newInstance (system.Date.today().year()+1, 4, 15);
                }else{
                 Oppty_new.Selling_Period__c = string.valueof(system.Date.today().year())+' - Fall';
                 Oppty_new.CloseDate = Date.newInstance (system.Date.today().year(), 4, 15);
                }
            
            }else if(system.Date.today().month() >= 6 && system.Date.today().month() <= 11){
                Oppty_new.Selling_Period__c = string.valueof(system.Date.today().year()+1)+' - Spring';
                Oppty_new.CloseDate = Date.newInstance (system.Date.today().year(), 10, 15);
            }
            Oppty_new.Selling_Year__c = string.valueof(system.Date.today().year());
            Oppty_new.Type = null;

            
            Fall_Season = Selling_Period__c.getValues(string.valueof(system.Date.today().year())+' - Fall');
            Spring_Season = Selling_Period__c.getValues(string.valueof(system.Date.today().year())+' - Spring');

            if(Fall_Season != null && Spring_Season != null){
                today_date = system.Date.today();
                Fall_start_date = Fall_Season.Start_Date__c;
                Fall_end_date = Fall_Season.End_Date__c;
                Spring_start_date = Spring_Season.Start_Date__c;
                Spring_end_date = Spring_Season.End_Date__c;
                
                String AssetfieldsList = Utils_allfields.getCreatableFieldsList('Asset');
                String soql = 'Select '+ AssetfieldsList  +', Product2.Publisher__c, Product2.Next_Edition__c, Product2.Relevance_Value__c, Product2.Competitor_Product__c, Product2.Next_Edition__r.Relevance_Value__c' +' from Asset where Course__c =: courseId AND Status__c = \'Active\' and Primary__c = true ORDER BY Product2.Copyright_Year__c Limit 1';
                List<Asset> product_in_use = Database.query(soql);
                if(product_in_use != null && !product_in_use.isEmpty()){
                    Oppty_new.StageName = Label.TA_Prequalification_Stage;
                    
                    for(Asset eachPUI : product_in_use){
                      
                        
                      // Opportunity Type Rollover
                      if(!eachPUI.Product2.Competitor_Product__c && eachPUI.Product2.Next_Edition__c != null && eachPUI.Product2.Next_Edition__r.Relevance_Value__c == 10){
                          Oppty_new.Type = Label.TA_Rollover_Type;
                          Oppty_new.StageName = Label.TA_Prequalification_Stage;
                      }else if(!eachPUI.Product2.Competitor_Product__c && (( eachPUI.Product2.Next_Edition__c != null && eachPUI.Product2.Next_Edition__r.Relevance_Value__c != 10) || eachPUI.Product2.Next_Edition__c == null)){
                                Oppty_new.Type = Label.TA_Existing_Business_Type;
                                Oppty_new.StageName = Label.TA_Prequalification_Stage;
                      } else if(eachPUI.Product2.Competitor_Product__c){
                                Oppty_new.Type = Label.TA_Takeaway_Type;
                                Oppty_new.StageName = Label.TA_Prequalification_Stage;
                      }
                    }
                }else{
                    //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'No assets or primary asset'));
                    //return null;
                    Oppty_new.Type = Label.TA_Takeaway_Type;
                    Oppty_new.StageName =Label.TA_Prequalification_Stage;
                }
                
               
            }
            try{
                insert Oppty_new;
                if(UniversityCourseContact != null && !UniversityCourseContact.isEmpty()){
                    Opptycontact_new = new List<OpportunityContactRole>();
                    for(UniversityCourseContact__c eachUnivcoursecontact :UniversityCourseContact){
                        OpportunityContactRole tempopptycontractRole = new OpportunityContactRole();
                        tempopptycontractRole.ContactId = eachUnivcoursecontact.Contact__c;
                        tempopptycontractRole.OpportunityId = Oppty_new.Id;
                        tempopptycontractRole.Role = 'Non-Participant';
                        Opptycontact_new.add(tempopptycontractRole);
                    }
                    
                    if(Opptycontact_new != null && !Opptycontact_new.isEmpty()){
                        insert Opptycontact_new;
                    }
                }
                /*List<Asset> Opp_product_in_Use = new Opp_product_in_Use();
                if(product_in_use != null && !product_in_use.isEmpty()){
                    for(Asset eachPUI : product_in_use){
                        Asset Temp_product_in_use = new Asset();
                        Temp_product_in_use =  eachPUI.clone(false, true);
                       Temp_product_in_use.Course__c = null;
                    }
                    
                }*/
            }catch(Exception ex){
                Throw ex;
                return null;
            }
            
            String DeleteURL = '%2Fapex%2FCreateOpportunityfromCourse%3Fopptyid%3D'+Oppty_new.Id+'%26id%3D'+courseId+'%26command%3DDeleteOpportunity';
            String saveURL = '%2Fapex%2FCreateOpportunityfromCourse%3Fopptyid%3D'+Oppty_new.Id+'%26id%3D'+courseId+'%26command%3DCreateOpportunityUniversityCourse';
           String redirectURL = CreateOpportunityUniversityCourse(Oppty_new.Id);
            //String redirectURL = '/'+Oppty_new.Id+'/e?cancelURL='+DeleteURL+'&saveURL='+saveURL;
           // string redirectURL = '/o';
            //return redirectURL;
            //https://c.cs18.visual.force.com/apex/CreateOpportunityfromCourse?opptyid=00611000009Pc6uAAC&id=a0F11000005ZXMOEA4&command=CreateOpportunityUniversityCourse&newid=00611000009Pc6u
            //String redirectURL =  Quote_create();
            return redirectURL;
        }
        return null;
    }
    
    public String DeleteOppty(){
        if(oppId != null && courseid != null){
            Opportunity oppty = new Opportunity(Id= oppId);
            try{
                delete oppty;
                return '/'+courseid;
            }catch(Exception ex){
                return null;
            }
        }
        return null;
    }
    
    public String CreateOpportunityUniversityCourse(String oppId){
        if(courseId != null && oppId != null) {
            UnivCourse = [select Id, Name,  Account__c, Fall_Enrollment__c, Spring_Enrollment__c, Winter_Enrollment__c, Summer_Enrollment__c, Adoption_Type__c
                                          from UniversityCourse__c where Id =:courseId ];
             Oppty_new = [select Id, Name from  Opportunity where Id =:oppId];
             
             new_OpportunityUniversityCourse =  new OpportunityUniversityCourse__c();
             new_OpportunityUniversityCourse.Opportunity__c = oppId;
             new_OpportunityUniversityCourse.UniversityCourse__c = courseId;
             new_OpportunityUniversityCourse.Close_Date__c = system.Date.today().addMonths(1);
             new_OpportunityUniversityCourse.Opportunity_University_Course_Amount__c = 0;
             new_OpportunityUniversityCourse.Account__c = UnivCourse.Account__c;
             new_OpportunityUniversityCourse.Opportunity_Name__c = Oppty_new.Name;
             new_OpportunityUniversityCourse.Fall__c = UnivCourse.Fall_Enrollment__c;
             new_OpportunityUniversityCourse.Spring__c = UnivCourse.Spring_Enrollment__c;
             new_OpportunityUniversityCourse.Summer__c = UnivCourse.Summer_Enrollment__c;
             new_OpportunityUniversityCourse.Winter__c = UnivCourse.Winter_Enrollment__c;
             new_OpportunityUniversityCourse.stage__c = 'Pending';
             new_OpportunityUniversityCourse.Adoption_Type__c = UnivCourse.Adoption_Type__c;
            
             try{
                insert new_OpportunityUniversityCourse;
                // create Pearson Course Equivalent 
                //String returnURL = '/apex/CreateOpportunityfromCourse?opptyid='+oppId+'&id='+courseId+'&command=CreatePearsonCourse';
                //return returnURL;
                String redirectURL=CreatePearsonCourse(oppId,courseId);
                return redirectURL;
             }catch(Exception ex){
                    return null;
                }
      }
        return null;
    }
    
    public String CreatePearsonCourse(String OppId,String CourseId){
        List<Pearson_Course_Equivalent__c> person_course = [select Id, Active__c, Course__c, Pearson_Course_Code_Hierarchy__c/*, Pearson_Course_Code__c*/, Primary__c from Pearson_Course_Equivalent__c where Course__c =:courseId];
        List<Opportunity_Pearson_Course_Code__c> oppty_Pearson_Course_Codes = new List<Opportunity_Pearson_Course_Code__c>();
        if(person_course != null && !person_course.isEmpty()){
            for(Pearson_Course_Equivalent__c eachperson_course : person_course){
                Opportunity_Pearson_Course_Code__c temp_oppty_Pearson_Course_Codes = new Opportunity_Pearson_Course_Code__c();
                temp_oppty_Pearson_Course_Codes.Opportunity__c = oppId;
                // temp_oppty_Pearson_Course_Codes.Pearson_Course_Code_Name__c = eachperson_course.Pearson_Course_Code__c;   Replaced Apttys to Non-Apttus              
                temp_oppty_Pearson_Course_Codes.Pearson_Course_Code_Hierarchy__c = eachperson_course.Pearson_Course_Code_Hierarchy__c;
                temp_oppty_Pearson_Course_Codes.Primary__c = eachperson_course.Primary__c;
                temp_oppty_Pearson_Course_Codes.Active__c = eachperson_course.Active__c;
                oppty_Pearson_Course_Codes.add(temp_oppty_Pearson_Course_Codes);
            }
            
        }else {
            return '/'+oppId;
        }
        if(oppty_Pearson_Course_Codes != null && !oppty_Pearson_Course_Codes.isEmpty()){
            try{
                insert oppty_Pearson_Course_Codes;
                return '/'+oppId;
            }catch(Exception ex){
                    return '/'+oppId;
                }
        }
        return '/'+oppId;
    }
    
}