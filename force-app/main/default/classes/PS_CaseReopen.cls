/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_CaseReopen.cls 
   Description:     This class contains methods for Email Messages Triggers
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  14/12/2015         1.0             Payal Popat                         Initial Release 
  April 7th 2017     1.2             Jaydip B                            INC3325761 updating status when case not closed
  09/05/2017         1.3             Saritha Singamsetty                 CR-01156:Extend Closed Case reopen period to 14 days
------------------------------------------------------------------------------------------------------------------------------------------------------------ */

Public without sharing class PS_CaseReopen{
    /**
    * Description : Method to send an auto-response after receiving an email to an existing closed case > 7 days
    * @param NA
    * @return String
    * @throws NA
    **/ 
    public static String mReopenCases(List<Id> CaseId,Map<Id,String> MapEmailAddress){
        List<Case> updateCaseLst= new List<Case>();
        String returnValue = '0';
        //Get the list of cases associated with this email and check the criteria for re-opening the case
        if (CaseId.size()>0){
            List<Case> caseList =new LIST <Case>([SELECT ClosedDate,ParentId,CaseNumber,Subject,ContactID,Recordtype.name,Program__c, Origin FROM CASE Where Id In: CaseId  and Status = : Label.PS_CaseClosureStatus]);
            List<Outbound_Notification__c> outnotlist= new List<Outbound_Notification__c>();
            // CR-01156: Saritha S: Added the below two lines to get Case reopen days from CS
              General_One_CRM_Settings__c ReopenDays = General_One_CRM_Settings__c.getinstance('ReopenDays');
            //if(ReopenDays != null){
              Integer CaseReopendays = (Integer)ReopenDays.CaseReopenDays__c; 
             // Added the below two line's as part of CR-3103 by Raja Gopalarao B to get the case reopen days from Custom setting for Customer Support
			General_One_CRM_Settings__c Reopendays42 = General_One_CRM_Settings__c.getinstance('Reopendays42');
			Integer caseReopendays42ForCS = (Integer)Reopendays42.CaseReopenDays__c; 
            
            for(Case objCase:caseList){
                DateTime  dT = objCase.ClosedDate ; 
                Date closeDate = date.newinstance(dT.year(), dT.month(), dT.day()); 
                Integer noOfDays = closeDate.daysBetween(date.today()); 
                //if case closed date is more than 7 days, send an email notification
               // if(noOfDays > 7 && Label.PS_ServiceRecordTypes.contains(objCase.Recordtype.name))
               // CR-01156: Saritha S: Added the below line to extend closed Case reopen period to 14 days
                /*Modified the below if condition  as part of CR-03103 by Raja Gopalarao B
                  for customer service case should reopen only after 42 days.*/
                if((noOfDays > CaseReopendays && Label.Service_Record_types_for_14_days.contains(objCase.Recordtype.name)) ||((noOfDays > caseReopendays42ForCS && (objCase.Recordtype.name == Label.PS_Customer_Service_Record_Type || objCase.Recordtype.name == Label.CaseRRRecordType))) )
                {
                    //Create Outbound Notification
                    Outbound_Notification__c outnot = new Outbound_Notification__c();
                    outnot.Event__c = 'Case AutoResponse';
                    outnot.Method__c = 'Email';
                    outnot.PS_CaseFromEmail__c = MapEmailAddress.get(objCase.Id);
                    outnot.Type__c = 'Response';
                    outnot.PS_Case__c=objCase.Id;
                   /*Description: Added below 2 lines of Code for School Assessment functionality
                    By Kaavya Kanna/Macha (SA Offshore) on 25 May 2017 */
                    outnot.CaseRecordType__c=objCase.recordtype.name;
                    outnot.Case_Program__c=objCase.Program__c;
                    outnot.Case_Origin__c=objCase.Origin;
                     
                    outnot.RelatedCaseNumber__c = objCase.CaseNumber;
                    outnot.RelatedCaseSubject__c = objCase.Subject;
                    outnotlist.add(outnot);
                    returnValue = '1';
                }else if (Label.PS_ServiceRecordTypes.contains(objCase.Recordtype.name) || objCase.Recordtype.name == Label.CaseRRRecordType){
                    //Reopen the Case
                    objCase.Status = Label.PS_Case_Additional_Information_Received;//Label.PS_CaseInProgressStatus;
                    If((objCase.Recordtype.name == Label.PS_Customer_Service_Record_Type || objCase.Recordtype.name == Label.CaseRRRecordType)){
                        objCase.Reopen_Reason__c = 'Customer replied within 42 days of case closure.';
                    } else{              
                                        
                    // objCase.Reopen_Reason__c = 'Customer replied within 7 days of case closure.';
                   // CR-01156: Saritha S: Added the below line to modify reopen reason
                    objCase.Reopen_Reason__c = 'Customer replied within 14 days of case closure.';
                    }
                    updateCaselst.add(objCase);
                    returnValue = '2';
                }
               
            }
           // }//end case for loop
            //send email to user
            if (outnotlist.size()>0){
                //try{
                    insert(outnotlist);
               // }catch(exception e){
                //    throw e;
                //}
            }
            // update Parent Case
            if(updateCaseLst.size()>0){
                try{
                    update(updateCaseLst);
                }catch(exception e){
                    throw e;
                }
            }
        } 
        return returnValue;
    }
  }