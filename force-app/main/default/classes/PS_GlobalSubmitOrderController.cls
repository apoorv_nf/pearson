/* --------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_GlobalSubmitOrderController
Description:     Creates PIU and redirects to PIU update page
Date             Version           Author                                              Summary of Changes 
-----------      ----------   -----------------     ---------------------------------------------------------------------------------------
07 july 2016      1.0           Accenture IDC                                                Created
21/07/2016        1.1           KP                                                           To set the order status to open :SUS629
25/07/2016        1.2           CP                                                           To prevent submission of cancelled order :SUS629 
25/07/2016        1.3           CP                                                           SUS635- Validation on DigitalOrderProduct 
23/08/2016        1.4           CP                                                           SUS635-Prevent order submission for Non-active ship-to and bill-to financial accounts
11/12/2018        1.5           Navaneetha Perumal.A                                 Modified the code for Project IR UK - CR # 02337 \ CRMSFDC-5840
18/01/2019        1.6           Navaneetha Perumal.A                                 Modified the code for Project IR UK - CR # 02337 \ CRMSFDC-5840 - Sharon & Kate Confirmation 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class PS_GlobalSubmitOrderController 
{
    public string orderId {get;set;}
    public string opptyId;
    public boolean displayPopUp{get;set;}
    public boolean Infopopup{get;set;}
    public Integer courseCount;
    //public List<Opportunity> lstWithOpportunity;
    public string pageRedirect{get;set;}
    public string ChannelString{get;set;} 
    public string sellingPeriod{get;set;} 
    public string opptyAccountId{get;set;} 
    public string sellingPeriodYear{get;set;} 
    public string accountId;
    public string contactId;
    public string orderRecordType;
    //public List<Order> lstWithOrderUpdate;
    public string orderMarket;
    public string orderBU;
    public string orderPurchaseOrderNum;
    public string errormsg{get;set;}
    public string infomsg {get;set;}
    public string orderChannel;
    public string orderStatus;
    public Id GlobalAssetRecordTypeId;
    public Date orderStartDate;
    public string orderSubmitted;
    Public boolean firstCheck = true;
    public List<OpportunityContactRole> lstWithOpptyCon;
    //transient Order mRev_order;
   // transient list<PS_Order_Creation_Mapping__mdt> submitOrderList;
    transient PS_Order_Creation_Mapping__mdt SubmitOrder;
    //constructor method
    public PS_GlobalSubmitOrderController(Apexpages.Standardcontroller orderObj)
    {   
        ChannelString = '';
        opptyAccountId = '';    
        sellingPeriod = '';
        displayPopUp = false;
        errormsg = '';
        orderSubmitted = '1';
        //get global asset record type id
        GlobalAssetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Global Products In Use').getRecordTypeId();
    }
    //Validations on R6 Submit Global Revenue order 
    public PageReference mSetRevenueOrder(String sOppId,List<Order> objOrder){
    try{
        // Commneted and Modified by Navaneeth for IRUK - CR # 02337 \ CRMSFDC-5840 - Start
        /*
        AggregateResult[]  mRecentQuoteCount = [select count(id)Cnt from Apttus_Proposal__Proposal__c where createdDate > : 
                                                    objOrder[0].Quote_Proposal__r.createdDate and Apttus_Proposal__Opportunity__c =: sOppId];//Quotes Created After Current orderCreation   
        if(objOrder[0].status=='Cancelled'){  // CP:25/07/2016 to prevent submission of cancelled order
            displayPopUp = true;
            errormsg = label.Cancelled_Order;
            return null;
        }
        if(objOrder[0].Quote_Proposal__r.Apttus_Proposal__Primary__c != true){
            displayPopUp = true;
            errormsg = Label.R6_Not_PrimaryQuote;
            return null ;
        }
        else if (firstCheck==true && (integer)mRecentQuoteCount[0].get('cnt')> 0){
            
            Infopopup = true;
            infomsg =Label.R6_MoreRecentQuotes;
            firstCheck = false ;
            return null; 
        }
        
        if(!objOrder[0].orderItems.isempty() && objOrder[0].orderItems!= null){  //CP : SUS635- Validation on DigitalOrderProduct 
            for(OrderItem OI :objOrder[0].orderItems){ 
                if(OI.Digital__c== true){  // CP : 24/8/2016  using the "digital" formula field 
                    
                    if(OI.Digital_Email__c == null){
                        displayPopUp = true;
                        errormsg=label.Blank_DigitalEmail;
                        return null;
                    }                        
                    else if(objOrder[0].Digital_Email__c == null) {
                        displayPopUp = true;
                        errormsg=label.Blank_DigitalEmail;
                        return null;
                    }
                }
             }//end for loop
         } 
        
        */

        if(objOrder[0].status=='Cancelled'){  
            displayPopUp = true;
            errormsg = label.Cancelled_Order;
            return null;
        }
        if(objOrder[0].Quote.IsSyncing!= true ){
            displayPopUp = true;
            errormsg = Label.R6_Not_PrimaryQuote;
            return null ;
        }
        
        // Commented by Navaneeth Start - As per confirmation from Kate & Sharon
        /*
        if(!objOrder[0].orderItems.isempty() && objOrder[0].orderItems!= null){  //CP : SUS635- Validation on DigitalOrderProduct 
            for(OrderItem OI :objOrder[0].orderItems){ 
                if(OI.Digital__c== true){  // CP : 24/8/2016  using the "digital" formula field 
                    
                    if(objOrder[0].Subscription_Email__c == null) {
                        displayPopUp = true;
                        errormsg=label.Blank_SubscriptionEmail;
                        return null;
                    }
                }
             }//end for loop
         } 
         */
         // Commented by Navaneeth End - As per confirmation from Kate & Sharon
                 
         if(objOrder[0].Purchase_Order_Number__c == null || objOrder[0].Purchase_Order_Number__c ==''){
        
         displayPopUp = true;
         errormsg=  label.Blank_PurchaseOrderNumber;
         return null; 
         }
         /*
         if(objOrder[0].Bill_To_Financial_Account__c !=null){
             if(objOrder[0].Bill_To_Financial_Account__r.ERP_Status__c!= 'Active' ){
                displayPopUp = true;
                errormsg= Label.Inactive_Billing_Accounts;
                return null; 
              }
         }
        
         if(objOrder[0].Ship_To_Financial_Account__c!=null){
              if(objOrder[0].Ship_To_Financial_Account__r.ERP_Status__c != 'Active' ){
                displayPopUp = true;
                errormsg= Label.Inactive_Shipping_Account;
                return null; 
              }
         }
         
         if(objOrder[0].Ship_To_Address__c != null){
              if(objOrder[0].Ship_To_Address__r.ERP_Address_Status__c!= 'Active' ){
                displayPopUp = true;
                errormsg= Label.R6_InactiveAccAddress;
                return null;
              }
         }
         
         if (objOrder[0].Bill_To_Address__c != null){
              if (objOrder[0].Bill_To_Address__r.ERP_Address_Status__c != 'Active'){
                displayPopUp = true;
                errormsg=Label.Inactive_billingAccount_Address ;
                return null;
              }
         }
         */ 
         // Commneted by Navaneeth for IRUK - CR # 02337 \ CRMSFDC-5840 - End
         return null; 
    }//end try
    catch(exception e ) {return null;}  
    }
    
    // Method to create Asset on order submission
    public pageReference mCreatePIURevenue(String sOppId,List<Opportunity> lstWithOpportunity,String sOrdId,String sAccId){
        pageReference  pageToRedirect; 
        try{
            if(SubmitOrder!= null){
                if(SubmitOrder.Create_Asset_On_Order_Submission__c == 1){
                    if(sOppId!= '' && sOppId!= null){
                        courseCount = [select count() from OpportunityUniversityCourse__c where Opportunity__c =: sOppId];
                        if(lstWithOpportunity.size() > 0){
                            sellingPeriodYear = lstWithOpportunity[0].Selling_Period_Year__c;
                            ChannelString = lstWithOpportunity[0].Channel__c;
                            sellingPeriod = lstWithOpportunity[0].Selling_Period__c;
                            opptyAccountId = lstWithOpportunity[0].AccountId;
                            if(sellingPeriodYear == null)  {
                                sellingPeriodYear = '';
                            }
                        }
                        if(courseCount > 0)  {    
                            pageRedirect = 'PS_CreatePIUWithCourses';
                            pageToRedirect = new pageReference('/apex/'+pageRedirect+'?opportunityId='+opptyId+'&channel='+ChannelString+'&sellingPeriod='+sellingPeriod+'&accountId='+opptyAccountId+'&sellingPeriodYear='+sellingPeriodYear+'&orderStatus='+orderStatus+'&oId='+orderId);
                        }
                        else  {
                            lstWithOpptyCon = [select Id,ContactId,Contact.Name,Role from OpportunityContactRole where OpportunityId =: sOppId limit 1];
                            if(lstWithOpptyCon.size() == 0)  {                                
                                displayPopUp = true;
                                errormsg = Label.PS_NoContactsOnOpptyForOrder;
                                return null;
                            }
                            pageRedirect = 'PS_CreatePIUWithoutCourses';
                            pageToRedirect = new pageReference('/apex/'+pageRedirect+'?opportunityId='+sOppId+'&channel='+ChannelString+'&accountId='+opptyAccountId+'&orderStatus='+orderStatus+'&oId='+sOrdId);
                        }                        
                    }//opptyid not null
                    else{
                        List<OrderItem> lstWithOrderLineItem = new List<OrderItem>();
                        Contact_Product_In_Use__c  contactProd;
                        List<Contact_Product_In_Use__c> lstWithContactProd = new List<Contact_Product_In_Use__c>();
                        Asset newAsset;
                        List<Asset> lstWithAsset = new List<Asset>();
                        if(sOrdId!= null){
                            //Map to hold asset key and orderItemId associated to it.
                            Map<String, Id> assetToOrderItemIdMap = new Map<String, Id>();
                            lstWithOrderLineItem = [select id,Quantity,PricebookEntryId,PricebookEntry.Product2Id,PricebookEntry.Product2.Name from OrderItem where orderid =: sOrdId];                        
                            if((lstWithOrderLineItem.size() > 0 && accountId != null)){
                                for(OrderItem orderLineItem : lstWithOrderLineItem){
                                    newAsset = new Asset();
                                    newAsset.Name = orderLineItem.PricebookEntry.Product2.Name;
                                    newAsset.Product2Id = orderLineItem.PricebookEntry.Product2Id; 
                                    newAsset.Status__c = 'Active'; 
                                    newAsset.AccountId = accountId; 
                                    newAsset.Quantity = orderLineItem.Quantity;
                                    newAsset.Order__c = orderId;
                                    newAsset.Order_Product_Id__c = orderLineItem.Id;
                                    newAsset.Channel__c = orderChannel;
                                    newAsset.InstallDate = Date.Today();
                                    newAsset.Expiry_Date__c = orderStartDate.addYears(1);
                                    if(GlobalAssetRecordTypeId != null)    {
                                        newAsset.RecordTypeId = GlobalAssetRecordTypeId;
                                    }
                                    lstWithAsset.add(newAsset);                                    
                                }
                            }   //orderlineitem size    
                            if(lstWithAsset.size()>0) {
                                insert lstWithAsset;
                            }
                            if(contactId != null && contactId != '' && lstWithAsset.size() > 0)   {
                                for(Asset contactPIU : lstWithAsset)  {
                                    contactProd = new Contact_Product_In_Use__c ();
                                    contactProd.Product_in_Use__c = contactPIU.Id;
                                    contactProd.Contact__c = contactId;
                                    contactProd.Status__c = 'Active';
                                    lstWithContactProd.add(contactProd);
                                } 
                                
                                if(lstWithContactProd.size() > 0) {
                                    insert lstWithContactProd;
                                }
                            }                                                                             
                        }
                        pageRedirect = 'PS_UpdatePIUWithoutCourses';
                        pageToRedirect = new pageReference('/apex/'+pageRedirect+'?accountId='+sAccId+'&channel='+ChannelString+'&orderId='+sOrdId);                        
                    }//end if oppty is null and go through order route
                }
            }//end metdata check
            return pageToRedirect;
        }//end try
        catch(exception e ) {return null;}   
    }

    
    //Method to Redirect page - called from submit order VF page
    public pageReference redirect(){
        Infopopup = false;
        pageReference  pageToRedirect;
        //Step -1 : Fetch Parameters from URL
        String opptyId = Apexpages.currentpage().getparameters().get('opportunityId');
        orderId = Apexpages.currentpage().getparameters().get('orderId');   
         accountId = Apexpages.currentpage().getparameters().get('accountId');
         contactId = Apexpages.currentpage().getparameters().get('contactId');
        String orderRecordType = Apexpages.currentpage().getparameters().get('recordType');
        String orderSubmitted = Apexpages.currentpage().getparameters().get('OrderSubmitted');    
        //Step -2: Fetch Order,Oppty and Metadata

        List<Opportunity> lstWithOpportunity = [select Id,AccountId,Selling_Period_Year__c,Selling_Period__c,StageName,Product_Count__c,
                                                Market__c,Business_Unit__c ,Line_Of_Business__c,Channel__c from Opportunity where Id =: opptyId];
        /* List<Order> objorderlst = [select Id,Opportunity.Enquiry_Type__c,Line_Of_Business__c,Ship_To_Financial_Account__r.ERP_Status__c,
                                Bill_To_Financial_Account__r.ERP_Status__c,Ship_To_Address__r.ERP_Address_Status__c,
                                Bill_To_Address__r.ERP_Address_Status__c,Quote_Proposal__r.Apttus_Proposal__Primary__c,
                                Quote_Proposal__r.createdDate,Status,EffectiveDate,Business_Unit__c, Market__c,
                                Purchase_Order_Number__c,Digital_Email__c,Sales_Channel__c,Quote.IsSyncing,Quote.CreatedDate,Subscription_Contact__c,Subscription_Email__c,
                                (select id,Digital_Email__c,Digital__c from orderItems) from order where id =: orderId limit 1]; // Added Quote Related fields  & Subscription Field on Order Object as part of IRUK by Navaneeth - CR # 02337 \ CRMSFDC-5840 */
        // Commented for CR-2949 Apttus(Deleted apttus related fields) by Rajesh Paleti
        List<Order> objorderlst = [select Id,Opportunity.Enquiry_Type__c,Line_Of_Business__c,Ship_To_Financial_Account__r.ERP_Status__c,
                                Bill_To_Financial_Account__r.ERP_Status__c,Ship_To_Address__r.ERP_Address_Status__c,
                                Bill_To_Address__r.ERP_Address_Status__c,Status,EffectiveDate,Business_Unit__c, Market__c,
                                Purchase_Order_Number__c,Digital_Email__c,Sales_Channel__c,Quote.IsSyncing,Quote.CreatedDate,Subscription_Contact__c,Subscription_Email__c,
                                (select id,Digital_Email__c,Digital__c from orderItems) from order where id =: orderId limit 1];
        
        if (objorderlst.size() > 0){                                                                           
            for( PS_Order_Creation_Mapping__mdt objOrderMetaData: [select Create_Asset_On_Order_Submission__c,Opp_Enquiry_Type__c  from 
                                    PS_Order_Creation_Mapping__mdt  where Oppty_Market__c = :objorderlst[0].market__C and 
                                    Oppty_LOB__c = :objorderlst[0].Line_Of_Business__c and Oppty_BU__c = :objorderlst[0].Business_Unit__c and 
                                    Oppty_Channel__c = :objorderlst[0].Sales_Channel__c order by Opp_Enquiry_Type__c nulls first]){
             
                if(objOrderMetaData.Opp_Enquiry_Type__c != null){
                    if(objorderlst[0].Opportunity.Enquiry_Type__c == objOrderMetaData.Opp_Enquiry_Type__c ){
                        SubmitOrder= objOrderMetaData;
                    }     
                }
                else{
                    SubmitOrder= objOrderMetaData;
                }                                                        
            }
        }    

        //step -3 : Validate Order submission
        List<Order> lstWithOrderUpdate = new List<Order>();
        List<Order> lstProcessOrder = new List<Order>();
        try{
        if(orderSubmitted == '0'){
            if(orderRecordType == 'Global Revenue Order'){
                mSetRevenueOrder(opptyId,objorderlst);// order validation
                
                if (displayPopUp == true || Infopopup == true)
                    return null;
                // CP : 29/8/2016 calling asset creation based on metadata set up
                if (SubmitOrder.Create_Asset_On_Order_Submission__c == 1 )
                    mCreatePIURevenue(opptyId,lstWithOpportunity,orderId,accountId);//creating PIU

            }
            //if(displayPopUp == false){
            for(Order orderUpd : objorderlst){
                orderUpd.Status = 'Open'; 
                orderUpd.Statusbypasscheck__c = true;
                orderUpd.Order_Submitted__c = True;
                orderMarket = orderUpd.Market__c;
                orderBU = orderUpd.Business_Unit__c;
                orderPurchaseOrderNum  = orderUpd.Purchase_Order_Number__c; 
                orderChannel = orderUpd.Sales_Channel__c;
                orderStatus = orderUpd.Status;
                orderStartDate = orderUpd.EffectiveDate;
                lstWithOrderUpdate.add(orderUpd);                
            }
            //}//sample order process
        }   //order submission check    
        else{
            displayPopUp = true;
            errormsg = 'Order has already been submitted.';
            return null;        
        }  
        if (lstWithOrderUpdate.size()>0)
            update lstWithOrderUpdate; 
        pageToRedirect = new pageReference('/'+orderId);                                           
        return pageToRedirect;
        }
        catch(Exception e)
        {
            if(e.getMessage().contains('Value does not exist or does not match filter criteria.: [Product2Id]'))  {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'You cannot create PIU for product(s) that does not belong to your market.');
                ApexPages.addMessage(myMsg);
            }else
                if(e.getMessage().contains('Shipping Zip must be of the correct format, four numeric digits')) {
                ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Error,'The Shipping Postal Code is not correctly formatted. Please update the Shipping Postal Code to four numeric digits before submitting this Order');
                ApexPages.addMessage(msg);
            }else   {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
                ApexPages.addMessage(myMsg);
            }
            return null;   
        }
    }    
    //Method to close popup
    public pageReference closePopup()
    {
        displayPopUp = false;
        pageReference pageToRedirect = new pageReference('/'+orderId);
        pageToRedirect.setRedirect(true);
        return pageToRedirect;
        
    }
}