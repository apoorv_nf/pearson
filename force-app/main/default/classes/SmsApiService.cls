/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 29/05/2018
 */
public class SmsApiService {

	private Boolean storeToken = false;
	private String endPoint;
	private Integer timeout;
	private Boolean retryWithAccessToken;

	public SmsApiService() {
		this.timeout = 120000;
		this.retryWithAccessToken = true;
	}

	public SmsApiService(Integer timeout, Boolean retryWithAccessToken) {
		this.timeout = timeout;
		this.retryWithAccessToken = retryWithAccessToken;
	}

	private Map<String, SMS_API__mdt> smsApis {
		get {
			if (this.smsApis != null) return this.smsApis;

			this.smsApis = new Map<String, SMS_API__mdt>();

			for (SMS_API__mdt smsApi : [SELECT Id, QualifiedApiName, Endpoint_Template__c FROM SMS_API__mdt]) {
				this.smsApis.put(smsApi.QualifiedApiName, smsApi);
			}

			return this.smsApis;
		} set;
	}

	private SMS_API_Settings__c smsApiSettings {
		get {
			if (this.smsApiSettings != null) return this.smsApiSettings;

			this.smsApiSettings = [SELECT Id, SSO_Token__c, API_Key__c, Username__c, Password__c FROM SMS_API_Settings__c];

			return this.smsApiSettings;
		} set;
	}

	public SmsApi.AccessCodeDetails getAccessCode(String accessCode) {
		this.endpoint = String.format(this.smsApis.get('AccessCode').Endpoint_Template__c, new List<String>{accessCode});		

		return this.getAccessCode();
	}

	public SmsApi.CurrentGenProduct getProducts(String accessCode) {
		this.endpoint = String.format(this.smsApis.get('CurrentGenProduct').Endpoint_Template__c, new List<String>{accessCode});

		return this.getProducts();
	}

	public SmsApi.CurrentGenCourse getCourse(String courseName) {
		this.endpoint = String.format(this.smsApis.get('CurrentGenCourses').Endpoint_Template__c, new List<String>{courseName});

		return this.getCourse();
	}

	private SmsApi.AccessCodeDetails getAccessCode() {
		HttpResponse response = this.getResponse();

		switch on response.getStatusCode() {
			when 200 {
				if(this.storeToken) this.updateApiSettings();
				return this.handleAccessCodeSuccess(response);
			}
			when 401 {
				if(!this.retryWithAccessToken) return (SmsApi.AccessCodeDetails)JSON.deserialize(response.getBody(), SmsApi.AccessCodeDetails.Class);
				this.setAuthenticationToken();
				return this.getAccessCode();
			}
			when else {
				return new SmsApi.AccessCodeDetails();
			}
		}
	}

	private SmsApi.CurrentGenProduct getProducts() {
		HttpResponse response = this.getResponse();

		switch on response.getStatusCode() {
			when 200 {
				if(this.storeToken) this.updateApiSettings();
				return (SmsApi.CurrentGenProduct)JSON.deserialize(response.getBody(), SmsApi.CurrentGenProduct.Class);
			}
			when 401 {
				if(!this.retryWithAccessToken) return (SmsApi.CurrentGenProduct)JSON.deserialize(response.getBody(), SmsApi.CurrentGenProduct.Class);
				this.setAuthenticationToken();
				return this.getProducts();
			}
			when else {
				return new SmsApi.CurrentGenProduct();
			}
		}
	}

	private SmsApi.CurrentGenCourse getCourse() {
		HttpResponse response = this.getResponse();

		switch on response.getStatusCode() {
			when 200 {
				if(this.storeToken) this.updateApiSettings();
				return (SmsApi.CurrentGenCourse)JSON.deserialize(response.getBody(), SmsApi.CurrentGenCourse.Class);
			}
			when 401 {
				if(!this.retryWithAccessToken) return (SmsApi.CurrentGenCourse)JSON.deserialize(response.getBody(), SmsApi.CurrentGenCourse.Class);
				this.setAuthenticationToken();
				return this.getCourse();
			}
			when else {
				return new SmsApi.CurrentGenCourse();
			}
		}
	}

	private SmsApi.AccessCodeDetails handleAccessCodeSuccess(HttpResponse response) {
		SmsApi.AccessCodeDetails accessCodeDetails = new SmsApi.AccessCodeDetails();
		Dom.Document xml = response.getBodyDocument();

		Dom.XmlNode accessCodeServiceResponseNode = xml.getRootElement();

		String status = accessCodeServiceResponseNode.getChildElement('status', null).getText();

		switch on status {
			when 'Found' {
				Dom.XmlNode accessCodeDetailsNode = accessCodeServiceResponseNode
					.getChildElement('AccessCodeDetails', null);

				accessCodeDetails.status = status;
				accessCodeDetails.cashedIn = accessCodeDetailsNode.getChildElement('CashedIn', null).getText();
				accessCodeDetails.poReqd = accessCodeDetailsNode.getChildElement('PoReqd', null).getText();

				//Some responses don't have a cachedInDate, but there isn't an easy way to check if a child node exists
				Dom.XmlNode cashedInDateNode = accessCodeDetailsNode.getChildElement('CashedInDate', null);
				if(cashedInDateNode != null)  accessCodeDetails.cashedInDate = Date.valueOf(cashedInDateNode.getText());

				accessCodeDetails.validFromDate = Date.valueOf(accessCodeDetailsNode.getChildElement('ValidFromDate', null).getText());
				accessCodeDetails.expirationDate = Datetime.valueOf(accessCodeDetailsNode.getChildElement('ExpirationDate', null).getText().left(19).replace('T', ' '));
				accessCodeDetails.creationDate = Datetime.valueOf(accessCodeDetailsNode.getChildElement('CreationDate', null).getText().left(19).replace('T', ' '));
				accessCodeDetails.numCashins = Integer.valueOf(accessCodeDetailsNode.getChildElement('NumCashins', null).getText());
				accessCodeDetails.numTimesCashedIn = Integer.valueOf(accessCodeDetailsNode.getChildElement('NumTimesCashedIn', null).getText());
			} when else {
				accessCodeDetails.status = status;
				accessCodeDetails.errorCode = Integer.valueOf(accessCodeServiceResponseNode.getChildElement('errorCode', null).getText());
			}
		}

		return accessCodeDetails;
	}

	private HttpResponse getResponse() {
		String token = String.isBlank(this.smsApiSettings.SSO_Token__c) ? '' : this.smsApiSettings.SSO_Token__c;

		HttpRequest request = new HttpRequest();
		request.setEndpoint(this.endPoint);
		request.setHeader('apikey', this.smsApiSettings.API_Key__c);
		request.setHeader('PearsonSSOSession', token);
		request.setMethod('GET');
		request.setTimeout(this.timeout);

		return new Http().send(request);
	}

	private void setAuthenticationToken() {
		SmsApi.Identity identity = this.getIdentify();
		this.smsApiSettings.SSO_Token__c = identity.tokenId;
		this.storeToken = true;
	}

	private void updateApiSettings() {
		Einstein_Bot_Event__e chatLogEvent = new Einstein_Bot_Event__e();
		chatLogEvent.Type__c = EinsteinBotEventHandler.RESET_ACCESS_TOKEN;
		chatLogEvent.Identity_Token__c = this.smsApiSettings.SSO_Token__c;

        Database.SaveResult saveResults = EventBus.publish(chatLogEvent);
	}

	public SmsApi.Identity getIdentify() {
		HttpRequest request = new HttpRequest();
		request.setEndpoint(this.smsApis.get('Identity').Endpoint_Template__c);
		request.setTimeout(this.timeout);
		request.setHeader('x-openam-username', this.smsApiSettings.Username__c);
		request.setHeader('x-openam-password', this.smsApiSettings.Password__c);
		request.setMethod('POST');

		HttpResponse response = new Http().send(request);
		SmsApi.Identity identity = (SmsApi.Identity)JSON.deserialize(response.getBody(), SmsApi.Identity.class);

		return identity;
	}
}