/* --------------------------------------------------------------------------------------------------------------------------------------

  Description:  Wrapper class used to allow custom sorting of assignment rules in Internal_Request_Assignment_Rule__mdt
  Test class:   PS_InternalRequestAssignmentTest.cls 

   Date             Version           Author                Summary of Changes 
   -----------      -------        -----------------   ----------------------------------------------------------------------
  16-Nov-2015         1.0          Tom Carman               Intial implementation
  -------------------------------------------------------------------------------------------------------------------------------------- */

public class PS_InternalRequestRuleWrapper implements Comparable {

	public String market;
	public String lineOfBusiness;
	public String recordTypeName;
	public String type;
	public String queueName;
	public String label;
	


	public PS_InternalRequestRuleWrapper(Internal_Request_Assignment_Rule__mdt rule) {

		market = rule.Market__c;
		lineOfBusiness = rule.Line_Of_Business__c;
		recordTypeName = rule.Record_Type__c;
		type = rule.Type__c;
		queueName = rule.Queue__c;
		label = rule.Label;

	}

	public Integer compareTo(Object compareTo) {

		/* Sort by: Market, Line Of Business, RecordType Name, Type */

		PS_InternalRequestRuleWrapper compareToRec = (PS_InternalRequestRuleWrapper)compareTo;

		if(market < compareToRec.market) {
			return 1;
		} else if (market > compareToRec.market) {
			return -1;
		} else if(lineOfBusiness < compareToRec.lineOfBusiness) {
			return 1;
		} else if(lineOfBusiness > compareToRec.lineOfBusiness) {
			return -1;
		} else if(recordTypeName < compareToRec.recordTypeName) {
			return 1;
		} else if(recordTypeName > compareToRec.recordTypeName) {
			return -1;
		} else if(type < compareToRec.type) {
			return 1;
		} else if(type > compareToRec.type) {
			return -1;
		} else {
			return 0;
		}


	}

}