/* -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            ScheduledCaseDeletionWithACR.cls 
Description:    This class is a scheduler test class for ScheduledCaseDeletionWithACR batch class.

Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ------------------------------------------------------------------------------------------------------------------------------------
May 20 2019       1.0                Priya                For CR-01780 : Batch Apex For Pre-Call Form Closed Case Records Deletion and Related Contact & ACR Record Deletion as well.                                                    
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
@isTest
private class ScheduledCaseDeletionWithACRTest {
    
    static testmethod void schedulerTest() {
        String CRON_EXP = '0 0 12 2 * ?';
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new ScheduledCaseDeletionWithACR());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
    
}