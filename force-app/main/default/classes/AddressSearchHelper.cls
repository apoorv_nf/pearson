/********************************************************
KP    v1.0    6/24/2016    Proxy class to call Address Validation service
********************************************************/

public class AddressSearchHelper{
    
    public static wwwPearsonComSoaSiSchemasEnterpris.EnterpriseHeader objHeader;
    public static wwwPearsonComSoaSiMdmAddressvalida.AddressDetails objAddress;
    public static wwwPearsonComSoaSiMdmAddressvalida.SearchAddressDetailsResponse objAddressResp;

    public static String mgenerateCorrelationId(){
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        return hexDigest;     
    }
    public static void mgenerateRequestHeader(){    
                        //Steps to set up Request Header
        objHeader = new wwwPearsonComSoaSiSchemasEnterpris.EnterpriseHeader();
        objHeader.HeaderVersion='1.0.0';
        //Set up Initiator        
        wwwPearsonComSoaSiSchemasEnterpris.Client  objInit = new wwwPearsonComSoaSiSchemasEnterpris.Client();
        objInit.ApplicationID=UserInfo.getOrganizationId();
        objInit.DispatchTimestamp=System.now(); 
        objInit.CorrelationID = mgenerateCorrelationId();      
        //Set up Service 
        wwwPearsonComSoaSiSchemasEnterpris.Service objService = new wwwPearsonComSoaSiSchemasEnterpris.Service();
        objService.Version='1.0.0';   
        //Set up Client
        wwwPearsonComSoaSiSchemasEnterpris.Client  objClient = new wwwPearsonComSoaSiSchemasEnterpris.Client();
        objClient.ApplicationID=UserInfo.getOrganizationId();
        objClient.DispatchTimestamp=System.now();
        objClient.CorrelationID = mgenerateCorrelationId();

        objHeader.Initiator = objInit;
        objHeader.Service = objService;
        objHeader.Client= objClient;     
    }
    // Manimala :IRUK - passed the county attribute for CR-2483 
    public static void mgenerateRequestBody(String sAddr1,String sAddr2,String sAddr3,String sAddr4,String sCity,String sState,String sCounty,String sCountry,String sZip){    
        objAddress = new wwwPearsonComSoaSiMdmAddressvalida.AddressDetails();    
        objAddress.OrganizationName='';
        objAddress.BuildingNumber1='';
        objAddress.BuildingNumber2='';
        objAddress.BuildingName1='';
        objAddress.BuildingName2='';
        if (sAddr1 != null)
            objAddress.Address1=sAddr1;        
        if (sAddr2 != null)
            objAddress.Address2=sAddr2;
        if (sAddr3 != null)        
            objAddress.Address3=sAddr3;
        if (sAddr4 != null)        
            objAddress.Address4=sAddr4;    
        objAddress.Address5='';
        objAddress.Address6='';            
        objAddress.City=sCity;
        if (sState != null)  
        {
            objAddress.Province=sState;
         }
          // Manimala :IRUK - Added for CR-2483 - code starts
         else
         {
           
           objAddress.Province=sCounty; 
             
         }
          // Manimala :IRUK - Added for CR-2483 - code ends
        objAddress.Country=sCountry;                        
        if (sZip != null)        
            objAddress.Postcode=sZip;      
    }
   
    
    //Method to call Webservice
     // Manimala :IRUK - passed the county attribute for CR-2483 
    public static List<AddressWrapper> mCallSearchService(String sAddr1,String sAddr2,String sAddr3,String sAddr4,
                                                          String sCity,String sState,String sCounty,String sCountry,String sZip){  
        List<AddressWrapper> oAddrRespList  = new List<AddressWrapper>();                                                          
        try{                                                          
        mgenerateRequestHeader();
          // Manimala :IRUK - passed the county attribute for CR-2483 
        mgenerateRequestBody(sAddr1,sAddr2,sAddr3,sAddr4,sCity,sState,sCounty,sCountry,sZip);                                                           
        wwwPearsonComSoaSiMdmAddressvalida.SearchAddressServiceSOAP objAddressReq= new wwwPearsonComSoaSiMdmAddressvalida.SearchAddressServiceSOAP();
        objAddressReq.timeout_x = 120000 ; // timeout in millisecond        
        objAddressResp = new wwwPearsonComSoaSiMdmAddressvalida.SearchAddressDetailsResponse();
        objAddressResp = objAddressReq.SearchAddressDetails(objHeader,objAddress);  
        
        List<wwwPearsonComSoaSiMdmAddressvalida.Response> objTarAddress = objAddressResp.SearchAddressDetailsResponse; 
        for(integer i=0;i<objTarAddress.size();i++){       
        if (objTarAddress[i].MatchCode != Label.AddressInvalidSearch){
          // Manimala :IRUK - passed the county attribute in addresswrapper for CR-2483 
            AddressWrapper oAddrResponse=new AddressWrapper(objTarAddress[i].Address1,objTarAddress[i].Address2,objTarAddress[i].Address3,
                objTarAddress[i].Address4,objTarAddress[i].City,objTarAddress[i].Province.replaceAll('\\s',''),objTarAddress[i].County,objTarAddress[i].Country,objTarAddress[i].PostCode,
                objTarAddress[i].OrganizationName,objTarAddress[i].MailablityScore,objTarAddress[i].MatchCode);
            oAddrRespList.add(oAddrResponse);
        }
        }  
        } //end try
        catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.CallTimeOut);
            ApexPages.addMessage(myMsg);        
            oAddrRespList  = null;   
        }
        return oAddrRespList;   
                                 
    }
    
}