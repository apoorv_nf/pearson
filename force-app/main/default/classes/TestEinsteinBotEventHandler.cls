/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 11/07/2018
 */
@isTest
private class TestEinsteinBotEventHandler {

    @TestSetup
    static void setupData(){
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {

            String firstName = 'John';
            String lastName = 'Doe';

            Account acc = new Account();
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_Creation_Request').getRecordTypeId();
            acc.Name = firstName + ' ' + lastName;

            insert acc;
            
            Contact con = new Contact();
            con.FirstName = firstName;
            con.LastName = lastName;
            con.Email = 'john@salesforce.com';
            con.AccountId = acc.Id;

            insert con;
        
            // Create a queue
            Group testGroup = new Group(Name='NAUS HETS SMS ACR', Type='Queue');
            insert testGroup;

            // Assign user to queue
            GroupMember testGrpMember = new GroupMember(GroupId = testGroup.Id, UserOrGroupId = UserInfo.getUserId());
            insert testGrpMember;

            MessagingChannel msgChannnel = new MessagingChannel(MasterLabel = '+11234567890', IsActive = TRUE, 
                                                                MessageType = 'Text', TargetQueueId = testGroup.Id,
                                                                MessagingPlatformKey = '+11234567890', DeveloperName = 'Text_11234567890'
                                                                );
            insert msgChannnel;

            // Create a test MessagingEndUser
            MessagingEndUser msgUser =  new MessagingEndUser(MessagingPlatformKey = '+12345678901', Name = '+12345678901',
                                                            IsOptedOut = FALSE, MessagingChannelId = msgChannnel.Id,
                                                            MessageType = 'Text',AccountId = acc.Id, ContactId = con.Id);
            insert msgUser;

            MessagingSession msgSession = new MessagingSession(MessagingEndUserId = msgUser.Id, MessagingChannelId = msgChannnel.Id, 
                                                            Status = 'Active');
            insert msgSession;

            // Create a queue
            Group testCaseGroup = new Group(Name='SMSBot Case Queue', Type='Queue');
            insert testCaseGroup;

            // Assign user to queue
            GroupMember testCaseGrpMember = new GroupMember(GroupId = testCaseGroup.Id, UserOrGroupId = UserInfo.getUserId());
            insert testCaseGrpMember;
        }
    }
/*
    @isTest
    private static void it_should_update_a_case() {
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];

        String chatKey = 'abc';

        Case cas = new Case();
        insert cas;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

        LiveChatTranscript transcript = new LiveChatTranscript(
            CaseId = cas.Id,
            LiveChatVisitorId = vistor.Id,
            LiveChatButtonId = [SELECT Id FROM LiveChatButton LIMIT 1].Id,
            ChatKey = chatKey);
        insert transcript;

        Einstein_Bot_Event__e event = new Einstein_Bot_Event__e();
        event.Type__c = EinsteinBotEventHandler.UPDATE_CASE;
        event.Role__c = 'Student';
        event.Email__c = 'test@email.com';
        event.Institution__c = 'Pearson';
        event.Live_Agent_Session_Id__c = chatKey;
        event.Access_Code__c = 'zxcvzxcv';
        event.Category__c = 'Category';
        event.Subcategory__c = 'Subcategory';
        event.Username__c = 'user123';
        event.Message__c = 'message';
        Database.SaveResult sr;

        Test.startTest();
        System.runAs(automatedProcess) {
            sr = EventBus.publish(event);
            Test.getEventBus().deliver();
        }
        Test.stopTest();

        //System.assert(sr.isSuccess(), sr.getErrors());

        /*System.assertEquals(1, [
            SELECT Count()
            FROM Case
            WHERE Access_Code__c = :event.Access_Code__c
            AND Customer_Username__c = :event.Username__c
        ]);*******
    }
 */   
    @isTest
    private static void it_should_create_a_contact() {
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];

        String chatKey = 'abc';

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

        LiveChatTranscript transcript = new LiveChatTranscript(ChatKey = chatKey, LiveChatVisitorId = vistor.Id);
        insert transcript;

        Einstein_Bot_Event__e event = new Einstein_Bot_Event__e();
        event.Type__c = EinsteinBotEventHandler.CREATE_CONTACT;
        event.Role__c = 'Educator';
        event.Email__c = 'test@email.com';
        event.First_Name__c = 'Rick';
        event.Last_Name__c = 'Sanchez';
        event.Institution__c = 'Council of Ricks';
        //event.Live_Agent_Session_Id__c = chatKey;
        Database.SaveResult sr;

        Test.startTest();
        System.runAs(automatedProcess) {
            sr = EventBus.publish(event);
            Test.getEventBus().deliver();
        }
        Test.stopTest();

        System.assert(sr.isSuccess(), sr.getErrors());

        System.assertEquals(1, [
            SELECT Count()
            FROM Contact
            WHERE Email = :event.Email__c
        ]);
    }

    @isTest
    private static void it_should_update_a_contact() {
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];

        String chatKey = 'abc';
        
        Account acc = [SELECT Id FROM Account LIMIT 1];

        Contact contact = new Contact(
            FirstName = 'Rick',
            LastName = 'Sanchez',
            Email = 'rick.sanchez@email.com',
            AccountId = acc.Id
        );
        insert contact;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

        LiveChatTranscript transcript = new LiveChatTranscript(
            ContactId = contact.Id,
            LiveChatVisitorId = vistor.Id,
            ChatKey = chatKey);
        insert transcript;

        EinsteinBotUpdateContact.UpdateContactRequest request = new EinsteinBotUpdateContact.UpdateContactRequest();
        request.contact = contact;
        request.role = 'Student';
        request.chatSessionKey = chatKey;

        Einstein_Bot_Event__e event = new Einstein_Bot_Event__e();
        event.Type__c = EinsteinBotEventHandler.UPDATE_CONTACT;
        event.Role__c = 'Educator';
        event.Contact_Id__c = contact.Id;
        event.Live_Agent_Session_Id__c = chatKey;

        Database.SaveResult sr;

        Test.startTest();
        System.runAs(automatedProcess) {
            sr = EventBus.publish(event);
            Test.getEventBus().deliver();
        }
        Test.stopTest();

        System.assert(sr.isSuccess(), sr.getErrors());

        System.assertEquals(1, [SELECT Count() FROM Contact WHERE Email = 'rick.sanchez@email.com' LIMIT 1]);
    }

    @isTest
    static void it_should_create_a_chat_log_resolution() {
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];

        String chatKey = 'abc';
        
        Account acc = [SELECT Id FROM Account LIMIT 1]; 
        
        Contact contact = new Contact(
                FirstName = 'Rick',
                LastName = 'Sanchez',
                Email = 'rick.sanchez2@email.com',
                AccountId = acc.Id
        );
        insert contact;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

        LiveChatTranscript transcript = new LiveChatTranscript(
                ContactId = contact.Id,
                LiveChatVisitorId = vistor.Id,
                ChatKey = chatKey);
        insert transcript;

        Einstein_Bot_Event__e event = new Einstein_Bot_Event__e();
        event.Type__c = EinsteinBotEventHandler.LOG_CHAT_RESOLUTION;
        event.Live_Agent_Session_Id__c = chatKey;
        event.Successful_Resolution__c = 'Yes';

        Database.SaveResult sr;

        Test.startTest();
        System.runAs(automatedProcess) {
            sr = EventBus.publish(event);
            Test.getEventBus().deliver();
        }
        Test.stopTest();

        System.assert(sr.isSuccess(), sr.getErrors());

        System.assertEquals(1, [SELECT Count() FROM Einstein_Bot_Chat_Log__c]);
    }

    @isTest
    static void it_should_create_a_chat_log() {
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];

        String chatKey = 'abc';

        Account acc = [SELECT Id FROM Account LIMIT 1];

        Contact contact = new Contact(
            FirstName = 'Rick',
            LastName = 'Sanchez',
            Email = 'rick.sanchez2@email.com',
            AccountId = acc.Id
        );
        insert contact;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

        LiveChatTranscript transcript = new LiveChatTranscript(
            ContactId = contact.Id,
            LiveChatVisitorId = vistor.Id,
            ChatKey = chatKey);
        insert transcript;

        Einstein_Bot_Event__e event = new Einstein_Bot_Event__e();
        event.Type__c = EinsteinBotEventHandler.LOG_CHAT;
        event.Live_Agent_Session_Id__c = chatKey;
        event.Current_Utterance__c = 'Testing';
        event.Current_Threshold_High__c = 0.5875;
        event.Current_Confidence_For_Utterance__c = 0.2981;
        event.Current_Dialog_Id__c = '12345';
        event.Current_Dialog_Name__c = 'Test12345';
        event.Successful_Resolution__c = 'Yes';

        Database.SaveResult sr;

        Test.startTest();
        System.runAs(automatedProcess) {
            sr = EventBus.publish(event);
            Test.getEventBus().deliver();
        }
        Test.stopTest();

        System.assert(sr.isSuccess(), sr.getErrors());

        System.assertEquals(1, [SELECT Count() FROM Einstein_Bot_Chat_Log__c]);
    }

    @isTest
    static void it_should_reset_access_token() {
        SMS_API_Settings__c settings = new SMS_API_Settings__c();
        settings.API_Key__c = 'abcdef';
        settings.Username__c = 'username123';
        settings.Password__c = 'password123';
        settings.SSO_Token__c = 'token1234';
        insert settings;

        Einstein_Bot_Event__e event = new Einstein_Bot_Event__e();
        event.Type__c = EinsteinBotEventHandler.RESET_ACCESS_TOKEN;
        event.Identity_Token__c = 'abc';

        Database.SaveResult sr;

        Test.startTest();
        sr = EventBus.publish(event);
        Test.getEventBus().deliver();
        Test.stopTest();

        System.assert(sr.isSuccess(), sr.getErrors());

        System.assertEquals(1, [SELECT Count() FROM SMS_API_Settings__c WHERE SSO_Token__c = 'abc']);
    }

  /*  @isTest
    private static void it_should_create_a_case() {
        List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
       insert usrLst;
     
           Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true);

       insert byp;         
       System.runas(usrLst[0]){
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];
        
        MessagingSession msgSession = [SELECT Id FROM MessagingSession LIMIT 1];

        
        Einstein_Bot_Event__e event = new Einstein_Bot_Event__e();
        event.Type__c = EinsteinBotEventHandler.INSERT_CASE;
        event.Role__c = 'Student';
        event.Email__c = 'test@email.com';
        event.Institution__c = 'Pearson';
        event.Live_Agent_Session_Id__c = msgSession.Id;
        event.Access_Code__c = 'zxcvzxcv';
        event.Category__c = 'Category';
        event.Subcategory__c = 'Subcategory';
        event.Username__c = 'user123';
        event.First_Name__c = 'Rick';
        event.Last_Name__c = 'Sanchez';
        event.Message__c = 'message';
        event.Course_Id__c = 'course';
        event.Origin__c ='SMS';

        Test.startTest();
        new EinsteinBotEventHandler().insertCase(event);
        Test.stopTest();
        System.assertEquals(1, [
            SELECT Count()
            FROM Case
            WHERE Origin = 'SMS'
            LIMIT 1
        ]);
    }
    }*/
     @isTest
    private static void it_should_create_a_case() {
        List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
       insert usrLst;
     
           Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true);

       insert byp;         
       System.runas(usrLst[0]){
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];
        
           MessagingSession msgSession = [SELECT Id FROM MessagingSession LIMIT 1];
            Einstein_Bot_Event__e event = new Einstein_Bot_Event__e();
        event.Type__c = EinsteinBotEventHandler.INSERT_CASE;
           event.Role__c = 'Student';
        event.Email__c = 'test@email.com';
        event.Institution__c = 'Pearson';
        event.Live_Agent_Session_Id__c = msgSession.Id;
        event.Access_Code__c = 'zxcvzxcv';
        event.Category__c = 'Category';
        event.Subcategory__c = 'Subcategory';
        event.Username__c = 'user123';
        event.First_Name__c = 'Rick';
        event.Last_Name__c = 'Sanchez';
        event.Message__c = 'message';
        event.Course_Id__c = 'course';
        event.Origin__c ='SMS';
         Test.startTest();
        new EinsteinBotEventHandler().insertCase(event);
           new EinsteinBotEventHandler().createContact(event);
           // new EinsteinBotEventHandler().logResolution(event);
        Test.stopTest();   
       }
    }
    @isTest
    private static void it_should_create_sms_contact() {
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];
        
        MessagingSession msgSession = [SELECT Id FROM MessagingSession LIMIT 1];

        Einstein_Bot_Event__e event = new Einstein_Bot_Event__e();
        event.Type__c = EinsteinBotEventHandler.CREATE_SMS_CONTACT;
        event.Role__c = 'Student';
        event.Email__c = 'test@email.com';
        event.Institution__c = 'Pearson';
        event.Live_Agent_Session_Id__c = msgSession.Id;
        event.Access_Code__c = 'zxcvzxcv';
        event.Category__c = 'Category';
        event.Subcategory__c = 'Subcategory';
        event.Username__c = 'user123';
        event.First_Name__c = 'Rick';
        event.Last_Name__c = 'Sanchez';
        event.Message__c = 'message';
        event.Course_Id__c = 'course';
        event.Origin__c ='SMS';
        Database.SaveResult sr;
        
        Test.startTest();
            System.runAs(automatedProcess) {
                sr = EventBus.publish(event);
                Test.getEventBus().deliver();
            }
        Test.stopTest();

        //System.assert(sr.isSuccess(), sr.getErrors());

        System.assertEquals(1, [
            SELECT Count()
                FROM Contact
                    WHERE Email = 'test@email.com'
                        LIMIT 1
        ]);
    }


}