/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            StateandCountryMap.cls
   Description:     State and Country Code Values From One CRM.
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------       -----------------------------------------------------------------------------------------------
    13-Oct-2017        1.0          Navaneeth                   State and Counrty Code Available Values into Map
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class StateandCountryMap{

 /**
    * Description : 
    * @param 
    * @return StateCode Map
    * @throws 
 **/
   public static Map<String,String> StateCodeMap()
    {
        Map<String,String> stateCodeMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = User.statecode.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            stateCodeMap.put(f.getValue(),f.getValue());
        }
        return stateCodeMap;
        
    } 
    
    

 /**
    * Description : 
    * @param 
    * @return StateCode Map
    * @throws 
 **/
   public static Map<String,String> StateDescCodeMap()
    {
        Map<String,String> stateCodeMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = User.statecode.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            stateCodeMap.put(f.getLabel(),f.getValue());
        }
        return stateCodeMap;
        
    } 
    

  /**
    * Description : 
    * @param 
    * @return CountryCode Map
    * @throws 
 **/
   public static Map<String,String> CountryCodeMap()
    {
        Map<String,String> countryCodeMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = User.Countrycode.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            countryCodeMap.put(f.getValue(),f.getValue());
        }
        return countryCodeMap;
    }
    
 /**
    * Description : 
    * @param 
    * @return CountryCode Map
    * @throws 
 **/
   public static Map<String,String> CountryDescCodeMap()
    {
        Map<String,String> countryCodeMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = User.Countrycode.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            countryCodeMap.put(f.getLabel(),f.getValue());
        }
        return countryCodeMap;
    }
}