/*=======================================================================================+
|  HISTORY                                                                               |
|                                                                                        |
|  DATE            DEVELOPER        DESCRIPTION                                          |
|  ====            =========        ===========                                          |
|  
16/10/2015      abhinav            Modifed stage name values      |
+=======================================================================================*/

@isTest(seealldata=true)
private class TestOpportunityTriggers
{   
    static testMethod void myUnitTest()
    {
        Profile ProfileNameadmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        List<User> u1 = TestDataFactory.createUser(ProfileNameadmin.Id,1);
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu1';
            u1[0].Email ='Opptyproposaltest11@testorg.com';
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='Opptyproposaltest12';
            u1[0].LanguageLocaleKey ='en_US';
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = ProfileNameadmin.Id;
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName ='standarduserQuoteCreation1@testorg.com';
            u1[0].Market__c ='US';
            u1[0].Business_Unit__c = 'Higher Ed';
            u1[0].line_of_Business__c = 'Higher Ed';
            u1[0].Product_Business_Unit__c = 'Higher Ed';
            insert u1;
        }
        
        Bypass_Settings__c bys = [Select id,SetupOwnerId,Disable_Validation_Rules__c,Disable_Process_Builder__c from Bypass_Settings__c limit 1];
        bys.SetupOwnerId = u1[0].id;
        bys.Disable_Validation_Rules__c = true;
        bys.Disable_Process_Builder__c = true;
        upsert bys;
        
        /*Bypass_Settings__c bs = new Bypass_Settings__c();
        bs.SetupOwnerId = UserInfo.getUserId();
        bs.Disable_Validation_Rules__c = true;
        bs.Disable_Process_Builder__c = true;
        upsert bs;*/
        
        System.runAs(u1[0]){ 
            TestClassAutomation.FillAllFields = true;
            List<Account> sAccount = new List<Account>();
            String RcordTypeid;
            // Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
            sAccount =TestDataFactory.createAccount(1,'Corporate');
            sAccount[0].BillingCountry         = 'Australia';
            sAccount[0].BillingState           = 'Victoria';
            sAccount[0].BillingCountryCode     = 'AU';
            sAccount[0].BillingStateCode       = 'VIC';
            sAccount[0].ShippingCountry        = 'Australia';
            sAccount[0].ShippingState          = 'Victoria';
            sAccount[0].ShippingCountryCode    = 'AU';
            sAccount[0].ShippingStateCode      = 'VIC';
            sAccount[0].Lead_Sponsor_Type__c   = null;
            sAccount[0].PS_AccountSegment1__c  ='1';
            
            insert sAccount;
            
            ActivityTemplate__c sAT1        = new ActivityTemplate__c();
            sAT1.Related_To__c              = 'Opportunity';
            sAT1.Status__c                  = 'On Schedule';
            sAT1.Stage__c                   = 'Needs Analysis';
            sAT1.Subject__c                 = 'Test opportunity activity';
            sAT1.Description__c             = 'Test opportunity activity';
            sAT1.Active__c                  = true;
            sAT1.NPS_Rating_Required__c     = true;
            sAT1.Days_Until_Due__c          = 10;
            sAT1.Account_Location__c        = 'Australia';
            insert sAT1;
            
            ActivityTemplate__c sAT2        = new ActivityTemplate__c();
            sAT2.Related_To__c              = 'Opportunity';
            sAT2.Status__c                  = 'On Schedule';
            sAT2.Stage__c                   = 'Needs Analysis';
            sAT2.Subject__c                 = 'Test opportunity activity 2';
            sAT2.Description__c             = 'Test opportunity activity 2';
            sAT2.Active__c                  = true;
            sAT2.NPS_Rating_Required__c     = true;
            sAT2.Days_Until_Due__c          = 10;
            sAT2.Account_Location__c        = 'Australia';
            insert sAT2;
            
            Test.startTest();
            
            Opportunity sOpportunity        = (Opportunity)TestClassAutomation.createSObject('Opportunity');
            sOpportunity.AccountId          = sAccount[0].Id;
            sOpportunity.StageName          = sAT1.Stage__c;
            sOpportunity.Status__c          = sAT1.Status__c;
            sOpportunity.Cadence__c         = '';
            sOpportunity.Priority__c        = '';
            sOpportunity.Enquiry_Type__c    = '';
            sOpportunity.Selling_Period__c    = 'None';
            sOpportunity.Other_Pearson_Solution__c =''; 
            //sOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
            // sOpportunity.Adoption_Type__c = 'Committee';    
            sOpportunity.PS_Registration_Campus__c=sAccount[0].id;
            sOpportunity.Registration_Payment_Reference__c= 'Testing';
            sOpportunity.Received_Signed_Registration_Contract__c = true;
            insert sOpportunity;
            
            sOpportunity.Registration_Payment_Reference__c= 'Testing';
            sOpportunity.StageName                  = 'Needs Analysis';
            sOpportunity.Renewal_Date__c            = system.today().addmonths(8);
            sOpportunity.Re_engagement_Date__c      = system.today().addmonths(1);
            
            update sOpportunity;
            
            Opportunity sClosedInsert           = new opportunity(Name='Test', CloseDate=system.today());
            
            sClosedInsert.AccountId             = sAccount[0].Id;
            sClosedInsert.StageName             = 'Negotiation';
            sClosedInsert.Re_engagement_Date__c = system.today().addMonths(1);
            sClosedInsert.Renewal_Date__c       = system.today().addMonths(12);
            insert sClosedInsert;
            
            sClosedInsert.StageName = 'Prospecting';
            update sClosedInsert;
            sClosedInsert.StageName = 'Solutioning';
            sClosedInsert.Student_Registered__c = true;
            sClosedInsert.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('D2L').getRecordTypeId(); 
            update sClosedInsert;
            sClosedInsert.StageName = 'Closed';
            update sClosedInsert;
            
            Id caseId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change Request').getRecordTypeId();    
            Case cas = new case(AccountId=sAccount[0].Id,Type = 'Change Campus',RecordTypeId=caseId,Status='New',Opportunity__c=sClosedInsert.Id,
                                Proposed_Campus__c='Durbanville',Current_Campus__c='Bedfordview',Validate_Transfer__c =true);
            insert cas;
            
            OpportunityTriggerHandler handler = new OpportunityTriggerHandler(true,1);
            
            Boolean bIsTriggerContext = handler.bIsTriggerContext;
            Boolean bIsVisualforcePageContext = handler.bIsVisualforcePageContext;
            Boolean bIsWebServiceContext = handler.bIsWebServiceContext;
            Boolean bIsExecuteAnonymousContext = handler.bIsExecuteAnonymousContext;
            
            delete sOpportunity;
            
            undelete sOpportunity;
            
            Test.stopTest();
        }
    }
    
    static testMethod void myUnitTest2()
    {
        user u =[select id,name from user where name = 'Copado Deployment'];
        
        Bypass_Settings__c bypas=[select id,name,Disable_process_builder__c from Bypass_Settings__c where SetupOwnerId =: u.Id limit 1];
        bypas.Disable_process_builder__c=true;
        update bypas;
        
        /*Bypass_Settings__c bs = new Bypass_Settings__c();
        bs.SetupOwnerId = UserInfo.getUserId();
        bs.Disable_Validation_Rules__c = true;
        bs.Disable_Process_Builder__c = true;
        upsert bs;*/
        
        system.runAs(u){
            TestClassAutomation.FillAllFields = true;
            
            List<Account> sAccount = new List<Account>();
            String RcordTypeid;
            // Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
            sAccount =TestDataFactory.createAccount(1,'Corporate');
            sAccount[0].BillingCountry         = 'Australia';
            sAccount[0].BillingState           = 'Victoria';
            sAccount[0].BillingCountryCode     = 'AU';
            sAccount[0].BillingStateCode       = 'VIC';
            sAccount[0].ShippingCountry        = 'Australia';
            sAccount[0].ShippingState          = 'Victoria';
            sAccount[0].ShippingCountryCode    = 'AU';
            sAccount[0].ShippingStateCode      = 'VIC';
            sAccount[0].Lead_Sponsor_Type__c   = null;
            sAccount[0].PS_AccountSegment1__c  ='1';
            sAccount[0].Pearson_Campus__c=true;
            sAccount[0].Lead_Sponsor_Type__c   = null;
            insert sAccount;
            Test.startTest();
            
            Opportunity sOpportunity        = (Opportunity)TestClassAutomation.createSObject('Opportunity');
            sOpportunity.AccountId          = sAccount[0].Id;
            sOpportunity.StageName          = 'Need Ananlysis';
            //sOpportunity.Status__c          = sAT1.Status__c;
            sOpportunity.Registration_Payment_Reference__c = 'Testing';
            sOpportunity.Received_Signed_Registration_Contract__c = true;
            sOpportunity.PS_Registration_Campus__c=sAccount[0].id;
            sOpportunity.Cadence__c         = '';
            sOpportunity.Priority__c         = '';
            sOpportunity.Enquiry_Type__c    = '';
            sOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
            sOpportunity.Selling_Period__c    = 'None';
            sOpportunity.Adoption_Type__c = 'Committee';
            try{
                insert sOpportunity;        
            }catch (exception e){
                
            }                   
           
            Test.stopTest();  
        }
    }
    
    static testMethod void myUnitTest3()
    {
        user u =[select id,name from user where name = 'Copado Deployment'];
        
        Bypass_Settings__c bypas=[select id,name,Disable_process_builder__c,Disable_Validation_Rules__c from Bypass_Settings__c where SetupOwnerId =: u.Id limit 1];
        bypas.Disable_process_builder__c=true;
        bypas.Disable_Validation_Rules__c = true;
        bypas.Disable_Triggers__c= True;
        update bypas;
        
        /*
        Bypass_Settings__c bs = new Bypass_Settings__c();
        bs.SetupOwnerId = UserInfo.getUserId();
        bs.Disable_Validation_Rules__c = true;
        bs.Disable_Process_Builder__c = true;
        upsert bs;
        */
        
        system.runAs(u){
            TestClassAutomation.FillAllFields = true;
            
            List<Account> sAccount = new List<Account>();
            String RcordTypeid;
            // Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
            sAccount =TestDataFactory.createAccount(1,'Corporate');
            sAccount[0].BillingCountry         = 'Australia';
            sAccount[0].BillingState           = 'Victoria';
            sAccount[0].BillingCountryCode     = 'AU';
            sAccount[0].BillingStateCode       = 'VIC';
            sAccount[0].ShippingCountry        = 'Australia';
            sAccount[0].ShippingState          = 'Victoria';
            sAccount[0].ShippingCountryCode    = 'AU';
            sAccount[0].ShippingStateCode      = 'VIC';
            sAccount[0].Lead_Sponsor_Type__c   = null;
            sAccount[0].PS_AccountSegment1__c  ='1';
            sAccount[0].Pearson_Campus__c=true;
            sAccount[0].Lead_Sponsor_Type__c   = null;
            insert sAccount;
            //Test.startTest();
            
            Opportunity sOpportunity        = (Opportunity)TestClassAutomation.createSObject('Opportunity');
            sOpportunity.AccountId          = sAccount[0].Id;
            sOpportunity.StageName          = 'Under Discussion';
            sOpportunity.Registration_Payment_Reference__c = 'Testing';
            sOpportunity.Received_Signed_Registration_Contract__c = true;
            sOpportunity.PS_Registration_Campus__c=sAccount[0].id;
            sOpportunity.Cadence__c         = '';
            sOpportunity.Priority__c         = '';
            sOpportunity.Enquiry_Type__c    = '';
            //sOpportunity.Check_Duplicates__c = false;
            sOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Global Rights Sales Opportunity').getRecordTypeId();
            sOpportunity.CurrencyIsoCode = 'GBP';
            insert sOpportunity;
            
            // Insert a test product.
            Product2 prod = new Product2(Name = '!Anda! Curso elemental para estudiantes avanzados (2-downloads)', 
                                         Family = 'Hardware',
                                         productCode = 'ABC',
                                         isActive = true);
            insert prod;
            // Get standard price book ID.        
            Id pricebookId = Test.getStandardPricebookId();
            
            //Insert a price book entry for the standard price book.        
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId,
                Product2Id = prod.Id,
                UnitPrice = 10.00,
                CurrencyIsoCode = 'GBP',
                //UseStandardPrice = true,
                IsActive = true);
            insert standardPrice; 
            
            //Create a custom price book
            Pricebook2 customPB = new Pricebook2(            
                Name='UK Higher Ed&ELT Order Price Book',
                isActive = true);
            insert customPB;
            
            // Insert a price book entry with a custom price.
            PricebookEntry customPrice = new PricebookEntry(
                Pricebook2Id = customPB.Id, 
                Product2Id = prod.Id,
                UnitPrice = 12.00,
                CurrencyIsoCode = 'GBP',
                UseStandardPrice = false,   
                IsActive = true);
            insert customPrice;
            
            OpportunityLineItem oppLine = new OpportunityLineItem(          
                OpportunityID = sOpportunity.Id,
                PricebookEntryId=customPrice.Id,
                //TotalPrice=2000,
                Quantity = 2, 
                Sales_Territory__c='Canada',    
                Territory_Africa__c='Algeria',
                Territory_Americas__c='Dominica',
                Territory_Asia_Oceania__c='Brunei Darussalam',
                Territory_Europe__c='Netherlands',
                Language__c = 'English',Discount=5.0,ServiceDate=system.Today(),UnitPrice=1,SyncedQuote_Line_Item__c=null);
            insert oppLine;
            
            System.debug('OLItem: '+oppLine);
            System.debug('Check_Duplicates__c: '+sOpportunity.Check_Duplicates__c);
            
                       
            //Opportunity opp = [select id, Check_Duplicates__c from Opportunity where id =:sOpportunity.Id];
            
            Test.startTest();
            
            System.debug('After New Transacation:::: @@@ Navanee');
            
            sOpportunity.Check_Duplicates__c = True;
            //update sOpportunity;
            
            System.debug('Duplicate: '+sOpportunity.Check_Duplicates__c);
            List<Opportunity> grlOpps = new List<Opportunity>();
            grlOpps.add(sOpportunity);
            
            Opportunity oldOpp = new Opportunity();
            oldOpp.Id = sOpportunity.Id;
            oldOpp.Check_Duplicates__c = False;
            
            OpportunityTriggerHandler oTH = new OpportunityTriggerHandler(false, 0);
            oTH.Opptyproductduplicate(grlOpps, null, new Map<Id, Opportunity>(new List<Opportunity>{oldOpp}));
            Test.stopTest();
        }
    }
}