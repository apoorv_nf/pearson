public class CopyAttachmentsToEASHandler {
    public static void afterInsert(){
        
        List<ContentDocumentLink> lstNewContDocLinksToIns = new List<ContentDocumentLink>();
        List<External_Assignment_Service__c> lstNewEAS = Trigger.new;
        List<ContentDocumentLink> lstConDocLinks =new List<ContentDocumentLink>();
        Set<id> caseIds = new Set<id>();        
        for(External_Assignment_Service__c eas: lstNewEAS){
            caseIds.add(eas.Case_Number__c);
        }
        lstConDocLinks= [SELECT LinkedEntityId, Id, ContentDocumentId FROM ContentDocumentLink where LinkedEntityId  In : caseIds];
        System.debug('@@@lstConDocLinks' +lstConDocLinks.size());
        
        Set<id> contdocIds = new Set<id>(); 
        for(ContentDocumentLink contdocLink : lstConDocLinks){
            contdocIds.add(contdocLink.ContentDocumentId);
        }
        
        
        List<ContentVersion>  cont =[SELECT Checksum,ContentDocumentId,ContentLocation,ContentSize,ContentUrl,Description,FileExtension,FileType,FirstPublishLocationId,Id,IsAssetEnabled,IsDeleted,Origin,OwnerId,PathOnClient,PublishStatus,RatingCount,ReasonForChange,SharingOption,Title,VersionData,VersionNumber FROM ContentVersion WHERE ContentDocumentId in: contdocIds];
        List<ContentVersion> ContVerlst = new List<ContentVersion>();
        for (ContentVersion Contver : cont){
            ContentVersion newcont = new ContentVersion();
            newcont.Title  = Contver.Title;
            newcont.PathOnClient  = Contver.PathOnClient;
            newcont.VersionData = Contver.VersionData;
          //  newcont.FirstPublishLocationId  = Contver.FirstPublishLocationId;
            ContVerlst.add(newcont);
        }
        insert ContVerlst;
        
        List<ContentVersion>  contversionlst =[SELECT Id,ContentDocumentId FROM ContentVersion WHERE Id in: ContVerlst];
        
        for(External_Assignment_Service__c eas : lstNewEAS){
            for(ContentVersion ContV : contversionlst){
                ContentDocumentLink condlink = new ContentDocumentLink();
                condlink.LinkedEntityId = eas.Id;
                condlink.ContentDocumentId = ContV.ContentDocumentId;
                condlink.ShareType = 'v';
                //condlink.Visibility = 'AllUsers';
                lstNewContDocLinksToIns.add(condlink);
            }    
        }
        if(lstNewContDocLinksToIns.size()>0){
            insert lstNewContDocLinksToIns;
        }
    }
}