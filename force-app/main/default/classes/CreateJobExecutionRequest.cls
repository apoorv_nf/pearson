/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            CreateJobExecutionRequest.cls 
   Description:     Creates and Inserts a single record of type Job_Execution_Request__c
   					Test Coverage by JobExecutionRequest_Scheduler.cls
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
   5/18/2016		1.0				Gregory Hardin			Created                                  
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public with sharing class CreateJobExecutionRequest {

	public static void createRequest(String type, String source) {
		Job_Execution_Request__c newJobRequest = new Job_Execution_Request__c();
		if(String.isNotBlank(type)) {
			if(String.isNotBlank(source)) {
				newJobRequest.Source__c = source;
				newJobRequest.Type__c = type;
				insert newJobRequest;
			}
		}
	}
}