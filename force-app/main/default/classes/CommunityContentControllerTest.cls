@isTest
public class CommunityContentControllerTest {
    @isTest
    public static void test1(){
        User usrRec = [Select ID from User where id = :UserInfo.getUserId()];
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrRec.id,Disable_Validation_Rules__c=true);
        insert byp;
        system.runas(usrRec){
            Community_Content__c cc = new Community_Content__c();
            cc.Community_Content_Type__c = 'Popular Topics';
            cc.Community_Type__c = 'School Assessment';
            cc.Community_Content_Detail_Group__c = 'Test';
            insert cc;
            
            Community_Content__c cc3 = new Community_Content__c();
            cc3.Community_Content_Type__c = 'Popular Topics';
            cc3.Community_Type__c = 'School Assessment';
            cc3.Community_Content_Detail_Group__c = 'Test2';
            insert cc3;
            
            Community_Content__c cc4 = new Community_Content__c();
            cc4.Community_Content_Type__c = 'Popular Topics';
            cc4.Community_Type__c = 'School Assessment';
            cc4.Community_Content_Detail_Group__c = 'Test2';
            insert cc4;
            
            Community_Content_Detail__c ccd = new Community_Content_Detail__c();
            ccd.Community_Content__c = cc.id;
            ccd.Display_Name_URL__c = 'Test';
            ccd.Google_Tracking_Id__c = 'id';
            ccd.Name = 'test';
            ccd.Visible_in_Community__c = True;
            insert ccd;
            
            Community_Content_Detail__c ccd2 = new Community_Content_Detail__c();
            ccd2.Community_Content__c = cc.id;
            ccd2.Display_Name_URL__c = 'Test';
            ccd2.Google_Tracking_Id__c = 'id';
            ccd2.Name = 'test';
            ccd2.Visible_in_Community__c = True;
            insert ccd2;
            
            Community_Content__c cc1 = new Community_Content__c();
            cc1.Community_Content_Type__c = 'Announcements';
            cc1.Community_Type__c = 'School Assessment';
            insert cc1;
            
            Community_Content_Detail__c ccd3 = new Community_Content_Detail__c();
            ccd3.Community_Content__c = cc1.id;
            ccd3.Display_Name_URL__c = 'Test';
            ccd3.Google_Tracking_Id__c = 'id';
            ccd3.Name = 'test';
            ccd3.Visible_in_Community__c = True;
            insert ccd3;
            
            CommunityContentController.getData('School Assessment', 'Popular Topics');
            CommunityContentController.getData('School Assessment', 'Announcements');
            
            CommunityContentTypePicklist ccp = new CommunityContentTypePicklist();
            ccp.getValues();
            ccp.getDefaultValue();
            
            CommunityTypePicklist ctp = new CommunityTypePicklist();
            ctp.getValues();
            ctp.getDefaultValue();
        }
    }
}