global class EE_OneCRM implements Database.Batchable<sobject>{  
  global String [] email = new String[] {'ganeshram.m2@cognizant.com'};//Add here your email address here
  
  //Start Method
  global Database.Querylocator start (Database.BatchableContext BC) {
    dateTime dtextract= CS_Update_Case_Feed__c.getInstance('Last Extract').Last_Extract_Time__c;
    if(dtextract==null) {
       dtextract=system.now().addHours(-1); 
    }
    
    //return Database.getQueryLocator('Select Id, CreatedDate, Exec_Escalation_Date_Time__c, (Select CaseId from Histories where Field=\'Exec_Escalation_Date_Time__c\') FROM Case where Exec_Escalation_Date_Time__c <> NULL and CreatedDate > ' + dtextract + ' order by CreatedDate Desc ');
    //string strQuery='Select Field,  CaseId, createdDate  From CaseHistory c where Field = \'Exec_Escalation_Date_Time__c\' and  CreatedDate > ' + dtextract +' and Exec_Escalation_Date_Time__c <> NULL  Order by CreatedDate desc';
    //system.debug('strQuery:'+strQuery);
    //return Database.getQueryLocator(strQuery);//Query which will be determine the scope of Records fetching the same
      return  database.getQueryLocator([Select Field,  CaseId, createdDate,NewValue  From CaseHistory c where Field = 'Exec_Escalation_Date_Time__c' and  CreatedDate >=:dtextract Order by CreatedDate desc]);
  }

  //Execute method
/*
  global void execute (Database.BatchableContext BC, List<sobject> scope) {
    List<case> listcasefeed = new List<case>();
    for (sobject objScope: scope) { 
      case icasefeed = (case)objScope; 
     icasefeed.Id = objScope.Id;
     listcasefeed.add(icasefeed);//Add records to the List
        System.debug('Value of listcasefeed '+ listcasefeed);
    } 

 if (listcasefeed!= null && listcasefeed.size()>0) {//Check if List is empty or not
        Database.insert(listcasefeed); System.debug('List Size '+ listcasefeed.size());//Update the Records
    }
    //update (listcasefeed);
  } */

// Modified execute Mehtod



global void execute (Database.BatchableContext BC, List<sobject> scope) {
    List<FeedItem> listcasefeed = new List<FeedItem>();
    for (sobject objScope: scope) { 
    CaseHistory icasefeed = (CaseHistory)objScope;      
    FeedItem fdItm=new FeedItem(Body='EE_OneCRM Exec Escalation Date & Time '+ icasefeed.NewValue ,ParentId=icasefeed.CaseId,createddate=icasefeed.createdDate);      

     listcasefeed.add(fdItm);//Add records to the List
        System.debug('Value of listcasefeed '+ listcasefeed);
    } 

  /* if (listcasefeed!= null && listcasefeed.size()>0) {//Check if List is empty or not
        Database.insert(listcasefeed); System.debug('List Size '+ listcasefeed.size());//Update the Records
    }*/
    insert listcasefeed;
  }


//End of modified execute method

  //Finish Method
  global void finish(Database.BatchableContext BC){
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

       CS_Update_Case_Feed__c recUpd=CS_Update_Case_Feed__c.getInstance('Last Extract');
       recUpd.Last_Extract_Time__c=system.now();
       update recUpd;

      //Below code will fetch the job Id
      AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors, a.JobType, a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById, a.CompletedDate From AsyncApexJob a WHERE id = :BC.getJobId()];//get the job Id
      System.debug('$$$ Jobid is'+BC.getJobId());
    
      //below code will send an email to User about the status
      mail.setToAddresses(email);
      mail.setReplyTo('jaydip.bhattacharya@pearson.com');//Add here your email address
      mail.setSenderDisplayName('Apex Batch Processing Module');
      mail.setSubject('Batch Processing '+a.Status);
      mail.setPlainTextBody('The Batch Apex job processed  '+a.TotalJobItems+'batches with  '+a.NumberOfErrors+'failures'+'Job Item processed are'+a.JobItemsProcessed);
  
      Messaging.sendEmail(new Messaging.SingleEmailmessage [] {mail});
  }


}