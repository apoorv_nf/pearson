/*******************************************************************************************************************
* Apex Class Name  : PS_OverideOrderNewButton
* Version          : 1.0 
* Created Date     : 07 June 2015
* Function         : Class used as Controller for Overloading of Order New Button
* Modification Log :
*
* Developer                   Date                    Description  
* ------------------------------------------------------------------------------------------------------------------
* Rony Joseph               07/06/2015              Created Initial Version of PS_OverideOrderNewButton
* Rony Joseph               12/08/2015              Update for DR-0338,DR-0339.
* Rony Joseph               12/09/2015              Update for D-2665.
* Rony Joseph               12/11/2015              Update for D-2665.
* Davi Borges               30/01/2016              Update for D-3899
* Rony Joseph               25/04/2016              Update for RD-1679.
* Rony Joseph               23/05/2016              Merging of R4 Code.     
* Jaydip Bhattacharya       9/9/2016                Parsing contact by taking 1st 15 characters, as advanced search returns long string
* Anitha Munagapati         31/10/2016              Added a middle Select Option Page 
* Vishista Reddy            28/04/2017              CR-01191: Warning the user when Do_Not_Send_Samples checkbox is checked on contact record
* Manimala                  20/07/2018              CR-2078 req 4 - Back office chatter post
********************************************************************************************************************/
public class PS_OverideOrderNewButton{


  private ApexPages.StandardController controller;
  public String retURL {get; set;}
  public String saveNewURL {get; set;}
  public String rType {get; set;}
  public String cancelURL {get; set;}
  public String ent {get; set;}
  public String confirmationToken {get; set;}
  public String accountID {get; set;}
  public Order orderRec{get;set;}
  public String mode{get;set;}
  public boolean isRedirect{get;set;}
  public String contactID{get; set;}
  public boolean isSelected{get;set;}
  public String erpLocation{get;set;} //TEPNA - 20th Jul- CR-2078 req 4 - Back office chatter post
  
  public List<AccountContact__c> lstAccountContact = new List<AccountContact__c>();
  public List<Contact> lstContact = new List<Contact>();
  public List<Contact> lstCont = new List<Contact>();
  public List<Account> accList = new List<Account>(); //TEPNA - 20th Jul- CR-2078 req 4 - Back office chatter post

public PS_OverideOrderNewButton(ApexPages.StandardController controller) {

    this.controller = controller;
    orderRec = new Order();
   retURL = ApexPages.currentPage().getParameters().get('retURL');
    rType = ApexPages.currentPage().getParameters().get('RecordType');
    cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
    ent = ApexPages.currentPage().getParameters().get('ent');
    confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
    saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
    accountID = ApexPages.currentPage().getParameters().get('aid');
    isRedirect = false;
    isSelected = false;
    OverideOrderNewButton();
    if(retURL.substring(1,4)=='001')
    {
        isRedirect = true; 
        orderRec.Order_Address_Type__c = 'Account';
        
    }
    if(retURL.substring(1,4)=='003')
    {
        String contId = retURL.remove('/');
        contactID = contId.substring(0,15);
        lstCont = [select id,Do_Not_Send_Samples__c from Contact where id=:contactID and Do_Not_Send_Samples__c = TRUE];
        if(lstCont.size() > 0)
        {
          isSelected = true;
        }
    }
    
}


public PageReference redirectMethod()
{
    isRedirect = true;
    return OverideOrderNewButton();
}

public PageReference returntoContact()
{
    PageReference redirectPage = new PageReference('/' + contactID);
    redirectPage.setRedirect(true);
    return redirectPage;
}
//TEPNA - 20th Jul- CR-2078 req 4 - Back office chatter post
public void backOfcChatPost()
{
    
            try
            {
                FeedItem posts = new FeedItem();
                String content;
                String accountURL;
            
                posts.ParentId =System.Label.USDataGroup;
                posts.CreatedById=userinfo.getuserid();
                accountURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + accountID;
                posts.Type = 'TextPost';
                content = 'Attention Group Member:';
                content+='\n\n An Order is not able to be processed for the Account linked below.  Please check that the Primary Selling ERP Billing Location field is properly populated.';
                content+='\n \n'+accountURL+'\n\n Thank you.';
                posts.Body = content;
               
                insert posts;
                
            }catch(exception e)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,e.getmessage()+'\n<br/>'+e.getStackTraceString()));
            }
}
public PageReference OverideOrderNewButton(){
       PageReference returnURL; 
       if(isRedirect)
       {
       
        set<ID> persetid = new set<ID>();
        
        General_One_CRM_Settings__c oOrdPer = General_One_CRM_Settings__c.getInstance('OrderOverrideNew');
        List<String> sPerNameSet=null;
        if (oOrdPer != null){
            sPerNameSet=oOrdPer.value__c.split(',');
            //System.debug(sPerNameSet+':'+sPerNameSet.size());
        }
         
        //for(permissionset perSet : [SELECT Id,name from permissionset where Name = 'Pearson_Backend_Order_Creation' OR Name = 'Pearson_Global_Backend_Sample' OR Name = 'Pearson_Global_Backend_Revenue' OR Name= 'Pearson_CS_Order_Entry_Team']) {
        for(permissionset perSet : [SELECT Id,name from permissionset where Name =:sPerNameSet]) {
             persetid.add(perSet.Id);
        }  
         
        List<PermissionSetAssignment> assigneduser =    [SELECT Id, PermissionSetId ,AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :Userinfo.getUserId() and PermissionSetId IN :persetid ];
         
        if(assigneduser.size()>0){
            
            //returnURL = new PageReference('/apex/PS_OverideOrderNewButtonv1'); //801/e
            returnURL = new PageReference('/801/e');
            returnURL.getParameters().put('retURL', retURL);
            returnURL.getParameters().put('RecordType', rType);
            returnURL.getParameters().put('cancelURL', cancelURL);
            returnURL.getParameters().put('ent', ent);
            returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
            returnURL.getParameters().put('save_new_url', saveNewURL);
            returnURL.getParameters().put('nooverride', '1');
            returnURL.getParameters().put('Status', 'New');
            //system.debug('retURL:'+retURL);
            user loginuser = [select id,market__c from user where id = :userinfo.getuserid() limit 1];
            
            if(loginuser.market__c == 'CA'){
                returnURL.getParameters().put('Type', 'Sample');
            }
            
    
            if (accountID != null){
                returnURL.getParameters().put('aid', accountID);
            }
        
            if(retURL.substring(1,4)=='001'){
                if(orderRec.Order_Address_Type__c == 'Account')
                {
                    system.debug('***PRINT ORDER TYPE***'+orderRec.Order_Address_Type__c);
                    lstAccountContact = [Select Contact__c,
                                                Contact__r.name,
                                                Account__r.ShippingCountryCode,
                                                Account__r.ShippingStateCode,
                                                Account__r.ShippingPostalCode, 
                                                Account__r.ShippingStreet,
                                                Account__r.ShippingCity,
                                                Account__r.BillingCountryCode,
                                                Account__r.BillingStateCode,
                                                Account__r.BillingPostalCode, 
                                                Account__r.BillingStreet,
                                                Account__r.BillingCity,
                                                Account__r.Name,
                                                Account__r.Primary_Selling_Account__r.name,
                                                Account__r.Market2__c,
                                                Account__r.Business_unit__c from AccountContact__c where Account__c =:accountID and Primary__c = true limit 1];
                     //TEPNA - 20th Jul- CR-2078 req 4 - Back office chatter post
                        accList=[Select id,Name,Billing_Address_Lookup__r.Primary_Selling_ERP_Billing_Location__c,Market2__c from Account where id=:accountID LIMIT 1];
                        erpLocation =accList[0].Billing_Address_Lookup__r.Primary_Selling_ERP_Billing_Location__c;
                    if(lstAccountContact.size() >0)
                    {
                        String formattedStreetAddress = PS_AddressFormattingUtility.formatOrderShippingAddress(lstAccountContact[0].Account__r.Name,lstAccountContact[0].Account__r.Primary_Selling_Account__r.name,lstAccountContact[0].Account__r.shippingstreet,True,Label.PS_CreateSampleOrderAccountAddress,lstAccountContact[0].Account__r.Market2__c,lstAccountContact[0].Account__r.Business_unit__c);
                        returnURL.getParameters().put('00Nb0000009hWpD', 'Account'); 
                        returnURL.getParameters().put('ShippingAddressCountry', lstAccountContact[0].Account__r.ShippingCountryCode);
                        returnURL.getParameters().put('ShippingAddressstate', lstAccountContact[0].Account__r.ShippingStateCode);
                        returnURL.getParameters().put('ShippingAddresszip', lstAccountContact[0].Account__r.ShippingPostalCode);
                        returnURL.getParameters().put('ShippingAddressstreet', formattedStreetAddress);
                        //returnURL.getParameters().put('ShippingAddressstreet', lstAccountContact[0].Account__r.ShippingStreet);
                        returnURL.getParameters().put('ShippingAddresscity', lstAccountContact[0].Account__r.ShippingCity);
                        returnURL.getParameters().put('ShipToContact', lstAccountContact[0].Contact__r.name);
                        returnURL.getParameters().put('ShipToContact_lkid', lstAccountContact[0].Contact__c);
                        returnURL.getParameters().put('EffectiveDate', System.today().format().escapeHtml4());
                        
                        returnURL.getParameters().put('BillingAddressCountry', lstAccountContact[0].Account__r.BillingCountryCode);
                        returnURL.getParameters().put('BillingAddressstate', lstAccountContact[0].Account__r.BillingStateCode);
                        returnURL.getParameters().put('BillingAddresszip', lstAccountContact[0].Account__r.BillingPostalCode);
                        //returnURL.getParameters().put('MailingAddressstreet', formattedStreetAddress);
                        returnURL.getParameters().put('BillingAddressstreet', lstAccountContact[0].Account__r.BillingStreet);
                        returnURL.getParameters().put('BillingAddresscity', lstAccountContact[0].Account__r.BillingCity);
                    }
                    //TEPNA - 20th Jul- CR-2078 req 4 - Back office chatter post
                    if(String.isempty(erpLocation) && accList[0].Market2__c =='CA')
                    {
                        backOfcChatPost(); 
                    }
                }
            }
    
            if(retURL.substring(1,4)=='003'){
                String contId = retURL.remove('/');
                //below changes to support advanced search
                contId=contId.substring(0,15);
               // system.debug('contId:'+contId);
                lstContact = [Select accountid,id,name,Mailling_Home_Address__c,Other_Home_Address__c,Preferred_Address__c,Account.ShippingCity,Account.ShippingCountry,Account.ShippingCountryCode,Account.ShippingLatitude,Account.ShippingLongitude,Account.ShippingPostalCode,Account.ShippingState,Account.ShippingStateCode,Account.ShippingStreet,OtherCity,OtherCountry,OtherCountryCode,OtherLatitude,OtherLongitude,OtherPhone,OtherPostalCode,OtherState,OtherStateCode,OtherStreet,MailingCity,MailingCountry,MailingCountryCode,MailingLatitude,MailingLongitude,MailingPostalCode,MailingState,MailingStateCode,MailingStreet,Account.Name,Account.Primary_Selling_Account__r.name,Account.Market2__c,Account.Business_unit__c from Contact where id=:contId];
                String formattedStreetAddress;
                if(lstContact.size() >0)
                {
    
                    returnURL.getParameters().put('aid', lstContact[0].accountid);
                    system.debug('***PRINT ORDER TYPE1***'+orderRec.Order_Address_Type__c);
                    if(orderRec.Order_Address_Type__c != null){
                        returnURL.getParameters().put('00Nb0000009I35Z', orderRec.Order_Address_Type__c); 
                        
                        if(orderRec.Order_Address_Type__c == 'Contact Mailing'){
                            formattedStreetAddress = PS_AddressFormattingUtility.formatOrderShippingAddress(lstContact[0].Account.Name,lstContact[0].Account.Primary_Selling_Account__r.name,lstContact[0].MailingStreet,lstContact[0].Mailling_Home_Address__c,Label.PS_CreateSampleOrderMailingAddress,lstContact[0].Account.Market2__c,lstContact[0].Account.Business_unit__c);
                            returnURL.getParameters().put('BillingAddressCountry', lstContact[0].MailingCountryCode);
                            returnURL.getParameters().put('BillingAddressstate', lstContact[0].MailingStateCode);
                            returnURL.getParameters().put('BillingAddresszip', lstContact[0].MailingPostalCode);
                            returnURL.getParameters().put('BillingAddressstreet', formattedStreetAddress);
                            //returnURL.getParameters().put('BillingAddressstreet', lstContact[0].MailingStreet);
                            returnURL.getParameters().put('BillingAddresscity', lstContact[0].MailingCity);
                            
                           /*
                            returnURL.getParameters().put('ShippingAddresscountry','');
                            returnURL.getParameters().put('ShippingAddressstate','');
                            returnURL.getParameters().put('ShippingAddresszip','');
                            returnURL.getParameters().put('ShippingAddressstreet',''); */
                            //returnURL.getParameters().put('BillingAddressstreet', lstContact[0].MailingStreet);
                            returnURL.getParameters().put('ShippingAddresscity',lstContact[0].MailingCity); 
                            returnURL.getParameters().put('ShippingAddresscountry',lstContact[0].MailingCountryCode);
                            returnURL.getParameters().put('ShippingAddressstate',lstContact[0].MailingStateCode);
                            returnURL.getParameters().put('ShippingAddresszip',lstContact[0].MailingPostalCode);
                            returnURL.getParameters().put('ShippingAddressstreet',formattedStreetAddress);
                            
                            
                        }
                        else if(orderRec.Order_Address_Type__c == 'Contact Other'){
                            
                            string billingctry;
                            formattedStreetAddress = PS_AddressFormattingUtility.formatOrderShippingAddress(lstContact[0].Account.Name,lstContact[0].Account.Primary_Selling_Account__r.name,lstContact[0].OtherStreet,lstContact[0].Other_Home_Address__c,Label.PS_CreateSampleOrderOtherAddress,lstContact[0].Account.Market2__c,lstContact[0].Account.Business_unit__c);
                            returnURL.getParameters().put('BillingAddresscountry',''); 
                           // System.currentPageReference().getParameters().put('BillingAddressCountry',null); 
                            returnURL.getParameters().put('BillingAddressstate','');
                            returnURL.getParameters().put('BillingAddresszip','');
                            returnURL.getParameters().put('BillingAddressstreet','');
                            //returnURL.getParameters().put('BillingAddressstreet', lstContact[0].MailingStreet);
                            returnURL.getParameters().put('BillingAddresscity','');
                            
                            returnURL.getParameters().put('ShippingAddressCountry', lstContact[0].OtherCountryCode);
                            returnURL.getParameters().put('ShippingAddressstate', lstContact[0].OtherStateCode);
                            returnURL.getParameters().put('ShippingAddresszip', lstContact[0].OtherPostalCode);
                            //returnURL.getParameters().put('ShippingAddressstreet', formattedStreetAddress);
                            returnURL.getParameters().put('ShippingAddressstreet', lstContact[0].OtherStreet);
                            returnURL.getParameters().put('ShippingAddresscity', lstContact[0].OtherCity);
                            
    
                        }
                        else if(orderRec.Order_Address_Type__c == 'Custom'){
                             string billingctry1;
                            returnURL.getParameters().put('BillingAddresscountry','');
                            returnURL.getParameters().put('BillingAddressstate', '');
                            returnURL.getParameters().put('BillingAddresszip', '');
                            returnURL.getParameters().put('BillingAddressstreet', '');
                            //returnURL.getParameters().put('BillingAddressstreet', lstContact[0].MailingStreet);
                            returnURL.getParameters().put('BillingAddresscity', '');
                            
                             returnURL.getParameters().put('ShippingAddresscountry','');
                            returnURL.getParameters().put('ShippingAddressstate', '');
                            returnURL.getParameters().put('ShippingAddresszip', '');
                            //returnURL.getParameters().put('ShippingAddressstreet', formattedStreetAddress);
                            returnURL.getParameters().put('ShippingAddressstreet', '');
                            returnURL.getParameters().put('ShippingAddresscity', '');
                            
                            
    
                        }
                        
                    }
                    else{
                        returnURL.getParameters().put('00Nb0000009hWpD', 'Account'); 
                        formattedStreetAddress = PS_AddressFormattingUtility.formatOrderShippingAddress(lstContact[0].Account.Name,lstContact[0].Account.Primary_Selling_Account__r.name,lstContact[0].Account.shippingstreet,True,Label.PS_CreateSampleOrderAccountAddress,lstContact[0].Account.Market2__c,lstContact[0].Account.Business_unit__c);
                        returnURL.getParameters().put('ShippingAddressCountry', lstContact[0].Account.ShippingCountryCode);
                        returnURL.getParameters().put('ShippingAddressstate', lstContact[0].Account.ShippingStateCode);
                        returnURL.getParameters().put('ShippingAddresszip', lstContact[0].Account.ShippingPostalCode);
                        returnURL.getParameters().put('ShippingAddressstreet', formattedStreetAddress);
                        //returnURL.getParameters().put('ShippingAddressstreet', lstContact[0].Account.ShippingStreet);
                        returnURL.getParameters().put('ShippingAddresscity', lstContact[0].Account.ShippingCity);
                    }
                    
                    returnURL.getParameters().put('ShipToContact', lstContact[0].name);
                    returnURL.getParameters().put('ShipToContact_lkid', lstContact[0].id);
                    returnURL.getParameters().put('EffectiveDate', System.today().format().escapeHtml4());
                    returnURL.getParameters().put('cid', contId);
                }
            }
            
        } 
        else
        {
            returnURL = new PageReference('/apex/PS_InsufficientPrivilegesErrorPage');
        }     
        returnURL.setRedirect(true);
        
        }
        return returnURL;
    }
               
}