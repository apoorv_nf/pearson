@isTest(SeeAllData = false)                   
public class ProductSearchingControllerTest{
     
     static testMethod void myTest1() { 
         List<User> usrLst= TestDataFactory.createUser(UserInfo.getProfileId());
        insert usrLst;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=UserInfo.getProfileId(),Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
        insert byp;
         Bypass_Settings__c byp1 = [select id,Disable_Triggers__c from Bypass_Settings__c where SetupOwnerId=:UserInfo.getProfileId()];
        byp1.Disable_Triggers__c = true;
         byp1.Disable_Process_Builder__c = true;
        byp1.Disable_Validation_Rules__c = true;
        upsert byp1;
         System.runas(usrLst[0]){
            Id standardPBId = Test.getStandardPricebookId();
            Account acc = new Account();
            acc.Name = 'CTI Bedfordview Campus';
            acc.Line_of_Business__c= 'Higher Ed';
            acc.Geography__c = 'Core';
            acc.Market2__c = 'UK';
            acc.Pearson_Campus__c=true;
            acc.Pearson_Account_Number__c='Campus';
            insert acc;
            
            Opportunity opp = new opportunity();
            opp.AccountId = acc.id;    
            opp.Name = 'OppTest';
            opp.StageName = 'Need Analysis';
            //opp.PriceBook2Id = standardPBId;
            opp.CloseDate = system.today();
            insert opp;
            
            Quote quote = new quote();
            PageReference pageRef = Page.CreateStandardQuote;
            pageRef.getParameters().put('oppid', opp.id);
            Id recordtypeid =  Schema.SObjectType.Quote.getRecordTypeInfosByName().get('HE Quote').getRecordTypeId();
            pageRef.getParameters().put('RecordType',recordtypeid);
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController stdQuote = new ApexPages.StandardController(quote);
            CreateStandardQuote controller = new CreateStandardQuote(stdQuote);
            //controller.Quote_create();
           
            Test.StartTest();
            //Quote quo = [SELECT ID,Name,OpportunityId From Quote LIMIT 1];
            Quote quo = new Quote();
            quo.Name= 'Test Quote';   
            quo.First_Payment_Date__c=System.today();
            quo.Payment_Period_In_Month__c='3';
            quo.Deposit__c=1000.00;
            quo.Payment_Type__c='Monthly Payment';
            quo.Opportunityid=opp.id;
            quo.Pricebook2Id=standardPBId;
            quo.Registration_Fee__c=800;
            quo.Total_Early_Bird_Securing_Fee_Payments__c=1700;
            insert quo;
            
            quo.ERP_Operating_Unit__c = 'GB Pearson Education OU';
            quo.Primary_Selling_Account__c = Opp.Account.Primary_Selling_Account__c;
            quo.Primary_Contact__c = opp.Primary_Contact__c;
            quo.Email = opp.Primary_Contact__r.Email;
            quo.Ship_To_Contact__c = opp.Primary_Contact__c; 
            quo.Subscription_Contact__c = opp.Primary_Contact__c;
            quo.Subscription_Contact_Email__c = opp.Primary_Contact__r.Email;
            quo.Market__c = opp.Market__c;
            quo.Name= opp.Name;
            quo.Business_unit__c = opp.Business_unit__c;
            quo.Line_of_Business__c = opp.Line_of_Business__c;
            quo.Geography__c = opp.Geography__c;
            quo.ERP_Operating_Unit__c = 'GB Pearson Education OU';
            quo.Freight_Override__c=true;
            quo.ExpirationDate=System.today();
            update quo;
          
            List<Product2> listProduct = new List<Product2>();
            Product2 prod = new Product2(Name = 'Companion Website for Law Express: Jurisprudence', 
                                        MDM_Profit_Centre__c = 'Law',
                                        MDM_UK_Product_Category__c = '36 months amortisation plant',
                                        Product_Series__c ='Law Express',
                                        ISBN__c='9781292210339',  
                                        Net_Price__c =  0.01, 
                                        Quantity_in_Stock__c = 0,  
                                        Publish_Date__c = system.today(),
                                        MDM_Product_Group__c = 'Law',
                                        Division__c = 'Higher Education',
                                        Imprint__c ='Pearson',
                                        MDM_UK_Product_Function__c = 'Companion Website',
                                        Product_Function__c = 'Companion Website',
                                        Family = 'Best Practices', IsActive = true);
            insert prod;
        
            
            PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, 
                                                            Product2Id = prod.Id,
                                                            UnitPrice = 10000, 
                                                            IsActive = true);
            insert standardPBE;
          
           
        
            List<Mailshot_Index_Product__c> mailshotList = new List<Mailshot_Index_Product__c>();
            Mailshot_Index_Product__c mailshot = new Mailshot_Index_Product__c(Mailshot_Description__c = 'PRIMCAR Primary Car Stock List',
                                                                               Index_Page__c = '001-Primary Stock to Carry',
                                                                               ISBN__c = prod.ISBN__c,
                                                                               Sort_Sequence__c = 20,
                                                                               IsActive__c = TRUE,
                                                                               Product2__c =prod.Id);
            insert mailshot;
            
            /*List<QuoteLineItem> qltList = new List<QuoteLineItem>();   
            QuoteLineItem qliliner = new QuoteLineItem(Product2Id=prod.id,Outside_Module__c=true,
                                                       QuoteId=quo.id,PriceBookEntryID=standardPBE.id,Quantity=4, 
                                                       UnitPrice =50);
            qltList.add(qliliner);                                                        
            insert qltList;*/
          
            ApexPages.currentPage().getParameters().put('quoId',quo.Id);
            ApexPages.currentPage().getParameters().put('prd2Id',prod.Id);
            PageReference pageRef1 = Page.ProductSearchingPage;
            pageRef1.getParameters().put('quoId', quo.id);
            ApexPages.StandardController stdQuote1 = new ApexPages.StandardController(mailshot);
            ProductSearchingController prdSearch = new ProductSearchingController(stdQuote1);
            prdSearch.searchProducts();
            prdSearch.addProductstoCart();
            prdSearch.cancelUrl();         
            Test.StopTest();
     }
    
}
}