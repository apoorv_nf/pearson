@isTest(SeeAllData = true)
public class SearchAddressTest{
    static testMethod void  myUnitTest() { 
    List<User> listWithUser = new List<User>();    
    listWithUser = TestDataFactory.createUser([select Id from Profile where Name=:'System Administrator'].Id,1);
    if(listWithUser != null && !listWithUser.isEmpty()){
        listWithUser[0].market__c='US';
        listWithUser[0].Business_Unit__c='US Field Sales';
        listWithUser[0].Line_of_Business__c='Higher Ed';
        listWithUser[0].Group__c='Higher Education';
        insert listWithUser; 
    }  
        
        SearchAddressController ocontroller1 = new SearchAddressController();  
        ocontroller1.bPrimaryFlg=true;
        ocontroller1.bShowPrimaryBilling=false; 
        System.assert(listWithUser[0].id != null, 'User creation failed');       

    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
    insert byp;        
    RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Lead' AND Name ='B2B'];    
    system.runas(listWithUser[0]){
  /*  Lead oLead = new Lead(FirstName='Schools',LastName = 'TestB2BUK',LeadSource='Field Sales',Company = 'TestB2BUKSchools',Status='Open',Email='testb2buk@testclass.com',recordtypeId=rt.Id,Organisation_Type1__c='School',Role__c='Educator',Role_Detail__c='Administrator');
    oLead.Country='India';
    oLead.City='Chennai';
    oLead.PostalCode='600012';
    oLead.Street='Madhavaram';
    oLead.State='Tamil Nadu';
    insert oLead;  
    System.assert(oLead != null, 'Lead creation failed');*/
        
    RecordType accrt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account' AND Name ='School'];
      
    account acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'CA', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
    insert acc;
          
    Account accountRecord = new Account(Name='Test B2B R6 School',Market2__c='UK',Line_of_Business__c='Schools',Business_Unit__c='BTEC & Apprenticeships',Group__c='Primary',Organisation_Type__c ='School',Type__c='Secondary',Sub_Type__c='Academy Second',
                        Phone='+91000001', ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037',TEP_Level__c ='Site',Abbreviated_Name__c='test1232', Primary_Selling_Account__c=acc.id);  

    accountRecord.recordtypeid=accrt.Id;        
    insert accountRecord;
    System.assert(accountRecord != null, 'Account creation failed');
        
    RecordType conrt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Contact' AND Name ='Global Contact'];  
    Contact conRecord = new Contact(FirstName='Schools',LastName='TestB2BUK',Market__c='UK',AccountId=accountRecord.Id,Role__c='Educator',Role_Detail__c='Chancellor',Phone='+91000001');  
    conRecord.recordtypeid=conrt.Id;
    conRecord.mailingstreet='Madhavaram';    
    conRecord.MailingCountry='India';
    conRecord.MailingCity='Chennai';
    conRecord.MailingPostalCode='600012';
    conRecord.MailingState='Tamil Nadu';
    conRecord.otherstreet='Madhavaram';    
    conRecord.otherCountry='India';
    conRecord.otherCity='Chennai';
    conRecord.otherPostalCode='600012';
    conRecord.otherState='Tamil Nadu';    
    insert conRecord;
    System.assert(conRecord != null, 'Contact creation failed');
    
    Financial_Account__c ofinAcc = new Financial_Account__c(Account__c=accountRecord.Id,Name=accountRecord.Name);
    insert ofinAcc;
    
    PageReference pageRef = Page.SearchAddress;
    Test.setCurrentPage(pageRef);
    //ApexPages.currentPage().getParameters().put('sId', oLead.Id);
    ApexPages.currentPage().getParameters().put('sObjType', 'Lead');
    SearchAddressController ocontroller = new SearchAddressController();
    Test.setMock(WebServiceMock.class,new TestSearchAddressServiceMockImpl());
    Test.starttest();
        ocontroller.mSearchAddress();
        ocontroller.mSaveAddress();
        ocontroller.accRecordId=accountRecord.Id;   
        ocontroller.bPrimaryFlg=true;
//        ocontroller.sAddressType='xyxz'; 
        ocontroller.billingtype=true;       
     
   List<Account_Addresses__c> addtoInsert = new List<Account_Addresses__c>();
        Account_Addresses__c accadd = new Account_Addresses__c(Name='Test',Account__c=accountRecord.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',/*Address_Type__c='Billing'*/Billing__c = true, Primary__c =true);
        addtoInsert.add(accadd);
        ocontroller.mAddressValidation(False);
        ocontroller.mPopulateCurrentAddr('India','IN','Chennai','INY','Madipakkam','Tamilnadu','TN');
        ocontroller.mPopulateCurrentAddrForUK('India','IN','Chennai','INY','Madipakkam','Tamilnadu');
    PageReference conpageRef = Page.SearchAddress;
    Test.setCurrentPage(conpageRef);
    ApexPages.currentPage().getParameters().put('sId', conRecord.Id);
    ApexPages.currentPage().getParameters().put('sObjType', 'Contact');  
    //ApexPages.currentPage().getParameters().put('sAddressType','Mailing'); 
    ApexPages.currentPage().getParameters().put('billingtype','true');
    SearchAddressController oconcontroller = new SearchAddressController();
          oconcontroller.AccountTepLevelValue='Site';
    oconcontroller.mSearchAddress();
    oconcontroller.mSaveAddress();   
        
    PageReference conotherpageRef = Page.SearchAddress;
    Test.setCurrentPage(conotherpageRef );
    ApexPages.currentPage().getParameters().put('sId', conRecord.Id);
    ApexPages.currentPage().getParameters().put('sObjType', 'Contact');  
    ApexPages.currentPage().getParameters().put('sAddressType','Other');          
    SearchAddressController oconothercontroller = new SearchAddressController();
    oconothercontroller.mSearchAddress();
    oconothercontroller.mSaveAddress();     
    oconothercontroller.mCancel();  
    PageReference finaccpageRef = Page.SearchAddress;
    Test.setCurrentPage(finaccpageRef);
    ApexPages.currentPage().getParameters().put('sId', ofinAcc.Id);
    ApexPages.currentPage().getParameters().put('sObjType', 'Account');         
    SearchAddressController ofinacccontroller = new SearchAddressController();
    ofinacccontroller.mInitCountry();
    ofinacccontroller.mInitState();
    ofinacccontroller.mInitCounty();
    ofinacccontroller.mInitcity();
    ofinacccontroller.mInitAddr();
    ofinacccontroller.getAddressTypes();
    ofinacccontroller.mSearchAddress();
    ofinacccontroller.mSaveAddress();     
    
    ofinacccontroller.sCountry='India';
    ofinacccontroller.sCity='Chennai';
    ofinacccontroller.bPrimaryFlg=true;
    //ofinacccontroller.sAddressType='Shipping';    
    ofinacccontroller.shippingtype=true;
    ofinacccontroller.mSaveAddress(); 
    
    ofinacccontroller.bPrimaryFlg=false;
    ofinacccontroller.bPrimaryShippingFlg=true;
    ofinacccontroller.billingtype=true;
    //ofinacccontroller.sAddressType='Billing';    
    ofinacccontroller.mSaveAddress(); 
    
    ofinacccontroller.Physicaltype=true;
    //ofinacccontroller.sAddressType='Shipping';    
    ofinacccontroller.bPrimaryphyFlg=true;
    ofinacccontroller.mSaveAddress(); 
            
    ofinacccontroller.bPrimaryFlg=true;
    //ofinacccontroller.sAddressType='Billing and Shipping';  
    ofinacccontroller.billingtype=true;
    ofinacccontroller.shippingtype=true;    
    ofinacccontroller.mSaveAddress();     
    Test.stoptest(); 
    }
	}
    static testMethod void  myUnitTest1() { 
    List<User> listWithUser = new List<User>();    
    listWithUser = TestDataFactory.createUser([select Id from Profile where Name=:'Pearson Sales User OneCRM'].Id,1);
    if(listWithUser != null && !listWithUser.isEmpty()){
        listWithUser[0].market__c='UK';
        listWithUser[0].Business_Unit__c='Schools';
        listWithUser[0].Line_of_Business__c='Schools';
        listWithUser[0].Group__c='Primary';
        insert listWithUser;
    }  
        
        SearchAddressController ocontroller1 = new SearchAddressController();  
        ocontroller1.bPrimaryFlg=true;
        ocontroller1.bShowPrimaryBilling=false; 
       System.assert(listWithUser[0].id != null, 'User creation failed');       

    //Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
    //insert byp;        
    RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Lead' AND Name ='B2B'];    
   //system.runas(listWithUser[0]){
  /*  Lead oLead = new Lead(FirstName='Schools',LastName = 'TestB2BUK',LeadSource='Field Sales',Company = 'TestB2BUKSchools',Status='Open',Email='testb2buk@testclass.com',recordtypeId=rt.Id,Organisation_Type1__c='School',Role__c='Educator',Role_Detail__c='Administrator');
    oLead.Country='India';
    oLead.City='Chennai';
    oLead.PostalCode='600012';
    oLead.Street='Madhavaram';
    oLead.State='Tamil Nadu';
    insert oLead;  
    System.assert(oLead != null, 'Lead creation failed');*/
        
    RecordType accrt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account' AND Name ='School'];    
    
    account acc = new Account (Name='Test Account1',Abbreviated_Name__c='', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'US', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
    insert acc;
    
    Account accountRecord = new Account(Name='Test B2B R6 School',Market2__c='US',Line_of_Business__c='Schools',Business_Unit__c='BTEC & Apprenticeships',Group__c='Primary',Organisation_Type__c ='School',Type__c='Secondary',Sub_Type__c='Academy Second',
                        Phone='+91000001', ShippingCountry = 'India',Abbreviated_Name__c='', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037',TEP_Level__c ='Site',Primary_Selling_Account__c=acc.id);  
    
    accountRecord.recordtypeid=accrt.Id;        
    insert accountRecord;
        
    System.assert(accountRecord != null, 'Account creation failed');
        
    RecordType conrt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Contact' AND Name ='Global Contact'];  
    Contact conRecord = new Contact(FirstName='Schools',LastName='TestB2BUK',Market__c='UK',AccountId=accountRecord.Id,Role__c='Educator',Role_Detail__c='Chancellor',Phone='+91000001');  
    conRecord.recordtypeid=conrt.Id;
    conRecord.mailingstreet='Madhavaram';    
    conRecord.MailingCountry='India';
    conRecord.MailingCity='Chennai';
    conRecord.MailingPostalCode='600012';
    conRecord.MailingState='Tamil Nadu';
    conRecord.otherstreet='Madhavaram';    
    conRecord.otherCountry='India';
    conRecord.otherCity='Chennai';
    conRecord.otherPostalCode='600012';
    conRecord.otherState='Tamil Nadu';    
    insert conRecord;
    System.assert(conRecord != null, 'Contact creation failed');
    
    Financial_Account__c ofinAcc = new Financial_Account__c(Account__c=accountRecord.Id,Name=accountRecord.Name);
    insert ofinAcc;
    
    PageReference pageRef = Page.SearchAddress;
    Test.setCurrentPage(pageRef);
    //ApexPages.currentPage().getParameters().put('sId', oLead.Id);
    ApexPages.currentPage().getParameters().put('sObjType', 'Lead');
    SearchAddressController ocontroller = new SearchAddressController();
    Test.setMock(WebServiceMock.class,new TestSearchAddressServiceMockImpl());
    Test.starttest();
        ocontroller.mSearchAddress();
        ocontroller.mSaveAddress();
        ocontroller.accRecordId=accountRecord.Id;   
        ocontroller.bPrimaryFlg=true;
        //ocontroller.sAddressType='xyxz'; 
        ocontroller.billingtype=true;   
     
     
   List<Account_Addresses__c> addtoInsert = new List<Account_Addresses__c>();
        Account_Addresses__c accadd = new Account_Addresses__c(Name='Test',Account__c=accountRecord.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',/*Address_Type__c='Billing'*/Billing__c = true, Primary__c =true);
        addtoInsert.add(accadd);
        ocontroller.mAddressValidation(False);
        ocontroller.mPopulateCurrentAddr('India','IN','Chennai','INY','Madipakkam','Tamilnadu','TN');
        ocontroller.mPopulateCurrentAddrForUK('India','IN','Chennai','INY','Madipakkam','Tamilnadu');
    PageReference conpageRef = Page.SearchAddress;
    Test.setCurrentPage(conpageRef);
    ApexPages.currentPage().getParameters().put('sId', conRecord.Id);
    ApexPages.currentPage().getParameters().put('sObjType', 'Contact');  
    //ApexPages.currentPage().getParameters().put('sAddressType','Mailing');
    ApexPages.currentPage().getParameters().put('billingtype','true');

    SearchAddressController oconcontroller = new SearchAddressController();
          oconcontroller.AccountTepLevelValue='Site';
    oconcontroller.mSearchAddress();
    oconcontroller.mSaveAddress();   
        
    PageReference conotherpageRef = Page.SearchAddress;
    Test.setCurrentPage(conotherpageRef );
    ApexPages.currentPage().getParameters().put('sId', conRecord.Id);
    ApexPages.currentPage().getParameters().put('sObjType', 'Contact');  
        ApexPages.currentPage().getParameters().put('sAddressType','Other');          
    SearchAddressController oconothercontroller = new SearchAddressController();
    oconothercontroller.AccountTepLevelValue='Site';
    oconothercontroller.mSearchAddress();
    oconothercontroller.mSaveAddress();     
    oconothercontroller.mCancel();  
    
    PageReference finaccpageRef = Page.SearchAddress;
    Test.setCurrentPage(finaccpageRef);
    ApexPages.currentPage().getParameters().put('sId', ofinAcc.Id);
    ApexPages.currentPage().getParameters().put('sObjType', 'Account');
    //ApexPages.currentPage().getParameters().put('sAddressType','None'); 
    ApexPages.currentPage().getParameters().put('shippingtype','false'); 
    
    SearchAddressController ofinacccontroller = new SearchAddressController();
    ofinacccontroller.mInitCountry();
    ofinacccontroller.mInitState();
    ofinacccontroller.mInitCounty();
    ofinacccontroller.mInitcity();
    ofinacccontroller.mInitAddr();
    ofinacccontroller.getAddressTypes();
    ofinacccontroller.sObjectType='SampleOrder';
    ofinacccontroller.mSearchAddress();
    ofinacccontroller.mSaveAddress();
    ofinacccontroller.accRecordId=accountRecord.Id;
    
    //ApexPages.currentPage().getParameters().put('sAddressType','Shipping');     
    ApexPages.currentPage().getParameters().put('shippingtype','true');     
        
    ofinacccontroller.sCountry='India';
    ofinacccontroller.sCity='Chennai';
    ofinacccontroller.shippingtype=false;
    ofinacccontroller.billingtype =true;
    ofinacccontroller.Physicaltype=true;
    ofinacccontroller.bPrimaryShippingFlg=true;
    //ofinacccontroller.bPrimaryShippingFlg=true;
    //ofinacccontroller.sAddressType='Shipping';
    ofinacccontroller.mAddressValidation(false);
    ofinacccontroller.mSaveAddress(); 
    
    ApexPages.currentPage().getParameters().put('billingtype','true'); 
        
    ofinacccontroller.bPrimaryFlg=false;
    ofinacccontroller.bPrimaryShippingFlg=true;
    //ofinacccontroller.sAddressType='Billing';
    ofinacccontroller.billingtype=false;
    ofinacccontroller.bPrimaryFlg=true;
    ofinacccontroller.mAddressValidation(false);    
    ofinacccontroller.mSaveAddress(); 
    
    ApexPages.currentPage().getParameters().put('Physicaltype','true'); 
    ofinacccontroller.shippingtype=true;
    ofinacccontroller.billingtype =true;
    ofinacccontroller.Physicaltype=false;
    ofinacccontroller.bPrimaryphyFlg=true;   
    ofinacccontroller.mAddressValidation(false);    
    ofinacccontroller.mSaveAddress();     
     
            
    ofinacccontroller.bPrimaryFlg=true;
    ApexPages.currentPage().getParameters().put('billingtype','true');
    ApexPages.currentPage().getParameters().put('shippingtype','true');
    ApexPages.currentPage().getParameters().put('Physicaltype','true');
    //ApexPages.currentPage().getParameters().put('sAddressType','Billing and Shipping');
    ofinacccontroller.billingtype=true;
    ofinacccontroller.Physicaltype=true;
    ofinacccontroller.shippingtype=true;        
    //ofinacccontroller.sAddressType='Billing and Shipping';     
    ofinacccontroller.mAddressValidation(false);          
    ofinacccontroller.mSaveAddress();
    
    // Added By Navaneeth - For Code Coverage - Start
    
    PageReference pageRefFin = Page.SearchAddress;
    Test.setCurrentPage(pageRefFin);
    ApexPages.currentPage().getParameters().put('sId', ofinAcc.Id);
    ApexPages.currentPage().getParameters().put('sObjType', 'Account');
    //ApexPages.currentPage().getParameters().put('sAddressType','Shipping'); 
    ApexPages.currentPage().getParameters().put('shippingtype','true'); 
    SearchAddressController srchAddCtrl = new SearchAddressController();
    srchAddCtrl.accRecordId=accountRecord.Id;
    srchAddCtrl.sCountry='India';
    srchAddCtrl.sCity='Chennai';           
    srchAddCtrl.billingtype = true;
    srchAddCtrl.shippingtype = true;    
    srchAddCtrl.bPrimaryFlg = true;
    srchAddCtrl.bPrimaryShippingFlg=false;    
    srchAddCtrl.mAddressValidation(false);    
           
         
    PageReference pageRefFin1 = Page.SearchAddress;
    Test.setCurrentPage(pageRefFin1);
    ApexPages.currentPage().getParameters().put('sId', ofinAcc.Id);
    ApexPages.currentPage().getParameters().put('sObjType', 'Account');
    ApexPages.currentPage().getParameters().put('billingtype','true'); 
    //ApexPages.currentPage().getParameters().put('sAddressType','Billing'); 
    SearchAddressController srchAddCtrl2= new SearchAddressController();
    srchAddCtrl2.accRecordId=accountRecord.Id;
    srchAddCtrl2.sCountry='India';
    srchAddCtrl2.sCity='Chennai';     
    srchAddCtrl2.billingtype = true;
    srchAddCtrl2.shippingtype = true;      
    srchAddCtrl2.bPrimaryFlg = false;
    srchAddCtrl2.bPrimaryShippingFlg= true;     
    srchAddCtrl2.mAddressValidation(false);     
    
    PageReference pageRefFin2 = Page.SearchAddress;
    Test.setCurrentPage(pageRefFin2);
    ApexPages.currentPage().getParameters().put('sId', ofinAcc.Id);
    ApexPages.currentPage().getParameters().put('sObjType', 'Account');
    //    ApexPages.currentPage().getParameters().put('sAddressType','Shipping'); 
    ApexPages.currentPage().getParameters().put('billingtype','true');
    ApexPages.currentPage().getParameters().put('shippingtype','true');
    
    SearchAddressController srchAddCtrl3= new SearchAddressController();
    srchAddCtrl3.accRecordId=accountRecord.Id;
    srchAddCtrl3.sCountry='India';
    srchAddCtrl3.sCity='Chennai';    
    srchAddCtrl3.billingtype = true;
    srchAddCtrl3.shippingtype = true;        
    srchAddCtrl3.bPrimaryFlg = false;
    srchAddCtrl3.bPrimaryShippingFlg= true;     
    srchAddCtrl3.mAddressValidation(false);
        
        
    PageReference pageRefFin3 = Page.SearchAddress;
    Test.setCurrentPage(pageRefFin3);
    ApexPages.currentPage().getParameters().put('sId', ofinAcc.Id);
    ApexPages.currentPage().getParameters().put('sObjType', 'Account');
//  ApexPages.currentPage().getParameters().put('sAddressType','Billing and Shipping'); 
    ApexPages.currentPage().getParameters().put('billingtype','true'); 
    ApexPages.currentPage().getParameters().put('shippingtype','true'); 

    SearchAddressController srchAddCtrl4= new SearchAddressController();
    srchAddCtrl4.accRecordId=accountRecord.Id;
    srchAddCtrl4.sCountry='India';
    srchAddCtrl4.sCity='Chennai';       
    srchAddCtrl4.billingtype = true;
    srchAddCtrl4.shippingtype = true;
        
    srchAddCtrl3.mAddressValidation(false);
        
    // Added By Navaneeth - For Code Coverage - End    
    Test.stoptest();
   
    }
    
    
    
    static testMethod void  myUnitTest2() { 
    
        List<User> listWithUser = new List<User>();    
        listWithUser = TestDataFactory.createUser([select Id from Profile where Name=:'Pearson Sales User OneCRM'].Id,1);
        if(listWithUser != null && !listWithUser.isEmpty()){
            listWithUser[0].market__c='UK';
            listWithUser[0].Business_Unit__c='Schools';
            listWithUser[0].Line_of_Business__c='Schools';
            listWithUser[0].Group__c='Primary';
            insert listWithUser;
        }  
             
        SearchAddressController ocontroller1 = new SearchAddressController();  
        ocontroller1.bPrimaryFlg=true;
        ocontroller1.bPrimaryShippingFlg=false; 
        System.assert(listWithUser[0].id != null, 'User creation failed');  
        
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Lead' AND Name ='B2B'];    

        RecordType accrt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account' AND Name ='School'];    
        
        account acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'CA', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
        insert acc;
        
        Account accountRecord = new Account(Name='Test B2B R6 School',Market2__c='UK',Line_of_Business__c='Schools',Business_Unit__c='BTEC & Apprenticeships',Group__c='Primary',Organisation_Type__c ='School',Type__c='Secondary',Sub_Type__c='Academy Second',
                            Phone='+91000001', ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037',TEP_Level__c ='Site',Primary_Selling_Account__c=acc.id);  

        accountRecord.recordtypeid=accrt.Id;        
        insert accountRecord;
        
        Financial_Account__c ofinAcc = new Financial_Account__c(Account__c=accountRecord.Id,Name=accountRecord.Name);
        insert ofinAcc;
        
        PageReference pageRef = Page.SearchAddress;
        Test.setCurrentPage(pageRef);
        //ApexPages.currentPage().getParameters().put('sId', oLead.Id);
        ApexPages.currentPage().getParameters().put('sObjType', 'Lead');
        SearchAddressController ocontroller = new SearchAddressController();
        Test.setMock(WebServiceMock.class,new TestSearchAddressServiceMockImpl());
        Test.starttest();
            ocontroller.mSearchAddress();
            ocontroller.mSaveAddress();
            ocontroller.accRecordId=accountRecord.Id;   
            ocontroller.bPrimaryShippingFlg=true;
            //ocontroller.sAddressType='xyxz'; 
            ocontroller.shippingtype=true;   
         
         
       List<Account_Addresses__c> addtoInsert = new List<Account_Addresses__c>();
            Account_Addresses__c accadd = new Account_Addresses__c(Name='Test',Account__c=accountRecord.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',/*Address_Type__c='Billing'*/Billing__c = true, Primary__c =true,Shipping__c=true,PrimaryShipping__c =true);
            addtoInsert.add(accadd);
            ocontroller.mAddressValidation(False);
            ocontroller.mPopulateCurrentAddr('India','IN','Chennai','INY','Madipakkam','Tamilnadu','TN');
            ocontroller.mPopulateCurrentAddrForUK('India','IN','Chennai','INY','Madipakkam','Tamilnadu');
        PageReference conpageRef = Page.SearchAddress;
        Test.setCurrentPage(conpageRef);
        
        //ApexPages.currentPage().getParameters().put('sId', conRecord.Id);
        ApexPages.currentPage().getParameters().put('sObjType', 'Contact');
        ApexPages.currentPage().getParameters().put('sAddressType','Mailing');
        ApexPages.currentPage().getParameters().put('shippingtype','true');

        SearchAddressController oconcontroller = new SearchAddressController();
              oconcontroller.AccountTepLevelValue='Site';
              oconcontroller.sAddressType ='Mailing';
        oconcontroller.mSearchAddress();
        oconcontroller.mSaveAddress();  

            PageReference finaccpageRef = Page.SearchAddress;
        Test.setCurrentPage(finaccpageRef);
        ApexPages.currentPage().getParameters().put('sId', ofinAcc.Id);
        ApexPages.currentPage().getParameters().put('sObjType', 'Account');
        //ApexPages.currentPage().getParameters().put('sAddressType','None'); 
        ApexPages.currentPage().getParameters().put('shippingtype','false'); 
        
        SearchAddressController ofinacccontroller = new SearchAddressController();
        ofinacccontroller.mInitCountry();
        ofinacccontroller.mInitState();
        ofinacccontroller.mInitCounty();
        ofinacccontroller.mInitcity();
        ofinacccontroller.mInitAddr();
        ofinacccontroller.getAddressTypes();
        ofinacccontroller.mSearchAddress();
        ofinacccontroller.mSaveAddress();
        ofinacccontroller.accRecordId=accountRecord.Id;
        
        //ApexPages.currentPage().getParameters().put('sAddressType','Shipping');     
        ApexPages.currentPage().getParameters().put('shippingtype','true');     
            
        ofinacccontroller.sCountry='India';
        ofinacccontroller.sCity='Chennai';
        ofinacccontroller.shippingtype=true;
        ofinacccontroller.billingtype =false;
        ofinacccontroller.Physicaltype=false;
        ofinacccontroller.bPrimaryShippingFlg=true;
        //ofinacccontroller.bPrimaryShippingFlg=true;
        //ofinacccontroller.sAddressType='Shipping';
        ofinacccontroller.mAddressValidation(false);
        ofinacccontroller.mSaveAddress(); 
        Test.stoptest();
} 
    
    
    static testMethod void  myUnitTest3() { 
    
        List<User> listWithUser = new List<User>();    
        listWithUser = TestDataFactory.createUser([select Id from Profile where Name=:'Pearson Sales User OneCRM'].Id,1);
        if(listWithUser != null && !listWithUser.isEmpty()){
            listWithUser[0].market__c='UK';
            listWithUser[0].Business_Unit__c='Schools';
            listWithUser[0].Line_of_Business__c='Schools';
            listWithUser[0].Group__c='Primary';
            insert listWithUser;
        }  
        
        
        SearchAddressController ocontroller1 = new SearchAddressController();  
        ocontroller1.bPrimaryphyFlg=true;
        ocontroller1.bPrimaryShippingFlg=false; 
        System.assert(listWithUser[0].id != null, 'User creation failed');   
        
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Lead' AND Name ='B2B'];    

    
        RecordType accrt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account' AND Name ='School'];    
    
    account acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'CA', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
    insert acc;
    
    Account accountRecord = new Account(Name='Test B2B R6 School',Market2__c='UK',Line_of_Business__c='Schools',Business_Unit__c='BTEC & Apprenticeships',Group__c='Primary',Organisation_Type__c ='School',Type__c='Secondary',Sub_Type__c='Academy Second',
                        Phone='+91000001', ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037',TEP_Level__c ='Site',Primary_Selling_Account__c=acc.id);  

    accountRecord.recordtypeid=accrt.Id;        
    insert accountRecord;
    
    RecordType conrt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Contact' AND Name ='Global Contact'];  
    Contact conRecord = new Contact(FirstName='Schools',LastName='TestB2BUK',Market__c='UK',AccountId=accountRecord.Id,Role__c='Educator',Role_Detail__c='Chancellor',Phone='+91000001');  
    conRecord.recordtypeid=conrt.Id;
    conRecord.mailingstreet='Madhavaram';    
    conRecord.MailingCountry='United Kingdom';
    conRecord.MailingCity='Cardiff';
    conRecord.MailingPostalCode='600012';
    conRecord.MailingState='Cardiff';
    conRecord.otherstreet='Madhavaram';    
    conRecord.otherCountry='United Kingdom';
    conRecord.otherCity='Cardiff';
    conRecord.otherPostalCode='600012';
    conRecord.otherState='Cardiff';    
    insert conRecord;
    System.assert(conRecord != null, 'Contact creation failed');

    
    Financial_Account__c ofinAcc = new Financial_Account__c(Account__c=accountRecord.Id,Name=accountRecord.Name);
    insert ofinAcc;
    
    PageReference pageRef = Page.SearchAddress;
        Test.setCurrentPage(pageRef);
        //ApexPages.currentPage().getParameters().put('sId', oLead.Id);
        ApexPages.currentPage().getParameters().put('sObjType', 'Lead');
        SearchAddressController ocontroller = new SearchAddressController();
        Test.setMock(WebServiceMock.class,new TestSearchAddressServiceMockImpl());
        Test.starttest();
            ocontroller.mSearchAddress();
            ocontroller.mSaveAddress();
            ocontroller.accRecordId=accountRecord.Id;   
            ocontroller.bPrimaryphyFlg=true;
            //ocontroller.sAddressType='xyxz'; 
            ocontroller.Physicaltype =true;   
         
         
       List<Account_Addresses__c> addtoInsert = new List<Account_Addresses__c>();
            Account_Addresses__c accadd = new Account_Addresses__c(Name='Test',Account__c=accountRecord.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',/*Address_Type__c='Billing'*/Physical__c=true,Primary_Physical__c=true);
            addtoInsert.add(accadd);
            ocontroller.mAddressValidation(False);
            ocontroller.mPopulateCurrentAddr('India','IN','Chennai','INY','Madipakkam','Tamilnadu','TN');
            ocontroller.mPopulateCurrentAddrForUK('India','IN','Chennai','INY','Madipakkam','Tamilnadu');
        PageReference conpageRef = Page.SearchAddress;
        Test.setCurrentPage(conpageRef);
        
        ApexPages.currentPage().getParameters().put('sId', conRecord.Id);
        ApexPages.currentPage().getParameters().put('sObjType', 'Contact');
        ApexPages.currentPage().getParameters().put('sAddressType','Mailing');
        ApexPages.currentPage().getParameters().put('Physicaltype ','true');

        SearchAddressController oconcontroller = new SearchAddressController();
        oconcontroller.AccountTepLevelValue='Site';
        oconcontroller.sAddressType ='Mailing';
        oconcontroller.mSearchAddress();
        oconcontroller.mSaveAddress();
        
        PageReference finaccpageRef = Page.SearchAddress;
        Test.setCurrentPage(finaccpageRef);
        ApexPages.currentPage().getParameters().put('sId', ofinAcc.Id);
        ApexPages.currentPage().getParameters().put('sObjType', 'Account');
        //ApexPages.currentPage().getParameters().put('sAddressType','None'); 
        ApexPages.currentPage().getParameters().put('Physicaltype','false'); 
        
        SearchAddressController ofinacccontroller = new SearchAddressController();
        ofinacccontroller.mInitCountry();
        ofinacccontroller.mInitState();
        ofinacccontroller.mInitCounty();
        ofinacccontroller.mInitcity();
        ofinacccontroller.mInitAddr();
        ofinacccontroller.getAddressTypes();
        ofinacccontroller.mSearchAddress();
        ofinacccontroller.mSaveAddress();
        ofinacccontroller.accRecordId=accountRecord.Id;
        
        //ApexPages.currentPage().getParameters().put('sAddressType','Shipping');     
        ApexPages.currentPage().getParameters().put('Physicaltype ','true');     
            
        ofinacccontroller.sCountry='India';
        ofinacccontroller.sCity='Chennai';
        ofinacccontroller.shippingtype=false;
        ofinacccontroller.billingtype =false;
        ofinacccontroller.Physicaltype=true;
        ofinacccontroller.bPrimaryShippingFlg=true;
        //ofinacccontroller.bPrimaryShippingFlg=true;
        //ofinacccontroller.sAddressType='Shipping';
        ofinacccontroller.mAddressValidation(false);
        ofinacccontroller.mSaveAddress(); 
        Test.stoptest();
    } 
  }