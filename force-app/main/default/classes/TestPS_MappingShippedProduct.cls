/*Replaced Qualification__c field by QuoteMigration_Qualification__c as part of ZA Apttus Decomm by Mani - 07/25/2018*/
@isTest
private Class TestPS_MappingShippedProduct{
    static testMethod void myUnitTest(){
    List<User> listWithUser = new List<User>();
                  
         Map<Id,OrderItem> newItems = new Map<Id,OrderItem>();
         Map<Id,OrderItem> oldItems = new Map<Id,OrderItem>();
         Id ordrId;
        // Commented and Modified by Navaneeth - Start 
        //listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        Profile profileId = [SELECT Id FROM Profile WHERE Name =: 'System Administrator' limit 1];

        User u = new user();
        u.LastName = 'LIVESTONAdT';
        u.FirstName='JASONAdT';
        u.Alias = 'jnavte';
        u.Email = 'jason.livestonT@pearson.com';
        u.Username = 'jason.livestonT@asdfsdf.com';                          
        u.ProfileId = profileId.id;                           
        u.TimeZoneSidKey = 'GMT';
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LocaleSidKey = 'en_US';
        u.License_Pools__c = 'test';
        listWithUser.add(u);
        
        insert listWithUser; 
        // Commented and Modified by Navaneeth - End
       Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Triggers__c=true);
       insert byp;
        System.runAs(listWithUser[0]){
        
            account acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', Line_of_Business__c= 'Schools', Geography__c= 'Core', ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
            acc.Pearson_Campus__c=true;
            insert acc;
            Opportunity opp = new opportunity ( Name = 'OpporName1',AccountId = acc.Id,CloseDate = System.TODAY() + 30,StageName = 'OpporStageName',Channel__c='Direct', QuoteMigration_Qualification__c = 'Graphic Design');
            insert opp;
            Contact con = new Contact(FirstName='TestContactFirstname1', LastName='TestContactLastname',AccountId=opp.accountid ,Salutation='MR.', Email= 'abcd@email.com',MobilePhone='111222333' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
            insert con;
            OpportunityContactRole oc = new OpportunityContactRole( opportunityId =  opp.Id,contactId = con.id ,Role =  'Business User');
            insert oc;
            String userId = UserInfo.getUserId();
            list<User> currentUser = [SELECT Id, Market__c, Line_of_Business__c, Product_Business_Unit__c FROM User WHERE Id =:userId];
            product2  prod = new product2(name='test', ISBN__c = '9781292099545', Market__c = currentUser[0].Market__c, Line_of_Business__c = currentUser[0].Line_of_Business__c, Business_Unit__c = currentUser[0].Product_Business_Unit__c );
            insert prod;
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry standardPrice = new PricebookEntry( Pricebook2Id = pricebookId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true); 
            insert customPB;
            PricebookEntry pbe = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id, UnitPrice = 0, currencyIsocode = userinfo.getDefaultCurrency());
            insert pbe;
            order sampleorder1 = new order(Accountid=opp.accountid,ShipToContactid=con.id,Opportunityid=opp.id,EffectiveDate=system.today(),Status='New',Pricebook2Id=customPB.id,CurrencyIsoCode = userinfo.getDefaultCurrency(),type=System.Label.Order_Type, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371', ShippingState = 'Karnataka', Salesperson__c = UserInfo.getUserId());
            insert sampleorder1;
            orderitem item1 = new orderitem(orderid=sampleorder1.id,Shipped_Product__c = prod.id,Shipped_Product_ISBN10__c = 'ABCD123456',Quantity = 1,pricebookentryid= pbe.id, unitprice=0,Shipping_Method__c= 'Ground' );
            insert item1;
            oldItems.put(item1.id,item1);
            newItems.put(item1.id,item1);

         
        Test.StartTest();
            PS_MappingShippedProduct firemethod = new PS_MappingShippedProduct();
            firemethod.mappShippedProduct(newItems,oldItems);       
        Test.StopTest();
        
    }
    
}
}