public without sharing class Peak_FileUploaderController {
    public class FileWrapper{
        @AuraEnabled
        @TestVisible Integer maxSize {get;set;}
        @AuraEnabled
        @TestVisible String extensions {get;set;}

        public FileWrapper(Integer MaxFileSize, String AllowedExtensions){
            maxSize = MaxFileSize;
            extensions = AllowedExtensions;
        }
    }
    @AuraEnabled
    public static FileWrapper getFileDetails(){
        Network curNetwork;
        List<Network> networkList;
        if(Test.isRunningTest()){
            networkList = [SELECT Id, MaxFileSizeKb, AllowedExtensions FROM Network];
        }else{
            networkList = [SELECT Id, MaxFileSizeKb, AllowedExtensions FROM Network WHERE Id = :Network.getNetworkId()];
        }

        if (!Peak_Utils.isNullOrEmpty(networkList)) {
            curNetwork = networkList[0];
        }
        FileWrapper fileInfo = new FileWrapper(curNetwork.MaxFileSizeKb, curNetwork.AllowedExtensions);
        return fileInfo;
    }
    @AuraEnabled
    public static String saveChunk(String parentId, String fileName, String base64Data, String contentType, String fileId) {
        Attachment uploadedFile;
        if (fileId == '' || fileId == null) {
            uploadedFile = saveTheFile(parentId, fileName, base64Data, contentType);
        } else {
            uploadedFile = appendToFile(fileId, base64Data);
        }
        return uploadedFile.Id;
    }

    public static Attachment saveTheFile(String parentId, String fileName, String base64Data, String contentType) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

        Attachment oAttachment = new Attachment();
        oAttachment.parentId = parentId;

        oAttachment.Body = EncodingUtil.base64Decode(base64Data);
        oAttachment.Name = fileName;
        oAttachment.ContentType = contentType;


        insert oAttachment;
        return oAttachment;
    }

    public static Attachment appendToFile(String fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

        Attachment a = [SELECT Id, Body, Name, ContentType FROM Attachment WHERE Id =: fileId];

        String existingBody = EncodingUtil.base64Encode(a.Body);

        a.Body = EncodingUtil.base64Decode(existingBody + base64Data);

        update a;
        return a;
    }
    @AuraEnabled
    public static Attachment deleteAttachment(String fileName, String parentId){
        Attachment dAttachment;
        List<Attachment> attachmentList = [SELECT Id, Name, ParentId FROM Attachment WHERE ParentId = :parentId AND Name = :fileName];
        if (!Peak_Utils.isNullOrEmpty(attachmentList)) {
            dAttachment = attachmentList[0];
        }
        delete dAttachment;
        return dAttachment;
    }
}