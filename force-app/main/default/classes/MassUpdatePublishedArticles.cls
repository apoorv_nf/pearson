public class MassUpdatePublishedArticles implements Database.Batchable<SObject>, Database.stateful {
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        List<Id> newDraftArticleIds = new List<Id>();
        
        List<Customer_Support__kav> publishedArticles = getArticlesToBeProcessed();
        if(publishedArticles != null){
            
            for(Customer_Support__kav publishedArticle : publishedArticles){
                String articleId = KbManagement.PublishingService.editOnlineArticle(publishedArticle.KnowledgeArticleId, false);
                newDraftArticleIds.add(articleId);
            }
            
            publishedArticles.clear();
        }
        
        return Database.getQueryLocator([SELECT Id, KnowledgeArticleId, RecordTypeId FROM Customer_Support__kav where PublishStatus = 'Draft' 
                                         and Id IN :newDraftArticleIds]);
    }
    
    public void execute(Database.BatchableContext bc, List<Customer_Support__kav> recordsToProcess){
        Id articleRecordTypeId = Customer_Support__kav.sObjectType.getDescribe().getRecordTypeInfosByName().get('Customer Support').getRecordTypeId();
        
        for(Customer_Support__kav draftArticle : recordsToProcess){
            draftArticle.RecordTypeId = articleRecordTypeId;
        }
        update recordsToProcess;
        
        for(Customer_Support__kav draftArticle : recordsToProcess){
            KbManagement.PublishingService.publishArticle(draftArticle.KnowledgeArticleId, false);
        }
        
        recordsToProcess.clear();
    }
    
    public void finish(Database.BatchableContext bc){
        Id articleRecordTypeId = Customer_Support__kav.sObjectType.getDescribe().getRecordTypeInfosByName().get('Customer Support').getRecordTypeId();
        
        List<String> articleDraftEscapeIds = System.Label.CustomerSupportDraftEscapeIds.split('\r\n');
                
        List<Customer_Support__kav> draftArticles = [SELECT Id, KnowledgeArticleId, RecordTypeId FROM Customer_Support__kav where PublishStatus = 'Draft' 
                                                     and RecordTypeId = '' and KnowledgeArticleId NOT IN :articleDraftEscapeIds limit 200];
        if(draftArticles != null && !draftArticles.isEmpty() && draftArticles.size() > 0){
            for(Customer_Support__kav dA : draftArticles){
                dA.RecordTypeId = articleRecordTypeId;
            }
            
            update draftArticles;
            
            draftArticles.clear();
        }
        
        List<Customer_Support__kav> publishedArticles = getArticlesToBeProcessed();
        if(publishedArticles != null && !publishedArticles.isEmpty() && publishedArticles.size() > 0 && !Test.isRunningTest()){
            Database.executeBatch(new MassUpdatePublishedArticles(), 200);
        }else{
        }
    }
    
    private List<Customer_Support__kav> getArticlesToBeProcessed(){
        List<Id> existingDraftArticleIds = new List<Id>();
        
        List<Customer_Support__kav> kaIds = [SELECT KnowledgeArticleId FROM Customer_Support__kav where PublishStatus = 'Draft'];
        for(Customer_Support__kav kaId : kaIds){
            existingDraftArticleIds.add(kaId.KnowledgeArticleId);
        }
        if(kaIds != null){
            kaIds.clear();
        }
        List<String> articleEscapeIds = System.Label.CustomerSupportEscapeIds.split('\r\n');
        
        List<Customer_Support__kav> publishedArticles = new List<Customer_Support__kav>();
        if(Test.isRunningTest()){
            publishedArticles = [SELECT Id, KnowledgeArticleId, RecordTypeId FROM Customer_Support__kav where PublishStatus = 'Online' 
                                 and KnowledgeArticleId NOT IN :existingDraftArticleIds and KnowledgeArticleId NOT IN :articleEscapeIds limit 99];
        }else {
            publishedArticles = [SELECT Id, KnowledgeArticleId, RecordTypeId FROM Customer_Support__kav where PublishStatus = 'Online' and RecordTypeId = '' 
                                 and KnowledgeArticleId NOT IN :existingDraftArticleIds and KnowledgeArticleId NOT IN :articleEscapeIds limit 99];
        }
        
        existingDraftArticleIds.clear();
        articleEscapeIds.clear();
        
        return publishedArticles;
    }
}