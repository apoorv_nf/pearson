/*******************************************************************************************************************
* Apex Class Name  : PS_UniversityCourseOperationsTest
* Version          : 1.0 
* Created Date     : 23/10/2015
* Function         : Test Class of the PS_UniversityCourseOperations class 
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
* Leonard Victor            23/10/2015                Intital version of Test class for primary selling account mapping
*******************************************************************************************************************/
@isTest
public  class PS_UniversityCourseOperationsTest 
{

    @testSetup static void setup() {
        List<Account> lstAccount = TestDataFactory.createAccount(15,'Organization');
        insert lstAccount;
        lstAccount[0].Primary_Selling_Account__c = lstAccount[1].id;
        lstAccount[1].Primary_Selling_Account__c = lstAccount[6].id;
        lstAccount[2].Primary_Selling_Account__c = lstAccount[7].id;
        lstAccount[3].Primary_Selling_Account__c = lstAccount[8].id;
        lstAccount[4].Primary_Selling_Account__c = lstAccount[9].id;
        update lstAccount;
    }

     static testMethod  void coursePrimaryCheck() {
         Test.startTest();
             List<Account> accLst = [Select id,Primary_Selling_Account__r.name from Account where name like 'Test Account%'];
             List<UniversityCourse__c> courseLst = TestDataFactory.insertCourse();
                    courseLst[0].Account__c = accLst[0].id;
                    courseLst[1].Account__c = accLst[1].id;
                    courseLst[2].Account__c = accLst[2].id;


             insert courseLst;
             


             List<UniversityCourse__c> courseLstPrimary = [Select id,Primary_Selling_AccountText__c from UniversityCourse__c where id in: courseLst]; 
             System.assertEquals(accLst[0].Primary_Selling_Account__r.name,courseLstPrimary[0].Primary_Selling_AccountText__c);
             //Update Condition

             courseLst[0].Account__c = accLst[3].id;
             update courseLst[0];
             List<UniversityCourse__c> courseLstUpdPrimary = [Select id,Primary_Selling_AccountText__c from UniversityCourse__c where id=: courseLst[0].id]; 
             System.assertEquals(accLst[3].Primary_Selling_Account__r.name,courseLstUpdPrimary[0].Primary_Selling_AccountText__c);
             accLst[3].Primary_Selling_Account__c = accLst[13].id;
             update accLst[3];

         
             delete courseLst[2];
             undelete courseLst[2];

         Test.stopTest();
     }
}