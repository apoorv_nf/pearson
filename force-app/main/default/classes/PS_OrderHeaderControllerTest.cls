/************************************************************
@Author     : Accenture IDC 
@Description: Controller to 'OrderHeader' page - contains logic for Orders
@Date       : 04/01/2015
@Version    : 1.0
**************************************************************/
/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            OrderHeaderController.cls 
Description:     Hold order header Logic.
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
12/15/2015         2.1        Rony Joseph         Updated for Changes related in PS_OverideOrderNewButton Class. 
3/09/2016          2.2        Raushan Kumar       Updated for changes related to R3.2 Release
*/
@isTest
public class PS_OrderHeaderControllerTest {  
    public static testmethod void processorder(){
        User u1 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = UserInfo.getProfileId(), Sample_Approval__c= false,Order_Approver__c= UserInfo.getUserId(),
                           TimeZoneSidKey='America/Los_Angeles', License_Pools__c='Enterprise',UserName='test1234'+Math.random()+'@pearson.com', Geography__c = 'Growth',Market__c = 'US',Line_of_Business__c = 'Higher Ed');
        insert u1;
        list<PermissionSetAssignment> psa = new list<PermissionSetAssignment>();
        list<string> permissionsetnames = new list<string>{'Pearson_Sample_Order_Approver','Pearson_Network_Sampler','Pearson_Backend_Order_Creation','Pearson_Front_End_Sampling'};
            list<permissionset> pertset = [select id from permissionset where name in :permissionsetnames]; 
        for(integer i=0;i< pertset.size();i++ )
        {        psa.add(new PermissionSetAssignment(PermissionSetId = pertset[i].id, AssigneeId=u1.id));  }
        insert psa;
        
        system.runas(u1) {
            Bypass_Settings__c settings = new Bypass_Settings__c();
          settings.Disable_Triggers__c = true;
          settings.SetupOwnerId = u1.id;
          insert settings; 
            order sampleorder = TestDataFactory.returnmodifiedorder(); 
            Test.starttest(); 
            PageReference testPage = new pagereference('/apex/OrderHeaderController');
            testPage.getParameters().put('oppid',sampleorder.Opportunityid);
            testPage.getParameters().put('orderid',sampleorder.id); 
            Test.setCurrentPage(testPage);
            OrderHeaderController orderheader = new OrderHeaderController();
            orderheader.fromOnclick = false;
            orderheader.conid=orderheader.ordercontactlist[0].con.id;
            orderheader.ordercontactlist[0].selectedValue= 'Other';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Mailing';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Account';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Custom';
            orderheader.setselectedaddress1(); 
            orderheader.ordercontactlist[0].selectedValue= 'Opportunity';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].address= 'Contact Other';
            orderheader.initiateaddressselction();
            orderheader.ordercontactlist[0].address= 'Contact Mailing';
            orderheader.initiateaddressselction();
            orderheader.ordercontactlist[0].address= 'Account';
            orderheader.initiateaddressselction();
            orderheader.ordercontactlist[0].address= 'Custom';
            orderheader.initiateaddressselction(); 
            orderheader.ordercontactlist[0].address= 'Opportunity';
            orderheader.initiateaddressselction(); 
            orderheader.conid = sampleorder.ShipToContactid;
            orderheader.ordercontactlisttotal[0].futureshipdate = system.today();
            orderheader.ordercontactlisttotal[0].canceldate = system.today()-1;
            orderheader.ordercontactlisttotal[0].shippinginstructions='Drop at door'; 
            orderheader.sortingcontacts();
            orderheader.contactidlist();
            orderheader.getStates();
            orderheader.shippingmethod = orderheader.getoptions();
            List<SelectOption> getoptions = orderheader.getoptions();
            orderheader.conid = sampleorder.ShipToContactid;
            list<orderitem> item = [select Shipped_Product__c from orderitem where orderid = :sampleorder.id];
            orderheader.currentProduct = item[0].Shipped_Product__c;
            orderheader.hidePopup();
            orderheader.duplicateProcess();
            orderheader.createorder = true;
            orderheader.updatedtotalordercontactlist();
            orderheader.updatetotalordercontactlist();
            orderheader.hideselectedaddress();
            orderheader.Next();
            orderheader.Previous();
            orderheader.removeconid = sampleorder.ShipToContactid;
            orderheader.QueryString='';
            orderheader.insertcontact = true;
            orderheader.OpportunityContactRolelookup.role='Non-Participant';
            orderheader.contactlookup.ShipToContactid =sampleorder.ShipToContactid;
            orderheader.OpportunityContactRolelookup.role='Other';
            orderheader.insertopportunitycontact();
            contact c = new contact();
            Map<id,String> contactmap=new map<id,String>();
            contactmap.put(sampleorder.ShipToContactid,'Other');
            //orderheader.warehouseinstruction='Drop at doorsteps';
            orderheader.initiateapproval(); 
            OrderHeaderController.DuplicateList test1= new OrderHeaderController.DuplicateList('Raushan',sampleorder.ShipToContactid,item[0].Shipped_Product__c);   
            orderheader.removeprodid=item[0].Shipped_Product__c;
            orderheader.removeproduct();
            orderheader.cancelPopup();
            orderheader.OffsetSize =3;
            orderheader.getlistofcontactaddress();
            Test.stopTest();
            
        }
        
    } 
    
 
    @isTest
    public static void processorder2(){
        
        User u1 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = UserInfo.getProfileId(), Sample_Approval__c= true,Order_Approver__c= UserInfo.getUserId(),
                           TimeZoneSidKey='America/Los_Angeles',License_Pools__c='Enterprise', UserName='test1234'+Math.random()+'@pearson.com', Geography__c = 'Growth',Market__c = 'US',Line_of_Business__c = 'Higher Ed' );
        insert u1;
        list<PermissionSetAssignment> psa = new list<PermissionSetAssignment>();
        list<string> permissionsetnames = new list<string>{'Pearson_Sample_Order_Approver','Pearson_Network_Sampler','Pearson_Backend_Order_Creation','Pearson_Front_End_Sampling'};
            list<permissionset> pertset = [select id from permissionset where name in :permissionsetnames]; 
        for(integer i=0;i< pertset.size();i++ )
        {        psa.add(new PermissionSetAssignment(PermissionSetId = pertset[i].id, AssigneeId=u1.id));  }
        insert psa; 
        system.runas(u1) {
               Bypass_Settings__c settings = new Bypass_Settings__c();
          settings.Disable_Triggers__c = true;
          settings.SetupOwnerId = u1.id;
          insert settings; 
            order sampleorder = TestDataFactory.returnmodifiedorder(); 
            Test.starttest(); 
            Account AccountRec = new account(Name='Test Account1', Phone='+9100000' ,IsCreatedFromLead__c = True, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037');
            insert AccountRec;
            List<contact> conlist=new list<contact>();
            for(Integer i=0;i<2;i++){
                Contact contactRecord = new Contact(FirstName='TC1Firstname'+ i, LastName='TCL1astname' + i,
                                                    AccountId=AccountRec.Id ,Salutation='MR.', Email='samplee1mailaddress1112' + i + '@email.com', 
                                                    Phone='11122233355555' + i, Preferred_Address__c = 'Mailing Address', MailingCountry = 'United Kingdom', 
                                                    MailingStreet = '1 Street', MailingPostalCode = 'NE27 0QQ', MailingCity  = 'City');     
                conlist.add(contactRecord);}
            insert conlist;
            
            list<orderitem> itemlist =  new list<orderitem>([select Shipped_Product__c from orderitem where orderid = :sampleorder.id]);
            
            PageReference testPage = new pagereference('/apex/OrderHeaderController');
            testPage.getParameters().put('oppid',sampleorder.Opportunityid);
            testPage.getParameters().put('orderid',sampleorder.id); 
            contact con = [SELECT Id, name,Preferred_Address__c,accountid,Role__c,email,Do_Not_Send_Samples__c,
                           MailingCity,MailingState,MailingCountry,MailingPostalCode,MailingStreet,
                           OtherStreet,OtherCity,OtherState,OtherCountry,OtherPostalCode,
                           account.shippingstreet,account.shippingcity,account.shippingstate,account.shippingcountry,account.shippingpostalcode
                           FROM Contact where id = :sampleorder.ShipToContactid limit 1 ];
            Test.setCurrentPage(testPage);
            OrderHeaderController orderheader = new OrderHeaderController(); 
            orderheader.insertcontact = false;
            contact con1 = [select id,accountid,name,lastname,Preferred_Address__c,Role__c,email,Do_Not_Send_Samples__c,MailingCity,MailingState,MailingCountry,MailingPostalCode,MailingStreet,
                            OtherStreet,OtherCity,OtherState,OtherCountry,OtherPostalCode,account.shippingstreet,account.shippingcity,
                            account.shippingstate,account.shippingcountry,account.shippingpostalcode,account.Address_1_PO_Box__c,Address_1_PO_Box__c,Address_2_PO_Box__c 
                            FROM Contact where id = :sampleorder.ShipToContactid limit 1 ];
          
            orderheader.contactlookup.ShipToContactid =conlist[0].id;
            orderheader.OpportunityContactRolelookup.role='None'; 
            orderheader.insertopportunitycontact();
            orderheader.fromOnclick = false;
            orderheader.conid=orderheader.ordercontactlist[0].con.id;
            orderheader.ordercontactlist[0].selectedValue= 'Custom';
            orderheader.setselectedaddress1(); 
            orderheader.ordercontactlist[0].selectedValue= 'Other';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Mailing';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Account';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Opportunity Address';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Primary Account Address';
            orderheader.setselectedaddress1();
            orderheader.AddressType='Merge Contact Other Address';
            orderheader.ordercontactlist[0].selectedValue= 'Custom';
            orderheader.setselectedaddress1();
            orderheader.AddressType='Merge Contact Mailing Address';
            orderheader.ordercontactlist[0].selectedValue= 'Custom';
            orderheader.setselectedaddress1();
            
            
            List<SelectOption> getoptions = orderheader.getoptions();
            orderheader.conid = sampleorder.ShipToContactid;
            orderheader.ordercontactlisttotal[0].futureshipdate = system.today();
            orderheader.ordercontactlisttotal[0].canceldate = system.today()-1;
            orderheader.ordercontactlisttotal[0].shippinginstructions='Drop at door'; 
            orderheader.getStates();
            orderheader.duplicateProcess();
            orderheader.hidePopup();
            orderheader.cancelPopup();
            orderheader.initiateapproval(); 
		   orderheader.primaryErpLocationErrorPost();
            orderheader.updatedtotalordercontactlist();
            orderheader.ordercontactlisttotal[0].futureshipdate = system.today()+1;
            orderheader.ordercontactlisttotal[0].canceldate = system.today()-2;
            orderheader.ordercontactlisttotal[0].shippinginstructions='Drop at doorstep'; 
            orderheader.updatetotalordercontactlist();
            orderheader.hideselectedaddress();
            orderheader.ordercontactlisttotal[0].Address ='Custom'; 
            orderheader.hideselectedaddress();
            orderheader.Next();
            
            
            orderheader.Previous();
            orderheader.removeconid = sampleorder.ShipToContactid; 
            con.Preferred_Address__c= System.Label.ContactMailing;
            con.MailingCountry= 'India';
            con.MailingState='Kerala';
            con.MailingCity='Cochin';
            con.Mailingstreet='Korpakkam';
            con.Mailingpostalcode='560025';
            update con;
            OrderHeaderController.Addressfields ad1 = new OrderHeaderController.Addressfields(con);
            con.Preferred_Address__c= System.Label.Contact_Other;
            update con;
            con.Preferred_Address__c= 'None';
            update con;
          
            String contactrolestring;
            Map<id,String> contactmap=new map<id,String>();
            Map<id,orderheadercontroller.ordercontacts> mapordercon =new Map<id,orderheadercontroller.ordercontacts>();
            
            map<id,account> contactaccountmap = new map<id,account>();
            orderheadercontroller.entereddates dates1 = new orderheadercontroller.entereddates(null,null,'');
            map<id,orderheadercontroller.entereddates> datemap = new  map<id,orderheadercontroller.entereddates>();
            datemap.put(con.id,dates1);
            orderheadercontroller.ordercontacts orc = new orderheadercontroller.Ordercontacts (con,sampleorder,sampleorder.Opportunityid, contactmap,datemap,contactaccountmap);
            con.Preferred_Address__c= System.Label.Contact_Other;
            update con;
            orderheadercontroller.Ordercontacts orc1 = new orderheadercontroller.Ordercontacts (con,sampleorder,sampleorder.Opportunityid, contactmap,datemap,contactaccountmap);       
            con.Preferred_Address__c='';
            update con;
            mapordercon.put(con.id,orc1);
            mapordercon.get(con.id).ischecked=False;
            list<orderitem> item =[select id,Shipped_Product__c from orderitem where orderid = :sampleorder.id ]; 
            orderheader.removeprodid=itemlist[0].Shipped_Product__c;
            orderheader.deletedProduct.add(orderheader.removeprodid);
            //orderheader.deletedProduct.add(con.id);
            orderheader.indexOfListToSetQty=0;
            orderheader.OrderLineItemcoll[orderheader.indexOfListToSetQty].Quantity=0;
            orderheader.valueOfQty=1;
            orderheader.setQuantity();        
            orderheader.setShippingMethod();
            orderheader.setshippinginstruction();
            orderheader.setcanceldate();
            orderheader.setfutureshipdate();
            orderheader.refereshverifybutton();
            orderheader.cancelAction(); 
            stdOrderTriggerHandler.createListMap([select id,status from order]);
            orderheader.removeproduct();       
            
            Test.stopTest();
        }
    }  
    public static testmethod void overridebutton()
    {
        //TC 01/02/2016 - commenting out because its breaking and adds no functional test value.
        /* 
Test.starttest(); 
Account AccountRec = new account(Name='Test Account1', Phone='+9100000' ,IsCreatedFromLead__c = True, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037');
insert AccountRec;
Contact contactRecord = new Contact(FirstName='TC1Firstname', LastName='TCL1astname',
AccountId=AccountRec.Id ,Salutation='MR.', Email='samplee1mailaddress1112' + '@email.com', 
Phone='11122233355555',MailingCountry= 'India',MailingState='Kerala',MailingCity='Cochin',Mailingstreet='Korpakkam',Mailingpostalcode='560025',OtherCountry= 'India',otherState='Kerala',otherCity='Cochin',otherstreet='Korpakkam',otherpostalcode='560025' );     
insert contactRecord; 
PageReference testPage = new pagereference('/apex/OrderHeader');
testPage.getParameters().put('RecordType','AAA');
testPage.getParameters().put('cancelURL','AAA');
testPage.getParameters().put('ent','AAA');
testPage.getParameters().put('_CONFIRMATIONTOKEN','AAA');
testPage.getParameters().put('save_new_url','AAA');
testPage.getParameters().put('aid','AAA');
testPage.getParameters().put('cid',contactRecord.id);
Test.setCurrentPage(testPage);
ContentVersion conv=new ContentVersion();
ApexPages.StandardController controller = new ApexPages.StandardController(conv);
PS_OverideOrderNewButton overrider = new PS_OverideOrderNewButton(controller);
//overrider.ContID = contactRecord.id;
overrider.OverideOrderNewButton();
contactRecord.Preferred_Address__c='Mailing Address';
update  contactRecord;
overrider.OverideOrderNewButton();
contactRecord.Preferred_Address__c='Other Address';
update  contactRecord;
overrider.OverideOrderNewButton();
Test.stoptest(); 
*/
    }

    public static testmethod void processorder3(){
        
        User u1 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = UserInfo.getProfileId(), Sample_Approval__c= true,Order_Approver__c= UserInfo.getUserId(),
                           TimeZoneSidKey='America/Los_Angeles',License_Pools__c='Enterprise', UserName='test1234'+Math.random()+'@pearson.com', Geography__c = 'Growth',Market__c = 'CA',Line_of_Business__c = 'Higher Ed');
        insert u1;
        list<PermissionSetAssignment> psa = new list<PermissionSetAssignment>();
        list<string> permissionsetnames = new list<string>{'Pearson_Sample_Order_Approver','Pearson_Network_Sampler','Pearson_Backend_Order_Creation','Pearson_Front_End_Sampling'};
            list<permissionset> pertset = [select id from permissionset where name in :permissionsetnames]; 
        for(integer i=0;i< pertset.size();i++ )
        {        psa.add(new PermissionSetAssignment(PermissionSetId = pertset[i].id, AssigneeId=u1.id));  }
        insert psa; 
        system.runas(u1) {
             Bypass_Settings__c settings = new Bypass_Settings__c();
          settings.Disable_Triggers__c = true;
          settings.SetupOwnerId = u1.id;
          insert settings;
            order sampleorder = TestDataFactory.returnmodifiedorder(); 
            Test.starttest(); 
            Account AccountRec = new account(Name='Test Account1', Phone='+9100000' ,IsCreatedFromLead__c = True, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037');
            insert AccountRec;
            List<contact> conlist=new list<contact>();
            for(Integer i=0;i<2;i++){
                Contact contactRecord = new Contact(FirstName='TC1Firstname'+ i, LastName='TCL1astname' + i,
                                                    AccountId=AccountRec.Id ,Salutation='MR.', Email='samplee1mailaddress1112' + i + '@email.com', 
                                                    Phone='11122233355555' + i, Preferred_Address__c = 'Mailing Address', MailingCountry = 'United Kingdom', 
                                                    MailingStreet = '1 Street', MailingPostalCode = 'NE27 0QQ', MailingCity  = 'City');     
                conlist.add(contactRecord);}
            insert conlist;
            
            list<orderitem> itemlist =  new list<orderitem>([select Shipped_Product__c from orderitem where orderid = :sampleorder.id]);
            
            PageReference testPage = new pagereference('/apex/OrderHeaderController');
            testPage.getParameters().put('oppid',sampleorder.Opportunityid);
            testPage.getParameters().put('orderid',sampleorder.id); 
            contact con = [SELECT Id, name,Preferred_Address__c,accountid,Role__c,email,Do_Not_Send_Samples__c,
                           MailingCity,MailingState,MailingCountry,MailingPostalCode,MailingStreet,
                           OtherStreet,OtherCity,OtherState,OtherCountry,OtherPostalCode,
                           account.shippingstreet,account.shippingcity,account.shippingstate,account.shippingcountry,account.shippingpostalcode
                           FROM Contact where id = :sampleorder.ShipToContactid limit 1 ];
            Test.setCurrentPage(testPage);
            OrderHeaderController orderheader = new OrderHeaderController(); 
            orderheader.insertcontact = true;
             
            contact con1 = [select id,accountid,name,lastname,Preferred_Address__c,Role__c,email,Do_Not_Send_Samples__c,MailingCity,MailingState,MailingCountry,MailingPostalCode,MailingStreet,
                            OtherStreet,OtherCity,OtherState,OtherCountry,OtherPostalCode,account.shippingstreet,account.shippingcity,
                            account.shippingstate,account.shippingcountry,account.shippingpostalcode,account.Address_1_PO_Box__c,Address_1_PO_Box__c,Address_2_PO_Box__c 
                            FROM Contact where id = :sampleorder.ShipToContactid limit 1 ];
            orderheader.contactlookup.ShipToContactid =conlist[0].id;
            orderheader.OpportunityContactRolelookup.role='None'; 
            orderheader.insertopportunitycontact();
            orderheader.fromOnclick = false;
            orderheader.conid=orderheader.ordercontactlist[0].con.id;
             orderheader.ordercontactlist[0].selectedValue= 'Custom';
            orderheader.setselectedaddress1(); 
            orderheader.ordercontactlist[0].selectedValue= 'Other';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Mailing';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Account';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Opportunity Address';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Primary Account Address';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Custom';
            orderheader.ordercontactlist[0].addressline1 ='test1';
            orderheader.ordercontactlist[0].addressline2 ='test2';
            orderheader.ordercontactlist[0].addressline3 ='test3';
            orderheader.ordercontactlist[0].addressline4 ='test4';
            orderheader.AddressType = 'Save as Contact Other Address';
            orderheader.ordercontactlist[0].selectedValue= 'Custom';
            orderheader.ordercontactlist[0].addressline1 ='test1';
            orderheader.ordercontactlist[0].addressline2 ='test2';
            orderheader.ordercontactlist[0].addressline3 ='test3';
            orderheader.ordercontactlist[0].addressline4 ='test4';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Custom';
            orderheader.AddressType = 'Save as Contact Mailing Address';
            orderheader.setselectedaddress1();
            orderheader.AddressType='Merge Contact Other Address';
            orderheader.ordercontactlist[0].selectedValue= 'Custom';
            orderheader.setselectedaddress1();
            orderheader.AddressType='Merge Contact Mailing Address';
            orderheader.ordercontactlist[0].selectedValue= 'Custom';
            orderheader.setselectedaddress1();
            orderheader.ordercontactlist[0].selectedValue= 'Account';
            orderheader.setselectedaddress1();
            
            List<SelectOption> getoptions = orderheader.getoptions();
            orderheader.conid = sampleorder.ShipToContactid;
            orderheader.ordercontactlisttotal[0].futureshipdate = system.today();
            orderheader.ordercontactlisttotal[0].canceldate = system.today()-1;
            orderheader.ordercontactlisttotal[0].shippinginstructions='Drop at door'; 
            orderheader.getStates();
            orderheader.duplicateProcess();
            orderheader.hidePopup();
            orderheader.cancelPopup();
            orderheader.initiateapproval(); 
           
            orderheader.updatedtotalordercontactlist();
            orderheader.ordercontactlisttotal[0].futureshipdate = system.today()+1;
            orderheader.ordercontactlisttotal[0].canceldate = system.today()-2;
            orderheader.ordercontactlisttotal[0].shippinginstructions='Drop at doorstep'; 
            orderheader.updatetotalordercontactlist();
            orderheader.hideselectedaddress();
            orderheader.ordercontactlisttotal[0].Address ='Custom'; 
            orderheader.hideselectedaddress();
            orderheader.Next();
            
            
            orderheader.Previous();
            orderheader.removeconid = sampleorder.ShipToContactid; 
            con.Preferred_Address__c= System.Label.ContactMailing;
            con.MailingCountry= 'India';
            con.MailingState='Kerala';
            con.MailingCity='Cochin';
            con.Mailingstreet='Korpakkam';
            con.Mailingpostalcode='600042';
            update con;
            OrderHeaderController.Addressfields ad1 = new OrderHeaderController.Addressfields(con);
            con.Preferred_Address__c= System.Label.Contact_Other;
            update con;
            OrderHeaderController.Addressfields ad2 = new OrderHeaderController.Addressfields(con);
            con.Preferred_Address__c= System.Label.ContactMailing;
            update con;
            String contactrolestring;
            Map<id,String> contactmap=new map<id,String>();
            Map<id,orderheadercontroller.ordercontacts> mapordercon =new Map<id,orderheadercontroller.ordercontacts>();
            
            map<id,account> contactaccountmap = new map<id,account>();
            orderheadercontroller.entereddates dates1 = new orderheadercontroller.entereddates(null,null,'');
            map<id,orderheadercontroller.entereddates> datemap = new  map<id,orderheadercontroller.entereddates>();
            datemap.put(con.id,dates1);
            orderheadercontroller.ordercontacts orc = new orderheadercontroller.Ordercontacts (con,sampleorder,sampleorder.Opportunityid, contactmap,datemap,contactaccountmap);
            orc.address='Custom';
            orderheader.ordercontactlist.add(orc);            
            con.Preferred_Address__c= System.Label.Contact_Other;
            update con;
            orderheadercontroller.Ordercontacts orc1 = new orderheadercontroller.Ordercontacts (con,sampleorder,sampleorder.Opportunityid, contactmap,datemap,contactaccountmap);
            orc1.selectedValue='Primary Account Address';
            orc1.selected='Account';
            orderheader.ordercontactlist.add(orc1);
            con.Preferred_Address__c='';
            update con;
            orderheadercontroller.Ordercontacts orc2 = new orderheadercontroller.Ordercontacts (con,sampleorder,sampleorder.Opportunityid, contactmap,datemap,contactaccountmap);
            orc2.email='';
            orc2.address='Contact Mailing';
            orc2.selected='Custom';
            orc2.line1='line1';
            orc2.line2='line2';
            orc2.line3='line3';
            orc2.line4='line4';
            orc2.street='street';
            orc2.textfieldrequired = false;
            orc2.disabelaccountbtn= false;
            orderheader.ordercontactlist.add(orc2);
            con.Preferred_Address__c='';
            orderheader.targetContacts.put(con.id,con);
            //orderheader.initiateaddressselction();            
            mapordercon.put(con.id,orc1);
            mapordercon.get(con.id).ischecked=true;
            list<orderitem> item =[select id,Shipped_Product__c from orderitem where orderid = :sampleorder.id ]; 
            orderheader.removeprodid=itemlist[0].Shipped_Product__c;
            orderheader.deletedProduct.add(orderheader.removeprodid);
            //orderheader.deletedProduct.add(con.id);
            orderheader.indexOfListToSetQty=0;
            orderheader.OrderLineItemcoll[orderheader.indexOfListToSetQty].Quantity=0;
            orderheader.valueOfQty=1;
            orderheader.setQuantity();        
            orderheader.setShippingMethod();
            orderheader.setshippinginstruction();
            orderheader.setcanceldate();
            orderheader.setfutureshipdate();
            orderheader.refereshverifybutton();
            orderheader.cancelAction(); 
            stdOrderTriggerHandler.createListMap([select id,status from order]);
            orderheader.removeproduct();
            orderheader.getAddressOptions();
            con.Preferred_Address__c= '';
            update con;
            List<Contact> contactsList = new List<Contact>();
            contactsList.add(con);
            OrderHeaderController.Addressfields ad12 = new OrderHeaderController.Addressfields(con); 
            ad12.line1='line1';
            ad12.line2='line2';
            ad12.line3='line3';
            ad12.line4='line4';
            ad12.street='street';
            Database.SaveResult[] results = Database.Update(contactsList, false);
            orderheader.inserterrorlog(results,'test');
            orderheader.ordercontactlist.add(orc1);
            orderheader.ordercontactlist.add(orc2);
            orderheader.targetContacts.put(con1.id,con1);
            orderheader.initiateaddressselction();
            orderheader.ordercontactlist[0].address= 'Contact Mailing';
            orderheader.initiateaddressselction();
            orderheader.ordercontactlist[1].address= 'Contact Other';
            orderheader.initiateaddressselction();
            orderheader.ordercontactlist[0].address= 'Account';
            orderheader.initiateaddressselction();
            orderheader.ordercontactlist[0].address= 'Custom';
            orderheader.initiateaddressselction(); 
            orderheader.ordercontactlist[0].address= 'Opportunity';
            orderheader.initiateaddressselction();
            orderheader.initiateapproval();
             OrderHeaderController.DuplicateList test1= new OrderHeaderController.DuplicateList('Ganesh',sampleorder.ShipToContactid,item[0].Shipped_Product__c);   
            orderheader.removeprodid=item[0].Shipped_Product__c;
            contact c = new contact();
            Map<id,String> contactmap1=new map<id,String>();
            contactmap1.put(sampleorder.ShipToContactid,'Other');
                 PageReference testPage1 = new pagereference('/apex/OrderHeaderController');
                 testPage1.getParameters().put('oppid',sampleorder.Opportunityid);
                 testPage1.getParameters().put('orderid',sampleorder.id); 
                 Test.setCurrentPage(testPage1); 
                 orderheader.RelatedAccount = AccountRec;
            orderheader.initiateapproval();
           
           orderheader.primaryErpLocationErrorPost();
            orderheader.getOrderLineItemcoll();
            orderheader.initiateapproval();
            
            Test.stopTest();
        }
    } 
    
     
              
      
    
    
}