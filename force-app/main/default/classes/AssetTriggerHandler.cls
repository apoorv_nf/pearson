/*
KP:10-28-2015: Added code to handle mass updates of Primary flag from Update Contacts page at Course.
KP:10-29-2015: To handle duplicate primary contacts at the time of update.
*/
public Class AssetTriggerHandler{
   //KP Added code for PIU validation for D2L opportunity
    public static void BeforeEventValidation(Map<id,Asset> inAsset,Map<id,Asset> inOldAsset,String Operation){
        List<PS_OpportunityValidationInterface> validations =PS_OpportunityValidationFactory.OpptyAssetValidations(inAsset,inOldAsset, operation);
        Map<String,List<String>> exceptions = new Map<String,List<String>>();

        for(PS_OpportunityValidationInterface validation: validations)
        {
            validation.opptyvalidateUpdate(exceptions);
        }

        if( ! exceptions.isEmpty())
        {
            PS_Util.addErrors_1(inAsset,exceptions);

            return;
        }         
    }  
    
    public void primaryContactv2(List<Asset> asList,Map<Id,Asset> asMap){
        set<id> conId = new set<id>();
        set<id> courseId = new set<id>();
        Map<Id,List<Asset>> newPrimaryPIU=new Map<Id,List<Asset>>();
        for(Asset ass: asList){
          if(ass.contactid != null && ass.course__c != null) {
              conId.add(ass.contactid);
              courseId.add(ass.course__c);
              if(ass.primary__c == true){
                  if (!newPrimaryPIU.containsKey(ass.contactid))
                      newPrimaryPIU.put(ass.contactid, new List<Asset>{ass});
                  else{
                      newPrimaryPIU.get(ass.contactid).add(ass);
                      //06/22/2017 (Rakshik): Commenting this as per instructions from Rob Power and will be opening a technical debt CR for the CI team since this logic is  incorrect.
                      //ass.addError('You have selected multiple Products-in-Use to be the primary product for a contact on this course. Please ensure that each contact has only one Product-in-Use marked as their primary material.');
                      return;
                  }
              }              
          }

        }
        List<Asset> asExistLists = [select id,name,Primary__c,ContactId from Asset where ContactId in : conId and 
                                    primary__c=true and status__c='Active' and product2id != '' and course__c in :courseId order by contactId]; 
     
        if(!asExistLists.isEmpty())  {
            Map<Id,List<Asset>> conPrimaryPIU=new Map<Id,List<Asset>>();
            Id sconId=null;
            for(Asset piumap:asExistLists){
                if (sconId != piumap.ContactId)
                    sconId=piumap.ContactId;
                if (!conPrimaryPIU.containsKey(sconId))
                        conPrimaryPIU.put(sconId, new List<Asset>{piumap});
                else
                        conPrimaryPIU.get(sconId).add(piumap);

            }//end for
            
            List<Asset> ass_toupdate=new List<Asset>();
            Set<Id> assIdtoup=new Set<Id>();
            for(Asset piu: asList){
                if (piu.contactId != null && piu.primary__c==true){
                    if (conPrimaryPIU.containsKey(piu.contactId)){
                         for(Asset piuup:conPrimaryPIU.get(piu.contactId)){
                             //if (piuup.id != piu.id && !asMap.containsKey(piu.id)){
                             if (piuup.id != piu.id && !assIdtoup.contains(piuup.id)){
                                 piuup.primary__c=false;
                                 ass_toupdate.add(piuup);
                                 assIdtoup.add(piuup.id);
                             }
                         }//end map for
                    }
                }//end if
            }//end for
            system.debug('Asset to update size:'+ ass_toupdate.size());
            if (!ass_toupdate.isEmpty()){
                try{
                    update ass_toupdate;     
                }
                Catch(DMLException e){
                    throw(e);
                }
            }         

         }//end if
     }//end fn
     
    public void primaryContactv2in(Asset asList){       
        if(asList.contactid != null && asList.course__c != null) {
            List<Asset> asExistLists = [select id,name,Primary__c,ContactId from Asset where ContactId =:asList.contactid and 
                                    primary__c=true and status__c='Active' and product2id != '' and course__c=:asList.course__c];
        
        if   (!asExistLists.isEmpty())  {
            List<Asset> ass_toupdate=new List<Asset>();
            for(Asset asExist : asExistLists){
              if(asExist .id != asList.id){
                  asExist.primary__c=false;
                  ass_toupdate.add(asExist);                  
              }
            }
            if (!ass_toupdate.isEmpty())
                update ass_toupdate;
        }  
        }                              
    } 
}