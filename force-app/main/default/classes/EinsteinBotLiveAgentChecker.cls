/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 12/05/2018
 * Update: Updated logic to check agent availability for 
 *         both the channels (Web and SMS) - Apoorv (Neuraflash) - 27/07/2019
 */
 
global without sharing class EinsteinBotLiveAgentChecker {
    @InvocableMethod(label='Einstein Bot - Live Agent Available?')
    global static List<Boolean> checkLiveAgent(List<String> liveAgentSessionIds) 
    {
        try {
            String sessionId = liveAgentSessionIds[0];

            if(sessionId.length() == 18 
                && Id.valueOf(sessionId).getSObjectType() == Schema.MessagingSession.SObjectType){

                /*MessagingSession msgSession = [SELECT Id, MessagingEndUser.MessagingChannel.TargetQueue.DeveloperName
                                                    FROM MessagingSession WHERE Id =: sessionId];
                                                    
                if(msgSession != NULL && msgSession.Id != NULL){*/
                List<UserServicePresence> presences = [
                        SELECT Id
                            FROM UserServicePresence
                                WHERE ServicePresenceStatus.DeveloperName = 'Available_for_SMS'
                                    AND OwnerId IN (
                                                    SELECT UserOrGroupId
                                                        FROM GroupMember
                                                            WHERE Group.DeveloperName =: System.Label.PTS_SMS_Bot_Queue 
                                                    )
                                    AND IsCurrentState = true
                                    AND CreatedDate = LAST_N_DAYS:2
                                    LIMIT 1];
                    return new List<Boolean>{!presences.isEmpty()};
            } else {
                for(LiveChatTranscript transcript : [
                    SELECT LiveChatButton.Queue.DeveloperName
                    FROM LiveChatTranscript
                    WHERE ChatKey IN :liveAgentSessionIds]) {

                    List<UserServicePresence> presences = [
                        SELECT Id
                        FROM UserServicePresence
                        WHERE ServicePresenceStatus.DeveloperName = 'Available_for_Chat'
                        AND OwnerId IN (
                            SELECT UserOrGroupId
                            FROM GroupMember
                            WHERE Group.DeveloperName = :transcript.LiveChatButton.Queue.DeveloperName)
                        AND IsCurrentState = true
                        AND CreatedDate = LAST_N_DAYS:2
                    ];

                    return new List<Boolean>{!presences.isEmpty()};
                }   
            }
            return new List<Boolean>{false}; 
        } catch(Exception ex) {
            return new List<Boolean>{false};
        }
    }
}