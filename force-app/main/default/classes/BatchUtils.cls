/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            BatchUtils.Cls
   Description:     Class that holds common menthods used by Batch Classes
   Test Class:      BatchUtilsTest
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   14/09/2016         0.1        Accenture - Davi Borges                 Created
                                             
-----------------------------------------------------------------------------------------------------------------------------------------------------------
*/

public class BatchUtils
{
	/* HELPER */


   public static Date getBatchRunFromDate(Date inputDate, String className) {

	    if(inputDate != null) {
	        return inputDate;
	    } else {
	        // try and set to date to completetion date of last run of batch,
	        //if not, use Date.today();
	        Date output = getLastProcessedDate(className);

	        if(output == null) {
	            return  Date.today();
	        }

          return output;
	    }
    }

    private static Date getLastProcessedDate(String className) {

        List<AsyncApexJob> previousBatchRuns = [SELECT Id, ApexClass.Name, CompletedDate, CreatedDate 
                                                FROM AsyncApexJob 
                                                WHERE ApexClass.Name = :className 
                                                AND Status = 'Completed' 
                                                ORDER BY CreatedDate DESC LIMIT 1];

        if(!previousBatchRuns.isEmpty()) {
            return previousBatchRuns[0].CompletedDate.date();
        } else {
            return null;
        }

    }
}