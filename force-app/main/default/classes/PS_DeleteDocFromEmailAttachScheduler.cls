public class PS_DeleteDocFromEmailAttachScheduler implements Schedulable {
	List<Folder> lstFolder = new List<Folder>();
	public PS_DeleteDocFromEmailAttachScheduler() {
		lstFolder = [SELECT id FROM Folder WHERE NAME = 'Temporary Attachment Folder' limit 1];
	}
  public void execute(SchedulableContext sc)
    {
        PS_BatchDeleteDocFromEmailAttachments batchAttchDeletion = new  PS_BatchDeleteDocFromEmailAttachments(lstFolder.get(0).Id);
        database.executebatch(batchAttchDeletion,1000);
    }
  }