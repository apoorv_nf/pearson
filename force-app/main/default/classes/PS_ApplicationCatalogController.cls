/**
* Name : PS_ApplicationCatalogController 
* Description : This class holds the Logic to get the List of Links of all applications               
*/
public with sharing class PS_ApplicationCatalogController {
    
     AppCatalog__c CustSetting = AppCatalog__c.getValues('First');
    
     String CATALOG = CustSetting.Catalog__c;
     String APPLICATIONNAME = CustSetting.APPLICATIONNAME__c;
     String CLASSNAME = CustSetting.PS_ApplicationCatalogController__c;
     String METHODNAME = CustSetting.fetchApplicationcatalogUrls__c;
    
    /**
       @Method Name  : getApplicationcatalogUrls
       @description  : This is getter method of apps.The method returns all the applications visble on catalog page.
       @param        : NA
       @return       : List<Application__c>
    */
    public List<Application__c> getApps() {
   
        Integer urlLimit = 0;
        List<Application__c> listApps = new List<Application__c>();
        try { 
           
            urlLimit = Integer.valueOf(ACURLLimit__c.getValues(CATALOG).Limit__c);
            //Get Application urls which are visible in application catalog
            listApps = [select Name, URL__c, App_Label__c, App_Type__c
                        FROM Application__c WHERE Visible_On_application_Catalog__c = True
                        Order By App_Label__c LIMIT : urlLimit];
        }
        catch(Exception gExcp){
             ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,gEXCp.getMessage());
             ApexPages.addMessage(errMsg);
             //UTIL_LoggingService.logHandledException(gExcp, userinfo.getOrganizationId(), APPLICATIONNAME,
                                                      //CLASSNAME, METHODNAME, null, LoggingLevel.ERROR);
        }
        
        return listApps;
    }
}