@isTest
public class B2CAccountSyncTest{
    @testSetup static void methodName1()
    {
        List<User> listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        insert listWithUser;
               
        list<Account> accountdatalist = TestDataFactory.createAccount(2,PS_Constants.ACCOUNT_LEARNER_RECCORD);   
        Insert accountdatalist;        
              
        list<Contact> contactdatalist = TestDataFactory.createContacts(2);          
        contactdatalist[0].AccountId=accountdatalist[0].id;
        contactdatalist[1].AccountId=accountdatalist[1].id;        

        system.runas(listWithUser[0]){
        Test.startTest();
        Insert contactdatalist;        
        AccountContact__c accCon = new AccountContact__c();
        accCon.Contact__c = contactdatalist[0].id;
        accCon.Account__c = accountdatalist[0].id;
        accCon.Primary__c = false;
        accCon.Financially_Responsible__c = true;
        insert accCon;
        }
    } 
    //harika
   
    //harika
   
    static testMethod void  myUnitTest2(){
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        if(listWithUser != null && !listWithUser.isEmpty()){
            insert listWithUser;
        }        
        System.assert(listWithUser[0].id != null, 'User creation failed'); 
        system.runas(listWithUser[0]){
        Test.startTest();
        list<Account> accountdatalist = TestDataFactory.createAccount(1,PS_Constants.ACCOUNT_LEARNER_RECCORD);   
        Insert accountdatalist;
        
        list<Contact> contactdatalist = TestDataFactory.createContacts(1);          
        contactdatalist[0].AccountId=accountdatalist[0].id;
        insert contactdatalist;
        
        AccountContact__c acccon = new AccountContact__c();
        acccon.Account__c = accountdatalist[0].id;
        acccon.Contact__c = contactdatalist[0].id;
        acccon.AccountRole__c ='Employee';
        acccon.Role_Detail__c='Director';
        insert acccon;
        
        acccon.Financially_Responsible__c = true;
        update acccon;
        //harika8
       contactdatalist[0].FirstName = 'test';
       contactdatalist[0].LastName = 'second test';
        update contactdatalist;
        //harika8
        if(acccon.Financially_Responsible__c == true || acccon.Primary__c == true){
            if(acccon.Primary__c == true){
            accountdatalist[0].Name = contactdatalist[0].FirstName + ' ' + contactdatalist[0].LastName;
            accountdatalist[0].FirstName__c = contactdatalist[0].FirstName;
            accountdatalist[0].LastName__c = contactdatalist[0].LastName;
            accountdatalist[0].Account_Surname__c = contactdatalist[0].LastName;
            accountdatalist[0].Phone = contactdatalist[0].Phone;
            accountdatalist[0].Mobile__c = contactdatalist[0].MobilePhone;
            accountdatalist[0].Email__c = contactdatalist[0].Email;
            accountdatalist[0].Account_Name_is_Read_only__c = false;
            }
            if(acccon.Financially_Responsible__c == true){
                accountdatalist[0].BillingCity = contactdatalist[0].MailingCity;
                 accountdatalist[0].BillingCountry = contactdatalist[0].MailingCountry;
                 accountdatalist[0].BillingCountryCode = contactdatalist[0].MailingCountryCode;
                 accountdatalist[0].BillingPostalCode = contactdatalist[0].MailingPostalCode;
                 accountdatalist[0].BillingState = contactdatalist[0].MailingState;
                 accountdatalist[0].BillingStateCode = contactdatalist[0].MailingStateCode;
                 accountdatalist[0].BillingStreet = contactdatalist[0].MailingStreet;
                 accountdatalist[0].Account_Name_is_Read_only__c = false;
            }
            update accountdatalist;
        }
        Test.stopTest();
        }
        
    }
    static testMethod void  myUnitTest3(){
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        if(listWithUser != null && !listWithUser.isEmpty()){
            insert listWithUser;
        }        
        System.assert(listWithUser[0].id != null, 'User creation failed'); 
        system.runas(listWithUser[0]){
        Test.startTest();
        list<Account> accountdatalist = TestDataFactory.createAccount(1,PS_Constants.ACCOUNT_LEARNER_RECCORD);   
        Insert accountdatalist;
        
        list<Contact> contactdatalist = TestDataFactory.createContacts(1);          
        contactdatalist[0].AccountId=accountdatalist[0].id;
        insert contactdatalist;
          List<ID> ContactsIdsList = new List<ID>();
             List<ID> AccountIdsList = new List<ID>();
        
        
            
        AccountContact__c acccon = new AccountContact__c();
        acccon.Account__c = accountdatalist[0].id;
        acccon.Contact__c = contactdatalist[0].id;
        acccon.AccountRole__c ='Employee';
        acccon.Role_Detail__c='Director';
        insert acccon;
            
        ContactsIdsList.add(accCon.Contact__c);
            AccountIdsList.add(accCon.Account__c);
        
        acccon.Financially_Responsible__c = true;
        update acccon;
        //harika8
       contactdatalist[0].FirstName = 'test';
       contactdatalist[0].LastName = 'second test';
        update contactdatalist;
        //harika8
        if(acccon.Financially_Responsible__c == true && acccon.Primary__c == true){
            if(acccon.Primary__c == true && acccon.Primary__c == true){
            accountdatalist[0].Name = contactdatalist[0].FirstName + ' ' + contactdatalist[0].LastName;
            accountdatalist[0].FirstName__c = contactdatalist[0].FirstName;
            accountdatalist[0].LastName__c = contactdatalist[0].LastName;
            accountdatalist[0].Account_Surname__c = contactdatalist[0].LastName;
            accountdatalist[0].Phone = contactdatalist[0].Phone;
            accountdatalist[0].Mobile__c = contactdatalist[0].MobilePhone;
            accountdatalist[0].Email__c = contactdatalist[0].Email;
            accountdatalist[0].Account_Name_is_Read_only__c = false;
          //  }
         // if(acccon.Financially_Responsible__c == true){
                accountdatalist[0].BillingCity = contactdatalist[0].MailingCity;
                 accountdatalist[0].BillingCountry = contactdatalist[0].MailingCountry;
                 accountdatalist[0].BillingCountryCode = contactdatalist[0].MailingCountryCode;
                 accountdatalist[0].BillingPostalCode = contactdatalist[0].MailingPostalCode;
                 accountdatalist[0].BillingState = contactdatalist[0].MailingState;
                 accountdatalist[0].BillingStateCode = contactdatalist[0].MailingStateCode;
                 accountdatalist[0].BillingStreet = contactdatalist[0].MailingStreet;
                 accountdatalist[0].Account_Name_is_Read_only__c = false;
            }
            update accountdatalist;
        }
        Test.stopTest();
        }
        
    }
    static testMethod void  myUnitTest4() { 
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        if(listWithUser != null && !listWithUser.isEmpty()){
            insert listWithUser;
        }        
        system.runas(listWithUser[0]){
        Test.startTest();
        list<Account> accountdatalist = TestDataFactory.createAccount(2,PS_Constants.ACCOUNT_LEARNER_RECCORD);   
        Insert accountdatalist;        
        
        list<Contact> contactdatalist = TestDataFactory.createContacts(2);          
        contactdatalist[0].AccountId=accountdatalist[0].id;
        contactdatalist[1].AccountId=accountdatalist[1].id;        

        Insert contactdatalist;        
        AccountContact__c accCon = new AccountContact__c();
        accCon.Contact__c = contactdatalist[0].id;
        accCon.Account__c = accountdatalist[0].id;
        accCon.Primary__c = false;
        accCon.Financially_Responsible__c = false;
        insert accCon;
        AccountContact__c accCon1 = new AccountContact__c();
        accCon1.Contact__c = contactdatalist[1].id;
        accCon1.Account__c = accountdatalist[1].id;
        accCon1.Primary__c = true;
        accCon1.Financially_Responsible__c = true;
        insert accCon1;
        
        List<AccountContact__c> accConList = new List<AccountContact__c>();
        accConList.add(accCon);
        accConList.add(accCon1);
        Map<ID, AccountContact__c> newAccConMap = new Map<ID, AccountContact__c>([select Account__c,Contact__c,Primary__c,Financially_Responsible__c
                                                                                  from AccountContact__c where id = :accConList[0].id]);
        Map<ID, AccountContact__c> oldAccConMap = new Map<ID, AccountContact__c>([select Account__c,Contact__c,Primary__c,Financially_Responsible__c
                                                                                  from AccountContact__c where id = :accConList[1].id]);
        B2CAccountSync b2c = new B2CAccountSync();
        b2c.B2CAccountSyncWhenAccountContactRelationshipIsSet(accConList);
        Test.stoptest();
        }
        
           
    }
     static testMethod void  myUnitTest5(){
         
         List<AccountContact__c> accContact = new List<AccountContact__c>([select id,Account__c,Contact__c,Primary__c from AccountContact__c where Primary__c = false LIMIT 1]);
         accContact[0].Primary__c = true;
         Update accContact;
         Map<ID, AccountContact__c> newAccConMap = new Map<ID, AccountContact__c>([select Account__c,Contact__c,Primary__c,Financially_Responsible__c
                                                                                 from AccountContact__c where id = :accContact[0].id]);
         Map<ID, AccountContact__c> oldAccConMap = new Map<ID, AccountContact__c>([select Account__c,Contact__c,Primary__c,Financially_Responsible__c
                                                                                 from AccountContact__c where id = :accContact[0].id]);
         B2CAccountSync b2c1 = new B2CAccountSync();
         checkRecurssion.run=true;
         b2c1.B2CAccountSyncWhenAccountContactRelationshipChange(accContact,newAccConMap,oldAccConMap);
       List<Contact> Conts = new List<Contact>([select id,FirstName,LastName,Phone,MobilePhone,Email,MailingStreet,MailingCity,MailingState,
                                                MailingCountry,MailingPostalCode,MailingStateCode,MailingCountryCode from Contact LIMIT 1]);
 Map<ID, Contact> newConMap = new Map<ID, Contact>([select Id,FirstName,LastName,Phone,MobilePhone,Email,MailingStreet,MailingCity,MailingState,
                                                    MailingCountry,MailingPostalCode,MailingStateCode,MailingCountryCode from Contact where id = :Conts[0].id]);
Map<ID, Contact> oldConMap = new Map<ID, Contact>([select Id,FirstName,LastName,Phone,MobilePhone,Email,MailingStreet,MailingCity,MailingState,
                                                   MailingCountry,MailingPostalCode,MailingStateCode,MailingCountryCode from Contact where id = :Conts[0].id]);
         List<Account> Accts = new List<Account>([SELECT Id, Name,Account_Surname__c, FirstName__c, LastName__c, Phone, Mobile__c, Email__c,
                                            BillingCity, BillingCountry, BillingCountryCode, BillingPostalCode, BillingState,
                                            BillingStateCode, BillingStreet
                                            FROM Account ]);
         AccountContact__c accCon = new AccountContact__c();
        accCon.Contact__c = Conts[0].id;
        accCon.Account__c = Accts[0].id;
        accCon.Primary__c = false;
        accCon.Financially_Responsible__c = false;
        insert accCon;
          AccountContact__c accCon1 = new AccountContact__c();
        accCon1.Contact__c = Conts[0].id;
        accCon1.Account__c = Accts[0].id;
        accCon1.Primary__c = True;
        accCon1.Financially_Responsible__c = True;
        insert accCon1;
  // b2c1.B2CAccountSyncWhenContactChange(Conts, newConMap, oldConMap);
        }
     static testMethod void  myUnitTest6(){
          List<Account> Accts = new List<Account>([SELECT Id, Name,Account_Surname__c, FirstName__c, LastName__c, Phone, Mobile__c, Email__c,
                                            BillingCity, BillingCountry, BillingCountryCode, BillingPostalCode, BillingState,
                                            BillingStateCode, BillingStreet
                                            FROM Account ]);
          List<Contact> Conts = new List<Contact>([select id,FirstName,LastName,Phone,MobilePhone,Email,MailingStreet,MailingCity,MailingState,
                                                MailingCountry,MailingPostalCode,MailingStateCode,MailingCountryCode from Contact LIMIT 1]);
 Map<ID, Contact> newConMap = new Map<ID, Contact>([select Id,FirstName,LastName,Phone,MobilePhone,Email,MailingStreet,MailingCity,MailingState,
                                                    MailingCountry,MailingPostalCode,MailingStateCode,MailingCountryCode from Contact where id = :Conts[0].id]);
Map<ID, Contact> oldConMap = new Map<ID, Contact>([select Id,FirstName,LastName,Phone,MobilePhone,Email,MailingStreet,MailingCity,MailingState,
                                                   MailingCountry,MailingPostalCode,MailingStateCode,MailingCountryCode from Contact where id = :Conts[0].id]);
          AccountContact__c accCon = new AccountContact__c();
        accCon.Contact__c = Conts[0].id;
        accCon.Account__c = Accts[0].id;
        accCon.Primary__c = false;
        accCon.Financially_Responsible__c = false;
        insert accCon;
          AccountContact__c accCon1 = new AccountContact__c();
        accCon1.Contact__c = Conts[0].id;
        accCon1.Account__c = Accts[0].id;
        accCon1.Primary__c = True;
        accCon1.Financially_Responsible__c = True;
        insert accCon1;
           List<AccountContact__c> accContact = new List<AccountContact__c>([select id,Account__c,Contact__c,Primary__c from AccountContact__c where Primary__c = false LIMIT 2]);
         accContact[0].Primary__c = true;
         accContact[1].Primary__c = False;
         Update accContact;
         Map<ID, AccountContact__c> newAccConMap = new Map<ID, AccountContact__c>([select Account__c,Contact__c,Primary__c,Financially_Responsible__c
                                                                                 from AccountContact__c where id = :accContact[0].id]);
         Map<ID, AccountContact__c> oldAccConMap = new Map<ID, AccountContact__c>([select Account__c,Contact__c,Primary__c,Financially_Responsible__c
                                                                                 from AccountContact__c where id = :accContact[0].id]);
        
        /* List<AccountContact__c> accContact1 = new List<AccountContact__c>([select id,Account__c,Contact__c,Primary__c from AccountContact__c where id = :Conts[0].id LIMIT 1]);
         accContact1[0].Primary__c = true;
         Update accContact1;*/
         Update accContact;
         B2CAccountSync b2c1 = new B2CAccountSync();
         checkRecurssion.run=true;
         b2c1.B2CAccountSyncWhenContactChange(Conts, newConMap, oldConMap);
     }
    //harika
    static testMethod void  myUnitTest99() { 
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        if(listWithUser != null && !listWithUser.isEmpty()){
            insert listWithUser;
        }        
        system.runas(listWithUser[0]){
        Test.startTest();
        list<Account> accountdatalist = TestDataFactory.createAccount(2,PS_Constants.ACCOUNT_LEARNER_RECCORD);   
        Insert accountdatalist;        
        
        list<Contact> contactdatalist = TestDataFactory.createContacts(2);          
        contactdatalist[0].AccountId=accountdatalist[0].id;
        contactdatalist[1].AccountId=accountdatalist[1].id;        

        Insert contactdatalist; 
            
            Map<ID, Contact> newConMap = new Map<ID, Contact>([select Id,FirstName,LastName,Phone,MobilePhone,Email,MailingStreet,MailingCity,MailingState,
                                                    MailingCountry,MailingPostalCode,MailingStateCode,MailingCountryCode from Contact where id = :contactdatalist[0].id]);
Map<ID, Contact> oldConMap = new Map<ID, Contact>([select Id,FirstName,LastName,Phone,MobilePhone,Email,MailingStreet,MailingCity,MailingState,
                                                   MailingCountry,MailingPostalCode,MailingStateCode,MailingCountryCode from Contact where id = :contactdatalist[0].id]);
        AccountContact__c accCon = new AccountContact__c();
        accCon.Contact__c = contactdatalist[0].id;
        accCon.Account__c = accountdatalist[0].id;
        accCon.Primary__c = false;
        accCon.Financially_Responsible__c = false;
        insert accCon;
        AccountContact__c accCon1 = new AccountContact__c();
        accCon1.Contact__c = contactdatalist[1].id;
        accCon1.Account__c = accountdatalist[1].id;
        accCon1.Primary__c = true;
        accCon1.Financially_Responsible__c = true;
        insert accCon1;
            
             AccountContact__c accCon2 = new AccountContact__c();
        accCon2.Contact__c = contactdatalist[1].id;
        accCon2.Account__c = accountdatalist[1].id;
        accCon2.Primary__c = true;
        accCon2.Financially_Responsible__c = false;
        insert accCon2;
         //  accountdatalist[3].LastName__c = contactdatalist[3].LastName;
          // Update accountdatalist[3];
             AccountContact__c accCon3 = new AccountContact__c();
        accCon3.Contact__c = contactdatalist[1].id;
        accCon3.Account__c = accountdatalist[1].id;
        accCon3.Primary__c = false;
        accCon3.Financially_Responsible__c = true;
        insert accCon3;
        
        List<AccountContact__c> accConList = new List<AccountContact__c>();
        accConList.add(accCon);
        accConList.add(accCon1);
             accConList.add(accCon2);
             accConList.add(accCon3);
            
            if(acccon.Financially_Responsible__c == true || acccon.Primary__c == true){
            if(acccon.Primary__c == true){
            accountdatalist[0].Name = contactdatalist[0].FirstName + ' ' + contactdatalist[0].LastName;
            accountdatalist[0].FirstName__c = contactdatalist[0].FirstName;
            accountdatalist[0].LastName__c = contactdatalist[0].LastName;
            accountdatalist[0].Account_Surname__c = contactdatalist[0].LastName;
            accountdatalist[0].Phone = contactdatalist[0].Phone;
            accountdatalist[0].Mobile__c = contactdatalist[0].MobilePhone;
            accountdatalist[0].Email__c = contactdatalist[0].Email;
            accountdatalist[0].Account_Name_is_Read_only__c = false;
            }
            if(acccon.Financially_Responsible__c == true){
                accountdatalist[0].BillingCity = contactdatalist[0].MailingCity;
                 accountdatalist[0].BillingCountry = contactdatalist[0].MailingCountry;
                 accountdatalist[0].BillingCountryCode = contactdatalist[0].MailingCountryCode;
                 accountdatalist[0].BillingPostalCode = contactdatalist[0].MailingPostalCode;
                 accountdatalist[0].BillingState = contactdatalist[0].MailingState;
                 accountdatalist[0].BillingStateCode = contactdatalist[0].MailingStateCode;
                 accountdatalist[0].BillingStreet = contactdatalist[0].MailingStreet;
                 accountdatalist[0].Account_Name_is_Read_only__c = false;
            }
            update accountdatalist;
        Map<ID, AccountContact__c> newAccConMap = new Map<ID, AccountContact__c>([select Id, Account__c, Contact__c, Primary__c, Financially_Responsible__c,AccountRole__c,Role_Detail__c
                                                                                  from AccountContact__c where id = :accConList]);
        Map<ID, AccountContact__c> oldAccConMap = new Map<ID, AccountContact__c>([select Id, Account__c, Contact__c, Primary__c, Financially_Responsible__c,AccountRole__c,Role_Detail__c
                                                                                  from AccountContact__c where id = :accConList]);
        B2CAccountSync b2c = new B2CAccountSync();
        b2c.B2CAccountSyncWhenContactChange(contactdatalist, newConMap, oldConMap );
        Test.stoptest();
        }
        
           
    }
    //harika

   
}
}