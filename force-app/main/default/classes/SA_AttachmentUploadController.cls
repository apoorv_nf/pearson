/************************************************************************************************************
* Apex Class Name   : AttachmentUploadController 
* Version           : 1.1
* CreatedBy         : SA Offshore team
* Function          : Attach a file from Web-To-Case page
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* 
************************************************************************************************************/


public with sharing class SA_AttachmentUploadController {
    public Case CaseObj {get;set;}
    public Attachment attachmentObj {get;set;}
    public string caseProgram {get; set;}  
    Public string caseStatus {get; set;}
    case CaseObj1= new case();
    public SA_AttachmentUploadController(ApexPages.StandardController controller) {
       String caseId = ApexPages.currentPage().getParameters().get('caseid');
        if(caseId != null && caseId !=''){
           // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Case has been inserted, Your case number is '+caseId));
            
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Thank you for contacting National Board Customer Support. Case Number ' + caseId + ' has been opened in response to your inquiry. Your ticket will be reviewed in the order in which it was received.'));
            ApexPages.currentPage().getParameters().remove('caseid');
        }
        CaseObj = new case();
        attachmentObj = new Attachment();
        caseProgram = 'NBPTS';
        caseStatus = 'New';
    }
    
    public string ContactName {get;set;}
    public string ConEmail {get;set;}
    public string caseSubject {get;set;}
    public string caseDescription {get;set;}    
    public string paramIdFromUrl;
    
    
    public PageReference createCase(){        
        List <Contact> ConList = new List <Contact>();
        ConList = [Select id, name, Email from Contact where email =: ConEmail LIMIT 1];      
        if(!ConList.isEmpty())
        {   
         
            CaseObj.ContactId = ConList[0].id;
            CaseObj.SuppliedName = ContactName;   
        }
        
        CaseObj.SuppliedEmail = ConEmail;
        CaseObj.origin = 'Web';
        CaseObj.RecordTypeId = Label.SA_Case_Record_Type_ID;
        CaseObj.subject = caseSubject;
        CaseObj.SuppliedName = ContactName; 
        CaseObj.Description = caseDescription;
        CaseObj.Program__c = caseProgram;
        CaseObj.status = caseStatus;
        
        string soql ='Select Id,name from Group where type = \'Queue\' AND name IN (' + Label.NBPTS_WebtoCase_queue + ' )';
        
        list<group> groupList = new list<group>();
        groupList = Database.query(soql);
        map<string,id> queueName = new map<string,id>();
        
        for(group queueRec : groupList )
        {
            queueName.put(queueRec.name, queueRec.id); 
        }
        paramIdFromUrl = ApexPages.currentPage().getParameters().get('nbptsid');
        try{
            if(paramIdFromUrl== 'customersupport'){
                if(queueName != null && queueName.containskey('NAUS-SA: NB Customer Support')){
                    CaseObj.ownerid = queueName.get('NAUS-SA: NB Customer Support');
                }
            }
            else if(paramIdFromUrl== 'tpprogram'){
                if(queueName != null && queueName.containskey('NAUS-SA: NB TPP Support')){
                   CaseObj.ownerid = queueName.get('NAUS-SA: NB TPP Support');
                } 
            }
        }
        catch (DMLException e)
        {
        }
        if(ContactName == null || ContactName == '')
        {
            attachmentObj = new Attachment();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please enter Contact Name'));
        }
        else if(ConEmail  == null || ConEmail  == '')
        {
            attachmentObj = new Attachment();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please enter Email'));
        }
        else if(caseSubject == null || caseSubject == '')
        {
            attachmentObj = new Attachment();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please enter Case Subject'));
        }
        else if(caseDescription == null || caseDescription == '')
        {
            attachmentObj = new Attachment();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please enter the Description'));
        }        
        
        else
        {   
            if(checkEmail(ConEmail)){
            insert CaseObj;
            CaseObj1 = [select id,CaseNumber from case where id =: CaseObj.id];
            //if(CaseObj.id != null){
                //return upload(CaseObj);            
            //}
            upload(CaseObj);      
            PageReference page = new PageReference(label.NBPTS_Page_Redirect);        
            page.getParameters().put('nbptsid', paramIdFromUrl);
            page.getParameters().put('caseid', CaseObj1.CaseNumber);
            return page;
        } 
            else {
                ApexPages.addMessage(New ApexPages.Message(ApexPAges.severity.Error, 'Please enter a valid email address'));
             }
        }    
        return null;       
            
        
    }
    
    public void upload(case CaseObj) {
        attachmentObj.OwnerId = UserInfo.getUserId();
        attachmentObj.ParentId = CaseObj.id; 
        attachmentObj.IsPrivate = false;
        try 
        {
            if(attachmentObj.body != NULL && CaseObj.id != null){
                insert attachmentObj;  
                attachmentObj = new Attachment();
            }
            
        } 
        catch (DMLException e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
           // return null;
        }
               
        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Case has been inserted, Your case number is '+CaseObj1.CaseNumber));    
        clearInputs();        
    }  
    public void clearInputs(){
        ContactName = ''; 
        ConEmail = '';
        caseSubject='';
        caseDescription='';
    }
    
    public  Boolean checkEmail(String sEmail) {
        Boolean returnValue = true;
        String InputEmail = ConEmail;
        String emailRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
        Pattern MyPattern = Pattern.compile(emailRegex);
        
        Matcher MyMatcher = MyPattern.matcher(InputEmail);
        
        if(!MyMatcher.matches()) {
            returnValue = false;
        }
        return returnValue;
    }
   
}