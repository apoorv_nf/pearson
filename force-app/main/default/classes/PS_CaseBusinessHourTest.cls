/************************************************************************************************************
* Apex Interface Name    : PS_CaseBusinessHourTest
* Version                : 1.1 
* Created Date           : 1/2/2016 2:44 
* Description            : Test class created for class PS_CaseBusinessHour
* Expected Inputs        : Pass case with recordtype like 'State & National'
* Expected Outputs       : Case related to business hour name should get stamp
* Modification Log       : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Hasi Chakravarty         1/2/2016
* Ajay Chakradhar           3/3/2016   
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_CaseBusinessHourTest{ //To calculate business hours of a case according to its recordtype
     /*
     * Method to cover scenario where case recordtype will stamp it related business hours profile name.
     */
    static testMethod void validatePS_CaseBusinessHour(){
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,1);
        insert listWithUser; 
        list<Permissionset> ps=new list<Permissionset>();
        ps=[select id,name from permissionset where name =:'Pearson_S_N_Agents'];
        PermissionSetAssignment psa = new PermissionSetAssignment
         (PermissionSetId = ps[0].id, AssigneeId = listWithUser[0].id );
        insert psa; 
                
        System.runAs(listWithUser[0]){
           Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Triggers__c=true);
           insert byp;
           Test.StartTest(); 
           Account acc = new Account(Name = 'Account 1',ShippingStreet = 'test account 1',ShippingCity='account city',ShippingPostalCode = '2222');
            insert acc;
           List<case> cases = TestDataFactory.createCase_SelfService(1,'State & National');
            cases[0].origin='Phone';
            cases[0].Program__c='PARCC';
            cases[0].accountId = acc.id;
            cases[0].Email_Initial_Response_Date__c = system.today()+1;
            insert cases;
           
            //update cases;
            Case ca=[select id,businesshours.name from  case where id=:cases[0].id];
            System.Assert(ca.businesshours.name != null, 'Business hour is not stamped');
            Test.StopTest();
          }
        }
    /*
     * Method to cover -ve scenario where case recordtype will stamp it related business hours profile name.
     */
     static testMethod void Negative_test_validatePS_CaseBusinessHour(){
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,1);
        insert listWithUser; 
        list<Permissionset> ps=new list<Permissionset>();
        ps=[select id,name from permissionset where name =:'Pearson_S_N_Agents'];
        PermissionSetAssignment psa = new PermissionSetAssignment
         (PermissionSetId = ps[0].id, AssigneeId = listWithUser[0].id );
        insert psa; 
        System.runAs(listWithUser[0]){
        Test.StartTest();
        List<case> cases = TestDataFactory.createCase_SelfService(1,'General');
        Account acc = new Account(Name = 'Account 1',ShippingStreet = 'test account 1',ShippingCity='account city',ShippingPostalCode = '2222');
        cases[0].origin='Phone';
        insert cases;
        insert acc;
        cases[0].accountId = acc.id;
        cases[0].Email_Initial_Response_Date__c = system.today()+1;
        update cases;
        Case ca=[select id,businesshours.name from  case where id=:cases[0].id];
        System.Assert(ca.businesshours.name == 'default', 'Business hour is not stamped');
        Test.StopTest();
      }
    }
}