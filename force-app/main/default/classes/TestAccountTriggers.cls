@isTest
private class TestAccountTriggers
{
    static testMethod void myUnitTest()
    {
        Test.startTest();
        
        // TO DO: implement unit test
        
        TestClassAutomation.FillAllFields = true;
        
        Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
        sAccount.BillingCountry         = 'Australia';
        sAccount.BillingState           = 'Victoria';
        sAccount.BillingCountryCode     = 'AU';
        sAccount.BillingStateCode       = 'VIC';
        sAccount.ShippingCountry        = 'Australia';
        sAccount.ShippingState          = 'Victoria';
        sAccount.ShippingCountryCode    = 'AU';
        sAccount.ShippingStateCode      = 'VIC';
        
       // insert sAccount;
        
        Database.SaveResult sr = Database.insert(sAccount, false);
        //Account acc = [SELECT id,ShippingCountry FROM Account WHERE id =: sr.getId()];
        //acc.ShippingCountry = 'Kenya';
        //system.debug('\n\n ################TestAccountTriggers account :'+acc+'\n\n');
        Account acct_2 = new Account (name = 'Test2',BillingStreet = 'Street1',
                   BillingCity = 'Karnataka', BillingPostalCode = 'ABC DEF', BillingCountry = 'India',
                    Vista_Account_Number__c = 'xyz');
        insert acct_2;
Account acct = new Account (name = 'Test1',BillingStreet = 'Street1',
                   BillingCity = 'Victoria', BillingPostalCode = 'ABC DEF', BillingCountry = 'Australia',
                    Vista_Account_Number__c = 'xyz' );
      Account accu = [SELECT id,ShippingCountry FROM Account WHERE id =: acct_2.Id];
        update accu;

        //update acc;
        
        delete accu;
        
        undelete accu;
        
        Test.stopTest();
    }
}