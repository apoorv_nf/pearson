/*******************************************************************************************************************
* Class Name       : PS_createAssetbasedStudentRegistration
* Version          : 1.0 
* Created Date     : 30 March 2015
* Function         : Test Class of the PS_createAssetbasedStudentRegistration
* Modification Log :
*
* Developer                                Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*   Accenture IDC                          9/05/2015              Created Initial Version
*******************************************************************************************************************/

@isTest             
public class TestPS_CreateAssetbasedStudent{
 static testMethod void myTest() { 

/*list<Account> accountdatalist = TestDataFactory.createAccount(3,'Learner');   
Insert accountdatalist;
Update accountdatalist;*/
    Account acc = new Account();
     acc.Name = 'Test';
     acc.Line_of_Business__c= 'Higher Ed';
     acc.Geography__c = 'North America';
     acc.Market2__c = 'US';
     insert acc;
     
     
     contact con = new contact();
     con.Lastname= 'testcon';
     con.Firstname= 'testcon1';
     con.MobilePhone = '9999'; 
     con.Preferred_Address__c = 'Other Address';
     con.OtherCountry  = 'India';
     con.OtherStreet = 'Test';
     con.OtherCity  = 'Test';
     con.OtherPostalCode  = '123456';
     con.accountId = acc.id;
     insert con;
     
     Test.startTest();
  //for(Account newAcc : accountdatalist){
     List<Opportunity>opplist = new List<Opportunity>();
     Map<Id,Opportunity> optyMap = new Map<Id,Opportunity>();
     Map<Id,Opportunity> oldMapOpty = new Map<Id,Opportunity>();
     Opportunity opp = new opportunity();
      opp.AccountId = acc.id;    
      opp.Name = 'OppTest';
      opp.StageName = 'Qualification';
      opp.Type = 'New Business';
      opp.Enrolment_Status__c = 'Enrolled';
      opp.CloseDate = system.today();
      opp.International_Student_Status__c = 'Yes';
      opp.Qualification__c = 'Psychology';
      opp.Market__c = 'US';
     opp.RecordTypeId = [Select id from RecordType where SObjectType = 'Opportunity' and Name = 'B2B'][0].ID;
     
      
      Opportunity opp1 = new opportunity();
      opp1.AccountId = acc.id;    
      opp1.Name = 'OppTest';
      opp1.StageName = 'Qualification';
      opp1.Type = 'Returning Business';
      opp1.Enrolment_Status__c = 'Enrolled';
      opp1.CloseDate = system.today();
      opp1.International_Student_Status__c = 'No';
      opp.Qualification__c = 'Graphic Design';
      opp1.Market__c = 'US';
     opp1.RecordTypeId = [Select id from RecordType where SObjectType = 'Opportunity' and Name = 'D2L'][0].ID;
      
     Opportunity opp2 = new opportunity();
      opp2.AccountId = acc.id;    
      opp2.Name = 'OppTest';
      opp2.StageName = 'Qualification';
      opp2.Type = 'New Business';
      opp2.Enrolment_Status__c = 'Enrolled';
      opp2.CloseDate = system.today();
      opp2.International_Student_Status__c = 'No';
      opp2.Qualification__c = 'Psychology';
      opp2.Market__c = 'US';
      opp2.RecordTypeId = [Select id from RecordType where SObjectType = 'Opportunity' and Name = 'D2L'][0].ID;
      
     Opportunity opp3 = new opportunity();
      opp3.AccountId = acc.id;    
      opp3.Name = 'OppTest';
      opp3.StageName = 'Qualification';
      opp3.Type = 'Returning Business';
      opp3.Enrolment_Status__c = 'Enrolled';
      opp3.CloseDate = system.today();
      opp3.International_Student_Status__c = 'Yes';
      opp3.Qualification__c = 'Psychology';
      opp3.Market__c = 'US';
      opp3.RecordTypeId = [Select id from RecordType where SObjectType = 'Opportunity' and Name = 'D2L'][0].ID;
         
      opplist.add(opp);
      opplist.add(opp1);
      opplist.add(opp2);
     opplist.add(opp3);
      
      
      insert opplist;
      
    
    Task tsk1 = new Task();
                tsk1.Status = 'Not Started';
                tsk1.WhatId = opp.Id;
                //tsk1.RecordTypeId = recId;
                tsk1.subject = 'Obtain Matric Certificate';
                tsk1.Auto_Generated__c = true;
                insert tsk1;
             
       PS_createAssetbasedStudentRegistration.createTask(opplist);
       PS_createAssetbasedStudentRegistration.OpportunityStage(optyMap,opplist,oldMapOpty );
       PS_createAssetbasedStudentRegistration.createTask_Method(opplist);
       PS_createAssetbasedStudentRegistration.createTask_Method1(opplist);
       PS_createAssetbasedStudentRegistration.ReturningBuisinesscreateTask(opplist);
       PS_createAssetbasedStudentRegistration.createTaskOnPsychology(opplist);
       PS_createAssetbasedStudentRegistration.ValidationOnTask(optyMap,opplist);
       PS_createAssetbasedStudentRegistration.ValidationOnTaskAndEvent(optyMap,opplist);
       
       Test.stoptest();
       
  }
}