/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 15/07/2018
 */
global without sharing class EinsteinBotChatLogger {

    @InvocableMethod(label='Einstein Bot - Log Chat')
    global static void logChat(List<ChatLoggerRequest> chatLogRequests) 
    {
        List<Einstein_Bot_Event__e> chatLogEvents = new List<Einstein_Bot_Event__e>();

        for(ChatLoggerRequest chatLogRequest : chatLogRequests) {

            Einstein_Bot_Event__e chatLogEvent = new Einstein_Bot_Event__e();
            chatLogEvent.Type__c = EinsteinBotEventHandler.LOG_CHAT;
            chatLogEvent.Live_Agent_Session_Id__c = chatLogRequest.liveAgentSessionId;
            chatLogEvent.Current_Utterance__c = chatLogRequest.currentUtterance;
            chatLogEvent.Current_Threshold_High__c = chatLogRequest.currentThresholdHigh;
            chatLogEvent.Current_Confidence_For_Utterance__c = chatLogRequest.currentConfidenceForUtterance;
            chatLogEvent.Current_Dialog_Id__c = chatLogRequest.currentDialogId;
            chatLogEvent.Current_Dialog_Name__c = chatLogRequest.currentDialogName;

            chatLogEvents.add(chatLogEvent);
        }

        List<Database.SaveResult> saveResults = EventBus.publish(chatLogEvents);
    }

    global class ChatLoggerRequest
    {
        @InvocableVariable
        global String liveAgentSessionId;

        @InvocableVariable
        global String currentUtterance;

        @InvocableVariable
        global Decimal currentThresholdHigh;

        @InvocableVariable
        global Decimal currentConfidenceForUtterance;

        @InvocableVariable
        global String currentDialogId;

        @InvocableVariable
        global String currentDialogName;
    }
}