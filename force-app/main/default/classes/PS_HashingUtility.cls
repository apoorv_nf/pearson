/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_HashingUtility.cls 
   Description:     Utility class to convert the string passed to Hash.
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  22/12/2015         1.0            Rahul Boinepally                       Initial Release 
 ---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public with sharing class PS_HashingUtility 
{
    public static string optOut = 'Opt Out';
    public static string reOpt = 'Re-Opt In';
    public static string hashId = 'SHA256';

    public static string generateHash(string hashType, string textToBeConverted) 
    {

        blob hash = Crypto.generateDigest(hashType, Blob.valueOf(textToBeConverted));
        string convertedHash = EncodingUtil.convertToHex(hash);
        return convertedHash;
        
    }

    public static void extractEmailHash(List<Case> caseInserts)
    {
        Set<ID> contactSet = new Set<ID>();
        Map<Id,String> emailMap = new Map<Id,String>();
        
        for(Case cc: caseInserts)
        {
            contactSet.add(cc.contactId);
        }
        
        for(List<Contact> con : [Select Email from Contact where ID in :contactSet])
        {
            
            for(Contact conFor: con)
            {               
                emailMap.put(conFor.Id, conFor.Email);  
            }
            
        }       

        for(Case cc: caseInserts)
        {
            if((cc.Type == optOut || cc.Type == reOpt) && ((emailMap.get(cc.ContactId)) != null)) 
            {               
                string emailToConvert = emailMap.get(cc.ContactId);
                String convertedEmail = generateHash(hashId, emailToConvert);
                cc.Email_Hash__c = convertedEmail;              
            }
        }
    }
    
    public static void extractEmailHash(List<sObject> sObjectRecords,String sObjectTypeStr)
    {
        sObjectTypeStr = sObjectTypeStr.toLowerCase();
        String fieldToBeHashed;
        if(sObjectTypeStr == Label.PS_SObjectContact)
        {
            fieldToBeHashed = 'Email';
        }else if(sObjectTypeStr == Label.PS_SObjectLead)
        {
            fieldToBeHashed = 'Email';
        }
        for(sObject sObjRecord : sObjectRecords)
        {
            if(sObjRecord.get(fieldToBeHashed) != null && sObjRecord.get(fieldToBeHashed) != '')
            {
                string emailToConvert = (String)sObjRecord.get(fieldToBeHashed);
                emailToConvert  = emailToConvert.toLowerCase();
                String convertedEmail = generateHash(hashId, emailToConvert);
                sObjRecord.put('Email_Hashed_ID__c',convertedEmail);
            }
        }
    }
}