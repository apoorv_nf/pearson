/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_UpdateAccountChildTeamsController
   Description:     Allows user to update the child account team member(s)
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   19 oct 2015      1.0           Accenture NDC                                         Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class PS_UpdateAccountChildTeamsController
{
    public Id parentAccountId;
    public Map<ID,String> mapWithAccountTeamMember{get;set;}
    public Map<ID,List<AccountShare>> mapWithAccountShare{get;set;}
    public Set<Id> setWithAcctIds;
    public List<Id> lstWithChildAcctIds{get;set;}
    public boolean checkFlag{get;set;}
    public boolean selectProductUI{get;set;}
    public Set<Id> setWithParentTeamMemIds;
    public Set<Id> setWithTeamMemUserIds;
    public String selectedAcctId{get;set;}
    public Id selectedAcctTeamMemId{get;set;}
    public String selectedUserId{get;set;}
    public Map<Id,List<AccountTeamMember>> mapWithAccountTeamMemberList{get;set;}
    public integer counter=0;  //keeps track of the offset
    public integer list_size=20; //sets the page size or number of rows
    public integer total_size{get;set;} //used to show user the total size of the list
    public List<childTeamMemberWrapper> lstWithChildMemberWrapper;
    public user loggedInUserBusinessUnit;
    public Map<Id,String> mapWithParentAccountTeamMemberRole;
    public PS_UpdateAccountChildTeamsController(ApexPages.StandardController controller) 
    {
        parentAccountId = ApexPages.currentPage().getParameters().get('accountId');
        mapWithAccountTeamMember = new Map<ID,String>();
        setWithParentTeamMemIds = new Set<Id>();
        mapWithAccountShare = new Map<ID,List<AccountShare>>();
        mapWithAccountTeamMemberList = new Map<Id,List<AccountTeamMember>>();
        lstWithChildMemberWrapper = new List<childTeamMemberWrapper>();
        setWithAcctIds = new Set<Id>();
        setWithTeamMemUserIds = new Set<Id>();
        lstWithChildAcctIds = new List<ID>();
        
        mapWithParentAccountTeamMemberRole = new Map<Id,String>();
        if(userinfo.getuserid() != null)
        {
            loggedInUserBusinessUnit = [select Business_Unit__c from user where id =: userinfo.getuserid()];
        }
        
        if(parentAccountId != null)
        {
            for(AccountTeammember accParTeam : [select Id,UserId,AccountId,TeamMemberRole from AccountTeamMember where AccountId =: parentAccountId limit 1000])
            {
                setWithParentTeamMemIds.add(accParTeam.UserId);
                mapWithParentAccountTeamMemberRole.put(accParTeam.UserId,accParTeam.TeamMemberRole);
            }  
            for(Account acctIds : [select Id from Account Where parentId =: parentAccountId])
            {
                setWithAcctIds.add(acctIds.Id);
                //used in VF page to render confirm button
                lstWithChildAcctIds.add(acctIds.Id);
            }
            total_size = [select count() from AccountTeamMember where AccountId IN : setWithAcctIds And User.Business_Unit__c =: loggedInUserBusinessUnit.Business_Unit__c And UserId NOT IN : setWithParentTeamMemIds];
        }
        
        if(setWithAcctIds.size() == 0)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'There are no child accounts for this Account');
            ApexPages.addMessage(myMsg);
        }else
        if(total_size != null)
        {
            if(total_size == 0)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'There are no differences on the child Account teams/There are currently no team members on the child Account(s)');
                ApexPages.addMessage(myMsg);
            }
        }
          
    }
    
    public Map<Id,List<AccountTeamMember>> getAcctTeamMember()
    {
        try
        {
        if(mapWithAccountTeamMemberList != null)
        {
            mapWithAccountTeamMemberList.clear();
        }    
        if(mapWithAccountShare != null)
        {
            mapWithAccountShare.clear();
        }    
        for(AccountTeamMember accTeamMember : [select Id,AccountId,UserId,User.Name,Account.Name,TeamMemberRole,AccountAccessLevel from AccountTeamMember where AccountId IN : setWithAcctIds And User.Business_Unit__c =: loggedInUserBusinessUnit.Business_Unit__c And UserId NOT IN : setWithParentTeamMemIds ORDER BY Account.Name LIMIT : list_size OFFSET: counter])
        {
            if(mapWithAccountTeamMemberList.containsKey(accTeamMember.AccountId))
            {
                mapWithAccountTeamMemberList.get(accTeamMember.AccountId).add(accTeamMember);
            }else
            {
                mapWithAccountTeamMember.put(accTeamMember.AccountId,accTeamMember.Account.Name);
                List<AccountTeamMember> tempList = new List<AccountTeamMember>();
                tempList.add(accTeamMember);
                mapWithAccountTeamMemberList.put(accTeamMember.AccountId,tempList);
            }
                setWithTeamMemUserIds.add(accTeamMember.UserId);        
        }
        for(AccountShare accountShareLst : [select UserOrGroupId,Accountid,AccountAccessLevel,OpportunityAccessLevel,CaseAccessLevel,ContactAccessLevel from AccountShare where Accountid IN: setWithAcctIds and UserOrGroupId IN: setWithTeamMemUserIds])
        {
            if(accountShareLst.AccountAccessLevel == Label.PS_AccountShareEdit)
            {
                accountShareLst.AccountAccessLevel = Label.PS_AccountShareRead_Write;
            }
            if(accountShareLst.OpportunityAccessLevel== Label.PS_AccountShareEdit)
            {
                accountShareLst.OpportunityAccessLevel= Label.PS_AccountShareRead_Write;
            }
            if(accountShareLst.CaseAccessLevel== Label.PS_AccountShareEdit)
            {
                accountShareLst.CaseAccessLevel= Label.PS_AccountShareRead_Write;
            }
            if(accountShareLst.ContactAccessLevel == Label.PS_AccountShareEdit)
            {
                accountShareLst.ContactAccessLevel = Label.PS_AccountShareRead_Write;
            }
            if(mapWithAccountShare.containsKey(accountShareLst.Accountid))
            {
                mapWithAccountShare.get(accountShareLst.Accountid).add(accountShareLst);
            }else
            {
                List<AccountShare> tempList = new List<AccountShare>();
                tempList.add(accountShareLst);
                mapWithAccountShare.put(accountShareLst.Accountid,tempList);
            }
        }
        }catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
            ApexPages.addMessage(myMsg);  
        }
        return mapWithAccountTeamMemberList;
    }
    
    public PageReference Beginning() 
    { 
        //user clicked beginning
        counter = 0;
        return null;
    }

    public PageReference Previous() 
    { 
        //user clicked previous button
        counter -= list_size;
        return null;
    }

    public PageReference Next() 
    { 
        //user clicked next button
        counter += list_size;
        return null;
    }

    public PageReference End() 
    { 
        //user clicked end
        counter = total_size - math.mod(total_size, list_size);
        return null;
    }

    public Boolean getDisablePrevious() 
    { 
        //this will disable the previous and beginning buttons
        if (counter>0) 
        {
            return false;
        }else 
        {
            return true;
        }
    }

    public Boolean getDisableNext() 
    { 
        //this will disable the next and end buttons
        if (counter + list_size < total_size) 
        {
            return false; 
        }else 
        {
            return true;
        }
    }

    public Integer getTotal_size() 
    {
        return total_size;
    }

    public Integer getPageNumber() 
    {
        return counter/list_size + 1;
    }

    public Integer getTotalPages() 
    {
        if(math.mod(total_size, list_size) > 0) 
        {
            return total_size/list_size + 1;
        }else 
        {
            return (total_size/list_size);
        }
        
    }
    
    public void saveSelectedRecordsInTemp()
    {
        boolean recordNotFound = false;
        boolean recordFoundForRemoval = false;
        Integer rowIndex = 0;
        try
        {
        if(selectedAcctId != null && selectedUserId != null && selectedAcctTeamMemId != null)
        {
            if(checkFlag)
            {
                if(lstWithChildMemberWrapper != null && !lstWithChildMemberWrapper.isEmpty())
                {
                        for(childTeamMemberWrapper wrapperRec : lstWithChildMemberWrapper)
                        {
                            if(wrapperRec.accountId == selectedAcctId && wrapperRec.teamMemberId == selectedUserId)
                            {
                                break;
                            }else
                            {
                                recordNotFound = true;
                            }
                        }
                }else
                {
                    lstWithChildMemberWrapper.add(new childTeamMemberWrapper(selectedAcctId,selectedUserId,selectedAcctTeamMemId));
                }
                if(recordNotFound)
                {
                    lstWithChildMemberWrapper.add(new childTeamMemberWrapper(selectedAcctId,selectedUserId,selectedAcctTeamMemId));
                }
            }else
            {
                if(lstWithChildMemberWrapper != null && !lstWithChildMemberWrapper.isEmpty())
                {
                        for(childTeamMemberWrapper wrapperRec : lstWithChildMemberWrapper)
                        {
                            if(wrapperRec.accountId == selectedAcctId && wrapperRec.teamMemberId == selectedUserId)
                            {
                                recordFoundForRemoval = true;
                                break;
                            }
                            rowIndex = rowIndex+1;
                        }
                        if(recordFoundForRemoval)
                        {
                            lstWithChildMemberWrapper.remove(rowIndex);
                        }
                }
            }
        }
        }catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
            ApexPages.addMessage(myMsg);  
        }
    }
    
    public void saveBulkSelectedRecordsInTemp()
    {
        boolean recordNotFound = false;
        boolean recordFoundForRemoval = false;
        Integer rowIndex = 0;
        try{
        if(selectedAcctId != null)
        {
            if(checkFlag)
            {
                if(lstWithChildMemberWrapper != null && !lstWithChildMemberWrapper.isEmpty())
                {
                    for(childTeamMemberWrapper wrapperRec : lstWithChildMemberWrapper)
                    {
                        if(wrapperRec.accountId == selectedAcctId)
                        {
                            break;
                        }else
                        {
                            recordNotFound = true;
                        }
                    }
                }else
                {
                    for(AccountTeamMember acctTeamMem : mapWithAccountTeamMemberList.get(selectedAcctId))
                    {
                        if(acctTeamMem.UserId != null)
                        {
                            lstWithChildMemberWrapper.add(new childTeamMemberWrapper(selectedAcctId,acctTeamMem.UserId,acctTeamMem.Id));
                        }    
                    }
                }
                
                if(recordNotFound )
                {
                    for(AccountTeamMember acctTeamMem : mapWithAccountTeamMemberList.get(selectedAcctId))
                    {
                        if(acctTeamMem.UserId != null)
                        {
                            lstWithChildMemberWrapper.add(new childTeamMemberWrapper(selectedAcctId,acctTeamMem.UserId,acctTeamMem.Id));
                        }    
                    }
                }
            }else
            {
                if(lstWithChildMemberWrapper != null && !lstWithChildMemberWrapper.isEmpty())
                {
                    for(childTeamMemberWrapper wrapperRec : lstWithChildMemberWrapper)
                    {
                        if(wrapperRec.accountId == selectedAcctId)
                        {
                            recordFoundForRemoval = true;
                            break;
                        }
                        rowIndex = rowIndex+1;
                    }
                    if(recordFoundForRemoval)
                    {
                        lstWithChildMemberWrapper.remove(rowIndex);
                    }
                }
            }
        }
        }catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
            ApexPages.addMessage(myMsg);  
        }
         
    }
    
    public void confirmChanges()
    {
        List<AccountTeamMember> lstWithDelAcctTeamMem = new List<AccountTeamMember>();
        AccountTeamMember newAccountTeamMember;
        List<AccountTeamMember> lstWithnewAccountTeamMember = new List<AccountTeamMember>();
        Map<Id,String> mapWithParentAccountTeamShare = new Map<Id,String>();
        AccountTeamMember delAcctTeamMem;
        AccountShare newAccountShare;
        List<AccountShare> lstWithNewAccountShare = new List<AccountShare>();
        try
        {
            if(setWithParentTeamMemIds.size() > 0 && parentAccountId != null)
            {
                for(AccountShare parentAccShare : [select AccountId,UserOrGroupID,AccountAccessLevel,ContactAccessLevel,OpportunityAccessLevel,CaseAccessLevel from AccountShare where AccountId =: parentAccountId])     
                {
                    mapWithParentAccountTeamShare.put(parentAccShare.UserOrGroupID,parentAccShare.AccountAccessLevel+','+parentAccShare.ContactAccessLevel+','+parentAccShare.OpportunityAccessLevel+','+parentAccShare.CaseAccessLevel);    
                }
            }
            for(childTeamMemberWrapper modifyAcctTeam : lstWithChildMemberWrapper)
            {
                delAcctTeamMem = new AccountTeamMember();
                delAcctTeamMem.Id = modifyAcctTeam.AcctTeamRecId;
                lstWithDelAcctTeamMem.add(delAcctTeamMem);
            }
            if(lstWithDelAcctTeamMem.size()>0)
            {
                delete lstWithDelAcctTeamMem;
            }
            
            if(setWithParentTeamMemIds.size() > 0 && !setWithParentTeamMemIds.isEmpty())
            {
                for(Id childAcc : setWithAcctIds)
                {
                    for(Id accTeam : setWithParentTeamMemIds)
                    { 
                        newAccountTeamMember = new AccountTeamMember();
                        newAccountTeamMember.UserId = accTeam;
                        newAccountTeamMember.AccountId = childAcc;
                        newAccountTeamMember.TeamMemberRole = mapWithParentAccountTeamMemberRole.get(accTeam); 
                        lstWithnewAccountTeamMember.add(newAccountTeamMember);
                    }     
                }
            }
            if(lstWithnewAccountTeamMember.size()>0)
            {
                Database.SaveResult[] lsr = Database.insert(lstWithnewAccountTeamMember,false);
                Integer newcnt=0; 
                for(Database.SaveResult sr:lsr)
                { 
                    if(!sr.isSuccess())
                    { 
                        Database.Error emsg =sr.getErrors()[0]; 
                    }
                    else
                    {
                        newAccountShare = new AccountShare();
                        String accessLevels = mapWithParentAccountTeamShare.get(lstWithnewAccountTeamMember[newcnt].UserId);
                        List<String> lstAccessLevel = accessLevels.split(',');
                        if(lstAccessLevel != null && !lstAccessLevel.isEmpty() && lstAccessLevel.size() > 0)
                        {
                            newAccountShare.AccountId = lstWithnewAccountTeamMember[newcnt].AccountId;
                            newAccountShare.UserOrGroupId= lstWithnewAccountTeamMember[newcnt].UserId;
                            newAccountShare.AccountAccessLevel = lstAccessLevel[0];
                            newAccountShare.ContactAccessLevel = lstAccessLevel[1];
                            newAccountShare.OpportunityAccessLevel = lstAccessLevel[2];
                            newAccountShare.CaseAccessLevel = lstAccessLevel[3];
                        }
                        lstWithNewAccountShare.add(newAccountShare);
                    } 
                    newcnt++;   
                } 
                Database.SaveResult[] lsr0 =Database.insert(lstWithNewAccountShare,false); 
                Integer newcnt0=0; 
                for(Database.SaveResult sr0:lsr0)
                {
                    if(!sr0.isSuccess())
                    { 
                        Database.Error emsg0=sr0.getErrors()[0]; 
                    } 
                    newcnt0++; 
                } 
            }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Request Processed Successfully');
            ApexPages.addMessage(myMsg); 
        }catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
            ApexPages.addMessage(myMsg);  
        }
    }
    
    public class childTeamMemberWrapper
    {
        public Id accountId{get;set;} 
        public Id teamMemberId;
        public Id AcctTeamRecId;
        public childTeamMemberWrapper(Id innerAccountId,ID innerTeamMemberId,ID innerAcctTeamRecId)
        {
            accountId = innerAccountId;
            teamMemberId = innerTeamMemberId;
            AcctTeamRecId = innerAcctTeamRecId;
        }
    }
}