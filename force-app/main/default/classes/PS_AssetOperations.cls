/************************************************************************************************************
* Apex Class Name   : PS_AssetOperations.cls
* Version           : 1.0 
* Created Date      : 03 Nov 2015
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Gousia begum                3/11/2015              Created as per RD-01023
************************************************************************************************************/

Public without sharing class PS_AssetOperations implements PS_GenricTriggerInterface {


public void beforeInsert(List<sObject> ccNewList, Map<ID,sObject> NewMap) {

     
    
 }
public void afterInsert(List<sobject> ccNewList, Map<ID,sobject> NewMap) {
     mapCourseContactAsset(ccNewList);
     
 
 }

 public void beforeUpdate(List<sobject> ccNewList, Map<ID,sobject> NewMap,List<sobject> ccOldList, Map<ID,sobject> OldMap) {    
     
 
 }

 public void afterUpdate(List<sobject> ccNewList,  Map<ID,sobject> NewMap, List<sobject> ccOldList, Map<ID,sobject> OldMap) {    
     
     syncStatusWithCourseContactPIU(ccNewList,OldMap);
 }

 
 public void beforeDelete(List<sobject> ccNewList, Map<ID,sobject> NewMap,List<sobject> ccOldList, Map<ID,sobject> OldMap) {    
     
 
 }
public void afterDelete(List<sobject> ccNewList, Map<ID,sobject> NewMap,List<sobject> ccOldList, Map<ID,sobject> OldMap) {
     
    
 }
public void afterUndelete(List<sobject> ccOldList, Map<ID,sobject> OldMap) {
     
    
 }

 
 public void mapCourseContactAsset(List<Asset> lstAsset){

        Map<Id,Id> mapCourseAsset = new Map<Id,Id>(); 
        List<Contact_Product_In_Use__c> lstContactPIU = new List<Contact_Product_In_Use__c>();
        List<Course_Product_in_Use__c> lstCoursePIU = new List<Course_Product_in_Use__c>();
        for(Asset assetObj:lstAsset ){    
          mapCourseAsset.put(assetObj.University_Course__c , assetObj.id);
          Course_Product_in_Use__c coursePIUObj = new Course_Product_in_Use__c();
          coursePIUObj.Course__c = assetObj.University_Course__c;
          coursePIUObj.Product_in_Use__c = assetObj.id;
          coursePIUObj.Status__c = 'Active';
          lstCoursePIU.add(coursePIUObj);
        } 
        List<UniversityCourseContact__c> courseContactLst = [select id, Contact__c,Contact_Role__c,PS_Contact_Role__c, UniversityCourse__c,Active__c from UniversityCourseContact__c where UniversityCourse__c in :mapCourseAsset.keySet() AND Active__c =True ];
        for(UniversityCourseContact__c courseContobj : courseContactLst ){
          if(mapCourseAsset.containsKey(courseContobj.UniversityCourse__c)){
              Contact_Product_In_Use__c contactPIUObj = new Contact_Product_In_Use__c();
              contactPIUObj.Contact__c = courseContobj.Contact__c;
              contactPIUObj.Role__c = courseContobj.PS_Contact_Role__c;
              contactPIUObj.Status__c = 'Active';
              contactPIUObj.Product_in_Use__c = mapCourseAsset.get(courseContobj.UniversityCourse__c);
              lstContactPIU.add(contactPIUObj);
            }
       
      
       }
      
       if(!lstContactPIU.isEmpty()){
            Database.SaveResult[] lstResult= Database.insert(lstContactPIU, false);
          
       }
       if(!lstCoursePIU.isEmpty()){
            Database.SaveResult[] lstResult= Database.insert(lstCoursePIU, false);
          
       }
  }
  
  public void syncStatusWithCourseContactPIU(List<Asset> lstNewAsset,Map<ID,sobject> oldMap)
  {
      Map<Id,String> mapWithAssetStatus = new Map<Id,String>();
      for(Asset newAsset : lstNewAsset)
      {
          Asset oldAsset = new Asset();
          oldAsset = (Asset)oldMap.get(newAsset.Id);
          if(newAsset.Status__c != oldAsset.Status__c)
          {
              mapWithAssetStatus.put(newAsset.Id,newAsset.Status__c);
          }    
      }
      if(mapWithAssetStatus.size() > 0)
      {
          List<Course_Product_in_Use__c> lstWithCourseAsset = new List<Course_Product_in_Use__c>();
          for(Course_Product_in_Use__c courseProdInUse : [select id,Status__c,Product_in_Use__c from Course_Product_in_Use__c where Product_in_Use__c IN : mapWithAssetStatus.keyset() limit 1000])
          {
              courseProdInUse.Status__c = mapWithAssetStatus.get(courseProdInUse.Product_in_Use__c);
              lstWithCourseAsset.add(courseProdInUse); 
          }
          
          List<Contact_Product_In_Use__c> lstWithContactAsset = new List<Contact_Product_In_Use__c>();
          for(Contact_Product_In_Use__c contactProdInUse : [select id,Status__c,Product_in_Use__c from Contact_Product_In_Use__c where Product_in_Use__c IN : mapWithAssetStatus.keyset() limit 1000])
          {
              contactProdInUse.Status__c = mapWithAssetStatus.get(contactProdInUse.Product_in_Use__c);
              lstWithContactAsset.add(contactProdInUse);
          }
          
          if(lstWithCourseAsset.size() > 0)
          {
              database.update(lstWithCourseAsset,false);
          }
          
          if(lstWithContactAsset.size() > 0)
          {
              database.update(lstWithContactAsset,false);
          }
      }
  }
  
 }