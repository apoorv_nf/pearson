/**
 * Apex Class Name : EinsteinBotReportingBatchTest
 * Version         : 1.0
 * Created On      : 12-07-2018
 * Function        : A test class for EinsteinBotReportingBatch and EinsteinBotReportingBatchHelper
 */

@IsTest
public class EinsteinBotReportingBatchTest {
    @testsetup static  void data(){

        User sysAdmn = [Select Id FROM User where Profile.Name='System Administrator' AND IsActive = TRUE Limit 1];
        System.runAs(sysAdmn) {
            Bypass_Settings__c triggerSwitch = new Bypass_Settings__c(disable_triggers__c=true) ;
            insert triggerSwitch;
            System.debug(triggerSwitch);
            BusinessHours bussiHour = [SELECT id FROM BusinessHours WHERE IsDefault=true LIMIT 1];
            Case cas = new Case(BusinessHoursId =bussiHour.Id);
            insert cas;

            LiveChatVisitor vistor = new LiveChatVisitor();
            insert vistor;

            LiveChatTranscript transcript = new LiveChatTranscript(
                    CaseId = cas.Id,
                    LiveChatVisitorId = vistor.Id,
                    StartTime = System.now(),
                    EndTime = System.now().addMinutes(3),
                    LiveChatButtonId = [SELECT Id FROM LiveChatButton WHERE DeveloperName = 'NAUS_HETS_Chatbot_ACR'].Id,
                    ChatKey = 'abc');
            insert transcript;
            LiveChatTranscript transcript1 = new LiveChatTranscript(
                    CaseId = cas.Id,
                    LiveChatVisitorId = vistor.Id,
                    StartTime = System.now(),
                    EndTime = System.now().addMinutes(3),
                    LiveChatButtonId = [SELECT Id FROM LiveChatButton WHERE DeveloperName = 'NAUS_HETS_Chatbot_ACR'].Id,
                    ChatKey = 'abc');
            insert transcript1;
            List<Einstein_Bot_Chat_Log__c> einsteinChatLogList = new List<Einstein_Bot_Chat_Log__c>();
            Einstein_Bot_Chat_Log__c einsteinChatLog1 = new Einstein_Bot_Chat_Log__c(Topic_Name__c='Access Code',Business_Metric_Type__c = 'Start',Live_Agent_Session_Id__c='abc',Live_Chat_Transcript__c=transcript.Id);
            einsteinChatLogList.add(einsteinChatLog1);
            Einstein_Bot_Chat_Log__c einsteinChatLog2 = new Einstein_Bot_Chat_Log__c(Successful_Resolution__c='Yes',Live_Agent_Session_Id__c='abc',Live_Chat_Transcript__c = transcript.Id);
            einsteinChatLogList.add(einsteinChatLog2);
            Einstein_Bot_Chat_Log__c einsteinChatLog3 = new Einstein_Bot_Chat_Log__c(Topic_Name__c='Access Code',Business_Metric_Type__c = 'End',Live_Agent_Session_Id__c='abc',Live_Chat_Transcript__c=transcript.Id);
            einsteinChatLogList.add(einsteinChatLog3);
            insert einsteinChatLogList;

            List<LiveChatTranscriptEvent> transcriptEventsList = new List<LiveChatTranscriptEvent>();

            LiveChatTranscriptEvent transcriptEvent4 = new LiveChatTranscriptEvent(
                    Type = 'AlertCriticalWaitChat',
                    LiveChatTranscriptId = transcript.Id,
                    Time = System.now().addMinutes(4)
            );
            transcriptEventsList.add(transcriptEvent4);
            LiveChatTranscriptEvent transcriptEvent3 = new LiveChatTranscriptEvent(
                    Type = 'TransferredToButton',
                    LiveChatTranscriptId = transcript.Id,
                    Time = System.now().addMinutes(1)
            );
            transcriptEventsList.add(transcriptEvent3);
            LiveChatTranscriptEvent transcriptEvent2 = new LiveChatTranscriptEvent(
                    Type = 'PushAssignment',
                    LiveChatTranscriptId = transcript.Id,
                    Time = System.now().addSeconds(2)
            );
            LiveChatTranscriptEvent transcriptEvent21 = new LiveChatTranscriptEvent(
                    Type = 'PushAssignment',
                    LiveChatTranscriptId = transcript1.Id,
                    Time = System.now().addSeconds(2)
            );
            transcriptEventsList.add(transcriptEvent21);
            transcriptEventsList.add(transcriptEvent2);

            LiveChatTranscriptEvent transcriptEvent22 = new LiveChatTranscriptEvent(
                    Type = 'Transfer',
                    LiveChatTranscriptId = transcript.Id,
                    Time = System.now().addSeconds(3)
            );
            transcriptEventsList.add(transcriptEvent22);
            LiveChatTranscriptEvent transcriptEvent1 = new LiveChatTranscriptEvent(
                    Type = 'LeaveVisitor',
                    LiveChatTranscriptId = transcript.Id,
                    Time = System.now().addSeconds(6)
            );
            transcriptEventsList.add(transcriptEvent1);
            LiveChatTranscriptEvent transcriptEvent01 = new LiveChatTranscriptEvent(
                    Type = 'ChatRequest',
                    LiveChatTranscriptId = transcript.Id,
                    Time = System.now().addSeconds(0)
            );
            transcriptEventsList.add(transcriptEvent01);
            insert transcriptEventsList;
        }
    }
    @IsTest 
    static void calculate_wait_time() {
        Test.startTest();
        EinsteinBotReportingBatch batchObj = new EinsteinBotReportingBatch('NAUS_HETS_Chatbot_ACR');
        Id batchId = Database.executeBatch(batchObj,200);
        Test.stopTest();
        LiveChatTranscript[] transcript = [SELECT Einstein_Analytics_Reporting_Process__c FROM LiveChatTranscript];
        System.assert(true,transcript[0].Einstein_Analytics_Reporting_Process__c);
    }
}