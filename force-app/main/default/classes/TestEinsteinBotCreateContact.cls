@isTest
private class TestEinsteinBotCreateContact {

	@isTest
	static void it_should_create_contact_when_an_account_already_exists() {
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];

		Account acc = new Account(
			Name='testAccount',
			BillingStreet = 'Street1',
            BillingCity = 'Sydney',
			BillingPostalCode = 'ABC DEF',
			BillingCountry = 'Australia');

		insert acc;

		EinsteinBotCreateContact.ContactCreationRequest request = new EinsteinBotCreateContact.ContactCreationRequest();
		request.role = 'Student';
		request.email = 'test@test.com';
		request.firstName = 'firstName';
		request.lastName = 'lastName';
		request.institutionName = 'testAccount';

		Test.startTest();
        System.runAs(automatedProcess) {
			EinsteinBotCreateContact.createContact(new List<EinsteinBotCreateContact.ContactCreationRequest>{request});
			Test.getEventBus().deliver();
        }
		Test.stopTest();

		System.assertEquals(1, [
			SELECT count()
			FROM Contact
			WHERE Email = :request.email
		]);
	}

	@isTest
	static void it_should_create_a_contact_when_no_account_exists() {

	}
}