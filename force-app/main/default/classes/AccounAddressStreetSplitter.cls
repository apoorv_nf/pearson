/**

* Controller : AccounAddressStreetSplitter
* Description: Controller class  To Split Account Street To Address1,Address2,Address3,Address4/

**/  
/* --------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            AccounAddressStreetSplitter.Cls
Description:      
Date             Version           Author                                Summary of Changes 
-----------      ----------   -----------------      ---------------------------------------------------------------------------------------
9/17/2016         0.1          Kyama                             Class To Split Account Street.
---------------------------------------------------------------------------------------------------------------------------------------------------*/

global class AccounAddressStreetSplitter{ 
    
    global class inputVariables { 
        @InvocableVariable(required=true) 
        public List<Account_Addresses__c> accAddr;
    } 
    
    @InvocableMethod(label='Split Account Street' description='Returns the street split into array of strings') 
    public static List<Account_Addresses__c> splitAccountStreet(List<Account_Addresses__c> accAddr) { 
        
        List<Account_Addresses__c> accAddroutput = new List<Account_Addresses__c>();                             
        for(Account_Addresses__c objAccAddr: accAddr){
            string[] streetlist;
            streetlist = RetrieveOneCRMHandler.splitStreetAddress(objAccAddr.AccountStreet__c);
            objAccAddr.Address_1__c= (streetlist.size()>=1) ? streetlist[0] : null;
            objAccAddr.Address_2__c= (streetlist.size()>=2) ? streetlist[1] : null;
            objAccAddr.Address_3__c= (streetlist.size()>=3) ? streetlist[2] : null;
            objAccAddr.Address_4__c= (streetlist.size()>=4) ? streetlist[3] : null;  
            objAccAddr.Source__c = 'One CRM';    
            accAddroutput.add(objAccAddr);                  
        }
        return accAddroutput;
    }//method end
}