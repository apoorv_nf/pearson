@isTest
public class PS_CreateMassCampaignSampleCtrlTest {
    
    public static testmethod void unitTest(){
        
        account acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'CA', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
        insert acc;
        set<id> accids = new set<id>();
        accids.add(acc.Id);
        
        List<Account_Addresses__c> addtoInsert = new List<Account_Addresses__c>();
        Account_Addresses__c accadd = new Account_Addresses__c(Name='Test',Account__c=acc.id,Address_1__c='Test address',Source__c='One CRM',Country__c = 'India', State__c = 'Karnataka', City__c = 'Bangalore', AccountStreet__c = 'BNG1', Zip_Code__c = '5600371', Primary__c =true);
        addtoInsert.add(accadd);
        Account_Addresses__c accadd1 = new Account_Addresses__c(Name='Test',Account__c=acc.id,Address_1__c='Test address',Source__c='One CRM',Country__c = 'India', State__c = 'Karnataka', City__c = 'Bangalore', AccountStreet__c = 'BNG1', Zip_Code__c = '5600371',PrimaryShipping__c=true);
        addtoInsert.add(accadd1);
        insert addtoInsert;
        
        Account acctoupdate = new Account();
        acctoupdate.Id = acc.Id;
        acctoupdate.Billing_Address_Lookup__c = accadd.Id;
        acctoupdate.Shipping_Address_Lookup__c = accadd1.Id;
        update acctoupdate;
      
        //AccountAddressesUpdateonAccount accaddclass = new AccountAddressesUpdateonAccount();
        //  AccountAddressesUpdateonAccount.afterInsertOrUpdateAccountAddresses(accids);
        
        List<Contact> lstContacttoInsert = new List<Contact>();
        Contact con = new Contact(FirstName='TestContactFirstname1', LastName='TestContactLastname',AccountId=acc.id ,Do_Not_Send_Samples__c=False,Global_Marketing_Unsubscribe__c=False,Salutation='MR.', Email='test@gmail.com',MobilePhone='111222333' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English',Preferred_Address__c = 'Other Address');  
        lstContacttoInsert.add(con);
        //Contact con1 = new Contact(FirstName='TestContactFirstname2', LastName='TestContactLastname2',AccountId=acc.Id,Do_Not_Send_Samples__c=False,Global_Marketing_Unsubscribe__c=False,Salutation='MR.', Email='test2@gmail.com',MobilePhone='324526786' , OtherCountry  = 'India' , OtherStreet = 'Test1',OtherCity  = 'Test1' , OtherPostalCode  = '654321',First_Language__c='English',Preferred_Address__c = 'Other Address');  
        //lstContacttoInsert.add(con1);
        insert lstContacttoInsert;
        
        List<RecordType> rt1 = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Campaign' AND Name = 'Sampling Campaign' LIMIT 1];  
        Campaign campobj = new Campaign(Name='Test',Campaign_Objectives__c='test1',RecordTypeId= rt1[0].Id,status='In Setup',Type='Mass Campaign',StartDate = system.today(),EndDate = system.today(),Line_of_Business__c = 'Schools');
        insert campobj;
        
        List<CampaignMember> lstcampmembtoInsert = new List<CampaignMember>();
        CampaignMember campmem = new CampaignMember(CampaignId=campobj.id,Contactid=con.id,Order_Status__c='Sent');
        lstcampmembtoInsert.add(campmem);
       // CampaignMember campmem1 = new CampaignMember(CampaignId=campobj.id,Contactid=con1.id,Order_Status__c='Sent');
       // lstcampmembtoInsert.add(campmem1);
        insert lstcampmembtoInsert;
        
        Product2 prod = new Product2();
        prod.Configuration_Type__c='Option'; 
        prod.name = 'NA Test Product5';
        prod.Duration__c = 'D20';
        prod.Market__c ='CA';
        prod.Business_Unit__c='Higher Ed';
        prod.Line_of_Business__c = 'Higher Ed';           
        prod.Author__c = 'Test Author'; 
        prod.Edition__c = '10';
        prod.Medium2__c = 'Print';
        prod.Relevance_Value__c = 10;
        prod.Brand__c = null;    
        prod.Binding__c = 'Cloth';
        insert Prod;
        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true); 
        insert customPB;
        PricebookEntry pbe2 =new PricebookEntry(Product2Id=Prod.Id,Pricebook2Id=pricebookId,UnitPrice = 100.00,isActive=true,UseStandardPrice = false);
        insert pbe2;
        
        Campaign_Product__c camprod = new Campaign_Product__c(Name='Test',Campaign__c=campobj.id,Product__c=Prod.id);
        ApexPages.currentPage().getParameters().put('Id',campobj.id);
        List<User> users = [Select Id from User where Name = :Label.Deployment_User];
        
        /*PermissionSetAssignment psPearsonManageOrders = new PermissionSetAssignment();
        psPearsonManageOrders.AssigneeId = users[0].ID;
        psPearsonManageOrders.PermissionSetId = '0PSb0000000Hd9l';
        insert psPearsonManageOrders;
        */
        users[0].Market__c = 'US';
        users[0].License_Pools__c ='Test User';
        update users;
         Bypass_Settings__c byp = new Bypass_Settings__C(SetupOwnerId = users[0].id,Disable_Triggers__c=true,
                                                         Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true); 
               insert byp;
           System.runAs(users[0]){
        Order orderObj = new Order();
        orderObj.Status= 'Open';
        orderObj.AccountId= acc.id;
        orderObj.ShippingCountry='United States';
        orderObj.ShippingCity='Fort Smith';
        orderObj.ShippingCountryCode='US';
        orderObj.ShippingStreet='5210 Grand Avenue';
        orderObj.ShippingState='Arkansas';
        orderObj.ShippingStateCode='AR';
        orderObj.ShippingPostalCode='72904';
        orderObj.EffectiveDate= System.Today();
        orderObj.ERP_Order_Type__c = 'HE-US-SAMPLE';
        insert orderObj; 
        
       // User users1 = [Select Id from User where Name = 'Ramesh veera' limit 1];
       //Profile profileId = [select id from profile where Name = 'System Administrator'];
        //List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
  
        //Bypass_Settings__c byp = new Bypass_Settings__C(SetupOwnerId=Users[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true); 
         //      insert byp;
     //hh   System.runAs(users[0]){
            PS_CreateMassCampaignSampleController obj = new PS_CreateMassCampaignSampleController();
            obj.errorOccured=true;
            obj.CreateOrders();
            //obj.PS_CreateMassCampaignSampleController();
            obj.errorOccured=false;
            obj.Cancel();
        } 
    }
}