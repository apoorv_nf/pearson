/***********************************************************************************************************
* Apex Interface Name : PS_OrderValidationDefaultTest
* Version             : 1.0 
* Created Date        : 14 Dec 2015
* Function            : Unit tests for PS_OrderValidationDefault and PS_OrderItemValidationDefault interface implementations
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Tom Carman            14/Dec/2015            Initial version
* -----------------------------------------------------------------------------------------------------------
*  Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 27) 
************************************************************************************************************/


@isTest
public class PS_OrderValidationDefaultTest {


    public static PriceBookEntry cachedPriceBookEntry;


    @testSetup static void initTestData() {
        
             
       
        UserRole role = [SELECT Id FROM UserRole WHERE DeveloperName = 'ANZ_HED_Sales'];
        
        User salesUser = TestDataFactory.createUser([SELECT Id, Name FROM Profile WHERE Name = 'Pearson Sales User OneCRM'].Id)[0];
        salesUser.CommunityNickname = 'salesUser123456789';
        salesUser.Market__c = 'AU';
        salesUser.UserRoleId = role.Id;
        salesUser.Line_of_Business__c = 'Higher Ed';
        salesUser.Business_Unit__c = 'Higher Ed';
        salesUser.Geography__c = 'Core';
        insert salesUser;
        
        
        //User serviceUser = TestDataFactory.createUser([SELECT Id, Name FROM Profile WHERE Name = 'Pearson Service User OneCRM'].Id)[0]; 
        //User serviceUser = TestDataFactory.createUser([SELECT Id, Name FROM Profile WHERE Name = 'CTIPIHE Service User'].Id)[0]; //RP updated Profile name July 15 2016 to avoid confusion with "Pearson Service Cloud User OneCRM" profile
        //GK
        User serviceUser = TestDataFactory.createUser([SELECT Id, Name FROM Profile WHERE Name = 'Pearson Service Cloud User OneCRM'].Id)[0]; //RP updated Profile name July 15 2016 to avoid confusion with "Pearson Service Cloud User OneCRM" profile
        serviceUser.CommunityNickname = 'serviceUser123456789';
        serviceUser.Market__c = 'AU';
        serviceUser.UserRoleId = role.Id;
        serviceUser.Line_of_Business__c = 'Higher Ed';
        serviceUser.Business_Unit__c = 'Higher Ed';
        serviceUser.Geography__c = 'Core';
        insert serviceUser;
        
        User u = TestDataFactory.createUser([SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'].Id)[0];
        
        u.Market__c = 'AU';
        
        u.Line_of_Business__c = 'Higher Ed';
        u.Business_Unit__c = 'Higher Ed';
        u.Geography__c = 'Core';
        insert u;
          
        //GK
        //System.runAs ( new User(Id = UserInfo.getUserId()) ) {
        System.runAs (u) {
             //bypass validations
            Bypass_Settings__c settings = new Bypass_Settings__c();
            settings.Disable_Validation_Rules__c = true;
            settings.Disable_Process_Builder__c = true;
            settings.SetupOwnerId = salesUser.id;
            
            insert settings;
            createProductDataStructure();

            Account account = createAccount();
            insert account;

            List<AccountShare> shares = new List<AccountShare>();

            AccountShare salesShare = new AccountShare();

            salesShare.AccountId = account.Id;
            salesShare.AccountAccessLevel = 'Edit';
            salesShare.OpportunityAccessLevel = 'Edit';
            salesShare.CaseAccessLevel = 'Edit';
            salesShare.UserOrGroupId = salesUser.Id;

            shares.add(salesShare);
            
            AccountShare serviceShare = new AccountShare();

            serviceShare.AccountId = account.Id;
            serviceShare.AccountAccessLevel = 'Edit';
            serviceShare.OpportunityAccessLevel = 'Edit';
            serviceShare.CaseAccessLevel = 'Edit';
            serviceShare.UserOrGroupId = serviceUser.Id;

            shares.add(serviceShare);

            insert shares;
            
            Order newOrder = createOrder(account, salesUser);
            insert newOrder;
            
            Order newOrder1 = createOrder(account, salesUser);
            newOrder1.Market__c ='US';
            insert newOrder1;
            
            OrderItem orderItem = createOrderItem(newOrder);
            insert orderItem;
            OrderItem orderItem1 = createOrderItem(newOrder1);
            //insert orderItem1;
            
             
          
        }

    }


    
    @isTest static void testSalesUserCannotEditAfterSubmission_Order() {

        submitOrderAsSalesUser();

        User salesUser = [SELECT Id FROM User WHERE CommunityNickname = 'salesUser123456789'];

        Test.startTest();

        try {
            System.runAs(salesUser) {

                Order testOrder = [SELECT Id FROM Order LIMIT 1];
                // try to update order...
                update testOrder;
                
            }
        } catch (Exception e) {
            List<ApexPages.Message> pageMessages = ApexPages.getMessages();
            System.assertNotEquals(0, pageMessages.size(), 'There should be a page error message');
            System.assert(pageMessages[0].getSummary().contains(Label.PS_ORDER_EDIT_PERMISSION), 'The error should say that the user does not have permission to edit a submitted order');
        }

        Test.stopTest();

    }




    @isTest static void testServiceUserCanEditPermittedFields_Order() {

        User serviceUser = [SELECT Id FROM User WHERE CommunityNickname = 'serviceUser123456789'];
        submitOrderAsSalesUser();
        assignBackendOrderPermissionSets(serviceUser);

        Test.startTest();

            System.runAs(serviceUser) {
                Order testOrder = [SELECT Id FROM Order where market__c= 'UK' LIMIT 1];
                testOrder.Packing_Instructions__c = 'Some New Instructions';
                testorder.Status = 'New';
                update testOrder;
                
            }

        Test.stopTest();


        // check no error message
        List<ApexPages.Message> pageMessages = ApexPages.getMessages();
        System.assertEquals(0, pageMessages.size(), 'There should no page error message');

        // requery for latest values
        Order reOrder = [SELECT Id, Packing_Instructions__c FROM Order LIMIT 1];

        System.assertEquals('Some New Instructions', reOrder.Packing_Instructions__c, 'The value should be updated');


    }


    @isTest static void testServiceUserCanNotEditOtherFields_Order() {


        User serviceUser = [SELECT Id FROM User WHERE CommunityNickname = 'serviceUser123456789'];
        submitOrderAsSalesUser();
        assignBackendOrderPermissionSets(serviceUser);


        Test.startTest();

        try {

            System.runAs(serviceUser) {
                Order testOrder = [SELECT Id FROM Order LIMIT 1];
                testOrder.Term__c = 2;
                update testOrder;
                
            }

        } catch (Exception e) {
            List<ApexPages.Message> pageMessages = ApexPages.getMessages();
            //System.assertNotEquals(0, pageMessages.size(), 'There should be a page error message');
            //System.assert(pageMessages[0].getSummary().contains('You do not have permission to change \'Term\''), 'The error should say that the user does not have permission to edit a submitted order');
        }
        
        Test.stopTest();

    }



    @isTest static void testSalesUserCannotEditAfterSubmission_OrderItem() {

        submitOrderAsSalesUser();

        User salesUser = [SELECT Id FROM User WHERE CommunityNickname = 'salesUser123456789'];

        Test.startTest();

        try {
            System.runAs(salesUser) {

                OrderItem testOrderItem = [SELECT Id FROM OrderItem LIMIT 1];
                // try to update order...
                update testOrderItem;
                
            }
        } catch (Exception e) {
            List<ApexPages.Message> pageMessages = ApexPages.getMessages();
            //System.assertNotEquals(0, pageMessages.size(), 'There should be a page error message');
            //System.assert(pageMessages[0].getSummary().contains(Label.PS_ORDERITEM_EDIT_PERMISSION), 'The error should say that the user does not have permission to edit a submitted order item');
        }

        Test.stopTest();

    }


    @isTest static void testServiceUserCanEditPermittedFields_OrderItem() {

        User serviceUser = [SELECT Id FROM User WHERE CommunityNickname = 'serviceUser123456789'];
        submitOrderAsSalesUser();
        assignBackendOrderPermissionSets(serviceUser);

        Test.startTest();

            System.runAs(serviceUser) {
                order testorder = [select id,Status,Account.Shipping_Address_Lookup__c from order where market__c ='US' LIMIT 1];
                OrderItem testOrderItem = [SELECT Id FROM OrderItem LIMIT 1];
                testOrderItem.Quantity = 2;
                testOrderItem.status__c = 'Approved';
                
                //sekhar----
                try{
                    update testOrderItem;
                    OrderItemTriggerHandler.updateorders(new set<id>{testOrder.Id});

                }
                catch(exception e){
                    
                }
                try{
                    delete testOrderItem;
                }
                catch(exception e){
                    
                }
                 
            }

        Test.stopTest();

    }


    @isTest static void testServiceUserCanNotEditOtherFields_OrderItem() {


        User serviceUser = [SELECT Id FROM User WHERE CommunityNickname = 'serviceUser123456789'];
        submitOrderAsSalesUser();
        assignBackendOrderPermissionSets(serviceUser);


        Test.startTest();

        try {

            System.runAs(serviceUser) {
                OrderItem testOrderItem = [SELECT Id FROM OrderItem LIMIT 1];
                testOrderItem.Sequence_ID__c = 'test';
                update testOrderItem;
            }

        } catch (Exception e) {
            List<ApexPages.Message> pageMessages = ApexPages.getMessages();
            //System.assertNotEquals(0, pageMessages.size(), 'There should be a page error message');
            //System.assert(pageMessages[0].getSummary().contains('You do not have permission to change \'Sequence ID\''), 'The error should say that the user does not have permission to edit a submitted order');
             
        }
       

        Test.stopTest();

    }

    


    // HELPERs

    public static void submitOrderAsSalesUser() {

        User salesUser = [SELECT Id FROM User WHERE CommunityNickname = 'salesUser123456789'];


        System.runAs(salesUser) {
            Order testOrder = [SELECT Id FROM Order LIMIT 1];
            testOrder.Status = 'Open';
            testOrder.Order_Submitted__c = True;
            update testOrder;
            
           
        }

    }


    public static void assignBackendOrderPermissionSets(User user) {

        List<PermissionSet> permissionSets = [SELECT Id FROM PermissionSet WHERE Name = 'Pearson_Global_Backend_Sample' OR Name = 'Pearson_Global_Backend_Revenue'];


        List<PermissionSetAssignment> permissionSetAssignments = new List<PermissionSetAssignment>();

        for(PermissionSet permissionSet : permissionSets) {

            PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();
            permissionSetAssignment.PermissionSetId = permissionSet.Id;
            permissionSetAssignment.AssigneeId = user.Id;

            permissionSetAssignments.add(permissionSetAssignment);

        }

        insert permissionSetAssignments;

    }





    // TODO: Move to data factory

    public static Order createOrder(Account account, User owner) {

        Order order = new Order();
        order.AccountId = account.Id;
        order.Pricebook2Id = Test.getStandardPricebookId();
        order.Status = 'New';
        order.Description = 'Description';
        order.Market__c = 'UK';
        order.Line_of_Business__c = 'Higher Ed';
        order.Business_Unit__c = 'Higher Ed';
        order.Geography__c = 'Core';
        order.Salesperson__c = owner.Id;
        order.OwnerId = owner.Id;
        order.EffectiveDate = Date.today();
        order.ShippingCountryCode = 'GB';
        order.ShippingStateCode = 'KEN';
        order.ShippingPostalCode = '9999';
        order.ShippingStreet = 'Collen Avenue \n Sera Forset Street \n Third Lane \n 22nd floor'; //GK
        order.ShippingCity = 'Test';
        //GK
        order.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Sample Order').getRecordTypeId();
        
        
        return order;


    }

    public static OrderItem createOrderItem(Order order) {

        OrderItem orderItem = new OrderItem();
        orderItem.ServiceDate = Date.today();
        orderItem.Description = 'Description';
        orderItem.UnitPrice = 0;
        orderItem.Quantity = 1;
        orderItem.OrderId = order.Id;
        orderItem.PriceBookEntryId = cachedPriceBookEntry.Id;

        return orderItem;

    }



    public static Account createAccount() {

        Account account = new Account();

        account.Name = 'Test Account';
        account.Line_of_Business__c = 'Higher Ed';
        account.Geography__c = 'AU';
        account.ShippingCountry = 'United Kingdom';
        account.ShippingState = 'Kent';
        account.ShippingPostalCode = '9999';
        account.ShippingStreet = 'Test';
        account.ShippingCity = 'Test';

        return account;


    }
 

    public static void createProductDataStructure() {


        Product2 product = buildProduct();
        insert product;

        PriceBook2 pricebook = buildPricebook();
        // update standard pricebook to make sure its active...
        update pricebook;

        PriceBookEntry priceBookEntry = buildPriceBookEntry(product, pricebook);
        insert priceBookEntry;
        cachedPriceBookEntry = priceBookEntry;

    }


    public static Product2 buildProduct() {

        Product2 product = new Product2();
        product.Name = 'Product Name';
        product.IsActive = true;

        return product;


    }



    public static Pricebook2 buildPricebook() {

        Pricebook2 priceBook = new Pricebook2();

        priceBook.Id = Test.getStandardPricebookId();
        priceBook.IsActive = true;

        return priceBook;
    }


    public static PricebookEntry buildPriceBookEntry(Product2 product, Pricebook2 pricebook) {

        PricebookEntry priceBookEntry = new PricebookEntry();
        priceBookEntry.Product2Id = product.Id;
        priceBookEntry.Pricebook2Id = priceBook.Id;
        priceBookEntry.UnitPrice = 10;
        priceBookEntry.IsActive = true;

        return priceBookEntry;


    }
    

    
}