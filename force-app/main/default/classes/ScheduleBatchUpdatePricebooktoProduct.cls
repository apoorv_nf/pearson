global class ScheduleBatchUpdatePricebooktoProduct implements Schedulable{
    
    global void execute(SchedulableContext sc)
    {
        database.executebatch(new BatchUpdatePricebooktoProduct(),30);
    }

}