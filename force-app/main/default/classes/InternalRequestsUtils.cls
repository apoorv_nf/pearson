/*******************************************************************************************************************
* Apex Class Name   : InternalRequestsUtils.cls 
* Version           : 1.0 
* Created Date      : 17 JULY 2016
* Function          : Utility Class for Internal Request Object
* Modification Log  :
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*Brandon Young - 17 July 2015 - Created Class
*Kyama- 24 November 2015-Modified For Finanace Workflow Defects D-2588.
* Karan Khanna - 23 Dec 2015 - Added check for DeveloperName in SOQL as there are 3 record types currently for this object and without specifying recordtype details
*                              any record type can be returned through SOQL
*Kyama- 24 November 2015-Modified For Finanace Workflow Defects D-2588.
*******************************************************************************************************************/

public with sharing class InternalRequestsUtils
{
     
     public Id getRecordTypeId(){
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Internal_Request__c' AND DeveloperName = 'Sales_Crediting_Territory_Management' LIMIT 1];
        Schema.DescribeSObjectResult d = Schema.SObjectType.Internal_Request__c; 
        Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
        Schema.RecordTypeInfo rtById =  rtMapById.get(rt.id);
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get(rt.name);
        
        
        Id rtId = rtByName.getRecordTypeId();
        String rtName = rtByName.getName();

        return rtId;
      }

     public String getRecordTypeName(){
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Internal_Request__c' AND DeveloperName = 'Sales_Crediting_Territory_Management' LIMIT 1];
        Schema.DescribeSObjectResult d = Schema.SObjectType.Internal_Request__c; 
        Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
        Schema.RecordTypeInfo rtById =  rtMapById.get(rt.id);
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get(rt.name);
       
        
        Id rtId = rtByName.getRecordTypeId();
        String rtName = rtByName.getName();

        return rtName;
      }

    // Update Owner with Salesperson Record
    // Applies only to Finacial Workflow Recordtype
    // Recordtype Name Stored in Custom Label
    public list<Internal_Request__c> updateRecords(Internal_Request__c[] lstOldInternalRequests, map<ID, Internal_Request__c> mapIDInternalRequest)
    {
        //String rtName = getRecordTypeName();
        Id rtId = getRecordTypeId();


        for(Internal_Request__c sOldInternalRequest : lstOldInternalRequests)
        {

            Internal_Request__c sOldInternalRequest1 = mapIDInternalRequest.get(sOldInternalRequest.Id);
            // Update the OwnerId with the Sales Person Name if the Recordtype = Financial Workflow
            //if(rtId == sOldInternalRequest.RecordTypeId && rtName == 'Sales Crediting & Territory Management' ){
              if(rtId == sOldInternalRequest.RecordTypeId){
                sOldInternalRequest.OwnerId =   sOldInternalRequest1.Sales_Person_Name__c ;
            }
            else{// do nothing
            }
        }
        
       return lstOldInternalRequests;
    }
    
    // After Update/Insert Logic to give IR Record access to owner Manager/Manager.Manager
    // Applies only to Finacial Workflow Recordtype
    public list<Internal_Request__Share> insertRecords(Internal_Request__c[] lstNewInternalRequests)
    {  
        String rtName = getRecordTypeName();
        Id rtId = getRecordTypeId();
        List<id> IROwners = new List<id>();
        List<Internal_Request__Share> irsl = new List<Internal_Request__Share>();
        for(Internal_Request__c sOldInternalRequest : lstNewInternalRequests)
        {
           if(rtId == sOldInternalRequest.RecordTypeId && rtName == 'Sales Crediting & Territory Management' ){
           IROwners.add(sOldInternalRequest.OwnerId); 
           }          
        }
        Map<Id,User> UserMap = new Map<Id,User>([select id, ManagerId, Manager.ManagerId, Manager.Manager.UserRole.Name from User where id in:IROwners]);
        for(Internal_Request__c sOldInternalRequest1 : lstNewInternalRequests)
         {
           if(rtId == sOldInternalRequest1.RecordTypeId && rtName == 'Sales Crediting & Territory Management' ){
               if(UserMap.get(sOldInternalRequest1.OwnerId).ManagerId != null){
                        Internal_Request__Share internalRequestShare = new Internal_Request__Share();
                        internalRequestShare.ParentId = sOldInternalRequest1.Id;
                        internalRequestShare.AccessLevel='Edit';
                        internalRequestShare.UserOrGroupId = UserMap.get(sOldInternalRequest1.OwnerId).ManagerId;
                        irsl.add(internalRequestShare);
            
                if(UserMap.get(sOldInternalRequest1.OwnerId).Manager.ManagerId != null){
                        Internal_Request__Share internalRequestShare1 = new Internal_Request__Share();
                        internalRequestShare1.ParentId = sOldInternalRequest1.Id;
                        internalRequestShare1.AccessLevel='Edit';
                        internalRequestShare1.UserOrGroupId = UserMap.get(sOldInternalRequest1.OwnerId).Manager.ManagerId;
                        irsl.add(internalRequestShare1);
                }
             }
          }     
       }
       
       return irsl;
    } 
}