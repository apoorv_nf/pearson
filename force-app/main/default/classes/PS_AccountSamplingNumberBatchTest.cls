@isTest
//class for covering the test class D - 03/11/2015
private class PS_AccountSamplingNumberBatchTest
{
    @isTest 
    static void myAccountBatchTest(){
      Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
     
      //code for creating an Userc
      User u = new User();
      u.LastName = 'territoryuser';
      u.alias = 'terrusr1'; 
      u.Email = 'territoryuser121@pearson.com';  
      u.Username='territoryuser171@gmail.com';
      u.LanguageLocaleKey='en_US'; 
      u.TimeZoneSidKey='America/New_York';
      u.Price_List__c='US HE All';
      u.LocaleSidKey='en_US';
      u.EmailEncodingKey='ISO-8859-1';
      u.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
      u.Geography__c = 'Growth';
      u.CurrencyIsoCode='USD';
      u.Line_of_Business__c = 'Higher Ed';
      u.Market__c = 'US';
      u.Business_Unit__c = 'US Field Sales';
        u.License_Pools__c = 'Test User';
      insert u; 
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u.id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true,Disable_Workflow_Rules__c=true); 
               insert byp;
      system.runas(u){
          test.startTest();
          //Get the record type ID using the recType parameter. recType will be the Record type name.
          List<RecordType> rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Organisation' LIMIT 1];  
          //Account set up 
          Account acc1 = new Account();
          Account acc2 = new Account();
          Account acc3 = new Account();
          acc1.Name='Test Account1'; 
          acc1.Organisation_Type__c ='School';
          acc1.Phone='+9100000'; 
          acc1.ShippingCountry = 'India'; 
          acc1.ShippingCity = 'Bangalore1';
          acc1.ShippingStreet = 'BNG1';
          acc1.BillingStreet = 'Sholinganallur';
          acc1.BillingCity = 'Chennai';
          acc1.BillingCountry ='India';
          acc1.ShippingPostalCode = '560031';
          acc1.market2__c = 'US';
          acc1.Primary_Selling_Account_check__c = true;
          acc1.Sampling_Account_Number__c='';
          acc1.RecordtypeId =  rt[0].id;
          insert acc1;
          
          acc2.Name='Test Account1'; 
          acc2.Organisation_Type__c ='School';
          acc2.Phone='+9100000';
          acc2.ShippingCountry = 'India'; 
          acc2.ShippingCity = 'Bangalore1';
          acc2.ShippingStreet = 'BNG1';
          acc2.BillingStreet = 'Sholinganallur';
          acc2.BillingCity = 'Chennai';
          acc2.BillingCountry ='India';
          acc2.ShippingPostalCode = '560031';
          acc2.market2__c = 'US';
          acc2.Sampling_Account_Number__c='0000123';
          acc2.RecordtypeId =  rt[0].id;
          insert acc2;
          
          /* acc3.Name='Test Account1'; 
          acc3.Organisation_Type__c ='School';
          acc3.Phone='+9100000';
          acc3.ShippingCountry = 'India'; 
          acc3.ShippingCity = 'Bangalore1';
          acc3.ShippingStreet = 'BNG1';
          acc3.ShippingPostalCode = '560031';
          acc3.market2__c = 'US';
          acc3.Sampling_Account_Number__c='0000124';
          acc3.RecordtypeId =  rt[0].id;
          insert acc3;
           */
          
          //code for SubSamplingAccountNumber 
          List<Account> sacc = new List<Account>();
          Account sacc1 = new Account();
          sacc1.Name='Test Account1'; 
          sacc1.Organisation_Type__c ='School';
          sacc1.Phone='+9100000';
          sacc1.ShippingCountry = 'India'; 
          sacc1.ShippingCity = 'Bangalore1';
          sacc1.ShippingStreet = 'BNG1';
          sacc1.BillingStreet = 'Sholinganallur';
          sacc1.BillingCity = 'Chennai';
          sacc1.BillingCountry ='India';
          sacc1.ShippingPostalCode = '560031'; 
          sacc1.market2__c = 'US'; 
          sacc1.Primary_Selling_Account_check__c = true;
          sacc1.Sampling_Account_Number__c = '0000789';
          sacc1.RecordtypeId =  rt[0].id;
         // acc.add(acc1); 
          insert sacc1;
          
          Account sacc2 = new Account();
          sacc2.Name='Test Account2'; 
          sacc2.Organisation_Type__c ='School';
          sacc2.Phone='+9100000';
          sacc2.ShippingCountry = 'India'; 
          sacc2.ShippingCity = 'Bangalore2';
          sacc2.ShippingStreet = 'BNG2';
          sacc2.ShippingPostalCode = '560032';
          sacc2.ShippingStreet = 'BNG1';
          sacc2.BillingStreet = 'Sholinganallur';
          sacc2.BillingCity = 'Chennai';
          sacc2.market2__c = 'US';
          sacc2.ParentId = sacc1.id;
          sacc2.Primary_Selling_Account__c = sacc1.id;
          sacc2.RecordtypeId =  rt[0].id;
          sacc.add(sacc2); 
          
          Insert sacc;
          PS_AccountSamplingNumberBatch batch1 = new PS_AccountSamplingNumberBatch('US');
          Database.executeBatch(batch1);
          
          test.stopTest();
           
          //PS_AccountSubSamplingNumberBatch batch = new PS_AccountSubSamplingNumberBatch('US');
          //Database.executeBatch(batch);
      }
    }
}