/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PreProcessingInvokeJobStep.cls
   Description:     Class used in flowa to invoke batch jobs based on Job Execution Requests


   Test Class:      PreProcessingInvokeJobStepTest
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   19/09/2016         1.0           Davi Borges               R6                        Created
   25/10/2016         1.1           Iegor Nechyporenko        R6          Make statuses compatible with Metadata Status Picklists
                                         
-----------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class PreProcessingInvokeJobStep {
	@TestVisible
	private static Boolean forceJobExecution = false;
	
    public class PreProcessingInvokeJobStepResponse {
        
        @InvocableVariable (label='Error Message' description='Error message related to the request' )
        public String error;

        @InvocableVariable (label='Status Code' description='Status Code 0 = Success, -1 failure' required=true)
        public String status;

        @InvocableVariable (label='Job Id ' description='Job Id of product deduplication')
        public String job_id ;

    }

    public class PreProcessingInvokeJobStepRequest {
        @InvocableVariable (label='Job Request Execution Id' description='Salesforce Id of the Job Execution Request' )
        public String Id;
        
        @InvocableVariable (label='Batch Apex Class' description='Name of the Batch Class to be invoked' )
        public String ClassName;
        
        @InvocableVariable (label='Batch Size' description='Size of the number of records to process per cycle' )
        public Integer BatchSize;

        @InvocableVariable (label='Data From' description='LastModified Date from where the data will be considered. If null uses the batch last execution date.' )
        public Date DataFrom;

    }

    @InvocableMethod(
        label='Invoke Batch Job Job'
        description='Invoke Batch Job Job')
    public static List<PreProcessingInvokeJobStepResponse> invokeJob(List<PreProcessingInvokeJobStepRequest> requests) {
       
       List<PreProcessingInvokeJobStepResponse> responses = new List<PreProcessingInvokeJobStepResponse>();
           
       for(PreProcessingInvokeJobStepRequest request : requests)
       {
           PreProcessingInvokeJobStepResponse response = invokeJob(request.Id, request.ClassName, request.BatchSize,request.DataFrom );
           
           responses.add(response);
       }
       
       System.debug('Responses: ' + responses);
       
       return responses;
    }
    
    
    
    //Method could be used to execute functionality manually from apex, not from flow scope
    public static void invokeJobManually(String jobRequestId, PreProcessingBatchJob batchJob){
    	//Get information about batch size, and date when batch should be executed for specific job
    	String queryString = 'SELECT Id, ' + batchJob.batchSizeFieldApiName + ', ' + batchJob.dataFromFieldApiName +
    			' FROM Job_Execution_Request__c ' +
    			' WHERE Id = \'' + jobRequestId + '\'';
    			
    	Job_Execution_Request__c jobExecutionRequest = Database.query(queryString);
    	if (Test.isRunningTest()){ forceJobExecution = true; } //Execute job even if it's running test
    	//Do job invocation to specify 
    	PreProcessingInvokeJobStepResponse invocationResult = invokeJob(
    		jobRequestId,
    		batchJob.className,
    		Integer.valueOf(jobExecutionRequest.get(batchJob.batchSizeFieldApiName)),
    		Date.valueOf(jobExecutionRequest.get(batchJob.dataFromFieldApiName)),
    		batchJob
    	);
    	
    	
    	//Manually update Error Message and Specific job status, when job is started
    	Job_Execution_Request__c jobDataToUpdate = new Job_Execution_Request__c(Id = jobRequestId); 
    	jobDataToUpdate.Error_Message__c = invocationResult.error;
    	jobDataToUpdate.put(batchJob.statusFieldApiName, invocationResult.status);
    	jobDataToUpdate.put(batchJob.jobIdFieldApiName, invocationResult.job_id);
    	Database.update(jobDataToUpdate);
    }

	//Invocation job method which will be used iside Visualforce Flow
	public static PreProcessingInvokeJobStepResponse invokeJob(String jobRequestId, String className, Integer batchSize, Date dataFrom ) {
		return invokeJob(jobRequestId, className, batchSize, dataFrom, null);
	}

	//Common Method which creates Batchable apex class and add it to Async Queue
    public static PreProcessingInvokeJobStepResponse invokeJob(String jobRequestId, String className, Integer batchSize, Date dataFrom, PreProcessingBatchJob batchjob ) {
       
       PreProcessingInvokeJobStepResponse response = new PreProcessingInvokeJobStepResponse ();
       
       try
       {
        	if (batchjob == null){
            	Type t = Type.forName(className);
            	batchjob = (PreProcessingBatchJob)t.newInstance();
        	}
        	
           	batchjob.initialize(jobRequestId,dataFrom);
 
            Id apexJobId = Database.executeBatch(batchjob, batchSize); 
           
            response.job_id = apexJobId;
    
            //Design Decision: batch job will be aborted during test execution
            if(Test.isRunningTest() && !forceJobExecution){
                System.abortJob(apexJobId);
            }
                     
             //For test purposes whole flow could go through the process
             response.status = Test.isRunningTest() ? PreProcessingBatchJob.STATUS_COMPLETED : PreProcessingBatchJob.STATUS_STARTED;
             response.error = ''; 
                           
       }catch(Exception e){
        
           response.status = PreProcessingBatchJob.STATUS_CRITICAL_ERROR;
           response.error = 'ERROR:' + e; 
       }
       
       return response;

    }
}