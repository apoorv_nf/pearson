/* --------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_LeadIntegrationUtilities_Test 
Description:     Test class for PS_LeadIntegrationUtilities
Date Written      Version         Author                                        Summary of Changes 
-----------      ----------   -----------------     ---------------------------------------------------------------------------------------
20th Oct 2015      1.0         Rahul Boinepally                  New test class for PS_LeadIntegrationUtilities class.
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

@isTest(seealldata =true)

private class PS_LeadIntegrationUtilities_Test 
{ 
    
    static testMethod void testCreateCampaignmember()
    {  
        List<User> userLst = TestDataFactory.createUser(Userinfo.getProfileid() , 1);
        userLst[0].Market__c = 'US';
        insert userLst;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=userLst[0].id,Disable_Triggers__c=false,
                                                        Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
        insert byp;
        list<Campaign> campaignList = new list<Campaign>();
        List<Account> accs = new List<Account>();
        System.runas(userLst[0]){
            test.startTest();
            campaignList = TestDataFactory.createCampaign();
            Database.insert(campaignList);
            accs = TestDataFactory.createAccount(1,'Corporate');
            
            accs[0].isBeingConvertedTo__c = true;
            accs[0].isBeingConvertedToText__c = 'True';
            accs[0].Market2__c = 'US';
            accs[0].Business_Unit__c = 'Pearson Assessment';
            accs[0].Organisation_Type__c = 'School';
            insert accs;
            
            
            list<lead> lstLeadRecords = new list<Lead>();
            lstLeadRecords = TestDataFactory.createLead(12,'B2B');
            for(Lead leadLst : lstLeadRecords)
            {             
                leadLst.campaign_id__c = campaignList[0].id;
                leadLst.isFromIntegration__c = True;
                leadLst.Record_Type__c = 'B2B';
                leadLst.Source_Details__c = 'Source';
                leadLst.Institution_Organisation__c = accs[0].Id;
                leadLst.Market__C = 'US';
            }
            
            Database.insert(lstLeadrecords); 
            test.stopTest();        
            List<CampaignMember> campMemberObject = [select id from CampaignMember where CampaignId = :campaignList[0].id AND LeadId = :lstLeadrecords[0].id];
            system.assertEquals(campMemberObject.size(),1); 
        }       
    }
    
    static testMethod void testMapRecordType()
    {  
        List<User> userLst = TestDataFactory.createUser(Userinfo.getProfileid() , 1);
        userLst[0].Market__c = 'US';
        insert userLst;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=userLst[0].id,Disable_Triggers__c=false,
                                                        Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
        insert byp;
        System.runas(userLst[0]){
            test.startTest();
            list<lead> lstLeadRecords = new list<Lead>();
            
            List<Account> accs = TestDataFactory.createAccount(1,'Corporate');
            accs[0].isBeingConvertedTo__c = true;
            accs[0].isBeingConvertedToText__c = 'True';
            accs[0].Market2__c = 'US';
            accs[0].Business_Unit__c = 'Pearson Assessment';
            accs[0].Organisation_Type__c = 'School';
            insert accs;
            lstLeadRecords = TestDataFactory.createLead(1,'B2B');
            for(Lead leadLst : lstLeadRecords)
            {            
                leadLst.isFromIntegration__c = True;
                leadLst.Record_Type__c = 'B2B';
                leadLst.Source_Details__c = 'Source';
                leadLst.Institution_Organisation__c = accs[0].Id;
                leadLst.Market__C = 'US';
            }
            Database.insert(lstLeadrecords);    
            test.stopTest();  
            
            list<Lead> updatedLeads = [select Id, RecordTypeId from Lead where Id in :lstLeadrecords];
            
            Id leadB2BRecordTypeId = Lead.sObjectType.getDescribe().getRecordTypeInfosByName().get('B2B').getRecordTypeId();
            Id leadRecType = updatedLeads[0].RecordTypeId;
            system.assertEquals(leadRecType, leadB2BRecordTypeId);
            System.debug(leadB2BRecordTypeId);
        }
        
    }    
    
}