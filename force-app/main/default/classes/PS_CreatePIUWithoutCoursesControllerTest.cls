/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_CreatePIUWithoutCoursesControllerTest
   Description:     Test class for PS_CreatePIUWithoutCoursesController
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   18 Nov 2015      1.0           Accenture NDC                                         Created
   10 May 2016      1.1           Abhinav                                               Modified class for code coverage.
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
  
@isTest
public class PS_CreatePIUWithoutCoursesControllerTest
{
    static testmethod void testWithCoursesForDirectChannel()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        List<Contact> lstWithContact = new List<Contact>();
        List<Product2> lstWithProduct = new List<Product2>();
        List<OpportunityLineItem> lstWithOpptyProd = new List<OpportunityLineItem>();
        List<OpportunityContactRole> lstOpptyContactRole = new List<OpportunityContactRole>();
        List<Asset> astlist = new List<Asset>();
        List<Contact_Product_In_Use__c> conastlist = new List<Contact_Product_In_Use__c>();
        lstWithProduct = TestDataFactory.createProduct(2);
        lstWithContact = TestDataFactory.createContact(1);
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Direct';
            lstWithOppty[0].Selling_Period__c = 'Fall';
            lstWithOppty[0].Selling_Period_Year__c = '2020';
            lstWithOppty[0].StageName = 'Awarded';
        }
        test.startTest();
        insert lstWithProduct;
        insert lstWithAccount;
        insert lstWithTestUser;
        insert lstWithContact;
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].AccountId = lstWithAccount[0].Id;
            insert lstWithOppty;
        }   
        opportunitycontactrole newOpptyCon = new opportunitycontactrole();
        newOpptyCon.contactId = lstWithContact[0].Id;
        newOpptyCon.opportunityId = lstWithOppty[0].Id;
        newOpptyCon.role = 'Team Member';
        lstOpptyContactRole.add(newOpptyCon);
        if(lstOpptyContactRole.size() > 0)
        {
            insert  lstOpptyContactRole;
        }
        if(lstWithProduct.size() > 0 && lstWithOppty.size() > 0)
        {
            List<PriceBookEntry> listWithPriceBookEntry = new List<PriceBookEntry>();
            for(Product2 insertedProd : lstWithProduct)
            {
                PriceBookEntry newPriceBookEntry = new PriceBookEntry();
                newPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();
                newPriceBookEntry.UnitPrice = 10.0;
                newPriceBookEntry.Product2Id = insertedProd.Id; 
                newPriceBookEntry.IsActive = true;
                listWithPriceBookEntry.add(newPriceBookEntry);
            }
            insert listWithPriceBookEntry;
            if(listWithPriceBookEntry.size() > 0)
            {
                for(PriceBookEntry pbe : listWithPriceBookEntry)
                {
                    opportunityLineItem newLineItem = new opportunityLineItem();
                    newLineItem.PricebookEntryId = pbe.Id;
                    newLineItem.OpportunityId = lstWithOppty[0].Id;
                    newLineItem.Quantity = 12;
                    newLineItem.UnitPrice = 10.0;
                    lstWithOpptyProd.add(newLineItem);
                }
            }
        }
        insert lstWithOpptyProd;
        astlist =  TestDataFactory.insertAsset(1,null, lstWithContact[0].id, lstWithProduct[0].id, lstWithAccount[0].id);
        if(astlist.size() > 0)
        {
           astlist[0].Pricing_UOM__c = 'Month';
           astlist[0].Selling_Term__c = 8;     
        }
        insert astlist;
        conastlist = TestDataFactory.insertContactPIU(1,astlist[0].id,lstWithContact[0].id);
        insert conastlist;
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
          //Test the class in 'PS_CreatePIUWithoutCourses' page context
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_CreatePIUWithoutCourses?accountId='+lstWithOppty[0].AccountId+'&opportunityId='+lstWithOppty[0].Id);
              Test.setCurrentPage(pageRef);
              PS_CreatePIUWithoutCoursesController controllerObj = new PS_CreatePIUWithoutCoursesController();
              controllerObj.getOpptyContactRole();
              controllerObj.Beginning();
              controllerObj.Previous(); 
              controllerObj.Next(); 
              controllerObj.End(); 
              controllerObj.getDisablePrevious();
              controllerObj.getDisableNext();
              controllerObj.getTotal_size(); 
              controllerObj.getPageNumber();
              controllerObj.getTotalPages();
              controllerObj.cancel();
              controllerObj.lstWrappercontactDetails[0].isSelected = true;
              controllerObj.createAndModifyTempList();
              controllerObj.createPIUForSelectedContacts();
          }
        }      
    }
    static testmethod void testWithCoursesForDirectChannel2()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        List<Contact> lstWithContact = new List<Contact>();
        List<Product2> lstWithProduct = new List<Product2>();
        List<OpportunityLineItem> lstWithOpptyProd = new List<OpportunityLineItem>();
        List<OpportunityContactRole> lstOpptyContactRole = new List<OpportunityContactRole>();
        List<Asset> astlist = new List<Asset>();
        List<Contact_Product_In_Use__c> conastlist = new List<Contact_Product_In_Use__c>();
        lstWithProduct = TestDataFactory.createProduct(2);
        lstWithContact = TestDataFactory.createContact(1);
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Direct';
            lstWithOppty[0].Selling_Period__c = 'Spring';
            lstWithOppty[0].Selling_Period_Year__c = '2120';
            lstWithOppty[0].StageName = 'Awarded';
        }
        test.startTest();
        insert lstWithProduct;
        insert lstWithAccount;
        insert lstWithTestUser;
        insert lstWithContact;
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].AccountId = lstWithAccount[0].Id;
            insert lstWithOppty;
        }   
        opportunitycontactrole newOpptyCon = new opportunitycontactrole();
        newOpptyCon.contactId = lstWithContact[0].Id;
        newOpptyCon.opportunityId = lstWithOppty[0].Id;
        newOpptyCon.role = 'Team Member';
        lstOpptyContactRole.add(newOpptyCon);
        if(lstOpptyContactRole.size() > 0)
        {
            insert  lstOpptyContactRole;
        }
        if(lstWithProduct.size() > 0 && lstWithOppty.size() > 0)
        {
            List<PriceBookEntry> listWithPriceBookEntry = new List<PriceBookEntry>();
            for(Product2 insertedProd : lstWithProduct)
            {
                PriceBookEntry newPriceBookEntry = new PriceBookEntry();
                newPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();
                newPriceBookEntry.UnitPrice = 10.0;
                newPriceBookEntry.Product2Id = insertedProd.Id; 
                newPriceBookEntry.IsActive = true;
                listWithPriceBookEntry.add(newPriceBookEntry);
            }
            insert listWithPriceBookEntry;
            if(listWithPriceBookEntry.size() > 0)
            {
                for(PriceBookEntry pbe : listWithPriceBookEntry)
                {
                    opportunityLineItem newLineItem = new opportunityLineItem();
                    newLineItem.PricebookEntryId = pbe.Id;
                    newLineItem.OpportunityId = lstWithOppty[0].Id;
                    newLineItem.Quantity = 12;
                    newLineItem.UnitPrice = 10.0;
                    lstWithOpptyProd.add(newLineItem);
                }
            }
        }
        insert lstWithOpptyProd;
        astlist =  TestDataFactory.insertAsset(1,null, lstWithContact[0].id, lstWithProduct[0].id, lstWithAccount[0].id);
        if(astlist.size() > 0)
        {
           astlist[0].Pricing_UOM__c = 'Year';
           astlist[0].Selling_Term__c = 10;     
        }
        insert astlist;
        conastlist = TestDataFactory.insertContactPIU(1,astlist[0].id,lstWithContact[0].id);
        insert conastlist;
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
          //Test the class in 'PS_CreatePIUWithoutCourses' page context
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_CreatePIUWithoutCourses?accountId='+lstWithOppty[0].AccountId+'&opportunityId='+lstWithOppty[0].Id);
              Test.setCurrentPage(pageRef);
              PS_CreatePIUWithoutCoursesController controllerObj = new PS_CreatePIUWithoutCoursesController();
              controllerObj.getOpptyContactRole();
              controllerObj.Beginning();
              controllerObj.Previous(); 
              controllerObj.Next(); 
              controllerObj.End(); 
              controllerObj.getDisablePrevious();
              controllerObj.getDisableNext();
              controllerObj.getTotal_size(); 
              controllerObj.getPageNumber();
              controllerObj.getTotalPages();
              controllerObj.cancel();
              controllerObj.lstWrappercontactDetails[0].isSelected = true;
              controllerObj.createAndModifyTempList();
              controllerObj.createPIUForSelectedContacts();
          }
        }      
    }
    static testmethod void testWithCoursesForDirectChannel3()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        List<Contact> lstWithContact = new List<Contact>();
        List<Product2> lstWithProduct = new List<Product2>();
        List<OpportunityLineItem> lstWithOpptyProd = new List<OpportunityLineItem>();
        List<OpportunityContactRole> lstOpptyContactRole = new List<OpportunityContactRole>();
        List<Asset> astlist = new List<Asset>();
        List<Contact_Product_In_Use__c> conastlist = new List<Contact_Product_In_Use__c>();
        lstWithProduct = TestDataFactory.createProduct(2);
        lstWithContact = TestDataFactory.createContact(1);
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Direct';
            lstWithOppty[0].Selling_Period__c = 'Semester 1';
            lstWithOppty[0].Selling_Period_Year__c = '2120';
            lstWithOppty[0].StageName = 'Awarded';
        }
        test.startTest();
        insert lstWithProduct;
        insert lstWithAccount;
        insert lstWithTestUser;
        insert lstWithContact;
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].AccountId = lstWithAccount[0].Id;
            insert lstWithOppty;
        }   
        opportunitycontactrole newOpptyCon = new opportunitycontactrole();
        newOpptyCon.contactId = lstWithContact[0].Id;
        newOpptyCon.opportunityId = lstWithOppty[0].Id;
        newOpptyCon.role = 'Team Member';
        lstOpptyContactRole.add(newOpptyCon);
        if(lstOpptyContactRole.size() > 0)
        {
            insert  lstOpptyContactRole;
        }
        if(lstWithProduct.size() > 0 && lstWithOppty.size() > 0)
        {
            List<PriceBookEntry> listWithPriceBookEntry = new List<PriceBookEntry>();
            for(Product2 insertedProd : lstWithProduct)
            {
                PriceBookEntry newPriceBookEntry = new PriceBookEntry();
                newPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();
                newPriceBookEntry.UnitPrice = 10.0;
                newPriceBookEntry.Product2Id = insertedProd.Id; 
                newPriceBookEntry.IsActive = true;
                listWithPriceBookEntry.add(newPriceBookEntry);
            }
            insert listWithPriceBookEntry;
            if(listWithPriceBookEntry.size() > 0)
            {
                for(PriceBookEntry pbe : listWithPriceBookEntry)
                {
                    opportunityLineItem newLineItem = new opportunityLineItem();
                    newLineItem.PricebookEntryId = pbe.Id;
                    newLineItem.OpportunityId = lstWithOppty[0].Id;
                    newLineItem.Quantity = 12;
                    newLineItem.UnitPrice = 10.0;
                    lstWithOpptyProd.add(newLineItem);
                }
            }
        }
        insert lstWithOpptyProd;
        astlist =  TestDataFactory.insertAsset(1,null, lstWithContact[0].id, lstWithProduct[0].id, lstWithAccount[0].id);
        if(astlist.size() > 0)
        {
           astlist[0].Pricing_UOM__c = 'Year';
           astlist[0].Selling_Term__c = 10;     
        }
        insert astlist;
        conastlist = TestDataFactory.insertContactPIU(1,astlist[0].id,lstWithContact[0].id);
        insert conastlist;
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
          //Test the class in 'PS_CreatePIUWithoutCourses' page context
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_CreatePIUWithoutCourses?accountId='+lstWithOppty[0].AccountId+'&opportunityId='+lstWithOppty[0].Id);
              Test.setCurrentPage(pageRef);
              PS_CreatePIUWithoutCoursesController controllerObj = new PS_CreatePIUWithoutCoursesController();
              controllerObj.getOpptyContactRole();
              controllerObj.Beginning();
              controllerObj.Previous(); 
              controllerObj.Next(); 
              controllerObj.End(); 
              controllerObj.getDisablePrevious();
              controllerObj.getDisableNext();
              controllerObj.getTotal_size(); 
              controllerObj.getPageNumber();
              controllerObj.getTotalPages();
              controllerObj.cancel();
              controllerObj.lstWrappercontactDetails[0].isSelected = true;
              controllerObj.createAndModifyTempList();
              controllerObj.createPIUForSelectedContacts();
          }
        }      
    }
    static testmethod void testWithCoursesForDirectChannel4()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        List<Contact> lstWithContact = new List<Contact>();
        List<Product2> lstWithProduct = new List<Product2>();
        List<OpportunityLineItem> lstWithOpptyProd = new List<OpportunityLineItem>();
        List<OpportunityContactRole> lstOpptyContactRole = new List<OpportunityContactRole>();
        List<Asset> astlist = new List<Asset>();
        List<Contact_Product_In_Use__c> conastlist = new List<Contact_Product_In_Use__c>();
        lstWithProduct = TestDataFactory.createProduct(2);
        lstWithContact = TestDataFactory.createContact(1);
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Direct';
            lstWithOppty[0].Selling_Period__c = 'Semester 2';
            lstWithOppty[0].Selling_Period_Year__c = '2120';
            lstWithOppty[0].StageName = 'Awarded';
        }
        test.startTest();
        insert lstWithProduct;
        insert lstWithAccount;
        insert lstWithTestUser;
        insert lstWithContact;
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].AccountId = lstWithAccount[0].Id;
            insert lstWithOppty;
        }   
        opportunitycontactrole newOpptyCon = new opportunitycontactrole();
        newOpptyCon.contactId = lstWithContact[0].Id;
        newOpptyCon.opportunityId = lstWithOppty[0].Id;
        newOpptyCon.role = 'Team Member';
        lstOpptyContactRole.add(newOpptyCon);
        if(lstOpptyContactRole.size() > 0)
        {
            insert  lstOpptyContactRole;
        }
        if(lstWithProduct.size() > 0 && lstWithOppty.size() > 0)
        {
            List<PriceBookEntry> listWithPriceBookEntry = new List<PriceBookEntry>();
            for(Product2 insertedProd : lstWithProduct)
            {
                PriceBookEntry newPriceBookEntry = new PriceBookEntry();
                newPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();
                newPriceBookEntry.UnitPrice = 10.0;
                newPriceBookEntry.Product2Id = insertedProd.Id; 
                newPriceBookEntry.IsActive = true;
                listWithPriceBookEntry.add(newPriceBookEntry);
            }
            insert listWithPriceBookEntry;
            if(listWithPriceBookEntry.size() > 0)
            {
                for(PriceBookEntry pbe : listWithPriceBookEntry)
                {
                    opportunityLineItem newLineItem = new opportunityLineItem();
                    newLineItem.PricebookEntryId = pbe.Id;
                    newLineItem.OpportunityId = lstWithOppty[0].Id;
                    newLineItem.Quantity = 12;
                    newLineItem.UnitPrice = 10.0;
                    lstWithOpptyProd.add(newLineItem);
                }
            }
        }
        insert lstWithOpptyProd;
        astlist =  TestDataFactory.insertAsset(1,null, lstWithContact[0].id, lstWithProduct[0].id, lstWithAccount[0].id);
        if(astlist.size() > 0)
        {
           astlist[0].Pricing_UOM__c = 'Year';
           astlist[0].Selling_Term__c = 10;     
        }
        insert astlist;
        conastlist = TestDataFactory.insertContactPIU(1,astlist[0].id,lstWithContact[0].id);
        insert conastlist;
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
          //Test the class in 'PS_CreatePIUWithoutCourses' page context
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_CreatePIUWithoutCourses?accountId='+lstWithOppty[0].AccountId+'&opportunityId='+lstWithOppty[0].Id);
              Test.setCurrentPage(pageRef);
              PS_CreatePIUWithoutCoursesController controllerObj = new PS_CreatePIUWithoutCoursesController();
              controllerObj.getOpptyContactRole();
              controllerObj.Beginning();
              controllerObj.Previous(); 
              controllerObj.Next(); 
              controllerObj.End(); 
              controllerObj.getDisablePrevious();
              controllerObj.getDisableNext();
              controllerObj.getTotal_size(); 
              controllerObj.getPageNumber();
              controllerObj.getTotalPages();
              controllerObj.cancel();
              controllerObj.lstWrappercontactDetails[0].isSelected = true;
              controllerObj.createAndModifyTempList();
              controllerObj.createPIUForSelectedContacts();
          }
        }      
    }
    static testmethod void testWithCoursesForIndirectChannel()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        List<OpportunityContactRole> lstOpptyContactRole = new List<OpportunityContactRole>();
        List<Contact> lstWithContact = new List<Contact>();
        List<Asset> astlist = new List<Asset>();
        List<Contact_Product_In_Use__c> conastlist = new List<Contact_Product_In_Use__c>();
        lstWithContact = TestDataFactory.createContact(1);
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');  
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Indirect';
            lstWithOppty[0].Selling_Period__c = 'Fall';
            lstWithOppty[0].Selling_Period_Year__c = '2020';
            lstWithOppty[0].StageName = 'Awarded';
        }
        test.startTest();
        insert lstWithAccount;
        insert lstWithTestUser;
        insert lstWithContact;
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].AccountId = lstWithAccount[0].Id;
            insert lstWithOppty;
        }
        opportunitycontactrole newOpptyCon = new opportunitycontactrole();
        newOpptyCon.contactId = lstWithContact[0].Id;
        newOpptyCon.opportunityId = lstWithOppty[0].Id;
        newOpptyCon.role = 'Team Member';
        lstOpptyContactRole.add(newOpptyCon);
        if(lstOpptyContactRole.size() > 0)
        {
            insert  lstOpptyContactRole;
        }
        astlist =  TestDataFactory.insertAsset(1,null, lstWithContact[0].id, null, lstWithAccount[0].id);
        if(astlist.size() > 0)
        {
           astlist[0].Pricing_UOM__c = 'Year';
           astlist[0].Selling_Term__c = 10;     
        }
        insert astlist;
        conastlist = TestDataFactory.insertContactPIU(1,astlist[0].id,lstWithContact[0].id);
        insert conastlist;
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
          //Test the class in 'PS_CreatePIUWithoutCourses' page context
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_CreatePIUWithoutCourses?accountId='+lstWithOppty[0].AccountId+'&opportunityId='+lstWithOppty[0].Id);
              Test.setCurrentPage(pageRef);
              PS_CreatePIUWithoutCoursesController controllerObj = new PS_CreatePIUWithoutCoursesController();
              //To cover try catch exception
              controllerObj.getOpptyContactRole();
              controllerObj.createAndModifyTempList();
              controllerObj.lstWithAllContactDetails = null;
              controllerObj.createPIUForSelectedContacts(); 
              controllerObj.selectedContact = true;
          }
        }        
    }
}