/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_CaseBusinessHour.cls 
   Description:     On insert/update/delete of Case record 
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  13/12/2015         1.0            Payal Popat                       Initial Release 
  23/12/2015         1.1            Ajay                    Calculating BusinessHour(RD 1588) and Email Initial Response Time(RD 1599)
  15/03/2016         1.2            Sakshi                  Updated code for Businesshour Exception
  ------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public with sharing class PS_CaseBusinessHour{
    /**
    * Description : Method to stamp case resolution time according to business hours.
    * @param     NA
    * @return    NA
    * @throws    NA
    **/
    public static void mPopulateBusinessHour(List<Case> newCases){
        //Intiating instance for bypass setting 
        List<Case> oServiceCases = new List<Case>();
        oServiceCases =  CommonUtils.getCaseServiceRecordType(System.Label.PS_ServiceRecordTypes, newCases);
        system.debug('@@oServiceCases' + oServiceCases);
        if(oServiceCases.size()>0){    
           Schema.DescribeSObjectResult describeSobjectResult = Schema.SObjectType.Case; 
           Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeSobjectResult.getRecordTypeInfosById();
           Map<Id,String> oCasetoBHmap=new Map<Id,String>();
           List<PS_BusinessHours__mdt> objBusinessHour = new List<PS_BusinessHours__mdt>();
           Set<Id> caseIds = new Set<Id>();
           Set<Id> caseRecordTypeId = new Set<Id>();
            Integer caseToBusinessHourCounter = 0;
            List<String> caseRecordType = new List<String>();
            List<PS_BusinessHours__mdt> businessHourList = new List<PS_BusinessHours__mdt>();
            Map<String,List<PS_BusinessHours__mdt>> casesToBusinessHourList = new Map<String,List<PS_BusinessHours__mdt>>();
            for(Case objCase: oServiceCases){
                caseIds.add(objCase.id);
                caseRecordTypeId.add(objCase.RecordTypeid);
                //caseRecordType.add(objCase.RecordType.name);
            }
            //Added as part of 101 SOQL error after SP,Lex and CI changes deploy
            Map<Id, Schema.RecordTypeInfo> rTInfos = Schema.SObjectType.Case.getRecordTypeInfosById();
            //List<RecordType> caseRecordTypeName = [SELECT Id,Name FROM RecordType WHERE (SobjectType = 'Case' and id IN:caseRecordTypeId)];
            System.debug('oServiceCases' + oServiceCases);
            System.debug('caseIds' + caseIds);
            System.debug('caseRecordTypeid' + caseRecordType);
            //Added as part of 101 SOQL error after SP,Lex and CI changes deploy
            /*for(RecordType recordTypObj : caseRecordTypeName){
                caseRecordType.add(recordTypObj.Name);
            }*/
            for(Id recordTypeId : caseRecordTypeId){
                caseRecordType.add(rTInfos.get(recordTypeId).getName());
            }
            System.debug('caseRecordTypeid;->' + caseRecordType);
            businessHourList= [Select Business_Hours__c,WorkingHours__c,Case_Record_Type__c,Program_Type__c From PS_BusinessHours__mdt where Case_Record_Type__c IN:caseRecordType];
            System.debug('businessHourList' + businessHourList);
            if(!businessHourList.isEmpty()){
                for(PS_BusinessHours__mdt businessHourObj : businessHourList){
                    System.debug('casesToBusinessHourList:->' + casesToBusinessHourList);
                    System.debug('casesToBusinessHourList.containsKey:->' + casesToBusinessHourList.containsKey(businessHourObj.Case_Record_Type__c));
                    
                    if(casesToBusinessHourList.get(businessHourObj.Case_Record_Type__c) != null){
                        if(!casesToBusinessHourList.isEmpty() && !casesToBusinessHourList.containsKey(businessHourObj.Case_Record_Type__c)){
                            List<PS_BusinessHours__mdt> businessHourObjList = casesToBusinessHourList.get(businessHourObj.Case_Record_Type__c);
                            System.debug('businessHourObjList:->' + businessHourObjList);
                            businessHourObjList.add(businessHourObj);
                            casesToBusinessHourList.put(businessHourObj.Case_Record_Type__c,businessHourObjList);
                        }   
                    }
                    else{
                        List<PS_BusinessHours__mdt> businessHourObjListToAdd = new List<PS_BusinessHours__mdt>();
                        businessHourObjListToAdd.add(businessHourObj);
                        casesToBusinessHourList.put(businessHourObj.Case_Record_Type__c,businessHourObjListToAdd);
                    }
                }
            }
            for(Case objCase: oServiceCases){
                if(!casesToBusinessHourList.isEmpty()){
                    System.debug('casesToBusinessHourList' + casesToBusinessHourList);
                    System.debug('recordTypeMap' + recordTypeMap.get(objCase.RecordTypeId).getName());                  
                    if(recordTypeMap.get(objCase.RecordTypeId).getName() == Label.State_National){
                    {                     
                        for(PS_BusinessHours__mdt businessHourObj : casesToBusinessHourList.get(recordTypeMap.get(objCase.RecordTypeId).getName())){
                                if((objCase.Program__c == 'PARCC' || objCase.Program__c == 'Accuplacer') && businessHourObj.Program_Type__c == objCase.Program__c){
                                    oCasetoBHmap.put(objCase.Id,businessHourObj.Business_Hours__c);          
                                }
                                else if(businessHourObj.Program_Type__c == ''){
                                    oCasetoBHmap.put(objCase.Id,businessHourObj.Business_Hours__c);
                                }
                            }
                            
                        }       
                    }else
                        {
                            System.debug('casesToBusinessHourList[0]' + casesToBusinessHourList.get(recordTypeMap.get(objCase.RecordTypeId).getName()));
                        if(!casesToBusinessHourList.isEmpty() && casesToBusinessHourList.get(recordTypeMap.get(objCase.RecordTypeId).getName()) != null){
                            System.debug('Business_Hours__c' + casesToBusinessHourList.get(recordTypeMap.get(objCase.RecordTypeId).getName())[0].Business_Hours__c);
                            oCasetoBHmap.put(objCase.Id,casesToBusinessHourList.get(recordTypeMap.get(objCase.RecordTypeId).getName())[0].Business_Hours__c);
                            }
                        }   
                }
            }//end for-loop
           Map<String,BusinessHours> compnyBusinessHr = new Map<String,BusinessHours>(); //PP Initailize map
           if(oCasetoBHmap.size()>0) {
               List<BusinessHours> oBusinessHrlist = [SELECT Id,Name from BusinessHours where Name =: oCasetoBHmap.values() and 
                                   isActive = true limit 1];
               if (oBusinessHrlist != null){                    
                    for(BusinessHours oBH: oBusinessHrlist){
                        compnyBusinessHr.put(oBH.Name,oBH);
                    }                    
                 for(Case caseToUpdate: oServiceCases){
                    if(compnyBusinessHr.containsKey(oCasetoBHmap.get(caseToUpdate.Id))){
                        caseToUpdate.BusinessHoursId = compnyBusinessHr.get(oCasetoBHmap.get(caseToUpdate.Id)).Id ;
                    }
                 }
               }
            } //end of if
        }//end of if
     }//end of method
    /**
    * Description : Method to stamp initial email response time according to businesshours.
    * @param     NA
    * @return    NA
    * @throws    NA
    **/
    public static void mCalculateInitialResponseTime(List<Case> newCases,Map<Id,Case> oldMap,Map<Id,Case> newMap){
        List<Case> oServiceCases =  CommonUtils.getCaseServiceRecordType(System.Label.PS_ServiceRecordTypes, newCases);
        if(oServiceCases.size()>0){    
            for(Case oCase:oServiceCases){
                if (oldMap.get(oCase.Id).Email_Initial_Response_Date__c != newMap.get(oCase.Id).Email_Initial_Response_Date__c){
                    long businessMillisecondsDiff = BusinessHours.diff(oCase.BusinessHoursId,oCase.CreatedDate,oCase.Email_Initial_Response_Date__c);//calculates initial response time according to business hours
                    decimal businessHoursDiff = businessMillisecondsDiff / (1000.0*60.0*60.0);//converts business hours in minutes format 
                    oCase.Email_Initial_Response_Time__c = businessHoursDiff;
                }
            }
       }
    }//end of method
}