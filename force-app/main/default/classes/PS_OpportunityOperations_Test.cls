/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_OpportunityOperations_Test.cls 
   Description:     Test Class to cover code coverage PS_OpportunityOperations
   Date:            09/12/2017
   Author:          Vishista Madati
------------------------------------------------------------------------------------------------------------------------------------------------------------- */
@isTest(seealldata=true)
public class PS_OpportunityOperations_Test {
    static testMethod void myUnitTest()
    {
        List<User> usrLst= TestDataFactory.createUser(UserInfo.getProfileId());
        insert usrLst;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].Id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp; 
            System.RunAs(usrLst[0]){
            
                                            TestClassAutomation.FillAllFields = true;
            RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'School'];
            Account sAccount        = (Account)TestClassAutomation.createSObject('Account');
            sAccount.BillingCountry      = 'Australia';
            sAccount.BillingState      = 'Victoria';
            sAccount.BillingCountryCode    = 'AU';
            sAccount.BillingStateCode    = 'VIC';
            sAccount.ShippingCountry    = 'Australia';
            sAccount.ShippingState      = 'Victoria';
            sAccount.ShippingCountryCode  = 'AU';
            sAccount.ShippingStateCode    = 'VIC';
            sAccount.Lead_Sponsor_Type__c   = null;
            sAccount.IsCreatedFromLead__c = true;
            sAccount.ShippingPostalCode = '234543';
            sAccount.ShippingStreet = 'TestStreet';
            sAccount.ShippingCity = 'Vns';
            sAccount.recordtypeId=OrgAcc.Id ;
            sAccount.PS_AccountSegment1__c='2';
            sAccount.CTIPIHE_Contract_Signed_Date__c = null;
            
            insert sAccount;
            Id pricebookId = Test.getStandardPricebookId();
            
            //System.debug(logginglevel.INFO,'@@sAccount '+sAccount);
            Opportunity sOpportunity        = (Opportunity)TestClassAutomation.createSObject('Opportunity');
            sOpportunity.AccountId          = sAccount.Id;
            sOpportunity.Enquiry_Type__c = '';
            sOpportunity.Cadence__c = '';
            sOpportunity.Priority__c = '';
            sOpportunity.Adoption_Type__c = 'Committee';
            sOpportunity.Student_Registered__c = True;
            sOpportunity.Pricebook2Id = pricebookId;
            //CP 07/24/2018 sOpportunity.Conferrer__c = 'PIHE';
            //CP 07/24/2018 sOpportunity.Qualification_Picklist__c = 'Bachelor of Arts (Psychology & English)';
            sOpportunity.RecordTypeId= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('D2L').getRecordTypeId();
            Test.startTest();
            insert sOpportunity;
            
            //System.debug(logginglevel.INFO,'@@sOpportunity '+sOpportunity);
            
            Contract contrct = new Contract();
            contrct.Opportunity__c = sOpportunity.id;
            contrct.AccountId = sOpportunity.AccountId;
            contrct.ContractTerm = 12;
            //contrct.StartDate = sOpportunity.CloseDate;
            //insert contrct;  
            //System.debug(logginglevel.INFO,'@@contrct '+contrct);
            
            //Product2 sProduct= (Product2)TestClassAutomation.createSObject('Product2');
            Product2 sProduct = new Product2();
            sProduct.Name = 'Test Product';
            sProduct.Configuration_Type__c='Bundle';   
            sProduct.Market__c ='US';
            sProduct.Business_Unit__c ='US Field Sales';  
            sProduct.Author__c = 'Test Author';
            sProduct.Status__c ='PUB';
            sProduct.Line_of_Business__c='Higher Ed'; 
            sProduct.Product_Family__c = '';
            //sProduct.Product_Author__c ='Test Author';
            sProduct.Edition__c ='Test Edition';
            sProduct.PIHE_Repeater__c = 'True';
            sProduct.RecordTypeId    = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('MDM Global Product').getRecordTypeId();
            insert sProduct;
           // System.debug(logginglevel.INFO,'@@sProduct '+sProduct);
            //Pricebook2 sPriceBook = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
            //sPriceBook.IsActive = true;
            //update sPriceBook;
            
            //Creating Custom price book
            Pricebook2 sPriceBookANZ = new Pricebook2();
            sPriceBookANZ.Description = 'ANZ Price Book - Used to restrict ANZ visibility to only the Non-One CRM products';
            sPriceBookANZ.IsActive = true;
            sPriceBookANZ.Name = 'ANZ Price Book';
            insert sPriceBookANZ;
            //System.debug(logginglevel.INFO,'@@sPriceBookANZ '+sPriceBookANZ); 
            //End of Custom pricebook creation  
            
            // Query Standard and Custom Price Books
            Pricebook2 customPriceBookRec=[select Id from Pricebook2 where Id=:sPriceBookANZ.Id];   
            
    
            PriceBookEntry sPriceBookEntry= (PriceBookEntry)TestClassAutomation.createSObject('PriceBookEntry');
            sPriceBookEntry.IsActive     = true;
            sPriceBookEntry.Product2Id   = sProduct.Id;
            sPriceBookEntry.Pricebook2Id = pricebookId;
            //sPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();
            sPriceBookEntry.UnitPrice    = 34.95;
            insert sPriceBookEntry;
           // System.debug(logginglevel.INFO,'@@sPriceBookEntry '+sPriceBookEntry);
            //Pricebook2 sPriceBookANZ = [select id from Pricebook2 where Name like 'ANZ%' limit 1];
            //sPriceBookANZ.IsActive = true;
            //update sPriceBookANZ;                                  
            
            OpportunityLineItem sOLI  = new OpportunityLineItem();
            sOLI.OpportunityId        = sOpportunity.Id;
            sOLI.PricebookEntryId     = sPriceBookEntry.Id;
            sOLI.TotalPrice           = 200;
            sOLI.Quantity             = 1;
            soLI.Override_Primary_Product_Logic__c = true;
            
            //system.debug('sOLI:::' + sOLI);
            insert sOLI;
            
            update sOLI;
            
            delete sOLI;                        
            Test.stopTest();
        }                                                                                             
    }
    static testMethod void myUnitTest1()
    {    
        List<User> usrLst= TestDataFactory.createUser(UserInfo.getProfileId());
        insert usrLst;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].Id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp; 
            System.RunAs(usrLst[0]){
            
            TestClassAutomation.FillAllFields = true;
            RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'School'];
            Account sAccount        = (Account)TestClassAutomation.createSObject('Account');
            sAccount.BillingCountry      = 'Australia';
            sAccount.BillingState      = 'Victoria';
            sAccount.BillingCountryCode    = 'AU';
            sAccount.BillingStateCode    = 'VIC';
            sAccount.ShippingCountry    = 'Australia';
            sAccount.ShippingState      = 'Victoria';
            sAccount.ShippingCountryCode  = 'AU';
            sAccount.ShippingStateCode    = 'VIC';
            sAccount.Lead_Sponsor_Type__c   = null;
            sAccount.IsCreatedFromLead__c = true;
            sAccount.ShippingPostalCode = '234543';
            sAccount.ShippingStreet = 'TestStreet';
            sAccount.ShippingCity = 'Vns';
            sAccount.recordtypeId=OrgAcc.Id ;
            sAccount.PS_AccountSegment1__c='2';
            
            Id stdpricebookId =  Test.getStandardPricebookId();
            insert sAccount;
            System.debug(logginglevel.INFO,'@@sAccount '+sAccount);
            Opportunity sOpportunity = (Opportunity)TestClassAutomation.createSObject('Opportunity');
            sOpportunity.AccountId = sAccount.Id;
            sOpportunity.Enquiry_Type__c = '';
            sOpportunity.Cadence__c = '';
            sOpportunity.Priority__c = '';
            sOpportunity.Adoption_Type__c = 'Committee'; 
            sOpportunity.Pricebook2Id = stdpricebookId;
           //CP 07/24/2018 sOpportunity.Conferrer__c = 'PIHE';
            //CP 07/24/2018 sOpportunity.Qualification_Picklist__c = 'Bachelor of Arts (Psychology & English)';
            sOpportunity.RecordTypeId= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Global Opportunity').getRecordTypeId();
          
            insert sOpportunity;
            System.debug(logginglevel.INFO,'@@sOpportunity '+sOpportunity);
            
            Contract contrct = new Contract();
            contrct.AccountId = sOpportunity.AccountId;
            //contrct.ContractTerm = 12;
            //contrct.StartDate = sOpportunity.CloseDate;
            //insert contrct;  
            System.debug(logginglevel.INFO,'@@contrct '+contrct);
            
            //Product2 sProduct= (Product2)TestClassAutomation.createSObject('Product2');
            Product2 sProduct = new Product2();
            sProduct.Name = 'Test Product';
            sProduct.Configuration_Type__c='Bundle';   
            sProduct.Market__c ='US';
            sProduct.Business_Unit__c ='US Field Sales';  
            sProduct.Author__c = 'Test Author';
            sProduct.Status__c ='PUB';
            sProduct.Line_of_Business__c='Higher Ed'; 
            sProduct.Product_Family__c = '';
            //sProduct.Product_Author__c ='Test Author';
            sProduct.Edition__c ='Test Edition';
            sProduct.PIHE_Repeater__c = 'True';
            sProduct.RecordTypeId    = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('MDM Global Product').getRecordTypeId();
            insert sProduct;
            //System.debug(logginglevel.INFO,'@@sProduct '+sProduct);
            //Pricebook2 sPriceBook = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
            //sPriceBook.IsActive = true;
            //update sPriceBook;
            
            PriceBookEntry sPriceBookEntry= (PriceBookEntry)TestClassAutomation.createSObject('PriceBookEntry');
            sPriceBookEntry.IsActive        = true;
            sPriceBookEntry.Product2Id      = sProduct.Id;
            sPriceBookEntry.Pricebook2Id    = stdpricebookId;
            sPriceBookEntry.UnitPrice       = 34.95;
            insert sPriceBookEntry;
            //System.debug(logginglevel.INFO,'@@sPriceBookEntry '+sPriceBookEntry);
            //Pricebook2 sPriceBookANZ = [select id from Pricebook2 where Name like 'ANZ%' limit 1];
            //sPriceBookANZ.IsActive = true;
            //update sPriceBookANZ;
            
            
            //Creating Custom price book
            Pricebook2 sPriceBookANZ = new Pricebook2();
            sPriceBookANZ.Description = 'ANZ Price Book - Used to restrict ANZ visibility to only the Non-One CRM products';
            sPriceBookANZ.IsActive = true;
            sPriceBookANZ.Name = 'ANZ Price Book';
            insert sPriceBookANZ;
            //System.debug(logginglevel.INFO,'@@sPriceBookANZ '+sPriceBookANZ);
            //End of Custom pricebook creation
            Proposal_Pricelist_Mapping__mdt proposalpricelist =  new Proposal_Pricelist_Mapping__mdt();
            Test.startTest();
            
            OpportunityLineItem sOLI   = new OpportunityLineItem();
            sOLI.OpportunityId         = sOpportunity.Id;
            sOLI.PricebookEntryId      = sPriceBookEntry.Id;
            sOLI.TotalPrice            = 200;
            sOLI.Quantity              = 1;
            soLI.Override_Primary_Product_Logic__c = true;
            
            //system.debug('sOLI:::' + sOLI + ' Oli Pricebook - ' + sOLI.pricebookentry.pricebook2id + ' Oppty pricebook='+ sOLI.Opportunity.pricebook2id);
            insert sOLI;
            
            update sOLI;
            
            delete sOLI;                
            
            Test.stopTest();
        }
    }
    /*
    static testMethod void myUnitTest2()
    {    
        List<User> usrLst= TestDataFactory.createUser(UserInfo.getProfileId());
        insert usrLst;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].Id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp; 
         Map<Id,Task> newTasks=new Map<Id,Task>();
         Map<Id,Task> oldTasks= new map<id,task>();
         Map<Id,Task> newTasksupdate=new Map<Id,Task>();
         Map<Id,Task> oldTasksupdate= new map<id,task>();
         Map<Id,Task> newTasksdelete=new Map<Id,Task>();
         Map<Id,Task> oldTasksdelete= new map<id,task>();
         List<Task> tasklist=new list<task>();
         List<Task> tasklistupdate=new list<task>();
         List<Task> tasklistdelete=new list<task>();
         
            System.RunAs(usrLst[0]){
                Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School',Market2__c='US',Phone = '9989887687');
                acc.ShippingStreet = 'TestStreet';
                acc.ShippingCity = 'Vns';
                acc.ShippingState = 'Delhi';
                acc.ShippingPostalCode = '234543';
                acc.ShippingCountry = 'India';
                acc.IsCreatedFromLead__c = TRUE;
                insert acc;
                Contact con = new Contact(FirstName='AssetHandler',LastName ='Test',Phone='9999888898',Email='AssetHandler.Test@testclass.com', AccountId = acc.Id,
                                          Preferred_Address__c = 'Mailing Address', MailingCountry = 'United Kingdom', MailingStreet = '1 Street', MailingPostalCode = 'NE27 0QQ', MailingCity  = 'City');
                insert con;
                
                Opportunity ro = new Opportunity(Name= 'OpTest', AccountId = acc.id, StageName = 'Solutioning', Type = 'New Business', Academic_Vetting_Status__c = 'Un-Vetted', Academic_Start_Date__c = System.Today(),CloseDate = System.Today(),International_Student__c = true);
                insert ro;
                
                Task ts = new Task(Subject ='Email',Priority ='Normal', Status ='Not Started', WhatId = ro.Id,Ownerid = userinfo.getUserId());
                //insert ts;
                Task ts2 = new Task(Subject ='Call',Priority ='Normal', Status ='Not Started', WhatId = ro.Id,Ownerid = userinfo.getUserId());
                insert ts2;
                Task ts3 = new Task(Subject ='Email',Priority ='High', Status ='Not Started', WhatId = ro.Id,Ownerid = userinfo.getUserId());
                //insert ts3;
                
                tasklist.add(ts);
                // tasklist.add(ts2);
                tasklist.add(ts3);
                insert tasklist;  
                              
                ts2.subject='Email';
                
                
                Test.startTest();
                
                ta
                sklistupdate.add(ts2);
                update tasklistupdate;                
                
                
                newTasksupdate.put(ts2.id, ts2);
                
                newTasksupdate.put(ts2.id,ts2);
                oldTasksupdate.put(ts2.id,ts2);
                
                
                PS_OpportunityValidationFactory.OpptyTaskValidations(tasklist,newTasksupdate,null,'Insert');
                
                Test.stopTest();
            
            }
     } 
     */      
    
}