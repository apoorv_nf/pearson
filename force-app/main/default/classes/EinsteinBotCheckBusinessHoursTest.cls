/**
* @description : Test class for EinsteinBotCheckBusinessHours, BusinessDaysUtil.
* @Author      : Apoorv (Neuraflash)  
* @CreatedDate : Aug 30, 2019
*/
@isTest
private class EinsteinBotCheckBusinessHoursTest {
	
	@isTest 
	static void testCheckDefaultBusinessHours(){
		List<Boolean> bhList = new List<Boolean>();
		Test.startTest();
			bhList = EinsteinBotCheckBusinessHours.checkBusinessHours(new List<String>());
		Test.stopTest();
		system.assertEquals(bhList.size()>0, TRUE);
	}

    @isTest 
	static void testCheckBusinessHours(){
		List<Boolean> bhList = new List<Boolean>();
		Test.startTest();
			bhList = EinsteinBotCheckBusinessHours.checkBusinessHours(new List<String>{'Default'});
		Test.stopTest();
		system.assertEquals(bhList.size()>0, TRUE);
	}

}