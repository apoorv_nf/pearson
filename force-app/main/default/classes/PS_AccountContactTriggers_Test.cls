/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:         PS_AccountContactTriggers_Test .cls
   Description:  Test Class For AccountContact.trigger
                
   Date             Version     Author                  Tag     Summary of Changes 
   -----------      -------     -----------------       ---     ------------------------------------------------------------------------
   11/09/2015        0.1         Accenture               None    Initial Version
   08/03/2016        0.2         Abhinav                 None    populating the National Identification Number on Contact with correct syntax. 
  -------------------------------------------------------------------------------------------------------------------------------------------------------- */
  
 @isTest
    private class PS_AccountContactTriggers_Test
    {
        static testMethod void myUnitTest()
        {
            
            PS_Util.isFetchedRecordType=False;
            PS_Util.isFetchedUserRecord=False;
            PS_Util.isProfileFetched=False;
            TestClassAutomation.FillAllFields = true;
            RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'School'];
            Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
            sAccount.BillingCountry         = 'Australia';
            sAccount.BillingState           = 'Victoria';
            sAccount.BillingCountryCode     = 'AU';
            sAccount.BillingStateCode       = 'VIC';
            sAccount.ShippingCountry        = 'Australia';
            sAccount.ShippingState          = 'Victoria';
            sAccount.ShippingCountryCode    = 'AU';
            sAccount.ShippingStateCode      = 'VIC';
            sAccount.Lead_Sponsor_Type__c   = null;
            sAccount.IsCreatedFromLead__c = True; 
            sAccount.recordtypeId=OrgAcc.Id ;
            List<Account> accList = new List<Account>();
                    
            Account acc = new Account();
            acc.Name='TestAcc123';
            
            accList.add(sAccount);
            accList.add(acc);
            insert accList;
            
            

            
            
            Contact sContact                = (Contact)TestClassAutomation.createSObject('Contact');
            sContact.AccountId              = accList[0].Id;
            sContact.OtherCountry           = 'Australia';
            sContact.Role__c                   = 'Educator';
            sContact.Role_Detail__c         = 'Director';
            sContact.OtherState             = 'Victoria';
            sContact.OtherCountryCode       = 'AU';
            sContact.OtherStateCode         = 'VIC';
            sContact.MailingCountry         = 'Australia';
            sContact.MailingState           = 'Victoria';
            sContact.MailingCountryCode     = 'AU';
            sContact.MailingStateCode       = 'VIC';
            //Abhinav - populating Contact record with the correct syntax of National Identity Number field.
            sContact.National_Identity_Number__c = '9412055708083';    
            Test.startTest();
                
                insert sContact;
                
            checkRecurssion.run = true;    
            AccountContact__c accContObj = new AccountContact__c();
            accContObj.Contact__c = sContact.id;
            accContObj.Account__c = accList[1].id;
            accContObj.AccountRole__c= 'Educator';
            accContObj.Role_Detail__c= 'Director';
            checkRecurssion.run = true; 
            insert accContObj;

             checkRecurssion.run = true; 
            accContObj.Role_Detail__c= 'Dean';
            //accContObj.Account__c=accList[1].id;
            update accContObj;

                checkRecurssion.run = true; 
            delete accContObj;
             checkRecurssion.run = true; 
             undelete accContObj;
            


            
             
                
    AccountContactTriggerHandler testa= new AccountContactTriggerHandler(true,5);
     

     AccountContact__c[] accnt;
     
    //accnt = [Select Id,AccountContact__c.Primary__c,account__c,contact__c,AccountRole__c,Financially_Responsible__c  FROM AccountContact__c WHERE Contact__c = : sContact.Id limit 1];
                // testa.OnBeforeInsert(accnt);
                
    //if(accnt!=null){         testa.OnAfterInsert(accnt);}
                 
            Test.stopTest();
        }
       Static testmethod void myUnitTest_1(){
             TestClassAutomation.FillAllFields = true;
            RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'School'];
            Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
            sAccount.BillingCountry         = 'Australia';
            sAccount.BillingState           = 'Victoria';
            sAccount.BillingCountryCode     = 'AU';
            sAccount.BillingStateCode       = 'VIC';
            sAccount.ShippingCountry        = 'Australia';
            sAccount.ShippingState          = 'Victoria';
            sAccount.ShippingCountryCode    = 'AU';
            sAccount.ShippingStateCode      = 'VIC';
            sAccount.Lead_Sponsor_Type__c   = null;
            sAccount.IsCreatedFromLead__c = True; 
            sAccount.recordtypeId=OrgAcc.Id ;
            insert sAccount;
            
            Contact sContact                = (Contact)TestClassAutomation.createSObject('Contact');
            sContact.AccountId              = sAccount.Id;
            sContact.OtherCountry           = 'Australia';
            sContact.OtherState             = 'Victoria';
            sContact.Role__c                = 'Educator';
            sContact.Role_Detail__c         = 'Director';
            sContact.OtherCountryCode       = 'AU';
            sContact.OtherStateCode         = 'VIC';
            sContact.MailingCountry         = 'Australia';
            sContact.MailingState           = 'Victoria';
            sContact.MailingCountryCode     = 'AU';
            sContact.MailingStateCode       = 'VIC';
            sContact.National_Identity_Number__c = '9412055708083';
                            
            Test.startTest();
                
             insert sContact;  

               
                checkRecurssion.run = true;    
            AccountContact__c accContObj = new AccountContact__c();
            accContObj.Contact__c = sContact.id;
            accContObj.Account__c = sAccount.id;
             accContObj.AccountRole__c= 'Educator';
            accContObj.Role_Detail__c= 'Director';
            checkRecurssion.run = true;
            insert accContObj;

             checkRecurssion.run = true; 
            accContObj.Role_Detail__c= 'Dean';
            update accContObj;
            
            checkRecurssion.run = true; 
            //delete accContObj;


            
           
             //AccountContact__c sAccountContact = [Select Id,AccountContact__c.Primary__c,account__c,contact__c,AccountRole__c,Financially_Responsible__c FROM AccountContact__c WHERE Contact__c = : sContact.Id limit 1];
             
    AccountContactTriggerHandler testa= new AccountContactTriggerHandler(true,5);
    System.assertEquals(true,  testa.bIsTriggerContext);  
               System.assertEquals(false,  testa.bIsVisualforcePageContext,false);   
               System.assertEquals(false, testa.bIsWebServiceContext,false);  
               System.assertEquals(false,  testa.bIsExecuteAnonymousContext,false); 
     AccountContact__c[] accnt;
     /*accnt = [Select Id,AccountContact__c.Primary__c,account__c,contact__c,AccountRole__c,Financially_Responsible__c,Sync_In_Progress__c FROM AccountContact__c WHERE Contact__c = : sContact.Id limit 1];

    AccountContact__c[] accntol;
         map<id,AccountContact__c> acc =new map<id,AccountContact__c>();
         //acc = [Select Id,AccountContact__c.Primary__c,account__c,contact__c,AccountRole__c,Financially_Responsible__c  FROM AccountContact__c WHERE Contact__c = : sContact.Id limit 1];

         map<id,AccountContact__c> acccon =new map<id,AccountContact__c>();
        
         testa.OnBeforeinsert(accnt);*/

             
             
             Test.stopTest();
                
         }  
}