/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_ApttusMarketingInformationControllerTest
   Description:     Test class for PS_ApttusMarketingInformationController
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   24 Nov 2015      1.0           Accenture NDC                                         Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
 
@isTest
public class PS_ApttusMarketingInfoControllerTest
{
    static testMethod void testMarketingInformationAbouThisProd()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Product2> lstWithProduct = new List<Product2>();
        List<Marketing_Information__c> lstWithMarketingInfo = new List<Marketing_Information__c>();
        List<Marketing_Information__c> lstWithMarketingInfoRepTools = new List<Marketing_Information__c>();
        lstWithProduct = TestDataFactory.createProduct(2);
        lstWithMarketingInfo = TestDataFactory.insertMarketInformation();
        test.startTest();
        insert lstWithProduct;
        insert lstWithTestUser;
        if(lstWithMarketingInfo.size() > 0)
        {
            lstWithMarketingInfo[0].Product__c = lstWithProduct[0].Id;
            lstWithMarketingInfo[0].Type__c = 'About this Product';
        }
        insert lstWithMarketingInfo;
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
            //Test the class in 'PS_ApttusMarketingInformation' page context
            PageReference pageRef = new PageReference('/apex/PS_ApttusMarketingInformation?productId='+lstWithProduct[0].Id);
            Test.setCurrentPage(pageRef);
            PS_ApttusMarketingInformationController controllerObj = new PS_ApttusMarketingInformationController();
            controllerObj.marketingInformationTypeString = 'About This Product';
            controllerObj.marketingInfoSubtypeString = 'Table of contents';
            controllerObj.marketingDetails();
        }    
    }
    static testMethod void testMarketingInformationRepTools()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Product2> lstWithProduct = new List<Product2>();
        List<Marketing_Information__c> lstWithMarketingInfoRepTools = new List<Marketing_Information__c>();
        lstWithProduct = TestDataFactory.createProduct(2);
        lstWithMarketingInfoRepTools = TestDataFactory.insertMarketInformation();
        test.startTest();
        insert lstWithProduct;
        insert lstWithTestUser;
        if(lstWithMarketingInfoRepTools.size() > 0)
        {
            lstWithMarketingInfoRepTools[0].Product__c = lstWithProduct[0].Id;
            lstWithMarketingInfoRepTools[0].Type__c = 'Rep Tools';
        }
        insert lstWithMarketingInfoRepTools;
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
            //Test the class in 'PS_ApttusMarketingInformation' page context
            PageReference pageRef = new PageReference('/apex/PS_ApttusMarketingInformation?productId='+lstWithProduct[0].Id);
            Test.setCurrentPage(pageRef);
            PS_ApttusMarketingInformationController controllerObj = new PS_ApttusMarketingInformationController();
            controllerObj.marketingInformationTypeString = 'Rep Tools';
            controllerObj.marketingDetails();
        }
    }       
}