/*****************************************************************************************************************************/
/*Description:This class is getting called from Process builder for creating a new user based on mirror user details

Date             Version              Author                          Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
01-Nov-2019      	0.1         	TAMIZHARASAN     		CR-03041
20-Nov-2019      	0.2         	TAMIZHARASAN        	CR-03158 (Mapping of extra fields from Mirror user to New User)      
/*****************************************************************************************************************************/
Public Class POC_CreateOnboardingUser_New
{
   @InvocableMethod
    Public static void createUser(List<Id> userOnboardingRequestId)
    {
          List<User> listUsersToInsert=new List<User>();           
          String insertedUserId;
          List<ID> userIds = new List<ID>();
          system.debug('userOnboardingRequestId======='+userOnboardingRequestId);
        If(userOnboardingRequestId.size()>0)
        {         
            List<User_Onboarding_Request__c> uSRList=new List<User_Onboarding_Request__c>();  
            
            uSRList=[SELECT Active__c,Alias__c,Address__c,Business_Unit__c,Business_Vertical__c,Bypass_Product_Lookup_Filter_On_Asset__c,
                Charge_Code__c,Comments__c,Company_Name__c,CreatedById,CreatedDate,CurrencyIsoCode,Currency__c,Department__c,
                Division__c,Email_Encoding__c,Email__c,Exception__c,Exempt_from_Deactivation_Protocol__c,
                External_Rep__c,Federation_ID__c,First_Name__c,Flow_User__c,Function__c,Geography__c,Group__c,
                Id,Incident_Number__c,IsDeleted,Job_Function__c,Knowledge_User__c,Language__c,
                LastModifiedById,LastModifiedDate,LastViewedDate,License_Pool__c,
                Line_of_Business__c,Live_Agent_User__c,Locale__c,Manager__c,Marketing_User__c,Market__c,
                Mirror_User__c,Name,Offline_User__c,Onboarding_User__c,Physical_Address__c,
                Physical_City__c,Physical_State__c,Physical_Zip__c,Price_List__c,Product_Business_Unit__c,
                Profile_Name__c,Profile__c,Quota_Carrying__c,RecordTypeId,Region__c,Remaining_License__c,
                Role__c,Sales_Force__c,SAP_Account__c,Service_Cloud_User__c,Specialty__c,Special_Price_RM_Approval_Group__c,
                Status__c,Support_Tier__c,SystemModstamp,Time_Zone__c,Title__c,User_Deactivation_Id__c,
                User_Deactivation_Input__c,User_License_Tracker__c,Re_activation_User_Name__c,Nickname__c,Username__c,Deactivation_User_Id__c,User_License__c,User_Re_activation__c,Manager__r.id FROM User_Onboarding_Request__c where id=:userOnboardingRequestId];
                Map<Id,Id> mirrorUserMap=new Map<Id,Id>();      
               for(User_Onboarding_Request__c objUSR:uSRList)
               {
                    string FullName = objUSR.First_Name__c+objUSR.Name;
                        
                    User usrRecordToInsert=new User();                   
                    usrRecordToInsert.Alias= FullName.substring(0,4);
                    usrRecordToInsert.Email=objUSR.Email__c;
                    usrRecordToInsert.FederationIdentifier = objUSR.Email__c;
                    usrRecordToInsert.LastName=objUSR.Name;
                    usrRecordToInsert.FirstName=objUSR.First_Name__c;
                    if(objUSR.Alias__c!=null)
                        usrRecordToInsert.CommunityNickname=objUSR.Name;
                    usrRecordToInsert.Username=objUSR.Email__c+'.OneCRM';
                    usrRecordToInsert.User_Onboarding_Request_Id__c=objUSR.Id;
                    //usrRecordToInsert.License_Tracker_Id__c=objUSR.User_License_Tracker__c;
                    usrRecordToInsert.License_Pools__c = objUSR.License_Pool__c;
                    usrRecordToInsert.ManagerID = objUSR.Manager__c;                        
                   system.debug('Manager1234' + objUSR.Manager__r.id);
                    //If mirror user provided in the request then get mirror user details to create new onboarding User.
                    if(objUSR.Mirror_User__c!=null)
                    {
                        User mirrorUserDetails=[select id,LocaleSidKey,EmailEncodingKey,UserRoleId,CompanyName,Country,CountryCode,CurrencyIsoCode,Division,ProfileId,License_Pool__c,LanguageLocaleKey,TimeZoneSidKey,
                        ManagerId,Market__c,Line_of_Business__c,Business_Unit__c,Geography__c,Product_Business_Unit__c,Price_List__c,Catalog__c,Special_Price_RM_Approval_Group__c,
                        Specialty__c,Quota_Carrying__c,Business_Vertical__c,Group__c,Region__c,Support_Tier__c,TEP_Operating_Unit__c,Default_ERP_Order_Type__c,CallCenterId, 
                        UserPermissionsKnowledgeUser,UserPermissionsLiveAgentUser,UserPermissionsMarketingUser,UserPermissionsSupportUser from user where id=:objUSR.Mirror_User__c]; 
                         
                        usrRecordToInsert.CurrencyIsoCode=mirrorUserDetails.CurrencyIsoCode;                       
                        usrRecordToInsert.EmailEncodingKey=mirrorUserDetails.EmailEncodingKey;
                        usrRecordToInsert.LanguageLocaleKey=mirrorUserDetails.LanguageLocaleKey;
                        usrRecordToInsert.ProfileId=mirrorUserDetails.ProfileId;                        
                        usrRecordToInsert.LocaleSidKey=mirrorUserDetails.LocaleSidKey;
                        usrRecordToInsert.Market__c=mirrorUserDetails.Market__c;
                        usrRecordToInsert.TimeZoneSidKey=mirrorUserDetails.TimeZoneSidKey;
                        usrRecordToInsert.Line_of_Business__c=mirrorUserDetails.Line_of_Business__c;
                        usrRecordToInsert.Business_Unit__c=mirrorUserDetails.Business_Unit__c;
                        usrRecordToInsert.Geography__c=mirrorUserDetails.Geography__c;
                        usrRecordToInsert.Product_Business_Unit__c=mirrorUserDetails.Product_Business_Unit__c;
                        usrRecordToInsert.Price_List__c=mirrorUserDetails.Price_List__c;
                        usrRecordToInsert.Catalog__c=mirrorUserDetails.Catalog__c;
                        usrRecordToInsert.Special_Price_RM_Approval_Group__c=mirrorUserDetails.Special_Price_RM_Approval_Group__c;
                        usrRecordToInsert.Specialty__c=mirrorUserDetails.Specialty__c;
                        usrRecordToInsert.Quota_Carrying__c=mirrorUserDetails.Quota_Carrying__c; 
                        usrRecordToInsert.Business_Vertical__c=mirrorUserDetails.Business_Vertical__c;
                        usrRecordToInsert.Group__c=mirrorUserDetails.Group__c;
                        usrRecordToInsert.Region__c=mirrorUserDetails.Region__c;
                        usrRecordToInsert.Support_Tier__c=mirrorUserDetails.Support_Tier__c;
                        usrRecordToInsert.TEP_Operating_Unit__c=mirrorUserDetails.TEP_Operating_Unit__c;
                        usrRecordToInsert.Default_ERP_Order_Type__c=mirrorUserDetails.Default_ERP_Order_Type__c;
                        usrRecordToInsert.CallCenterId=mirrorUserDetails.CallCenterId;                        
                        usrRecordToInsert.Is_Mirror_User_Provided__c=true;
                        usrRecordToInsert.UserPermissionsKnowledgeUser = mirrorUserDetails.UserPermissionsKnowledgeUser;
                        usrRecordToInsert.UserPermissionsLiveAgentUser = mirrorUserDetails.UserPermissionsLiveAgentUser;
                        usrRecordToInsert.UserPermissionsMarketingUser = mirrorUserDetails.UserPermissionsMarketingUser;
                        usrRecordToInsert.UserPermissionsSupportUser = mirrorUserDetails.UserPermissionsSupportUser;
                        mirrorUserMap.put(objUSR.id,objUSR.Mirror_User__c);
                    }
                    else
                   {                   
                       //When mirror user details not provided in the request.Get details from the user onboarding requst record.
                        usrRecordToInsert.CurrencyIsoCode=objUSR.CurrencyIsoCode;                       
                        usrRecordToInsert.EmailEncodingKey=objUSR.Email_Encoding__c;
                        usrRecordToInsert.LanguageLocaleKey=objUSR.Language__c;
                        usrRecordToInsert.ProfileId=objUSR.Profile__c;                        
                        usrRecordToInsert.LocaleSidKey=objUSR.Locale__c;
                        usrRecordToInsert.Market__c=objUSR.Market__c;
                        usrRecordToInsert.TimeZoneSidKey=objUSR.Time_Zone__c;
                        usrRecordToInsert.UserRoleId=objUSR.Role__c;
                                               
                   } 
                   
                    listUsersToInsert.add(usrRecordToInsert); 
                                                    
               }
               
                Set<Id> newlyCreatedUserIds=new Set<Id>();
                if(listUsersToInsert.size()>0)
                {
                    object UserOnboardingReqId;
                    Database.SaveResult[] srList = Database.insert(listUsersToInsert,false);
                    for(integer i=0; i< srList.size(); i++) 
                    {
                        if (srList.get(i).isSuccess() == false)
                        {  
                            for(Database.Error err : srList.get(i).getErrors()) 
                             { 
                                 UserOnboardingReqId=listUsersToInsert[i].get('Id');
                                 system.debug('=========error'+err.getMessage());
                             }
                        }
                        else
                        {
                              UserOnboardingReqId=listUsersToInsert[i].get('Id');   
                              newlyCreatedUserIds.add((Id)UserOnboardingReqId);
                              system.debug('=========sucess'+UserOnboardingReqId);
                        }    
                    }
                }
                
            POC_MirrorUserPermissionAssignment.addPermissionSet(newlyCreatedUserIds,mirrorUserMap);
      POC_MirrorUserPermissionAssignment.Addrole(newlyCreatedUserIds,mirrorUserMap);
        }
                
    }
}