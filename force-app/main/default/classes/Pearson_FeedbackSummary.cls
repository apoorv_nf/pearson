public class Pearson_FeedbackSummary {
 @AuraEnabled
    public static Id createFlowRecord(Integer type, String artId, String Name, String comment, Integer rScore, String reasons ,String CommunityType) {
       
        list<Article_Vote_Summary__c> artSummary = [Select Id,KB_Article_ID__c FROM Article_Vote_Summary__c WHERE KB_Article_ID__c=:artId];
        if(artSummary.isEmpty())
        {
            Article_Vote_Summary__c artSum = new Article_Vote_Summary__c();
            artSum.Name = Name;
            artSum.KB_Article_ID__c =artId;
            artSum.Community_Type__c =CommunityType;
            try{
                insert artSum;    
            } catch(Exception e)
            {
            }
            Id artlId = artSum.Id;  
            return(createChild(artlId,type,comment,Name,rScore,reasons));
        } else{
            return(createChild(artSummary.get(0).Id,type,comment,Name,rScore,reasons));
        }
    }
    
    private static Id createChild(Id kId,integer vot,String comment,String Name,Integer rScore, String reasons) {
        Article_Votes__c artVote = new Article_Votes__c();
        artVote.Name = Name;
        artVote.Feedback__c = comment;
        artVote.Vote__c	= Integer.valueOf(vot);
        artVote.KB_Article__c = kId;
		artVote.Recommendations_Score__c =rScore;
        artVote.Reason_Not_Helpful__c =reasons;
        try{
            insert artVote;   
        } catch(Exception e)
        {
        }
        return artVote.Id;
    }
    
}