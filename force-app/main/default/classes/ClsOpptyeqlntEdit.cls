/*----------------------------------------------------------------------------------------------------------------------------------------------------------
Date             Version         Author                             Summary of Changes 
----            ---------      ---------                     ------------------------------
17-Oct-2019        2          Anurutheran                  CR-02949-Apttus Decommission(Changes)
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public with sharing class ClsOpptyeqlntEdit 
{
    ApexPages.StandardController myStdController;
    public string pceId{get;set;}
    public boolean isError{get;set;}
    public boolean PearsonCourseCodeNameisNull{get;set;}
    public Opportunity_Pearson_Course_Code__c pceList{get;set;}
    
      
           
    //For Course Object
    public Pearson_Course_Equivalent__c  CourseList{get;set;}
    public boolean PearsonCourseCodeisNull{get;set;}
    public string pceCourseId{get;set;}
         
    public ClsOpptyeqlntEdit(ApexPages.StandardController stdController) 
    {
        //myStdController = (Opportunity_Pearson_Course_Code__c)stdController.getRecord();
        myStdController = stdController;
        pceList = new Opportunity_Pearson_Course_Code__c();
        PearsonCourseCodeNameisNull = false;
       
        if (Apexpages.currentPage().getParameters().get('id') != null && Apexpages.currentPage().getParameters().get('id') != '') 
        {
             pceId = Apexpages.currentPage().getParameters().get('id');
        } else 
        {
             pceId = '';
        }
         system.debug('pceid'+pceId ); 
         List<Opportunity_Pearson_Course_Code__c> opcc = new List<Opportunity_Pearson_Course_Code__c>();    
         // CP 12/05/2017 modified to support non-apttus objects
         //opcc = [select id,Pearson_Course_Code_Name__r.name from Opportunity_Pearson_Course_Code__c where id=:pceId ]; 
         //opcc = [select id,Pearson_Course_Code_Hierarchy__r.name,Pearson_Course_Code_Name__r.name from Opportunity_Pearson_Course_Code__c where id=:pceId ]; //Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran          
         opcc = [select id,Pearson_Course_Code_Hierarchy__r.name from Opportunity_Pearson_Course_Code__c where id=:pceId ]; //Added for CR-02949 Apttus Decommission(Changes) by Anurutheran
         if (opcc.size() > 0) 
         {
             pceList = opcc[0];
         }
         
         
         //For course Object
          CourseList = new Pearson_Course_Equivalent__c();
        PearsonCourseCodeisNull = false;
        if (Apexpages.currentPage().getParameters().get('id') != null && Apexpages.currentPage().getParameters().get('id') != '') 
        {
             pceCourseId = Apexpages.currentPage().getParameters().get('id');
        } else 
        {
             pceCourseId = '';
        }
         system.debug('pceCourseId '+pceCourseId); 
         List<Pearson_Course_Equivalent__c> CourseCC = new List<Pearson_Course_Equivalent__c>();    
         // CP 12/05/2017 modified to support non-apttus objects
         //CourseCC  = [select id,name,Pearson_Course_Code__r.name from Pearson_Course_Equivalent__c where id=:pceCourseId ];  
         //CourseCC  = [select id,name,Pearson_Course_Code_Hierarchy__r.name,Pearson_Course_Code__r.name from Pearson_Course_Equivalent__c where id=:pceCourseId ];//Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran 
         CourseCC  = [select id,name,Pearson_Course_Code_Hierarchy__r.name from Pearson_Course_Equivalent__c where id=:pceCourseId ]; //Added for CR-02949 Apttus Decommission(Changes) by Anurutheran 
         if (CourseCC.size() > 0) 
         {
             CourseList = CourseCC[0];
         }
     
         
    }    
   
}