public class UpdateQuoteStatus{
    @InvocableMethod
    public Static void updateStatus(List<Id> quoteIDs){
       List<Quote> lstQut = [Select Id,Name,Status,Manual_Discount__c from Quote where ID in : quoteIDs];
       List<Quote> updQuotes = new List<Quote>();
       
       for(Quote qt : lstQut){
         
         if(qt.Manual_Discount__c != NULL){  
           qt.Manual_Discount__c = NULL;
           updQuotes.add(qt);
         }
         else {
             if(qt.Manual_Discount__c == NULL && qt.Status == 'Approved'){
               qt.Status = 'Needs Pricing';
               updQuotes.add(qt);
             }
         }
       }
       update updQuotes;
    }
}