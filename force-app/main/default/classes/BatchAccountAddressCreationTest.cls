@isTest
public class BatchAccountAddressCreationTest{
    static testMethod void  myUnitTest() { 
    List<User> listWithUser = new List<User>();
    
    //listWithUser = TestDataFactory.createUser([select Id from Profile where Name=:'System Administrator'].Id,1);
     User usr;
     usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Price_List__c= 'Math & Science');

        usr.market__c='UK';
        usr.Business_Unit__c='Schools';
        usr.Line_of_Business__c='Schools';
        usr.License_Pools__c='UK Marketing';
        usr.Group__c='Primary';
        insert usr;
     /* Set < Id > permissionSetIds = new Set < Id >();
      for ( SetupEntityAccess access : [ SELECT ParentId FROM SetupEntityAccess WHERE SetupEntityId IN ( SELECT Id FROM CustomPermission WHERE DeveloperName = :permissionName )]) 
      permissionSetIds.add(access.ParentId);
      userList = [SELECT Username FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId IN :permissionSetIds AND AssigneeId =: userId )];*/
      PermissionSet ps = new PermissionSet();
      ps.Name = 'Test';
      ps.Label = 'Test';
      insert ps;
      SetupEntityAccess sea = new SetupEntityAccess();
      sea.ParentId = ps.id;
      sea.SetupEntityId = [select Id from CustomPermission where DeveloperName = 'PS_BypassDupeCheck' Limit 1].id;
      insert sea;
      System.debug('SetupEntityAccess:'+sea);
      PermissionSetAssignment psa = new PermissionSetAssignment();
      psa.AssigneeId = usr.Id;
      psa.PermissionSetId = ps.id;
      insert psa;
      System.debug('PermissionsetAssignment:'+psa);


       
    System.assert(usr.id != null, 'User creation failed'); 
    //Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
    //insert byp;
    RecordType accrt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account' AND Name ='School'];    
    system.runAs(usr){
    Account accountRecord = new Account(Name='Test B2B R6 School',Market2__c='UK',Line_of_Business__c='Schools',Business_Unit__c='Schools',Group__c='Primary',Organisation_Type__c ='School',Type__c='Secondary',Sub_Type__c='Academy Second',
                        Phone='+91000001', ShippingCountry = 'United kingdom',ShippingState = 'greater london', ShippingCity = 'london', ShippingStreet = 'BNG', ShippingPostalCode = 'SE27 0AA',billingstreet='BNG', billingcity='London', billingstate='Greater london',billingcountry='United Kingdom',billingpostalcode='SE27 0AA');  

    accountRecord.recordtypeid=accrt.Id;
    accountRecord.ShippingCountry='United kingdom';
    insert accountRecord;
    System.debug('accountRecord...'+accountRecord);
    System.assert(accountRecord != null, 'Account creation failed'); 
    Test.starttest();
    //String accid=accountRecord.id;
    BatchAccountAddressCreation ptoc = new BatchAccountAddressCreation(1,false);
    Database.executeBatch(ptoc);
    Test.stoptest();
}
   }
  }