@isTest
public  class PS_AddNewTeamMember_Test {
public static testmethod void insertteammember1()
{

    id p2id=[select id from profile where name='Pearson Sales User OneCRM'].id;
    User u3 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
    LocaleSidKey='en_US', ProfileId = p2id, 
    TimeZoneSidKey='America/Los_Angeles', UserName='test1234'+Math.random()+'@gmail.com', Geography__c = 'Growth',Market__c='UK' , Business_Unit__c='Clinical',Line_of_Business__c = 'Higher Ed');
    insert u3;
    
    List<RecordType> rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account' AND Name ='School' LIMIT 1]; 
    Account acc1 = new Account(name='Test1',IsCreatedFromLead__c = True,Line_of_Business__c='Higher Ed',Geography__c='Growth',Phone='+9100000',ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037');
    insert acc1;
    system.assert('Account created successfully:'+ acc1.id != null);
           
    AccountTeamMember atm3 = new AccountTeamMember(accountid=acc1.id,UserId=u3.id,TeamMemberRole='Sales' );
    insert atm3;
    AccountTeamMember atm21 = atm3;
    system.runas(u3)
     {
     
     ApexPages.StandardController sc1 = new ApexPages.standardController(atm21);
     PS_AddNewTeamMemberController atm4 = new PS_AddNewTeamMemberController(sc1);
     atm4.acctid = acc1.id;
     atm4.addTeamMemberAndAccountShare();
     atm4.getAccountTeamMembers();

      }
}

public static testmethod void insertteammember()

{

      id pid=[select id from profile where name='Pearson Sales User OneCRM'].id;
      id p1id=[select id from profile where name='Pearson Sales User OneCRM'].id;
      User u1 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p1id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='test1234'+Math.random()+'@gmail.com', Geography__c = 'Growth',Market__c='UK' , Business_Unit__c='Clinical',Line_of_Business__c = 'Higher Ed');
      insert u1;
      Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u1.id,Disable_Validation_Rules__c=true);
        insert byp;
      system.runas(u1)
      {
      Bypass_Settings__c byp1 = [select id,Disable_Validation_Rules__c from Bypass_Settings__c where SetupOwnerId=:u1.id];
        byp1.Disable_Validation_Rules__c = true;
        upsert byp1;
      TestClassAutomation.FillAllFields = true;
      boolean displayForm=false;
      User u2 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = pid, 
      TimeZoneSidKey='America/Los_Angeles', UserName='test1234'+Math.random()+'@gmail.com', Geography__c = 'Growth',Market__c = 'ZA',Line_of_Business__c = 'Higher Ed');
      insert u2;
         
         
      List<RecordType> rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account' AND Name ='School' LIMIT 1]; 
      Account acc = new Account(name='Test1',IsCreatedFromLead__c = True,Line_of_Business__c='Higher Ed',Geography__c='Growth',Phone='+9100000',ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037');
      insert acc;
      system.assert('Account created successfully:'+acc.id != null);
      AccountTeamMember atm2 = new AccountTeamMember(accountid=acc.id,UserId=u2.id,TeamMemberRole='Sales' );
      insert atm2;
      AccountTeamMember atm = atm2;    
      PageReference testPage = new pagereference('/apex/PS_AddNewTeamMember');
      id atmid= atm.accountid;
      testPage.getParameters().put('AccountID',atmid);
      Test.setCurrentPage(testPage);
      ApexPages.StandardController sc = new ApexPages.standardController(atm);
      PS_AddNewTeamMemberController atm1 = new PS_AddNewTeamMemberController(sc);
      try{
      PS_AddNewTeamMemberController.AccountTeamMemberSelected  wrap = new  PS_AddNewTeamMemberController.AccountTeamMemberSelected(atm);
      }Catch(Exception e){}
      Test.starttest();
             
      atm1.doSaveTeamMember();
      atm1.SaveAndExitmessage();
      atm1.getItems();
      atm1.getpermissions();
      atm1.selectedteammember = UserInfo.getUserId();
      atm1.accountaccess='Read';
      atm1.contactaccess='Read';
      atm1.opportunityaccess='Read';
      atm1.caseaccess='Read';  
      atm1.getAccountTeamMembers();
      atm1.DeleteAndExitmessage();
      atm1.selectedteammember = u2.Id;
      atm1.updatepermission();
      atm1.redirectToUpdateChildPage();
      
      Test.stopTest();
      
      }
         
  }
  
}