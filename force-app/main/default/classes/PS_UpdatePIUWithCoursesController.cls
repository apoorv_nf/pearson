/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_UpdatePIUWithCoursesController
   Description:     Allows user to update PIU(s)
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   13 oct 2015      1.0           Accenture NDC                                         Created
   09 Sep 2019      1.1           Anurutheran             Modified class for September CI CR-02850: Adding Channel detail picklist 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
 

public class PS_UpdatePIUWithCoursesController
{
    public string opportunityID;
    public Map<Id,String> lstWithOpptyCourse{get;set;} 
    private integer counter=0;  //keeps track of the offset
    private integer list_size=10; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list  
    public Set<Id> setWithCourseId{get;set;}
    public set<Id> setWithUpdatingAssetId;
    public string selectedAssetId{get;set;}
    public string selectedFieldVal{get;set;}
    public String selectedUsageVal{get;set;}
    public string selectedModeOfDelVal{get;set;} 
    public string  selectedCourseId{get;set;}
    public List<innerPIUWrapperSelected> lstWithWraperRec;
    public List<innerPIUWrapper> PIUWrapperRecords{get;set;}
    public String channelStr{get;set;}
    public List<innerPIUWrapper> lstWithModifiedWrapperRecords;
    public String depFieldJsonMap{get;set;}  //CR-02850
    public PS_UpdatePIUWithCoursesController()
    {
         PIUWrapperRecords = new List<innerPIUWrapper>();
         lstWithWraperRec = new List<innerPIUWrapperSelected>();
         lstWithModifiedWrapperRecords = new List<innerPIUWrapper>();
         setWithCourseId = new Set<Id>();
         lstWithOpptyCourse = new Map<Id,String>();
         opportunityID = ApexPages.currentPage().getParameters().get('opportunityId');
         channelStr = ApexPages.currentPage().getParameters().get('channel');
         depFieldJsonMap=JSON.serialize(PS_GlobalPickListUtilities.assetChannelDetail()); //CR-02850
         if(opportunityID != null)
         {
             for(OpportunityUniversityCourse__c opptyUnviCourse : [select UniversityCourse__c,UniversityCourse__r.Name,Opportunity__c from OpportunityUniversityCourse__c where Opportunity__c =: OpportunityId ])
             { 
                 //lstWithOpptyCourse.add(opptyUnviCourse);
                 setWithCourseId.add(opptyUnviCourse.UniversityCourse__c);
             } 
             total_size = [select count() from Course_Product_in_Use__c where Course__c IN : setWithCourseId AND (Product_in_Use__r.Status__c = 'Active' OR Product_in_Use__r.Status__c = 'Pending') AND Product_in_Use__c != null AND Product_in_Use__c != ''];   
         }       
         getAssetUniversityCourse();
    }
       
    public List<innerPIUWrapper> getAssetUniversityCourse()
    {
        try
        {
            if(PIUWrapperRecords != null)
            {
                PIUWrapperRecords.clear();
            }
            if(lstWithOpptyCourse != null)
            {
                lstWithOpptyCourse.clear();
            }
            for(Course_Product_in_Use__c universityCourse : [select Id,Product_in_Use__c,Course__c,Course__r.Name,Product_in_Use__r.Product2Id,Product_in_Use__r.Quantity,Product_in_Use__r.Product2.Name,Product_in_Use__r.Usage__c,Product_in_Use__r.Digital_Usage__c,Product_in_Use__r.Status__c,Product_in_Use__r.Mode_of_Delivery__c,Product_in_Use__r.Third_Party_LMS__c,Product_in_Use__r.Channel__c,Product_in_Use__r.InstallDate,Product_in_Use__r.Expiry_Date__c,Product_in_Use__r.Name,Product_in_Use__r.Channel_Detail__c from Course_Product_in_Use__c where Course__c IN : setWithCourseId AND (Product_in_Use__r.Status__c = 'Active' OR Product_in_Use__r.Status__c = 'Pending') AND Product_in_Use__c != null AND Product_in_Use__c != '' ORDER BY Product_in_Use__r.InstallDate DESC LIMIT : list_size OFFSET: counter]) 
            {
                lstWithOpptyCourse.put(universityCourse.Course__c,universityCourse.Course__r.Name);
                //Jaydip changes below to use asset Name instead of asset product name
                //PIUWrapperRecords.add(new innerPIUWrapper(universityCourse.Product_in_Use__r.Product2.Name,universityCourse.Id,universityCourse.Product_in_Use__c,universityCourse.Course__c,universityCourse.Product_in_Use__r.Usage__c,universityCourse.Product_in_Use__r.Quantity,universityCourse.Product_in_Use__r.Status__c,universityCourse.Product_in_Use__r.Mode_of_Delivery__c,universityCourse.Product_in_Use__r.Third_Party_LMS__c,universityCourse.Product_in_Use__r.Channel__c,universityCourse.Product_in_Use__r.InstallDate,universityCourse.Product_in_Use__r.Expiry_Date__c));
               PIUWrapperRecords.add(new innerPIUWrapper(universityCourse.Product_in_Use__r.Name,universityCourse.Id,universityCourse.Product_in_Use__c,universityCourse.Course__c,universityCourse.Product_in_Use__r.Usage__c,universityCourse.Product_in_Use__r.Quantity,universityCourse.Product_in_Use__r.Status__c,universityCourse.Product_in_Use__r.Mode_of_Delivery__c,universityCourse.Product_in_Use__r.Third_Party_LMS__c,universityCourse.Product_in_Use__r.Channel__c,universityCourse.Product_in_Use__r.InstallDate,universityCourse.Product_in_Use__r.Expiry_Date__c,universityCourse.Product_in_Use__r.Channel_Detail__c)); //CR-02850
            }
         }catch(Exception e)
         {
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
             ApexPages.addMessage(myMsg); 
         } 
         return PIUWrapperRecords;    
    }
    
    //get Usage picklist values from  Asset object using describe method
    public List<SelectOption> getUsage()
    {
        system.debug(PS_GlobalPickListUtilities.assetUsage());
        return PS_GlobalPickListUtilities.assetUsage();    
    }
    
    //get Status picklist values from Asset object using describe method
    public List<SelectOption> getStatus()
    {
        return PS_GlobalPickListUtilities.assetStatus();
    }
    //get Mode Of Delivery picklist values from Asset object using describe method
    public List<SelectOption> getModeOfDelivery()
    {
        return PS_GlobalPickListUtilities.assetModeOfDelivery();
    }
    //get Third Party LMS picklist values from Asset object using describe method
    public List<SelectOption> getThirdPartyLMS()
    {
        return PS_GlobalPickListUtilities.assetThirdPartyLMS();
    }
    //get Channel picklist values from Asset object using describe method
    public List<SelectOption> getChannel()
    {
        return PS_GlobalPickListUtilities.assetChannel();
    }
    //CR-02850: Changes Start
    //get Channel Detail picklist values from Asset object using describe method
      public List<SelectOption> getChannelDetail()
    {    
        Map<Object,List<String>> DependentFieldValMap=PS_GlobalPickListUtilities.assetChannelDetail();
        List<String>ple=DependentFieldValMap.get(channelStr);
        return PS_GlobalPickListUtilities.selectOptionUtility(ple);
    }
    //CR-02850: Changes End

    public PageReference Beginning() 
    { 
        //user clicked beginning
        createNewWrapperRecord();
        counter = 0;
        getAssetUniversityCourse();
        return null;
    }

    public PageReference Previous() 
    { 
        //user clicked previous button
        createNewWrapperRecord();
        counter -= list_size;
        getAssetUniversityCourse();
        return null;
    }

    public PageReference Next() 
    { 
        //user clicked next button
        createNewWrapperRecord();
        counter += list_size;
        getAssetUniversityCourse();
        return null;
    }

    public PageReference End() 
    { 
        //user clicked end
        createNewWrapperRecord();
        counter = total_size - math.mod(total_size, list_size);
        getAssetUniversityCourse();
        return null;
    }

    public Boolean getDisablePrevious() 
    { 
        //this will disable the previous and beginning buttons
        if (counter>0) 
        {
            return false;
        }else 
        {
            return true;
        }
    }

    public Boolean getDisableNext() 
    { 
        //this will disable the next and end buttons
        if (counter + list_size < total_size) 
        {
            return false; 
        }else 
        {
            return true;
        }
    }

    public Integer getTotal_size() 
    {
        return total_size;
    }

    public Integer getPageNumber() 
    {
        return counter/list_size + 1;
    }

    public Integer getTotalPages() 
    {
        if(math.mod(total_size, list_size) > 0) 
        {
            return total_size/list_size + 1;
        }else 
        {
            return (total_size/list_size);
        }
    }
    
    public pageReference cancel()
    {
        pageReference returnToOppty =new pageReference('/'+opportunityID);
        return returnToOppty ;
    }
    
    public void createNewWrapperRecord()
    {
        try
        {
        Integer counter =0;
        boolean counterFlag = false;
        Integer wrapperIndex;
        boolean recordNotFound = false;
        if(selectedAssetId != null)
        {
           if(!lstWithWraperRec.isEmpty() && lstWithWraperRec != null)
           {
               for(innerPIUWrapperSelected innerPIU : lstWithWraperRec)
               {
                   if(innerPIU.innerAssetID == selectedAssetId && innerPIU.innerCourseId == selectedCourseId)
                   {
                       wrapperIndex = counter;
                       counterFlag = true;
                       break;
                   }else
                   {
                       recordNotFound = true;
                   }
                       counter = counter+1;
               }
           }else
           {
               lstWithWraperRec.add(new innerPIUWrapperSelected(selectedAssetId,selectedFieldVal,selectedUsageVal,selectedModeOfDelVal,selectedCourseId));
           }  
                    
           if(counterFlag && wrapperIndex != null)
           {
               lstWithWraperRec[wrapperIndex].innerStatusVal = selectedFieldVal;
               lstWithWraperRec[wrapperIndex].innerUsageVal = selectedUsageVal;
               lstWithWraperRec[wrapperIndex].innerModeOfDelivery = selectedModeOfDelVal;
           }else 
           if(recordNotFound)
           {
               lstWithWraperRec.add(new innerPIUWrapperSelected(selectedAssetId,selectedFieldVal,selectedUsageVal,selectedModeOfDelVal,selectedCourseId)); 
           }
           }
        }catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
            ApexPages.addMessage(myMsg); 
        }
    }
    
    public pageReference updatePIU()
    {
        pageReference returnToOppty =new pageReference('/'+opportunityID);
        List<Asset> lstWithAsset = new List<Asset>();
        Asset newAsset;
        setWithUpdatingAssetId = new Set<Id>();
        
        if(!lstWithWraperRec.isEmpty() && lstWithWraperRec!= null)
        {
            for(innerPIUWrapperSelected assetRec : lstWithWraperRec)
            {
                newAsset = new Asset();
                newAsset.id = assetRec.innerAssetId;
                newAsset.Status__c = assetRec.innerStatusVal;
                newAsset.Usage__c = assetRec.innerUsageVal;
                newAsset.Mode_of_Delivery__c = assetRec.innerModeOfDelivery;
                lstWithAsset.add(newAsset);
                system.debug('lstWithAsset'+lstWithAsset);
                setWithUpdatingAssetId.add(assetRec.innerAssetId);
            }
            if(!lstWithAsset.isEmpty() && lstWithAsset !=null)
            {
                try
                {
                    update lstWithAsset;
                    Map<Id,Set<Id>> mapWithContactProductInUse = new Map<Id,Set<Id>>();
                    Map<Id,Set<Id>> mapWithAssetUniversityCourse = new Map<Id,Set<Id>>();
                    Contact_Product_In_Use__c  contactProd;
                    List<Contact_Product_In_Use__c> lstWithContactProd = new List<Contact_Product_In_Use__c>();
                    Course_Product_in_Use__c univCourse;
                    List<Course_Product_in_Use__c > lstWithAssetUnivCourse = new List<Course_Product_in_Use__c >();
                    if(setWithUpdatingAssetId.size() > 0 && !setWithUpdatingAssetId.isEmpty())
                    {
                        for(Course_Product_in_Use__c assetUniv : [select id,Product_in_Use__c ,Course__c,Status__c from Course_Product_in_Use__c where Product_in_Use__c  IN : setWithUpdatingAssetId])
                        {
                            if(mapWithAssetUniversityCourse.containsKey(assetUniv.Product_in_Use__c))
                            {
                                mapWithAssetUniversityCourse.get(assetUniv.Product_in_Use__c).add(assetUniv.Id);
                            }else
                            {
                                Set<ID> setWithCourseId = new Set<ID>();
                                setWithCourseId.add(assetUniv.Id);
                                mapWithAssetUniversityCourse.put(assetUniv.Product_in_Use__c,setWithCourseId);
                            }
                        }
                        
                        for(Contact_Product_In_Use__c contactAsset : [select id,Product_in_Use__c,Status__c from Contact_Product_In_Use__c where Product_in_Use__c IN : setWithUpdatingAssetId])
                        {
                            if(mapWithContactProductInUse.containsKey(contactAsset.Product_in_Use__c))
                            {
                                mapWithContactProductInUse.get(contactAsset.Product_in_Use__c).add(contactAsset.Id);
                            }else
                            {
                                Set<ID> setWithContactId = new Set<ID>();
                                setWithContactId.add(contactAsset.Id);
                                mapWithContactProductInUse.put(contactAsset.Product_in_Use__c,setWithContactId);
                            }
                        }
                    }
                    if(!mapWithAssetUniversityCourse.isEmpty())
                    {
                        for(Asset assetUpdt : lstWithAsset)
                        {
                            if(mapWithAssetUniversityCourse.containsKey(assetUpdt.Id))
                            {
                                for(ID assetDet : mapWithAssetUniversityCourse.get(assetUpdt.Id))
                                {
                                    univCourse = new Course_Product_in_Use__c (); 
                                    univCourse.Id = assetDet;
                                    univCourse.Status__c = assetUpdt.Status__c;
                                    lstWithAssetUnivCourse.add(univCourse); 
                                }
                            } 
                        }
                    }
                    if(!mapWithContactProductInUse.isEmpty())
                    {
                        system.debug(mapWithContactProductInUse);
                        
                        for(Asset assetUpdt : lstWithAsset)
                        {
                           if(assetUpdt.Id != null && mapWithContactProductInUse.containsKey(assetUpdt.Id))
                           {
                               for(ID contactDet : mapWithContactProductInUse.get(assetUpdt.Id))   
                               {                       
                                    contactProd = new Contact_Product_In_Use__c();
                                    contactProd.Id = contactDet ;
                                    contactProd.Status__c = assetUpdt.Status__c;
                                    lstWithContactProd.add(contactProd);
                                }
                            }
                        }
                    }
                    if(!lstWithContactProd.isEmpty() && lstWithContactProd != null)  
                    {
                        database.update(lstWithContactProd,false); 
                    }   
                    if(lstWithAssetUnivCourse !=null && !lstWithAssetUnivCourse.isEmpty())
                    {        
                        database.update(lstWithAssetUnivCourse,false);
                    }    
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Updated Successfully');
                    ApexPages.addMessage(myMsg); 
                }catch(Exception e)
                {
                    if(e.getMessage().contains('Value does not exist or does not match filter criteria.: [Product2Id]'))
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'You cannot create PIU for product(s) that does not belong to your market.');
                        ApexPages.addMessage(myMsg);
                    }else
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
                        ApexPages.addMessage(myMsg);
                    }
                    return null;
                }    
            }
        }else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Please change atleast one row and click update or click cancel to proceed' );
            ApexPages.addMessage(myMsg); 
            return null;
        } 
        return returnToOppty;    
    }
    
    public class innerPIUWrapper
    {
        public ID innercourseProdInUseId{get;set;}
        public ID innerAssetId{get;set;} 
        public ID innerCourseId{get;set;} 
        public string innerUsage{get;set;} 
        public Decimal innerQuantity{get;set;} 
        public string innerStatus{get;set;} 
        public string innerModeOfDelivery{get;set;}  
        public string innerThirdPartyLMS{get;set;} 
        public string innerChannel{get;set;} 
        public string innerChannelDetail{get;set;}//CR-02850
        public date innerInstallDate{get;set;} 
        public date innerExpiryDate{get;set;} 
        public String innerProductName{get;set;}
        public innerPIUWrapper(string prodName,ID courseProdInUseId,ID assetId,ID courseId,string usage,Decimal quantity,string status,string modeOfDelivery,string thirdPartyLMS,string channel,date installDate,date expiryDate,string channelDetail)
        {
            innercourseProdInUseId = courseProdInUseId;
            innerProductName = prodName;
            innerAssetId = assetId;
            innerCourseId = courseId;
            innerUsage = usage;
            innerQuantity = quantity;
            innerStatus = status;
            innerModeOfDelivery = modeOfDelivery;
            innerThirdPartyLMS = thirdPartyLMS;
            innerChannel = channel;
            innerChannelDetail = channelDetail;//CR-02850
            innerInstallDate = installDate;
            innerExpiryDate = expiryDate;
        }
    }
    
    public class innerPIUWrapperSelected
    {
        public string innerAssetId;
        public string innerStatusVal;
        public string innerUsageVal;
        public string innerModeOfDelivery;
        public string innerCourseId;
        public innerPIUWrapperSelected(string assetDetail,string statusVal,string usageVal,string modeOfDel,string courseId)
        {
            innerAssetId = assetDetail;
            innerStatusVal = statusVal;
            innerUsageVal = usageVal;
            innerModeOfDelivery = modeOfDel;
            innerCourseId = courseId;
        }
    }
}