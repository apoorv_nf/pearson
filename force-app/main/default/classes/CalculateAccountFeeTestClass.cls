/*******************************************************************************************************************
* Apex Class Name  : CalculateAccountFeeTestClass
* Version          : 1.0 
* Created Date     : 06 May 2015
* Function         : Test Class of the CalculateAccountFee Class
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*                      06/05/2015              Created Initial Version of CalculateAccountFeeTestClass
*******************************************************************************************************************/
@isTest 
public class CalculateAccountFeeTestClass {
    
    static testMethod void VerifyCalculateFee()
    {
        List<Account> accList = TestDataFactory.createAccount(5, 'Learner');
        Integer i=0;
        User u = new User();
        u.DefaultCurrencyISOCode = 'NZD';
        u.id = UserInfo.getUserId();
        update u;
        
        for(Account acc : accList){
            acc.Annual_Fee__c = 3000;
            if(i==0)
        		acc.CurrencyIsoCode ='GBP';
            else if(i==1)
                acc.CurrencyIsoCode ='AUD';
            else if(i==2)
                acc.CurrencyIsoCode ='NZD';
            else if(i==3)
                acc.CurrencyIsoCode ='USD';
            else if(i==4)
                acc.CurrencyIsoCode ='ZAR';
            
            i++;
        }   
        insert accList;
        
        
    }   

}