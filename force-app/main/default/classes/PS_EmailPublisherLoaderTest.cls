/*----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_EmailPublisherLoaderTest.cls 

Date             Version              Author                         
-----------      ----------      -----------------   
22-Jan-2016                       Sakshi Agarwal       
26-May-2017                       Christwin Durai
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
@isTest

public class PS_EmailPublisherLoaderTest{
    static testmethod void EmailTest()
        {
       
        Exception failureDuringExecution = null;
        User u1= [SELECT Id,Name FROM User WHERE ID='005b0000003A3Cc'];
             Case c= new case();
            id caseid;
        System.runas(u1){
         List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        insert lstAccount ;
       
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        insert lstContact; 
        
             Product__c p = new Product__c();
            p.Active_in_Case__c = true;
            p.Active_in_Self_Service__c =true;
            p.Line_of_Bussiness__c ='Higher Education';
            insert p;
            
            // 26th May 2017 by Christwin for SA
            RecordType rectype = [SELECT Id FROM RecordType where Name = 'School Assessment' and sobjectType = 'Case'];
            if (rectype != null) {
                c.recordtypeId = rectype.id;
            }
            c.Program__c = 'PARCC';
            // 26th May 2017 by Christwin for SA
            
            c.To_Email_Origin_Address__c = 'a@a.com';
            c.origin ='Phone';
            c.status ='New';
            c.Account =lstAccount[0];
            c.Contact = lstContact[0];
            c.Contact_Type__c = 'College Educator';
            c.PS_Business_Vertical__c ='Higher Education';
            c.Request_Type__c = 'Technical Support';
            c.Platform__c = 'Other';
            c.Products__c =p.id;
            c.Category__c ='Scoring & Results';
            c.Subcategory__c ='Learning History & Reports';
            c.Subject ='Hi';
            c.Description = 'Hello';
            c.Priority ='Medium';
            c.Customer_Username__c ='Username1';
            c.Access_Code__c ='test';
            c.Error_Message_Code__c ='test';
            c.URL__c ='www.test.com';
            //c.RecordType.Name ='Technical Support';
        insert c; 
        c.origin ='Email';
        c.Program__c = 'ACCUPLACER';
        update c;
        c.origin ='Email';
        c.Program__c = 'NBPTS'; 
        update c;   
        caseid = c.id;                  
            
        }
          
        String defaultsAsJSON = '[{"targetSObject":{"attributes":{"type":"EmailMessage"},"TextBody":"",'
            + '"FromName":"Test","FromAddress":"test@example.com","HtmlBody":"<html><body></body></html>","BccAddress":"test@example.com",'
            + '"CcAddress":"","ToAddress":"test@example.com","Subject":"Testing"},"contextId":"'+caseid+'","actionType":"Email",'
            + '"actionName":"Case.Email","fromAddressList":["salesforce@test.com"]}]';
        system.debug(defaultsAsJSON+'****PRINT defaultsAsJSON****');
        List<QuickAction.SendEmailQuickActionDefaults> defaultsSettings = 
            (List<QuickAction.SendEmailQuickActionDefaults>)JSON.deserialize(defaultsAsJSON, List<QuickAction.SendEmailQuickActionDefaults>.class);
        PS_EmailPublisherLoader obj = new PS_EmailPublisherLoader();
         System.runas(u1){
        Test.startTest();
        try {
        (new PS_EmailPublisherLoader()).onInitDefaults(defaultsSettings);
        //obj.getFromAddress('test@test.com','Email');      
        }
        catch(Exception failure) { failureDuringExecution = failure; }
        Test.stopTest();
       }
        PS_EmailPublisherLoader psemail = new PS_EmailPublisherLoader();
       //psemail.getFromAddress('FromAddress','Email');
         
       }
       static testmethod void EmailTest1(){
        PS_EmailPublisherLoader psemail = new PS_EmailPublisherLoader();
       // psemail.getFromAddress('null','new');
        //psemail.getSAFromAddress('test','new');
       }
       
}