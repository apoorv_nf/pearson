/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_UserTriggerHandlerTest.class
Description:     Test Class for  PS_UserTriggerHandler.class
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
19/04/2016         1.0            Rohit Kulkarni         
24/06/2016         1.1            Abhinav Srivastava        Modified for test class fix.          
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
@isTest
public class PS_UserTriggerHandlerTest{
    
    static testMethod void test_Partner(){
       

        List<Contact> lstContact = TestDataFactory.createContact(1);
        lstContact[0].Role__c = 'Partner';        
        lstContact[0].MailingState = 'England';
        lstContact[0].sSelfServiceCreated__c = 'true';
        insert lstContact;
        
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name = 'Pearson Self-Service User'].Id,1);    
        listWithUser[0].Contactid = lstContact[0].id; 
        insert listWithUser; 
        
        String usrid=listWithUser[0].id;
            Test.startTest();
               System.runAs(listWithUser[0]){
               PS_UserTriggerHandler.addPermSet(usrid);
               }
            Test.stopTest();
        
    }
 static testMethod void test_Educator(){
       

        List<Contact> lstContact = TestDataFactory.createContact(1);
        lstContact[0].Role__c = 'Educator';
        lstContact[0].Role_detail__c = 'Instructor';
        lstContact[0].MailingState = 'England';
        lstContact[0].sSelfServiceCreated__c = 'true';
        //lstContact[0].Account = new Account(Name = 'AccTest1',);
        insert lstContact;
        
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name = 'Pearson Self-Service User'].Id,1);    
        listWithUser[0].Contactid = lstContact[0].id; 
        insert listWithUser; 
        
        String usrid=listWithUser[0].id;
            Test.startTest();
               System.runAs(listWithUser[0]){
               PS_UserTriggerHandler.addPermSet(usrid);
               }
            Test.stopTest();
        
    }

}