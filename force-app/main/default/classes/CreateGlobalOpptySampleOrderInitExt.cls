/*******************************************************************************************************************
* Apex Class Name       : CreateGlobalOpptySampleOrderInitExt
* Created Date          : 14 Sep 2018
* Function              : Implemented the JavaScript Button Validation in to Apex Class Validation to support Lightning
* 
* Developer                   Version        Date                      Description
* ------------------------------------------------------------------------------------------------------------------
* Shiva                        1.0       14/Sep/2018           Created Initial Version of CreateANZSampleExt
* Navaneeth                    1.1       18/Sep/2018           Modified the SOQL Query and Javscript Condition for Western Europe
* Navaneeth                    1.2       21/Sep/2018           Modifed the Validation to a Custom Permission Enabled for the user.
*******************************************************************************************************************/
public class CreateGlobalOpptySampleOrderInitExt {

    public Id OpportunityId {get; set;}
    public static ID OpptyId;
    public Opportunity OpportunityRec {get;set;}
    public User lstUser;
    public Id UserId {get; set;}
    public String errormsg {get; set;}
    public boolean displayPopup {get; set;}
    public List<OpportunityLineItem> oppLineItemList {get;set;}
    public boolean marketCheck = false;

    public CreateGlobalOpptySampleOrderInitExt (ApexPages.StandardController stdController) {
      
        opptyId = ApexPages.currentPage().getParameters().get('Id');
        OpportunityRec = (Opportunity)stdController.getRecord();        
        OpportunityId =  OpportunityRec.id;      
        userId = userinfo.getUserId();
        
    }    
    
    public PageReference closePopup() {
    
        displayPopup = false;
        PageReference pg = new  PageReference('/'+OpportunityId);
        return pg;
    }
    
    public pagereference opptySampleOrderValidation(){
    // Commented and Modified by Navaneeth - Start
    /*
       List<User> lstUsers = [SELECT Id, Market__c, Business_Unit__c FROM User 
                                    WHERE (Market__c='AU' OR Market__c='NZ' OR Market__c='DE' OR Market__c='EG' OR Market__c='NL' OR 
                                            Market__c='PL' OR Market__c='SE' OR Market__c='Global') AND 
                                          ( Business_Unit__c='Higher Ed' OR Business_Unit__c='Edify' OR 
                                            Business_Unit__c='ERPI' OR Business_Unit__c='Schools') AND 
                                            Id =: userId Limit 1];
    */
    /*
        List<User> lstUsers = [SELECT Id, Market__c, Business_Unit__c, UserRole.Name FROM User WHERE Id =: userId Limit 1];
        if((((lstUsers[0].Market__c == 'AU' || lstUsers[0].Market__c=='NZ') && (lstUsers[0].Business_Unit__c=='Higher Ed' || lstUsers[0].Business_Unit__c=='Edify'))) || ( (lstUsers[0].Market__c == 'CA') && (lstUsers[0].Business_Unit__c=='ERPI' || lstUsers[0].Business_Unit__c=='Schools')) || ((lstUsers[0].Market__c == 'DE' || lstUsers[0].Market__c == 'FR' || lstUsers[0].Market__c == 'NL') || (lstUsers[0].UserRole.Name =='Core' || lstUsers[0].UserRole.Name =='Western Europe' || lstUsers[0].UserRole.Name.startsWith('WE - '))))
        {
            marketCheck=true;
        }
      // Commented and Modified by Navaneeth - End  
            System.debug('------------------------'+lstUsers.size());
      // Commented and Modified by Navaneeth - Start    
      //  if(!lstUsers.isEmpty() ){    
          if(marketCheck){
      // Commented and Modified by Navaneeth - End
    */
    // Commented and Modified by Navaneeth - End
      if(System.FeatureManagement.checkPermission('Pearson_Create_Sample_Order')){         
            oppLineItemList = [SELECT Id,Product2.Name, Sample__c,Product2.ISBN__c , Product2.Author__c,Product2.Edition__c, Quantity, ISBN__c, Edition__c, Product2.Copyright_Year__c, Product2.Item_Type_Classification__c,Opportunity.Market__c,Opportunity.Line_of_Business__c,Opportunity.Business_unit__c FROM OpportunityLineItem where OpportunityId = :opportunityId AND Sample__c = true];
            System.debug('------------------------'+oppLineItemList.size());    

            If(!oppLineItemList.isEmpty()){ 

              String redirectUrl = '/apex/PS_OppCreateSampleOrder?Id=' +OpportunityId;            
              return new PageReference(redirectUrl);
            }
            else{           
                errormsg = Label.PS_CreateSampleOrderNoSampleProdsError;
                system.debug('Error'+errormsg);
                displayPopup = true;
                return null;    
            }
        }     
        else{            
            errormsg = Label.PS_CreateSampleOrderNoUserMarketError;
            system.debug('Error'+errormsg);
            displayPopup = true;             
            return null;             
            } 
    }
}