/*
 Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 58) 
*/
@istest
public class OpportunitieCourseAssetsTest{

static testmethod void constructorTest() {



       List<Account> accountList = TestDataFactory.createAccount(1,'Organisation');    
        insert accountList ;
        
        List<Contact> contactList=TestDataFactory.createContact(1);
        insert contactList;

        List<UniversityCourse__c> courseList = TestDataFactory.insertCourse();
        for(UniversityCourse__c courseObj : courseList){
                courseObj.Account__c = accountList[0].id;
        }
        insert courseList;


        List<UniversityCourseContact__c> courseContList = TestDataFactory.insertCourseContact(1,courseList[0].id,contactList[0].id);
        insert courseContList;
        
         List<Opportunity> optyList = TestDataFactory.createOpportunity(1, 'B2B');
         insert optyList;
                
        Test.setCurrentPage(Page.OpportunitieCourseAssets);
        
        
       OpportunityUniversityCourse__c s = new OpportunityUniversityCourse__c();
       s.Close_Date__c =System.today();
       s.Account__c= accountList[0].id;
      s.Opportunity_Name__c =optyList[0].Name;
      s.Opportunity_University_Course_Amount__c=100.00;
       s.UniversityCourse__c = courseList[0].id;
       s.Opportunity__c =  optyList[0].id;
       insert s; 



        //Apttus_Config2__ClassificationName__c category = new Apttus_Config2__ClassificationName__c(Name='testcategoryy',Apttus_Config2__HierarchyLabel__c='testlabele');
        ProductCategory__c category = new ProductCategory__c(Name='Professional & Career Services',HierarchyLabel__c='Professional & Career Services');
        insert category;
 
        //Apttus_Config2__ClassificationHierarchy__c catre= new Apttus_Config2__ClassificationHierarchy__c(Name ='testcategory',Apttus_Config2__HierarchyId__c=category.id,Apttus_Config2__Label__c='testlabel');
        Hierarchy__c catre= new Hierarchy__c(Name ='testcategory',ProductCategory__c=category.id,Label__c='testlabel');
        insert catre;
        Opportunity_Pearson_Course_Code__c opppearson =new Opportunity_Pearson_Course_Code__c(Opportunity__c =optyList[0].id,Pearson_Course_Code_Hierarchy__c=catre.id);
            insert opppearson;
        //Apttus_Config2__ClassificationHierarchy__c csa =new Apttus_Config2__ClassificationHierarchy__c(name ='estt',Apttus_Config2__HierarchyId__c=category.id,Apttus_Config2__Label__c='testlabel');
        Hierarchy__c csa= new Hierarchy__c(Name ='estt',ProductCategory__c=category.id,Label__c='testlabel');
            insert csa; 
        Pearson_Course_Equivalent__c perasoncourese = new Pearson_Course_Equivalent__c(Pearson_Course_Code_Hierarchy__c = csa.id,Course__c=courseList[0].id);
            insert perasoncourese ;


        List<User> usMarketLSt = TestDataFactory.createUser(Userinfo.getProfileId(),1);
            usMarketLSt[0].Product_Business_Unit__c = 'CTIPIHE';
            insert usMarketLSt;
        System.runas(usMarketLSt[0]){
        system.debug('usMarketLSt-------->'+usMarketLSt);           
        List<Product2> prodList = TestDataFactory.createProduct(1);
            prodList[0].Market__c = usMarketLSt[0].Market__c;
            prodList[0].Line_of_Business__c = usMarketLSt[0].Line_of_Business__c;
            prodList[0].Business_Unit__c = usMarketLSt[0].Product_Business_Unit__c;
            insert prodList;
        
        system.debug('prodList-------->'+prodList);
        
        List<Asset> astList = TestDataFactory.insertAsset(1, courseList[0].Id, contactList[0].id, prodList[0].Id, accountList[0].Id);
        system.debug('astList-------->'+astList);
       
            insert astList;
        }


Test.startTest();
         
  PageReference createOppty = new pagereference('apex/OpportunitieCourseAssets');

                        id courseid=  courseList[0].id;
                        id ldid= optyList[0].id;
                        Test.setCurrentPage(createOppty);
                        OpportunitieCourseAssets controller = new OpportunitieCourseAssets(new ApexPages.StandardController(optyList[0]));

        controller.gethasAssets();
        controller.getCourseAssets();
        controller.beginning();
        controller.previous();
        controller.next();
        controller.end();
        controller.getprev();
        controller.getnxt();

   Test.stopTest();
 
}

}