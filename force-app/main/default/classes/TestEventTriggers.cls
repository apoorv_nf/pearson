/************************************************************************************************************
* Apex Interface Name : Create_Amendment_ChainTest
* Version             : 1.0 
* Created Date        : 3/24/2014 
* Modification Log    : 
* Developer                   Date                Description                
* -----------------------------------------------------------------------------------------------------------
* Abhinav                01/27/2015             Modified class to satisfy new case validation
  
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

@isTest
private class TestEventTriggers
{
    static testMethod void myUnitTest()
    {
               
        List<Account> lstAccont = TestDataFactory.createAccount(1 , 'Learner');
        insert lstAccont;
        List<Contact> contLst = TestDataFactory.createContact(1);
        insert contLst;

        Test.startTest();
        TestClassAutomation.FillAllFields = true;
        Id RecordTypeIdVal = [Select id from RecordType where sObjectType = 'Event' AND RecordType.Name='Interview'].id;
       // system.debug('@@@@id'+RecordTypeIdVal);
            Event sEvent                    = (Event)TestClassAutomation.createSObject('Event');
            sEvent.WhatId                   = lstAccont[0].Id;
            sEvent.WhoId                    =contLst[0].Id ;
            
            sEvent.ActivityDateTime         = system.now();
            sEvent.StartDateTime            = sEvent.ActivityDateTime;
            sEvent.EndDateTime              = system.now().addHours(1);
            sEvent.DurationInMinutes        = null;
            sEvent.IsRecurrence             = false;
            sEvent.RecurrenceStartDateTime  = null;
            sEvent.RecurrenceEndDateOnly    = null;
            sEvent.RecurrenceInterval       = null;
            sEvent.RecurrenceDayOfWeekMask  = null;
            sEvent.RecurrenceDayOfMonth     = null;
            sEvent.RecurrenceMonthOfYear    = null;
            sEvent.RecurrenceInstance       = null;
            sEvent.RecurrenceEndDateOnly    = null;
            sEvent.RecurrenceType           = null;
            sEvent.RecurrenceTimeZoneSidKey = null;
            sEvent.Reason_for_Cancellation__c='No Show';
            sEvent.No_Show_Reason__c='Personal';
            sEvent.RecordTypeid             =RecordTypeIdVal;
            sEvent.Status__c = 'Cancelled' ;
       sEvent.Call_Outcome__c = '';
        	sEvent.isdc_dialer_call_type__c = '';
            insert sEvent;
            
            update sEvent;
            
            delete sEvent;
            
            undelete sEvent;
        
        Test.stopTest();
    }
    
}