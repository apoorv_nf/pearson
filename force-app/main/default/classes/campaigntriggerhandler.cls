/************************************************************************************************************
* Apex Class Name  : campaigntriggerhandler.cls
* Version          : 1.0 
* Created Date     : 26 MARCH 2018
* Function         : Handler class for Campaign Object Trigger
* Modification Log :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Darshan MS                  26/03/2018              CR-01459-Req-13: Configure campaigns for sales process
************************************************************************************************************/

Public with sharing class campaigntriggerhandler 
{
    //Constat variables hold record type ID
    // Commented and Modified as part of US-HE:SP - Global Record Type - Renamed. By Navaneeth - Start
    //public static final String STR_GLOBAL_CAMPAIGN = 'Global Campaign';
    public static final String STR_GLOBAL_CAMPAIGN = Label.CampaignGlobalRecordType;
    // Commented and Modified as part of US-HE:SP - Global Record Type - Renamed. By Navaneeth - End
    public static final String STR_SAMPLING_CAMPAIGN = 'Sampling Campaign';
    public static final String STR_ERPI =  'ERPI';
    
    public static final ID GLOBAL_CAMPAIGN_RECORDTYPE_ID = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get(STR_GLOBAL_CAMPAIGN).getRecordTypeId();
    public static final ID SAMPLING_CAMPAIGN_RECORDTYPE_ID = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get(STR_SAMPLING_CAMPAIGN).getRecordTypeId();
    
    // EXECUTE BEFORE INSERT LOGIC
    public static void OnBeforeInsertupdate(list<campaign> campNewRecordsLst)
    {
        user loggedInUserRec = [SELECT Geography__c, Market__c, Business_Unit__c, Line_of_business__c 
                                FROM user 
                                WHERE id =: UserInfo.getUserId()];
        
        //CR-01459-Req-13 : Created by Darshan - To autopopulate fields with values of Loggedin user 
        for(Campaign newRec: campNewRecordsLst)
        {
            if(newRec.RecordTypeID == GLOBAL_CAMPAIGN_RECORDTYPE_ID)
            {
                System.debug('Global Trigger method entered');
                newRec.Geography__c = loggedInUserRec.Geography__C;
                newRec.Line_of_business__c = loggedInUserRec.Line_of_business__c;
                newRec.Market__c = loggedInUserRec.Market__c;
                newRec.Business_unit__c = loggedInUserRec.Business_unit__c;
            }
            
         //CR-01459-Req-20 : Created by Darshan - To autopopulate fields with values of Loggedin user when BU='ERPI'
            else if(newRec.RecordTypeID == SAMPLING_CAMPAIGN_RECORDTYPE_ID)
            {
                If(loggedInUserRec.Business_Unit__c == STR_ERPI)
                {
                    System.debug('Sampling Trigger method entered');
                    newRec.Geography__c = loggedInUserRec.Geography__C;
                    newRec.Business_unit__c = loggedInUserRec.Business_unit__c; 
                    newRec.Market__c = loggedInUserRec.Market__c;
                }
            }
        }
    }
    
}