/*Batch Class to update Opportunity Primary Contact by a trigger as part of CR-0368
/*
*/
global class OpptyPrimaryContact implements Database.Batchable<sObject>
{    
    Opportunity opp;
    List<Opportunity> updateOppty;
    Set<id> setofOpptyIds = new set<id>();
    List<PS_OpptyPrimaryContactBatch__c> lastbatch = PS_OpptyPrimaryContactBatch__c.getall().values();
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String lastBatchRundate= lastbatch[0].LastBatchRunTime__c.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'','GMT');
        //return Database.getQueryLocator('Select ContactID, Contact.Name,lastmodifieddate, OpportunityID,IsPrimary from OpportunityContactRole where isPrimary=true AND lastmodifieddate>='+lastBatchRundate);
        return Database.getQueryLocator('Select ContactID, Contact.Name,lastmodifieddate, OpportunityID,IsPrimary from OpportunityContactRole where lastmodifieddate>='+lastBatchRundate);
    }

    global void execute(Database.BatchableContext BC, List<OpportunityContactRole> scope)
    {
        //updateOppty= new List<Opportunity>();
        for (OpportunityContactRole oppRole : scope)
        {
           //opp = new Opportunity();
           //opp.Id=oppRole.OpportunityID;
           //updateOppty.add(opp);
           setofOpptyIds.add(oppRole.OpportunityID);
           
        }
        //update updateOppty;
        updateOppty = [select id from Opportunity where id in : setofOpptyIds];
        update updateOppty;
        
    }  
    global void finish(Database.BatchableContext BC)
    {
        PS_OpptyPrimaryContactBatch__c updateRec = new PS_OpptyPrimaryContactBatch__c ();
        updateRec.LastBatchRunTime__c=System.Now();
        updateRec.Id=lastBatch[0].Id;
        update updateRec;
    }
}