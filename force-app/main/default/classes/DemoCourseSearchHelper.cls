/*
    *Name: Steven M. Giangrasso & Matt Hime (tquila)
    *Description: This class is created to cover the shared functionality of the CourseSearchGlobalController and CourseSearchTreeviewController
    *-Refactoring both classes to share common methods
    *Date: 8/21/2012
    * Modified: 12/04/2017 Cristina to support load of non-Apttu tables
    * Modified: 02/02/2018 Cristina to support INC3875733 where primary was not getting set on first OpptyPearsonCourseCode selected
    * Modified: 13/10/2019 Anurutheran CR-02940[Replacing Apttus with Non-Apttus]

*/
//with sharing used to enforce the sharing rules that apply to the current user: http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_classes_keywords_sharing.htm
public with sharing class DemoCourseSearchHelper {
    /*
     *Each visualforce page that uses this class is passed a type value in the URL.
     *0 indicates that the caller was an Opportunity record
     *1 indicates that the caller was an University Course record
     *It's extremely unlikely that an incorrect value will ever get passed through as it's hard coded,  but,
     *maybe some day another will be added - in which case we need to make sure there's no unexpected behaviour
     */
    private enum ObjectType {OPPORTUNITY, UNIVERSITY_COURSE, UNKNOWN}
    
    //The incoming OwnerRecordType expressed as ObjectType
    private final ObjectType Type;
    
    //This data is held a map so that we can display the values and search the keySet
    private map<Id, Hierarchy__c> ExistingCourseData;

    //The Sf id of the record that called the search page
    public string OwnerRecordId {get; private set;}
    
    //The type of record that called the search page
    public integer OwnerRecordType {get; private set;}
    public list<Hierarchy__c> ExistingCourses{
        get {
            return ExistingCourseData.values();
        }
        
        private set;
    }
    
    // Start of Changes by Kote & Priya - CR #
    Public Id HelperPCEId {get;set;}
    Public String HelperMode {get;set;}
    public list<Opportunity_Pearson_Course_Code__c> HelperPCESCList {get;set;}
    
    /*for course object*/
    public list<Pearson_Course_Equivalent__c > HelperPCECourseSCList{get;set;}

    // End of Changes by Kote & Priya - CR #
    
    /*
     *The constructor casts the incoming OwnerRecordType to a value of ObjectType
     *it also grabs the data for any PearsonCourseStructures already associated with
     *the OwnerRecordId bu searching the junction objects 
     *Opportunity_Pearson_Course_Code__c and UniversityCoursePearsonCourseStructure__c
     */
    public DemoCourseSearchHelper(Id Owner, integer OwnerType,string Mode,Id PCEId ){

        //Assign the OwnerRecordID variable to the Owner parameter in the CourseSearchHelper Constructor
        OwnerRecordId = Owner;
        //Assign the OwnerRecordType variable to the OwnerType parameter in the CourseSearchHelper Constructor
        OwnerRecordType = OwnerType;
        
        // Start of Changes by Kote & Priya - CR #
        HelperPCEId = PCEID;
        HelperMode = Mode;
        // End of Changes by Kote & Priya - CR #
        
        try{
            Type =  ObjectType.values().get(OwnerRecordType);
        }
        catch(Exception e){
            Type = ObjectType.UNKNOWN;
        }
        
        getExistingCourses();
    }
    
    /*
     *This method searches the junction objects to come up with a map of existing records
     *The map values are displayed on screen whilst the keySet is used later for depduping
     */
    private void getExistingCourses(){
        ExistingCourseData = new map<Id, Hierarchy__c>();
        
        //If the Type is equal to an Opportunity, insert a OpportunityPearsonCourseStructure with a put method into the ExistingCourseData
        //Added Pearson_Course_Code_Name__r.Pearson_Course_Structure_Code__c to Query below to display in DemoCourseSearchGlobal VF Page.by Kyama
        //CP - 12/05/2017 Added Pearson_Course_Code_Hierarchy__c to the queries below to load the non apttus objects and modify the method calls
        if(Type == ObjectType.OPPORTUNITY){
            //Apttus to Non-Apttus(Start)
            for(Opportunity_Pearson_Course_Code__c item: [select Pearson_Course_Code_Hierarchy__c, Pearson_Course_Code_Hierarchy__r.Name, Pearson_Course_Code_Hierarchy__r.Type__c,Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c
                                                                from Opportunity_Pearson_Course_Code__c 
                                                                where Opportunity__c = :OwnerRecordId]){
                
                ExistingCourseData.put(item.Pearson_Course_Code_Hierarchy__c, createPearsonCourseStructure(item.Pearson_Course_Code_Hierarchy__c, item.Pearson_Course_Code_Hierarchy__r.Name, item.Pearson_Course_Code_Hierarchy__r.Type__c,item.Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c));//CR-02940[Replacing Apttus with Non-Apttus]
            }
            //Apttus to Non-Apttus(End)
        }
        //If the Type is equal to an University Course, insert a OpportunityPearsonCourseStructure with a put method into the ExistingCourseData
        //Added Pearson_Course_Code_Hierarchy__r.Pearson_Course_Structure_Code__c  to Query below to display in DemoCourseSearchGlobal VF Page.by Kyama
        //CP - 12/05/2017 Added Pearson_Course_Code_Hierarchy__c to the queries below to load the non apttus objects and modify the method calls
        else if(Type == ObjectType.UNIVERSITY_COURSE){
           for(Pearson_Course_Equivalent__c item: [select Pearson_Course_Code_Hierarchy__c, Pearson_Course_Code_Hierarchy__r.Name, Pearson_Course_Code_Hierarchy__r.Type__c,Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c 
                                                                    from Pearson_Course_Equivalent__c 
                                                                    where Course__c = :OwnerRecordId]){
                
                ExistingCourseData.put(item.Pearson_Course_Code_Hierarchy__c, createPearsonCourseStructure(item.Pearson_Course_Code_Hierarchy__c, item.Pearson_Course_Code_Hierarchy__r.Name, item.Pearson_Course_Code_Hierarchy__r.Type__c,item.Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c));
            }
        }
    }
    
    /*
     *Creates Hierarchy__c object from the parameters passed to it
     *Used by getExistingCourses to convert junction objects into Hierarchy__c objects
     *for display purposes 
     *Added pcs.PearsonCourseStructureCode__c = PearsonCourseCode by kyama
     */
    private Hierarchy__c createPearsonCourseStructure(Id pcsId, string Name, string Type, string PearsonCourseCode){
        Hierarchy__c pcs = new Hierarchy__c(Id = pcsId);
        pcs.Name = Name;
        pcs.Type__c = Type;
        pcs.PearsonCourseStructureCode__c=PearsonCourseCode;
        return pcs;
    }
    
    /*
     *Takes an array of Hierarchy__c objects and creates junction objects to attach them to
     *the Opportunity or University Course record that called the search page
     */
    public PageReference Attach(Hierarchy__c[] selectedPCS){
        
        list<Sobject> newItems = new list<Sobject>();
        
        Hierarchy__c[] deDupedCourses = removeDuplicates(selectedPCS);
        
        if(Type == ObjectType.OPPORTUNITY){
            //Cp 02/02/2018 added to support INC3875733
            integer primCount = 0;
            List<Opportunity_Pearson_Course_Code__c> opceListPrimary = [select id, name from Opportunity_Pearson_Course_Code__c where Opportunity__c =: OwnerRecordId and Primary__c = true];
            
            for(Hierarchy__c pcs: deDupedCourses){
                Opportunity_Pearson_Course_Code__c opcs = new Opportunity_Pearson_Course_Code__c();
                opcs.Opportunity__c = OwnerRecordId;
                //Cp 02/02/2018 added to support INC3875733
                if(opceListPrimary.isEmpty() && primCount == 0){
                        opcs.Primary__c = true;
                        primCount = 1;
                }
                //opcs.Pearson_Course_Code_Name__c = pcs.Id;
                opcs.Pearson_Course_Code_Hierarchy__c = pcs.Id;
                opcs.Active__c = true;
                newItems.add(opcs);
            }
        }
        else if(Type == ObjectType.UNIVERSITY_COURSE){
            integer count = 0;
            Boolean check = true;
            // Pooja |Querying record with Primary checkbox checked from Pearson Course Equivalent object associated to Course id 'OwnerRecordId'.
            List<Pearson_Course_Equivalent__c> pceList = [select id, name from Pearson_Course_Equivalent__c where Course__c =: OwnerRecordId and Primary__c =: check];
            for(Hierarchy__c pcs: deDupedCourses){
                Pearson_Course_Equivalent__c ucpcs = new Pearson_Course_Equivalent__c();
               // Pooja |setting Primary checkbox to true when there is no Primary checked record already exist on Pearson_Course_Equivalent__c related list on Course.
               // Pooja |Only one record should be Primary checked so imcrementing Count with one
               if(pceList.isEmpty() && count == 0){
                        ucpcs.Primary__c = true;
                        count = 1;
                }
                ucpcs.Course__c = OwnerRecordId;
                //Cp 12/6/2017 added to support non Apttus objects
                //ucpcs.Pearson_Course_Code_Hierarchy__c = pcs.Id;
                ucpcs.Pearson_Course_Code_Hierarchy__c = pcs.Id;
                ucpcs.Active__c = true;
                newItems.add(ucpcs);
            }
        }
        
        //The collection will be empty if an invalid type was passed to the helper
        if(!newItems.isEmpty()){
            try{
                // Start of Changes By Priya & Kote - CR #
                //insert newItems;
                If(HelperMode=='Add')
                {
                    insert newItems;
                }
                else if(Type == ObjectType.OPPORTUNITY && HelperMode=='Edit' && newItems.size()==1)
                {
                    HelperPCESCList =[select id,name, Pearson_Course_Code_Hierarchy__c from Opportunity_Pearson_Course_Code__c where id=:HelperPCEId];
                    List<Opportunity_Pearson_Course_Code__c> PCESCListSelect = (List<Opportunity_Pearson_Course_Code__c>)newItems;
                    //HelperPCESCList[0].Pearson_Course_Code_Name__c = PCESCListSelect[0].Pearson_Course_Code_Name__c;//CR-02940
                    //Cp 12/6/2017 added to support non Apttus objects and INC3875733
                    HelperPCESCList[0].Active__c =true;
                    HelperPCESCList[0].Pearson_Course_Code_Hierarchy__c = PCESCListSelect[0].Pearson_Course_Code_Hierarchy__c;
                    update HelperPCESCList;
                }
                  else if(Type == ObjectType.UNIVERSITY_COURSE && HelperMode=='Edit' && newItems.size()==1)
                {
                    HelperPCECourseSCList  =[select id,name,Pearson_Course_Code_Hierarchy__c from Pearson_Course_Equivalent__c  where id=:HelperPCEId];
                    List<Pearson_Course_Equivalent__c > PCESCCourseListSelect = (List<Pearson_Course_Equivalent__c >)newItems;
                    HelperPCECourseSCList [0].Pearson_Course_Code_Hierarchy__c = PCESCCourseListSelect[0].Pearson_Course_Code_Hierarchy__c;
                    //Cp 12/6/2017 added to support non Apttus objects and INC3875733
                    HelperPCECourseSCList [0].Pearson_Course_Code_Hierarchy__c = PCESCCourseListSelect[0].Pearson_Course_Code_Hierarchy__c;
                
                }
                // End of Changes By Priya & Kote - CR # 
            }
            catch(Exception ex){
                return null;
            }
        }
        
        return ReturnToCaller();
    }
    
    /*
     *Users can easily select duplicate Hierarchy__c objects,  so,  before creating junction
     *objects,  iterate through the array of chosen items and interrogate the ExistingCoursesData keySet
     *to see if the item has already been used before
     */
    private Hierarchy__c[] removeDuplicates(Hierarchy__c[] selectedPCS){
        list<Hierarchy__c> deDupedCourses = new list<Hierarchy__c>();
        
        for(Hierarchy__c pcs: selectedPCS){
            //If the item is NOT in the keySet,  it's okay to use it
            if(!ExistingCourseData.keySet().contains(pcs.Id)){
                deDupedCourses.add(pcs);
            }
        }
        
        return deDupedCourses;
    }
    
    /*
     *Called by each search page whenever they are returning to the opportunity
     *or university course record that called them (i.e. Cancel or Attach methods)
     */
    public PageReference ReturnToCaller(){
        return new PageReference('/' + OwnerRecordId);
    }
}