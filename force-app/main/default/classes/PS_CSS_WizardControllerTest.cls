/************************************************************************************************************
* Apex Interface Name : PS_CSS_WizardControllerTest
* Version             : 1.1 
* Created Date        : 13-JUNE-2017 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
*Manikanta Nagubilli        13-JUNE-2017 
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_CSS_WizardControllerTest
{
    static testMethod void test_getwizardData(){
        UserRole tempUserRole;
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
         User usr = new User(LastName = 'happy', firstName='Iam', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
         
         System.runAs(usr){
            Test.StartTest();
                PS_CSS_WizardController.getValueWizardData();
            Test.StopTest();
            }
    }
}