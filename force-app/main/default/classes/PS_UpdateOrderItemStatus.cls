/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_UpdateOrderItemStatus.Cls
   Description:     Handler class for updating Order Items Status from Order trigger
   Test Class:      PS_UpdateOrderItemStatusTest
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   03/12/2015         0.1        Accenture - Karan Khanna                Created
                                
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class PS_UpdateOrderItemStatus {
    
    public Map<Id,Order> newOrdersMap = new Map<Id,Order>();
    public Map<Id,Order> oldOrdersMap = new Map<Id,Order>();
    public Id sampleOrderRTID;
    public Id revenueOrderRTID;
    
    // Constructor
    public PS_UpdateOrderItemStatus(Map<Id,Order> newOrdersMap, Map<Id,Order> oldOrdersMap){
        
        this.newOrdersMap = newOrdersMap;
        this.oldOrdersMap = oldOrdersMap;
        sampleOrderRTID = PS_Util.fetchRecordTypeByName(Order.SobjectType,Label.PS_OrderSampleOrderType);
        revenueOrderRTID = PS_Util.fetchRecordTypeByName(Order.SobjectType,Label.PS_OrderRevenueRecordType);
    }
    
    /*---------------------------------------------------------------------------------------------------------------------------------
    Author:         Karan Khanna
    Company:        Accenture
    Description:    This method is used when Order Status is changed to Filled
    Inputs:         None
    Returns:        None
    
    <Date>          <Authors Name>      <Brief Description of Change> 
    03/12/2015      Accenture_Karan     Initial version of the code
     
    -------------------------------------------------------------------------------------------------------------------------------*/
    public void StatusChangedToFilled() {
        
        Set<Id> filledOrderIdSet = new Set<Id>();
        Set<Id> filledIntegrationOrderIdSet = new Set<Id>();
        try{
            for(Order ord : newOrdersMap.Values()) {
                if(ord.Status == 'Filled' && ord.Status != oldOrdersMap.get(ord.Id).Status) {
                    // not applicable for US market
                    if(ord.Market__c != Label.PS_USMarket) {
                        filledOrderIdSet.add(ord.Id);
                    }
                }
            }
            if(!filledOrderIdSet.isEmpty()) {               
                for(Order ord : newOrdersMap.Values()) {
                    if(filledOrderIdSet.Contains(ord.Id)) {
                        if(ord.Market__c == Label.PS_AUMarket || ord.Market__c == Label.PS_NZMarket) {
                            // Integration_Enabled_Object_Configuration__c custom setting will only contain data for records eligible for Integration, hence referring to Custom setting instead of harcoding the criteria
                            for(Integration_Enabled_Object_Configuration__c integrationRec : Integration_Enabled_Object_Configuration__c.getall().Values()) {
                                if(ord.RecordTypeId == revenueOrderRTID) {                                 
                                    if(integrationRec.Object_Name__c == 'Order' && integrationRec.Object_Record_Type__c == 'Revenue' && ord.Market__c == integrationRec.Market__c && ord.Line_of_Business__c == integrationRec.Line_of_Business__c && ord.Business_Unit__c == integrationRec.Business_Unit__c) {
                                        filledIntegrationOrderIdSet.add(ord.Id);
                                    }
                                }                               
                                if(ord.RecordTypeId == sampleOrderRTID) {                                  
                                   if(integrationRec.Object_Name__c == 'Order' && integrationRec.Object_Record_Type__c == 'Sample' && ord.Market__c == integrationRec.Market__c && ord.Line_of_Business__c == integrationRec.Line_of_Business__c && ord.Business_Unit__c == integrationRec.Business_Unit__c) {
                                        filledIntegrationOrderIdSet.add(ord.Id);
                                    }                                                                  
                                }
                            }
                        }
                    }
                }
            }
            if(!filledIntegrationOrderIdSet.isEmpty()) {
                filledOrderIdSet.removeAll(filledIntegrationOrderIdSet);
            }
            if(!filledOrderIdSet.isEmpty()) {
                List<OrderItem> oiList = new List<OrderItem>();
                for(OrderItem oi : [SELECT Id, Status__c, OrderId FROM OrderItem WHERE OrderId IN: filledOrderIdSet AND (Status__c != 'Backordered' AND Status__c != 'Cancelled' AND Status__c != 'Processed')]) {
                    oi.Status__c = 'Shipped';
                    oiList.add(oi);
                }
                if(!oiList.isEmpty()) {
                    update oiList;
                }
            }
        }
        catch (exception e) {
        }
    }
}