/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            AccountAddressesUpdateonAccount.cls
   Description:     On Insert/Update of Account Addresses record as well as Update Account on Address Look Up fields.
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------       -----------------------------------------------------------------------------------------------
    12-Oct-2017        1.0          Navaneeth                   Created the Class for Updating Billing & Shipping Address Look up in Account Level
    25-Nov-2018        2.0          Darshan                     Replace Address_type__c with Billing__c,Shipping__c as part of CR-02281-Req-3
    30-Nov-2018       3.0          Vinoth                      Add Physical Address(Primary_Physical__c,Physical__c ) as part of CR-02397
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class AccountAddressesUpdateonAccount{

    public static void afterInsertOrUpdateAccountAddresses( Set<Id> UniqueAccountIdSet)
    {
        List<Account> AccountUpdateList = new List<Account>();
        List<Account> AccountUnique = new List<Account>();
        Map<Id,List<Account_Addresses__c>> AccountAddressMap = new Map<Id,List<Account_Addresses__c>>();
        try{
            Map<String,String> mapState = StateandCountryMap.StateCodeMap();
            Map<String,String> mapCountry = StateandCountryMap.CountryCodeMap();
            List<Account_Addresses__c> AccountAddressesQueryList = [Select Id, Account__c, Address_Type__c,Billing__c,Shipping__c,Physical__c,
                                                                    Account__r.Billing_Address_Lookup__c, Account__r.Physical_Address_Lookup__c, Account__r.Shipping_Address_Lookup__c, 
                                                                    Primary__c, PrimaryShipping__c,Primary_Physical__c, ERP_DEF_BILL_Site_Use_ID__c, ERP_Billing_Site_Use_ID__c, 
                                                                    LastModifiedDate, Address_1__c, Address_2__c, Address_3__c, Address_4__c, City__c, 
                                                                    Country__c, State__c, Zip_Code__c FROM Account_Addresses__c Where Account__c IN :UniqueAccountIdSet ];
            AccountUnique = [Select Id, Primary_Selling_Account__c, Primary_Selling_Account__r.Billing_Address_Lookup__c, Billing_Address_Lookup__c, Shipping_Address_Lookup__c,Physical_Address_Lookup__c,ShippingCountry, ShippingCountryCode, ShippingStreet, ShippingCity, ShippingState, ShippingStateCode, ShippingPostalCode, BillingCountry, BillingCountryCode, BillingStreet, BillingCity, BillingState, BillingStateCode, BillingPostalCode FROM Account Where Id IN :UniqueAccountIdSet];
            for(Id AccId : UniqueAccountIdSet)
            {
                List<Account_Addresses__c> TempAccListAddress = new List<Account_Addresses__c>();
                for(Account_Addresses__c AccAdd : AccountAddressesQueryList)
                {
                    Account_Addresses__c TempAccAddress = new Account_Addresses__c();
                    if(AccId == AccAdd.Account__c )
                    {
                        TempAccAddress = AccAdd;
                        TempAccListAddress.add(TempAccAddress);
                    }
                }
                AccountAddressMap.put(AccId,TempAccListAddress);
            }
            for(Account Acc : AccountUnique)
            {
                Integer pbillcount = 0;
                Integer pshipcount = 0;
                Integer pphycount = 0;    //CR-02397
                Boolean AddAccList = false;
                Id pbillAccAddresses;
                Id LatBillTypeAccAddresses;
                Id pphyAccAddresses;    //CR-02397
                Id billingAccAddress;
                Id pshipAccAddresses;
                Id pphyingAccAddresses;    //CR-02397
                String BillStreet=null;
                String BillCountryCode=null;
                String BillStateCode=null;
                String BillCity=null;
                String BillPostalCode=null;
                String ShipStreet=null;
                String ShipCountryCode=null;
                String ShipStateCode=null;
                String ShipCity=null;
                String ShipPostalCode=null;
                String PhyStreet=null;    //CR-02397
                String PhyCountryCode=null;    //CR-02397
                String PhyStateCode=null;    //CR-02397
                String PhyCity=null;    //CR-02397
                String PhyPostalCode=null;    //CR-02397
                String PhyAddress=null;    //CR-02397
                
                List<Account_Addresses__c> CheckAccountAddressesList = AccountAddressMap.get(Acc.Id);
                List<Account_Addresses__c> BillingTypeAccAddList = new List<Account_Addresses__c>();
                List<Account_Addresses__c> ShippingTypeAccAddList = new List<Account_Addresses__c>();
                List<Account_Addresses__c> PhysicalTypeAccAddList = new List<Account_Addresses__c>();    //CR-02397
                
                for (Account_Addresses__c AccAdd:CheckAccountAddressesList)
                {
                    if(AccAdd.Primary__c == true)
                    {
                        pbillcount = pbillcount +1;
                        pbillAccAddresses = AccAdd.Id;
                        /*if(AccAdd.Address_1__c!=null)
                            BillStreet=AccAdd.Address_1__c;
                        if(AccAdd.Address_2__c!=null)
                            BillStreet=BillStreet+AccAdd.Address_2__c;
                        if(AccAdd.Address_3__c!=null)
                            BillStreet=BillStreet+AccAdd.Address_3__c;
                        if(AccAdd.Address_4__c!=null)
                            BillStreet=BillStreet+AccAdd.Address_4__c;
                        BillStreet = BillStreet.trim();
                        BillCountryCode = mapCountry.get(AccAdd.Country__c);
                        BillStateCode = mapState.get(AccAdd.State__c);
                        BillCity = AccAdd.City__c;
                        BillPostalCode = AccAdd.Zip_Code__c;*/
                    }   
                    if(AccAdd.PrimaryShipping__c == true)
                    {
                        pshipcount= pshipcount+1;
                        pshipAccAddresses = AccAdd.Id;
                        /*if(AccAdd.Address_1__c!=null)
                            ShipStreet=AccAdd.Address_1__c;
                        if(AccAdd.Address_2__c!=null)
                            ShipStreet=ShipStreet+AccAdd.Address_2__c;
                        if(AccAdd.Address_3__c!=null)
                            ShipStreet=ShipStreet+AccAdd.Address_3__c;
                        if(AccAdd.Address_4__c!=null)
                            ShipStreet=ShipStreet+AccAdd.Address_4__c;
                        ShipStreet = ShipStreet.trim();
                        ShipCountryCode = mapCountry.get(AccAdd.Country__c);
                        ShipStateCode = mapState.get(AccAdd.State__c);
                        ShipCity = AccAdd.City__c;
                        ShipPostalCode = AccAdd.Zip_Code__c;*/
                    }
                    //CR-02397 - Start
                    if(AccAdd.Primary_Physical__c== true)
                    {
                        pphycount = pphycount+1;
                        pphyAccAddresses= AccAdd.Id;
                        // PhyAddress = AccAdd.Address_1__c + ' ' + AccAdd.Address_2__c + ' ' + AccAdd.Address_3__c + ' ' + AccAdd.Address_4__c + ' ' + mapCountry.get(AccAdd.Country__c) + ' ' + mapState.get(AccAdd.State__c) + ' ' + AccAdd.City__c + ' ' + AccAdd.Zip_Code__c;
                        
                    }
                    //CR-02397 - End
                    
                    if(AccAdd.Billing__c ==True)
                    {
                        BillingTypeAccAddList.add(AccAdd);
                    }
                    if(AccAdd.Shipping__c==True && AccAdd.ERP_DEF_BILL_Site_Use_ID__c!=null)
                    {
                        ShippingTypeAccAddList.add(AccAdd);
                    }  
                    
                    //CR-02397
                    if(AccAdd.Physical__c==True)
                    {
                        PhysicalTypeAccAddList.add(AccAdd);
                    }                 
                }
               
                if(pbillcount==1&&pshipcount==1)
                {
                    if((Acc.Billing_Address_Lookup__c!=pbillAccAddresses)&&(Acc.Shipping_Address_Lookup__c!=pshipAccAddresses))
                    {
                        Acc.Billing_Address_Lookup__c=pbillAccAddresses;
                        Acc.Shipping_Address_Lookup__c=pshipAccAddresses;                        
                        /*Acc.BillingCountryCode = BillCountryCode;
                        Acc.BillingStreet = BillStreet;
                        Acc.BillingCity = BillCity;
                        Acc.BillingStateCode = BillStateCode;
                        Acc.BillingPostalCode = BillPostalCode;
                        
                        Acc.ShippingCountryCode = ShipCountryCode;
                        Acc.ShippingStreet = ShipStreet;
                        Acc.ShippingCity = ShipCity;
                        Acc.ShippingStateCode = ShipStateCode;
                        Acc.ShippingPostalCode = ShipPostalCode;
                        if(BillCountryCode!=null&&BillStateCode!=null&&BillCountryCode!=null&&BillStateCode!=null)*/
                        //AccountUpdateList.add(Acc);
                    }
                    else if((Acc.Billing_Address_Lookup__c!=pbillAccAddresses)&&(Acc.Shipping_Address_Lookup__c==pshipAccAddresses))
                    {
                        Acc.Billing_Address_Lookup__c=pbillAccAddresses;
                        /*Acc.BillingCountryCode = BillCountryCode;
                        Acc.BillingStreet = BillStreet;
                        Acc.BillingCity = BillCity;
                        Acc.BillingStateCode = BillStateCode;
                        Acc.BillingPostalCode = BillPostalCode;
                        if(BillCountryCode!=null&&BillStateCode!=null)*/
                        //AccountUpdateList.add(Acc);
                    }
                    else if((Acc.Billing_Address_Lookup__c==pbillAccAddresses)&&(Acc.Shipping_Address_Lookup__c!=pshipAccAddresses))
                    {
                        Acc.Shipping_Address_Lookup__c=pshipAccAddresses;
                        /*Acc.ShippingCountryCode = ShipCountryCode;
                        Acc.ShippingStreet = ShipStreet;
                        Acc.ShippingCity = ShipCity;
                        Acc.ShippingStateCode = ShipStateCode;
                        Acc.ShippingPostalCode = ShipPostalCode;
                        if(ShipCountryCode!=null&&ShipStateCode!=null)*/
                        //AccountUpdateList.add(Acc);
                    }       
                }                   
                else if((pbillcount==0 || pshipcount==1) || Test.isRunningTest())
                {
                    if(Acc.Shipping_Address_Lookup__c!=pshipAccAddresses && pshipcount==1)
                    { 
                        Acc.Shipping_Address_Lookup__c=pshipAccAddresses;
                       /*Acc.ShippingCountryCode = ShipCountryCode;
                        Acc.ShippingStreet = ShipStreet;
                        Acc.ShippingCity = ShipCity;
                        Acc.ShippingStateCode = ShipStateCode;
                        Acc.ShippingPostalCode = ShipPostalCode;
                        if(ShipCountryCode!=null&&ShipStateCode!=null)*/
                        AddAccList=true;
                    }
                      /*
                      *Logic to be added for Billing Address Populate when the Shipping Address matches.
                      */  
                    for (Account_Addresses__c AccAddShip :ShippingTypeAccAddList )
                    {   //integer ittt = 1;
                        for (Account_Addresses__c AccAddBill : BillingTypeAccAddList)
                        {
                            //integer ittt2 = 1;
                            if(AccAddShip.ERP_DEF_BILL_Site_Use_ID__c==AccAddBill.ERP_Billing_Site_Use_ID__c)
                            {
                                billingAccAddress = AccAddBill.Id;
                              /*  if(AccAddBill.Address_1__c!=null)
                                    BillStreet=AccAddBill.Address_1__c;
                                if(AccAddBill.Address_2__c!=null)
                                    BillStreet=BillStreet+AccAddBill.Address_2__c;
                                if(AccAddBill.Address_3__c!=null)
                                    BillStreet=BillStreet+AccAddBill.Address_3__c;
                                if(AccAddBill.Address_4__c!=null)
                                    BillStreet=BillStreet+AccAddBill.Address_4__c;
                                BillStreet = BillStreet.trim();
                                BillCountryCode = mapCountry.get(AccAddBill.Country__c);
                                BillStateCode = mapState.get(AccAddBill.State__c);
                                BillCity = AccAddBill.City__c;
                                BillPostalCode = AccAddBill.Zip_Code__c;*/
                            }
                        }
                    }
                    /*
                        Logic to be added for Billing Address Based on Address Type as Billing - Latest Record
                    */
                    if(billingAccAddress==null)
                    {
                        for(Integer i=0;i<BillingTypeAccAddList.size();i++)                 
                        {
                            for(Integer j=0;j<=i;j++)
                            {
                                if(BillingTypeAccAddList[j].LastModifiedDate >= BillingTypeAccAddList[i].LastModifiedDate)
                                {
                                    billingAccAddress = BillingTypeAccAddList[j].Id;
                                    /*if(AccAddBill.Address_1__c!=null)
                                        BillStreet=AccAddBill.Address_1__c;
                                    if(AccAddBill.Address_2__c!=null)
                                        BillStreet=BillStreet+AccAddBill.Address_2__c;
                                    if(AccAddBill.Address_3__c!=null)
                                        BillStreet=BillStreet+AccAddBill.Address_3__c;
                                    if(AccAddBill.Address_4__c!=null)
                                        BillStreet=BillStreet+AccAddBill.Address_4__c;
                                    BillStreet = BillStreet.trim();
                                    BillCountryCode = mapCountry.get(AccAddBill.Country__c);
                                    BillStateCode = mapState.get(AccAddBill.State__c);
                                    BillCity = AccAddBill.City__c;
                                    BillPostalCode = AccAddBill.Zip_Code__c;*/
                                }
                            }
                        }
                    }
                    /*
                        *Child Account - Primary Selling Account - Billing Addresses Has to populated on Child of we would not able to find the billing      address based on the logic provided by business.
                        *Primary Selling Account would not present with the 
                    */
                    if(BillingTypeAccAddList.size()==0)
                    {
                        if(Acc.Primary_Selling_Account__c!=null)
                        {
                            billingAccAddress=Acc.Primary_Selling_Account__r.Billing_Address_Lookup__c;
                        }   
                        // No Else part : Since Primary Selling Account - must have a Billing Address.
                    }
                    if(Acc.Billing_Address_Lookup__c!=billingAccAddress && pbillcount==0 && pshipcount==1)
                    {
                        Acc.Billing_Address_Lookup__c=billingAccAddress;
                       /* Acc.BillingCountryCode = BillCountryCode;
                        Acc.BillingStreet = BillStreet;
                        Acc.BillingCity = BillCity;
                        Acc.BillingStateCode = BillStateCode;
                        Acc.BillingPostalCode = BillPostalCode;
                        if(BillCountryCode!=null&&BillStateCode!=null)*/
                        AddAccList=true;
                    }
                    /*
                    if(AddAccList)
                    {
                        AccountUpdateList.add(Acc);
                    }
                    */
                }
                else if(pshipcount==0 || pbillcount==1)
                {
                    if(Acc.Billing_Address_Lookup__c!=pbillAccAddresses && pbillcount==1 )
                    {
                        Acc.Billing_Address_Lookup__c=pbillAccAddresses;
                       /* Acc.BillingCountryCode = BillCountryCode;
                        Acc.BillingStreet = BillStreet;
                        Acc.BillingCity = BillCity;
                        Acc.BillingStateCode = BillStateCode;
                        Acc.BillingPostalCode = BillPostalCode;
                        if(BillCountryCode!=null&&BillStateCode!=null)*/
                        AddAccList=true;
                    }
                    /*
                    if(AddAccList)
                    {
                        AccountUpdateList.add(Acc);
                    }
                    */
                }  
                
                //CR-02397 - Start                
                if(pphycount==1) {
                    //if(Acc.Physical_Address_Lookup__c!=pphyingAccAddresses){
                        Acc.Physical_Address_Lookup__c =pphyAccAddresses;                       
                     //} 
                     AddAccList=true;                      
                                                                
                } 
                else{
                    Acc.Physical_Address_Lookup__c = pshipAccAddresses;
                    AddAccList=true;
                }
                
                if(AddAccList){
                        AccountUpdateList.add(Acc);
                }
                
                //CR-02397 - End
                
            }
       if(AccountUpdateList.size() > 0)
            {
        
                //Update AccountUpdateList;
              //  database.update(AccountUpdateList,false);
              List<Database.SaveResult> results = Database.update(AccountUpdateList, false);
                for (Database.SaveResult result : results) {
                if (!result.isSuccess()){
                for (Database.Error err : result.getErrors()){
                    }
                  }
                }
              
            }
        }
        catch(Exception e)
        {
            throw e;
        }
    }
}