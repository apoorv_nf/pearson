/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_UpdatePIUWithCoursesControllerTest
   Description:     Test class for PS_UpdatePIUWithCoursesController
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   18 Nov 2015      1.0           Accenture NDC                                         Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- 
   *  Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 39,46) 
*/
 
@isTest
public class PS_UpdatePIUWithCoursesControllerTest
{
    static testmethod void testWithCoursesForIndirectChannel()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Contact> lstWithContact = new List<Contact>();
        List<Product2> lstWithProduct = new List<Product2>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        List<Course_Product_in_Use__c> lstWithCourseProductInUse = new List<Course_Product_in_Use__c>();
        Course_Product_in_Use__c newCourseProductInUse = new Course_Product_in_Use__c();
        List<Contact_Product_In_Use__c> lstWithContactProductInUse = new List<Contact_Product_In_Use__c>();
        Contact_Product_In_Use__c newContactProductInUse = new Contact_Product_In_Use__c();
        List<Asset> lstWithAsset = new List<Asset>();
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');  
        lstWithProduct = TestDataFactory.createProduct(2);
        lstWithContact = TestDataFactory.createContact(1);
        List<OpportunityUniversityCourse__c> lstWithOpportunityUniversityCourse = new List<OpportunityUniversityCourse__c>();
        List<UniversityCourse__c> univCourse = new List<UniversityCourse__c>();
        univCourse = TestDataFactory.insertCourse();
        lstWithOpportunityUniversityCourse = TestDataFactory.createOpportunityUniversityCourse();
        if(lstWithProduct.size() > 0)
        {
            for(Product2 newProduct : lstWithProduct)
            {
                newProduct.Business_Unit__c = 'CTIPIHE';
            }
        }
        if(lstWithTestUser.size() > 0)
        {
            for(User newUser : lstWithTestUser)
            {
                newUser.Product_Business_Unit__c = 'CTIPIHE';
            }
        } 
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Indirect';
            lstWithOppty[0].Selling_Period__c = 'Fall';
            lstWithOppty[0].Selling_Period_Year__c = '2020';
            lstWithOppty[0].StageName = 'Awarded';
            if(lstWithTestUser.size() > 0)
            {
                lstWithTestUser[0].Market__c = 'US';
            }
            test.startTest();
            insert lstWithProduct;
            insert lstWithAccount;
            insert lstWithContact;
            if(lstWithOppty.size() > 0)
            {
                lstWithOppty[0].AccountId = lstWithAccount[0].Id;
                insert lstWithOppty;
            }
            lstWithOpportunityUniversityCourse[0].Opportunity__c = lstWithOppty[0].Id;
            lstWithOpportunityUniversityCourse[0].Account__c = lstWithAccount[0].Id;
            for(UniversityCourse__c newUnivCourse : univCourse)
            {
                newUnivCourse.Account__c = lstWithAccount[0].Id;
            }
            insert univCourse;   
            lstWithOpportunityUniversityCourse[0].UniversityCourse__c = univCourse[0].Id;
            insert lstWithOpportunityUniversityCourse;
            test.stopTest();
        }
        System.runAs(lstWithTestUser[0]) 
        {
          //Test the class in 'PS_CreatePIUWithCourses' page context
          ID ContId;
          lstWithAsset = TestDataFactory.insertAsset(1,ContId,ContId,lstWithProduct[0].Id,lstWithAccount[0].Id);
          if(lstWithAsset.size() > 0)
          {
              insert lstWithAsset;
          }
          newCourseProductInUse.Product_in_Use__c = lstWithAsset[0].Id;
          newCourseProductInUse.Course__c = univCourse[0].Id;
          lstWithCourseProductInUse.add(newCourseProductInUse);
          insert lstWithCourseProductInUse;
          newContactProductInUse.Product_in_Use__c = lstWithAsset[0].Id;
          newContactProductInUse.Contact__c = lstWithContact[0].Id;
          lstWithContactProductInUse.add(newContactProductInUse);
          insert lstWithContactProductInUse;
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_UpdatePIUWithCourses?channel='+lstWithOppty[0].Channel__c+'&opportunityId='+lstWithOppty[0].Id);
              Test.setCurrentPage(pageRef);
              PS_UpdatePIUWithCoursesController controllerObj = new PS_UpdatePIUWithCoursesController();
              controllerObj.getAssetUniversityCourse();
              controllerObj.getUsage();
              controllerObj.getStatus();
              controllerObj.getModeOfDelivery();
              controllerObj.getThirdPartyLMS();
              controllerObj.getChannel();
              controllerObj.Next();
              controllerObj.Previous();
              controllerObj.End();
              controllerObj.Beginning();
              controllerObj.getDisablePrevious();
              controllerObj.getDisableNext();
              controllerObj.getTotal_size();
              controllerObj.getPageNumber();
              controllerObj.getTotalPages();
              controllerObj.cancel();
              controllerObj.selectedAssetId = lstWithAsset[0].Id;
              controllerObj.createNewWrapperRecord();
              controllerObj.updatePIU();
              controllerObj.createNewWrapperRecord();
          } 
        }     
    }
}