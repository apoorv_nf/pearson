public class MobileCacheWarmerController {
    @RemoteAction
    @readOnly
    public static Integer getPCl(String pBID) {

        list < HierarchyProduct__c > rpCount = [SELECT Id FROM HierarchyProduct__c WHERE((Product__c IN(SELECT Product__c FROM CatalogPrice__c WHERE Catalog__c =: pBID AND ValidForMobile__c = '1'
            AND isActive__c = true))) LIMIT 100000];
        Integer rpCountRes = rpCount.size();
        return rpCountRes;
    }

    @RemoteAction
    @readOnly
    public static Integer getMI(String pBID) {
            list < Marketing_Information__c > rpCount = [SELECT Id FROM Marketing_Information__c WHERE((Family__c in (SELECT Product__c FROM CatalogPrice__c WHERE Catalog__c =: pBID AND ValidForMobile__c = '1'
                AND isActive__c = true))) LIMIT 100000];
        Integer rpCountRes = rpCount.size();
        return rpCountRes;
    }

    @RemoteAction
    @readOnly
    public static Integer getRP(String pBID) {
        list < RelatedProduct__c > rpCount = [SELECT Id FROM RelatedProduct__c WHERE((Product__c IN(SELECT Product__c FROM CatalogPrice__c WHERE Catalog__c =: pBID AND ValidForMobile__c = '1'
            AND isActive__c = true))) LIMIT 100000];
        Integer rpCountRes = rpCount.size();
        return rpCountRes;
    }

    @RemoteAction
    @readOnly
    public static Integer getPC(String pBID) {


        list < Pearson_Choice__c > rpCount = [SELECT Id FROM Pearson_Choice__c WHERE((Product_Family__c IN(SELECT Product__c FROM CatalogPrice__c WHERE Catalog__c =: pBID AND ValidForMobile__c = '1'
                AND isActive__c = true)))

       LIMIT 100000];
        Integer rpCountRes = rpCount.size();
        return rpCountRes;
    }

}