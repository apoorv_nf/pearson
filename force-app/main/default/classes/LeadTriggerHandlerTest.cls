@IsTest(SeeAllData=true)
public class LeadTriggerHandlerTest {
    static testMethod void testLeadCreationWebRequest()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        lstWithTestUser[0].market__c='UK';
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c=true,
                                                        Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
        insert byp; 
        AccountTeamMember newAccountTeamMember = new AccountTeamMember();
        List<Lead> lstLeadRecords = new List<Lead>();
        Id accountId;  
        System.runAs(lstWithTestUser[0]) 
        { 
            RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional' Limit 1];
            Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
            acc.ShippingStreet = 'TestStreet';
            acc.ShippingCity = 'Vns';
            acc.ShippingState = 'Delhi';
            acc.ShippingPostalCode = '234543';
            acc.ShippingCountry = 'India';
            acc.recordtypeId=OrgAcc.Id ;
            acc.Academic_Achievement__c='High';
            acc.Market2__c = 'UK';
            acc.Organisation_Type__c='School';
            acc.IsCreatedFromLead__c = true;
            acc.SelfServiceCreated__c=false;
            acc.Abbreviated_Name__c='TestName';            
            insert acc;
            
            //Contact acc = new Contact (Name='testContact',
            List<Contact> lstContact = TestDataFactory.createContact(1);
            insert lstContact;
            
            lstLeadRecords = TestDataFactory.createLead(1,'Web Request');
            
            for(Lead lstLead : lstLeadRecords)
            {  
                lstLead.Channel__c ='Direct';
                lstLead.Organisation_Type1__c = 'Large Corporate';
                lstLead.Role__c = 'Employee';
                lstLead.Role_Detail__c = 'Administrator';
                lstLead.Other_Role_Detail__c = 'Test';
                lstLead.Trust_Level__c = 'Low';
                lstLead.PostalCode = '345678';
                lstLead.State = 'Bihar';
                lstLead.Street = 'TestStreet';                
                lstLead.Institution_Organisation__c =acc.Id;
                lstLead.Contact_Lookup__c=lstContact[0].id;
                lstLead.Market__c = 'US';   
                lstLead.Enquiry_Type__c ='Digital Access';
            }
            
            test.startTest();
            Database.insert(lstLeadRecords);
            Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
            for(Lead newLead : lstLeadRecords)
            {
                mapWithLead.put(newLead.Id,newLead);
            }
            for(Lead newLead : lstLeadRecords)
            {
                Database.LeadConvert convertLead = new database.LeadConvert();
                convertLead.setLeadId(newLead.ID);
                convertLead.setDoNotCreateOpportunity(false);
                convertLead.setConvertedStatus('Validated');
                Database.LeadConvertResult lcr = Database.convertLead(convertLead);
                System.assert(lcr.isSuccess());
                accountId = lcr.getAccountId();
            }
            List<Lead> lstWithLead = [select isConverted,Campaign__c,ownerid,LeadSource,Account_Lookup__c,Contact_Lookup__c,Comments__c,City,State,Country,CurrencyIsoCode,FirstName,Street,LastName,Phone,Email,Enquiry_Type__c,Validated_By__c,Validated_On__c,Trust_Level__c,Global_Marketing_Unsubscribe__c,Salutation,Role__c,Role_Detail__c,Other_Role_Detail__c,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,Market__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId from lead where id IN: mapWithLead.keyset()];
            PS_LeadConversionMapping.createMapping(lstWithLead,mapWithLead);
            
            test.stopTest();
        }
        /*LeadTriggerHandler Ledtrighandlr = new LeadTriggerHandler();
Ledtrighandlr.OnBeforeDelete(lstWithLead, mapWithLead);
Ledtrighandlr.OnAfterDelete(lstWithLead, mapWithLead);*/
    }
    static testMethod void testLeadCreationWebRequest2()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        lstWithTestUser[0].market__c='UK';
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c=true,
                                                        Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
        insert byp; 
        AccountTeamMember newAccountTeamMember = new AccountTeamMember();
        List<Lead> lstLeadRecords = new List<Lead>();
        Id accountId;  
        System.runAs(lstWithTestUser[0]) 
        { 
            RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional' Limit 1];
            
            Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
            acc.ShippingStreet = 'TestStreet';
            acc.ShippingCity = 'Vns';
            acc.ShippingState = 'Delhi';
            acc.ShippingPostalCode = '234543';
            acc.ShippingCountry = 'India';
            acc.recordtypeId=OrgAcc.Id ;
            acc.Academic_Achievement__c='High';
            acc.Market2__c = 'UK';
            acc.Organisation_Type__c='School';
            acc.IsCreatedFromLead__c = true;
            acc.SelfServiceCreated__c=false;
            acc.Abbreviated_Name__c='TestName';            
            insert acc;
            List<Account> lstAccount = new List<Account>();
            lstAccount.add(acc);
            //Contact acc = new Contact (Name='testContact',
            List<Contact> lstContact = TestDataFactory.createContact(1);
            insert lstContact;
            lstLeadRecords = TestDataFactory.createLead(1,'Web Request');
            
            for(Lead lstLead : lstLeadRecords)
            { 
                lstLead.Channel__c ='Direct';
                lstLead.Organisation_Type1__c = 'Large Corporate';
                lstLead.Role__c = 'Employee';
                lstLead.Role_Detail__c = 'Administrator';
                lstLead.Other_Role_Detail__c = 'Test';
                lstLead.Trust_Level__c = 'Low';
                lstLead.PostalCode = '345678';
                lstLead.State = 'Bihar';
                lstLead.Street = 'TestStreet';                
                lstLead.Institution_Organisation__c =acc.Id;
                lstLead.Contact_Lookup__c=lstContact[0].id;
                lstLead.Market__c = 'UK';   
                lstLead.Enquiry_Type__c ='Digital Access';
                lstLead.Line_of_Business__c='Schools';
                lstLead.Business_Unit__c='Schools';
                lstLead.Geography__c='Growth';
            }
            
            test.startTest();
            Database.insert(lstLeadRecords);
            Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
            for(Lead newLead : lstLeadRecords)
            {
                mapWithLead.put(newLead.Id,newLead);
            }
            List<Lead> lstWithLead = [select isConverted,Campaign__c,ownerid,LeadSource,Account_Lookup__c,Contact_Lookup__c,Comments__c,City,State,Country,CurrencyIsoCode,FirstName,Street,LastName,Phone,Email,Enquiry_Type__c,Validated_By__c,Validated_On__c,Trust_Level__c,Global_Marketing_Unsubscribe__c,Salutation,Role__c,Role_Detail__c,Other_Role_Detail__c,Estimated_Value__c,Business_unit__c,Decision_Making_Level__c,RecordTypeId,Channel__c,Organisation_Type1__c,Market__c,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId from lead where id IN: mapWithLead.keyset()];
            
             
             LeadTriggerHandler Ledtrighandlr = new LeadTriggerHandler(true,2);
            Ledtrighandlr.OnBeforeDelete(lstWithLead, mapWithLead);
             
            //LeadTriggerHandler.OnBeforeDelete(lstWithLead, mapWithLead);
             Ledtrighandlr.OnAfterDelete(lstWithLead, mapWithLead);
             Ledtrighandlr.OnUndelete(lstWithLead);
            PS_Lead_UpdateStudentId ryut = new PS_Lead_UpdateStudentId();
            Test.StopTest(); 
            // ryut.beforelead1('lstAccount');
            //  ryut.beforelead('lstAccount');
        }
    }
     static testMethod void testLeadTriggerHandlerVariable()
    {
        LeadTriggerHandler Ledtrighandlr = new LeadTriggerHandler(true,2);
               
    }
}