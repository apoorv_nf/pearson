@istest
public class CasepopulateDefaultTest {

static testmethod void defaultcase(){
    
    List<case> caseListForSC = TestDataFactory.createCase_SelfService(1,'General');
    ApexPages.StandardController sc = new ApexPages.StandardController(caseListForSC[0]);
    
    string recordtypeId = '';
    recordtype recordtypeInst = [select Id from recordtype where developername = 'Technical_Support' and sobjecttype = 'Case'];
    recordtypeId = recordtypeInst.Id;
     
    ApexPages.currentPage().getParameters().put('RecordType',recordtypeId);
  
    // Set up User
    String ProfileId = [select Id from profile where Name = 'Pearson Sales User OneCRM'].Id;
    List<User> ULst = TestDataFactory.createUser(ProfileId);
    user u = ULst[0];
 
/*    //Set up a custom setting
    list<CS_user_BV_Map__c> lstContacts=new   list<CS_user_BV_Map__c>();//bulk List of custom setting object for bulk insert

 	CS_user_BV_Map__c csContactFields=new CS_user_BV_Map__c(); 
	csContactFields.Name='CreatedDate';//Static record 1 of custom setting
	lstContacts.add(csContactFields);
	insert lstContacts;*/
  
    
    System.runAs(u) {
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();   
    }
    
    ULst[1].Line_of_Business__c = 'Professional';
    
    System.runAs(ULst[1]) {
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();
    }
    
    ULst[2].Line_of_Business__c = '';
    
    System.runAs(ULst[2]) {
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();
    }
    ULst[3].Line_of_Business__c = 'Higher Ed';
      System.runAs(ULst[3]) {
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();
    }
    
       
    /*if(recordtypeId != null && recordtypeId != 'Technical support'){
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();
    }
   else  if(recordtypeId == null && u.Business_Vertical__c  == null){
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();
    }
    else  if(recordtypeId == null && u.Business_Vertical__c  != null){
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();
    }*/
}
    
static testmethod void defaultcaseNotTS(){
    
    List<case> caseListForSC = TestDataFactory.createCase_SelfService(1,'General');
    ApexPages.StandardController sc = new ApexPages.StandardController(caseListForSC[0]);
    
    string recordtypeId = '';
    recordtype recordtypeInst = [select Id from recordtype where developername = 'Technical_Support' and sobjecttype = 'Case'];
    recordtypeId = recordtypeInst.Id;
     
    ApexPages.currentPage().getParameters().put('RecordType',recordtypeId);
  
    // Set up User
    String ProfileId = [select Id from profile where Name = 'Pearson Sales User OneCRM'].Id;
    List<User> ULst = TestDataFactory.createUser(ProfileId);
    user u = ULst[0];
 
/*    //Set up a custom setting
    list<CS_user_BV_Map__c> lstContacts=new   list<CS_user_BV_Map__c>();//bulk List of custom setting object for bulk insert

 	CS_user_BV_Map__c csContactFields=new CS_user_BV_Map__c(); 
	csContactFields.Name='CreatedDate';//Static record 1 of custom setting
	lstContacts.add(csContactFields);
	insert lstContacts;*/
  
    
    System.runAs(u) {
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();   
    }
    
    ULst[1].Line_of_Business__c = 'Professional';
    ULst[1].Business_Vertical__c = 'Higher Education';
    
    System.runAs(ULst[1]) {
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();
    }
    
    ULst[2].Business_Vertical__c = '';
    
    System.runAs(ULst[2]) {
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();
    }
    
    ULst[3].Line_of_Business__c = 'Higher Ed';
      System.runAs(ULst[3]) {
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();
    }
    
    recordtypeInst = [select Id from recordtype where developername = 'General' and sobjecttype = 'Case'];
    recordtypeId = recordtypeInst.Id;
    ApexPages.currentPage().getParameters().put('RecordType',recordtypeId);
    
    ULst[4].Business_Vertical__c = 'Higher Education';
      System.runAs(ULst[4]) {
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();
    }
    
    ApexPages.currentPage().getParameters().put('RecordType',null);
    
    ULst[5].Business_Vertical__c = 'Higher Education';
      System.runAs(ULst[5]) {
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();
    }
    
    ULst[6].Business_Vertical__c = null;
      System.runAs(ULst[6]) {
        CasepopulateDefault CaseDefaultInst = new CasepopulateDefault(sc);
    	CaseDefaultInst.Redirect();
    }
    
    

}
}