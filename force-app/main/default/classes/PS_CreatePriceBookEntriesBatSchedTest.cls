/**
 * Name : PS_CreatePriceBookEntriesBatSchedTest
 * Author : Accenture_Karan
 * Description : Test class used for testing PS_CreatePriceBookEntriesBatchSchedule
 * Date :15/01/16 
 * Version : <intial Draft> 
 */
 
@istest
class PS_CreatePriceBookEntriesBatSchedTest {

    static testmethod void testScheduleClass() {
        
        String CRON_EXP = '0 0 0 3 9 ? 2022';
        
        Test.startTest();
        
        // Schedule the test job

        String jobId = System.schedule('testBasicScheduledApex', CRON_EXP, new PS_CreatePriceBookEntriesBatchSchedule());

        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

        // Verify the expressions are the same
        System.assertEquals(CRON_EXP, ct.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));

        Test.stopTest();
    }
}