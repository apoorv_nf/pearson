/**
 * Name : PS_UpdateOrderItemStatusTest
 * Author : Accenture_Karan
 * Description : Test class used for testing PS_UpdateOrderItemStatus
 * Date : 07/12/15 
 * Version : <intial Draft> 
 */

@isTest
public class PS_UpdateOrderItemStatusTest
{
    static testMethod void testUpdateOrderItemStatus1(){
    
        // Test data preparation                            
        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1); 
        insert usList;       
        Bypass_Settings__c settings = new Bypass_Settings__c();
        settings.Disable_Triggers__c = true;
       	settings.SetupOwnerId = usList[0].id;
        insert settings;  
        
        System.RunAs(usList[0]){
            test.startTest();
            Order ord = TestDataFactory.returnorder();
            
            settings.Disable_Triggers__c = false;
       		settings.SetupOwnerId = usList[0].id;
            update settings;
            ord.Salesperson__c = usList[0].Id;      
            ord.Status = 'Filled';
            ord.Market__c = 'AU';
            ord.Line_of_Business__c = 'Higher Ed';
            ord.Business_Unit__c = 'Higher Ed';           
            update ord;         
        	test.stopTest();     
        }   
    }
    
    static testMethod void testUpdateOrderItemStatus2(){
    
        // Test data preparation                            
        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        insert usList;        
            
        
        Bypass_Settings__c settings = new Bypass_Settings__c();
        settings.Disable_Triggers__c = true;
       	settings.SetupOwnerId = usList[0].id;
        insert settings;  
         
        System.RunAs(usList[0]){
        
            test.startTest();
            Order ord = TestDataFactory.returnorder();
            settings.Disable_Triggers__c = false;
       		settings.SetupOwnerId = usList[0].id;
            update settings;
            ord.Salesperson__c = usList[0].Id;      
            ord.Status = 'Filled';
            ord.Market__c = 'NZ';
            ord.Line_of_Business__c = 'Higher Ed';
            ord.Business_Unit__c = 'Edify';
            update ord;             
        	test.stopTest();     
        }
    }
    
    static testMethod void testUpdateOrderItemStatus3(){
    
        // Test data preparation                            
        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        insert usList;       
        Bypass_Settings__c settings = new Bypass_Settings__c();
        settings.Disable_Triggers__c = true;
       	settings.SetupOwnerId = usList[0].id;
        insert settings;  
        
        System.RunAs(usList[0]){  
            test.startTest();
        	Order ord = TestDataFactory.returnorder();
           
        	settings.Disable_Triggers__c = false;
       		settings.SetupOwnerId = usList[0].id;
            update settings;
            ord.Salesperson__c = usList[0].Id;       
            ord.Status = 'Filled'; 
            ord.Market__c = 'UK';
            ord.Line_of_Business__c = 'Higher Ed';
            ord.Business_Unit__c = 'Higher Ed';
            update ord;
            OrderItem oi = [select id, status__c, orderId from OrderItem where orderId=: ord.Id];
            
            System.assertEquals('Shipped', oi.status__c);
        	test.stopTest(); 
        }
    }
}