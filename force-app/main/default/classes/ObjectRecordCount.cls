public without sharing class ObjectRecordCount implements schedulable {

        public void execute(SchedulableContext sc){updateRecordCounts();}
        
        public class DeserializeAccessToken{
        public String id;
        public String access_token;
        }
        
       
        @future(callout=true)
        public static void updateRecordCounts()
        {
            String granttype = 'password';
            ObjectRecordCountClient__c objRec = ObjectRecordCountClient__c.getInstance('Credential');
            String clientId = objRec.ClientId__c;            
            String clientSecret = objRec.Client_Secret__c;
            ObjectRecordUserCredentials__c userCred = ObjectRecordUserCredentials__c.getInstance('UserCredentials');
            String userName = userCred.UserName__c;
            String userPassword = userCred.UserPassword__c;
            String reqBody = 'grant_type='+granttype+'&client_id='+clientId+'&client_secret='+clientSecret+'&username='+username+'&password='+userPassword;
            String endPoint  = Label.ObjectRecordEndPoint;
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            HttpResponse response = new HttpResponse();

            request.setMethod('POST');
            request.setBody(reqBody);
            request.setEndpoint(endPoint);
            response = http.send(request);
            String accessToken='';
            if(!Test.isRunningtest()){
            DeserializeAccessToken deToken = (DeserializeAccessToken) JSON.deserialize(response.getBody(), DeserializeAccessToken.class);
            accessToken = deToken.access_token;
            }
            String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm(); 
            ObjectRecordCounts__c orc = new ObjectRecordCounts__c();
            orc.Name=string.valueOf(system.now());
            
            String restAPIURL = sfdcURL + '/services/data/v40.0/limits/recordCount?sObjects='+Label.ObjectsName;
            HttpRequest httpRequest = new HttpRequest();  
            httpRequest.setMethod('GET');   
            httpRequest.setHeader('Authorization', 'Bearer ' + accessToken); 
            httpRequest.setEndpoint(restAPIURL);  
            String count= '';
            Integer objCount;
              
            Http http1 = new Http();   
            HttpResponse httpResponse = http1.send(httpRequest);  
            if (httpResponse.getStatusCode() == 200 ) {  
               system.JSONParser parser = JSON.createParser(httpResponse.getBody());
               while(parser.nextToken() != null)
                    {
                            if(parser.getText() == 'count')
                            {
                                parser.nextToken();
                                count= parser.getText();
                                objCount = Integer.valueOf(count);
                            }
                            
                            if(parser.getText()=='Account'){
                                 orc.Account__c = objCount;
                             }
                            else if(parser.getText()=='Account_Correlation__c'){
                                 orc.Account_Correlation__c= objCount;  
                             }
                            else if(parser.getText()=='AccountContact__c'){
                                 orc.AccountContact__c= objCount;  
                             }
                            else if(parser.getText()=='Apttus_Config2__ClassificationHierarchy__c'){
                                 orc.Apttus_Config2_ClassificationHierarchy__c= objCount;  
                             }
                            else if(parser.getText()=='Apttus_Config2__ClassificationName__c'){
                                 orc.Apttus_Config2_ClassificationName__c= objCount;  
                             }
                            else if(parser.getText()=='Apttus_Config2__PriceList__c'){
                                 orc.Apttus_Config2_PriceList__c= objCount;  
                             }
                            else if(parser.getText()=='Apttus_Config2__PriceListCategory__c'){
                                 orc.Apttus_Config2_PriceListCategory__c= objCount;  
                             }
                            else if(parser.getText()=='Apttus_Config2__PriceListItem__c'){
                                 orc.Apttus_Config2_PriceListItem__c= objCount; 
                             }
                            else if(parser.getText()=='Apttus_Config2__ProductClassification__c'){
                                 orc.Apttus_Config2_ProductClassification__c= objCount;  
                             }
                            else if(parser.getText()=='Apttus_Config2__RelatedProduct__c'){
                                 orc.Apttus_Config2_RelatedProduct__c= objCount;  
                             }
                            else if(parser.getText()=='Asset'){
                                 orc.Asset__c= objCount;  
                             }
                            else if(parser.getText()=='Campaign'){
                                 orc.Campaign__c= objCount;  
                             }
                            else if(parser.getText()=='CampaignMember'){
                                 orc.CampaignMember__c= objCount;  
                             }
                            else if(parser.getText()=='Case'){
                                 orc.Case__c= objCount;  
                             }
                            else if(parser.getText()=='Competitor_Product__c'){
                                 orc.Competitor_Product__c= objCount;  
                             }
                            else if(parser.getText()=='Contact'){
                                 orc.Contact__c= objCount;  
                             }
                            else if(parser.getText()=='Contact_Correlation__c'){
                                 orc.Contact_Correlation__c= objCount;  
                             }
                            else if(parser.getText()=='Event'){
                                 orc.Event__c= objCount;  
                             }
                            else if(parser.getText()=='EventRelation'){
                                 orc.EventRelation__c= objCount;  
                             }
                            else if(parser.getText()=='Lead'){
                                 orc.Lead__c= objCount;  
                             }
                            else if(parser.getText()=='ObjectTerritory2AssignmentRule'){
                                 orc.ObjectTerritory2AssignmentRule__c= objCount;  
                             }
                            else if(parser.getText()=='ObjectTerritory2AssignmentRuleItem'){
                                 orc.ObjectTerritory2AssignmentRuleItem__c= objCount;  
                             }
                            else if(parser.getText()=='ObjectTerritory2Association'){
                                 orc.ObjectTerritory2Association__c= objCount;  
                             }
                            else if(parser.getText()=='Opportunity'){
                                 orc.Opportunity__c= objCount;  
                             }
                            else if(parser.getText()=='OpportunityContactRole'){
                                 orc.OpportunityContactRole__c= objCount;  
                             }
                            else if(parser.getText()=='OpportunityHistory'){
                                 orc.OpportunityHistory__c= objCount;  
                             }
                            else if(parser.getText()=='OpportunityLineItem'){
                                 orc.OpportunityLineItem__c= objCount;  
                             }
                            else if(parser.getText()=='OpportunityUniversityCourse__c'){
                                 orc.OpportunityUniversityCourse__c= objCount;  
                             }
                            else if(parser.getText()=='Order'){
                                 orc.Order__c= objCount;  
                             }
                            else if(parser.getText()=='OrderItem'){
                                 orc.OrderItem__c= objCount;  
                             }
                            else if(parser.getText()=='Pearson_Choice__c'){
                                 orc.Pearson_Choice__c= objCount;  
                             }
                            else if(parser.getText()=='Pearson_Course_Equivalent__c'){
                                 orc.Pearson_Course_Equivalent__c= objCount;  
                             }
                            else if(parser.getText()=='Price_List_Mapping__c'){
                                 orc.Price_List_Mapping__c= objCount;  
                             }
                            else if(parser.getText()=='PricebookEntry'){
                                 orc.PricebookEntry__c= objCount;  
                             }
                            else if(parser.getText()=='Product__c'){
                                 orc.Product__c= objCount;  
                             }
                            else if(parser.getText()=='Product2'){
                                 orc.Product2__c= objCount;  
                             }
                            else if(parser.getText()=='RuleTerritory2Association'){
                                 orc.RuleTerritory2Association__c= objCount;  
                             }
                            else if(parser.getText()=='Task'){
                                 orc.Task__c= objCount;  
                             }
                            else if(parser.getText()=='Territory2'){
                                 orc.Territory2__c= objCount;  
                             }
                            else if(parser.getText()=='Territory2Model'){
                                 orc.Territory2Model__c= objCount;  
                             }
                            else if(parser.getText()=='Territory2Type'){
                                 orc.Territory2Type__c= objCount;  
                             }
                            else if(parser.getText()=='UniversityCourse__c'){
                                 orc.UniversityCourse__c= objCount;  
                             }
                            else if(parser.getText()=='UniversityCourseContact__c'){
                                 orc.UniversityCourseContact__c= objCount;  
                             }
                            else if(parser.getText()=='User'){
                                 orc.User__c= objCount;  
                             }
                            else if(parser.getText()=='UserRole'){
                                 orc.UserRole__c= objCount;  
                             }
                            else if(parser.getText()=='UserTerritory2Association'){
                                 orc.UserTerritory2Association__c= objCount;  
                             }
                            else if(parser.getText()=='VisitPlan__c'){
                                 orc.VisitPlan__c= objCount;  
                             }
                            
                         
                     }
                insert orc; 

              } else {  
                 throw new CalloutException( httpResponse.getBody() );  
              } 
            list<ObjectRecordCounts__c> oldorcs=[select id from ObjectRecordCounts__c where CreatedDate<:system.today().addDays(-60)];
            if(oldorcs.size()>0){database.delete(oldorcs);}
            
     }
}