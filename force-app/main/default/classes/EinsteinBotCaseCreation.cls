/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 12/05/2018
 * Update: Added Error_Message__c - Shantanu (Neuraflash) - 27/12/2018
 *         Removed the Role and hardcoded the role in the platform event trigger handler
 * Update: Added Origin__c - checks for the origin and sets the Type of the platform event accordingly - Apoorv (Neuraflash) - 26/07/2019
 * Update: Reverting back to asynchronous case creation - 13/11/2019             
 */
global without sharing class EinsteinBotCaseCreation {

    @InvocableMethod(label='Einstein Bot - Create Case')
    global static void createCase(List<CaseCreationRequest> caseRequests) 
    {
        List<Einstein_Bot_Event__e> createCaseEvents = new List<Einstein_Bot_Event__e>();
        
        for(CaseCreationRequest caseRequest : caseRequests) {
            Einstein_Bot_Event__e createCaseEvent = new Einstein_Bot_Event__e();
            if(!String.isBlank(caseRequest.origin) && caseRequest.origin == 'SMS'){
                createCaseEvent.Type__c = EinsteinBotEventHandler.INSERT_CASE;
            } else {
                createCaseEvent.Type__c = EinsteinBotEventHandler.UPDATE_CASE;
            }
            createCaseEvent.Email__c = caseRequest.email;
            createCaseEvent.Institution__c = caseRequest.institution;
            createCaseEvent.Live_Agent_Session_Id__c = caseRequest.liveAgentSessionId;
            createCaseEvent.Access_Code__c = caseRequest.accessCode;
            createCaseEvent.Category__c = caseRequest.category;
            createCaseEvent.Subcategory__c = caseRequest.subcategory;
            createCaseEvent.Platform__c = caseRequest.platform;
            createCaseEvent.Contact_Id__c = caseRequest.courseName;
            createCaseEvent.Message__c = caseRequest.message;
            createCaseEvent.Username__c = caseRequest.userName;
            createCaseEvent.Error_Message__c = caseRequest.errorMessage;
            createCaseEvent.Origin__c = caseRequest.origin;
            
            createCaseEvents.add(createCaseEvent);
        }

        // if(createCaseEvents[0].Type__c == EinsteinBotEventHandler.INSERT_CASE){
        //     new EinsteinBotEventHandler().insertCase(createCaseEvents[0]);
        // } else {
        List<Database.SaveResult> saveResults = EventBus.publish(createCaseEvents);
        //}
    }

    global class CaseCreationRequest
    {
        @InvocableVariable
        global String liveAgentSessionId;

        @InvocableVariable
        global String email;

        @InvocableVariable
        global String institution;


        @InvocableVariable
        global String platform; 

        @InvocableVariable
        global String accessCode;

        @InvocableVariable
        global String courseName;

        @InvocableVariable
        global String errorMessage;

        @InvocableVariable
        global String category;

        @InvocableVariable
        global String subcategory;

        @InvocableVariable
        global String message;

        @InvocableVariable
        global String userName;

        @InvocableVariable
        global String origin;
        
    }
}