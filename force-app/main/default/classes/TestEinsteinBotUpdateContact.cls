@isTest
private class TestEinsteinBotUpdateContact {

	@isTest
	static void it_should_update_contact() {
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];

		String chatKey = 'abc';

		Contact contact = new Contact(
			FirstName = 'Rick',
			LastName = 'Sanchez',
			Email = 'rick.sanchez@email.com'
		);
		insert contact;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

		LiveChatTranscript transcript = new LiveChatTranscript(
			ContactId = contact.Id,
			ChatKey = chatKey,
			LiveChatVisitorId = vistor.Id);
		insert transcript;

		EinsteinBotUpdateContact.UpdateContactRequest request = new EinsteinBotUpdateContact.UpdateContactRequest();
		request.contact = contact;
		request.role = 'Student';
		request.chatSessionKey = chatKey;

		Test.startTest();
        System.runAs(automatedProcess) {
			EinsteinBotUpdateContact.updateContact(new List<EinsteinBotUpdateContact.UpdateContactRequest>{request});
			Test.getEventBus().deliver();
        }
		Test.stopTest();

		System.assertEquals(1, [
			SELECT Count()
			FROM Contact
			WHERE Role__c = 'Student'
		]);
	}
}