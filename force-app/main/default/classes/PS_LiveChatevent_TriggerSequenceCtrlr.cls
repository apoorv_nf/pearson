/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_LiveChatevent_TriggerSequenceCtrlr.cls 
   Description:     On insert/update/delete of Live Chat Transcript record 
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
    02/12/2016           1.0            Karthik.A.S                     Live Chat Transcripts event on Cases
    02/24/2016           2.0            KP                              changes after PS_LiveChatTranscriptEventtracking amendment
    10/03/2016           3.0            KP                              amended code
  ------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public  class PS_LiveChatevent_TriggerSequenceCtrlr 
{   
    /**
    * Description : Method to call after insert of chat events
    * @param : new event list and new event map
    * @return NA
    * @throws NA
    **/
    public static void afterInsert(List<LiveChatTranscriptevent> chatList,Map<Id,LiveChatTranscriptevent> chatMap)
    {  
        PS_LiveChatTranscriptEventtracking.mInitChatEvent(chatList,chatMap);
    }
}