/************************************************************************************************************
* Apex Class Name   : PS_UniversityCourseContactOperations.cls 
* Version           : 1.0
* Created Date      : 07 OCT 2014 
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Gousia begum                07/10/2015              Created as per RD-01274
************************************************************************************************************/

public with sharing class PS_UniversityCourseContactOperations implements PS_GenricTriggerInterface {

 public void beforeInsert(List<sObject> ccNewList, Map<ID,sObject> NewMap) {
     
    
 }
 public void afterInsert(List<sobject> ccNewList, Map<ID,sobject> NewMap) {
     //updatePrimaryContact(ccNewList);
     updatePrimaryContactInUniversityCourse(ccNewList);
 }
 
 public void beforeUpdate(List<sobject> ccNewList, Map<ID,sobject> NewMap,List<sobject> ccOldList, Map<ID,sobject> OldMap) {    
     
 
 }
 
 public void afterUpdate(List<sobject> ccNewList,  Map<ID,sobject> NewMap, List<sobject> ccOldList, Map<ID,sobject> OldMap) {
     //updatePrimaryContact(ccNewList);
     updatePrimaryContactInUniversityCourse(ccNewList);
 }
 
 
 
 public void beforeDelete(List<sobject> ccNewList, Map<ID,sobject> NewMap,List<sobject> ccOldList, Map<ID,sobject> OldMap) {    
     
 }
 public void afterDelete(List<sobject> ccNewList, Map<ID,sobject> NewMap,List<sobject> ccOldList, Map<ID,sobject> OldMap) {
    updatePrimaryContactInUniversityCourse(ccOldList);
 }
 public void afterUndelete(List<sobject> ccOldList, Map<ID,sobject> OldMap) {
     
    
 } 
 
 //Method to update primary contact on Course
    /*public void updatePrimaryContact(List<UniversityCourseContact__c> ccList){
          List<UniversityCourse__c > uCourseList = new List<UniversityCourse__c >();
          Map<Id,Id> uCourseContactMap= new Map<Id,Id>();
          //For loop to check the contact role ,and add it to map
          try{
          for(UniversityCourseContact__c courseContObj : ccList){
                if(courseContObj.PS_Contact_Role__c!=null && courseContObj.PS_Contact_Role__c.equalsIgnoreCase(Label.PS_Decision_Maker)){        
                    uCourseContactMap.put(courseContObj.UniversityCourse__c, courseContObj.Contact__c);            
                }          
            }
            for(UniversityCourse__c courseObj : [select Id,Primary_Contact__c from UniversityCourse__c where Id IN :uCourseContactMap.keySet()]){        
              //Assign the university course contact which has Role='Decision Maker' to primary contact of course
              if(uCourseContactMap.containsKey(courseObj.ID) && courseObj.Primary_Contact__c == null){
                courseObj.Primary_Contact__c = uCourseContactMap.get(courseObj.ID);               
                uCourseList.add(courseObj);
                }
              }
            /////////////////////////////////////////////////////////////////////////////////////
              if(!uCourseList.isEmpty()){
                   Database.SaveResult[] lstResult =  Database.update(uCourseList, false);
                    if (!lstResult.isEmpty()){
                            List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
                                for(Database.SaveResult resultObj:lstResult){
                                        String ErrMsg='';
                                        if (!resultObj.isSuccess()){
                                                PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                                                errlogger.InterfaceName__c='Course Primary Contact Mapping';
                                                errlogger.ApexClassName__c='PS_UniversityCourseContactOperations';
                                                errlogger.CallingMethod__c='updatePrimaryContact';
                                                errlogger.UserLogin__c=UserInfo.getUserName(); 
                                                errlogger.recordid__c=resultObj.getId();
                                                for(Database.Error err : resultObj.getErrors())
                                                          ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage(); 
                                                errlogger.ExceptionMessage__c=  ErrMsg;  
                                                errloggerlist.add(errlogger);    
                                        }
                                    }
                                if(errloggerlist.size()>0){
                                        insert errloggerlist;
                                }
                    }
              }
            }
            catch(DmlException e){
                 ExceptionFramework.LogException('CRITICAL','PS_UniversityCourseContactOperations','updatePrimaryContact',e.getMessage(),UserInfo.getUserName(),'');
            }
             
     } */
    //method to update the primary contact field After deleting, after Update and after insert the records
    public void updatePrimaryContactInUniversityCourse(List<UniversityCourseContact__c> newlist){
        set<id> parentRecordIds = new set<id>();
        
        map<UniversityCourse__c,map<String, List<UniversityCourseContact__c>>> existingDataToUpdate = new map<UniversityCourse__c,map<String, List<UniversityCourseContact__c>>>();
        list<UniversityCourse__c> courseList = new List<UniversityCourse__c>();
        List<Primary_Contact_Field_Hierarchy__c> customSetList = new List<Primary_Contact_Field_Hierarchy__c>();
        try {
            customSetList = [SELECT ID,Contact_Role__c,Role_level_Number__c FROM Primary_Contact_Field_Hierarchy__c where Object_API_Name__c = 'UniversityCourseContact__c' and  IsActive__c = true ORDER BY Role_level_Number__c ASC];
            for (UniversityCourseContact__c temp : newlist) {
                parentRecordIds.add(temp.UniversityCourse__c);
            }
            courseList = [SELECT ID,Primary_Contact__c,(select id,PS_Contact_Role__c,Contact__c from University_Course_Contacts__r where isdeleted = false and Active__c = TRUE ORDER BY LastModifiedDate DESC) from UniversityCourse__c where Id in : parentRecordIds];
            for (UniversityCourse__c temp : courseList) {
                  //  temp.Primary_Contact__c = null;
                map<String, List<UniversityCourseContact__c>> existingDataToUpdateChildRecs = new map<String, List<UniversityCourseContact__c>>();
                for (UniversityCourseContact__c tempucc : temp.University_Course_Contacts__r) {
                 //   system.debug('********************* tempucc  :'  +tempucc);
                    //if(tempucc!=null){
                    List<UniversityCourseContact__c> uccList = new List<UniversityCourseContact__c>();
                    if (existingDataToUpdateChildRecs.containsKey(tempucc.PS_Contact_Role__c)) {
                        uccList = existingDataToUpdateChildRecs.get(tempucc.PS_Contact_Role__c);
                    }
                    uccList.add(tempucc);
                    existingDataToUpdateChildRecs.put(tempucc.PS_Contact_Role__c,uccList);
                }
               // }
                existingDataToUpdate.put(temp,existingDataToUpdateChildRecs);
            }
            courseList.clear();
            for (UniversityCourse__c ucRec : existingDataToUpdate.keySet()) {
                map<String, List<UniversityCourseContact__c>> childRecsMap = new map<String, List<UniversityCourseContact__c>>();
                childRecsMap = existingDataToUpdate.get(ucRec);
                boolean contactSet = False;
                for (Primary_Contact_Field_Hierarchy__c custSetTemp : customSetList) {
                    if (childRecsMap.containskey(custSetTemp.Contact_Role__c) && !contactSet) {
                        ucRec.Primary_Contact__c =  childRecsMap.get(custSetTemp.Contact_Role__c)[0].Contact__c;
                        courseList.add(ucRec);    
                        contactSet = true;
                        //system.debug('##################### end of first if');
                    }
                }
                if (!contactSet) {
                    ucRec.Primary_Contact__c = null;
                    courseList.add(ucRec);
                    //system.debug('$$$$$$$$$$$$$$$$$$$$$ end of second if');
                }
            }
            if (courseList.size() > 0) {
                update courseList;
            }
        }
        catch(DmlException e){
             ExceptionFramework.LogException('CRITICAL','PS_UniversityCourseContactOperations','updatePrimaryContact',e.getMessage(),UserInfo.getUserName(),'');
        }    
    }
  
 }