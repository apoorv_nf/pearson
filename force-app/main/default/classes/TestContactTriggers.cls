@isTest
private class TestContactTriggers
{
  static testMethod void myUnitTest()
  {
    Id usrId;
    Contact contObj;
    List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileid());  
    insert usrLst;  
    Test.startTest();
    System.runas(usrLst[0])
    {
      List<Account> accLst = TestDataFactory.createAccount(1,'Learner');
      insert accLst;
            
      List<Contact> cntLst = TestDataFactory.createContact(1);
      insert cntLst;
            
      cntLst[0].LastName ='Test2123';
      cntLst[0].Contact_Status__c = True;
      If(cntLst[0].Contact_Status__c = False){
      cntLst[0].Active_Contact__c = null;
      cntLst[0].Inactive_Contact__c = cntLst[0].accountid;
      }
      else
      {
      cntLst[0].Active_Contact__c = cntLst[0].accountid;
      cntLst[0].Inactive_Contact__c = null;
      }
      update cntLst[0];
      contObj = [Select id,ownerid from contact where id=:cntLst[0].id];
      usrId = contObj.ownerid;      
    }
        
    User cntOwner = [Select id from user where id=: usrId];
    system.runas(cntOwner)
    {        
      delete contObj;
      undelete contObj;
    }           
        
    ContactTriggerHandler testa=new ContactTriggerHandler();
    Test.stopTest();
   }  
    
}