/*
 * Author: Apoorv Saxena (Neuraflash)
 * Date: 06/08/2019
 * Description: Checks if the String being passed is a valid email format - Developed for SMS Bot.
 */
public with sharing class EinsteinBotVerifyEmailFormat {

    @InvocableMethod(label='Einstein SMS Bot - Validate Email Format')
    public static List<Boolean> verifyEmailFormat(List<String> emails) {
    
        try{
            String email = emails[0];
            if(!String.isBlank(email)){
                email = email.trim();
                String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
                Pattern myPattern = Pattern.compile(emailRegex);
                Matcher myMatcher = myPattern.matcher(email);

                if (myMatcher.matches()){ 
                    return new List<Boolean>{true};
                }
            }
        } catch(Exception ex){}
        return new List<Boolean>{false};
	}
}