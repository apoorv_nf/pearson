/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_ApttusMarketingInformationController
   Description:     Controller class for PS_ApttusMarketingInformation VF Page - Displays the marketing information for the product id passed in url.
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   20 Nov 2015      1.0           Accenture NDC                                         Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class PS_ApttusMarketingInformationController
{
    public string productId;
    public List <Marketing_Information__c> listWithAboutProductContent{get;set;}
    public List <Marketing_Information__c> listWithRepToolsContent{get;set;} 
    public List <Marketing_Information__c> listWithMarketingContent{get;set;} 
    public List<String> listWithAboutThisProductDisplay{get;set;}
    public List<String> listWithRepToolsDisplay{get;set;}
    public String marketingInfoSubtypeString{get;set;}
    public String marketingInformationTypeString{get;set;}
    public List<AggregateResult> orderDetails;
    public PS_ApttusMarketingInformationController()
    {
        listWithMarketingContent = new List <Marketing_Information__c>();
        listWithAboutThisProductDisplay = new List <String>();
        listWithRepToolsDisplay = new List <String>();
        listWithAboutProductContent = new List <Marketing_Information__c>();
        listWithRepToolsContent = new List <Marketing_Information__c>();
        orderDetails = new List<AggregateResult>();
        productId = ApexPages.currentPage().getParameters().get('productId');
        if(productId != null)
        {
            marketingInformationDetails();
        }
    }
    
    public void marketingInformationDetails() 
    {
         String subtype;
         Decimal order;
              
         orderDetails = [select min(order__c) OrderValue, sub_type__c from Marketing_Information__c where Product__c = : productId and type__c = 'About this Product' group by sub_type__c];
         
         if (orderDetails != null && !orderDetails .isEmpty()) 
         {
             subtype = (String) orderDetails[0].get('sub_type__c');
             order = (Decimal) orderDetails[0].get('OrderValue');
         }
         if (subtype != null && order != null) 
         {
             listWithAboutProductContent = [select content__c from Marketing_Information__c where Product__c = : productId and type__c = 'About this Product' and sub_type__c = : subtype and order__c = : order];
         }
         if (orderDetails != null && !orderDetails.isEmpty()) 
         {
             for (AggregateResult ar: orderDetails) 
             {
                 listWithAboutThisProductDisplay.add((String) ar.get('sub_type__c'));
             }
         }
         orderDetails.clear();    
         orderDetails = [select min(order__c) OrderValue, sub_type__c from Marketing_Information__c where Product__c = : productId and type__c = 'Rep Tools' group by sub_type__c];
         if(orderDetails != null && !orderDetails.isEmpty()) 
         {
             subtype = (String) orderDetails[0].get('sub_type__c');
             order = (Decimal) orderDetails[0].get('OrderValue');
         }
            
         listWithRepToolsContent = [select content__c from Marketing_Information__c where Product__c = : productId and type__c = 'Rep Tools' and sub_type__c = : subtype and order__c = : order];
            
         if (orderDetails != null && !orderDetails.isEmpty()) 
         {
             for(AggregateResult ar: orderDetails) 
             {
                 listWithRepToolsDisplay.add((String) ar.get('sub_type__c'));
             }
         }
    }
    public void marketingDetails() 
    {
        Integer MarketingOrder;
        orderDetails.clear();
        if(marketingInformationTypeString == 'About This Product')
        {
            listWithAboutProductContent.clear();
        }else
        {
            listWithRepToolsContent.clear();    
        }
        
        orderDetails = [select min(order__c) from Marketing_Information__c where Product__c = : productId AND Type__c = : marketingInformationTypeString And Sub_Type__c = : marketingInfoSubtypeString];
        
        for (AggregateResult ar: orderDetails) 
        {
            MarketingOrder = integer.ValueOf(ar.get('expr0'));
        }
        if (marketingInfoSubtypeString != null && marketingInformationTypeString != null) 
        {
            listWithMarketingContent = [select Id, Content__c, Name from Marketing_Information__c where Type__c = : marketingInformationTypeString And Sub_Type__c = : marketingInfoSubtypeString AND Order__c = : MarketingOrder AND Product__c = : productId];
        }
    }
}