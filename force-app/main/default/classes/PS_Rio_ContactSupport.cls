public without sharing class PS_Rio_ContactSupport {

    @AuraEnabled
    public static List<PS_CategoryGrouping__c> getpickvalCaseCat() 
    {
        List<PS_CategoryGrouping__c> categoryGrouping = new List<PS_CategoryGrouping__c>();
        categoryGrouping = [Select id , name , grouping__c,CountryCode__c from PS_CategoryGrouping__c where Line_Of_Business__c = null and name != 'Refunds' Order By name asc ];
		return categoryGrouping; 
    } 
    @AuraEnabled
    public static List<CS_role__c> getValueRole() 
    {        
        
        list<CS_role__c> rolelist = [SELECT Category__c,Id,Label__c,Name FROM CS_role__c where Category__c = null OR Category__c = 'All' ORDER BY Name ASC];
        
        return rolelist;
    }
    @AuraEnabled
    public static String getCaseData(String butName, String depName, String recName, String prodName) 
    {
        String retData;
        String orgId = UserInfo.getOrganizationId();
        LiveChatButton buttonList = [SELECT Id, DeveloperName, MasterLabel FROM LiveChatButton WHERE DeveloperName = :butName LIMIT 1];
        LiveChatDeployment deployList = [SELECT Id, DeveloperName, MasterLabel FROM LiveChatDeployment WHERE DeveloperName = :depName LIMIT 1];
        RecordType typeList = [SELECT Id, DeveloperName, Name FROM RecordType WHERE DeveloperName = :recName LIMIT 1];
        Product__c prodList = [SELECT Id from Product__c where name = :prodName];
        retData = buttonList.Id+':'+deployList.Id+':'+orgId+':'+typeList.Id+':'+prodList.Id;
        return retData;
    }
}