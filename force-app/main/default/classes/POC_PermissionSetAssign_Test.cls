/*****************************************************************************************************************************/
/*Description:This class is getting called from POC_CreateOnboardingUser_NewTest for code coverage

/*****************************************************************************************************************************/
public class POC_PermissionSetAssign_Test {
@future
    Public static void UserPermissionSet(id userid){
        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Pearson_Sales_Sampling_Request'];
		insert new PermissionSetAssignment(AssigneeId = userid, PermissionSetId = ps.Id);
        
        Group g = new Group(Type='Queue', Name='Test Queue');
        insert g;
        GroupMember gm = new GroupMember(GroupId = g.id, UserOrGroupId =userid);
        insert gm;
        
        PackageLicense pl = [SELECT Id FROM PackageLicense WHERE NamespacePrefix = 'copado'];
        insert new UserPackageLicense(UserId = userid, PackageLicenseId = pl.id);

    }
}