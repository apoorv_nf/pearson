/************************************************************************************************************
* Apex Interface Name : SelfService_ContactUsTest
* Version             : 1.0 
* Created Date        : 9/16/2016 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Supriyam                 9/16/2016          
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class SelfService_ContactUsTest {
 
       //Method to cover getAgentMessages()
    static testMethod void test_getAgentMessages(){
       List<String> agentMessages=new List<String>();
       String online = System.label.PS_AgentOnline; 
       String offline=  System.label.PS_AgentOffline;
       agentMessages.add(online);
       agentMessages.add(offline);
       agentMessages = SelfService_ContactUs.getAgentMessages();  
    }
    
     /*
     * Method to cover the method getValues()
     */ 
    static testMethod void test_getValues(){
        String communityUserProfileName = 'Pearson Self-Service User';
        List<User> userList = new List<User>();
        List<Customer_Support__kav> knowledgeArtVerList;
        //List<Customer_Support__kav> knowledgeArtVerToInsertList = new List<Customer_Support__kav>();
        List<TopicAssignment> topicAssignToInsertList = new List<TopicAssignment>();
        List<String> searchStr= new List<String>();
        String countryStr, roleStr;
        searchStr.add('Test Article');
        searchStr.add('TestQual');
        searchStr.add('TestQualSub');
        countryStr = '';
        roleStr = '';
        User usr;
        UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){      
            //user for 'Customer Community Login' license
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
        }
            Test.StartTest();
        //String qualificationOptValue='TestQual';
        //String qualificationSubjectOptValue='TestQualSub';
            knowledgeArtVerList = SelfService_ContactUs.mGetValues(searchStr);
            System.assertNotEquals(knowledgeArtVerList, null);
            Test.StopTest();
    }
        /**
    * Description : Test Method to get Qualification Subject picklist values
    * @param NA
    * @return List<String>
    * @throws NA
    **/
 
    static testMethod void test_mGetQualificationSubject() 
    {        
        List<String> options = new List<String>();  
        options = SelfService_ContactUs.mGetQualificationSubject();
       
    }
       /*
     * Method to cover getter and setter methods
     */ 
    static testMethod void test_getSet(){
    
        SelfService_ContactUs conSupportObj = new SelfService_ContactUs();
        string getValueStr;         
        String communityUserProfileName = 'Pearson Self-Service User';      
        User usr;
        List<User> userList = new List<User>();
        UserRole tempUserRole;
        
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){      
            //user for 'Customer Community Login' license
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
        }
        
        
            Test.startTest();
        conSupportObj.getisAuthUser();
            SelfService_ContactUs.Subject='TestSub';
       
            conSupportObj.selectedProduct='TestProd';
            conSupportObj.uphone='TestPhone';
           // conSupportObj.sOrganization='TestOrg';
            conSupportObj.sRecType='TestRecType';
            conSupportObj.sRecTypeId='TestRecTypeId';
            conSupportObj.sAccountName='TestAccount';
            SelfService_ContactUs.QualificationSubject='ESOL';
            SelfService_ContactUs.sAANumber='12345';
            SelfService_ContactUs.CenterNumber='12345';
            SelfService_ContactUs.Product='TestProd';
            SelfService_ContactUs.Topic='TestTopic';
            SelfService_ContactUs.sSubCat='TestsSubCat';
            conSupportObj.Fname='TestFname';
            conSupportObj.Lname='TestLname';
            conSupportObj.uemail='TestLname@uemail.com';
            Test.stopTest();
        
    } 
      /*
     * Method to cover multiple methods
     */  
    static testMethod void test_SelfService_ContactUs(){
        String communityUserProfileName = 'Pearson Self-Service User';
        List<User> userList = new List<User>();
        List<product__c> getCategory = new List<product__c>();
        List<product__c> getProdValues = new List<product__c>();
        List<String> CategorySubcategoryValue = new List<String>();
        List<product__c> ProductValuesforComponent = new List<product__c>();
        List<product__c> CategoryValuesForRequestType = new List<product__c>();
        List<product__c> TopicValuesforComponent = new List<product__c>();
        List<product__c> QualificationSubjectValuesforComponent = new List<product__c>();
        List<CategorySubcategoryMapping__c> CategorySubcategoryMapping = new List<CategorySubcategoryMapping__c>();
        List<SelfServicePhoneMapping__c> SelfServicePhoneMapping= new List<SelfServicePhoneMapping__c>();
         List<PS_FetchChatButton__mdt> chatButtonList = new List<PS_FetchChatButton__mdt>();
        List<CS_country__c> country= new List<CS_country__c>();
        List<String> buttonIdList = new List<String>();  
        List<String> lstChatButDets = new List<String>(); 
        UserRole tempUserRole;
        String Businessver;
        String CustType;
        User userRec, usr;
                    //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){      
            //user for 'Customer Community Login' license
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
       //data to cover getProgramValues.getProductValues and getProgramValues.getProgramValues
            getCategory.add(new Product__c(name='category1', Active_in_Case__c=true, Type__c='Category',Market__c='UK'));
            getCategory.add(new Product__c(name='category2', Active_in_Case__c=true, Type__c='Category',Market__c='UK'));
            insert getCategory;
            
        //data to cover .getProductValues 
            getProdValues.add(new Product__c(name='prod1', Active_in_Case__c=true, Type__c='Category',Market__c='UK'));
            getProdValues.add(new Product__c(name='prdo2', Active_in_Case__c=true, Type__c='Category',Market__c='UK'));
            insert getProdValues;
           //data to cover get Product Values from Product object for lighting component  
            ProductValuesforComponent.add(new Product__c(name='prod1', Active_in_Case__c=true, Type__c='Product',Market__c='UK'));
            ProductValuesforComponent.add(new Product__c(name='prdo2', Active_in_Case__c=true, Type__c='Platform',Market__c='UK'));
            insert ProductValuesforComponent;     
       
              //data to cover get category Values from Product object for lighting component  
            CategoryValuesForRequestType.add(new Product__c(name='cate1', Active_in_Case__c=true, Type__c='Category',Market__c='UK'));
            CategoryValuesForRequestType.add(new Product__c(name='cate2', Active_in_Case__c=true, Type__c='Category',Market__c='UK'));
            insert CategoryValuesForRequestType;
            
                   //data to cover get Topic Values from Product object for lighting component  
            TopicValuesforComponent.add(new Product__c(name='Topic1', Active_in_Case__c=true, Type__c='Topic',Market__c='UK'));
            TopicValuesforComponent.add(new Product__c(name='Topic2', Active_in_Case__c=true, Type__c='Topic',Market__c='UK'));
            insert TopicValuesforComponent;
            
                  //data to cover get QualificationSubject Values from Product object for lighting component  
            QualificationSubjectValuesforComponent.add(new Product__c(name='QualificationSubjectValues1', Active_in_Case__c=true, Type__c='Qualification Subject',Market__c='UK'));
            QualificationSubjectValuesforComponent.add(new Product__c(name='QualificationSubjectValues2', Active_in_Case__c=true, Type__c='Qualification Subject',Market__c='UK'));
            insert QualificationSubjectValuesforComponent;
               //data to cover get category subcategory mapping Values from Product object for lighting component  
            CategorySubcategoryMapping.add(new CategorySubcategoryMapping__c(Name='1',AACategory__c='Category1',AASubcategory__c='SubCategory1' ));
            CategorySubcategoryMapping.add(new CategorySubcategoryMapping__c(Name='2',AACategory__c='Category2',AASubcategory__c='SubCategory2' ));
            insert CategorySubcategoryMapping;
                  //data to cover get phone mapping Values from Product object for lighting component  
            SelfServicePhoneMapping.add(new SelfServicePhoneMapping__c(Name='1',buttonoptions__c='admin',UKnumbers__c='12345'));
            SelfServicePhoneMapping.add(new SelfServicePhoneMapping__c(Name='2',buttonoptions__c='teacher',UKnumbers__c='12345'));
            insert SelfServicePhoneMapping;
              //data to cover get country Values from Product object for lighting component  
            country.add(new CS_country__c(Name='USA'));
            country.add(new CS_country__c(Name='UK'));
            insert country;
        }
         Test.StartTest();
        SelfService_ContactUs.getOrganization();
        getCategory=SelfService_ContactUs.getCategoryValues(Businessver);
        getProdValues=SelfService_ContactUs.getProductValues(Businessver);
        ProductValuesforComponent=SelfService_ContactUs.getProductValuesforComponent();
        CategoryValuesForRequestType=SelfService_ContactUs.getCategoryValuesForRequestType();
        TopicValuesforComponent=SelfService_ContactUs.getTopicValuesforComponent();
        QualificationSubjectValuesforComponent=SelfService_ContactUs.getQualificationSubjectValuesforComponent();
        CategoryValuesForRequestType=SelfService_ContactUs.getCategoryValuesforComponent(Businessver);
        TopicValuesforComponent=SelfService_ContactUs.getRoleValuesforComponent(Businessver,CustType);
        CategorySubcategoryMapping=SelfService_ContactUs.getCategorySubcategoryMapping();
        SelfServicePhoneMapping=SelfService_ContactUs.mgetphoneMapping();
        buttonIdList = SelfService_ContactUs.getButtonId('LCCI','Qualifications');
        lstChatButDets = SelfService_ContactUs.getChatButtonId('TestDevName1','TestDevName2','TestDevName3');
        getProdValues=SelfService_ContactUs.mGetProduct();
        //chatButtonList=SelfService_ContactUs.getButtonId(Businessver,CustType);
        country=SelfService_ContactUs.mgetCountryValues();          
        getProdValues=SelfService_ContactUs.getTopicValues(CustType);  
        getProdValues=SelfService_ContactUs.getCategoryValuesFromRequestType(CustType,'Exams');       
        // getProdValues=SelfService_ContactUs.getProductIdValue(Businessver);
        }
       /*
     * Method to cover the method getValues()
     */ 
    static testMethod void test_mGetFeedItem(){
        String communityUserProfileName = 'Pearson Self-Service User';
        List<User> userList = new List<User>();
        List<FeedItem> ofeedItem;
        //List<Customer_Support__kav> knowledgeArtVerToInsertList = new List<Customer_Support__kav>();
        List<TopicAssignment> topicAssignToInsertList = new List<TopicAssignment>();
        List<String> searchTitleStr = new List<String>();
        String countryStr, roleStr;
        searchTitleStr.add('Test Article');
        countryStr = '';
        roleStr = '';
        User usr;
        UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){      
            //user for 'Customer Community Login' license
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
        }
            Test.StartTest();
            ofeedItem = SelfService_ContactUs.mGetFeedItem(searchTitleStr);
            System.assertNotEquals(ofeedItem, null);
            Test.StopTest();
    }
     /*
     * Method to cover the method getCase
     */  

     /*
     * Method to cover the method getProductIdValue
     */  
    static testMethod void test_getProductIdValue(){
        String sProductValue;
        User usr;
        List<Product__c> prodList = new List<Product__c>();
        Product__c prod;
        UserRole tempUserRole;
         //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        insert usr;
        System.runAs(usr){
            Test.StartTest();
            prod = new Product__c(name='NVQ',Active_in_Case__c=true, Type__c='Product', market__c='uk');
            prodList.add(prod);
            insert prodList;
            Test.StopTest();
        }
         System.runAs(usr)
         {
        sProductValue=SelfService_ContactUs.getProductIdValue(String.valueOf(prodList[0].name));
         }
    }
      /*
     * Method to cover the method getCategorySubcategoryValue
     */  
    static testMethod void test_getCategorySubcategoryValue(){
        List<String> sSubCategoryList=new List<String>();
        User usr;
        List<CategorySubcategoryMapping__c> CatgoryList = new List<CategorySubcategoryMapping__c>();
        CategorySubcategoryMapping__c cat;
        UserRole tempUserRole;
         //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        insert usr;
        System.runAs(usr){
            Test.StartTest();
            cat = new CategorySubcategoryMapping__c(Name='2',AACategory__c='Category2',AASubcategory__c='SubCategory2' );
            CatgoryList.add(cat);
            insert CatgoryList;
            Test.StopTest();
        }
         System.runAs(usr)
         {
        SelfService_ContactUs.getCategorySubcategoryValue(CatgoryList[0].name,'');
         }
    }
        /*Method to test outbound notification*/
    static testMethod void testOutNotif(){
        String communityUserProfileName = 'Pearson Self-Service User';
        List<User> listWithUser = new List<User>();
        listWithUser = TestDataFactory.createUser([select Id from Profile where Name =: communityUserProfileName].Id,1);
        String yourEmail =listWithUser[0].email;
        String url='https://www.google.com';
        String articleName='ArticleName';
        String recEmail='h@hotmail.com';
        String firstName=listWithUser[0].name;
        String recName='recName';
        /*List<Outbound_Notification__c> sendEmailList = new List<Outbound_Notification__c>();
        sendEmailList = PS_CSSContactPrint_EmailTest.createOutNot();
        insert sendEmailList;*/
        String str;
        
        Test.StartTest();
        SelfService_ContactUs.articleSendEmail(yourEmail,url,articleName,recEmail,firstName,recName);
        system.assertequals(firstName,listWithUser[0].name);
        
        Test.StopTest();
    }
  static testMethod void test_getCase(){
        String communityUserProfileName = 'Pearson Self-Service User';
        String accountRecType = 'Organisation';
        String caseRecType = 'Customer Service';
        String caseRecTypeAPIName = 'Customer_Service';
        String contactRecType = 'Contact';
         String FileId='';
         String fileName='word';
         String base64Data='';
         String contentType='';
         String Title='';
        List<User> userList = new List<User>();
        List<Account> accountList = new List<Account>();
        List<Case> caseList = new List<Case>();
        List<Product__c> prodList = new List<Product__c>();
        String successMsgStr, detailsStr;
        Product__c prod;
        user caseOwnerUsr = new user();
        User usr;
        UserRole tempUserRole;
        String vCountry='United States';
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'UK', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        //insert usr;
        
        
        System.runAs(usr){
            //create account and contact
        List<Contact> con  = TestDataFactory.createContact(1);
        con[0].Email = 'Test@gmail.com';
        insert con;
            
             prod = new Product__c(name='NVQ',Active_in_Case__c=true, Type__c='Product', market__c='uk');
            prodList.add(prod);
            insert prodList;
            Test.StartTest();
            successMsgStr = SelfService_ContactUs.mGetCaseDetails(FileId,'01p7E000000AMN3QAO',fileName,base64Data,contentType,vCountry,caseRecTypeAPIName,'Teacher','Qualifications','Test_FirstName','Test_LastName','Test@gmail.com','1234567890','12345','NVQ','Accounting','TEstTopic','Test_Subject','abcd','AB1234','Category','TEst_subcategory','12345');
              //System.assert(successMsgStr.contains('Your case has been created successfully.'));
                 Test.StopTest();
      }
    }
      
}