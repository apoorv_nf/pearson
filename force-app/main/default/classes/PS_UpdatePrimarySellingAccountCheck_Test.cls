/*******************************************************************************************************************
* Apex Class Name   : PS_UpdatePrimarySellingAccountCheck_Test 
* Created Date      : 11th Sep 2015
* Description       : Test class for UpdatePrimarySellingAccountCheck class
* Last Modified By  : Navaneeth - As part of US-HE : SP - [CRMSFDC-2392]   
*******************************************************************************************************************/

@isTest
public class PS_UpdatePrimarySellingAccountCheck_Test {

    static testMethod void updatePrimarySellingAccount() {
    
        Account sysAcc =new Account();
        sysAcc.Name='Test System Account';
        sysAcc.Phone = '00012544';
        sysAcc.System_Account_Indicator__c = true;
        sysAcc.Primary_Selling_Account_check__c = false;
        insert sysAcc;
        
        
        Account primAcc=new Account();
        primAcc.ParentId= sysAcc.Id;
        primAcc.Name='Test Primary Account';
        primAcc.Phone = '0001254';
        primAcc.Primary_Selling_Account_check__c =true;
        insert primAcc;
        
        Account deptAcc1=new Account();
        deptAcc1.ParentId= primAcc.Id;
        deptAcc1.Name='Test deptAcc1 Account1';
        deptAcc1.Phone = '0001254444';
        deptAcc1.Primary_Selling_Account_check__c =false;
        insert deptAcc1;
        
        Account dept1Child1=new Account();
        dept1Child1.ParentId= deptAcc1.Id;
        dept1Child1.Name='Test dept1Child1 Account1';
        dept1Child1.Phone = '00012522';
        dept1Child1.Primary_Selling_Account_check__c =false;
        insert dept1Child1;
        
        Account dept1Child1Child1=new Account();
        dept1Child1Child1.ParentId= dept1Child1.Id;
        dept1Child1Child1.Name='Test Sample Account1';
        dept1Child1Child1.Phone = '00012522';
        dept1Child1Child1.Primary_Selling_Account_check__c =false;
        insert dept1Child1Child1;
        
        
        Account dept1Child1Child1Child1=new Account();
        dept1Child1Child1Child1.ParentId= dept1Child1Child1.Id;
        dept1Child1Child1Child1.Name='Test Sample Account1';
        dept1Child1Child1Child1.Phone = '00012522';
        dept1Child1Child1Child1.Primary_Selling_Account_check__c =false;
        insert dept1Child1Child1Child1;
        
        
        Account lastChild=new Account();
        lastChild.ParentId= dept1Child1Child1Child1.Id;
        lastChild.Name='Test Sample Account1';
        lastChild.Phone = '00012522';
        lastChild.Primary_Selling_Account_check__c =false;
        //insert lastChild;
        
        List<Account> lstCreateAccount=new List<Account>();
        List<Account> accountList = new List<Account>();
        accountList.add(lastChild);
        
        //List<Account> accountList=TestDataFactory.createAccount(1,'Learner');
        //insert accountList;
        
        
        checkRecurssionAfter.run =true;
        test.startTest();
            List<Account> lstAccount=new List<Account>();
            for(Account acc:accountList){
               // acc.parentId=dept1Child1.Id;
                acc.parentId=dept1Child1Child1Child1.Id;
                acc.Primary_Selling_Account_check__c =false;
                lstAccount.add(acc);
            }
            lstCreateAccount.addAll(lstAccount);
            insert lstCreateAccount;
                      
                      
            List<Account> updateAccList = new List<Account>();
            
            checkRecurssionAfter.run =true;
            primAcc.Primary_Selling_Account_check__c =false;
            primAcc.System_Account_Indicator__c =false; 
            
            sysAcc.Primary_Selling_Account_check__c =false;
            sysAcc.System_Account_Indicator__c =false; 
            
            updateAccList.add(primAcc);
            updateAccList.add(sysAcc);
            
            update updateAccList;
            
            checkRecurssionAfter.run =true;
            List<Account> updateAccListNew = new List<Account>();

            primAcc.Primary_Selling_Account_check__c =true;
            primAcc.System_Account_Indicator__c =false; 
            
            sysAcc.Primary_Selling_Account_check__c =false;
            sysAcc.System_Account_Indicator__c =true; 
            
            updateAccListNew.add(primAcc);
            updateAccListNew.add(sysAcc);
            
            update updateAccListNew;
                      
            
            
        test.stopTest();
    }
    
    /*
    New TestMethod Created as part of US HE SP : For Code Coverage    
    */
    static testMethod void updateSystemAccount() {
    
        Account sysAcc =new Account();
        sysAcc.Name='Test System Account';
        sysAcc.Phone = '00012544';
        sysAcc.System_Account_Indicator__c = true;
        sysAcc.Primary_Selling_Account_check__c = false;
        insert sysAcc;
        
        
        Account primAcc=new Account();
        primAcc.ParentId= sysAcc.Id;
        primAcc.Name='Test Primary Account';
        primAcc.Phone = '0001254';
        primAcc.Primary_Selling_Account_check__c =false;
        primAcc.System_Account_Indicator__c = false;
        insert primAcc;
        
        Account deptAcc1=new Account();
        deptAcc1.ParentId= primAcc.Id;
        deptAcc1.Name='Test deptAcc1 Account1';
        deptAcc1.Phone = '0001254444';
        deptAcc1.Primary_Selling_Account_check__c =false;
        deptAcc1.System_Account_Indicator__c = false;
        insert deptAcc1;
        
        Account dept1Child1=new Account();
        dept1Child1.ParentId= deptAcc1.Id;
        dept1Child1.Name='Test dept1Child1 Account1';
        dept1Child1.Phone = '00012522';
        dept1Child1.Primary_Selling_Account_check__c =false;
        dept1Child1.System_Account_Indicator__c = false;
        insert dept1Child1;
        
        Account dept1Child1Child1=new Account();
        dept1Child1Child1.ParentId= dept1Child1.Id;
        dept1Child1Child1.Name='Test Sample Account1';
        dept1Child1Child1.Phone = '00012522';
        dept1Child1Child1.Primary_Selling_Account_check__c =false;
        dept1Child1Child1.System_Account_Indicator__c = false;
        insert dept1Child1Child1;
        
        
        Account dept1Child1Child1Child1=new Account();
        dept1Child1Child1Child1.ParentId= dept1Child1Child1.Id;
        dept1Child1Child1Child1.Name='Test Sample Account1';
        dept1Child1Child1Child1.Phone = '00012522';
        dept1Child1Child1Child1.Primary_Selling_Account_check__c =false;
        dept1Child1Child1Child1.System_Account_Indicator__c = false;
        insert dept1Child1Child1Child1;
        
        
        Account lastChild=new Account();
        lastChild.ParentId= dept1Child1Child1Child1.Id;
        lastChild.Name='Test Sample Account1';
        lastChild.Phone = '00012522';
        lastChild.Primary_Selling_Account_check__c =false;
        //insert lastChild;
        
        List<Account> lstCreateAccount=new List<Account>();
        List<Account> accountList = new List<Account>();
        accountList.add(lastChild);
        
        //List<Account> accountList=TestDataFactory.createAccount(1,'Learner');
        //insert accountList;
        
        
        checkRecurssionAfter.run =true;
        test.startTest();
            List<Account> lstAccount=new List<Account>();
            for(Account acc:accountList){
                acc.parentId=dept1Child1.Id;
                acc.Primary_Selling_Account_check__c =false;
                lstAccount.add(acc);
            }
            lstCreateAccount.addAll(lstAccount);
            insert lstCreateAccount;
                      
           
            
            /* Added By Navaneeth for CRMSFDC-2392 */
            
            checkRecurssionAfter.run =true;
            Account sysAccTop =new Account();
            sysAccTop.Name='Test System Account';
            sysAccTop.Phone = '00012544';
            sysAccTop.System_Account_Indicator__c = true;
            sysAccTop.Primary_Selling_Account_check__c = false;
            insert sysAccTop;
            
                        
            checkRecurssionAfter.run =true;
            List<Account> lstAccountnew =new List<Account>();
            sysAcc.ParentId = sysAccTop.Id;
            lstAccountnew.add(sysAcc);
            primAcc.System_Account_Indicator__c = true;
            lstAccountnew.add(primAcc);
            deptAcc1.System_Account_Indicator__c = true;
            lstAccountnew.add(deptAcc1);
            dept1Child1.System_Account_Indicator__c = true;
            lstAccountnew.add(dept1Child1);
            dept1Child1Child1.System_Account_Indicator__c = true;
            lstAccountnew.add(dept1Child1Child1);
            dept1Child1Child1Child1.System_Account_Indicator__c =true;
            lstAccountnew.add(dept1Child1Child1Child1);
            
            Update lstAccountnew;            
                     
            /* Added By Navaneeth for CRMSFDC-2392 */
            
            
        test.stopTest();
    }
    
    
    /*
    New TestMethod Created as part of US HE SP : For Code Coverage    
    */
    static testMethod void updateSystemAccountnew() {
    
        Account sysAcc =new Account();
        sysAcc.Name='Test System Account';
        sysAcc.Phone = '00012544';
        sysAcc.System_Account_Indicator__c = false;
        sysAcc.Primary_Selling_Account_check__c = true;
        insert sysAcc;
        
        
        Account primAcc=new Account();
        primAcc.ParentId= sysAcc.Id;
        primAcc.Name='Test Primary Account';
        primAcc.Phone = '0001254';
        primAcc.Primary_Selling_Account_check__c =false;
        insert primAcc;
        
        Account deptAcc1=new Account();
        deptAcc1.ParentId= primAcc.Id;
        deptAcc1.Name='Test deptAcc1 Account1';
        deptAcc1.Phone = '0001254444';
        deptAcc1.Primary_Selling_Account_check__c =false;
        insert deptAcc1;
        
        Account dept1Child1=new Account();
        dept1Child1.ParentId= deptAcc1.Id;
        dept1Child1.Name='Test dept1Child1 Account1';
        dept1Child1.Phone = '00012522';
        dept1Child1.Primary_Selling_Account_check__c =false;
        insert dept1Child1;
        
        Account dept1Child1Child1=new Account();
        dept1Child1Child1.ParentId= dept1Child1.Id;
        dept1Child1Child1.Name='Test Sample Account1';
        dept1Child1Child1.Phone = '00012522';
        dept1Child1Child1.Primary_Selling_Account_check__c =false;
        insert dept1Child1Child1;
        
        
        Account dept1Child1Child1Child1=new Account();
        dept1Child1Child1Child1.ParentId= dept1Child1Child1.Id;
        dept1Child1Child1Child1.Name='Test Sample Account1';
        dept1Child1Child1Child1.Phone = '00012522';
        dept1Child1Child1Child1.Primary_Selling_Account_check__c =false;
        insert dept1Child1Child1Child1;
        
        
        Account lastChild=new Account();
        lastChild.ParentId= dept1Child1Child1Child1.Id;
        lastChild.Name='Test Sample Account1';
        lastChild.Phone = '00012522';
        lastChild.Primary_Selling_Account_check__c =false;
        // insert lastChild;
        
        List<Account> lstCreateAccount=new List<Account>();
        List<Account> accountList = new List<Account>();
        accountList.add(lastChild);
        
        //List<Account> accountList=TestDataFactory.createAccount(1,'Learner');
        //insert accountList;
        
        
        checkRecurssionAfter.run =true;
        test.startTest();
            List<Account> lstAccount=new List<Account>();
            for(Account acc:accountList){
                acc.parentId=dept1Child1.Id;
                acc.Primary_Selling_Account_check__c =false;
                lstAccount.add(acc);
            }
            lstCreateAccount.addAll(lstAccount);
            insert lstCreateAccount;
                      
           
            
            /* Added By Navaneeth for CRMSFDC-2392 */
            
            checkRecurssionAfter.run =true;
            Account sysAccTop =new Account();
            sysAccTop.Name='Test System Account';
            sysAccTop.Phone = '00012544';
            sysAccTop.System_Account_Indicator__c = false;
            sysAccTop.Primary_Selling_Account_check__c = true;
            insert sysAccTop;
            
                        
            checkRecurssionAfter.run =true;
            List<Account> lstAccountnew =new List<Account>();
            
            primAcc.Primary_Selling_Account_check__c = true;
            lstAccountnew.add(primAcc);
            deptAcc1.Primary_Selling_Account_check__c = true;
            lstAccountnew.add(deptAcc1);
            dept1Child1.Primary_Selling_Account_check__c = true;
            lstAccountnew.add(dept1Child1);
            dept1Child1Child1.Primary_Selling_Account_check__c = true;
            lstAccountnew.add(dept1Child1Child1);
            dept1Child1Child1Child1.Primary_Selling_Account_check__c =true;
            lstAccountnew.add(dept1Child1Child1Child1);
                        
            Update lstAccountnew;            
                                
            
        test.stopTest();
    }
}