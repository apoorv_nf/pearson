/*******************************************************************************************************************
* Apex Class Name  : CaseUKAssignmentRuleActivation 
* Version          : 1.0 
* Created Date     : 12 July 2017
* Function         : Fire Case assignment rule for UK market
* Modification Log : 
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
* Payal Popat              12/07/2016              Initial documentation of the class. 
  Tamizharasan 			   19/07/2019			   Added a condition to check the status of the case		
* --------------------------------------------------------------------------------------------------------------------*/
public without sharing class CaseUKAssignmentRuleActivation{
    /**
    * Description : Method to check Service Case and activate assignment rule for all UK cases
    * @param List<Case> newCases
    * @return NA
    * @throws NA
    **/
    public static AssignmentRule oAR;
    public static Database.DMLOptions dmlOpts;

    @InvocableMethod
    public static void mActivateUKAssignmentRule(List<Case> newcases){
         String sAssignRuleName= system.Label.UKCaseAssignmentRule;
         //Query Assignment Rule
         list<AssignmentRule> oAssignmentRuleList = [select id from AssignmentRule where SobjectType = 'Case' and 
                                              Name =: sAssignRuleName limit 1]; 
         if(!oAssignmentRuleList.isEmpty()){
          if(oAssignmentRuleList[0] != null){
              oAR= new AssignmentRule();
              oAR= oAssignmentRuleList[0];              
              
              dmlOpts = new Database.DMLOptions();
              dmlOpts.assignmentRuleHeader.assignmentRuleId= oAR.Id;
              
          }

        }  
        
        List<Case> oCaseLst = new List<Case>();  
        if(dmlOpts != null){
            for(Case c: newcases){                    
                    c.setOptions(dmlOpts);
                    oCaseLst.add(c);                    
                    
                }
          }
            try{
                if(oCaseLst.size()>0){
                    List<Database.SaveResult> result = DataBase.update(oCaseLst,false);
                    for(Database.SaveResult sr : result){
                        if(sr.isSuccess()){
                                                        
                        }else{
                            // Create two Cases, one of which is missing a required field
                            for(Database.Error err : sr.getErrors()) {
                               
                            }
                         }
                    }
                }
             }    
            catch(exception e){
                throw e;
            }      
        
    } 
    /**
    * Description : Method to check Escalated Service Case and activate assignment rule for all UK cases
    * @param List<Case> newCases, Map<Id,Case> oldMap
    * @return NA
    * @throws NA
    **/
    public static void mAssignEscalationCase(List<Case> newcases,Map<Id,Case> oldMap){
        List<Id> lstCaseId = new List<Id>();//To capture cases other than created through emails.
        for(Case oCase: newcases){
            if(oCase.Market__c == System.Label.PS_UKMarket){
                 if(oldMap != null){
                     if((oCase.Escalation_Point__c != oldMap.get(oCase.Id).Escalation_Point__c && !string.IsEmpty(oCase.Escalation_Point__c))  || oCase.Request_Type__c != oldMap.get(oCase.Id).Request_Type__c){
                         lstCaseId.add(oCase.Id);
                     }
                 }
                 else{
                    // if(!string.IsEmpty(oCase.Escalation_Point__c))
                         lstCaseId.add(oCase.Id);
                 }
            }
         }
         // Below code added for INC4960341 -- Owner change issue 
        List<Case> oCaselst = [Select Id,Account.Market2__c,status from Case where Id In: lstCaseId and (RecordType.Name =: System.Label.CaseHeTechSupportRecordType or RecordType.Name =: System.Label.PS_Customer_Service_Record_Type)];
        List<case> cas = new list<case>();
        for(Case c: oCaselst){                   
                     if(c.status == 'New'){
                         cas.add(c);
                     }  
                 }
        try{
             if(cas.size()>0){                
                 mActivateUKAssignmentRule(cas);
                 }   
             }
         catch(exception e){
            throw e;
        }
    }
    
 }