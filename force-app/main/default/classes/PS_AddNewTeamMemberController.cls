/**
  * VF Page    : Account Team Management
  * Controller : PS_AddNewmemberController
  * Description: Controller class to Add, Modify Permissions and Remove Account Team Members 
  **/  
 
 
/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_AddNewmemberController.Cls
   Description:      
   Date             Version           Author           Tag                                Summary of Changes 
   -----------      ----------   -----------------   --------     ---------------------------------------------------------------------------------------
   07/06/2015         0.1          Leonard Victor                     Updated canAddTeamMember() method , removed AccountShare SOQL and added 
                                |                                     SOQL for UserRcordAccess.
   
   21/08/2015         0.2         Rahul Boinepally     T1             Removed the hard coded error messages/headers and replaced with Custom labels.
   
   19/10/2015         0.3         Kamal Chandran                      Added redirectToUpdateChildPage() method   
   
   27/10/2015         0.4         Kamal Chandran                      Modified 'updatepermission' Method to avoid 'list has no rows' error       
   
   29/10/2015         0.5         Gousia Begum                        Added the code to show An Error message when attempting to add a User to the Team who is already there.as per D-2192
   
   24/11/2015         0.6         Karan Khanna                        Logic for adding Id of User to set 'accTeamUserids' when User is added and removing Id of User from same Set when it's deleted
   
   27/11/2015         0.7         Ron Matthews                        Dont show errors when saving and closing when none of the fields are provided
   
   21/01/2016         0.8         Mulakdeep Singh                     Added spaces to error messages for adding/removing team members (D-3431)
   15/09/2016         0.9          kyama                               Added The Logic to STop salesUser from Adding Account team member.
---------------------------------------------------------------------------------------------------------------------------------------------------------- */




public without sharing class PS_AddNewTeamMemberController { 

    //Declaration of Common Variables
    public Id acctid;
    private AccountTeamMember accTeamMember;
    public List<AccountTeamMemberSelected> AccountTeamMemberList {get; set;}     
    public integer accountteammembersadded {get;set;}
    public integer accountteammembersremoved{get;set;}
    public string selectedteammember{get;set;}
    public string accountaccess{get;set;}    
    public string contactaccess{get;set;}
    public string opportunityaccess{get;set;} 
    public string caseaccess{get;set;}
    public Account primarySellingAccountCheck{get;set;}
    public Set<Id> accTeamUserids = new Set<Id>();
    public User loggedUserObj; 
    public List<UserRestrictionToAddAccountTeam__mdt> userRestriction;
    public boolean displayForm{get;set;}
    
  //Constructor    
    public PS_AddNewTeamMemberController(ApexPages.Standardcontroller controller) {
        loggedUserObj= new User();
        userRestriction= new List<UserRestrictionToAddAccountTeam__mdt>();
        displayForm=true;
        this.accTeamMember = (AccountTeamMember)controller.getRecord();      
        acctid = ApexPages.currentPage().getParameters().get('AccountID');
        accountteammembersadded=0;
        accountteammembersremoved=0;
        //Added for RD-01266
        primarySellingAccountCheck = new Account();
        if(acctid != null)
        {
            primarySellingAccountCheck = [select id,Primary_Selling_Account_check__c ,(select UserId from AccountTeamMembers ) from Account where id =: acctid];
            for( AccountTeamMember accTeam :primarySellingAccountCheck.AccountTeamMembers ){
                accTeamUserids.add(accTeam.UserId);
            }
        }    
        //Kyama:02/09/2016:R6-Created to Restrict the sales User Profile from Adding Account Team Member.
        loggedUserObj = [Select id, market__c, Business_Unit__c,Profile.name from User where id = : Userinfo.getUserid()];
        if(loggedUserObj!=null){
        userRestriction = [Select Market__c,Profile__c,Business_Unit__c,RestrictAddMember__c from UserRestrictionToAddAccountTeam__mdt WHERE Market__c=:loggedUserObj.market__c and Business_Unit__c=:loggedUserObj.Business_Unit__c and Profile__c=:loggedUserObj.Profile.name limit 1];
        }
            if(!userRestriction.isEmpty() ||  test.IsRunningTest()){
            if(userRestriction[0].RestrictAddMember__c==1)
            {
                ApexPages.Message msg = new ApexPages.message(ApexPages.severity.Warning,Label.PS_MngAccntTeam_AccessAdd); 
                ApexPages.addMessage(msg);
                displayForm=false;
            }
           }
        
    }
    
        //Used to insert Account Team Members
    public void addTeamMemberAndAccountShare(){
        try{ 
         //Adding AccountTeamMember
       
        if(accTeamMember.UserId == null || accTeamMember.TeamMemberRole== null )//to ensure that both user and role is selected 
        {
           ApexPages.Message msg = new ApexPages.message(ApexPages.severity.Error,Label.PS_MngAccntTeam_SelectBoth); 
           ApexPages.addMessage(msg);
        }
        else if(accTeamUserids.contains(accTeamMember.userid))
        {
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.PS_MngAccntTeam_DuplicateUser));
          
        }
        else
        {
                                //inserting Account Team
        AccountTeamMember addTeamMember= new AccountTeamMember();
        addTeamMember.AccountId = acctid;
        addTeamMember.UserId = accTeamMember.UserId;
        addTeamMember.TeamMemberRole = accTeamMember.TeamMemberRole;
        insert addTeamMember;
        accTeamUserids.add(accTeamMember.userid);
        accountteammembersadded++;
        
        //Retrieving and updating Access Level Permissions
        AccountShare addAccountShare = [select Accountid,AccountAccessLevel,OpportunityAccessLevel,CaseAccessLevel,ContactAccessLevel from AccountShare where Accountid =: acctid and UserOrGroupId =: accTeamMember.UserId and RowCause=:'Team'];
        addAccountShare.AccountAccessLevel = 'Edit';
        addAccountShare.OpportunityAccessLevel = 'Edit';
        addAccountShare.CaseAccessLevel = 'Edit';
        addAccountShare.ContactAccessLevel = 'Edit';               
        update addAccountShare;
        String username=[select name from user where id=:accTeamMember.UserId].name;
        String summary = username + ' ' + label.PS_MngAccntTeam_Added; //T1
        ApexPages.Message msg = new ApexPages.message(ApexPages.severity.info,summary);
        ApexPages.addMessage(msg);


        AccountTeamMemberSelected atm = new AccountTeamMemberSelected(addTeamMember,addAccountShare);
        atm.userid = accTeamMember.UserId;
        atm.username =username;

        AccountTeamMemberList.add(atm);
        
       
                   }}catch(Exception e){
             ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,label.PS_MngAccntTeam_AccessError); //T1
              ApexPages.addMessage(msg);
                   }      
         

    }
        
   //query the account team members related to account
   public List<AccountTeamMemberSelected> getAccountTeamMembers() {
      
      if(AccountTeamMemberList == null ) {
          AccountTeamMemberList = new List<AccountTeamMemberSelected>();

          Map<Id, AccountShare> accountshares= new Map<Id, AccountShare>();

          for(AccountShare s : [select UserOrGroupId,Accountid,AccountAccessLevel,OpportunityAccessLevel,CaseAccessLevel,ContactAccessLevel from AccountShare where accountid =: acctid] )
          {
            accountshares.put(s.UserOrGroupId  ,s);
          }   
          
          for(AccountTeamMember temp: [select Id, User.Id, User.Name , TeamMemberRole,Accountid from AccountTeamMember where accountid=:acctid]) {
                      AccountTeamMemberList.add(new AccountTeamMemberSelected(temp,accountshares.get(temp.User.Id)));
          }
      } 
       
      return AccountTeamMemberList;
  }    
        
  //delete the selected AccountTeamMembers
  public PageReference DeleteSelected() {
        
        Boolean permissionCheck = False;
        permissionCheck = canAddTeamMember();
               
        if(permissionCheck)
        {
        Set<Id> toBeRemovedMembersIdSet = new Set<Id>();
        List<AccountTeamMember> selectedAccountTeamMembers = new List<AccountTeamMember>();
              for(AccountTeamMemberSelected temp: getAccountTeamMembers()) {
                   if(temp.selected == true) {
                        selectedAccountTeamMembers.add(temp.atm);
                        toBeRemovedMembersIdSet.add(temp.userId);
                         String summary = temp.username + ' ' + label.PS_MngAccntTeam_Removed; //T1
                         ApexPages.Message msg = new ApexPages.message(ApexPages.severity.info,summary);
                         ApexPages.addMessage(msg);}

                       }
                   
               
            accountteammembersremoved=accountteammembersremoved+selectedAccountTeamMembers.size();
             if(selectedAccountTeamMembers == null)  
             {
               ApexPages.Message msg = new ApexPages.message(ApexPages.severity.info,label.PS_MngAccntTeam_SelectDelete); //T1
               ApexPages.addMessage(msg);
             }
              else
             { 
                delete selectedAccountTeamMembers;
                accTeamUserids.removeAll(toBeRemovedMembersIdSet);       
             }         
          
                 AccountTeamMemberList=null;
         
             return null;
         } 
     else{
            //User does not have permission to remove account team members.
            ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,label.PS_MngAccntTeam_AccessRemove); //T1
            ApexPages.addMessage(msg);
            return null;        
        }
          
    } 

       
    //used to direct the flow of insertion based on user permission
    public PageReference save() {        
        Boolean permissionCheck = False;
        permissionCheck = canAddTeamMember();
               
        if(permissionCheck)
        {
            addTeamMemberAndAccountShare();
        }
        else{
            //User does not have permission to add an account team member.
            ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,label.PS_MngAccntTeam_AccessAdd); //T1
            ApexPages.addMessage(msg);
            return null;        
        }
        
        return null;        
    }

   //user to check if the user has permission to add Account Team Member
   private Boolean canAddTeamMember()
    {
        Boolean accessLevel = False;
        
        //Get the running user:
        Id creatorId = UserInfo.getUserId();
        
        //Check for the object permission and permission set for the login user 
          //Commented for fixing account team member addition issue
        //List<AccountShare> accAccessLevel = new List<AccountShare>();
        List<ContactShare> ContactAccessLevel = new List<ContactShare>();
        List<OpportunityShare> OpportunityAccessLevel = new List<OpportunityShare>();
        List<caseShare> caseAccessLevel = new List<caseShare>();
        List<ObjectPermissions> AccEditPermission=[SELECT Id, SObjectType, PermissionsEdit, Parent.label, Parent.IsOwnedByProfile FROM ObjectPermissions WHERE (ParentId IN (SELECT PermissionSetId FROM PermissionSetAssignment WHERE Assignee.Id =: creatorId )) AND (PermissionsEdit = true) AND (SobjectType = 'Account')];
       //Commented for fixing account team member addition issue
       // accAccessLevel = [SELECT AccountAccessLevel FROM AccountShare WHERE (AccountId =: acctid and UserOrGroupId =:creatorId )];
        List<UserRecordAccess> userRecordLst = [Select HasEditAccess,recordid from UserRecordAccess where RecordId =:acctid and UserId = :creatorId];
        ID perid = [select id from PermissionSet where label='Pearson Manage Account Team'].id;
        list<PermissionSetAssignment>  perser =[ select Assignee.Id from PermissionSetAssignment where PermissionSetId =:perid and Assignee.Id = :creatorId];
         //Removed Account share list and added UseRecordAccess
           // if(((AccEditPermission.size()>0) || (perser.size()>0))&&(accAccessLevel.size()>0)){
        if(((AccEditPermission.size()>0) || (perser.size()>0))&&(userRecordLst.size()>0 && userRecordLst[0].HasEditAccess)){
            accessLevel = True;
         }
        // returns true if the user has necessary permission or the necessary permission set
                return accessLevel;
    }  
    
   //user to initiate the insertion process
   public PageReference doSaveTeamMember(){
        save();
        return null;
    }
    
  //used to insert the account TeamMember,display summary of insertion and exit 
  public void SaveAndExitmessage()
    {
     // D-2471 - Dont show error messages when closing and none of the fields are provided
     if(accTeamMember.UserId != null && accTeamMember.TeamMemberRole != null )
     {  
       save();
       String summary = accountteammembersadded + label.PS_MngAccntTeamMembesAdded; //T1
       ApexPages.Message msg = new ApexPages.message(ApexPages.severity.info,summary);
       ApexPages.addMessage(msg);
     }
    }
    
   //used to delete the account TeamMember,display summary of deletion and exit  
   public void DeleteAndExitmessage()
    {
     DeleteSelected() ;
     String summary = accountteammembersremoved + label.PS_MngAccntTeam_MembersRemoved; //T1
     ApexPages.Message msg = new ApexPages.message(ApexPages.severity.info,summary);
     ApexPages.addMessage(msg);
    }
    
  //used to populate the team members in a picklist 
  public List<SelectOption> getItems() {
        
        List<SelectOption> options = new List<SelectOption>();

        if(AccountTeamMemberList == null){

          getAccountTeamMembers();

        }

        for(AccountTeamMemberSelected temp: AccountTeamMemberList)    
        {
          options.add(new selectoption(temp.userid,temp.username));
        }
        
        return options;
  }
         
  //used to populate Permissions in a picklist 
  public List<SelectOption> getpermissions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new selectoption('Read', 'Read'));
        options.add(new selectoption('Edit', 'Read/Write'));
        return options;
            }

//used for the updation of object permissions 
  public void updatepermission()
  {
                   
      Boolean permissionCheck = False;
      
      permissionCheck = canAddTeamMember();
      List<AccountShare> lstWithAddAccountShare = new List<AccountShare>();
               
      if(permissionCheck)
      {
          
           if(selectedteammember != null && acctid != null)   
           {
                lstWithAddAccountShare = [select Accountid,AccountAccessLevel,OpportunityAccessLevel,CaseAccessLevel,ContactAccessLevel from AccountShare where accountid =: acctid and UserOrGroupId =:selectedteammember];
           }
            if(lstWithAddAccountShare != null && !lstWithAddAccountShare.isEmpty() && lstWithAddAccountShare.size()>0)
            {
                if(lstWithAddAccountShare[0].AccountAccessLevel !=accountaccess  )
                {
                     lstWithAddAccountShare[0].AccountAccessLevel = string.valueof(accountaccess);
                }

                if(lstWithAddAccountShare[0].ContactAccessLevel !=contactaccess  )
                {
                     lstWithAddAccountShare[0].ContactAccessLevel = string.valueof(contactaccess);
                }
                if(lstWithAddAccountShare[0].OpportunityAccessLevel != opportunityaccess )                                            
                {
                     lstWithAddAccountShare[0].OpportunityAccessLevel = string.valueof(opportunityaccess);
                }
                if(lstWithAddAccountShare[0].CaseAccessLevel != caseaccess )                                             
                {
                     lstWithAddAccountShare[0].CaseAccessLevel = string.valueof(caseaccess);
                }

                update lstWithAddAccountShare;
            }else
            {
                if(selectedteammember != null && acctid != null)
                {
                    lstWithAddAccountShare.clear();
                    AccountShare newAccShare = new AccountShare();
                    newAccShare.AccountId = acctid;
                    newAccShare.UserOrGroupId = selectedteammember;
                    newAccShare.AccountAccessLevel = string.valueof(accountaccess);
                    newAccShare.ContactAccessLevel = string.valueof(contactaccess);
                    newAccShare.OpportunityAccessLevel = string.valueof(opportunityaccess);
                    newAccShare.CaseAccessLevel = string.valueof(caseaccess);
                    lstWithAddAccountShare.add(newAccShare);
                    if(lstWithAddAccountShare.size() > 0)
                    {
                        insert lstWithAddAccountShare;
                    }
                }
            }
            
            if(lstWithAddAccountShare != null && lstWithAddAccountShare.size() > 0 && !lstWithAddAccountShare.isEmpty())
            {
                for(AccountTeamMemberSelected tempactm: getAccountTeamMembers())
                {
                    if(tempactm.userid == selectedteammember) 
                    {
                        tempactm.accpermission=lstWithAddAccountShare[0];
                    } 
                }               
            }                                                                    
                        
                
            
            ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.info,label.PS_MngAccntTeam_PermUpdated); //T1
            ApexPages.addMessage(msg);    
             
        }
        else{
            //User does not have permission modify permissions.
            ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,label.PS_MngAccntTeam_AccessModify); //T1
            ApexPages.addMessage(msg);
               
        }
              
  }                         
    
    //Added by kamal for RD-01266 - 19/10/2015
    public pageReference redirectToUpdateChildPage()
    {
        pageReference redirectToChildPage = new pageReference('/apex/PS_UpdateAccountChildTeams?accountId='+acctid);
        redirectToChildPage.setRedirect(true);
        return redirectToChildPage;
    }
                    
   //wrapper class used.It contains account teammember,boolean for selection,string for username and accountshare to store permission levels 
   public class AccountTeamMemberSelected {

     public AccountTeamMember atm {get; set;}
     public Boolean selected {get;set;}
     public string username{get; set;} 
     public Id userid {get; set;}
     public AccountShare accpermission{get;set;}   
     //This is the contructor method. 
     public AccountTeamMemberSelected(AccountTeamMember c) {
     atm=c;
     selected = false;     
     username=[select name from user where id=:c.userid].name;
     //accpermission=accper;     
     }

     public AccountTeamMemberSelected(AccountTeamMember c, AccountShare s ) {
         atm=c;
         selected = false;     
         username= c.User.Name;
         userId =c.User.Id;
         accpermission = s;
      }
  }
  
}