@isTest
private class TestEinsteinBotCategorySetter
{
	@isTest
	static void it_should_set_the_category_subcategory_and_message() {
		List<EinsteinBotCategorySetter.CaseCreationResponse> responses = EinsteinBotCategorySetter.setCategory(new List<String>{'Redeemed_Expired'});

		System.assert(!responses.isEmpty());

		System.assertEquals('Access Code', responses[0].category);
		System.assertEquals('Error - Expired', responses[0].subcategory);
		System.assertEquals('Code was not manually expired by Pearson - Extension recommend', responses[0].message);
	}
}