/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 15/07/2018
 */
@isTest
private class TestEinsteinBotChatLogger {

    @isTest
    static void it_should_log_chats() {
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];
        String chatKey = 'abc';

		Contact contact = new Contact(
			FirstName = 'Rick',
			LastName = 'Sanchez',
			Email = 'rick.sanchez2@email.com'
		);
		insert contact;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

		LiveChatTranscript transcript = new LiveChatTranscript(
			ContactId = contact.Id,
			LiveChatVisitorId = vistor.Id,
			ChatKey = chatKey);
		insert transcript;

        EinsteinBotChatLogger.ChatLoggerRequest request = new EinsteinBotChatLogger.ChatLoggerRequest();
        request.liveAgentSessionId = chatKey;
        request.currentUtterance = 'Testing';
        request.currentThresholdHigh = 0.5875;
        request.currentConfidenceForUtterance = 0.2981;
        request.currentDialogId = '12345';
        request.currentDialogName = 'Test12345';

        Test.startTest();
        System.runAs(automatedProcess) {
            EinsteinBotChatLogger.logChat(new List<EinsteinBotChatLogger.ChatLoggerRequest>{request});
            Test.getEventBus().deliver();
        }
        Test.stopTest();

        System.assertEquals(1, [SELECT Count() FROM Einstein_Bot_Chat_Log__c]);
    }
}