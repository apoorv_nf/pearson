/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_CloseParentCaseWithChildCases_Test.cls 
Description:     Test Class for PS_CloseParentCaseWithChildCases. 
CR Info:         CR-00275
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
26/07/2017         1.0            MAYANK LAL                       Initial Design 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */

@isTest
public class PS_CloseParentCaseWithChildCases_Test {
    
    static testmethod void testCloseParentwithChilds() {
    Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);        
        insert lstWithTestUser;
        System.runAs(lstWithTestUser[0]) 
            {
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c    =true);
        insert byp; 
        User usr ;
        Account acc = new Account();
        acc.RecordTypeId = '012b0000000DpIM'; 
        acc.Name = 'Test Account';
        acc.Geography__c = 'Growth';
        acc.Line_of_Business__c = 'Higher Ed';
        acc.Market2__c = 'US';
        
        insert acc;
        System.debug('@@acc '+acc.Name);
        
        List<Contact> lstCon = TestDataFactory.createContacts(1);
        lstCon[0].AccountId = acc.Id;
        insert lstCon;
        System.debug('@@lstCon '+lstCon);
        
        Product__c  pro = new Product__c ();
        pro.name = 'Test Product';
        //pro.RecordTypeId = '012b0000000DcVl';
        pro.CurrencyIsoCode = 'GBP';
        insert pro;
        System.debug('@@pro '+pro.Name);
        //List<Product2> lstProduct = TestDataFactory.createProduct(1);
        //insert lstProduct;
        
        //Creating Parent Case
        Case cs = new Case();
        Map<id,Case> parentmap = new Map<id,Case>();
        Map<id,Case> Oldparentmap = new Map<id,Case>();
        cs.Contact_Type__c = 'College Educator';
        cs.AccountId = acc.Id;
        //   for(Contact ct : lstCon) {
        cs.ContactId = lstCon[0].id;
        //   }
        cs.Status = 'New';
        cs.Origin = 'Email';
        cs.Priority = 'Medium';
        cs.Customer_Username__c = 'test';
        cs.Access_Code__c = 'test';
        cs.Error_Message_Code__c = 'test';
        cs.URL__c = 'www.test.com';
        cs.PS_Business_Vertical__c = 'Clinical';
        cs.Request_Type__c = 'Technical Support';
        cs.Platform__c = 'Q-interactive';
        cs.Products__c = pro.id;    
        cs.Category__c = 'Sign in/ Password';
        cs.Subcategory__c = 'Wrong site';
        cs.Subject = 'test';
        cs.Description = 'test';
        cs.Case_Resolution_Category__c = '';
        cs.Case_Resolution_Sub_Category__c = '';
        cs.Reopen_Reason__c = 'Test Reason';
        //Test.startTest();
        insert cs;
        Oldparentmap.put(cs.id,cs);
        
        cs.Status = 'Closed';
        cs.Case_Resolution_Category__c = 'Replaced Access Code';
        cs.Case_Resolution_Sub_Category__c = 'Access Code Unreadable';
        cs.Reopen_Reason__c = 'Test Reason';
        update cs;
        
        System.debug('@@cs Reopen_Reason__c '+cs.Reopen_Reason__c);
        
        parentmap.put(cs.Id, cs);
        
        //Creating Child Cases
        List<Case> lstCase = new List<Case>();
        for(Integer i=0;i<3;i++) {
            Case childcase = new Case();
            childcase.ParentId = cs.Id;
            childcase.AccountId = acc.Id;
            childcase.ContactId = lstCon[0].id;
            childcase.Contact_Type__c = 'College Educator';
            childcase.Status = 'New';
            childcase.Origin = 'Email';
            childcase.Priority = 'Medium';
            childcase.Customer_Username__c = 'test';
            childcase.Access_Code__c = 'test';
            childcase.Error_Message_Code__c = 'test';
            childcase.URL__c = 'www.test.com';
            childcase.PS_Business_Vertical__c = 'Clinical';
            childcase.Request_Type__c = 'Technical Support';
            childcase.Platform__c = 'Q-interactive';
            //for(Product2 pro :lstProduct ) {
            childcase.Products__c = pro.id;    
            //  }
            childcase.Category__c = 'Sign in/ Password';
            childcase.Subcategory__c = 'Wrong site';
            childcase.Subject = 'test';
            childcase.Description = 'test';
            lstCase.add(childcase);
        }
            Test.StartTest(); 
        Database.insert(lstCase, false);
        
        PS_CloseParentCaseWithChildCases psBatchCloseParentandChilds = new PS_CloseParentCaseWithChildCases(parentmap,Oldparentmap);
        
        Database.executeBatch(psBatchCloseParentandChilds);
        Test.stopTest();
    }}
    
    /*static testmethod void testParentwithNegativeChilds() {
        User usr;
        
        //insert usr;
        Map<id,Case> parentmap = new Map<id,Case>();
            List<Case> lstCase = new List<Case>();
        //UserRole tempUserRole;
        // tempUserRole = new UserRole(Name='NA HE TS');
        // insert tempUserRole;
        
        //System.runAs(usr){
            Account acc = new Account();
            acc.RecordTypeId = '012b0000000DpIM'; 
            acc.Name = 'Test Account';
            acc.Geography__c = 'Growth';
            acc.Line_of_Business__c = 'Higher Ed';
            acc.Market2__c = 'Test market';
            insert acc;
            
            System.debug('@@acc '+acc.Name);
            
            List<Contact> lstCon = TestDataFactory.createContacts(1);
            insert lstCon;
            
            System.debug('@@lstCon '+lstCon);
            
            Product__c  pro = new Product__c ();
            pro.name = 'Test Product';
            //pro.RecordTypeId = '012b0000000DcVl';
            pro.CurrencyIsoCode = 'GBP';
            insert pro;
            System.debug('@@pro '+pro.Name);
            
            //Creating Parent Case
            Case cs = new Case();
            
            cs.Contact_Type__c = 'College Educator';
            cs.AccountId = acc.Id;
            //   for(Contact ct : lstCon) {
            cs.ContactId = lstCon[0].id;
            //   }
            cs.Status = 'New';
            cs.Origin = 'Email';
            cs.Priority = 'Medium';
            cs.Customer_Username__c = 'test';
            cs.Access_Code__c = 'test';
            cs.Error_Message_Code__c = 'test';
            cs.URL__c = 'www.test.com';
            cs.PS_Business_Vertical__c = 'Clinical';
            cs.Request_Type__c = 'Technical Support';
            cs.Platform__c = 'Q-interactive';
            cs.Products__c = pro.id;    
            cs.Category__c = 'Sign in/ Password';
            cs.Subcategory__c = 'Wrong site';
            cs.Subject = 'test';
            cs.Description = 'test';
            cs.Case_Resolution_Category__c = 'test';
            cs.Case_Resolution_Sub_Category__c = 'test';
            cs.Reopen_Reason__c = 'Test Reason';
            cs.recordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
            Test.startTest();
            insert cs;
            cs.Status = 'Closed';
            cs.Case_Resolution_Category__c = 'Replaced Access Code';
            cs.Case_Resolution_Sub_Category__c = 'Access Code Unreadable';
            cs.Reopen_Reason__c = 'Test Reason';
            update cs;
            
            parentmap.put(cs.Id, cs);
            
            Account accChild = new Account();
            accChild.RecordTypeId = '012b0000000DpIM'; 
            accChild.Name = 'Test Account';
            accChild.Geography__c = 'Growth';
            accChild.Line_of_Business__c = 'Higher Ed';
            accChild.Market2__c = 'Test market';
            insert accChild;  
            
            List<Contact> lstConChild = TestDataFactory.createContacts(1);
            lstConChild[0].firstname = 'Test';
            lstConChild[0].lastname = 'Account';    
            insert lstConChild;
            usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science');
            System.runAs(usr){
            Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Validation_Rules__c=true,Disable_Triggers__c=true);
            insert byp;
            
            //Creating Child Cases
            System.debug('@@cs id:'+cs.Id);
            for(Integer i=0;i<3;i++) {
                Case childcase = new Case();
                childcase.ParentId = cs.Id;
                childcase.AccountId = accChild.Id;
                childcase.ContactId = lstConChild[0].id;
                childcase.Contact_Type__c = 'College Educator';
                childcase.Status = 'New';
                childcase.Origin = 'Email';
                childcase.Priority = 'Medium';
                childcase.Customer_Username__c = 'test';
                childcase.Access_Code__c = 'test';
                childcase.Error_Message_Code__c = 'test';
                childcase.URL__c = 'www.test.com';
                childcase.Escalation_Point__c = 'Command Center OCC';
                childcase.Escalation_Reason__c = 'ESc Reasons';
                childcase.PS_Business_Vertical__c = 'Higher Education';
                childcase.Request_Type__c = 'Technical Support';
                childcase.Platform__c = 'Q-interactive';
                //for(Product2 pro :lstProduct ) {
                childcase.Products__c = pro.id; 
                cs.recordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Service').getRecordTypeId();
                //  }
                childcase.Category__c = 'Sign in/ Password';
                childcase.Subcategory__c = 'Wrong site';
                childcase.Subject = 'test';
                childcase.Description = 'test';
                lstCase.add(childcase);
            }
            Database.insert(lstCase, false);
            System.debug('@@lstCase '+lstCase.size());
              
            List<Case> updateCaseOwner = new List<case>();    
            for(Case csOwn : lstCase) {
                System.debug('@@csOwn '+csOwn);
                
            }
            //update updateCaseOwner;    
        System.debug('@@parentmap '+parentmap);
        System.debug('@@updateCaseOwner '+updateCaseOwner);
        
        PS_CloseParentCaseWithChildCases psBatchCloseParentandChilds = new PS_CloseParentCaseWithChildCases(parentmap);
        
        Database.executeBatch(psBatchCloseParentandChilds);
        Test.stopTest();
        } 
    } */
    
        static testmethod void testCloseParentwithChildsV2() {
        User usr;
        UserRole tempUserRole;
        tempUserRole = new UserRole(Name='NA HE TS Agent');
        insert tempUserRole;
        
        //user to avoid Mixed DML error and User validation errors
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com',
                       License_Pools__c= 'Test User', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
       // UserRoleId=tempUserRole.Id,License_Pools__c='Test user');
        insert usr;
            System.runAs(usr){
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c =true,Disable_Process_Builder__c =true,Disable_Validation_Rules__c =true);
        insert byp;
                Account acc = new Account();
        acc.RecordTypeId = '012b0000000DpIM'; 
        acc.Name = 'Test Account';
        acc.Geography__c = 'Growth';
        acc.Line_of_Business__c = 'Higher Ed';
        acc.Market2__c = 'US';
        
        insert acc;
        System.debug('@@acc '+acc.Name);
        
        List<Contact> lstCon = TestDataFactory.createContacts(1);
        lstCon[0].firstname = 'Test';
        lstCon[0].lastname = 'Account';
        lstCon[0].AccountId = acc.Id;
        insert lstCon;
        System.debug('@@lstCon '+lstCon);
        
        Product__c  pro = new Product__c ();
        pro.name = 'Test Product';
        pro.CurrencyIsoCode = 'GBP';
        insert pro;
        System.debug('@@pro '+pro.Name);
        
        
        //Creating Parent Case
        Case cs = new Case();
        Map<id,Case> parentmap = new Map<id,Case>();
        Map<id,Case> Oldparentmap = new Map<id,Case>();
        cs.Contact_Type__c = 'College Educator';
        cs.AccountId = acc.Id;
        cs.ContactId = lstCon[0].id;
        cs.Status = 'New';
        cs.Origin = 'Email';
        cs.Priority = 'Medium';
        cs.Customer_Username__c = 'test';
        cs.Access_Code__c = 'test';
        cs.Error_Message_Code__c = 'test';
        cs.URL__c = 'www.test.com';
        cs.PS_Business_Vertical__c = 'Clinical';
        cs.Request_Type__c = 'Technical Support';
        cs.Platform__c = 'Q-interactive';
        cs.Products__c = pro.id;    
        cs.Category__c = 'Sign in/ Password';
        cs.Subcategory__c = 'Wrong site';
        cs.Subject = 'test';
        cs.Description = 'test';
        cs.Case_Resolution_Category__c = '';
        cs.Case_Resolution_Sub_Category__c = '';
        cs.Reopen_Reason__c = 'Test Reason';
                cs.Error_Message__c = 'HTTP 400';
        Test.startTest();
        insert cs;
        Oldparentmap.put(cs.id,cs);
        
        cs.Status = 'Closed';
        cs.Case_Resolution_Category__c = 'Replaced Access Code';
        cs.Case_Resolution_Sub_Category__c = 'Access Code Unreadable';
        cs.Reopen_Reason__c = 'Test Reason';
        update cs;
        
        System.debug('@@cs Reopen_Reason__c '+cs.Reopen_Reason__c);
        
        parentmap.put(cs.Id, cs);
        
        //Creating Child Cases
        List<Case> lstCase = new List<Case>();
        for(Integer i=0;i<3;i++) {
            Case childcase = new Case();
            childcase.ParentId = cs.Id;
            childcase.AccountId = acc.Id;
            childcase.ContactId = lstCon[0].id;
            childcase.Contact_Type__c = 'College Educator';
            childcase.Status = 'New';
            childcase.Origin = 'Email';
            childcase.Priority = 'Medium';
            childcase.Customer_Username__c = 'test';
            childcase.Access_Code__c = 'test';
            childcase.Error_Message_Code__c = 'test';
            childcase.URL__c = 'www.test.com';
            childcase.PS_Business_Vertical__c = 'Higher Education';
            childcase.Request_Type__c = 'Technical Support';
            childcase.Platform__c = 'Q-interactive';
            //for(Product2 pro :lstProduct ) {
            childcase.Products__c = pro.id;    
            //  }
            childcase.Category__c = 'Access Code';
            childcase.Subcategory__c = 'Product mismatch';
            childcase.Subject = 'test';
            childcase.Description = 'test';
            lstCase.add(childcase);
        }
        Database.insert(lstCase, false);
        PS_CloseParentCaseWithChildCases psBatchCloseParentandChilds = new PS_CloseParentCaseWithChildCases(parentmap,Oldparentmap);
        
        Database.executeBatch(psBatchCloseParentandChilds);
        Test.stopTest();        
            }
        }
}