/******************************************************************************************************************* 
* Apex Class Name  : PS_CreateContractOrder 
* Version          : 1.0 
* Created Date     : 19 Feb 2019
* Function         : Order create when click on submit the contract button on opportunity
* Modification Log :
*
* Developer                   Date                    Description
Shiva                     19/02/2019                Order create

*******************************************************************************************************************/

public class PS_CreateContractOrder {
    public opportunity oppt;
    
    public PS_CreateContractOrder(ApexPages.StandardController controller) {
        oppt = (opportunity)controller.getrecord();
    }
    
    public PageReference Createorderitem(){
        
        //Create new order record
        Order ord = new order();                  
        
        if(oppt.Accountid == null ){
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please add account to this Opportunity'));
            return null;
        }
        
        Account actt = [SELECT Id,shippingstreet,shippingcity,shippingcountry,shippingstate,shippingpostalcode FROM Account where Id =: oppt.Accountid];
        System.Debug('####################3 ' + oppt);
        
        /** if(actt.shippingstreet == null || actt.shippingcity == null || actt.shippingcountry == null || actt.shippingstate == null || actt.shippingpostalcode == null){

ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please update the Shipping Address fields in Account'));
return null;
} **/
        
        ID oppPriceBookId = [SELECT Pricebook2Id FROM Opportunity WHERE id=:oppt.ID].Pricebook2Id;
        Id OrderRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Global Rights Sales Contract').getRecordTypeId(); 
        
        ord.Type = oppt.Type;
        ord.Status = 'New';
        ord.CurrencyIsoCode = oppt.CurrencyIsoCode;
        ord.isTemporary__c = True;
        ord.Accountid= oppt.Accountid;
        ord.ShipToContactid = oppt.Primary_Contact__r.id;
        ord.Third_Party_Account__c = oppt.Third_Party_Account__c;
        ord.Third_Party_Contact__c = oppt.Third_Party_Contact__c;
        ord.Rights_Contact__c = oppt.Rights_Contact__c;        
        ord.OpportunityId = oppt.id;
        ord.EffectiveDate = system.today();
        ord.Ownerid = oppt.Ownerid;
        ord.Salesperson__c = oppt.Ownerid;
        ord.Pricebook2Id = oppPriceBookId;
        ord.RecordTypeId = OrderRecordTypeId;                
        ord.Market__c = oppt.Market__c;
        ord.Business_Unit__c = oppt.Business_Unit__c;
        ord.Line_of_Business__c = oppt.Line_of_Business__c;
        ord.Geography__c = oppt.Geography__c;
        ord.ShippingStreet = actt.shippingstreet;
        ord.ShippingCity = actt.shippingcity;
        ord.ShippingCountry = actt.shippingcountry;
        ord.ShippingState = actt.shippingstate;
        ord.ShippingPostalCode = actt.shippingpostalcode;
        
        insert ord;
        
        System.Debug('####################1 ' + ord);
        
        List<OpportunityLineItem> oliList = [SELECT Id,Product2id,Name,Opportunityid,ISBN__c,Advance_Currency__c,Author__c,Product_Type__c,Quantity,Contract_Term__c,Expiry_Basis__c,
                                             First_Advance_Amount__c, First_Break_Quantity__c,Format__c,Language__c, Print_Run__c,Rights_Channel__c,Royalty_Accounting_Period__c,
                                             Edition__c,Royalty_Method__c, Royalty_Rate_First_Break__c, Royalty_Rate_Second_Break__c,Royalty_Rate_Third_Break__c,Sales_Territory__c,
                                             Second_Advance_Amount__c,Second_Break_Quantity__c,Subright__c,Third_Break_Quantity__c,UnitPrice,Territory_Africa__c,Territory_Asia_Oceania__c,
                                             Territory_Americas__c, Territory_Europe__c, Publish_w_in_Months__c, Outright_Payment_Amount__c, Outright_Payment_Currency__c,Price_Per_Unit__c FROM OpportunityLineItem 
                                             WHERE OpportunityId= :oppt.Id];  
        
        List<PriceBookEntry> priceBookEntryList =new List<PriceBookEntry>();
        PriceBookEntry pbentry=new PriceBookEntry();
        
        list<id> optid = new list<id>();
        for(OpportunityLineItem optyItem :oliList) {
            optid.add(optyItem.Product2id );
        }  
        
        priceBookEntryList = [SELECT Id, Product2Id,CurrencyIsoCode,Pricebook2.Name, Product2.Name FROM PriceBookEntry WHERE CurrencyIsoCode =: userinfo.getDefaultCurrency() 
                              AND Pricebook2Id = :oppPriceBookId AND Product2Id in :optid];
        
        List<OrderItem> ordeitemlist = new List<OrderItem>();  
        
        if(oliList.size()>0){
            System.Debug('####################2 ' + oliList);        
            for(OpportunityLineItem opptlinitm : oliList){
                for(PriceBookEntry pbe : priceBookEntryList){
                    if(pbe.Product2id == opptlinitm.product2id){            
                        OrderItem ordLnItm = new OrderItem();                  
                        ordLnItm.Product2id = opptlinitm.product2id;
                        ordLnItm.Shipped_Product__c = opptlinitm.product2id;
                        ordLnItm.Orderid = ord.id;   
                        ordLnItm.Order_Product_Type__c = opptlinitm.Product_Type__c;
                        ordLnItm.Status__c = '';
                        ordLnItm.UnitPrice = opptlinitm.UnitPrice;
                        ordLnItm.PricebookEntryId = pbe.ID;
                        ordLnItm.Quantity = opptlinitm.Quantity;  
                        ordLnItm.Advance_Currency__c = opptlinitm.Advance_Currency__c;                
                        ordLnItm.Contract_Term__c= opptlinitm.Contract_Term__c;
                        ordLnItm.Expiry_Basis__c= opptlinitm.Expiry_Basis__c;   
                        ordLnItm.First_Advance_Amount__c= opptlinitm.First_Advance_Amount__c;
                        ordLnItm.First_Break_Quantity__c= opptlinitm.First_Break_Quantity__c;
                        ordLnItm.Format__c= opptlinitm.Format__c;   
                        ordLnItm.Language__c= opptlinitm.Language__c;                    
                        ordLnItm.Print_Run__c= opptlinitm.Print_Run__c;
                        ordLnItm.Publish_Months__c= opptlinitm.Publish_w_in_Months__c;   
                        ordLnItm.Rights_Channel__c= opptlinitm.Rights_Channel__c;
                        ordLnItm.Royalty_Accounting_Period__c= opptlinitm.Royalty_Accounting_Period__c;
                        ordLnItm.Royalty_Method__c= opptlinitm.Royalty_Method__c;  
                        ordLnItm.Royalty_Rate_First_Break__c= opptlinitm.Royalty_Rate_First_Break__c;                    
                        ordLnItm.Royalty_Rate_Second_Break__c= opptlinitm.Royalty_Rate_Second_Break__c;
                        ordLnItm.Royalty_Rate_Third_Break__c= opptlinitm.Royalty_Rate_Third_Break__c;   
                        ordLnItm.Sales_Territory__c= opptlinitm.Sales_Territory__c;
                        ordLnItm.Second_Advance_Amount__c= opptlinitm.Second_Advance_Amount__c;
                        ordLnItm.Second_Break_Quantity__c= opptlinitm.Second_Break_Quantity__c;   
                        ordLnItm.Subright__c= opptlinitm.Subright__c;
                        ordLnItm.Third_Break_Quantity__c= opptlinitm.Third_Break_Quantity__c;
                        ordLnItm.Edition__c = opptlinitm.Edition__c;
                        ordLnItm.Territory_Africa__c = opptlinitm.Territory_Africa__c;
                        ordLnItm.Territory_Asia_Oceania__c = opptlinitm.Territory_Asia_Oceania__c;
                        ordLnItm.Territory_Americas__c = opptlinitm.Territory_Americas__c;
                        ordLnItm.Territory_Europe__c = opptlinitm.Territory_Europe__c;
                        ordLnItm.Outright_Payment_Currency__c = opptlinitm.Outright_Payment_Currency__c;
                        ordLnItm.Outright_Payment_Amount__c = opptlinitm.Outright_Payment_Amount__c;
                        ordLnItm.Price_Per_Unit__c = opptlinitm.Price_Per_Unit__c;
                        ordLnItm.Shipping_Method__c = 'Best Method of Shipment';
                        
                        ordeitemlist.add(ordlnitm); 
                    }    
                }     
            }
        }
        System.Debug('#######before orderline insert ' + ordeitemlist.size());
        insert ordeitemlist;  
        System.Debug('#######after orderline insert ' + ordeitemlist.size());
        
        ord.Status = 'Open';
        
        update ord;
        
        PageReference orditemPage = new PageReference('/'+oppt.id);
        orditemPage.setRedirect(false);
        return orditemPage;  
    }    
}