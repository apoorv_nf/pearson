@istest(seealldata =false)
private class CreateOpportunityfromCoursetest{
    static testmethod void constructorTest() {
        //try{
        
              
        Selling_Period__c fallSetting = new Selling_Period__c();
        Selling_Period__c SpringSetting = new Selling_Period__c();      
        //if(system.Date.today().month() == 12 || (system.Date.today().month() >=1 && system.Date.today().month() <= 5)){
            fallSetting.Name = '2018 - Fall';
            fallSetting.Start_Date__c = Date.newInstance (system.Date.today().year()-1, 12, 1);
            fallSetting.End_Date__c =Date.newInstance (system.Date.today().year(), 5, 31);
            //Fall custom setting
         //}else if(system.Date.today().month() >= 6 && system.Date.today().month() <= 11){
             //Sprin custom setting
            SpringSetting.Name = '2018 - Spring';
            SpringSetting.Start_Date__c = Date.newInstance (system.Date.today().year()-1, 6, 1);
            SpringSetting.End_Date__c =Date.newInstance (system.Date.today().year()-1, 11, 30);
         //}
        insert fallSetting;
        insert SpringSetting;
        
        Account acc = new Account(Name = 'AccTest1',Territory_Code_s__c='WEE02',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
        insert acc;
        
        Contact con = new Contact(FirstName='karthik',LastName ='A.S',Phone='9999888898',Email='test1234@gmail.com', AccountId = acc.Id);
        insert con;
        UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true,Fall_Enrollment__c=10);
        insert UnivCourse;
        Test.startTest();
        opportunity ld = new opportunity(Name='Test',Type='Takeaway', StageName='Prequalification', CloseDate=system.today());         
        insert ld;
        
        
        UniversityCourseContact__c uni = new UniversityCourseContact__c(UniversityCourse__c=UnivCourse.id,Contact__c = con.Id);
        insert uni;
        
        
       
        CreateOpportunityfromCourse testnew = new CreateOpportunityfromCourse();
        
        //testnew.createOppty();
        
        
        PageReference createOppty = new pagereference('apex/CreateOpportunityfromCourse');
        
        id courseid=  UnivCourse.id;
        id ldid= ld.id;
        Test.setCurrentPage(createOppty);
        createOppty.getParameters().put('id', courseid);
        createOppty.getParameters().put('command', 'Opportunitycreate');
        
        createOppty.getParameters().put('command', 'CreateOpportunityUniversityCourse');
        
        //createOppty.getParameters().put('command', 'DeleteOpportunity');
        createOppty.getParameters().put('opptyid', ldid);
        ApexPages.StandardController sc = new ApexPages.standardController(UnivCourse);
        CreateOpportunityfromCourse testnew1 = new CreateOpportunityfromCourse();
        
        CreateOpportunityfromCourse testnew2 = new CreateOpportunityfromCourse();      
        CreateOpportunityfromCourse testnew3 = new CreateOpportunityfromCourse();
        product2 produ= new product2();
        produ.name ='testproduct'; 
        produ.Next_Edition__c=null;
        produ.Publisher__c='Pearson';
        List<User> user1 = [Select Id, Market__c, Line_of_Business__c, Business_Unit__c from User where Id = :UserInfo.getUserId()];
        produ.Market__c = user1[0].Market__c;
        produ.Line_of_Business__c = user1[0].Line_of_Business__c;
        produ.Business_Unit__c = user1[0].Business_Unit__c;
        insert produ;
        
        Asset asset = new Asset();
        asset.name = 'TerritoryAsset'+1;
        asset.Product2Id = produ.id;
        asset.AccountId = acc.id;
        asset.Course__c = courseid;
        asset.Primary__c = True;
        asset.Status__c = 'Active';
        insert asset;
        
        testnew1.onLoad_CourseOpptyEdit();
        
        //Apttus_Config2__ClassificationName__c category = new Apttus_Config2__ClassificationName__c(Name='Professional & Career Services',Apttus_Config2__HierarchyLabel__c='Professional & Career Services');
        ProductCategory__c category = new ProductCategory__c(Name='Professional & Career Services',HierarchyLabel__c='Professional & Career Services');
        insert category;
        
        List<ProductCategory__c> listWithCategory = new List<ProductCategory__c>();
        ProductCategory__c newCategory = new ProductCategory__c();
        newCategory.HierarchyLabel__c = 'Professional & Career Services';
        newCategory.Name = 'Professional & Career Services';
        //newCategory.Category_ExternalID__c = category.ID;
        listWithCategory.add(newCategory);
        insert listWithCategory;
        
        //Apttus_Config2__ClassificationHierarchy__c catre= new Apttus_Config2__ClassificationHierarchy__c(Name ='testcategory',Apttus_Config2__HierarchyId__c=category.id,Apttus_Config2__Label__c='testlabel');
        Hierarchy__c catre= new Hierarchy__c(Name ='testcategory',ProductCategory__c=category.id,Label__c='testlabel');
        insert catre;
        List<Opportunity_Pearson_Course_Code__c> opppearson =new List<Opportunity_Pearson_Course_Code__c>();
        Opportunity_Pearson_Course_Code__c opppearsonitem =new Opportunity_Pearson_Course_Code__c();
        
        for(integer i=0;i<1;i++)
        {
            opppearsonitem.Opportunity__c =ld.id;
            opppearsonitem.Pearson_Course_Code_Hierarchy__c=catre.id;
            opppearson.add(opppearsonitem);
        }
        insert opppearson;
        //Apttus_Config2__ClassificationHierarchy__c csa =new Apttus_Config2__ClassificationHierarchy__c(name ='estt',Apttus_Config2__HierarchyId__c=category.id,Apttus_Config2__Label__c='testlabel');
        Hierarchy__c csa= new Hierarchy__c(Name ='estt',ProductCategory__c=category.id,Label__c='testlabel');
        insert csa; 
        List<Pearson_Course_Equivalent__c> perasoncourese = new List<Pearson_Course_Equivalent__c>();
        Pearson_Course_Equivalent__c perasoncoureseitem = new Pearson_Course_Equivalent__c();
        for(integer i=0;i<3;i++)
        {
            perasoncoureseitem = new Pearson_Course_Equivalent__c();
            perasoncoureseitem.Pearson_Course_Code_Hierarchy__c = csa.id;
            perasoncoureseitem.Course__c=UnivCourse.id;
            perasoncourese.add(perasoncoureseitem);
        }
        insert perasoncourese;
        //Pearson_Course_Equivalent__c perasoncouresee = new Pearson_Course_Equivalent__c(Pearson_Course_Code__c = csa.id,Course__c=UnivCourse.id);
        //insert perasoncouresee ;
        
        asset asse = new asset(name ='test asset',AccountId = acc.Id,Contactid = con.id ,Course__c=UnivCourse.id,/*IsCompetitorProduct=true,*/product2=produ);
        insert asse;
       
        
        createOppty.getParameters().put( 'id' , opppearson[0].id);
        testnew1.today_date = system.today();
        
        testnew1.Fall_start_date = system.today()-1;
        testnew1.Fall_end_date = system.today()+1;
        testnew1.Spring_start_date = system.today();
        testnew1.Spring_end_date = system.today();
        
        Selling_Period__c instFall = new Selling_Period__c();
        instFall.Name = '2017 - Fall';
        instFall.End_Date__c = system.today() + 15;
        instFall.Start_Date__c = system.today() - 15;
        insert instFall;
        
        Selling_Period__c instSpring = new Selling_Period__c();
        instSpring.Name = '2017 - Fall';
        instSpring.End_Date__c = system.today() + 15;
        instSpring.Start_Date__c = system.today() - 15;
         insert instSpring;
        
        Selling_Period__c instSpring1 = new Selling_Period__c();
        instSpring1.Name = '2019 - Fall';
        instSpring1.End_Date__c = system.today() + 15;
        instSpring1.Start_Date__c = system.today() - 15;
        insert instSpring1;
        
        Selling_Period__c instSpring2= new Selling_Period__c();
        instSpring2.Name = '2019 - Spring';
        instSpring2.End_Date__c = system.today() + 15;
        instSpring2.Start_Date__c = system.today() - 15;
        insert instSpring2;
        
        // testnew2.createOppty();
        
        
        testnew1.createOppty();
        //testnew2.onLoad_CourseOpptyEdit(); 
        //testnew2.CreateOpportunityUniversityCourse(); 
        //testnew.CreateOpportunityUniversityCourse();
        testnew.DeleteOppty();
        testnew.courseId = UnivCourse.id;
        //testnew.CreatePearsonCourse();
        
        
        Test.stopTest();/*}
catch(Exception e){return ;}*/
    }         
    static testmethod void constructorTest1() {
        try{
            Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode=null,Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
            insert acc;
            
            Contact con = new Contact(FirstName='karthik',LastName ='A.S',Phone='9999888898',Email='test@gmail.com', AccountId = acc.Id);
            insert con;
            UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true);
            insert UnivCourse;
            Test.startTest();
            opportunity ld = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=Date.newInstance(2015,04,15));
            insert ld;
            
            
            
            CreateOpportunityfromCourse testnew = new CreateOpportunityfromCourse();
            testnew.onLoad_CourseOpptyEdit ();
            //testnew.createOppty();
            
            
            PageReference createOppty = new pagereference('apex/CreateOpportunityfromCourse');
            
            id courseid=  UnivCourse.id;
            id ldid= ld.id;
            Test.setCurrentPage(createOppty);
            createOppty.getParameters().put('id', courseid);
            createOppty.getParameters().put('command', 'Opportunitycreate');
            
            
            createOppty.getParameters().put('opptyid', ldid);
            Product2 pf = TestDataFactory.insertRelatedProducts();
            Asset asset = new Asset();
            asset.name = 'TerritoryAsset'+1;
            asset.Product2Id = pf.id;
            asset.AccountId = acc.id;
            asset.Course__c = courseid;
            asset.Primary__c = True;
            asset.Status__c = 'Active';
            insert asset;
            
            //ApexPages.StandardController sc = new ApexPages.standardController(UnivCourse);
            CreateOpportunityfromCourse testnew1 = new CreateOpportunityfromCourse();
            
            CreateOpportunityfromCourse testnew2 = new CreateOpportunityfromCourse();      
            CreateOpportunityfromCourse testnew3 = new CreateOpportunityfromCourse();             
            testnew1.onLoad_CourseOpptyEdit();
            date today_date = system.today();
            date Spring_start_date=system.today()-1;
            date Spring_end_date=system.today()+1; 
            // testnew1.Fall_start_date;
            testnew1.today_date = system.today();
            
            testnew1.Fall_start_date = system.today()-1;
            testnew1.Fall_end_date = system.today()+1;
            testnew1.Spring_start_date = system.today();
            testnew1.Spring_end_date = system.today();
            
            // testnew2.createOppty();
            
            testnew1.createOppty();
            //opportunity ldd = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=Date.newInstance(2015,04,15));
            // update ldd;
            
            
            
            Test.stopTest();}
        catch(exception e){return ;}
    }   
    static testmethod void constructorTest2() {
        try{
            Selling_Period__c setting = new Selling_Period__c();
                
            if(system.Date.today().month() == 12 || (system.Date.today().month() >=1 && system.Date.today().month() <= 5)){
                setting.Name = '2018 - Fall';
                setting.Start_Date__c = Date.newInstance (system.Date.today().year(), 10, 15);
                setting.End_Date__c =Date.newInstance (system.Date.today().year(), 5, 31);
                //Fall custom setting
             }else if(system.Date.today().month() >= 6 && system.Date.today().month() <= 11){
                 //Sprin custom setting
                 // setting.Name = '2018 - Fall';
                setting.Start_Date__c = Date.newInstance (system.Date.today().year(), 10, 15);
                setting.End_Date__c =Date.newInstance (system.Date.today().year(), 10, 15);
             }
            insert setting;
            
            Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
            insert acc;
            
            Contact con = new Contact(FirstName='karthik',LastName ='A.S',Phone='9999888898',Email='test@gmail.com', AccountId = acc.Id);
            insert con;
            UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true);
            insert UnivCourse;
            Test.startTest();
            opportunity ld = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=Date.newInstance(2015,04,15));
            
            insert ld;
            
            UniversityCourseContact__c uni = new UniversityCourseContact__c(UniversityCourse__c=UnivCourse.id,Contact__c = con.Id);
            insert uni;
            
            
            CreateOpportunityfromCourse testnew = new CreateOpportunityfromCourse();
            testnew.onLoad_CourseOpptyEdit ();
            //testnew.createOppty();
            date today_date = system.today();
            date Spring_start_date=system.today()+1;
            date Spring_end_date=system.today()-1; 
            
            PageReference createOppty = new pagereference('apex/CreateOpportunityfromCourse');
            
            id courseid=  UnivCourse.id;
            id ldid= ld.id;
            Test.setCurrentPage(createOppty);
            createOppty.getParameters().put('id', courseid);
            createOppty.getParameters().put('command', 'CreatePearsonCourse');
            
            
            createOppty.getParameters().put('opptyid', ldid);
            //ApexPages.StandardController sc = new ApexPages.standardController(UnivCourse);
            CreateOpportunityfromCourse testnew1 = new CreateOpportunityfromCourse();
            
            CreateOpportunityfromCourse testnew2 = new CreateOpportunityfromCourse();      
            CreateOpportunityfromCourse testnew3 = new CreateOpportunityfromCourse();             
            testnew1.onLoad_CourseOpptyEdit();
            //Apttus_Config2__ClassificationName__c category = new Apttus_Config2__ClassificationName__c(Name='testcategoryy',Apttus_Config2__HierarchyLabel__c='testlabele');
            ProductCategory__c category = new ProductCategory__c(Name='Professional & Career Services',HierarchyLabel__c='Professional & Career Services');
            insert category;
            
            //Apttus_Config2__ClassificationHierarchy__c catre= new Apttus_Config2__ClassificationHierarchy__c(Name ='testcategory',Apttus_Config2__HierarchyId__c=category.id,Apttus_Config2__Label__c='testlabel');
            Hierarchy__c catre= new Hierarchy__c(Name ='testcategory',ProductCategory__c=category.id,Label__c='testlabel');
            insert catre;
            Opportunity_Pearson_Course_Code__c opppearson =new Opportunity_Pearson_Course_Code__c(Opportunity__c =ld.id,Pearson_Course_Code_Hierarchy__c=catre.id);
            insert opppearson;
            //Apttus_Config2__ClassificationHierarchy__c csa =new Apttus_Config2__ClassificationHierarchy__c(name ='estt',Apttus_Config2__HierarchyId__c=category.id,Apttus_Config2__Label__c='testlabel');
            Hierarchy__c csa= new Hierarchy__c(Name ='estt',ProductCategory__c=category.id,Label__c='testlabel');
            insert csa; 
            Pearson_Course_Equivalent__c perasoncourese = new Pearson_Course_Equivalent__c(Pearson_Course_Code_Hierarchy__c= csa.id,Course__c=UnivCourse.id);
            insert perasoncourese ;
            product2 produ= new product2(name ='testproduct',Next_Edition__c=null,Publisher__c='Pearson');//,Relevance__c=Date.today());
            insert produ;
           
            asset asse = new asset(name ='test asset',AccountId = acc.Id,Contactid = con.id ,Course__c=UnivCourse.id,IsCompetitorProduct=true,product2=produ);
            insert asse;
                        
            createOppty.getParameters().put( 'id' ,opppearson.id);
            
            //testnew2.createOppty();
            testnew1.today_date = system.today();
            
            testnew1.Fall_start_date = system.today()-1;
            testnew1.Fall_end_date = system.today()+1;
            testnew1.Spring_start_date = system.today();
            testnew1.Spring_end_date = system.today();
            testnew1.Fall_Season = Selling_Period__c.getValues(string.valueof(system.Date.today().year())+' - Fall');
            testnew1.Spring_Season = Selling_Period__c.getValues(string.valueof(system.Date.today().year())+' - Spring');
            
            // testnew2.createOppty();
            
            testnew1.createOppty();
            
            Test.stopTest();}
        catch(exception e){ return ;} 
    }      
    static testmethod void constructorTest3() {
        try{
            
            Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
            insert acc;
            
            Contact con = new Contact(FirstName='karthik',LastName ='A.S',Phone='9999888898',Email='test@gmail.com', AccountId = acc.Id);
            insert con;
            UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true);
            insert UnivCourse;
            Test.startTest();
            opportunity ld = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=Date.newInstance(2015,04,15),Fall__c=10,Spring__c=10, Winter__c=10, Summer__c=10);
            
            insert ld;
            UniversityCourseContact__c uni = new UniversityCourseContact__c(UniversityCourse__c=UnivCourse.id,Contact__c = con.Id);
            insert uni;
            
            CreateOpportunityfromCourse testnew = new CreateOpportunityfromCourse();
            testnew.onLoad_CourseOpptyEdit ();
            //testnew.createOppty();
            
            
            
            PageReference createOppty = new pagereference('apex/CreateOpportunityfromCourse');
            
            id courseid=  UnivCourse.id;
            id ldid= ld.id;
            Test.setCurrentPage(createOppty);
            createOppty.getParameters().put('id', courseid);
            createOppty.getParameters().put('command', 'DeleteOpportunity');
            
            
            createOppty.getParameters().put('opptyid', ldid);
            //ApexPages.StandardController sc = new ApexPages.standardController(UnivCourse);
            CreateOpportunityfromCourse testnew1 = new CreateOpportunityfromCourse();
            
            //CreateOpportunityfromCourse testnew2 = new CreateOpportunityfromCourse();      
            //CreateOpportunityfromCourse testnew3 = new CreateOpportunityfromCourse();             
            testnew1.onLoad_CourseOpptyEdit();
            testnew1.today_date = system.today();
            
            testnew1.Fall_start_date = system.today()-1;
            testnew1.Fall_end_date = system.today()+1;
            testnew1.Spring_start_date = system.today();
            testnew1.Spring_end_date = system.today();
            testnew1.createOppty();
            //opportunity ldd = new opportunity(Name='Test',  CloseDate=Date.newInstance(2015,04,15),Fall__c=10,Spring__c=10, Winter__c=10, Summer__c=10);
            
            //delete ldd;
            Test.stopTest();
           }
        catch(exception e){ 
        }
    }
    
    static testmethod void constructorTest7() {
        try{
            
            Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
            insert acc;
            
            Contact con = new Contact(FirstName='karthik',LastName ='A.S',Phone='9999888898',Email='test@gmail.com', AccountId = acc.Id);
            insert con;
            UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true);
            insert UnivCourse;
            Test.startTest();
            opportunity ld = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=Date.newInstance(2015,04,15));
            
            insert ld;
            
            UniversityCourseContact__c uni = new UniversityCourseContact__c(UniversityCourse__c=UnivCourse.id,Contact__c = con.Id);
            insert uni;
            Test.stopTest();  
            
            CreateOpportunityfromCourse testnew = new CreateOpportunityfromCourse();
            testnew.onLoad_CourseOpptyEdit ();
            //testnew.createOppty();
            date today_date = system.today();
            date Spring_start_date=system.today()+1;
            date Spring_end_date=system.today()-1; 
            
            PageReference createOppty = new pagereference('apex/CreateOpportunityfromCourse');
            id courseid=  UnivCourse.id;
            id ldid= ld.id;
            Test.setCurrentPage(createOppty);
            createOppty.getParameters().put('id', courseid);
            createOppty.getParameters().put('command', 'Opportunitycreate');               
            
            createOppty.getParameters().put('opptyid', ldid);
            //ApexPages.StandardController sc = new ApexPages.standardController(UnivCourse);
            CreateOpportunityfromCourse testnew1 = new CreateOpportunityfromCourse();
            testnew1.command = 'Opportunitycreate';
            testnew1.onLoad_CourseOpptyEdit();
            testnew1.command = 'CreateOpportunityUniversityCourse';
            testnew1.onLoad_CourseOpptyEdit();
            testnew1.command = 'DeleteOpportunity';
            testnew1.onLoad_CourseOpptyEdit();
            
        }
        catch(Exception e) {}
    }
    
    static testmethod void constructorTest8() {
        try{
            //Test.startTest();
            Boolean bIsExecuting=False;             
            OpportunityTriggerHandler optyhandler=new OpportunityTriggerHandler(bIsExecuting,10);
            
            Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
            insert acc;
            
            Contact con = new Contact(FirstName='karthik',LastName ='A.S',Phone='9999888898',Email='test@gmail.com', AccountId = acc.Id);
            insert con;
            UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true);
            insert UnivCourse;
            Test.startTest();
            opportunity ld = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=Date.newInstance(2015,04,15));
            ld.RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('D2L').getRecordTypeId();
            ld.Student_Registered__c = true;
            ld.Registration_Payment_Reference__c = 'Testing';
            ld.Received_Signed_Registration_Contract__c = true;
            ld.AccountId = acc.id;
            insert ld;
            
            Contract contrct = new Contract();
            contrct.AccountId = ld.AccountId;
            contrct.Opportunity__c = ld.Id;
            insert contrct; 
            
            UniversityCourseContact__c uni = new UniversityCourseContact__c(UniversityCourse__c=UnivCourse.id,Contact__c = con.Id);
            insert uni;
            Test.stopTest();  
            
            CreateOpportunityfromCourse testnew = new CreateOpportunityfromCourse();
            testnew.onLoad_CourseOpptyEdit ();
            
            date today_date = system.today();
            date Spring_start_date=system.today()+1;
            date Spring_end_date=system.today()-1; 
            
            PageReference createOppty = new pagereference('apex/CreateOpportunityfromCourse');
            id courseid=  UnivCourse.id;
            id ldid= ld.id;
            Test.setCurrentPage(createOppty);
            createOppty.getParameters().put('id', courseid);
            createOppty.getParameters().put('command', 'Opportunitycreate');               
            
            createOppty.getParameters().put('opptyid', ldid);
            //ApexPages.StandardController sc = new ApexPages.standardController(UnivCourse);
            CreateOpportunityfromCourse testnew1 = new CreateOpportunityfromCourse();
            testnew1.command = 'Opportunitycreate';
            testnew1.onLoad_CourseOpptyEdit();
            testnew1.command = 'CreateOpportunityUniversityCourse';
            testnew1.onLoad_CourseOpptyEdit();
            testnew1.command = 'DeleteOpportunity';
            testnew1.onLoad_CourseOpptyEdit();
            
        }
        catch(Exception e) {}
    }
}