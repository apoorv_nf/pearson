/* --------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_SetAccountOnLeadConversion 
Description:     Helper class for 'Account_TriggerSequenceCtrlr' class, which is fired from Account trigger on before insert event. 
Basically,the class sets the field on Account
Date             Version           Author                                            Summary of Changes 
-----------      ----------   -----------------             ---------------------------------------------------------------------------------------
06th Nov 2015      1.0           Accenture NDC                                         Created
9th Nov 2015       1.1       Rahul Boinepally(Accenture)    RD-01591 -- Store the multi valued picklist field 'Business unit' from the business
unit picklist of Lead.  (Tag T1)      
10th Dec 2015      1.0      Leonard Victor                 Fixed business unit defect , updated business==null condition. 
20th May 2016      1.2      Karthik.A.S                    D-5156/INC2550673-Removed state mandatory while converting lead.
19th Aug 2016      1.3      Kyama                          Populate the Group field on Account on Lead Conversion
07th Sep 2017      1.4      Abraham Daniel                 Exclude "US" market from Lead to account conversion fieldset map.
18th May 2018      1.5      Krishna Priya                  When enterprise lead is converted account conversion is mapped to organisation layout 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
public without sharing class PS_SetAccountOnLeadConversion 
{
    public static void setAccountFieldsBeforeInsert(List<Account> lstAccount)
    {
        //get account record type id(s)
        Id CorporateAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.Corporate).getRecordTypeId();
        Id HigherEducationAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Higher Education').getRecordTypeId();
        Id SchoolAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('School').getRecordTypeId();
        Id ProfessionalAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.PS_LeadOrgTypeProfessional).getRecordTypeId(); 
        Id OrganisationAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.PS_OrganisationRecordType).getRecordTypeId(); 
        Id LearnerAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.PS_LearnerRecordType).getRecordTypeId(); 
        String user_id = UserInfo.getUserId();
        List <User> userdetails = [select Market__c , Line_of_Business__c , Geography__c,Region__c from User where id =:user_id limit 1];
        
        for(Account newAccount : lstAccount)
        {
            //T1 - change starts 
            
            if(newAccount.Business_unit_from_Lead__c != null)
            {                                       
                
                if(newAccount.Business_Unit__c == null ||newAccount.Business_Unit__c == '') // Business unit is blank for Account to be converted
                {               
                    newAccount.Business_Unit__c = newAccount.Business_unit_from_Lead__c; 
                    newAccount.Business_unit_from_Lead__c = null;
                }
                
                else // Business_unit__c already exist, so append the values.  
                {                                                                                    
                    if(!newAccount.Business_Unit__c.contains(newAccount.Business_unit_from_Lead__c))
                    {
                        
                        newAccount.Business_Unit__c += ';'+ newAccount.Business_unit_from_Lead__c;                                              
                        newAccount.Business_unit_from_Lead__c = null;
                        
                    }                                               
                }
            }
            // CR-01794:Saritha Singamsetty:10/11/2018: Added code for populating LOB from NAR Lead to Account.
            if(newAccount.Lead_Conversion_Record_Type__c == Label.PS_NewAccountRequestLeadRecordType && newAccount != null)
            {  
                if(newAccount.Lead_Line_Of_Business__c != null && newAccount.Line_of_Business__c== null){
                    newAccount.Line_of_Business__c = newAccount.Lead_Line_Of_Business__c;
                }
                if(newAccount.Lead_Region__c != null && newAccount.Region__c == null){
                    newAccount.Region__c = newAccount.Lead_Region__c;
                }
                if(newAccount.Lead_Parent_Account_ID__c != null && newAccount.Parentid == null){
                    newAccount.Parentid = newAccount.Lead_Parent_Account_ID__c;
                }
                if(newAccount.Lead_Primary_Selling_Account__c != null && newAccount.Primary_Selling_Account__c == null){
                    newAccount.Primary_Selling_Account__c = newAccount.Lead_Primary_Selling_Account__c;
                }
                if(newAccount.Lead_System_Account__c != null && newAccount.System_Account__c == null){
                    newAccount.System_Account__c = newAccount.Lead_System_Account__c;
                }
                if(newAccount.Lead_Primary_Account_Y_N__c != null && newAccount.Lead_Primary_Account_Y_N__c == True && newAccount.Primary_Selling_Account_check__c == null && newAccount.TEP_Level__c == null){
                    newAccount.Primary_Selling_Account_check__c = newAccount.Lead_Primary_Account_Y_N__c;                   
                    newAccount.TEP_Level__c = 'PARTY';
                }
                if(newAccount.Lead_System_Account_Indicator__c != null && newAccount.Lead_System_Account_Indicator__c == True && newAccount.System_Account_Indicator__c == null && newAccount.TEP_Level__c == null ){
                    newAccount.System_Account_Indicator__c = newAccount.Lead_System_Account_Indicator__c;
                    newAccount.TEP_Level__c = 'PARTY';
                }
                if(newAccount.Lead_Account_Record_Type__c != null && trigger.old == null){
                    Id NARAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(newAccount.Lead_Account_Record_Type__c).getRecordTypeId();
                   // if(newAccount.RecordTypeId != null){
                   //     newAccount.RecordTypeId = NARAccountRecordTypeId;
                        accountMappingUtility(newAccount,NARAccountRecordTypeId);   
                   // }
                    
                }
                //newAccount.Ownerid = Label.Pearson_User_ID;
                
            }
            //KR-08/19/2016-R6:Added Below Code to populate the Group field on Account on Lead Conversion.
            if(newAccount.Group_from_Lead__c != null)
            {                                       
                
                if(newAccount.Group__c == null ||newAccount.Group__c == '') //  Group__c is blank for Account to be converted
                {               
                    newAccount.Group__c = newAccount.Group_from_Lead__c; 
                    newAccount.Group_from_Lead__c = null;
                }
                
                else // Group__c already exist, so append the values.  
                {                                                                                    
                    if(!newAccount.Group__c.contains(newAccount.Group_from_Lead__c))
                    {
                        
                        newAccount.Group__c += ';'+ newAccount.Group_from_Lead__c;                                              
                        newAccount.Group_from_Lead__c = null;
                        
                    }                                               
                }
            }//Code Addition End
            
            //T1 - change ends
            
            if(newAccount.Lead_Pearson_ID_Number__c !=null)
            {
                if(newAccount.Pearson_Account_Number__c == null && newAccount.Pearson_Account_Number__c == '')
                {
                    newAccount.Pearson_Account_Number__c = newAccount.Lead_Pearson_ID_Number__c;
                }    
            }
            //@Abhishek:-By passing for account which is not converted
            // change starts 
            if(newAccount.IsCreatedFromLead__c){
                if(/*newAccount.BillingStateCode!= null&&*/newAccount.BillingStreet != null && newAccount.BillingPostalCode != null && newAccount.BillingCountryCode != null && newAccount.BillingCity != null)
                {
                    //newAccount.Lead_State__c = newAccount.BillingStateCode; 
                    newAccount.Lead_State__c = newAccount.BillingState; 
                    newAccount.Lead_Street__c = newAccount.BillingStreet; 
                    newAccount.Lead_PostalCode__c = newAccount.BillingPostalCode; 
                    // newAccount.Lead_Country__c = newAccount.BillingCountryCode;
                    newAccount.Lead_Country__c = newAccount.BillingCountry;
                    newAccount.Lead_City__c = newAccount.BillingCity;
                    
                }
                
                
                if(newAccount.Lead_Sponsor_Type__c != null && newAccount.Lead_Sponsor_Type__c != '' && newAccount.Lead_Sponsor_Type__c != 'Self' && newAccount.Lead_Sponsor_Address_City__c != null && newAccount.Lead_Sponsor_Address_State_Province__c != null && newAccount.Lead_Sponsor_Address_Street__c !=null && newAccount.Lead_Sponsor_Address_ZIP_Postal_Code__c !=null && newAccount.Lead_Sponsor_Address_Country__c !=null)
                {
                    newAccount.BillingCity = newAccount.Lead_Sponsor_Address_City__c;
                    newAccount.BillingCountry = newAccount.Lead_Sponsor_Address_Country__c;
                    newAccount.BillingPostalCode = newAccount.Lead_Sponsor_Address_ZIP_Postal_Code__c;
                    newAccount.BillingStreet = newAccount.Lead_Sponsor_Address_Street__c;
                    newAccount.BillingState = newAccount.Lead_Sponsor_Address_State_Province__c;
                }
                if (newAccount.Lead_City__c != null && newAccount.Lead_Country__c != null && newAccount.Lead_PostalCode__c != null && newAccount.Lead_Street__c != null && newAccount.Lead_Conversion_Record_Type__c != Label.Lead_Record_Type_Value /*&& newAccount.Lead_State__c != null*/)
                {
                    newAccount.ShippingCity = (newAccount.ShippingCity==null || newAccount.ShippingCity=='')?newAccount.Lead_City__c:newAccount.ShippingCity;
                    // newAccount.ShippingCountryCode = (newAccount.ShippingCountryCode == null || newAccount.ShippingCountryCode == '')?newAccount.Lead_Country__c:newAccount.ShippingCountryCode;
                    newAccount.ShippingCountry = (newAccount.ShippingCountry == null || newAccount.ShippingCountry == '')?newAccount.Lead_Country__c:newAccount.ShippingCountry;
                    newAccount.ShippingPostalCode = (newAccount.ShippingPostalCode == null || newAccount.ShippingPostalCode== '')?newAccount.Lead_PostalCode__c:newAccount.ShippingPostalCode;
                    newAccount.ShippingStreet = (newAccount.ShippingStreet== null || newAccount.ShippingStreet == '')?newAccount.Lead_Street__c:newAccount.ShippingStreet;
                    //newAccount.ShippingStateCode = (newAccount.ShippingStateCode == null || newAccount.ShippingStateCode == '')?newAccount.Lead_State__c:newAccount.ShippingStateCode;
                    newAccount.ShippingState = (newAccount.ShippingState == null || newAccount.ShippingState == '')?newAccount.Lead_State__c:newAccount.ShippingState;
                }    
                
                //Added by Krishnapriya-CRMSFDC 2741 CR-01908-Req-67
               
               
                if(newAccount.Lead_Conversion_Record_Type__c == Label.Enterprise_Lead_Record_Type)
                {
                    accountMappingUtility(newAccount,OrganisationAccountRecordTypeId);
                }                
                //end Krishnapriya changes   
                
                if((newAccount.Lead_Conversion_Record_Type__c == Label.PS_B2BLeadRecordType) /*|| (newAccount.Lead_Conversion_Record_Type__c == Label.PS_NewAccountRequestLeadRecordType)*/)  
                {
                    //Added by Abraham
                    if(newAccount.Is_created_from_case__c != True && userdetails[0].Geography__c == 'North America' && userdetails[0].Line_of_Business__c == 'Higher Ed' && userdetails[0].Market__c == 'US')
                    {
                        
                        accountMappingUtility(newAccount,OrganisationAccountRecordTypeId);
                    }
                    //Abraham change ends
                    
                    else if(newAccount.Organisation_Type__c == Label.PS_LeadOrgTypeGovernment_Public || newAccount.Organisation_Type__c == Label.PS_LeadOrgTypeInstitution || newAccount.Organisation_Type__c == Label.PS_LeadOrgTypePurchasing || newAccount.Organisation_Type__c == Label.PS_LeadOrgTypeNGO_Association || newAccount.Organisation_Type__c == Label.PS_LeadOrgTypeLargeCorporate || newAccount.Organisation_Type__c == Label.PS_LeadOrgTypeSmallCorporate)
                    {
                        if(CorporateAccountRecordTypeId != null)
                        {
                            
                            accountMappingUtility(newAccount,CorporateAccountRecordTypeId);
                        }
                    }
                    else if(newAccount.Organisation_Type__c  == Label.PS_LeadOrgTypeHigherEducation)
                    {
                        if(HigherEducationAccountRecordTypeId != null)
                        {
                            accountMappingUtility(newAccount,HigherEducationAccountRecordTypeId);
                        }
                    }
                    else if(newAccount.Organisation_Type__c == Label.PS_LeadOrgTypeSchool)
                    {
                        if(SchoolAccountRecordTypeId !=null)
                        {
                            accountMappingUtility(newAccount,SchoolAccountRecordTypeId);
                        }   
                    } 
                    else if(newAccount.Organisation_Type__c == Label.PS_LeadOrgTypeProfessional)
                    {
                        if(ProfessionalAccountRecordTypeId != null)
                        {
                            accountMappingUtility(newAccount,ProfessionalAccountRecordTypeId);
                        }
                    } 
                    else{
                        if(newAccount.Business_unit_from_Lead__c!='US Field Sales' && newAccount.market2__c!='US')
                        {
                            if(CorporateAccountRecordTypeId != null)
                            {
                                accountMappingUtility(newAccount,CorporateAccountRecordTypeId);
                            }
                        }
                        else
                        {
                            if(OrganisationAccountRecordTypeId != null)
                            {
                                accountMappingUtility(newAccount,OrganisationAccountRecordTypeId);
                            }
                        }
                        
                    }
                }
                
                if(newAccount.Lead_Conversion_Record_Type__c  == Label.PS_D2LLeadRecordType)
                {
                    if(newAccount.RecordTypeId != null)
                    {
                        newAccount.RecordTypeId = LearnerAccountRecordTypeId; 
                    }    
                }
                
                if(newAccount.Type == null || newAccount.Type == '')
                {
                    if(newAccount.Type__c != null && newAccount.Type__c !='')
                    {
                        newAccount.Type = newAccount.Type__c;
                    }
                }
            }
        }// - change END 
        
    }
    
    public static void accountMappingUtility(Account accountIns,ID accountRecTypeID)
    {
        accountIns.RecordTypeId = accountRecTypeID;
    }    
}// TESTBAU