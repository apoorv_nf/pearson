@isTest
public class OpportunityProbabilityUpdationBatchTest {
    static Opportunity masterOpp;
    static Opportunity masterOpp1; 
    static List<Opportunity> childOpportunityList;
    static List<Opportunity> childOpportunityQuote;
    public static Map<Id,Opportunity> oppStageAwarded; //child closed won
    public static Map<Id,Opportunity> oppLostmasternego; //child closed lost
    public static Map<Id,Opportunity> oppLostmasterneedanalysis; //child closed lost
    public static Map<Id,List<id>>    oppChildIds; //map to hold child oppty for quotes
    static RecordType opprt; 
    static RecordType caseRecord; 
    static Campaign cmp;    
    static Account acc;
    static Account acc1;
    static Contact con;
    static Contact con1;
    static Case cs;
    public static string batchQuery; 
   
    static testMethod void oppProbUpdateTestMethod(){
         String recordLimit = '';
        Boolean settingFound = false;
        String customSettingDate='';   
        //Create Test data
        acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', 
                                  Line_of_Business__c= 'Schools', Geography__c= 'Core', 
                                  ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', 
                                  ShippingPostalCode = '5600371',Pearson_Customer_Number__c='1111');
        insert acc;
        acc1 = new Account(Name='Test Account2', IsCreatedFromLead__c = True,Phone='+910000013',Market2__c= 'UK', 
                                  Line_of_Business__c= 'Schools', Geography__c= 'Core', 
                                  ShippingCountry = 'India', ShippingCity = 'Bangalore4', ShippingStreet = 'BNG13', 
                                  ShippingPostalCode = '56003714',Pearson_Customer_Number__c='12344');
        insert acc1;
        
        con = new Contact(FirstName='TestContactFirstname1', LastName='TestContactLastname',AccountId=acc.id ,Salutation='MR.', Email='xyz@email.com',MobilePhone='111222333' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
        insert con;
        
        con1 = new Contact(FirstName='TestContactFirstname2', LastName='TestContactLastname2',AccountId=acc1.id ,Salutation='MRS', Email='xyz1@email.com',MobilePhone='1112223334' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test1',OtherCity  = 'Test1' , OtherPostalCode  = '1234567',First_Language__c='English');  
        insert con1;
        
        String recordType = 'Global Opportunity';
        opprt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name =: recordType ];
        
        Test.startTest(); 
        Opportunity opp = new Opportunity();
        opp.Name = 'TestMasterOppProbName';
        opp.accountid = acc.id;
        opp.StageName = 'Prequalification';
        opp.CloseDate = System.TODAY() + 10;
        opp.Include_in_Probablity_Calculations__c = true;
        opp.Probability = 10; 
        opp.Enquiry_Type__c= 'Qualifications';
        opp.RecordTypeId = opprt.id;
        insert opp;
        
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TestMasterOppProbName1';
        opp1.accountid = acc1.id;
        opp1.StageName = 'Prequalification';
        opp1.CloseDate = System.TODAY() + 10;
        opp1.Include_in_Probablity_Calculations__c = true;
        opp1.Probability = 10; 
        opp1.Enquiry_Type__c= 'Qualifications';
        opp1.RecordTypeId = opprt.id;
        insert opp1;
        
         //Create Opp with Child Stage = 'Closed Won' senario
        childOpportunityList = new List<Opportunity>();
        Opportunity childOpportunity = new Opportunity();
        childOpportunity.Name = 'TestChildOppProbName4';
        childOpportunity.StageName = 'Closed Won';
        childOpportunity.CloseDate = System.TODAY() + 10;
        childOpportunity.Related_Opportunity__c = opp.id;
        childOpportunity.Probability = 100; 
        childOpportunity.RecordTypeId = opprt.id;
        insert childOpportunity;

        cmp = createCampaign();
        insert cmp;
        
        Campaign_Opportunity__c cmpOpp = new Campaign_Opportunity__c();//create Campaign Opportunity
        cmpOpp.Campaign__c =  cmp.id;
        cmpOpp.Opportunity__c = opp1.id;
        insert cmpOpp;
        
        CampaignMember cmpMem = createCampaignMember('Customer Qualified Interest',true);
        insert cmpMem;


//Insert Custom setting
        list<General_One_CRM_Settings__c> lstcust=new list<General_One_CRM_Settings__c>();
        General_One_CRM_Settings__c objSetting = new General_One_CRM_Settings__c();
        objSetting.Name='MasterOppProbabilityCalculation';
        Date d = Date.newInstance(2016, 06, 20);
        objSetting.PS_Integration_Delete_Date__c = d;
        objSetting.Category__c = 'Batch Processing';
        objSetting.Description__c ='This record holds the date for which opportunities need to be processed';
        objSetting.Value__c='6000';
        lstcust.add(objSetting);
        insert lstcust;
        
        batchQuery ='SELECT Id, Name,Probability,StageName,Account.Pearson_Customer_Number__c, (select id, Campaign__c, opportunity__c from Campaign_Opportunities__r),'+
            '(select id,StageName,Probability,Recordtype.Name from Opportunities__r) from opportunity where Include_in_Probablity_Calculations__c = true '+
            'and (StageName!=\'Closed Won\' AND StageName!=\'Closed Lost\' AND StageName!=\'Awarded\') and '+
            'Enquiry_Type__c =\'Qualifications\' and '+
            'RecordType.Name=\'Global Opportunity\'';        
        
        if (lstcust[0]!= null)
            customSettingDate = lstcust[0].PS_Integration_Delete_Date__c+''; 
        if (customSettingDate != ''){
            customSettingDate=customSettingDate.substring(0,customSettingDate.indexOf(' '))+'T00:00:00Z'; 
            batchQuery = batchQuery +' and createddate >= '+customSettingDate;
        }
        
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        //Run the test
        System.runAs(listWithUser[0]){
            
            OpportunityProbabilityUpdationBatch batchObject = new OpportunityProbabilityUpdationBatch();            
            batchObject.batchQuery = batchQuery ;
            ID batchprocessid = database.executeBatch(batchObject);
            Test.stopTest();    
        
        }
    }
     
  /*  public static Campaign_Opportunity__c createCampaignOpp(){
        Campaign_Opportunity__c cmpOpp = new Campaign_Opportunity__c();//create Campaign Opportunity
        cmpOpp.Campaign__c =  cmp.id;
        cmpOpp.Opportunity__c = masterOpp.id;
        return cmpOpp;
    }*/
    
     public static Campaign createCampaign(){
         cmp = new Campaign(Name='TestCampaign');
         cmp.Campaign_Objectives__c = 'objectives XYZ';
         return cmp;
     } 
    
    public static CampaignMember createCampaignMember(String statusC, boolean subevSury){
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignID = cmp.id;
        
        cmpMem.ContactId= con1.id;
        cmpMem.Status__c = statusC;
        cmpMem.Submitted_Event_Survey__c = subevSury;
        cmpMem.Status = 'Responded';
        cmpMem.Considers_Offering__c=true;
        //cmpMem.type='Contact'; 
        cmpMem.Centre_Number__c = '1234';
        return cmpMem;        
    }

public static CampaignMember createCampaignMember1(String statusC, boolean subevSury){
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignID = cmp.id;
        
        cmpMem.ContactId= con1.id;
        cmpMem.Status__c = statusC;
        cmpMem.Submitted_Event_Survey__c = subevSury;
        cmpMem.Status = 'Responded';
        //cmpMem.Considers_Offering__c=true;
        //cmpMem.type='Contact'; 
        cmpMem.Centre_Number__c = '1234';
        return cmpMem;        
    }
    

    static testMethod void oppProbUpdateTestMethod1(){
      
             String customSettingDate=''; 
             //Create Test data
        acc1 = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', 
                                  Line_of_Business__c= 'Schools', Geography__c= 'Core', 
                                  ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', 
                                  ShippingPostalCode = '5600371',Pearson_Customer_Number__c='1234');
        insert acc1;
        con1 = new Contact(FirstName='TestContactFirstname3', LastName='TestContactLastname',AccountId=acc1.id ,Salutation='MRS.', Email='xyz3@email.com',MobilePhone='11122233344' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
        insert con1;
        String recordType = 'Global Opportunity';
        opprt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name =: recordType ];
  
       Test.startTest();
     //Campaign Members Senario
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TestMasterOppProbName3';
        opp1.accountid = acc1.id;
        opp1.StageName = 'Prequalification';
        opp1.CloseDate = System.TODAY() + 10;
        opp1.Include_in_Probablity_Calculations__c = true;
        opp1.Probability = 10; 
        opp1.Enquiry_Type__c= 'Qualifications';
        opp1.RecordTypeId = opprt.id;
        insert opp1;
        
        Opportunity opp2 = new Opportunity();
        opp2.Name = 'TestMasterOppProbName3';
        opp2.accountid = acc1.id;
        opp2.StageName = 'Prequalification';
        opp2.CloseDate = System.TODAY() + 10;
        opp2.Include_in_Probablity_Calculations__c = true;
        opp2.Probability = 10; 
        opp2.Enquiry_Type__c= 'Qualifications';
        opp2.RecordTypeId = opprt.id;
        insert opp2;
        
        cmp = createCampaign();
        insert cmp;
        Campaign_Opportunity__c cmpOpp = new Campaign_Opportunity__c();//create Campaign Opportunity
        cmpOpp.Campaign__c =  cmp.id;
        cmpOpp.Opportunity__c = opp1.id;
        insert cmpOpp;
        
        CampaignMember cmpMem = createCampaignMember1('Customer Researching',false);
        insert cmpMem;
        
        //Child Lost Opp
        
        childOpportunityQuote = new List<Opportunity>();
        Opportunity childOpportunity1 = new Opportunity();
        childOpportunity1.Name = 'TestChildOppProbName4';
        childOpportunity1.StageName = 'Prequalification';
        childOpportunity1.CloseDate = System.TODAY() + 10;
        childOpportunity1.Related_Opportunity__c = opp2.id;
        childOpportunity1.Probability = 5;
        childOpportunity1.Channel__C='Direct'; 
        childOpportunity1.Lost_Reason__c = 'No Bid';
        childOpportunity1.RecordTypeId = opprt.id;
        childOpportunityQuote.add(childOpportunity1);
        insert childOpportunityQuote;
        
                //Quote Insert 
        List<RecordType> quteRecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Quote' AND Name = 'Global Quote' LIMIT 1]; 
            Quote qute = new Quote();
            qute.Name='Quote1';
            //qute.AccountId= acc1.id;
            qute.OpportunityId= childOpportunityQuote[0].id;
            // Commented for CR-2949 Apttus(Changes) by Rajesh Paleti            
            //qute.Quote_Channel__c = 'Direct';
            qute.RecordTypeId = quteRecType[0].id;
            insert qute; 

    
//Insert Custom setting
        list<General_One_CRM_Settings__c> lstcust=new list<General_One_CRM_Settings__c>();
        General_One_CRM_Settings__c objSetting = new General_One_CRM_Settings__c();
        objSetting.Name='MasterOppProbabilityCalculation';
        Date d = Date.newInstance(2016, 06, 20);
        objSetting.PS_Integration_Delete_Date__c = d;
        objSetting.Category__c = 'Batch Processing';
        objSetting.Description__c ='This record holds the date for which opportunities need to be processed';
        objSetting.Value__c='6000';
        lstcust.add(objSetting);
        insert lstcust;
        
        
        batchQuery ='SELECT Id, Name,Probability,StageName,Account.Pearson_Customer_Number__c, (select id, Campaign__c, opportunity__c from Campaign_Opportunities__r),'+
            '(select id,StageName,Probability from Opportunities__r) from opportunity where Include_in_Probablity_Calculations__c = true '+
            'and (StageName!=\'Closed Won\' AND StageName!=\'Closed Lost\' AND StageName!=\'Awarded\') and '+
            'Enquiry_Type__c =\'Qualifications\' and '+
            'RecordType.Name=\'Global Opportunity\'';        
        
        if (lstcust[0]!= null)
            customSettingDate = lstcust[0].PS_Integration_Delete_Date__c+''; 
        if (customSettingDate != ''){
            customSettingDate=customSettingDate.substring(0,customSettingDate.indexOf(' '))+'T00:00:00Z'; 
            batchQuery = batchQuery +' and createddate >= '+customSettingDate;
        }
        
       
        //Run the test
          List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        System.runAs(listWithUser[0]){    
         
            OpportunityProbabilityUpdationBatch batchObject = new OpportunityProbabilityUpdationBatch();            
            batchObject.batchQuery = batchQuery ;
            ID batchprocessid = database.executeBatch(batchObject);
            Test.stopTest();    
        
        }
    
    }
 
    static testMethod void oppTriggerHandlertest1()
    {
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].Id,Disable_Validation_Rules__c =true,Disable_Process_Builder__c =true,Disable_Triggers__c =true,Disable_Workflow_Rules__c =true);
        insert byp;
        System.runAs(listWithUser[0]){
        
        String customSettingDate=''; 
             //Create Test data
        acc1 = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', 
                                  Line_of_Business__c= 'Schools', Geography__c= 'Core', 
                                  ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', 
                                  ShippingPostalCode = '5600371',Pearson_Customer_Number__c='1234');
        insert acc1;
        con1 = new Contact(FirstName='TestContactFirstname3', LastName='TestContactLastname',AccountId=acc1.id ,Salutation='MRS.', Email='xyz3@email.com',MobilePhone='11122233344' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
        insert con1;
        String recordType = 'B2B';
        opprt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name =: recordType ];
      Map<Id,OpportunityContactRole> ocrsMap = new Map<Id,OpportunityContactRole>();
        Pricebook2 pb=new Pricebook2();
        pb.name='Standard Price Book';
        insert pb;
        Test.startTest();
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TestMasterOppProbName3';
        opp1.accountid = acc1.id;
        opp1.StageName = 'Prequalification';
        opp1.CloseDate = System.TODAY() + 10;
        opp1.Include_in_Probablity_Calculations__c = true;
        opp1.Probability = 10; 
        opp1.RecordTypeId = opprt.id;
        opp1.Pricebook2Id=pb.Id;
        opp1.Registration_Payment_Reference__c= 'testing1';
        opp1.Received_Signed_Registration_Contract__c=True;
        insert opp1;
        
        Quote_Settings__c qsvalue = new Quote_Settings__c();
        qsvalue.Academic_Start_Date_Term_1__c=System.TODAY() + 10;
        qsvalue.Academic_Start_Date_Term_2__c= System.TODAY() + 10;
        qsvalue.Academic_Start_Date_Term_3__c=System.TODAY() + 10;
        qsvalue.Academic_Start_Date_Term_4__c=System.TODAY() + 10;
        
        Contract con = new Contract();
        con.Opportunity__c = opp1.Id;
        con.AccountId= opp1.AccountId;
        con.StartDate =system.today();
        con.ContractTerm = 4;    
        //insert con;
        
      /*   OpportunityContactRole ocr = new OpportunityContactRole();
    ocr.ContactId = con1.Id;
    ocr.OpportunityId = opp1.Id;
    ocr.IsPrimary = TRUE;
    ocr.Role = 'Decision Maker';
    insert ocr;*/
       opp1.StageName = 'Needs Analysis';
     /*  opp1.TotalOpportunityQuantity=25.0;
       opp1.Fall__c=1;
       opp1.Primary_Pearson_Course_Code__c='test'; 
       opp1.Decision_Date__c= System.TODAY() + 10;*/
        try{
            update opp1;
        }
        catch(Exception ex)
        {
             System.assert(true, 'Please provide at least one Contact in the Opportunity Contact Role related list before changing the Stage from Prequalification.');
        }  
         Test.stopTest();  }

    } 
    
    static testMethod void oppTriggerHandlertestEnterprise2()
    {
        
        String customSettingDate=''; 
             //Create Test data
        acc1 = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', 
                                  Line_of_Business__c= 'Schools', Geography__c= 'Core', 
                                  ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', 
                                  ShippingPostalCode = '5600371',Pearson_Customer_Number__c='1234');
        insert acc1;
        con1 = new Contact(FirstName='TestContactFirstname3', LastName='TestContactLastname',AccountId=acc1.id ,Salutation='MRS.', Email='xyz3@email.com',MobilePhone='11122233344' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
        insert con1;
        String recordType = 'Enterprise';
        opprt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name =: recordType ];
         Map<Id,OpportunityContactRole> ocrsMap;
        Map<Id,OpportunityCompetitor> competitorsMap;
        
    Default_B2B_Service_Assignee__c rd= new Default_B2B_Service_Assignee__c(); 
        rd.Assignee_User_ID__c='005b0000002hB9C';
        insert rd;
        Test.startTest();
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TestMasterOppProbName3';
        opp1.accountid = acc1.id;
        opp1.StageName = 'Needs Analysis';
        opp1.CloseDate = System.TODAY() + 10;
        opp1.Include_in_Probablity_Calculations__c = true;
        opp1.Probability = 10; 
        opp1.RecordTypeId = opprt.id;
        opp1.Assigned_Regional_Director__c=rd.Assignee_User_ID__c;
        insert opp1;
        
        Quote_Settings__c qsvalue = new Quote_Settings__c();
        qsvalue.Academic_Start_Date_Term_1__c=System.TODAY() + 10;
        qsvalue.Academic_Start_Date_Term_2__c= System.TODAY() + 10;
        qsvalue.Academic_Start_Date_Term_3__c=System.TODAY() + 10;
        qsvalue.Academic_Start_Date_Term_4__c=System.TODAY() + 10;
      
     
      opp1.StageName = 'Solutioning';
    
        try{
            update opp1;
        }
        catch(Exception ex)
        {           
            System.assert(true, 'Please provide at least one Contact before changing the Stage from Needs Analysis to Solutioning');
             System.assert(true, 'Please provide at least one Competitor before changing the Stage from Needs Analysis to Solutioning');
        }
        Test.stopTest();  
    } 

     static testMethod void oppTriggerHandlertestD2L()
    {
         UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEOHigher Education Consultant');
        insert tempUserRole;
      User usr;
      usr = new User(LastName = 'ETL Integration', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', License_Pools__c='Test User ',LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'ZA', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
          System.runAs(usr){ 
        
        String customSettingDate=''; 
             //Create Test data
        acc1 = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', 
                                  Line_of_Business__c= 'Schools', Geography__c= 'Core', 
                                  ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', 
                                  ShippingPostalCode = '5600371',Pearson_Customer_Number__c='1234');
        insert acc1;
        con1 = new Contact(FirstName='TestContactFirstname3', LastName='TestContactLastname',AccountId=acc1.id ,Salutation='MRS.', Email='xyz3@email.com',MobilePhone='11122233344' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
        insert con1;
         
       Id caserId = [select Id, name from RecordType where name ='Change Request' and  SObjectType='Case' limit 1].Id;
       
        
        String recordType = 'D2L';
        opprt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name =: recordType ];
       
    Default_B2B_Service_Assignee__c rd= new Default_B2B_Service_Assignee__c(); 
        rd.Assignee_User_ID__c='005b0000002hB9C';
        insert rd;
        Test.startTest();
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TestMasterOppProbName3';
        opp1.accountid = acc1.id;
        opp1.StageName = 'Needs Analysis';
        opp1.CloseDate = System.TODAY() + 10;
        opp1.Include_in_Probablity_Calculations__c = true;
        opp1.Probability = 10; 
        opp1.RecordTypeId = opprt.id;
        opp1.Assigned_Regional_Director__c=rd.Assignee_User_ID__c;
        insert opp1;
         cs=new Case(AccountId = acc1.Id, ContactId =con1.Id,RecordTypeId = caserId,Type='Change Campus',Status='New',Current_Campus__c ='Vanderbijlpark',Proposed_Campus__c ='Vanderbijlpark',Opportunity__c=opp1.Id,Validate_Transfer__c = True);
        insert cs;
        
      opp1.StageName = 'Closed';
    
        try{
            
            update opp1;             
        }
        catch(Exception ex)
        {
          
        }
        Test.stopTest();  
    } 
    }  
    
    static testMethod void oppTriggerHandlertestD2L1()
    {
        
        
        String customSettingDate=''; 
             //Create Test data
        acc1 = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', 
                                  Line_of_Business__c= 'Schools', Geography__c= 'Core', 
                                  ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', 
                                  ShippingPostalCode = '5600371',Pearson_Customer_Number__c='1234');
        insert acc1;
        con1 = new Contact(FirstName='TestContactFirstname3', LastName='TestContactLastname',AccountId=acc1.id ,Salutation='MRS.', Email='xyz3@email.com',MobilePhone='11122233344' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
        insert con1;
         
       Id caserId = [select Id, name from RecordType where name ='Change Request' and  SObjectType='Case' limit 1].Id;
       
        
        String recordType = 'D2L';
        opprt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name =: recordType ];
       
    Default_B2B_Service_Assignee__c rd= new Default_B2B_Service_Assignee__c(); 
        rd.Assignee_User_ID__c='005b0000002hB9C';
        insert rd;
        Test.startTest();
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TestMasterOppProbName3';
        opp1.accountid = acc1.id;
        opp1.StageName = 'Needs Analysis';
        opp1.CloseDate = System.TODAY() + 10;
        opp1.Include_in_Probablity_Calculations__c = true;
        opp1.Probability = 10; 
        opp1.RecordTypeId = opprt.id;
        opp1.Assigned_Regional_Director__c=rd.Assignee_User_ID__c;
        insert opp1;
         cs=new Case(AccountId = acc1.Id, ContactId =con1.Id,RecordTypeId = caserId,Type='Change Campus',Status='New',Current_Campus__c ='Durban',Proposed_Campus__c ='Durban',Opportunity__c=opp1.Id,Validate_Transfer__c = True);
        insert cs;
      opp1.StageName = 'Closed';
    
        try{
            
            update opp1;             
        }
        catch(Exception ex)
        {
          
        }
        Test.stopTest();  
    } 
    
}