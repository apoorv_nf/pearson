/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            AssignTasks.cls 
   Description:     Batch Class for creating the Tasks for the Inner Public Groups.
   Test class:      
   Date             Version              Author                          Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  02-Nov-2016         0.2              Manikanta Nagubilli               Created
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
global class AssignTasks implements Database.Batchable<sObject>,Database.Stateful
{     
    global final set<id> subgroupid;
    global final boolean isUserExcluded;
    global final Task taskRecord;
    global set<id> finalgroupids;
    public set<id> Roleids;
    global AssignTasks(Set<id> subgroups,boolean UserExcluded,task t){
             subgroupid = subgroups;
             isUserExcluded = UserExcluded;
             finalgroupids = new set<id>();
             taskRecord = t;
             Roleids = new set<id>();
    }    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator('select id,UserOrGroupId FROM GroupMember where group.id IN:subgroupid and group.type=\'Regular\'');
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope)
    {       
        set<id> UserIds = new set<id>();
        set<id> GroupIds = new set<id>();
        List<GroupMember> selectedGroupMembers = (List<GroupMember>)scope;
        system.debug(selectedGroupMembers+'**PRINT MEMBERS***');
        for(GroupMember groupMemberIds : selectedGroupMembers){
            if(((string)groupMemberIds.UserOrGroupId).startswith(Schema.SObjectType.User.getKeyPrefix()))
            {
                if (isUserExcluded){
                    if (groupMemberIds.UserOrGroupId != userinfo.getuserid())
                        UserIds.add(groupMemberIds.UserOrGroupId);     
                }
                else
                    UserIds.add(groupMemberIds.UserOrGroupId);
            } 
            else if(((string)groupMemberIds.UserOrGroupId).startswith(Schema.SObjectType.Group.getKeyPrefix()))
            {
                GroupIds.add(groupMemberIds.UserOrGroupId);
            }                            
        }
        
         if(!GroupIds.IsEmpty())
        {
            Set<id> PassRoleids = new Set<id>();
            Set<id> SubUsersids = new Set<id>();
            Map<id,Group> GrpList = new Map<id,Group>([Select id,RelatedId,Type from Group where id IN: GroupIds]);
            if(!GrpList.Values().IsEmpty())
            {
                for(group grp: GrpList.Values())
                {
                    if(grp.Relatedid != null && ((string)grp.Relatedid).startswith(Schema.SObjectType.UserRole.getKeyPrefix()))
                    {
                        Roleids.add(grp.RelatedId);
                        if(grp.type.contains('RoleAnd'))
                        {
                            PassRoleids.add(grp.RelatedId);
                        }
                    }
                    else if(grp.Relatedid == null && grp.Type == 'Regular')
                    {
                        finalgroupids.add(grp.id);
                    }
                }
                SubUsersids = RoleUtils.getAllSubRoleIds(PassRoleids);
                if(!SubUsersids.IsEmpty())
                {
                    Map<Id,User> users = new Map<Id, User>([Select Id, Name From User where UserRoleId IN :SubUsersids]);
                    UserIds.addall(users.Keyset());
                }
            }
        }
        system.debug('***PRINT BATCH FINAL GROUP IDS***'+finalgroupids);
        if(!Roleids.IsEmpty())
        {
            Map<Id,User> users = new Map<Id, User>([Select Id, Name From User where UserRoleId IN :Roleids Limit: (Limits.getLimitQueryRows()- Limits.getQueryRows())]);
            UserIds.addall(users.Keyset());
            if(isUserExcluded && UserIds.contains(userinfo.getuserid()))
            UserIds.remove(userinfo.getuserid());
        } 
        
        if(!UserIds.IsEmpty()) 
        {
            String count = RoleUtils.createTask(UserIds,taskRecord);
            System.debug('count -->'+count);
            if(count != null)
            {
                integer grpErrorCount = 0;
                integer errorCount = 0;
                integer sucessCount = 0;
                if(count != null && count.contains('grpError'))
                {
                    grpErrorCount = integer.valueof(count.substringAfter(','));
                }
                if(count != null && count.contains('Sucess'))
                {
                    sucessCount = integer.valueof(count.substringAfter(','));
                }
                if(count != null && count.contains('error'))
                {
                    errorCount = integer.valueof(count.substringAfter(','));
                }
             }   
            
        }   
        
    }
    global void finish(Database.BatchableContext BC)
    {
       if(!finalgroupids.IsEmpty())
       {
           Id batchInstanceId = Database.executeBatch(new AssignTasks(finalgroupids,isUserExcluded,taskRecord), 50);
       }
    }
}