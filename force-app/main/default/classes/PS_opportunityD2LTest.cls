/************************************************************************************************************
* Apex Interface Name : PS_opportunityD2LTest
* Version             : 1.0 
* Created Date        : 9/9/2015 4:44 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Abhinav Srivastav         9/9/2015              R2.1 version
-------------------------------------------------------------------------------------------------------------
*  Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 65,72) 
************************************************************************************************************/

@isTest(SeeAllData=true)
private Class PS_opportunityD2LTest{
    static testMethod void myUnitTest(){
         List<User> listWithUser = new List<User>();
         Map<Id,opportunity> newOpps = new Map<Id,opportunity>();
         Map<Id,opportunity> oldOpps = new Map<Id,opportunity>();
         List<Opportunity> opptylist = new List<Opportunity>();
         Id oppId ;
         
         Map<Id,Quote> newprops = new Map<Id,Quote>();
         Map<Id,Quote> oldprops = new Map<Id,Quote>();
         Id propId;
         
         Map<Id,OpportunityLineItem> newLineItems = new Map<Id,OpportunityLineItem>();
         Map<Id,OpportunityLineItem> oldLineItems = new Map<Id,OpportunityLineItem>();
         Id opliId;
         
         Map<Id,Asset> newAssets = new Map<Id,Asset>();
         Map<Id,Asset> oldAssets = new Map<Id,Asset>();
         Id asId;
         
         Map<Id,Event> newEvents = new Map<Id,Event>();
         Map<Id,Event> oldEvents = new Map<Id,Event>();
         List<Event> evlist = new List<Event>();
         Id evId;
         
         Map<Id,Task> newTasks = new Map<Id,Task>();
         Map<Id,Task> oldTasks = new Map<Id,Task>();
         List<Task> tklist = new List<Task>();
         Id tkId;
         
         Map<String,List<String>> strmap = new Map<String,List<String>>();
         List<String> strlist = new List<String>{'a','b','c'};
         strmap.put('1',strlist);
         Boolean issalesuser1;
         issalesuser1=false;
         Boolean isSalesUser;
         isSalesUser=true;
         Integer k;
         k=0;
         listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        System.runAs(listWithUser[0]){
        
        Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School',Market2__c='US',Phone = '9989887687');
        acc.ShippingStreet = 'TestStreet';
        acc.ShippingCity = 'Vns';
        acc.ShippingState = 'Delhi';
        acc.ShippingPostalCode = '234543';
        acc.ShippingCountry = 'India';
        acc.IsCreatedFromLead__c = true;
        insert acc;
        
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true limit 1];
        Product2 pr1 = new Product2(Name = 'Bundle Product', Market__c= listWithUser[0].Market__c, IsActive = true, Configuration_Type__c = 'Bundle',Qualification_Name__c = 'Test Bundle',Campus__c='Durbanville',Qualification_Level_Name__c= 2,Business_Unit__c = 'CTIPIHE',Line_of_Business__c='Higher Ed');
        pr1.Market__c = listWithUser[0].Market__c;
        pr1.Line_of_Business__c = listWithUser[0].Line_of_Business__c;
        pr1.Business_Unit__c = listWithUser[0].Product_Business_Unit__c;
        insert pr1;
        
        
        Product2 pr2 = new Product2(Name = 'Option Product', IsActive = true, Configuration_Type__c = 'Option',Business_Unit__c = 'CTIPIHE',Market__c = 'US',Line_of_Business__c='Higher Ed');
        insert pr2;
        
        Pricebook2 pbk1 = new Pricebook2 (Name='Test Pricebook Entry 1',Description='Test Pricebook Entry 1',isActive=true);
        Database.insert(pbk1);
        
        //Pricebook2 oppprice = [select pricebook from Opportunity where id =: op.id].pricebook;
        
        PricebookEntry pbe1 = new PricebookEntry (Product2ID= pr1.id,Pricebook2ID=standardPB.id,UnitPrice=50,IsActive =true);
        PricebookEntry customprice = new pricebookentry( product2id = pr1.id, pricebook2id = pbk1.id,UnitPrice=50,usestandardprice = false, IsActive = true);
        Database.insert(pbe1);
        Database.insert(customprice);
        Opportunity ro = new Opportunity(Name= 'OpTest', AccountId = acc.id, StageName = 'Solutioning', Type = 'New Business', Academic_Vetting_Status__c = 'Un-Vetted', Academic_Start_Date__c = System.Today(),CloseDate = System.Today(),International_Student__c = true, Channel__c = 'Direct');
        insert ro;
        oppId = ro.id;
        newOpps.put(ro.Id,ro);
        Quote prop = new Quote(Name = 'OpTest', OpportunityId = ro.Id, Payment_Method__c='Direct Deposit', Payment_Type__c='Monthly Payment',Payment_Period_In_Month__c= '10',First_Payment_Date__c= System.Today());
        insert prop ;
        propId = prop.Id;
        newprops.put(prop.id,prop);
        OpportunityLineItem oli = new OpportunityLineItem(PricebookEntryId = pbe1.id, OptionId__c = pr2.id, TotalPrice= 1000,Discount_Reference__c = 'Financial Credit',Amendment_Action__c = 'Deferred', Quantity = 1, Opportunityid = ro.id);
        insert oli;
        opliId = oli.id;
        oldLineItems.put(oli.id,oli);
        newLineItems.put(oli.id,oli);
        Event e = new Event(OwnerId = userinfo.getUserId(),whatId = ro.Id, Type__c = 'Face To Face Meeting',IsRecurrence  = false,subject__c = 'Returning Student Interview', Subject = 'Enrollment Date - Returning Interview',EndDateTime =System.Today(),StartDateTime = System.Today(),Location = 'To Be Determined',IsAllDayEvent = true,Status__c = 'Completed',ActivityDate = System.Today(),ActivityDateTime =  System.Today(),Sponsor_Attended__c = True);
        insert e;
        evId = e.id;
        evlist.add(e);
        if(!evlist.isEmpty()){
       try{
            insert evlist;
            }catch(System.DMLException de){
        }
        }
        newevents.put(e.id,e);
        oldevents.put(e.id,e);
        Task tk = new Task(Subject ='Email',Priority ='Normal', Status ='Not Started', WhatId = ro.Id,Ownerid =userinfo.getUserId());
        insert tk;
        tkId = tk.id;
        tklist.add(tk);
        if(!tklist.isEmpty()){
           try{
            insert tklist ;
            }catch(System.DMLException de){
        }
        }
        newtasks.put(tk.id,tk);
        oldtasks.put(tk.id,tk);
        Asset ass = new Asset(Product2id=pr1.id,status__c='Active',Name='Bundle Product',AccountId= acc.Id, Opportunity__c=ro.id);
        insert ass;
        newAssets.put(ass.id,ass);
        asId = ass.id; 
        Test.StartTest();
        
        PS_opportunityD2L oppD2L = new PS_opportunityD2L();
        oppD2L.opptyinitialize(opptylist,newOpps,oldOpps);
        //oppD2L.opptyvalidateUpdate(strmap);
        oppD2L.proposalopptyinitialize(newprops,oldprops);
        oppD2L.opptyvalidateUpdate(strmap);
        oppD2L.opptylineiteminitialize(newlineItems,oldLineItems,'Update');
        oppD2L.opptyvalidateUpdate(strmap);
        oppD2L.opptyPIUinitialize(newAssets,oldAssets,'Update');
        oppD2L.opptyeventinitialize(evlist,newEvents,oldEvents,'Update');
        oppD2L.opptyvalidateUpdate(strmap);
        oppD2L.opptytaskinitialize(tklist,newTasks,oldTasks,'Update');
        oppD2L.opptyvalidateUpdate(strmap);
        oppD2L.getOpportunityInfo();
        oppD2L.opptyvalidateUpdate(strmap);
        //oppD2L.validateUpdateSingleOppty(oppId,'Opportunity',isSalesUser);
        oppD2L.validateUpdateSingleOppty(propId,'Proposal',isSalesUser);
        oppD2L.validateUpdateSingleOppty(opliId ,'OpportunityLineItem',isSalesUser);
        oppD2L.validateUpdateSingleOppty(evId,'Event',isSalesUser);
        oppD2L.validateUpdateSingleOppty(evId,'Event',isSalesUser1);
        oppD2L.validateUpdateSingleOppty(asId,'Asset',isSalesUser);
        oppD2L.validateUpdateSingleOppty(asId,'Asset',isSalesUser1);
        oppD2L.validateUpdateSingleOppty(tkId ,'Task',isSalesUser);
        oppD2L.validateUpdateSingleOppty(tkId ,'Task',isSalesUser1);

        oppD2L.opptylineiteminitialize(newlineItems,oldLineItems,'Delete');
        oppD2L.opptyvalidateUpdate(strmap);
        oppD2L.opptyeventinitialize(evlist,newEvents,oldEvents,'Delete');
        oppD2L.opptyvalidateUpdate(strmap);
        oppD2L.opptytaskinitialize(tklist,newTasks,oldTasks,'Delete');
        oppD2L.opptyvalidateUpdate(strmap);
        oppD2L.getOpportunityInfo();
        oppD2L.opptyvalidateUpdate(strmap);
        oppD2L.validateUpdateSingleOppty(opliId ,'OpportunityLineItem',isSalesUser);
        oppD2L.validateUpdateSingleOppty(evId,'Event',isSalesUser);
        oppD2L.validateUpdateSingleOppty(tkId ,'Task',isSalesUser);
        oppD2L.validateUpdateSingleOppty(opliId ,'OpportunityLineItem',issalesuser1);
        oppD2L.opptyeventinitialize(evlist,newEvents,oldEvents,'Insert');
        oppD2L.opptyvalidateUpdate(strmap);
        oppD2L.opptytaskinitialize(tklist,newTasks,oldTasks,'Insert');
        oppD2L.opptyvalidateUpdate(strmap);
        oppD2L.getOpportunityInfo();
        oppD2L.opptyvalidateUpdate(strmap);
        oppD2L.validateUpdateSingleOppty(k,'Task',isSalesUser);
        oppD2L.validateUpdateSingleOppty(k,'Event',isSalesUser);
        
        Test.StopTest();
        
    }
    
}
}