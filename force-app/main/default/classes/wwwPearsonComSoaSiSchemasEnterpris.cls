//Generated by wsdl2apex

public class wwwPearsonComSoaSiSchemasEnterpris {
    public class EnterpriseHeader {
        public String HeaderVersion;
        public wwwPearsonComSoaSiSchemasEnterpris.Client Initiator;
        public wwwPearsonComSoaSiSchemasEnterpris.Service Service;
        public wwwPearsonComSoaSiSchemasEnterpris.Client Client;
        public wwwPearsonComSoaSiSchemasEnterpris.Event Event;
        private String[] HeaderVersion_type_info = new String[]{'HeaderVersion','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'1','1','false'};
        private String[] Initiator_type_info = new String[]{'Initiator','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'1','1','false'};
        private String[] Service_type_info = new String[]{'Service','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'0','1','false'};
        private String[] Client_type_info = new String[]{'Client','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'0','1','false'};
        private String[] Event_type_info = new String[]{'Event','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.pearson.com/soa/si/schemas/enterpriseheader/1','true','false'};
        private String[] field_order_type_info = new String[]{'HeaderVersion','Initiator','Service','Client','Event'};
    }
    public class Service {
        public String Version;
        private String[] Version_type_info = new String[]{'Version','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.pearson.com/soa/si/schemas/enterpriseheader/1','true','false'};
        private String[] field_order_type_info = new String[]{'Version'};
    }
    public class Event {
        public String EventName;
        public String SubEventName;
        public String BusinessGeography;
        public String Market;
        public String AccountType;
        private String[] EventName_type_info = new String[]{'EventName','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'0','1','false'};
        private String[] SubEventName_type_info = new String[]{'SubEventName','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'0','1','false'};
        private String[] BusinessGeography_type_info = new String[]{'BusinessGeography','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'0','1','false'};
        private String[] Market_type_info = new String[]{'Market','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'0','1','false'};
        private String[] AccountType_type_info = new String[]{'AccountType','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.pearson.com/soa/si/schemas/enterpriseheader/1','true','false'};
        private String[] field_order_type_info = new String[]{'EventName','SubEventName','BusinessGeography','Market','AccountType'};
    }
    public class Client {
        public String ApplicationID;
        public String CorrelationID;
        public DateTime DispatchTimestamp;
        private String[] ApplicationID_type_info = new String[]{'ApplicationID','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'1','1','false'};
        private String[] CorrelationID_type_info = new String[]{'CorrelationID','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'0','1','false'};
        private String[] DispatchTimestamp_type_info = new String[]{'DispatchTimestamp','http://www.pearson.com/soa/si/schemas/enterpriseheader/1',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.pearson.com/soa/si/schemas/enterpriseheader/1','true','false'};
        private String[] field_order_type_info = new String[]{'ApplicationID','CorrelationID','DispatchTimestamp'};
    }
}