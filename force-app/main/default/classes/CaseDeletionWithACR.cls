/* -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            CaseDeletionWithACR.cls 
Description:     Batch Apex For Pre-Call Form Closed Case Records Deletion and Related Contact & ACR Record Deletion as well.

Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ------------------------------------------------------------------------------------------------------------------------------------
May 08 2019       1.0                priya                For CR-01780 : Batch Apex For Pre-Call Form Closed Case Records Deletion and Related Contact & ACR Record Deletion as well.                                                    
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class CaseDeletionWithACR implements Database.Batchable<SObject> , Database.Stateful {
    transient List<Database.DeleteResult> drAccList;
    transient List<Database.DeleteResult> drContactList;
    transient List<Database.DeleteResult> drCaseList;
    Static Map<Id,String> accErrorMsgMap = new Map<Id,String>();
    Static Map<Id,String> contactErrorMsgMap = new Map<Id,String>();
    Static Map<Id,String> caseErrorMsgMap = new Map<Id,String>();
    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        List<CaseACRDeletionParameter__mdt> casedeletionmdt =[SELECT Id, Days__c, Owner_Queue_Name__c FROM CaseACRDeletionParameter__mdt limit 1];
        String caseOwner;
        Integer days;
        String status ='Closed';
        for(CaseACRDeletionParameter__mdt cd :casedeletionmdt){
            caseOwner= cd.Owner_Queue_Name__c;
            days=integer.valueOf(cd.Days__c);
            
        }
        String query= 'SELECT Id, ContactId, AccountId FROM Case where Owner.Name ='+'\''+ caseOwner + '\'' + 'AND Status = '+'\''+ status + '\'' + 'AND CreatedDate <= LAST_N_DAYS:'+ days;
        if(Test.isRunningTest()){
            return Database.getQueryLocator([SELECT Id, ContactId, AccountId FROM Case where CreatedDate = Today]);
        }
        else{
            // return Database.getQueryLocator(query);
            return Database.getQueryLocator([SELECT Id, ContactId, AccountId FROM Case where Owner.Name ='Pre-Call Form Queue'AND ID IN('5000E00000A0jKN')]);
            //return Database.getQueryLocator([SELECT Id, ContactId, AccountId FROM Case where Owner.Name ='Pre-Call Form Queue'AND Status = 'Closed'AND CreatedDate <= LAST_N_DAYS:30 limit 100 ]);
        }
    }
    
    public void execute(Database.BatchableContext bc, List<Case> recordsToProcess){
        Set<Id> setAccountId = new Set<Id>(); 
        
        for(Case cs:recordsToProcess) 
        {
            setAccountId.add(cs.AccountId);
        }
        drCaseList = Database.delete(recordsToProcess, false);
        
        Id acrRecordTypeId = Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
        
        List<Account> deleteAcct = new List<Account>();
        Set<Id> contactIds = new Set<Id>();
        
        List <Account> accDetails = new List<Account>();
        for(Account acct : [Select Id,(Select Id from Contacts) from Account 
                            where Id IN :setAccountId and RecordTypeId =:acrRecordTypeId]) 
        {       
            for(Contact con:acct.Contacts){
                contactIds.add(con.Id);                   
            }
        }
        
        
        List <Case> caseList =[Select Id,ContactId from Case where ContactId IN:contactIds];
        List <OpportunityContactRole> opptyList =[SELECT Id, ContactId FROM OpportunityContactRole where ContactId IN :contactIds ];
        
        Set<Id> delConIds = new Set<Id>();
        Map<Id, Case> contactIdCaseMap = new Map<Id, Case>();
        Map<Id, OpportunityContactRole> contactIdopptyMap = new Map<Id, OpportunityContactRole>();
        for(Case c : caseList){
            contactIdCaseMap.put(c.ContactId, c);
        }
        for(OpportunityContactRole opp : opptyList){
            contactIdopptyMap.put(opp.ContactId, opp);
        }
        
        for(Id conId : contactIds){
            if(contactIdCaseMap.get(conId) == null && contactIdopptyMap.get(conId) == null){
                delConIds.add(conId);    
            }
        }
        
        List<Contact> deleteCont = [Select Id from Contact where Id IN :delConIds];
        
        if(deleteCont.size()>0){
            drContactList= Database.delete(deleteCont, false); 
        }
        for(Account acct : [Select Id,ERP_ID__c,(Select Id from Cases), (Select Id from Contacts), (Select Id from Opportunities), (Select Id from ActivityHistories) ,(Select Id from Leads__r) from Account 
                            where Id IN :setAccountId and RecordTypeId =:acrRecordTypeId]) 
        {
            if(String.isBlank(acct.ERP_ID__c) && acct.Cases.isEmpty() && acct.Opportunities.isEmpty() && acct.ActivityHistories.isEmpty() && acct.Contacts.isEmpty() && acct.Leads__r.isEmpty()){
                deleteAcct.add(acct);
            }
        }
        if(deleteAcct.size()>0){
            drAccList= Database.delete(deleteAcct, false);
        }
        
        // List for account to retrieve the failed account Id's and put into the map
        if(drAccList!=null && !drAccList.isEmpty()){ 
            for(integer i =0;i<drAccList.size();i++){
                if(!drAccList.get(i).isSuccess()){
                    Database.Error error = drAccList.get(i).getErrors().get(0);
                    String failedDML = error.getMessage();
                    accErrorMsgMap.put(drAccList.get(i).Id, failedDML);
                }
            }
        }
        
        // List for Contact to retrieve the failed Contact Id's and put into the map
        if(drContactList!=null && !drContactList.isEmpty()){ 
            for(integer i =0;i<drContactList.size();i++){
                if(!drContactList.get(i).isSuccess()){
                    Database.Error error = drContactList.get(i).getErrors().get(0);
                    String failedDML = error.getMessage();
                    contactErrorMsgMap.put(drContactList.get(i).Id, failedDML);
                }
            }
        }
        
        // List for Cases to retrieve the failed case Id's and put into the map
        if(drCaseList!=null && !drCaseList.isEmpty()){ 
            for(integer i =0;i<drCaseList.size();i++){
                if(!drCaseList .get(i).isSuccess()){
                    Database.Error error = drCaseList.get(i).getErrors().get(0);
                    String failedDML = error.getMessage();
                    caseErrorMsgMap.put(drCaseList.get(i).Id, failedDML);
                }
            }
        }
    }
    
    public void finish(Database.BatchableContext bc){
        
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email, ExtendedStatus
                          from AsyncApexJob where Id = :bc.getJobId()];      
        if(a.Status == 'Completed') {
            List<Messaging.SingleEmailMessage> mmails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<Id> userIds = new List<Id>();
            userIds.add('005b0000002ffLB');
            mail.setToAddresses(userIds);
            mail.setSenderDisplayName('Batch Processing');
            mail.setSubject('Cases With ACR Deletion status: ' + a.Status);
            String actErrormsg= '';
            String contErrormsg= '';
            String caseErrormsg= '';
            //retrive the failed account Id and send in email
            for(Id accId : accErrorMsgMap.keySet()){
                actErrormsg = 'Failed Error record Ids ' + accErrorMsgMap.get(accId);  
            }
            //retrive the failed Contact Id and send in email
            for(Id accId : contactErrorMsgMap.keySet()){
                contErrormsg = 'Failed Error record Ids ' + contactErrorMsgMap.get(accId);  
            }
            //retrive the failed Case Id and send in email
            for(Id accId : caseErrorMsgMap.keySet()){
                caseErrormsg = 'Failed Error record Ids ' + caseErrorMsgMap.get(accId);  
            }
            mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
                                  ' with the following error'+ a.NumberOfErrors + ' Account record Ids failed ' + actErrormsg + 
                                  ' Contact record Ids failed ' + contErrormsg  + ' Case record Ids failed ' + caseErrormsg );
            mmails.add(mail); 
            list<Messaging.SendEmailResult> results1 =  Messaging.sendEmail(mmails);
            
        }
    }
}