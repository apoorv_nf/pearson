/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:         PS_contactidvalidationtest.cls 
   Description:  Test Class For  To check valid identification number on contact
                
   Date             Version     Author                  Tag     Summary of Changes 
   -----------      -------     -----------------       ---     ------------------------------------------------------------------------
   Karthik.A.S        1.0         21/01/2016              To check valid identification number on contact
  
  -------------------------------------------------------------------------------------------------------------------------------------------------------- */
@isTest
private class PS_contactidvalidationtest
{  
  static testMethod void contactidvalidationtest() 
  {
    List<contact> testcontact = TestDataFactory.createContact(1); 
         
    contact testcontact1 = testcontact[0];
    
    testcontact1.National_Identity_Number__c='112124124';
    try{
    insert testcontact1;
    
    }
    catch(exception e){}
        
    
    
    }
    static testMethod void contactidvalidationtest1() 
   {
    List<contact> testcontact = TestDataFactory.createContact(1); 
         
    contact testcontact1 = testcontact[0];
    
    testcontact1.National_Identity_Number__c='00000';
   try{
    insert testcontact1;
    }
     catch(exception e){}
    
    }
    }