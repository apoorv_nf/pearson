/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            CallFlowforUKCaseOwnerAssignment.cls 
   Description:     R6: SUS96: Call Flow to update case owner
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
    12/7/2016         1.0            Payal Popat                       Initial Release 
-------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
public without sharing class CallFlowforUKCaseOwnerAssignment{
    /**
    * Description : Method to Call Flow to update Case Owner based on account Manager criteria
    * @param List<Case> newCases
    * @return NA
    * @throws NA 
    **/
    public static void mInitiateFlow(List<Case> newCases,Boolean isInsert,Boolean bIsOwnerChanged){
        try{
            system.debug('Inside Try=======');
            Case oCase = [Select Id,Origin,OwnerId,RecordTypeId,AccountId,SuppliedEmail,Account.Market2__c,Subject,Market__c,To_Email_origin_Address__c,Cc_Email_Origin_Address__c  from Case where Id =: newCases[0].id Limit 1];
            
            Map<String,Object> mapCase = new Map<String,Object>();
            mapCase.put('objInputCase',oCase);
            mapCase.put('AccountMarket',oCase.Account.Market2__c);
            mapCase.put('isInsert',isInsert);
            mapCase.put('IsOwnerChanged',bIsOwnerChanged);
            system.debug('test 1 : '+oCase.Account.Market2__c); 
            system.debug('test 2 : '+bIsOwnerChanged); 
            system.debug('test 3 : '+oCase.Market__c); 
            system.debug('mapCase:'+mapCase);
            
            system.debug('oCase.To_Email_origin_Address__c:'+oCase.To_Email_origin_Address__c);
            system.debug('oCase.Cc_Email_Origin_Address__c  :'+oCase.Cc_Email_Origin_Address__c  );
            
            Flow.Interview.CaseAssignmentUK initiateFlow = new Flow.Interview.CaseAssignmentUK(mapCase);
            initiateFlow.start();
           
              system.debug('checking the value of market: '+oCase.Market__c); 
            String returnValue = (String) initiateFlow.getVariableValue('Output');
            system.debug('Inside Try=======1'+returnValue);
            
            system.debug('@@@@$$$$oCase.To_Email_origin_Address__c:'+oCase.To_Email_origin_Address__c);
            system.debug('@@@@$$$$oCase.Cc_Email_Origin_Address__c  :'+oCase.Cc_Email_Origin_Address__c  );
             
        }catch(Exception e){
            system.debug('Inside Try=======2');
            throw e;
        }
    }
    
}