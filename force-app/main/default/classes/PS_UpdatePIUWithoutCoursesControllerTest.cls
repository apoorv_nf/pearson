/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_UpdatePIUWithoutCoursesControllerTest
   Description:     Test class for PS_UpdatePIUWithoutCoursesController
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   18 Nov 2015      1.0           Accenture NDC                                         Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- 
     *  Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 33,40) 
*/
 
@isTest
public class PS_UpdatePIUWithoutCoursesControllerTest
{
    static testmethod void testWithoutCoursesWithoutOrder()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Contact> lstWithContact = new List<Contact>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        List<Product2> lstWithProduct = new List<Product2>();
        List<Contact_Product_In_Use__c> lstWithContactProductInUse = new List<Contact_Product_In_Use__c>();
        Contact_Product_In_Use__c newContactProductInUse = new Contact_Product_In_Use__c();
        List<Asset> lstWithAsset = new List<Asset>();
        lstWithContact = TestDataFactory.createContact(1);
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');  
        lstWithProduct = TestDataFactory.createProduct(2);
        if(lstWithProduct.size() > 0)
        {
            for(Product2 newProduct : lstWithProduct)
            {
                newProduct.Business_Unit__c = 'CTIPIHE';
            }
        }
        if(lstWithTestUser.size() > 0)
        {
            for(User newUser : lstWithTestUser)
            {
                newUser.Product_Business_Unit__c = 'CTIPIHE';
            }
        } 
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Indirect';
            lstWithOppty[0].Selling_Period__c = 'Fall';
            lstWithOppty[0].Selling_Period_Year__c = '2020';
            lstWithOppty[0].StageName = 'Awarded';
        }
        if(lstWithTestUser.size() > 0)
        {
            lstWithTestUser[0].Market__c = 'US';
        }
        test.startTest();
        insert lstWithAccount;
        insert lstWithTestUser;
        insert lstWithContact;
        insert lstWithProduct;
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].AccountId = lstWithAccount[0].Id;
            insert lstWithOppty;
        } 
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
            ID ContId;
            lstWithAsset = TestDataFactory.insertAsset(1,ContId,ContId,lstWithProduct[0].Id,lstWithAccount[0].Id);
            if(lstWithAsset.size() > 0)
            {
                insert lstWithAsset;
            }
            newContactProductInUse.Product_in_Use__c = lstWithAsset[0].Id;
            newContactProductInUse.Contact__c = lstWithContact[0].Id;
            lstWithContactProductInUse.add(newContactProductInUse);
            insert lstWithContactProductInUse;

          //Test the class in 'PS_UpdatePIUWithoutCourses' page context
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_UpdatePIUWithoutCourses?accountId='+lstWithOppty[0].AccountId+'&opportunityId='+lstWithOppty[0].Id+'&orderId='+lstWithOppty[0].Id);
              Test.setCurrentPage(pageRef);
              PS_UpdatePIUWithoutCoursesController controllerObj = new PS_UpdatePIUWithoutCoursesController(); 
              controllerObj.getAssetDetails();
              controllerObj.getStatus();
              controllerObj.getUsage();
              controllerObj.getModeOfDelivery();
              controllerObj.getThirdPartyLMS();
              controllerObj.getChannel();
              controllerObj.Beginning();
              controllerObj.Previous();
              controllerObj.Next();
              controllerObj.End();
              controllerObj.getDisablePrevious();
              controllerObj.getDisableNext();
              controllerObj.getTotal_size();
              controllerObj.getPageNumber();
              controllerObj.getTotalPages();
              controllerObj.cancel();
              if(lstWithAsset.size() > 0)
              {
                  controllerObj.selectedAssetId = lstWithAsset[0].Id;
              }
              controllerObj.createNewWrapperRecord();
              controllerObj.lstWithWraperRec[0].innerAssetID = lstWithAsset[0].Id;
              controllerObj.createNewWrapperRecord();
              controllerObj.updatePIU();
          }
        }      
    }
    static testmethod void testWithoutCoursesWithOrder()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');  
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Indirect';
            lstWithOppty[0].Selling_Period__c = 'Fall';
            lstWithOppty[0].Selling_Period_Year__c = '2020';
            lstWithOppty[0].StageName = 'Awarded';
        }
        test.startTest();
            insert lstWithTestUser;
            insert lstWithAccount;
            if(lstWithOppty.size() > 0)
            {
                lstWithOppty[0].AccountId = lstWithAccount[0].Id;
                insert lstWithOppty;
            } 
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
          //Test the class in 'PS_UpdatePIUWithoutCourses' page context
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_UpdatePIUWithoutCourses?accountId='+lstWithOppty[0].AccountId+'&orderId='+lstWithOppty[0].Id);
              Test.setCurrentPage(pageRef);
              PS_UpdatePIUWithoutCoursesController controllerObj = new PS_UpdatePIUWithoutCoursesController();
              controllerObj.updatePIU();
          }
        }      
    }   
}