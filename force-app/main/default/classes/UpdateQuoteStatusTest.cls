@isTest(SeeAllData = False)
public class UpdateQuoteStatusTest{
  
    static testMethod void myTest() {
      
      
       Account acc = new Account();
          acc.Name = 'CTI Bedfordview Campus';
          acc.Line_of_Business__c= 'Higher Ed';
          acc.Geography__c = 'North America';
          acc.Market2__c = 'US';
          acc.Pearson_Campus__c=true;
          acc.Pearson_Account_Number__c='Campus';
          insert acc;
          
          Opportunity opp = new opportunity();
          opp.AccountId = acc.id;    
          opp.Name = 'OppTest';
          opp.StageName = 'Negotiation';
          opp.CloseDate = system.today();
          opp.Type='NEW BUSINESS';
          opp.Earlybird_Payment_Reference__c= TRUE;
          opp.CreatedDate=system.today();
          //opp.Primary_Contact__c=ci.Id;
          insert opp;
      Test.StartTest();  
        Product2 prod = new Product2(Name = 'Bachelor of Commerce', 
                                        PIHE_Repeater__c='true',
                                        Semester__c ='First Semester',
                                        PIHE_Conferer__c='PIHE',
                                        PIHE_Qualification_Year__c='1',
                                        PIHE_Qualification__c='Pre-degree Foundation Programme (Social Sciences)',
                                        Module_Type__c='Core',
                                        Module_Code__c='FPIP021',
                                        Configuration_Type__c='Option',
                                        Auto_Selected__c=true,                                                                                 
                                        Family = 'Best Practices', IsActive = true);
       insert prod;
       
       Id standardPBId = Test.getStandardPricebookId();
       PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, 
                                                        Product2Id = prod.Id,
                                                        UnitPrice = 10000, 
                                                        IsActive = true);
       insert standardPBE;
          
       List<Quote> Lstquo = new List<Quote>();
       List<Id> LstquoId = new List<Id>();
       Quote quo = new Quote();
       quo.Name= 'Test Quote';   
       quo.First_Payment_Date__c=System.today();
       quo.Payment_Period_In_Month__c='3';
       quo.Deposit__c=1000.00;
       quo.Payment_Type__c='Monthly Payment';
       quo.Opportunityid=opp.id;
       quo.Pricebook2Id=standardPBId;
       quo.Registration_Fee__c=800;
       quo.Total_Early_Bird_Securing_Fee_Payments__c=1700;
       //quo.Market__c ='Uk';
       Lstquo.add(quo);
       insert Lstquo;
       
       /*
       Map<id,Quote> Qutmap = new Map<id,Quote>();
       Quote quo1 = new Quote();
       quo1.Name= 'Test Quote';   
       quo1.First_Payment_Date__c=System.today();
       quo1.Payment_Period_In_Month__c='3';
       quo1.Deposit__c=1000.00;
       quo1.Payment_Type__c='Monthly Payment';
       quo1.Opportunityid=opp.id;
       quo1.Pricebook2Id=standardPBId;
       quo1.Registration_Fee__c=800;
       quo1.Total_Early_Bird_Securing_Fee_Payments__c=1700;
       insert quo1;
       //quo.Market__c ='Uk';
       Qutmap.put(quo1.id, quo1);
       */
       
       
       for(Quote qt:Lstquo) {
         LstquoId.add(qt.Id);
       }
       
       QuoteLineItem qLI = new QuoteLineItem();
       qLI.QuoteId = Lstquo[0].Id;
       qLI.Promo_Discount_Code__c ='50PD2016';
       qLI.PriceBookEntryID=standardPBE.id;
       //System.assertEquals('dfsafsd',standardPBE.id);
       qLI.Product2Id=prod.Id;
       //System.assertEquals('dfsafsd',prod.Id);
       qLI.Quantity=4; 
       qLI.UnitPrice =50;  
       insert qLI;
       
       Lstquo[0].Manual_Discount__c = 10;
       update Lstquo;
       UpdateQuoteStatus.updateStatus(LstquoId);
       
       Lstquo[0].Manual_Discount__c = NULL;
       Lstquo[0].Status = 'Approved';
       update Lstquo;
       UpdateQuoteStatus.updateStatus(LstquoId);
      Test.StopTest(); 
    }
}