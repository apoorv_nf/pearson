/**
 * Name : PS_BatchUpdateCoursesFrontlists_CA_Test 
 * Author : MAYANK LAL
 * Description : Test class used for testing the PS_BatchUpdateCoursesFrontlists_CA
 * Date : 06/01/17 
 * Version : <intial Draft> 
 */
@isTest
public class PS_BatchUpdateCoursesFrontlists_CA_Test {

    static testmethod void testBatchUpdateCourses()
    { 
        List<Account> accList = TestDataFactory.createAccount(2,'Direct Corporate');
        insert accList;
        
        UniversityCourse__c unvobject = new UniversityCourse__c(); 
        unvobject.Account__c = accList[0].Id;
        unvobject.Active__c = true;
        unvobject.Name = 'Test Univ course Name';
        unvobject.Adoption_Type__c = 'committee';
        unvobject.Course_Name__c = 'Test course Name';
        unvobject.Fall_Enrollment__c = 121;
        unvobject.Spring_Enrollment__c = 110;
        unvobject.Winter_Enrollment__c = 415;
        unvobject.Fall_Front_List__c= true;
        unvobject.Spring_Front_List__c= true;
        integer iMonth = system.today().month() +1;
               // unvobject.  = Course
      // unvobject.
          insert unvobject;
        List<UniversityCourse__c> lstUnivcourse = new List<UniversityCourse__c>();
       // lstUnivcourse.add(unvobject);
       // lstUnivcourse.clear();
        ProductCategory__c category = new ProductCategory__c(Name='testcategoryy',HierarchyLabel__c='testlabele');
        insert category; 
        Hierarchy__c csa =new Hierarchy__c(name ='estt',ProductCategory__c=category.id,Label__c='testlabel');
        csa.Market__c = 'CA';
        csa.Type__c ='Course';
        csa.CurrentPeriod__c = true;
        csa.PriorPeriod__c = true;
        //csa.CreatedDate =system.today().addDays(-4);
        //csa.LastModifiedDate = system.today().addDays(-2);
        insert csa;           
        List<Pearson_Course_Equivalent__c>  pcourseList = new List<Pearson_Course_Equivalent__c>();
        Pearson_Course_Equivalent__c  pcourseObject = new Pearson_Course_Equivalent__c();
        pcourseObject.Course__c  =unvobject.Id;
        pcourseObject.Active__c = true;  
        pcourseObject.Primary__c =true;
        pcourseObject.Pearson_Course_Code_Hierarchy__c = csa.Id;
        pcourseList.add(pcourseObject);
        
        Pearson_Course_Equivalent__c  pcourseObject1 = new Pearson_Course_Equivalent__c();
        pcourseObject1.Course__c  =unvobject.Id;
        pcourseObject1.Active__c = true;  
        pcourseObject1.Primary__c =true;
        pcourseObject1.Pearson_Course_Code_Hierarchy__c = csa.Id;
        pcourseList.add(pcourseObject1);
        
        Pearson_Course_Equivalent__c  pcourseObject2 = new Pearson_Course_Equivalent__c();
        pcourseObject2.Course__c  =unvobject.Id;
        pcourseObject2.Active__c = true;  
        pcourseObject2.Primary__c =true;
        pcourseObject2.Pearson_Course_Code_Hierarchy__c = csa.Id;
        pcourseList.add(pcourseObject2);
        
        insert pcourseList;
        System.debug('pcourseList--->'+pcourseList);
        
     PS_BatchUpdateCoursesFrontlists_CA batchObject = new PS_BatchUpdateCoursesFrontlists_CA('CA',unvobject.Id, true); 
     PS_BatchUpdateCoursesFrontlists_CA batchObject1 = new PS_BatchUpdateCoursesFrontlists_CA('CA',unvobject.Id, false); 
      Test.startTest();  
      database.executeBatch(batchObject) ; 
        database.executeBatch(batchObject1) ; 
   //  system.schedule();
     SchedulableContext sc = null;
     // tsc.execute(sc);
     batchObject.execute(sc);
         batchObject1.execute(sc);
        Test.stopTest();
     
    }
    
}