@isTest(SeeAllData = false)                   
public class RepaymentCalculatorUpdateTest{
     static testMethod void myTest() { 
          //PS_Market__c mr=new PS_Market__c();
        // mr.Market__c='AU';
        // mr.Name='AU';
       //  insert mr;
          Account acc = new Account();
          acc.Name = 'CTI Bedfordview Campus';
          acc.Line_of_Business__c= 'Higher Ed';
          acc.Geography__c = 'Growth';
          acc.Market2__c = 'ZA';
          acc.Pearson_Campus__c=true;
          acc.Pearson_Account_Number__c='Campus';
          insert acc;
          
          /*Product2 prod = new Product2(Name = 'Anti-infectives 2007',PIHE_Repeater__c='true', Family = 'Best Practices', IsActive = true);

          Id standardPBId = Test.getStandardPricebookId();

          PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
          insert standardPBE;
          Pricebook2 customPB1 = new Pricebook2(Name='Standard Price Book', isActive=true);
          insert customPB1;
          PricebookEntry customPBE1 = new PricebookEntry(Pricebook2Id = customPB1.Id, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
          insert customPBE1;*/
          
           Id standardPBId = Test.getStandardPricebookId();
            //List<Product2> prodList= TestDataFactory.createProduct(2);
            Product2 prod = new Product2(Name = 'Anti-infectives 2007',PIHE_Repeater__c='true',PIHE_Qualification_Year__c='1', Family = 'Best Practices', IsActive = true);
            insert prod;
            PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
            insert standardPBE;
            Pricebook2 customPB1 = new Pricebook2(Name='Standard Price Book', isActive=true);
            insert customPB1;
            PricebookEntry customPBE1 = new PricebookEntry(Pricebook2Id = customPB1.Id, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
            insert customPBE1;

            Pricebook2 customPB2 = new Pricebook2(Name='UK HE All Products Price Book', isActive=true);
            insert customPB2;
      
            PS_CreatePriceBookEntriesBatch b = new PS_CreatePriceBookEntriesBatch();
            database.executebatch(b);
            
          Opportunity opp = new opportunity();
          opp.AccountId = acc.id;    
          opp.Name = 'OppTest';
          //opp.StageName = 'Qualification';
          opp.StageName= 'Proposal/Quote';
          opp.CloseDate = system.today();
          opp.Pricebook2id=customPB1.id;
         opp.Market__c='ZA';
          insert opp;
          Quote_Settings__c qs = new Quote_Settings__c ();
          qs.Name='System Properties';
          qs.Academic_Start_Date_Term_1__c=system.today();
          qs.Academic_Start_Date_Term_2__c=system.today()+14;
          qs.Academic_Start_Date_Term_3__c=system.today()+1;
          qs.Academic_Start_Date_Term_4__c=system.today()+2;
          qs.Default_Registration_Fee_New_Business__c=800;
          qs.Default_Registration_Fee_ReturnBusiness__c=0;
          qs.Earlybird_Securing_Fee_New_Business__c=1700;
          qs.Earlybird_Securing_Fee_Return_Business__c=800;
          qs.Early_Bird__c=system.today()+4;
          qs.Early_Bird_Price_List_Id__c='a2s6E0000007VKo';
          qs.Non_Early_Bird_Price_List_Id__c='a2s6E0000007VKt';
          qs.Administration_Fee_Rate__c=17;
          qs.PriceBook_Actual__c='01s250000003DsC';
          qs.PriceBook_Discounted__c='01s250000003DsH';
          insert qs;
          
          Quote quote = new quote();
          quote.First_Payment_Date__c=System.today();
          quote.Name='Test Quote23';
          quote.Payment_Period_In_Month__c='3';
          quote.Deposit__c=1000.00;
          quote.Payment_Type__c='Monthly Payment';
          quote.Opportunityid=opp.id;
          quote.Pricebook2Id=Opp.Pricebook2Id;
          quote.Market__c='ZA';
          insert quote;
          
          Test.StartTest();
          PageReference pageRef = Page.CreateStandardQuote; //replace with your VF page name
          pageRef.getParameters().put('oppid', opp.id);
          Test.setCurrentPage(pageRef);
          ApexPages.StandardController stdQuote = new ApexPages.StandardController(quote);
        CreateStandardQuote controller = new CreateStandardQuote(stdQuote);
         controller.Quote_create();
          Quote quo = new Quote();
          quo = [SELECT ID,Course_Fee_Rollup__c,Payment_Period_In_Month__c,First_Payment_Date__c From Quote where Id = :quote.Id];
          //quo.Course_Fee_Rollup__c;
          quo.First_Payment_Date__c=System.today();
          quo.Name='Test Quote1';
          quo.Payment_Period_In_Month__c='3';
          quo.Deposit__c=1000.00;
         quo.Securing_Fee_Discount__c='5';
         quo.Bursary_Amount__c=100;
          quo.Payment_Type__c='Monthly Payment';
          quo.Opportunityid=opp.id;
          quo.Pricebook2Id=Opp.Pricebook2Id;
          quo.Market__c='ZA';
          //insert quo;
          
          update quo;
          List<Quote> quoList = new List<Quote>();
          quoList.add(quo);
          
        /* Repayment_Calculator__c rc = new Repayment_Calculator__c();
          rc.Quote__c =quo.Id;
          rc.Payment_Date__c=System.today()-1;
          rc.Total_Payment__c=1000.00;
          rc.Beginning_Balance__c=100;
          rc.Discount_Reversed__c=0.1;
          rc.Principal__c=10000.00;
          rc.Ending_Balance__c=500.00;
          rc.Scheduled_Payment__c=1000;
          rc.Interest_Rate__c=17;
          
          rc.Sequence_No__c=5;
          
          
          insert rc;*/
          //String s = quo.Id;
          
          QuoteLineItem qliliner = new QuoteLineItem(Product2Id=prod.id,Outside_Module__c=true,QuoteId=quo.id,PriceBookEntryID=customPBE1.id,Quantity=4, UnitPrice =50);
          insert qliliner;
          
          qliliner.Outside_Module__c=False;
          update qliliner;
          /*QuoteLineItem qlitrim = new QuoteLineItem(Product2Id=prod.id,Outside_Module__c=false,QuoteId=quo.id,PriceBookEntryID=customPBE1 .id,Quantity=4, UnitPrice =50);
          insert qlitrim;

          QuoteLineItem qliacc = new QuoteLineItem(Product2Id=prod.id,Outside_Module__c=true,QuoteId=quo.id,PriceBookEntryID=customPBE1 .id,Quantity=4, UnitPrice =50);
          insert qliacc;*/
          

          
          /*QuoteLineItem qli = new QuoteLineItem();
          qli.UnitPrice=200;
          qli.Quantity=3;
          qli.PricebookEntryId=pbk1.Id;
          qli.Product2Id=p.Id;
          
          qli.QuoteId=quo.id;
          insert qli;*/
          
          //delete qliliner;
          RepaymentCalculatorUpdate.updateRepaymentCalculator(quoList);
          Test.StopTest();
          
     }
}