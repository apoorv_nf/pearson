public class CampaignMemberTriggerHandler {

    public void OnAfterInsert(CampaignMember[] afterInsertlstNewCampMemb)
    {
        updateCampaignStatusandNumberofOrderFields(afterInsertlstNewCampMemb);
    }
    public void OnAfterUpdate(CampaignMember[] afterupdatelstNewCampMemb)
    {
        updateCampaignStatusandNumberofOrderFields(afterupdatelstNewCampMemb);
    }
    public void OnAfterDelete(CampaignMember[] afterdeletelstNewCampMemb)
    {
        updateCampaignStatusandNumberofOrderFields(afterdeletelstNewCampMemb);
    }
    public void updateCampaignStatusandNumberofOrderFields(CampaignMember[] lstNewCampMemb)
    {
        set<id> campids = new Set<id>();
        system.debug('lstNewCampMemb'+lstNewCampMemb);
        for(CampaignMember campmemb : lstNewCampMemb){
            	  campids.add(campmemb.CampaignId);
        }
        system.debug('number of campaigns'+ campids);
        
        Map<string,Integer> mapCampaigncount = new Map<string,Integer>();
		AggregateResult[] groupedResults = [select count(id),campaignid from campaignmember where campaignid IN : campids and Order_Status__c='sent' group by campaignid];       
        for(AggregateResult gr : groupedResults){
            mapCampaigncount.put((String)gr.get('campaignid'),(Integer)gr.get('expr0'));
        }
        /*system.debug('campaign map'+mapofCampaign);
        
        for(Campaign cmp : Campaign){
            system.debug('number of order creted'+ mapofCampaign.get(cmp.Id).size());
            cmp.No_of_Orders_Created__c = String.Valueof(mapofCampaign.get(cmp.Id).size());
            cmp.status = 'Sent';
            campaignsToUpdate.add(cmp);
        }*/
        List<Campaign> campaignsToUpdate = new List<Campaign>();
        for(String cmpid : mapCampaigncount.keyset()){
            Campaign cmp = new Campaign();
            cmp.Id = cmpid;
            system.debug('String.valueof(mapCampaigncount.get(cmpid))'+String.valueof(mapCampaigncount.get(cmpid)));
            cmp.No_of_Orders_Created__c = String.valueof(mapCampaigncount.get(cmpid));
            cmp.status = 'Sent';
            campaignsToUpdate.add(cmp);
        }
        if(campaignsToUpdate.size()>0){
            update campaignsToUpdate;
        }
    }
}