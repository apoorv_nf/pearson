/************************************************************************************************************
* Apex Class Name   : OrderApproval.cls
* Version           : 1.1 
* Created Date      : 26 May 2015
* Function          : Class for the updatation of order status.
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Rony Joseph                 3/11/2015               Updation for the Defect related to New Status.
* Karan Khanna                7/12/2015               Added a market check for US specific functionality. Updated submitApprovalChatterPost() and submitOrder()
* Rony Joseph                 7/03/2016               Updated error log generater to display accurate information.
* Rony Joseph                 3/05/2016               Updated code for order status for CA market using custom label for CA market.
* BoopathyRaja Elangovan      12/28/2016              Updated the code in submit order method for Cancelled Status for CR:00955
* Asha G                      11/04/2018              Updated the code in submit order method to  set the order and order line status Pending Approval and requires for Approval based upon sampling restriction value for CR-2023 Req 3 and 5
*******************************************************************************************************************/

public class OrderApproval{
    
    private static User currentUrser;
    private Map <Id,Order> newOrders;
    private Map <Id,Order> oldOrders;
    public list<id> orderitemcountEmptyvalues;
    
    private List<OrderItem> orderItems;
    
    public OrderApproval(Map <Id,Order>newOrders,  Map <Id,Order> oldOrders)
    {
        this.newOrders = newOrders;
        this.oldOrders = oldOrders;
        
        orderItems = [select id,orderid,OrderItemNumber,status__C, Order.Id,Shipped_Product__r.Sampling_Restriction__c,Market__c,Target_System__c  from orderitem where orderid  in :newOrders.keySet() Order By OrderId , OrderItemNumber]; //Added new field in query for CR-2023
    }
    
    public User retrieveCurrentUser()
    {
      try
      {
        if(currentUrser !=null)
        {
            return currentUrser;
        }
        
         currentUrser = [Select id,Name,Manager.id,Sample_Approval__c,Order_Approver__r.id, Order_Approver__r.Name from user where id=:Userinfo.getuserid()];
         
         return currentUrser;
      }catch (exception e){
                           string errormessage=e.getMessage()+e.getStackTraceString();
                           ExceptionFramework.LogException('Order Update','OrderApproval','retrieveCurrentUser',errormessage,UserInfo.getUserName(),'');           
                           return null;
                          }        
    }
    
    private Boolean statusChangedTo(Id newOrderId, String status)
    {
        return (statusChanged(newOrderId) && newOrders.get(newOrderId).Status == status);
    }
    
    private Boolean statusChanged(Id newOrderId)
    {
        return (newOrders.get(newOrderId).Status != oldOrders.get(newOrderId).Status);
    }
    
    private Boolean statusChangedFrom(Id newOrderId,String status)
    {
        return (statusChanged(newOrderId) && oldOrders.get(newOrderId).Status == status);
    }
    
    /**Added new condition for  CR -2023 req 5 to update  order status in existing method starts**/
    public void submitOrder(){
      try{
        User u = retrieveCurrentUser();
        
        List <Order> orderLSt = newOrders.values();
         map<id,list<id>> orderitemcount=new map<id,list<id>>();   
        map<id,list<id>> orderitemEmptycount=new map<id,list<id>>(); 
        map<id,list<id>> orderitemERP=new map<id,list<id>>();   
        map<id,list<id>> orderitemCourseSmart=new map<id,list<id>>();       
        integer i=0,j=0,k=0;            
        list<Orderitem> orderitemproduct=[select id,orderid,OrderItemNumber,status__C,Shipped_Product__r.Sampling_Restriction__c,Order.Id,Target_System__c from orderitem where orderid  in :newOrders.keySet() ];  
        for(orderitem ord:orderitemproduct)
           {
               if(ord.Target_System__c=='ERP' && ord.Shipped_Product__r.Sampling_Restriction__c=='R')
               {
                   orderitemERP.put(ord.orderid,new list<id>{ord.id});
                     if(orderitemcount.containskey(ord.orderid))
                     {
                         list<id> orderitemsinglelsit=orderitemcount.get(ord.orderid);
                         orderitemsinglelsit.add(ord.id);
                         orderitemcount.put(ord.orderid,orderitemsinglelsit);
                     }
                    else
                     {
                         orderitemcount.put(ord.orderid,new list<id>{ord.id});
                     }
                }                    
                if(ord.Target_System__c=='ERP' && ord.Shipped_Product__r.Sampling_Restriction__c==null)
                {
                    orderitemERP.put(ord.orderid,new list<id>{ord.id});
                     orderitemEmptycount.put(ord.orderid,new list<id>{ord.id});
                     if(orderitemEmptycount.containskey(ord.orderid))
                     {
                         list<id> orderitemsingleEmptylsit=orderitemEmptycount.get(ord.orderid);
                         //orderitemsingleEmptylsit.add(ord.id);
                         orderitemEmptycount.put(ord.orderid,orderitemsingleEmptylsit);
                     }
                     else
                     {
                         orderitemEmptycount.put(ord.orderid,new list<id>{ord.id});
                     }
                } 
                if(ord.Target_System__c!='ERP')
                {
                    orderitemCourseSmart.put(ord.orderid,new list<id>{ord.id});
                }                   
           } 
           
           
                             
        if(orderLSt.size()>0){
        for(Order  orderObj : orderLSt)
        {
          // boolean hasERPandCS =FALSE;
           boolean hasERP =FALSE;
           boolean hasCS =FALSE;
            if(orderitemCourseSmart.containskey(orderObj.id)) hasCS =TRUE;
            
            if(orderitemERP.containskey(orderObj.id)) hasERP =TRUE;
            
            Order oldOrder = oldOrders.get(orderObj.Id);
            
            if(orderObj.Market__c == Label.PS_USMarket) 
            {
                       
                if((statusChangedFrom(orderObj.Id, 'New') || orderObj.status== 'New') && !(statusChangedTo(orderObj.Id, 'Cancelled')) ) // Boopathy: Added Order Status Cancelled for CR-00955
                {           
                    list<id>orderitemcountn=orderitemcount.get(orderobj.id);    
                    orderitemcountEmptyvalues = orderitemEmptycount.get(orderobj.id);     
                    if((u.Sample_Approval__c || (orderitemcountn!=null && orderitemcountn.size()>0)) && (hasERP))
                    {                    
                            orderObj.status= 'Pending Approval';  
                    }                                                                   
                    else if(orderitemcountEmptyvalues!=null && orderitemcountEmptyvalues.size()>0)
                    {
                     orderObj.status= 'Open';
                    }
                    else if(hasCS)
                    {
                    orderObj.status= 'Open';    
                    }
                    
                }
            } 
            
           else if(orderObj.Market__c == Label.PS_CAMarket)  
            {
                if(statusChangedFrom(orderObj.Id, 'New') && !(statusChangedTo(orderObj.Id, 'Cancelled'))) // Boopathy: Added Order Status Cancelled for CR-00955
                {
                  /*  if(u.Sample_Approval__c)
                    {
                        orderObj.status= 'Pending Approval';
                                            
                    }else
                    {
                         orderObj.status= 'Open';
                    } */
                    orderObj.status= 'Open';    //Updated for 2023 as orders should not go for approval for CA Market: Asha_17_11_2018
                }
              if(statusChangedFrom(orderObj.Id, 'New') && (statusChangedTo(orderObj.Id, 'Cancelled')))
                {
                orderObj.status='Cancelled';
                } 
            }                  
        }
       }
     }catch(exception e){
                       string errormessage=e.getMessage()+e.getStackTraceString();
                       ExceptionFramework.LogException('Order Update','OrderApproval','submitOrder',errormessage,UserInfo.getUserName(),'');           
                        }     
    }
    /**Added new condition for  CR -2023 req 5 to update  order status in existing method ends**/  

    public void submitApprovalChatterPost()
    {
      try
      {
        List<FeedItem> posts = new List<FeedItem>();

        User u = retrieveCurrentUser();

        List <Order> orderLSt = newOrders.values();
        if(orderLSt.size()>0){
        for(Order  orderObj : orderLSt)
        {
            Order oldOrder = oldOrders.get(orderObj.Id);
            
            if(orderObj.Market__c == Label.PS_USMarket||orderObj.Market__c == 'CA')
            {
                if(statusChangedFrom(orderObj.Id, 'New'))
                {
                    if(u.Sample_Approval__c)
                    {
                        posts.add(chattermessage(orderObj));                                       
                    }
                }
            }
          }
        }
        
        if(posts.size() > 0)
        {
            insert posts;
        }  
      }catch(exception e){
                         string errormessage=e.getMessage()+e.getStackTraceString();
                         ExceptionFramework.LogException('Order Update','OrderApproval','submitApprovalChatterPost',errormessage,UserInfo.getUserName(),'');        
                         } 

    }
        
    public FeedItem chattermessage(Order orderObj)
    {
        try{                                    
        User u = retrieveCurrentUser();
        
        String msg;
                                
        FeedItem post = new FeedItem();
        post.ParentId = u.Order_Approver__r.id; 
        String str = URL.getSalesforceBaseUrl().toExternalForm() + '/' + orderObj.id ;
        msg = 'Dear '+ u.Order_Approver__r.Name+', \n Order ' +orderObj.OrderNumber+'  has been submitted for approval by User  '+u.name + '.Please navigate to the order ('+str+'),remove any order products that are not required and set the Order Status to Open or Cancelled';
        msg += '\n \n Please contact your administrator if you have any questions.';
        msg += '\n \n Thanks,\n \n Pearson OneCRM Admin ';
        post.Body = msg;
        post.CreatedDate = System.now();
        
        return post;
        }catch(exception e){
                           string errormessage=e.getMessage()+e.getStackTraceString();
                           ExceptionFramework.LogException('Order Update','OrderApproval','chattermessage',errormessage,UserInfo.getUserName(),'');        
                           return null;
                          }                                 
    }
    /*****Added new condition for order line item status based upon sampling restriction for CR-2023 Req 3 and 5 starts*****/
    public void updatelorderlineitemstatus()//method used to update the Order Line Item Status
    {
      try{
        List<OrderItem> submittedItems = new  List<OrderItem>();
        if(orderItems.size()>0){
        for(orderitem item:orderItems)
        {
            if(statusChangedTo(item.Order.Id, 'Open'))
            {
            /******* Commented for CR-02023
                item.status__c = 'Entered';
                submittedItems.add(item);
              ****/ 
                //CR-02023 Changes Begin
                if((item.status__c=='Approved' || item.status__c==null || item.Shipped_Product__r.Sampling_Restriction__c==null) && item.market__c==Label.PS_USMarket)
                    {
                item.status__c = 'Entered';
                submittedItems.add(item);
                }               
                    else if(item.market__c!=Label.PS_USMarket)  
                    {
                        item.status__c = 'Entered';
                        submittedItems.add(item);
                    }   
                    
            }  
             if(statusChangedTo(item.Order.Id, 'Pending Approval'))
            {
                if((item.Shipped_Product__r.Sampling_Restriction__c=='R') && (item.Target_System__c=='ERP'))  //Asha modified for CoursesmartProduct 
                {
                    item.status__c = 'Requires Approval';
                    submittedItems.add(item);
                }
                else if(item.Shipped_Product__r.Sampling_Restriction__c=='R')  
                {
                    item.status__c = 'Entered';
                    submittedItems.add(item);
                }
                else if(item.Shipped_Product__r.Sampling_Restriction__c==null)
                {
                    item.status__c = 'Entered';
                    submittedItems.add(item);
                }                             
                else
                {}
            } 
            if(statusChangedTo(item.Order.Id, 'Cancelled'))
            {               
                    item.status__c = 'Cancelled';
                    submittedItems.add(item);
            }   
          }
        }
        //CR-02023 Changes End
        if(submittedItems.size()>0) 
        {
            defineItemNumbers(submittedItems);
        } 
      }catch(exception e){
                         string errormessage=e.getMessage()+e.getStackTraceString();
                         ExceptionFramework.LogException('Order Update','OrderApproval','updatelorderlineitemstatus',errormessage,UserInfo.getUserName(),'');        
                         }   
    } 
     /*****Added new condition for order line item status based upon sampling restriction for CR-2023 Req 3 and 5 Ends*****/
    private void defineItemNumbers(List<OrderItem> items)
    {   
        Id orderId=null;
        
        Integer i = 0;
        
        for(OrderItem item: items)
        {
            if(orderId ==null || orderId != item.OrderId )
            {
                orderId = item.OrderId;
                i =0;
            }
            
            item.Item_Number__c= ++i;
        }
    }
       
   
    public void saveOrderItems()
    {
      Database.SaveResult[] lstOrderUpdateResult =Database.update(orderitems,false);
      if (lstOrderUpdateResult!= null){
          inserterrorlog(lstOrderUpdateResult,'OrderItem Update');
      }       
    }
      
    public void inserterrorlog(Database.SaveResult[] DMLResult,String InterfaceName)
    {
        List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
        for (Database.SaveResult sr : DMLResult) {
            String ErrMsg='';
            if (!sr.isSuccess() || Test.isRunningTest()){
                PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                errlogger.InterfaceName__c= 'OrderItem Update';
                errlogger.ApexClassName__c='OrderApproval';
                errlogger.CallingMethod__c='saveOrderItems';
                errlogger.UserLogin__c=UserInfo.getUserName(); 
                errlogger.recordid__c=sr.getId();
                for(Database.Error err : sr.getErrors()) 
                    ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage() + ': '+err.getFields(); 
                errlogger.ExceptionMessage__c=  ErrMsg;  
                errloggerlist.add(errlogger);    
            }
        }
        if(errloggerlist.size()>0){insert errloggerlist;}
    } 
    
}