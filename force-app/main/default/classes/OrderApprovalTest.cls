/***********************************************************************************************************
* Apex Interface Name : OrderApprovalTest 
* Version             : 1.0 
* Created Date        : 11/20/2018
* Function            : Unit tests for OrderApproval
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Rubavarnan         11/20/2018                Initial version
************************************************************************************************************/
@isTest
public class OrderApprovalTest {

    public static PriceBookEntry cachedPriceBookEntry;
 @testSetup static void initTestData() {
       
        UserRole role = [SELECT Id FROM UserRole WHERE DeveloperName = 'ANZ_HED_Sales'];
        
        User salesUser = TestDataFactory.createUser([SELECT Id, Name FROM Profile WHERE Name = 'Pearson Sales User OneCRM'].Id)[0];
        salesUser.CommunityNickname = 'salesUser123456789';
        salesUser.Market__c = 'AU';
        salesUser.UserRoleId = role.Id;
        salesUser.Line_of_Business__c = 'Higher Ed';
        salesUser.Business_Unit__c = 'Higher Ed';
        salesUser.Geography__c = 'Core';
        salesUser.Sample_Approval__c = True;
        insert salesUser;
                
        User serviceUser = TestDataFactory.createUser([SELECT Id, Name FROM Profile WHERE Name = 'Pearson Service Cloud User OneCRM'].Id)[0]; //RP updated Profile name July 15 2016 to avoid confusion with "Pearson Service Cloud User OneCRM" profile
        serviceUser.CommunityNickname = 'serviceUser123456789';
        serviceUser.Market__c = 'AU';
        serviceUser.UserRoleId = role.Id;
        serviceUser.Line_of_Business__c = 'Higher Ed';
        serviceUser.Business_Unit__c = 'Higher Ed';
        serviceUser.Geography__c = 'Core';
        serviceUser.Sample_Approval__c = True;
        insert serviceUser;   
                
        
        User u = TestDataFactory.createUser([SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'].Id)[0];        
        u.Market__c = 'AU';        
        u.Line_of_Business__c = 'Higher Ed';
        u.Business_Unit__c = 'Higher Ed';
        u.Geography__c = 'Core';
        u.Sample_Approval__c = True;
        insert u;
        assignBackendOrderPermissionSets(salesUser);
        System.runAs (salesUser) {
            
            Bypass_Settings__c settings = new Bypass_Settings__c();
            settings.Disable_Validation_Rules__c = true;
            settings.SetupOwnerId = salesUser.id;
            insert settings;
            createProductDataStructure();

            Account account = createAccount();
            insert account;            
            
            Product2 prod = buildProduct();
            prod.Name = 'ProductName1';
            prod.sampling_restriction__c ='R';
            prod.Status__c = 'TU';
            prod.IsActive = true;
            insert prod;
                        
            Product2 product = buildProduct();
            product.Name = 'Product Name';
            product.sampling_restriction__c ='';
            product.Status__c = 'TU';
            product.IsActive = true;
            insert product;       

            Order newOrder = createOrder(account, salesUser);   
            newOrder.Market__c ='CA'; 
            newOrder.status = 'New';           
            newOrder.ERP_Operating_Unit__c = 'US Pearson OU';                    
            insert newOrder;
            
            Order newOrder1 = createOrder(account, salesUser);
            newOrder1.Market__c ='US';  
            newOrder1.status = 'New';          
            newOrder1.ERP_Operating_Unit__c = 'US Pearson OU';
            newOrder1.Description = 'NewToCancel';
            insert newOrder1;  
            
            
            Order newOrder2 = createOrder(account, salesUser);
            newOrder2.Market__c ='US';  
            newOrder2.status = 'Open';          
            newOrder2.ERP_Operating_Unit__c = 'US Pearson OU';
            newOrder2.Description = 'NewToCancel';
            insert newOrder2;
           
            OrderItem orderItem = createOrderItem(newOrder);
            orderitem.OrderId= newOrder.Id;
            orderitem.shipped_product__C = prod.id; 
            insert orderItem;
            
            OrderItem orderItem1 = createOrderItem(newOrder);
            orderitem1.OrderId= newOrder1.Id;
            orderitem1.shipped_product__C = product.id; 
            orderitem1.status__c = 'Open';            
            insert orderItem1;
            
            Order newOrders = createOrder(account, salesUser);
            insert newOrders;
            Order newOrder1s = createOrder(account, salesUser);
            newOrder1s.Market__c ='US';
            insert newOrder1s;

            OrderItem orderItems = createOrderItem(newOrder);
            orderitems.OrderId= newOrder2.Id;
            insert orderItems;
            
          
        }

    }

    @isTest static void OrderApprovalMethod() {

        User serviceUser = [SELECT Id FROM User WHERE CommunityNickname = 'serviceUser123456789'];
        User salesUser = [SELECT Id FROM User WHERE CommunityNickname = 'salesUser123456789'];
        submitOrderAsSalesUser();        
        assignBackendOrderPermissionSets(serviceUser);

        //Test.startTest();

            System.runAs(serviceUser) {
            Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=serviceUser.Id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
            insert byp;                    
                Map<ID, Order> oldOrder= new Map<ID, Order>([SELECT Id,Order_Submitted__c, Status, Market__c FROM Order]);
                Order testOrder1 = [SELECT Id FROM Order LIMIT 1];
                testOrder1.Status = 'New';
                testOrder1.Order_Submitted__c = True;
                testOrder1.Market__c = 'US';
                update testOrder1; 
                
                testOrder1.Status = 'Cancelled';
                update testOrder1; 
                
                Test.startTest();

                testOrder1.Status = 'Open';
                update testOrder1;                                     
                
                Product2 prod = [SELECT id,Name,Status__C from Product2 WHERE Name = 'ProductName1'];
               
                OrderItem testOrderItem = [SELECT Id FROM OrderItem WHERE market__c = 'US' LIMIT 1];
                testOrderItem.Quantity = 2;
                testOrderItem.status__c = 'Approved'; 
                testOrderItem.shipped_product__C = prod.id;               
                update testOrderItem;
                
                OrderItem testOrderItem1 = [SELECT Id FROM OrderItem WHERE Order.Status = 'Open' LIMIT 1];
                testOrderItem1.Quantity = 2;
                testOrderItem1.status__c =''; 
                testOrderItem1.shipped_product__C = prod.id;
                update testOrderItem1; 
                
                OrderItem testOrderItem2 = [SELECT Id FROM OrderItem WHERE Order.Status = 'Open' LIMIT 1];
                testOrderItem2.Quantity = 2;
                testOrderItem2.status__c ='Pending Approval'; 
                testOrderItem2.shipped_product__C = prod.id;
                update testOrderItem2;
                
                Map<ID, Order> newOrder= new Map<ID, Order>([SELECT Id,Order_Submitted__c, Status, Market__c FROM Order ]);
             List<Order> testOrder = [SELECT Id FROM Order];
            OrderApproval approval =new OrderApproval(newOrder,oldOrder); 
            approval.submitOrder();           
            approval.submitApprovalChatterPost();
            approval.chattermessage(testOrder[0]);          
            approval.updatelorderlineitemstatus();
            approval.saveOrderItems();
   
              
            }

        Test.stopTest();

    }

// Helper Methods
    public static void submitOrderAsSalesUser() {

        User salesUser = [SELECT Id FROM User WHERE CommunityNickname = 'salesUser123456789'];

        System.runAs(salesUser) {
            
            Map<ID, Order> oldOrder= new Map<ID, Order>([SELECT Id,Order_Submitted__c, Status, Market__c FROM Order ]);

            List<Order> testOrder = [SELECT Id FROM Order];
            for(Order ord :testOrder ){
            ord .status = 'Open';
            ord .Order_Submitted__c = True;}            
            update testOrder;
            Map<ID, Order> newOrder= new Map<ID, Order>([SELECT Id,Order_Submitted__c, Status, Market__c FROM Order ]); 
            OrderApproval approval =new OrderApproval(newOrder,oldOrder); 
            approval.submitOrder();           
            approval.submitApprovalChatterPost();
            approval.chattermessage(testOrder[0]);
            approval.updatelorderlineitemstatus();
            approval.saveOrderItems();
           
        }

    }

    public static void assignBackendOrderPermissionSets(User user) {

        List<PermissionSet> permissionSets = [SELECT Id FROM PermissionSet WHERE Name = 'Pearson_Global_Backend_Sample' OR Name = 'Pearson_Global_Backend_Revenue' OR Name = 'Pearson_Sample_Order_Approver' OR Name = 'Pearson_Front_End_Sampling'];

        List<PermissionSetAssignment> permissionSetAssignments = new List<PermissionSetAssignment>();

        for(PermissionSet permissionSet : permissionSets) {

            PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();
            permissionSetAssignment.PermissionSetId = permissionSet.Id;
            permissionSetAssignment.AssigneeId = user.Id;

            permissionSetAssignments.add(permissionSetAssignment);

        }

        insert permissionSetAssignments;

    }


    public static Order createOrder(Account account, User owner) {

        Order order = new Order();
        order.AccountId = account.Id;
        order.Pricebook2Id = Test.getStandardPricebookId();
        order.Status = 'New';
        order.Description = 'Description';
        order.Market__c = 'UK';
        order.Line_of_Business__c = 'Higher Ed';
        order.Business_Unit__c = 'Higher Ed';
        order.Geography__c = 'Core';
        order.Salesperson__c = owner.Id;
        order.OwnerId = owner.Id;
        order.EffectiveDate = Date.today();
        order.ShippingCountryCode = 'GB';
        order.ShippingStateCode = 'KEN';
        order.ShippingPostalCode = '9999';
        order.ShippingStreet = 'Collen Avenue \n Sera Forset Street \n Third Lane \n 22nd floor';
        order.ShippingCity = 'Test';
        order.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Sample Order').getRecordTypeId();
        
        
        return order;


    }

    public static OrderItem createOrderItem(Order order) {

        OrderItem orderItem = new OrderItem();
        orderItem.ServiceDate = Date.today();
        orderItem.Description = 'Description';
        orderItem.UnitPrice = 0;
        orderItem.Quantity = 1;
        orderItem.OrderId = order.Id;
        orderItem.PriceBookEntryId = cachedPriceBookEntry.Id;
        return orderItem;

    }



    public static Account createAccount() {

        Account account = new Account();

        account.Name = 'Test Account';
        account.Line_of_Business__c = 'Higher Ed';
        account.Geography__c = 'AU';
        account.ShippingCountry = 'United Kingdom';
        account.ShippingState = 'Kent';
        account.ShippingPostalCode = '9999';
        account.ShippingStreet = 'Test';
        account.ShippingCity = 'Test';

        return account;


    }
 

    public static void createProductDataStructure() {


        Product2 product = buildProduct();        
        product.Name = 'Product Name';
        product.IsActive = true;
        product.sampling_restriction__c = 'R';
        product.Status__c = 'TU';
        insert product;

        PriceBook2 pricebook = buildPricebook();
        // update standard pricebook to make sure its active...
        update pricebook;

        PriceBookEntry priceBookEntry = buildPriceBookEntry(product, pricebook);
        insert priceBookEntry;
        cachedPriceBookEntry = priceBookEntry;

    }


    public static Product2 buildProduct() {
       Product2 product = new Product2();
        product.Name = 'Product Name';
        product.sampling_restriction__c = 'R';
        product.Status__c = 'TU';
        product.IsActive = true;

        return product;

    }



    public static Pricebook2 buildPricebook() {

        Pricebook2 priceBook = new Pricebook2();

        priceBook.Id = Test.getStandardPricebookId();
        priceBook.IsActive = true;

        return priceBook;
    }


    public static PricebookEntry buildPriceBookEntry(Product2 product, Pricebook2 pricebook) {

        PricebookEntry priceBookEntry = new PricebookEntry();
        priceBookEntry.Product2Id = product.Id;
        priceBookEntry.Pricebook2Id = priceBook.Id;
        priceBookEntry.UnitPrice = 10;
        priceBookEntry.IsActive = true;

        return priceBookEntry;


    }
    
    
}