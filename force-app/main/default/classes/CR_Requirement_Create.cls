/************************************************************************************************************
* Apex Class Name   : CR_Requirement_Create.cls
* Version           : 1.0 
* Created Date      : 26 Sept 2017
* Function          : Auto populate CR Requirements 
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Cristina Perez              09/26/2017              Controller for VF Page that populates the Name of the CR and the  Jira Integration fields
************************************************************************************************************/

public class CR_Requirement_Create {

    Public Id crId;

    public CR_Requirement_Create ()
    {
       crId= ApexPages.currentPage().getParameters().get('Id');
       System.debug('crId:'+crId);
    }
    
    public PageReference createRequirement(){
        String crfields = Utils_allfields.getCreatableFieldsList('OneCRM_Change_Request__c');
        String soql = 'SELECT '+ crfields +' FROM OneCRM_Change_Request__c  WHERE id = :crId';
        System.debug('Query :'+soql);
        OneCRM_Change_Request__c CR = (OneCRM_Change_Request__c )Database.query(soql);
          
        CR_Requirements__c requirement_new = new CR_Requirements__c();
        
        if(CR.status__c == 'Approved for Development'){
            requirement_new.Jira_Integration__c = 'Send to Jira';
        }
        requirement_new.Name = CR.Name + '-Req-' +  String.valueOf(CR.Last_Requirement_Number__c + 1);
        requirement_new.Requires_Code__c = 'Select One';
        requirement_new.OneCRM_Change_Request__c = crId;
        
        // Create a set of Status
        Set<String> CR_StatusLst = new Set<String>();
              
         // Add status to the Set 
        for( CR_Status_New_Approved__c approvedStatusList : CR_Status_New_Approved__c.getAll().values()){
            CR_StatusLst.add(approvedStatusList.Name);
            
        }  
        
        //if CR is not in Development then user can create requirements otherwise error
        if (CR_StatusLst.contains(CR.status__c)) {
            try
            {
                                  
                 insert requirement_new;
                 if(requirement_new.id != null)
                 {
                    PageReference pg = new PageReference('/'+requirement_new.id);
                    return pg;
                 }
            } catch(Exception ex)
            {
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.errorProposalCreation));
            }
         } else
         {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.errorCreateRequirement));
         }
             
        return null;
        
    }
    
}