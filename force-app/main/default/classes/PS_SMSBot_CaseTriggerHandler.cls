/*
 * Author: Apoorv Saxena (Neuraflash)
 * Date: 10/12/2019
 * Description: Created to bypass the PS_CaseTrigger logic 
 *              when the case is being created from SMS Bot.
 */
public with sharing class PS_SMSBot_CaseTriggerHandler {
    public static boolean bypassTrigger = false;
}