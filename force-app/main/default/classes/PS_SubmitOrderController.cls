/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_SubmitOrderController
   Description:     Creates PIU and redirects to PIU update page
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   13 Nov 2015      1.0           Accenture NDC                                         Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
 
public class PS_SubmitOrderController
{
    public string orderId; 
    public string opptyId;
    public boolean displayPopUp{get;set;}
    public Integer courseCount;
    public List<Opportunity> lstWithOpportunity;
    public string pageRedirect{get;set;}
    public string ChannelString{get;set;} 
    public string sellingPeriod{get;set;} 
    public string opptyAccountId{get;set;} 
    public string sellingPeriodYear{get;set;} 
    public string accountId;
    public string contactId;
    public string orderRecordType;
    public List<Order> lstWithOrderUpdate;
    public string orderMarket;
    public string orderBU;
    public string orderPurchaseOrderNum;
    public string errormsg{get;set;}
    public string orderChannel;
    public string orderStatus;
    public Id GlobalAssetRecordTypeId;
    public Date orderStartDate;
    public string orderSubmitted;
    public List<OpportunityContactRole> lstWithOpptyCon;
    public LIST<OpportunityUniversityCourse__c> lstCourses=new LIST<OpportunityUniversityCourse__c>();
    public PS_SubmitOrderController(Apexpages.Standardcontroller orderObj)
    {
          
        ChannelString = '';
        opptyAccountId = '';    
        sellingPeriod = '';
        displayPopUp = false;
        errormsg = '';
        orderSubmitted = '1';
        //get global asset record type id
        GlobalAssetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Global Products In Use').getRecordTypeId();
    }
    
    public pageReference redirect()
    {
        pageReference  pageToRedirect;
        opptyId = Apexpages.currentpage().getparameters().get('opportunityId');
        orderId = Apexpages.currentpage().getparameters().get('orderId');   
        accountId = Apexpages.currentpage().getparameters().get('accountId');
        contactId = Apexpages.currentpage().getparameters().get('contactId');
        orderRecordType = Apexpages.currentpage().getparameters().get('recordType');
        orderSubmitted = Apexpages.currentpage().getparameters().get('OrderSubmitted'); 
        lstWithOpportunity = new List<Opportunity>();
        lstWithOrderUpdate = new List<Order>();
        lstWithOpptyCon = new List<OpportunityContactRole>();
        try
        {
        if(orderSubmitted == '0')
        {
        if(orderRecordType == 'Revenue Order')
        {
            for(Order orderUpd : [select Id,Status,EffectiveDate,Business_Unit__c, Market__c,Purchase_Order_Number__c,Sales_Channel__c from order where id =: orderId])
            {
                if(orderUpd.Market__c == 'UK' && (orderUpd.Business_Unit__c == 'Higher Ed' || orderUpd.Business_Unit__c == 'ELT'))
                {
                    orderUpd.Status = 'Filled';
                }else
                {
                    orderUpd.Status = 'Open';    
                } 
                orderUpd.Order_Submitted__c = True;
                orderMarket = orderUpd.Market__c;
                orderBU = orderUpd.Business_Unit__c;
                orderPurchaseOrderNum  = orderUpd.Purchase_Order_Number__c; 
                orderChannel = orderUpd.Sales_Channel__c;
                orderStatus = orderUpd.Status;
                orderStartDate = orderUpd.EffectiveDate;
                lstWithOrderUpdate.add(orderUpd);
            }
        }  
  
        if(orderRecordType == 'Sample Order')
        {
            for(Order orderUpd : [select Id,Status,EffectiveDate,Business_Unit__c,Market__c,Purchase_Order_Number__c,Sales_Channel__c from order where id =: orderId])
            {
                orderUpd.Status = 'Open'; 
                orderUpd.Order_Submitted__c = True;
                orderMarket = orderUpd.Market__c;
                orderBU = orderUpd.Business_Unit__c;
                orderPurchaseOrderNum  = orderUpd.Purchase_Order_Number__c; 
                orderChannel = orderUpd.Sales_Channel__c;
                orderStatus = orderUpd.Status;
                orderStartDate = orderUpd.EffectiveDate;
                lstWithOrderUpdate.add(orderUpd);
                update lstWithOrderUpdate;
            }
        }
        //update lstWithOrderUpdate;
        
        if(orderRecordType == 'Revenue Order')
        {
        if(opptyId != '' && opptyId != null)
        {
           // courseCount = [select count() from OpportunityUniversityCourse__c where Opportunity__c =: opptyId];
           lstCourses = [Select ID  from OpportunityUniversityCourse__c where Opportunity__c =: opptyId];
            lstWithOpportunity = [select Id,AccountId,Channel__c,Selling_Period_Year__c,Selling_Period__c,StageName,Product_Count__c,Market__c from Opportunity where Id =: opptyId];
            
            if(lstWithOpportunity.size() > 0)
            {
                sellingPeriodYear = lstWithOpportunity[0].Selling_Period_Year__c;
                ChannelString = lstWithOpportunity[0].Channel__c;
                sellingPeriod = lstWithOpportunity[0].Selling_Period__c;
                opptyAccountId = lstWithOpportunity[0].AccountId;
                if(sellingPeriodYear == null)
                {
                    sellingPeriodYear = '';
                }
            }
            //if(courseCount > 0)
            if(lstCourses.size()>0)
            {    
                pageRedirect = 'PS_CreatePIUWithCourses';
                pageToRedirect = new pageReference('/apex/'+pageRedirect+'?opportunityId='+opptyId+'&channel='+ChannelString+'&sellingPeriod='+sellingPeriod+'&accountId='+opptyAccountId+'&sellingPeriodYear='+sellingPeriodYear+'&orderStatus='+orderStatus+'&oId='+orderId);
            }else
            {
                lstWithOpptyCon = [select Id,ContactId,Contact.Name,Role from OpportunityContactRole where OpportunityId =: opptyId limit 1];
                if(lstWithOpptyCon.size() == 0)
                {
                    displayPopUp = true;
                    errormsg = Label.PS_NoContactsOnOpptyForOrder;
                    return null;
                }
                pageRedirect = 'PS_CreatePIUWithoutCourses';
                pageToRedirect = new pageReference('/apex/'+pageRedirect+'?opportunityId='+opptyId+'&channel='+ChannelString+'&accountId='+opptyAccountId+'&orderStatus='+orderStatus+'&oId='+orderId);
            }
        }else
        {
            List<OrderItem> lstWithOrderLineItem = new List<OrderItem>();
            Contact_Product_In_Use__c  contactProd;
            List<Contact_Product_In_Use__c> lstWithContactProd = new List<Contact_Product_In_Use__c>();
            Asset newAsset;
            List<Asset> lstWithAsset = new List<Asset>();
            if(orderId != null)
            {
                //Map to hold asset key and orderItemId associated to it.
                Map<String, Id> assetToOrderItemIdMap = new Map<String, Id>();
                lstWithOrderLineItem = [select id,Quantity,PricebookEntryId,PricebookEntry.Product2Id,PricebookEntry.Product2.Name from OrderItem where orderid =: orderId];
                if(lstWithOrderLineItem.size() > 0 && accountId != null)
                {
                    for(OrderItem orderLineItem : lstWithOrderLineItem)
                    {
                        newAsset = new Asset();
                        newAsset.Name = orderLineItem.PricebookEntry.Product2.Name;
                        newAsset.Product2Id = orderLineItem.PricebookEntry.Product2Id; 
                        newAsset.Status__c = 'Active'; 
                        newAsset.AccountId = accountId; 
                        newAsset.Quantity = orderLineItem.Quantity;
                        newAsset.Order__c = orderId;
                        newAsset.Order_Product_Id__c = orderLineItem.Id;
                        newAsset.Channel__c = orderChannel;
                        newAsset.InstallDate = Date.Today();
                        newAsset.Expiry_Date__c = orderStartDate.addYears(1);
                        if(GlobalAssetRecordTypeId != null)
                        {
                            newAsset.RecordTypeId = GlobalAssetRecordTypeId;
                        }
                        lstWithAsset.add(newAsset);
                    }
                } 
                if(lstWithAsset.size()>0)
                {
                    insert lstWithAsset;
                }
                if(contactId != null && contactId != '' && lstWithAsset.size() > 0)
                {
                    for(Asset contactPIU : lstWithAsset)
                    {
                        contactProd = new Contact_Product_In_Use__c ();
                        contactProd.Product_in_Use__c = contactPIU.Id;
                        contactProd.Contact__c = contactId;
                        contactProd.Status__c = 'Active';
                        lstWithContactProd.add(contactProd);
                    } 
                    
                    if(lstWithContactProd.size() > 0)
                    {
                        insert lstWithContactProd;
                    }
                }
            }
            pageRedirect = 'PS_UpdatePIUWithoutCourses';
            pageToRedirect = new pageReference('/apex/'+pageRedirect+'?accountId='+accountId+'&channel='+ChannelString+'&orderId='+orderId);
        } 
        }else
        if(orderRecordType == 'Sample Order')
        {
             pageToRedirect = new pageReference('/'+orderId);    
        }
        }else
        {
            displayPopUp = true;
            errormsg = 'Order has already been submitted.';
            return null;
        }
        }catch(Exception e)
        {
             if(e.getMessage().contains('Value does not exist or does not match filter criteria.: [Product2Id]'))
             {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'You cannot create PIU for product(s) that does not belong to your market.');
                 ApexPages.addMessage(myMsg);
             }else
             if(e.getMessage().contains('Shipping Zip must be of the correct format, four numeric digits'))
             {
                 ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Error,'The Shipping Postal Code is not correctly formatted. Please update the Shipping Postal Code to four numeric digits before submitting this Order');
                 ApexPages.addMessage(msg);
             }else
             {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
                 ApexPages.addMessage(myMsg);
             }
             return null;   
        }
        //update lstWithOrderUpdate;
        return pageToRedirect;
    }
    
    public pageReference closePopup()
    {
        displayPopUp = false;
        pageReference pageToRedirect = new pageReference('/'+orderId);
        return pageToRedirect;
    }
}