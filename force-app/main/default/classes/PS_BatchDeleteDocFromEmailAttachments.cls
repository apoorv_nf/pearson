public class PS_BatchDeleteDocFromEmailAttachments implements Database.Batchable<sObject>{
	
    Set<Id> EmailAttFolderId = new Set<Id>();
    Set<Id> setDocIdtobeDeleted = new Set<Id>();
    List<Database.DeleteResult> lstDBSaveResult = null;
    
    public PS_BatchDeleteDocFromEmailAttachments(Id folderId){
        if(folderId != null){
            System.debug('@@folderID '+folderId);
            EmailAttFolderId.add(folderId);
        }
        System.debug('@@EmailAttFolderId '+EmailAttFolderId);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        if(!Test.isRunningTest()) {
            return Database.getQueryLocator([SELECT id,createddate FROM Document where Type = 'pdf' and folderId =: EmailAttFolderId  and createddate <: System.now().addDays(-1)]);
        }
        else {
            return Database.getQueryLocator([SELECT id,createddate FROM Document where folderId =: EmailAttFolderId]);
        }
        
    }
    
    public void execute(Database.BatchableContext BC, List<Document> lstDocument){
        System.debug('@@lstDocument '+lstDocument.size());
       	lstDBSaveResult = Database.delete(lstDocument, false);
        
        List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
        try{            
            for (integer i=0;i<lstDocument.size();i++){
                Document docu=lstDocument[i];
                Database.DeleteResult res=lstDBSaveResult[i];
                String ErrMsg='';
                if (!res.isSuccess() || Test.isRunningtest()){
                    PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                    errlogger.InterfaceName__c='PS_BatchDeleteDocFromEmailAttachments';
                    errlogger.ApexClassName__c='PS_BatchDeleteDocFromEmailAttachments';
                    errlogger.CallingMethod__c='finish';
                    errlogger.UserLogin__c=UserInfo.getUserName(); 
                    errlogger.RecordId__c=docu.id;   
                    for(Database.Error err : res.getErrors()) {
                        ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage();
                    }   
                    errlogger.ExceptionMessage__c=ErrMsg;
                    errloggerlist.add(errlogger);                     
                }
            }
            if(errloggerlist.size()>0){insert errloggerlist;}
            
        }
       catch(Exception e){
            ExceptionFramework.LogException('PS_BatchDeleteDocFromEmailAttachments','PS_BatchDeleteDocFromEmailAttachments','execute',e.getMessage(),UserInfo.getUserName(),'');
        }
    }
    
    public void finish(Database.BatchableContext BC){}
    
}