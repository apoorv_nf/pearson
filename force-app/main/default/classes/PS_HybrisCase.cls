/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_HybrisCase.cls 
Description:     On insert of Case record
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
06/05/2019         1.0            Asha G                       Logic to handle Account and Contact for Hybris Cases 
Sep 03 2019		   1.1			  Priya	  					CR-02933 Req 3: Logic to create Learner Account and Consumer Contact if MDM Id is null	
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class PS_HybrisCase {
    public static void hybrisCaseCreation(List<Case> caseList,Set<string> suppliedMDMId,Set<string> suppliedEmail,Boolean isSuppliedMDMIDempty){
        Map<String,Id> mapContactCase = new Map<String,Id>();
        Map<String,Id> mapAccountCase = new Map<String,Id>();
        Map<String,Account> mapNewAccount = new Map<String,Account>();
        Map<String,Contact> mapNewContact = new Map<String,Contact>();
        Map<String,Account> mapNewlearnerAccount= new Map<String,Account>();
        Map<String,String> mapAccountContactKey = new Map<String,String>();
        Map<String,String> mapLearnerAccountContactKey = new Map<String,String>();
        string ACRRecTypeID=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
        string GlobalConRecTypeID=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Global Contact').getRecordTypeId();
       //Added by priya for CR-02933 Req 3
        string learnerRecTypeID=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Learner').getRecordTypeId();
        string consumerConRecTypeID=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
       //End changes of priya
        if(isSuppliedMDMIDempty == false){ //Added by priya for CR-02933 Req 3
        for(Account actRec:[Select id,MDM_ID__c from Account where MDM_ID__c in :suppliedMDMId]){            
            mapAccountCase.put(actRec.MDM_ID__c,actRec.Id);
        } }  
        for(Contact conRec:[Select id,email from Contact where email in :suppliedEmail]){
            mapContactCase.put(conRec.email,conRec.Id);
        }   
        for(Case caseRecd :caseList){
            if(mapAccountCase.containsKey(caseRecd.Supplied_MDM_ID__c)){
                caseRecd.AccountID=mapAccountCase.get(caseRecd.Supplied_MDM_ID__c);
                system.debug('@@@Account to Case' +caseRecd.AccountID);
            }
            else{
                if(isSuppliedMDMIDempty == false){
                    Account newAcc=new Account(Name=caseRecd.SuppliedName, RecordTypeId=ACRRecTypeID);
                    mapNewAccount.put(caseRecd.Supplied_MDM_ID__c,newAcc);}
                else{
                    Account newAcc=new Account(Name=caseRecd.SuppliedName, RecordTypeId=learnerRecTypeID);
                    //  mapNewAccount.put(caseRecd.Supplied_MDM_ID__c,newAcc); 
                    mapNewlearnerAccount.put(caseRecd.SuppliedName,newAcc);
                }
            }
            if(mapContactCase.containsKey(caseRecd.SuppliedEmail)){
                caseRecd.ContactID=mapContactCase.get(caseRecd.SuppliedEmail);
                system.debug('@@@Contact to Case' +caseRecd.ContactID);
            }
            else{
                string[] fullName = caseRecd.suppliedname.split(' ');
                Contact newCon;
                if(isSuppliedMDMIDempty == false){
                    newCon=new Contact(RecordTypeId=GlobalConRecTypeID,email=caseRecd.SuppliedEmail,FirstName=fullName[0],LastName=fullName[1],Role__c='Professional');
                    if(mapAccountCase.containsKey(caseRecd.Supplied_MDM_ID__c)){
                        newCon.AccountID=mapAccountCase.get(caseRecd.Supplied_MDM_ID__c);
                        system.debug('@@@Newcon' +newCon.AccountID);
                    }else{
                        mapAccountContactKey.put(caseRecd.SuppliedEmail,caseRecd.Supplied_MDM_ID__c);
                    }
                }
                else{
                    newCon=new Contact(RecordTypeId=consumerConRecTypeID,email=caseRecd.SuppliedEmail,FirstName=fullName[0],LastName=fullName[1],Role__c='Professional');   
                    mapLearnerAccountContactKey.put(caseRecd.SuppliedEmail,caseRecd.SuppliedName);		
                }
                
                mapNewContact.put(caseRecd.SuppliedEmail,newCon);
            }
        }
        
        if(!mapNewAccount.isEmpty()){
            Insert mapNewAccount.values();
        }
        if(!mapNewlearnerAccount.isEmpty()){
            Insert mapNewlearnerAccount.values();
        }
        if(!mapNewContact.isEmpty()){
            
            for(Contact objCon:mapNewContact.values()){    
                if(isSuppliedMDMIDempty == false){
                    if(objCon.AccountId  == null && mapAccountContactKey.containsKey(objCon.email)){
                        system.debug('@@@Accountnull and contact check');
                        String supplied_MDM_ID= mapAccountContactKey.get(objCon.email);
                        objCon.AccountId = mapNewAccount.get(supplied_MDM_ID).Id;
                    }     
                }
                else{
                    if(objCon.AccountId  == null && mapLearnerAccountContactKey.containsKey(objCon.email)){
                        system.debug('@@@Accountnull and contact check');
                        String SuppliedName= mapLearnerAccountContactKey.get(objCon.email);
                        objCon.AccountId = mapNewlearnerAccount.get(SuppliedName).Id;
                    }  
                }
                
                
            }
            if(!Test.isRunningTest())
            Insert mapNewContact.values();
        }
        
        for(Case caseRecd :caseList){
            if(isSuppliedMDMIDempty == false){
                if(caseRecd.AccountID == null && mapNewAccount.containsKey(caseRecd.Supplied_MDM_ID__c)){
                    caseRecd.AccountID=mapNewAccount.get(caseRecd.Supplied_MDM_ID__c).Id;
                }}
            else{
                caseRecd.AccountID=mapNewlearnerAccount.get(caseRecd.SuppliedName).Id;
            }
            if(caseRecd.ContactID == null && mapNewContact.containsKey(caseRecd.SuppliedEmail)){
                caseRecd.ContactID=mapNewContact.get(caseRecd.SuppliedEmail).Id;
            }
        }
        
    }
}