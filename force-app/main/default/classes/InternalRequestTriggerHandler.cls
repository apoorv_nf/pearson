/************************************************************************************************************
* Apex Class Name   : InternalRequestTriggerHandler.cls 
* Version           : 1.0 
* Created Date      : 17 JULY 2015
* Function          : Handler class for Internal Request Object Trigger
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Mitch Hunt                  17/07/2015              Created Internal Request Handler Class Template
* -----------------------------------------------------------------------------------------------------------
* Karan Khanna                21/12/2015              Added logic for populating 'Market__c', 'Line_of_Business__c', 'Business_Unit__c', 'Geography__c' from the current User in BeforeInsert()
* Kyama                       24-Nov-2015             Modified For Finance Workflow Defects D-2588.
* Madhu                       6/22/16         As part of R5 hypercare D-5583 logic to populate maket/LOB/BU/Group on IR 
* Kyama                       15-Jun-2016             Update code for SUS512 - contact deduplication for R6
************************************************************************************************************/

public with sharing class InternalRequestTriggerHandler 
{
      public InternalRequestTriggerHandler(ApexPages.StandardController controller)
    {
    }
   InternalRequestsUtils utils = new InternalRequestsUtils();

   private boolean m_bIsExecuting = false;
    private integer iBatchSize = 0;
    private Id Finance_recordTypeId ;
    private Id Territory_recordTypeId;
    private Id sAccountRequestRTId = Schema.SObjectType.Internal_Request__c.getRecordTypeInfosByName().get('Account Request').getRecordTypeId();
    //KR:6/14/2016:R6--Added to get Contact record type for SUS512
    private Id sContactRequestRTId = Schema.SObjectType.Internal_Request__c.getRecordTypeInfosByName().get('Contact Request').getRecordTypeId();
    private Id sSalesandCreditingRTId = Schema.SObjectType.Internal_Request__c.getRecordTypeInfosByName().get('Sales Crediting & Territory Management').getRecordTypeId();
      
    public InternalRequestTriggerHandler (boolean bIsExecuting, integer iSize)
    {
        m_bIsExecuting = bIsExecuting;
        iBatchSize = iSize;
    }

    // EXECUTE BEFORE INSERT LOGIC
    //
    public void OnBeforeInsert(Internal_Request__c[] lstNewInternalRequests)
    {
      // Madhu : 6/22/16
       //Finance_recordTypeId =  Schema.SObjectType.Internal_Request__c.getRecordTypeInfosByName().get('Finance Workflow').getRecordTypeId();
       //Territory_recordTypeId = Schema.SObjectType.Internal_Request__c.getRecordTypeInfosByName().get('Territory Realignment').getRecordTypeId(); 
       // Begin - Logic for populatiing 'Market__c', 'Line_of_Business__c', 'Business_Unit__c', 'Geography__c' from the current User
       List<Internal_Request__c> intRequestNotCreatedFromAccount = new List<Internal_Request__c>();
        for(Internal_Request__c ir : lstNewInternalRequests)
        {          
        
            // check for the financeworkflow/Territory realignment record types, then populate the user values.
            //if(ir.RecordTypeId == Finance_recordTypeId || ir.RecordTypeId == Territory_recordTypeId   )
            //Saritha Singamsetty:02/19/2018: added new rt id for IR.
            if(ir.RecordTypeId == sSalesandCreditingRTId)
            {
                intRequestNotCreatedFromAccount.add(ir);
            }
      // Check for Account value, if the record is created from Chater Action on Account then value of fields will be passed from Account otherwise from current user
      else if (ir.recordtypeid == sContactRequestRTId || ir.recordtypeid == sAccountRequestRTId)
            {
                intRequestNotCreatedFromAccount.add(ir);
            }
        }
        if(!intRequestNotCreatedFromAccount.isEmpty())
        {
            PS_UserFieldUtill.setUserFieldValues(intRequestNotCreatedFromAccount);
        }
        // End - Logic for populatiing 'Market__c', 'Line_of_Business__c', 'Business_Unit__c', 'Geography__c' from the current User
    }
   
    //Added By Kyama
    // EXECUTE AFTER INSERT LOGIC
    // Give  IR record Access To Owner Manager/Manager's.Manager
    public void OnAfterInsert(Internal_Request__c[] lstNewInternalRequests)
    {
        PS_InternalRequestAssignment assignmentRuleUtility;
        // In future, handler should be introduced to determine what type of requests invoked this trigger, and construct the class with the right sobject type
        // Currenlty functionality is only required for Account
        //KR:6/14/2016:R6--Modified the existing Functinality to Pass the Sobject Based on the IR Request(Account/Contact) and record type for SUS512

        for(Internal_Request__c internal : lstNewInternalRequests){
           if(internal.RecordTypeId == sContactRequestRTId){
               assignmentRuleUtility = new PS_InternalRequestAssignment(Contact.SObjectType);
           }
           else if(internal.RecordTypeId == sAccountRequestRTId){
               assignmentRuleUtility = new PS_InternalRequestAssignment(Account.SObjectType);
           }
        }
        if (assignmentRuleUtility != null)
            assignmentRuleUtility.processAssignmentRules(lstNewInternalRequests);
        list<Internal_Request__Share> lstInternalRequeststoInsert  = new list<Internal_Request__Share>();
        lstInternalRequeststoInsert = utils.insertRecords(lstNewInternalRequests);
        if(lstInternalRequeststoInsert.size()>0)
        {
          Database.SaveResult[] lsr = Database.insert(lstInternalRequeststoInsert,false);
        }
    }
    
    // BEFORE UPDATE LOGIC
    // Return a list of update records from the utility to the trigger.new trigger class
    public list<Internal_Request__c> OnBeforeUpdate(Internal_Request__c[] lstOldInternalRequests, Internal_Request__c[] lstUpdatedInternalRequests, map<ID, Internal_Request__c> mapIDInternalRequest)
    {

       list<Internal_Request__c> lstInternalRequeststoUpdate = new list<Internal_Request__c>();
       lstInternalRequeststoUpdate  = utils.updateRecords(lstUpdatedInternalRequests, mapIDInternalRequest);
       return lstInternalRequeststoUpdate;

    }
    
     //Added By Kyama
    // AFTER UPDATE LOGIC
    // Give  IR record Access To Owner Manager/Manager's.Manager
    public void OnAfterUpdate(Internal_Request__c[] lstUpdatedInternalRequests)
    {
           list<Internal_Request__Share> lstInternalRequestsafterUpdate  = new list<Internal_Request__Share>();
           lstInternalRequestsafterUpdate = utils.insertRecords(lstUpdatedInternalRequests);
           if(lstInternalRequestsafterUpdate.size()>0)
              {
                Database.SaveResult[] lsr = Database.insert(lstInternalRequestsafterUpdate,false);
              }
    }
    
    
  /*  
    // BEFORE DELETE LOGIC
    //
    public void OnBeforeDelete(Internal_Request__c[] lstInternalRequestsToDelete, map<ID, Internal_Request__c> mapIDInternalRequest)
    {
        
    }
    
    // AFTER DELETE LOGIC
    //
    public void OnAfterDelete(Internal_Request__c[] lstDeletedInternalRequests,map<ID, Internal_Request__c> mapIDInternalRequest)
    {
        
    }
    
    // AFTER UNDELETE LOGIC
    //
    public void OnUndelete(Internal_Request__c[] lstRestoredInternalRequests)
    {
        
    }
    */
    @TestVisible public boolean bIsTriggerContext
    {
        get{ return m_bIsExecuting; }
    }
    
    @TestVisible public boolean bIsVisualforcePageContext
    {
        get{ return !bIsTriggerContext; }
    }
    
    @TestVisible public boolean bIsWebServiceContext
    {
        get{ return !bIsTriggerContext; }
    }
    
    @TestVisible public boolean bIsExecuteAnonymousContext
    {
        get{ return !bIsTriggerContext; }
    }

}