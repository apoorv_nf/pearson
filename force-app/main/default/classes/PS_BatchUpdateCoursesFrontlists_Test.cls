/**
 * Name : PS_BatchUpdateCoursesFrontlists_Test 
 * Author : Accenture
 * Description : Test class used for testing the PS_BatchUpdateCoursesFrontlists
 * Date : 7/01/15 
 * Version : <intial Draft> 
 */
@isTest
public class PS_BatchUpdateCoursesFrontlists_Test {

    static testmethod void testBatchUpdateCourses()
    { 
        List<Account> accList = TestDataFactory.createAccount(2,'Direct Corporate');
        insert accList;
        
        UniversityCourse__c unvobject = new UniversityCourse__c(); 
        unvobject.Account__c = accList[0].Id;
        unvobject.Active__c = true;
        unvobject.Name = 'Test Univ course Name';
        unvobject.Adoption_Type__c = 'committee';
        unvobject.Course_Name__c = 'Test course Name';
        unvobject.Fall_Enrollment__c = 121;
        unvobject.Spring_Enrollment__c = 110;
        unvobject.Winter_Enrollment__c = 415;
        unvobject.Fall_Front_List__c= true;
        unvobject.Spring_Front_List__c= true;
        integer iMonth = system.today().month();
               // unvobject.  = Course
       // unvobject.
        insert unvobject;
        List<UniversityCourse__c> lstUnivcourse = new List<UniversityCourse__c>();
       // lstUnivcourse.add(unvobject);
       // lstUnivcourse.clear();
        ProductCategory__c category = new ProductCategory__c(Name='testcategoryy',HierarchyLabel__c='testlabele');
        insert category; 
        Hierarchy__c csa =new Hierarchy__c(name ='estt',ProductCategory__c=category.id,Label__c='testlabel');
        csa.Market__c= 'US';
        csa.Type__c ='Course';
        csa.CurrentPeriod__c = true;
        csa.PriorPeriod__c = true;
        insert csa;  
        csa.CategoryHierarchy_ExternalID__c=csa.id;       
        Update csa;               
        Pearson_Course_Equivalent__c  pcourseObject = new Pearson_Course_Equivalent__c();
        pcourseObject.Course__c  =unvobject.Id;
        pcourseObject.Active__c = true;  
        pcourseObject.Primary__c =true;
        pcourseObject.Pearson_Course_Code_Hierarchy__c = csa.Id;
       
        insert pcourseObject;
        system.debug('###csa.Id=>'+csa.Id+':pcourseObject.Pearson_Course_Code_Hierarchy__c:'+pcourseObject.Pearson_Course_Code_Hierarchy__c);
        system.debug('###Pearson_Course_Equivalent__c=>'+pcourseObject+'::'+pcourseObject.Pearson_Course_Code_Hierarchy__r+'::'+pcourseObject.Pearson_Course_Code_Hierarchy__r.Market__c);
        list<Pearson_Course_Equivalent__c> PCE=[SELECT Name,Pearson_Course_Code_Hierarchy__c,Pearson_Course_Code_Hierarchy__r.Market__c,Pearson_Course_Code_Hierarchy__r.CurrentPeriod__c,Pearson_Course_Code_Hierarchy__r.PriorPeriod__c FROM Pearson_Course_Equivalent__c where Pearson_Course_Code_Hierarchy__c=: csa.Id];
        
        system.debug('###Pearson_Course_Equivalent__c  PCE.size()=>'+PCE.size());
        if(PCE.size()>0)
        system.debug('###Pearson_Course_Equivalent__c: '+'\nPCE:'+PCE+'\nPCE[0].Pearson_Course_Code_Hierarchy__c:'+PCE[0].Pearson_Course_Code_Hierarchy__c+'\nPearson_Course_Code_Hierarchy__r.Market__c:=>'+PCE[0].Pearson_Course_Code_Hierarchy__r.Market__c+'\nPearson_Course_Code_Hierarchy__r.CurrentPeriod__c:=>'+PCE[0].Pearson_Course_Code_Hierarchy__r.CurrentPeriod__c+'\nPearson_Course_Code_Hierarchy__r.PriorPeriod__c:=>'+PCE[0].Pearson_Course_Code_Hierarchy__r.PriorPeriod__c);
        Hierarchy__c hier=[SELECT Market__c,CurrentPeriod__c,PriorPeriod__c FROM Hierarchy__c];
        system.debug('###Market__c=>'+hier.Market__c+'\nCurrentPeriod__c=>'+hier.CurrentPeriod__c);
     PS_BatchUpdateCoursesFrontlists batchObject = new PS_BatchUpdateCoursesFrontlists('US',unvobject.Id, true);   
      Test.startTest();  
      database.executeBatch(batchObject) ; 
   //  system.schedule();
     SchedulableContext sc = null;
     // tsc.execute(sc);
     batchObject.execute(sc);
        Test.stopTest();
     
    }
    
}