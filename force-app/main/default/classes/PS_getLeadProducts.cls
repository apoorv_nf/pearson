/*
 * ********************************************************************************************************************************
 * @Author : Asha G
 * @Date : 06/07/2019
 * @Description: Controller to 'PS_LeadProductDetail' Page - contains logic to display all the Lead Products fields in detail as Read Only
 * @Version : 1.0
 * ********************************************************************************************************************************
 */
public class PS_getLeadProducts {
    public Lead_Product__c LeadProd{get;set;}
    public PS_getLeadProducts()
    {
  leadProd = [select Id,Name,Lead__c,Status__c,Opportunity__c,OwnerId,Requested_Product__c,Requested_ISBN_10__c,Requested_Authors__c,Requested_ISBN_13__c,Requested_Publisher__c,Requested_Title_Edition__c,Copyright_Date__c,Course__c,Availability__c,Book_Adopted__c,Product_In_Use__c,Consider_Adopting__c,Publisher_in_Use__c,Term__c,Decision_Date__c,Enrollment__c,Adoption_Type__c,CreatedBy.name,LastModifiedBy.name,RecordType.name from Lead_Product__c where Id=: ApexPages.currentPage().getParameters().get('id')];
    }

}