/************************************************************************************************************
* Apex Interface Name : PS_LiveChatTranscriptEventtrackingTest
* Version             : 1.1 
* Created Date        : 2/2/2016  
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
  Sudhakar Navuluri        2/19/2016    
  SN                       07/03/2016           Amended Code
  SN                       10/03/2016           Amended Code

-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_LiveChatTranscriptEventtrackingTest
{
    static testMethod void validateLiveChatTranscriptEventPostive()
    {
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');        
        insert lstAccount ;
        
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].accountid=lstAccount[0].id;
        lstContact[0].MailingState='England';
        
        insert lstContact;  
        
        List<User> agent = new List<User>();
        agent = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,2);
        agent[0].UserPermissionsLiveAgentUser=true;
        agent[1].UserPermissionsLiveAgentUser=true;
        
        List<case> cases = TestDataFactory.createCase(1,'School Assessment'); 
        cases[0].AccountId=lstAccount[0].id;
        cases[0].Contact=lstContact[0]; 
        cases[0].Status='Closed';
        cases[0].CreatedDate=system.today()-3;  
        cases[0].ClosedDate =system.today()-2;           
        
        Test.StartTest();
        insert cases;      
        
        LiveChatVisitor LiveChatVisi=new LiveChatVisitor();
        insert LiveChatVisi; 
        
        LiveChatTranscript transcript =new LiveChatTranscript();
        transcript.LiveChatVisitorId = LiveChatVisi.Id;
        transcript.caseId = cases[0].id;
        transcript.StartTime = System.now();
        transcript.EndTime = DateTime.Now().addMinutes(6);            
      
        insert transcript;
        
        
        List<LiveChatTranscriptevent> eventaccept=new List<LiveChatTranscriptevent>();
        
        LiveChatTranscriptevent eventaccept0 = new LiveChatTranscriptevent();   
        eventaccept0.LiveChatTranscriptId = transcript.id;
        eventaccept0.Type='ChatRequest';
        eventaccept0.Time=DateTime.Now(); 
        eventaccept.add(eventaccept0);
         
        LiveChatTranscriptevent eventaccept1 = new LiveChatTranscriptevent(); 
        eventaccept1.LiveChatTranscriptId = transcript.id;
        eventaccept1.Type='PushAssignment';
        eventaccept1.AgentId=agent[0].id;
        eventaccept1.Time=DateTime.Now();
        eventaccept.add(eventaccept1);
        
        LiveChatTranscriptevent eventaccept2 = new LiveChatTranscriptevent(); 
        eventaccept2.LiveChatTranscriptId = transcript.id;
        eventaccept2.Type='Accept';
        eventaccept2.AgentId=agent[0].id;
        eventaccept2.Time=DateTime.Now(); 
        eventaccept.add(eventaccept2); 
        
        insert eventaccept;
        
        List<LiveChatTranscriptevent> eventtransfer=new List<LiveChatTranscriptevent>();  
        
        LiveChatTranscriptevent eventtransfer0 = new LiveChatTranscriptevent();   
        eventtransfer0.LiveChatTranscriptId = transcript.id;
        eventtransfer0.Type='ChatRequest';
        eventtransfer0.Time=DateTime.Now(); 
        eventtransfer.add(eventtransfer0);
         
        LiveChatTranscriptevent eventtransfer1 = new LiveChatTranscriptevent(); 
        eventtransfer1.LiveChatTranscriptId = transcript.id;
        eventtransfer1.Type='PushAssignment';
        eventtransfer1.AgentId=agent[0].id;
        eventtransfer1.Time=DateTime.Now();
        eventtransfer.add(eventtransfer1);
        
        LiveChatTranscriptevent eventtransfer2 = new LiveChatTranscriptevent(); 
        eventtransfer2.LiveChatTranscriptId = transcript.id;
        eventtransfer2.Type='Accept';
        eventtransfer2.AgentId=agent[0].id;
        eventtransfer2.Time=DateTime.Now(); 
        eventtransfer.add(eventtransfer2); 
        
        LiveChatTranscriptevent eventtransfer3 = new LiveChatTranscriptevent();  
        eventtransfer3.LiveChatTranscriptId = transcript.id;
        eventtransfer3.Type='TransferRequest';
        eventtransfer3.AgentId=agent[0].id;
        eventtransfer3.Time=DateTime.Now().addMinutes(1);
        eventtransfer.add(eventtransfer3);
        
        LiveChatTranscriptevent eventtransfer4 = new LiveChatTranscriptevent(); 
        eventtransfer4.LiveChatTranscriptId = transcript.id;
        eventtransfer4.Type='Transfer';
        eventtransfer4.AgentId=agent[1].id;
        eventtransfer4.Time=DateTime.Now().addMinutes(2);
        eventtransfer.add(eventtransfer4);    
        
        insert eventtransfer;
        
        List<LiveChatTranscriptevent> eventend=new List<LiveChatTranscriptevent>();
        
        LiveChatTranscriptevent  eventend0 = new LiveChatTranscriptevent();   
        eventend0.LiveChatTranscriptId = transcript.id;
        eventend0.Type='ChatRequest';
        eventend0.Time=DateTime.Now(); 
        eventend.add(eventend0);
         
        LiveChatTranscriptevent eventend1 = new LiveChatTranscriptevent(); 
        eventend1.LiveChatTranscriptId = transcript.id;
        eventend1.Type='PushAssignment';
        eventend1.AgentId=agent[0].id;
        eventend1.Time=DateTime.Now();
        eventend.add(eventend1);
        
        LiveChatTranscriptevent eventend2 = new LiveChatTranscriptevent(); 
        eventend2.LiveChatTranscriptId = transcript.id;
        eventend2.Type='Accept';
        eventend2.AgentId=agent[0].id;
        eventend2.Time=DateTime.Now(); 
        eventend.add(eventend2); 
        
        LiveChatTranscriptevent eventend3 = new LiveChatTranscriptevent();  
        eventend3.LiveChatTranscriptId = transcript.id;
        eventend3.Type='TransferRequest';
        eventend3.AgentId=agent[0].id;
        eventend3.Time=DateTime.Now().addMinutes(1);
        eventend.add(eventend3);
        
        LiveChatTranscriptevent eventend4 = new LiveChatTranscriptevent(); 
        eventend4.LiveChatTranscriptId = transcript.id;
        eventend4.Type='Transfer';
        eventend4.AgentId=agent[1].id;
        eventend4.Time=DateTime.Now().addMinutes(2);
        eventend.add(eventend4); 
        
        LiveChatTranscriptevent eventend5 = new LiveChatTranscriptevent();  
        eventend5.LiveChatTranscriptId = transcript.id;
        eventend5.Type='EndAgent';
        eventend5.AgentId=agent[1].id;
        eventend5.Time=DateTime.Now().addMinutes(5);      
        eventend.add(eventend5); 
        
        LiveChatTranscriptevent eventend6 = new LiveChatTranscriptevent(); 
        eventend6.LiveChatTranscriptId = transcript.id;
        eventend6.Type='LeaveVisitor';
        eventend6.Time=DateTime.Now().addMinutes(6); 
        eventend.add(eventend6);        
        
        insert eventend;
        Test.StopTest();   
    }   
}