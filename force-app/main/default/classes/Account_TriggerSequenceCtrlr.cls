/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_PSAccount_TriggerSequenceCtrlr.cls
   Description:     On insert/update/delete of Account record 
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  03/2015           1.1             Manikandan           Initial Release 
  21/08/2015        1.2             Leonard Victor       Code streamline as part of R3
  10/2015           1.3             Chaitra              D-0761: Bypassed Update Primary Selling Account check for Accounts created from Lead Conversion
  05/11/2015        1.4             Karan Khanna         D-2296: Updated change of V1.3 such that it bypasses Accounts created from D2L leads
  09/11/2015        1.5             Kamal Chandran       D-2352 : Updated before insert and update methods to call 'setAccountFieldsBeforeInsert'   
  03/12/2015        1.6             Ron Matthews         D-2894 Reduce number of SOQL Statements for lead conversion process.
  09/27/2016        1.7             Kyama                Invoking new method from beforUpdate to Restore account values when Integration user is modifying.
  05/07/2018        1.8             Navaneeth            Added logic as part of US HE SP Project : CRMSFDC-2392
  04/26/2019        1.9             Navaneeth & Harika   Added logic to Skip for Integration Profile - Tech Debt.
  05/08/2019        2.0             Navaneeth            Added Boolean Check for Account Merge Functionality
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class Account_TriggerSequenceCtrlr {
    
    
    // Start of Addition by Navaneeth
       Static Bypass_Settings_Trigger_Function__c bypassAccTrigFunc = Bypass_Settings_Trigger_Function__c.getInstance();    
    // End of Addition by Navaneeth
    
    /**
    * Description : Performing all  Before Update Operations
    * @param NA
    * @return NA
    * @throws NA
    **/
    public static void beforeUpdate(List<Account> newAccounts,Map <Id,account>OldMap){
        //System.debug('\n\n###########Before Update is called#####################\n\n');
        Ps_PreventChangingOwner.PreventChangingAccountOwner(newAccounts,oldMap);
        // Madhu:01/05/2016 Added the switch condition for the chatter notifications.   
        // if (System.Label.PS_EnableChatterNotifcation.equalsIgnoreCase('On') || Test.isRunningTest())
        // Added Condition - To Skip the functionality for Integration Profile - By Navaneeth & Harika
         if ((System.Label.PS_EnableChatterNotifcation.equalsIgnoreCase('On') || Test.isRunningTest()) && (!bypassAccTrigFunc.disableAccountTrigger__c))
        {    
          PS_AccountOperations.AccountTerritoryAssociationNotification(newAccounts,oldMap);
        }  
        //D-2352
        // Added Condition - To Skip the functionality for Integration Profile - By Navaneeth & Harika
        if(!bypassAccTrigFunc.disableAccountTrigger__c)
        {
        PS_SetAccountOnLeadConversion.setAccountFieldsBeforeInsert(newAccounts); 
        PS_LeadConvertUtility plu = new PS_LeadConvertUtility();
        plu.rollbackIncorrectAccountConversion((Map<Id, Account>)Trigger.NewMap, OldMap); 
        }
        //Added By Kyama For R6 Implementation...
        PS_AccountOperations.restoreAccountFieldsOnELTuserUpdate(newAccounts,oldMap);   
    } 
    
    /**
    * Description : Performing all  before Insert operation
    * @param NA
    * @return NA
    * @throws NA
    **/
    public static void beforeInsert(List<Account> newAccounts){
        if(newAccounts.size()>0){
        PS_Account_RecordTagging_Ctrlr.accountRecordTagging(newAccounts);
        //D-2352
        // Added Condition - To Skip the functionality for Integration Profile - By Navaneeth & Harika
            if(!bypassAccTrigFunc.disableAccountTrigger__c)
            {
               PS_SetAccountOnLeadConversion.setAccountFieldsBeforeInsert(newAccounts);
            }
        }
    }
    
    /**
    * Description : Performing all  before Insert operation
    * @param NA
    * @return NA
    * @throws NA
    **/
    //Commenting the whole method since its not used anymore.
    /*  public static void beforeInsertOrUpdate(List<Account> newAccounts , Boolean isInsert){
       // System.debug('newAccounts--------->'+newAccounts);
         
        Map<String, Schema.SObjectType> sObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType Accnt = sObjectMap.get('Account') ; // getting Sobject Type
        Schema.DescribeSObjectResult accntresSchema = Accnt.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> accntrecordTypeInfo = accntresSchema.getRecordTypeInfosByName(); //getting all Recordtype for the Sobject
          
            if(isInsert){
                for(Account accObj : newAccounts){
                    if(accObj.IsCreatedFromLead__c)
                            accObj.recordtypeid = accntrecordTypeInfo.get('Learner').getRecordTypeId();
                }
            } 
           //  system.debug('newAccounts[0].recordtypeid--------->'+newAccounts[0].recordtypeid);
         //CalculateAccountFee.CalculateFee(newAccounts);
    }*/
    
    
    /**
    * Description : Performing all  After Insert Operations
    * @param List<Account> newAccounts
    * @return NA
    * @throws NA
    **/
    public static void afterInsert( List<Account> newAccounts){
        PS_AccountOperations.inserAccountTeam(newAccounts);
        UpdateTeamMember.updateAccountOwner(newAccounts);
        UpdateTeamMember.addAccountTeamMember(newAccounts);
        UpdateTeamMember.UpdateAccountShare(newAccounts);
        GeneratePearsonId.GenerateRandomPearsonID(newAccounts);
    }
    
     /**
    * Description : Performing all  After Update Operations
    * @param NA
    * @return NA
    * @throws NA
    **/
    public static void afterUpdate(List<Account> newAccount , Map<Id,Account> oldAccountMap){
        // Added Condition - To Skip the functionality for Integration Profile - By Navaneeth & Harika
        if(!bypassAccTrigFunc.disableAccountTrigger__c){
            PS_AccountOperations.updateCoursePrimaryAccount(newAccount,oldAccountMap);
        }
    }
    
    /**
    * Description : Performing all  AfterInsert or AfterUpdate operation
    * @param NA List<Account> newtriggeredAccounts,Boolean isInsert,Boolean isUpdate 
    * @return NA
    * @throws NA
    * Added oldAccounts Map as part of CRMSFDC-2392
    **/
    public static void afterInsertOrUpdate( List<Account> neworgAccountList, Boolean isInsert, Boolean isUpdate, Map<Id,Account> oldMapAccounts )
    {
       // Start of Addition by Navaneeth
       // Skipped ACR Record Type as per Rob Suggestion
       Id acrRecordTypeId = Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
       // End of Addition by Navaneeth
       List<account> nonD2LLeadConvertedAccountList = new List<account>();
       
        // Start of Addition by Navaneeth
        boolean accountlist_has_Merge = false;
        // End of Addition by Navaneeth

        if(isInsert){
            // Moved this for loop inside the above if condition - By Navaneeth - For Tech Debt
            for(Account acc : neworgAccountList) 
            {          
                // We dont need to perform the primary selling check if the account is from a lead 
                if(!LeadConversionChecker.inLeadConversion)
                {
                  // Lead_Type__c is a hidden field which gets value from Lead record type during conversion
                  if(acc.Lead_Type__c != 'D2L' && acc.RecordTypeId != acrRecordTypeId ){                 
                    nonD2LLeadConvertedAccountList.add(acc );
                    // Start of Addition by Navaneeth
                    // To Identify the Account Merge Functionality
                    if(acc.Account_Duplicate__c != null && accountlist_has_Merge == false)
                    {
                    accountlist_has_Merge = true;
                    }
                    // End of Addition by Navaneeth
                  }
                }
            }
        }
        
        // Start of Addition by Navaneeth - Start
        // Added as part of Tech Debt
        if(isUpdate){
            for(Account acc : neworgAccountList) 
            {          
                if(!LeadConversionChecker.inLeadConversion && acc.Lead_Type__c != 'D2L' &&  (acc.RecordTypeId != acrRecordTypeId)  && ((acc.Primary_Selling_Account_check__c != oldMapAccounts.get(acc.Id).Primary_Selling_Account_check__c ) || ( acc.System_Account_Indicator__c != oldMapAccounts.get(acc.Id).System_Account_Indicator__c )))
                {
                    nonD2LLeadConvertedAccountList.add(acc );
                    // Start of Addition by Navaneeth
                    // To Identify the Account Merge Functionality
                    if(acc.Account_Duplicate__c != null && accountlist_has_Merge == false)
                    {
                    accountlist_has_Merge = true;
                    }
                    // End of Addition by Navaneeth
                }
            }
        }
        // End of Addition by Navaneeth - End
        
        // Commented and Modified for CRMSFDC-2392 - by Navaneeth - Start
        /* 
        if(nonD2LLeadConvertedAccountList.size() > 0)
            UpdatePrimarySellingAccountCheck.processOnInsert_Update(nonD2LLeadConvertedAccountList,isInsert,isUpdate);
        */ 
        if( nonD2LLeadConvertedAccountList.size() > 0 && accountlist_has_Merge == false)
        {
            UpdatePrimarySellingAccountCheck.processOnInsert_Update( nonD2LLeadConvertedAccountList,isInsert,isUpdate,oldMapAccounts );
        }
        //Commented and Added as part of CRMSFDC-2392 by Navaneeth - End  
    }
    
    /**
    * Description : Performing all  After Delete operation
    * @param NA
    * @return NA
    * @throws NA
    **/
   /* public static void afterDelete()
    {
     }*/

}