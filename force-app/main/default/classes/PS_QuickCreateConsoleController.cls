/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_QuickCreateConsoleController .cls 
Description:     Quick Create Account and Contact
Test class:      PS_QuickCreateConsoleControllerTest.cls
Date             Version              Author                          Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
Apr-7-2016       0.1                  NS                         Quick Create Account and Contact
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class PS_QuickCreateConsoleController 
{
    public Id conRecordTypeId{set;get;}
    public Id accRecordTypeId{set;get;}
    
    public Id profileId{set;get;}
    public String profileName;
    
    
    //constructor
    
    public PS_QuickCreateConsoleController() {  
        profileId = UserInfo.getProfileId();        
        profileName = [Select Id,Name from Profile where Id=:profileId].Name;       
               
        accRecordTypeId= Schema.SObjectType.Account.getRecordTypeInfosByName().get(Label.PS_AccountCreationRequest_ID).getRecordTypeId();  
        
        //Description: Added code for Contact Record type for School Assessment by Kaavya Kanna (SA Offshore Team) on 23rd May 2017
        
        if(profileName == label.SA_ProfileName){
            conRecordTypeId= Schema.getGlobalDescribe().get('Contact').getDescribe().getRecordTypeInfosByName().get(Label.School_Assessment).getRecordTypeId();
        }
        else {
            conRecordTypeId= Schema.getGlobalDescribe().get('Contact').getDescribe().getRecordTypeInfosByName().get(Label.PS_Global_Contact).getRecordTypeId();
        }     
    }
}