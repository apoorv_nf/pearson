/* -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            OpptyProductSalesPriceUpdate.cls 
Description:     Batch Apex For Updating the Oppty Product Sales price

Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ------------------------------------------------------------------------------------------------------------------------------------
May 08 2019       1.0                Priya                For CR-02909 : Opportunity Product Sales price update.                                                    
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
public class OpptyProductSalesPriceUpdate implements Database.Batchable<SObject> , Database.Stateful  {
    transient Database.SaveResult[] srList;
    Static Map<Id,String> errorMsgMap = new Map<Id,String>();    
    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        List<OpportunityLineItem> finallist = new List<OpportunityLineItem>(); 
        List<Selling_Period_Parameter__mdt> sellingperiodmdt =[SELECT Id, Selling_Period_Name__c,Active__c,Opportunity_Market__c FROM Selling_Period_Parameter__mdt where Active__c =true];
        List<String>sellname = new List<String>();
        String opptyMarket;
        for(Selling_Period_Parameter__mdt sp :sellingperiodmdt){
            sellname.add(sp.Selling_Period_Name__c);
            opptyMarket=sp.Opportunity_Market__c;
        }
        if(Test.isRunningTest()){
            return Database.getQueryLocator([SELECT Id, OpportunityId,Product2.Net_Price__c,UnitPrice FROM OpportunityLineItem where CreatedDate = Today]);
        }else{
           
        // return Database.getQueryLocator([Select Id, OpportunityId,Opportunity.Market__c, Product2.Net_Price__c,UnitPrice,isNetPriceNoMatch__c FROM OpportunityLineItem where isNetPriceNoMatch__c = true  and Opportunity.Market__c =:opptyMarket and Opportunity.Selling_Period__c IN :sellname and Opportunity.special_pricing_renewal__c = false]);
          return Database.getQueryLocator([Select Id, OpportunityId,Opportunity.Market__c, Product2.Net_Price__c,UnitPrice,isNetPriceNoMatch__c FROM OpportunityLineItem where Id ='00k0E00000546HP' and Opportunity.Market__c='US']);
        }
    }
    public void execute(Database.BatchableContext bc, List<OpportunityLineItem> recordsToProcess){
        List<OpportunityLineItem> lstToUpdate = new List<OpportunityLineItem>();
        for(OpportunityLineItem oppty :recordsToProcess){
            oppty.UnitPrice = oppty.Product2.Net_Price__c ;
            /*if( oppty.UnitPrice == oppty.Product2.Net_Price__c)
{oppty.PriceUpdateCheck__c = true;}*/
            lstToUpdate.add(oppty);
        }
        if(lstToUpdate.size()>0){
            srList = Database.update(lstToUpdate, false);
        }
        
        if(srList!=null && !srList.isEmpty()){
            for(integer i =0;i<srList.size();i++){
                if(!srList.get(i).isSuccess()){
                    Database.Error error = srList.get(i).getErrors().get(0);
                    String failedDML = error.getMessage();
                    errorMsgMap.put(srList.get(i).Id, failedDML);
                }
            }
        }
        
    }
    public void finish(Database.BatchableContext bc){
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email, ExtendedStatus
                          from AsyncApexJob where Id = :bc.getJobId()];      
        if(a.Status == 'Completed') {
            List<Messaging.SingleEmailMessage> mmails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<Id> userIds = new List<Id>();
            userIds.add('005b0000002ffLB');
            mail.setToAddresses(userIds);
            mail.setSenderDisplayName('Batch Processing for Oppty Product Update');
            mail.setSubject('Oppty Sales Price Update: ' + a.Status);
            String errormsg= '';
           // Id errorIds;
            //retrive the failed Id and send in email
            for(Id errId : errorMsgMap.keySet()){
                errormsg = 'Failed Error record Ids ' + errorMsgMap.get(errId);  
            }
            
            mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
                                  ' with the following error'+ a.NumberOfErrors + ' Ids failed ' + errormsg );
            mmails.add(mail); 
            list<Messaging.SendEmailResult> results1 =  Messaging.sendEmail(mmails);
            system.debug('results --->'+results1);
        }
    }
    
}