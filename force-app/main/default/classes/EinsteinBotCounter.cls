/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 30/05/2018
 */
public with sharing class EinsteinBotCounter {

	@InvocableMethod(label='Einstein Bot - Increment')
	public static List<Integer> increment(List<Integer> values) {
		List<Integer> newValues = new List<Integer>();
		for(Integer value : values) {
			newValues.add(value == null ? 1 : ++value);
		}

		return newValues;
	}
}