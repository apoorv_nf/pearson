/**
* @description : Test class for EinsteinBotCheckBusinessHours, BusinessDaysUtil.
* @Author      : Apoorv (Neuraflash)  
* @CreatedDate : Aug 30, 2019
* @update      : Commented testCheckContactByPhoneSceanrioTwo (Read time out issue in class)
*/
@isTest
private class EinsteinBotCheckContactByPhoneTest {

    @TestSetup
    static void setupData(){
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {

            String firstName = 'John';
            String lastName = 'Doe';

            Account acc = new Account();
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_Creation_Request').getRecordTypeId();
            acc.Name = firstName + ' ' + lastName;

            insert acc;
            
            Contact con = new Contact();
            con.FirstName = firstName;
            con.LastName = lastName;
            con.Email = 'john@salesforce.com';
            con.Phone = '+12345678901';
            con.AccountId = acc.Id;
            insert con;
        
            // Create a queue
            Group testGroup = new Group(Name='NAUS HETS SMS ACR', Type='Queue');
            insert testGroup;

            // Assign user to queue
            GroupMember testGrpMember = new GroupMember(GroupId = testGroup.Id, UserOrGroupId = UserInfo.getUserId());
            insert testGrpMember;

            MessagingChannel msgChannnel = new MessagingChannel(MasterLabel = '+11234567890', IsActive = TRUE, 
                                                                MessageType = 'Text', TargetQueueId = testGroup.Id,
                                                                MessagingPlatformKey = '+11234567890', DeveloperName = 'Text_11234567890'
                                                                );
            insert msgChannnel;

            // Create a test MessagingEndUser
            List<MessagingEndUser> msgUserList = new  List<MessagingEndUser>();
            msgUserList.add(new MessagingEndUser(MessagingPlatformKey = '+12345678901', Name = '+12345678901',
                                                IsOptedOut = FALSE, MessagingChannelId = msgChannnel.Id,
                                                MessageType = 'Text',AccountId = acc.Id, ContactId = con.Id));
            msgUserList.add(new MessagingEndUser(MessagingPlatformKey = '+10987654321', Name = '+10987654321',
                                                IsOptedOut = FALSE, MessagingChannelId = msgChannnel.Id,
                                                MessageType = 'Text'));
            insert msgUserList;

            List<MessagingSession> msgSessionList = new  List<MessagingSession>();
            msgSessionList.add(new MessagingSession(MessagingEndUserId = msgUserList[0].Id, MessagingChannelId = msgChannnel.Id, 
                                                    Status = 'Active'));
            msgSessionList.add(new MessagingSession(MessagingEndUserId = msgUserList[1].Id, MessagingChannelId = msgChannnel.Id, 
                                                    Status = 'Active'));
            insert msgSessionList;
        }
    }

    @IsTest
    static void testCheckContactByPhone(){
        
        Id msgUserId = [SELECT Id FROM MessagingEndUser WHERE MessagingPlatformKey = '+12345678901' LIMIT 1].Id;
        String msgSessionId = [SELECT Id FROM MessagingSession WHERE MessagingEndUserId =: msgUserId LIMIT 1].Id;
        
        Test.startTest();
            List<Contact> conList = EinsteinBotCheckContactByPhone.checkContactExistenceByPhone(new List<String>{msgSessionId});
        Test.stopTest();
        System.assertEquals(TRUE, conList != NULL);
        
    }

    /*@IsTest
    static void testCheckContactByPhoneSceanrioTwo(){
        
        Id msgUserId = [SELECT Id FROM MessagingEndUser WHERE MessagingPlatformKey = '+10987654321' LIMIT 1].Id;
        String msgSessionId = [SELECT Id FROM MessagingSession WHERE MessagingEndUserId =: msgUserId LIMIT 1].Id;
        
        Test.startTest();
            List<Contact> conList = EinsteinBotCheckContactByPhone.checkContactExistenceByPhone(new List<String>{msgSessionId});
        Test.stopTest();
        System.assertEquals(TRUE, conList != NULL);
        
    }*/

    @IsTest
    static void testAssociateMessagingUser(){
        
        Id msgUserId = [SELECT Id FROM MessagingEndUser WHERE MessagingPlatformKey = '+12345678901' LIMIT 1].Id;
        String msgSessionId = [SELECT Id FROM MessagingSession WHERE MessagingEndUserId =: msgUserId LIMIT 1].Id;

        Contact con = [SELECT Id FROM Contact WHERE Phone = '+12345678901' LIMIT 1];
        EinsteinBotAssociateMessagingUser.ContactInfoInput conInfo = new EinsteinBotAssociateMessagingUser.ContactInfoInput();
        conInfo.conRec = con;
        conInfo.routableId = msgSessionId;

        Test.startTest();
            EinsteinBotAssociateMessagingUser.associateMessaginguser(new List<EinsteinBotAssociateMessagingUser.ContactInfoInput>{conInfo});
        Test.stopTest();
        
    }
}