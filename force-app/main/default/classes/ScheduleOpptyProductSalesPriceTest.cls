/* -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            ScheduleOpptyProductSalesPrice.cls 
Description:    This class is a scheduler for OpptyProductSalesPriceUpdate batch class.

Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ------------------------------------------------------------------------------------------------------------------------------------
May 13 2019       1.0                Priya              For CR-02909 : Opportunity Product Sales price update.                                                    
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
@isTest
private class ScheduleOpptyProductSalesPriceTest {
 static testmethod void schedulerTest() {
        String CRON_EXP = '0 0 12 2 * ?';
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new ScheduleOpptyProductSalesPrice());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}