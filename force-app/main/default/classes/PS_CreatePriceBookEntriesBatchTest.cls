/**
 * Name : PS_CreatePriceBookEntriesBatchTest
 * Author : Accenture_Karan
 * Description : Test class used for testing PS_CreatePriceBookEntriesBatch
 * Date :15/01/16
 * Version : <intial Draft>
 */

@istest
class PS_CreatePriceBookEntriesBatchTest {

    static testmethod void testBatchClassNewEntries() {

        

        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        usList[0].Market__c = 'AU';
        usList[0].Line_of_Business__c = 'Higher Ed';
        usList[0].Business_Unit__c = 'Higher Ed';
        usList[0].CurrencyIsoCode = 'AUD';
        usList[0].DefaultCurrencyIsoCode = 'AUD';
        insert usList;

        System.runAs(usList[0]) {

            Test.StartTest();
            Id standardPBId = Test.getStandardPricebookId();
            List<Product2> prodList = TestDataFactory.createProduct(2);
            prodList[0].Market__c = 'AU';
            prodList[0].Line_of_Business__c = 'Higher Ed';
            prodList[0].Business_Unit__c = 'Higher Ed';
            prodList[0].CurrencyISOCode = 'AUD';
            insert prodList;

            PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = prodList[0].Id,UnitPrice = 10000, IsActive = true);
            insert standardPBE;
            Pricebook2 customPB1 = new Pricebook2(Name='Standard Price Book', isActive=true);
            insert customPB1;
            PricebookEntry customPBE1 = new PricebookEntry(Pricebook2Id = customPB1.Id, Product2Id = prodList[0].Id,UnitPrice = 10000, IsActive = true);
            insert customPBE1;

            Pricebook2 customPB2 = new Pricebook2(Name='UK HE All Products Price Book', isActive=true);
            insert customPB2;
			
            PS_CreatePriceBookEntriesBatch b = new PS_CreatePriceBookEntriesBatch();
            database.executebatch(b);

            Test.StopTest();
            system.debug('--->customPB2.Id'+customPB2.Id);
            PricebookEntry createdPBE = [SELECT Id, Product2Id, Pricebook2Id, UnitPrice, IsActive FROM PricebookEntry WHERE Id =: customPBE1.Id];
            System.Assert(createdPBE.IsActive);
            System.AssertEquals(createdPBE.UnitPrice, 10000);
        }

    }
    
    // TODO: Uncomment this method after Spring16 goes live in Production as it uses Test.setCreatedDate
    
    static testmethod void testBatchClassOldEntries() {

        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        usList[0].Market__c = 'AU';
        usList[0].Line_of_Business__c = 'Higher Ed';
        usList[0].Business_Unit__c = 'Higher Ed';
        usList[0].CurrencyIsoCode = 'AUD';
        usList[0].DefaultCurrencyIsoCode = 'AUD';
        insert usList;

        System.runAs(usList[0]) {

            Id standardPBId = Test.getStandardPricebookId();
            List<Product2> prodList = TestDataFactory.createProduct(2);
            prodList[0].Market__c = 'AU';
            prodList[0].Line_of_Business__c = 'Higher Ed';
            prodList[0].Business_Unit__c = 'Higher Ed';
            prodList[0].CurrencyISOCode = 'AUD';
            insert prodList;

            PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = prodList[0].Id,UnitPrice = 10000, IsActive = true);
            insert standardPBE;
            Pricebook2 customPB1 = new Pricebook2(Name='Standard Price Book', isActive=true);
            insert customPB1;
            PricebookEntry customPBE1 = new PricebookEntry(Pricebook2Id = customPB1.Id, Product2Id = prodList[0].Id,UnitPrice = 10000, IsActive = true);
            insert customPBE1;
            Datetime yesterday = Datetime.now().addDays(-2);
            Test.setCreatedDate(customPBE1.Id, yesterday);
			
            Pricebook2 customPB2 = new Pricebook2(Name='UK HE All Products Price Book', isActive=true);
            insert customPB2;
            PricebookEntry customPBE2 = new PricebookEntry(Pricebook2Id = customPB2.Id, Product2Id = prodList[0].Id,UnitPrice = 20000, IsActive = true);
            insert customPBE2;
			System.debug('before customPBE2.UnitPrice= '+customPBE2.UnitPrice);
            Test.StartTest();

            PS_CreatePriceBookEntriesBatch b = new PS_CreatePriceBookEntriesBatch(System.Today() - 1);
            database.executebatch(b);

            Test.StopTest();
			System.debug('After customPBE2.UnitPrice= '+customPBE2.UnitPrice);
            PricebookEntry updatedPBE = [SELECT Id, Product2Id, Pricebook2Id, UnitPrice, IsActive FROM PricebookEntry WHERE Id =: customPBE2.Id];
            System.AssertEquals(updatedPBE.UnitPrice, 10000);
        }

    }
    
}