@isTest
public class BatchUpdatePricebooktoProductTest {
    static testMethod void testMethod1(){
        //Product2
        Product2 newProduct1 = new Product2(Name = 'Test Product1');        
        insert newProduct1;
        
        Product2 newProduct2 = new Product2(Name = 'Test Product1', Pricebook__c='HE Pricebook Austria');        
        insert newProduct2;
        
        //Standard Pricebook 
        Id stdPricebookId = Test.getStandardPricebookId(); 
		insert new PricebookEntry(pricebook2id = stdPricebookId, product2id = newProduct1.id,unitprice=1.0, isActive=true);
        insert new PricebookEntry(pricebook2id = stdPricebookId, product2id = newProduct2.id,unitprice=1.0, isActive=true);
        
        //Pricebook
        Pricebook2 newPricebook1 = new Pricebook2(Name = 'HE Pricebook Austria');
        insert newPricebook1;
        
        Pricebook2 newPricebook2 = new Pricebook2(Name = 'HE Pricebook Benelux');
        insert newPricebook2;
        
        //PricebookEntry
        PricebookEntry newPricebookEntry1 = new PricebookEntry(pricebook2id=newPricebook1.Id, product2id=newProduct1.id,unitprice=1.0, isActive=true); 
        insert newPricebookEntry1;
        
        PricebookEntry newPricebookEntry2 = new PricebookEntry(pricebook2id=newPricebook1.Id, product2id=newProduct2.id,unitprice=1.0, isActive=true); 
        insert newPricebookEntry2;
        
        PricebookEntry newPricebookEntry3 = new PricebookEntry(pricebook2id=newPricebook2.Id, product2id=newProduct2.id,unitprice=2.0, isActive=true); 
        insert newPricebookEntry3;
        
        Test.startTest();
        Database.executeBatch(new BatchUpdatePricebooktoProduct(),50);
        Test.stopTest();
    }
}