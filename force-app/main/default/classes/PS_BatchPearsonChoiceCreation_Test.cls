/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_BatchPearsonChoiceCreation_Test
Description:     Test Class for Pearson Choice Generation.
Date             Version         Author                             Summary of Changes
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
25/07/2015        1.1            Sachin Kadam             Updated code to generate pearson choice for Canada 
10/11/2019        1.2           Manimekalai.M              Replaced Apttus Object with Non-Apttus Object(Replaced     Apttus_Config2__RelatedProduct__c and its fields with RelatedProduct__c and its fields  for CR-02949 )
----------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest(seeAllData=true)
public  class PS_BatchPearsonChoiceCreation_Test
{
    
    static testMethod void  myUnitTest() {     
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        Product2 prd = new Product2();
        List<Product2> prdList = new List<Product2>();
        
        prdList = TestDataFactory.createProduct(2);
        insert prdList;
        prd =  TestDataFactory.insertRelatedProducts();
        
        
        List<Product2> plist=[select id,name,(select id,name,RelatedProduct__c,
                                              RelatedProduct__r.Name, RelatedProduct__r.Market__c from Related_Products__r) from product2 where Market__c='US' and Configuration_Type__c ='Bundle' and
                              Line_of_Business__c = 'Higher Ed' and Business_Unit__c='US Field Sales' and Relevance_value__c>=10 and 
                              relevance_value__c<=100 and Market__c='US'and id=:prd.Id];
        System.runAs(listWithUser[0]){
            Test.startTest(); 
            PS_BatchPearsonChoiceCreation batchObject = new PS_BatchPearsonChoiceCreation('US','US Field Sales','US HE All',prd.Id); 
            batchObject.InitProcessMaps(plist);  
            batchObject.PearsonChoiceCreation(); 
            database.executeBatch(batchObject);
            Test.stopTest();
        }
    }
    
}