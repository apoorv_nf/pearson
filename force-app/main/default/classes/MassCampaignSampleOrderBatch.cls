global class MassCampaignSampleOrderBatch implements Database.Batchable<sObject>
{
    Id CampaignId; 
    List<Campaign> lstCampaigns = new List<Campaign>();
    List<CampaignMember> campmemb = new List<CampaignMember>();
    List<Campaign_product__c> campprod = new List<Campaign_product__c>();
    List<Order> OrdersToInsert;
    List<OrderItem> lstOrderLineItemsToInsert;
    set<id> camcontactids = new set<id>();
    String camShippingMethod; //Oct 11 : Added for the CR-02115
    String camPricebook;
    
    
    global MassCampaignSampleOrderBatch(Campaign[] lstCamp, Set<id> campaigncontids) {
            lstCampaigns = lstCamp;
                for(id temp : campaigncontids){
                camcontactids.add(temp);
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([select id,Name,Do_Not_Send_Samples__c,Global_Marketing_Unsubscribe__c,AccountId,
                               Account.ShippingStreet,Account.ShippingCity,Account.ShippingState,Account.ShippingPostalCode,
                               Account.ShippingCountry,Account.Shipping_Address_Lookup__c,Account.Billing_Address_Lookup__c FROM Contact 
                               where id IN : camcontactids AND Do_Not_Send_Samples__c=FALSE AND 
                               Account.Shipping_Address_Lookup__c !=null AND Account.Billing_Address_Lookup__c !=null AND
                               Global_Marketing_Unsubscribe__c =FALSE AND Account.ShippingStreet!=null AND 
                               Account.ShippingCity!=null AND Account.ShippingState!=null AND Account.ShippingPostalCode!=null 
                               AND Account.ShippingCountry!=null]);
    }
    global void execute(Database.BatchableContext BC, List<Contact> ValidContactlist)
    {
        for(Campaign cam : lstCampaigns){
            CampaignId = cam.Id;
            campmemb = cam.Campaignmembers;
            campprod = cam.campaign_products__r;
            //Oct 11 : Added for the CR-02115
            camShippingMethod = cam.Shipping_Method__c;
            camPricebook = cam.Pricebook__c;
        }
        /*User Usr = new User(); 
        Usr =  [select id,Market__c,Line_of_Business__c,Business_Unit__c,Price_List__c from user 
                where id=:userinfo.getUserId()];
        
        List<Proposal_Pricelist_Mapping__mdt> proPriceList = new List<Proposal_Pricelist_Mapping__mdt>();
        proPriceList = [select id,Order_Price_Book__c,Price_List_Name__c,Rep_Business_Unit__c,Rep_Line_of_Business__c,
                        Rep_Market__c from Proposal_Pricelist_Mapping__mdt];
        
        String userPriceBook;
        for(Proposal_Pricelist_Mapping__mdt ppm : proPriceList){
            if(ppm.Rep_Market__c == Usr.Market__c && ppm.Rep_Line_of_Business__c == Usr.Line_of_Business__c && 
               ppm.Rep_Business_Unit__c == Usr.Business_Unit__c && ppm.Price_List_Name__c == Usr.Price_List__c){
                   userPriceBook = ppm.Order_Price_Book__c;
               }
        }
         */
        
        Set<id> productsIdSet = new Set<id>();
        for(Campaign_Product__c cmpp : campprod){
            productsIdSet.add(cmpp.product__c);
        }
        
        // SOQL on Price Book Entry to populate Unit price and PBE Id while creating OrderItems
        List<PriceBookEntry> priceBookEntryList = [SELECT Id, Product2Id, CurrencyIsoCode, Product2.Name,unitprice 
                                                   FROM PriceBookEntry 
                                                   WHERE Product2Id IN:productsIdSet AND 
                                                   Pricebook2.Name =:Label.PS_CreateSampleOrderPriceBookName AND CurrencyIsoCode =: userinfo.getDefaultCurrency()/*userPriceBook  Label.PS_CreateSampleOrderPriceBookName limit 1 AND CurrencyIsoCode =: userinfo.getDefaultCurrency()*/];
        
        //Oct 11 : Added for the CR-02115
        List<PriceBookEntry> priceBookEntryWEList = [SELECT Id, Product2Id, CurrencyIsoCode, Product2.Name,unitprice 
                                                   FROM PriceBookEntry 
                                                   WHERE Product2Id IN:productsIdSet AND Pricebook2.Name =:camPricebook];
        
        Map<Id, Id> prodPriceBookmap = new Map<Id,Id>();
        Map<Id, Id> prodPriceBookmapWE = new Map<Id,Id>();
        
        for(Id prodid : productsIdSet){
            for(PriceBookEntry pbe : priceBookEntryList){
                if(pbe.Product2Id == prodid){                            
                    prodPriceBookmap.put(prodid,pbe.id);
                }
            }
            for(PriceBookEntry pbeWE : priceBookEntryWEList){
                if(pbeWE.Product2Id == prodid){                            
                    prodPriceBookmapWE.put(prodid,pbeWE.id);
                }
            }
         }
       
        OrdersToInsert = new List<Order>();
        ID standPriceBookId =  [select id from Pricebook2 where name =: Label.PS_CreateSampleOrderPriceBookName limit 1].id;
        ID PriceBookId;
        If (!String.isempty(camPricebook)) PriceBookId =  [select id from Pricebook2 where name =: camPricebook limit 1].id;
        Id sampleOrderRecordTypeId;
        sampleOrderRecordTypeId = PS_Util.fetchRecordTypeByName(Order.SobjectType,PS_Constants.ORDER_SAMPLE_ORDER);
        for(contact c : ValidContactlist){
            Order orderobj = new Order();
            orderobj.RecordTypeId = sampleOrderRecordTypeId;
            orderobj.campaign__c = CampaignId;
            orderobj.Type = 'Sample';
            
            If (!String.isempty(PriceBookId)) {
                orderobj.Pricebook2Id = PriceBookId;
            }
            Else{
                orderobj.Pricebook2Id = standPriceBookId;
            }
            orderobj.AccountId = c.AccountId;
            orderobj.Salesperson__c = userinfo.getUserId();
            orderobj.Selected_Address_Type__c='Account';
            orderobj.Order_Address_Type__c='Account'; // Jun 22nd : added for CRMSFDC-4005
            orderobj.Bill_To_Address__c = c.account.Shipping_Address_Lookup__c;
            orderobj.Ship_To_Address__c = c.account.Billing_Address_Lookup__c;
            orderobj.ShipToContactId = c.Id;
            orderobj.ShippingCity = c.account.ShippingCity;
            orderobj.ShippingCountry = c.account.ShippingCountry;
            orderobj.ShippingState = c.account.ShippingState;
            orderobj.ShippingStreet = c.account.ShippingStreet;
            orderobj.ShippingPostalCode = c.account.ShippingPostalCode;
            If (!String.isempty(camShippingMethod)) orderobj.Shipping_Method__c = camShippingMethod; //Oct 11 : Added for the CR-02115
            orderobj.EffectiveDate = system.today();
            orderobj.Status = 'Open';
            if(Test.isRunningTest())
                orderobj.ERP_Order_Type__c = 'HE-US-SAMPLE';
            OrdersToInsert.add(orderobj);
        }
        //Database.SaveResult[] lstResult = Database.insert(OrdersToInsert,false);
        insert OrdersToInsert;
        if(OrdersToInsert!=null){
            lstOrderLineItemsToInsert = new List<OrderItem>();
            for(order ord : OrdersToInsert){
                for(Campaign_Product__c camproduct : campprod){
                    OrderItem oliObj = new OrderItem();
                    oliObj.Orderid =  ord.id;
                    oliObj.product2id = camproduct.Product__c; 
                    oliObj.Status__c = 'Entered';
                    oliObj.ServiceDate = system.today();
                    oliObj.Quantity = 1;
                    oliObj.UnitPrice = 0.0;
                    //Oct 11 : Added for the CR-02115
                    If (!String.isempty(prodPriceBookmapWE.get(camproduct.Product__c))) {
                        oliObj.PricebookEntryId = prodPriceBookmapWE.get(camproduct.Product__c);
                    }
                    Else{
                        oliObj.PricebookEntryId = prodPriceBookmap.get(camproduct.Product__c);
                    }
                    lstOrderLineItemsToInsert.add(oliObj);
                } 
            }
            if(lstOrderLineItemsToInsert.size()>0){
                insert lstOrderLineItemsToInsert;
            }
            
            Set<id> setCampMembafterOrder = new Set<id>();
            for(order ordersInserted : OrdersToInsert){
                for(Campaignmember cammember : campmemb){
                    if(cammember.Contactid == ordersInserted.ShipToContactId){
                        setCampMembafterOrder.add(cammember.Id);
                    }
                }
            }
            List<Campaignmember> lstCampaignMembToUpdate = new List<Campaignmember>();
            
            //List<CampaignMember> campmembers = [select id,name from CampaignMember where id in:setCampMembafterOrder];
            for(Id cmemberId : setCampMembafterOrder){
                Campaignmember CampaignmemberObj = new Campaignmember();
                CampaignmemberObj.Id = cmemberId;
                CampaignmemberObj.Order_Status__c = 'Sent';
                lstCampaignMembToUpdate.add(CampaignmemberObj);
            }
            update lstCampaignMembToUpdate;
        }
    }
   
    
    
    global void finish(Database.BatchableContext BC) {
    }
}