/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_SendEmailAlertsForOrder.Cls
   Description:     Handler class for sending email notifications for Order object
   Test Class:      PS_SendEmailAlertsForOrderTest
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   01/12/2015         0.1        Accenture - Karan Khanna                Created
   26/02/2016         1.0        Accenture - Davi Borges                 Inclusio  of Buisness Unit at the outbound notification                             
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class PS_SendEmailAlertsForOrder {
    
    public Map<Id,Order> newOrdersMap = new Map<Id,Order>();
    public Map<Id,Order> oldOrdersMap = new Map<Id,Order>();
    public Id sampleOrderRTID;
    public Id revenueOrderRTID;
    
    // Constructor
    public PS_SendEmailAlertsForOrder(Map<Id,Order> newOrdersMap, Map<Id,Order> oldOrdersMap){
        
        this.newOrdersMap = newOrdersMap;
        this.oldOrdersMap = oldOrdersMap;
        sampleOrderRTID = PS_Util.fetchRecordTypeByName(Order.SobjectType,Label.PS_OrderSampleOrderType);
        revenueOrderRTID = PS_Util.fetchRecordTypeByName(Order.SobjectType,Label.PS_OrderRevenueRecordType);
    }
    
    /*---------------------------------------------------------------------------------------------------------------------------------
    Author:         Karan Khanna
    Company:        Accenture
    Description:    This method is used when Order Status is changed to Open
    Inputs:         None
    Returns:        None
    
    <Date>          <Authors Name>      <Brief Description of Change> 
    01/12/2015      Accenture_Karan     Initial version of the code
     
    -------------------------------------------------------------------------------------------------------------------------------*/
    public void StatusChangedToOpen() {
        
        list<Order> openOrderList = new List<Order>();
        System.debug('### newOrdersMap '+ newOrdersMap);
        System.debug('### oldOrdersMap '+ oldOrdersMap);
        try{
            
            for(Order ord : newOrdersMap.Values()) {                        
            if(ord.Status == 'Open' && ord.Status != oldOrdersMap.get(ord.Id).Status) {
                if(ord.Market__c == Label.PS_UKMarket && ord.RecordTypeId == sampleOrderRTID && (ord.Business_Unit__c  == Label.PS_BusinessUnit_HigherEd || ord.Business_Unit__c  == Label.PS_BusinessUnit_ELT)) {
                    openOrderList.add(ord);
                }
                else if(ord.Market__c == Label.PS_AUMarket) {
                    if(ord.RecordTypeId == revenueOrderRTID && (ord.Business_Unit__c  == Label.PS_BusinessUnit_HigherEd || ord.Business_Unit__c  == Label.PS_BusinessUnit_Equella)) {
                        openOrderList.add(ord);
                    }
                    else if((ord.RecordTypeId == sampleOrderRTID || ord.RecordTypeId == revenueOrderRTID) && ( ord.Business_Unit__c  == Label.PS_Business_Unit_PCTA_Affiliated || ord.Business_Unit__c  == Label.PS_BusinessUnit_Equella)) {
                        openOrderList.add(ord);
                    }
                }
                else if(ord.Market__c == Label.PS_NZMarket) {
                    if(ord.RecordTypeId == revenueOrderRTID && (ord.Business_Unit__c  == Label.PS_BusinessUnit_Edify || ord.Business_Unit__c  == Label.PS_BusinessUnit_Equella) && ord.Line_of_Business__c == Label.PS_BusinessUnit_HigherEd) {
                        openOrderList.add(ord);
                    }
                    else if((ord.RecordTypeId == sampleOrderRTID || ord.RecordTypeId == revenueOrderRTID) && (ord.Business_Unit__c  == Label.PS_Business_Unit_PCTA_Affiliated || ord.Business_Unit__c  == Label.PS_BusinessUnit_Equella)) {
                        openOrderList.add(ord);
                    }
                }
            }
            }
            if(!openOrderList.isEmpty()) {
                List<Outbound_Notification__c> notificationList = new List<Outbound_Notification__c>();
                for(Order ord : openOrderList) {
                    Outbound_Notification__c notification = new Outbound_Notification__c();
                    notification.Order__c = ord.Id;
                    notification.Address__c = ord.ShippingStreet + ', ' + ord.ShippingCity + ', ' + ord.ShippingState + ', ' + ord.ShippingPostalCode + ', ' + ord.ShippingCountry;
                    notification.Shipping_Method__c = ord.Shipping_Method__c;
                    notification.Account__c = ord.AccountId;
                    notification.Contact__c = ord.ShipToContactId;
                    notification.User__c = ord.Salesperson__c;
                    notification.Order_Type__c = ord.Type;
                    notification.Business_Unit__c = ord.Business_Unit__c;
                    notification.Type__c = 'Order';
                    notification.Event__c = 'Order Open';
                    notification.To_Email_Address__c = Label.PS_OpenOrderNotification_ToAddress;
                    
                    notificationList.add(notification);
                }
                if(!notificationList.isEmpty()) {
                    insert notificationList;
                }
            }
        }
        catch (exception e) {
            System.Debug('### Exception has occurred in StatusChangedToOpen ' + e);                                        
        }
    }
}