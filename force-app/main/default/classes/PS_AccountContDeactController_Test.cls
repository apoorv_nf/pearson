/**
 * Name : PS_AccountContDeactController_Test 
 * Author : Accenture_Karan
 * Description : Test class used for testing the PS_AccountContactDeactivationController and PS_AccountContactDeactivationHandler
 * Date : 30/09/15 
 * Version : <intial Draft> 
 */


@isTest
public class PS_AccountContDeactController_Test
{
    static testMethod void testAccountContDeactController(){
    
        // Test data preparation
            
        List<Account> accountList = TestDataFactory.createAccount(1,'Organisation');    
        insert accountList ;
        
        List<Contact> contactList=TestDataFactory.createContact(1);
        insert contactList;
        
        List<AccountContact__c> accContList = TestDataFactory.createAccountContact(1, accountList[0].Id, contactList[0].id);    
        insert accContList;
        
        list<UniversityCourse__c> courList = TestDataFactory.insertCourse();
        courList[0].Account__c = accountList[0].Id;
        courList[1].Account__c = accountList[0].Id;
        courList[2].Account__c = accountList[0].Id;
        insert courList;


        List<User> usMarketLSt = TestDataFactory.createUser(Userinfo.getProfileId(),1);
        insert usMarketLSt; 
                    
        list<UniversityCourseContact__c> courContList = TestDataFactory.insertCourseContact(1, courList[0].Id, contactList[0].id);
        insert courContList;
        
        List<Opportunity> optyList = TestDataFactory.createOpportunity(1, 'B2B');
        insert optyList;
        
        
        List<Product2> prodList = TestDataFactory.createProduct(1);
            prodList[0].Market__c = usMarketLSt[0].Market__c;
            prodList[0].Line_of_Business__c = usMarketLSt[0].Line_of_Business__c;
            prodList[0].Business_Unit__c = usMarketLSt[0].Product_Business_Unit__c;
            
            insert prodList;
        
        List<Asset> astList = TestDataFactory.insertAsset(1, courList[0].Id, contactList[0].id, prodList[0].Id, accountList[0].Id);
        
        System.runas(usMarketLSt[0]){
            insert astList;
        }
        
        List<Contact_Product_In_Use__c> cPList = TestDataFactory.insertContactPIU(1, astList[0].Id, contactList[0].id);
        insert cPList;
        
        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =: 'Chatter Free User'].Id,1);
        insert usList; 
        
        
        
        test.startTest();
        
            PageReference pageRef = Page.PS_AccountContactDeactivationPage;
            pageRef.getParameters().put('id', String.valueOf(accContList[0].Id));               
            Test.setCurrentPage(pageRef);
            PS_AccountContactDeactivationController obj = new PS_AccountContactDeactivationController(new ApexPages.StandardController(accContList[0]));
            obj.proceedDeactivation();
            obj.confirmation = true;
            obj.proceedDeactivation();
            System.assertEquals(false, accContList[0].active__c);
                        
            // Logging in as Chatter user to cover Exception scenarios
            system.runAs(usList[0]){
                
                PS_AccountContactDeactivationHandler obhH = new PS_AccountContactDeactivationHandler();         
                obhH.initiateMethodsExecution(accContList[0]);  
            } 
        
        test.stopTest();
    
    }
}