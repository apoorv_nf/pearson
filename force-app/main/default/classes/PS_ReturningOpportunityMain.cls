/*******************************************************************************************************************
 * Apex Class Name  : PS_ReturningOpportunityMain
 * Version          : 1.0
 * Created Date     : 17 November 2015
 * Function         : To check integration validations on Returning opportunity 
 * Modification Log :
 *
 * Developer                   Date                    Description
 * ------------------------------------------------------------------------------------------------------------------
 * Karthik.A.S                17/11/2015              To check integration validations on Returning opportunity 
 * --------------------------------------------------------------------------------------------------------------------
  *******************************************************************************************************************/
public with sharing class PS_ReturningOpportunityMain {

 
    public pageReference openPage() 
    {
       ID Oid;
       Oid = ApexPages.currentPage().getParameters().get('id');
       list<opportunity>oppty=[select id,stagename,RecordType.Name from opportunity where id=:Oid];
      if(Oppty[0].stagename =='Lost' || (Oppty[0].RecordType.Name == 'B2B' && Oppty[0].StageName == 'Closed Lost')){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,system.label.Ps_ReturnigLostOpp + '<a href=/'+oid+' >Click here to return</a>'));
           return null;  
       }
     else if(Oppty[0].stagename !='closed' && Oppty[0].RecordType.Name != 'B2B'){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,system.label.PS_ReturningClose +'<a href=/'+oid+' >Click here to return</a>'));
           return null;  
       }
       
       
       pageReference pg = new pageReference('/apex/rollover?id='+oid);
       pg.setRedirect(true);
       return pg;
       
    }
}