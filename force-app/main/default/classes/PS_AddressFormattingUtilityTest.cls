@isTest
public class PS_AddressFormattingUtilityTest {


    
    static testMethod void PS_AddressFormattingUtilityTestMethod(){
        
        Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
        User u = new User();
        u.LastName = 'territoryuser';
        u.alias = 'terrusr'; 
        u.Email = 'territoryuser@gmail.com';  
        u.Username='territoryuser@gmail.com';
        u.LanguageLocaleKey='en_US'; 
        u.TimeZoneSidKey='America/New_York';
        u.Price_List__c='US HE All';
        u.LocaleSidKey='en_US';
        u.EmailEncodingKey='ISO-8859-1';
        u.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
        u.Geography__c = 'Growth';
        u.CurrencyIsoCode='USD';
        u.Line_of_Business__c = 'Higher Ed';
        u.Market__c = 'US';
        u.Business_Unit__c = 'US Field Sales';
        insert u;    
    	
        System.runAs(u){
            Test.startTest();
          
            String streetAddress = 'GARDEN FLAT    28 LUXOR STREET   18th AVENUE STARBUCK LANE';
            PS_AddressFormattingUtility.standardStreetAddressForGlobalOrders(streetAddress);
            
            PS_AddressFormattingUtility.formatOrderShippingAddress(null,null,streetAddress,false,null,'UK','Higher Ed');
            streetAddress =streetAddress + 'GARDEN FLAT    28 LUXOR STREET   18th AVENUE STARBUCK LANE';
            
            PS_AddressFormattingUtility.formatOrderShippingAddress(null,null,streetAddress,true,null,'UK','Higher Ed');
            
            PS_AddressFormattingUtility.formatOrderShippingAddress(null,null,streetAddress,true,'Account Address','AU','Higher Ed');
            PS_AddressFormattingUtility.standardStreetAddressForGlobalOrders(streetAddress);
            Test.stopTest();
        }
    }

}