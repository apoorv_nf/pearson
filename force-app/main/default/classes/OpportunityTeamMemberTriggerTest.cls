@isTest
private class OpportunityTeamMemberTriggerTest {
    
     static testMethod void myUnitTest() 
     {
        list<Account> lstAccount = new list<Account>();
        
        Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
        User u = new User();
        u.LastName = 'territoryuser';
        u.alias = 'terrusr'; 
        u.Email = 'territoryuser4@pearson.com';  
        u.Username='territoryuser4@pearson.com';
        u.LanguageLocaleKey='en_US'; 
        u.TimeZoneSidKey='America/New_York';
        u.Price_List__c='US HE All';
        u.LocaleSidKey='en_US';
        u.License_Pools__c='Test User';
        u.EmailEncodingKey='ISO-8859-1';
        u.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
        u.Geography__c = 'Growth';
        u.CurrencyIsoCode='USD';
        u.Line_of_Business__c = 'Higher Ed';
        u.Market__c = 'CA';
        u.Business_Unit__c = 'US Field Sales';
        insert u;

       

        //Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
 
        Account objacc =  new Account(Name = 'Account 1', Market2__c = 'ZA', Line_of_Business__c = 'Schools', Geography__c='Growth',IsCreatedFromLead__c = True, ShippingCity ='Shipping City', ShippingCountry = 'United Kingdom', ShippingStreet = 'Shipping Street', ShippingPostalCode = 'NE27 0QQ'); 

         
        lstAccount.add(objacc);

        Account objacc1 =  new Account(Name = 'Account 1', Market2__c = 'ZA', Line_of_Business__c = 'Schools', Geography__c='Growth',IsCreatedFromLead__c = True, ShippingCity ='Shipping City', ShippingCountry = 'United Kingdom', ShippingStreet = 'Shipping Street', ShippingPostalCode = 'NE27 0QQ'); 
            
        lstAccount.add(objacc1);
         system.runas(u){
        insert lstAccount;
       }
          list<Opportunity> lstOpp = new list<Opportunity>();
        Opportunity  objopp = new Opportunity(Name = 'Opp Name',AccountId = objacc.id,StageName='Prospecting',Type='Rollover',
                      CloseDate = system.today(),Selling_Period__c = 'Fall 2017',ForecastCategoryName='Omitted');
        lstOpp.add(objOpp);
         system.runas(u){
        insert lstOpp;
        }
          
        OpportunityTeamMember objteam = new OpportunityTeamMember(Userid = userinfo.getuserid(),TeamMemberRole = 'Support',OpportunityId = objOpp.id);
         system.runas(u){
        insert objteam;
        }
        objteam.TeamMemberRole = 'Team Member';
        system.runas(u){
        update objteam;
        }
        
        OpportunityTeamMember delobjteam = new OpportunityTeamMember(Userid = userinfo.getuserid(),TeamMemberRole = 'Support',OpportunityId = objOpp.id);
          system.runas(u){
         insert delobjteam;
        delete delobjteam;
        }
           
     }
}