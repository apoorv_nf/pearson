@istest(seealldata=false)
private class PS_PrimaryAccountUtilities_Test {
    
    static testMethod void myUnitTest() { 
        
         Profile ProfileNameadmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        List<User> u1 = TestDataFactory.createUser(ProfileNameadmin.Id,1);
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu1'; 
            u1[0].Email ='Opptyproposaltest11@pearson.com'; 
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='Opptyproposaltest12'; 
            u1[0].LanguageLocaleKey ='en_US'; 
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = ProfileNameadmin.Id; 
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName ='standarduserQuoteCreation1@testorg.com';
            u1[0].Market__c ='UK';
            u1[0].Business_Unit__c = 'Higher Ed';
            u1[0].line_of_Business__c = 'Higher Ed';
            u1[0].Product_Business_Unit__c = 'Higher Ed';
            insert u1;
        }
        
        //Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u1[0].id,Disable_Triggers__c=true,Disable_Process_Builder__c=true);
         //Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u1[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true,Disable_Workflow_Rules__c=true); 

        //insert byp;
        
        System.runAs(u1[0]){ 
        //PrimaryAccountUtilities PrmAcctutil = new PrimaryAccountUtilities();
        List<Contact> contLst = TestDataFactory.createContact(1);
        insert contLst;
        
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        insert lstAccount ;
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
        List<Contact> contactLSt = new List<Contact>();
        
        for(Contact contObj : contLst){
            Contact contTempObj = new Contact();
            contTempObj.id = contObj.id;
            contTempObj.Role_Detail__c = 'Mother';
            contTempObj.Role__c = 'Educator';
            contTempObj.Mobile_Last_Updated__c = null;
            contTempObj.isleadConvertedContact__c = false;
            contTempObj.Market__c= 'AU';
            contTempObj.RecordTypeId=recordTypeId; 
            contactLSt.add(contTempObj);
            
        }
        if(!contactLSt.isEmpty()){
            checkRecurssion.run = true;
            update contactLSt;
        }
        
        
        List<AccountContact__c> contLst1 = new List<AccountContact__c>();
        AccountContact__c accCon = new AccountContact__c();
        accCon.Contact__c = contLst[0].id;
        accCon.Account__c = lstAccount[0].id;
        accCon.AccountRole__c = contLst[0].role__c;
        accCon.Primary__c = true;
        accCon.Financially_Responsible__c = true;
        insert accCon;
        contLst1.add(accCon);
   // }
        List<AccountContact__c> contactLSt1 = new List<AccountContact__c>();
        for(AccountContact__c ACCtConObj : contLst1){
            //sai
            AccountContact__c accCont = new AccountContact__c();
            accCont.id = ACCtConObj.id;
            contactLSt1.add(accCont);
        }
        if(!contactLSt1.isEmpty()){
            checkRecurssion.run = true;
            update contactLSt1;
        }
        //sai
        Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
        sAccount.BillingCountry         = 'Australia';
        sAccount.BillingState           = 'Victoria';
        sAccount.BillingCountryCode     = 'AU';
        sAccount.BillingStateCode       = 'VIC';
        sAccount.ShippingCountry        = 'Australia';
        sAccount.ShippingState          = 'Victoria';
        sAccount.ShippingCountryCode    = 'AU';
        sAccount.ShippingStateCode      = 'VIC';
        
        insert sAccount;
        
        Contact sContact                = (Contact)TestClassAutomation.createSObject('Contact');
        sContact.AccountId              = sAccount.Id;
        sContact.FirstName = 'testrec';
        sContact.Role__c = 'Educator';
        sContact.Role_Detail__c = 'Mother';
        sContact.Secondary_Email__c = 'test123@gmail.com';
        sContact.OtherCountry           = 'Australia';
        sContact.OtherState             = 'Victoria';
        sContact.OtherCountryCode       = 'AU';
        sContact.OtherStateCode         = 'VIC';
        sContact.MailingCountry         = 'Australia';
        sContact.MailingState           = 'Victoria';
        sContact.MailingCountryCode     = 'AU';
        sContact.MailingStateCode       = 'VIC';
        sContact.Mobile_Last_Updated__c = null;
        sContact.isleadConvertedContact__c = false;
           insert sContact;
        AccountContact__c accCont = new AccountContact__c();
        accCont.Account__c  = sContact.AccountId;
        accCont.Contact__c = sContact.Id;
        accCont.AccountRole__c = sContact.Role__c;
        accCont.Role_Detail__c = sContact.Role_Detail__c;
        insert accCont;
       
        //sai
        Test.StartTest(); 
        Map<ID, Contact> newConMap = new Map<ID, Contact>([select ID, LastName,role__c,Role_Detail__c,AccountId from Contact where id = :contLst[0].id]);
        Map<ID, Contact> OldConMap = new Map<ID, Contact>([select ID, LastName,role__c,Role_Detail__c,AccountId from Contact where id = :contactLSt[0].id]);
        Map<ID, AccountContact__c> newAccConMap = new Map<ID, AccountContact__c>([select ID,Primary_Account__c,AccountRole__c,Role_Detail__c from AccountContact__c where id = :contLst1[0].id]);
        Map<ID, AccountContact__c> OldAccConMap = new Map<ID, AccountContact__c>([select ID,Primary_Account__c,AccountRole__c,Role_Detail__c from AccountContact__c where id = :contactLSt1[0].id]);
        //sss
        AccountContact__c newac = new AccountContact__c();
            newac.Account__c = contLst[0].AccountId;
            newac.Contact__c = contLst[0].Id;
            newac.AccountRole__c = contLst[0].Role__c;
            newac.Role_Detail__c = contLst[0].Role_Detail__c;
            contactLSt1.add(newac);
        //sss
        
        
        PrimaryAccountUtilities.checkAccountContactExists(contLst);
        PrimaryAccountUtilities.updateRoleInformationOnAccountContact(contLst,contactLSt,newConMap,OldConMap);
        PrimaryAccountUtilities.updateRoleInformationOnContact(contLst1,contactLSt1,newAccConMap,OldAccConMap);
        //   PrimaryAccountUtilities.updateRoleInformationOnContact(lstOldAccountContacts, lstNewAccountContacts, 
        //                                            mapNewIDAccountContact, mapOldIDAccountContact)
        //updateRoleInformationOnContact
        //Ps_ApttusTemplates.ApttusTemplates(Quoteprop);
        // PrmAcctutil.updateRoleInformationOnContact(lstOldAccountContacts, lstNewAccountContacts, mapNewIDAccountContact, mapOldIDAccountContact)
        Test.StopTest();
        }
    }
     static testMethod void myUnitTest2() {
          List<Contact> contLst = TestDataFactory.createContact(1);
        insert contLst;
        
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        insert lstAccount ;
        
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
        List<Contact> contactLSt = new List<Contact>();
        
        for(Contact contObj : contLst){
            Contact contTempObj = new Contact();
            contTempObj.id = contObj.id;
            contTempObj.Role_Detail__c = 'Mother';
            contTempObj.Role__c = 'Parent';
            contTempObj.AccountId = lstAccount[0].id;
            contTempObj.Mobile_Last_Updated__c = null;
            contTempObj.isleadConvertedContact__c = false;
            contTempObj.RecordTypeId=recordTypeId; 
            contactLSt.add(contTempObj);
            
        }
        if(!contactLSt.isEmpty()){
            checkRecurssion.run = true;
            update contactLSt;
        }
          List<AccountContact__c> contLst1 = new List<AccountContact__c>();
        AccountContact__c accCon = new AccountContact__c();
        accCon.Contact__c = contLst[0].id;
        accCon.Account__c = lstAccount[0].id;
        accCon.Primary__c = True;
        accCon.Financially_Responsible__c = true;
         accCon.AccountRole__c = 'Learner';
         accCon.Role_Detail__c = 'Adviser';
        insert accCon;
         accCon.Primary__c = true;
          update accCon;
        contLst1.add(accCon);
         //contactLSt1
          List<AccountContact__c> contactLSt1 = new List<AccountContact__c>();
        for(AccountContact__c ACCtConObj : contLst1){
            //sai
            AccountContact__c accCont = new AccountContact__c();
            accCont.id = ACCtConObj.id;
           // accCont.AccountRole__c =ACCtConObj.AccountRole__c;
            // accCont.Role_Detail__c =ACCtConObj.Role_Detail__c;
            accCont.AccountRole__c ='Educator';
            accCont.Role_Detail__c ='Admissions';
            accCont.Primary__c = true;
            contactLSt1.add(accCont);
        }
        if(!contactLSt1.isEmpty()){
            checkRecurssion.run = true;
            update contactLSt1;
        }
         
        Map<ID, Contact> newConMap = new Map<ID, Contact>([select ID, LastName,role__c,Role_Detail__c,AccountId from Contact where id = :contLst[0].id]);
        Map<ID, Contact> OldConMap = new Map<ID, Contact>([select ID, LastName,role__c,Role_Detail__c,AccountId from Contact where id = :contactLSt[0].id]);
        Map<ID, AccountContact__c> newAccConMap = new Map<ID, AccountContact__c>([select ID,Primary_Account__c,AccountRole__c,Role_Detail__c from AccountContact__c where id = :contactLSt1[0].id ]);
        Map<ID, AccountContact__c> OldAccConMap = new Map<ID, AccountContact__c>([select ID,Primary_Account__c,AccountRole__c,Role_Detail__c from AccountContact__c where id = :contLst1[0].id]);
        // Map<Id , List<AccountContact__c>> accContMap = new  Map<Id , List<AccountContact__c>>();
          List <Contact> contactsToUpdate = new List<Contact>();
         Set<Id> contIdSet = new Set<Id>();
         Map<Id,Contact> mapContact = new Map<Id,Contact>([SELECT id, Role__c, Role_Detail__c FROM Contact WHERE Id in:contIdSet]);
         boolean changed = false;
        PrimaryAccountUtilities.updateRoleInformationOnAccountContact(contLst,contactLSt,newConMap,OldConMap);
        PrimaryAccountUtilities.updateRoleInformationOnContact(contLst1,contactLSt1,newAccConMap,OldAccConMap);
        PrimaryAccountUtilities.checkAccountContactExists(contLst);
     }
        
    
    
}