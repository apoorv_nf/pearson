/*************************************************************
@Name : OpportunityPrimaryContactScheduler 
@Author     : Mayank Lal 
@Description: Schedular class to call batch class to update Primary Contact in Opportunity
@Date       : 21/06/2017
@Version    : 1.0
**************************************************************/ 
global class OpportunityPrimaryContactScheduler implements Schedulable {
    global void execute(SchedulableContext SC) {
        try{
            // Commented and Modified by Navaneeth for CR-02857 - Start
            /*
            OpptyPrimaryContact opp = new OpptyPrimaryContact();
            General_One_CRM_Settings__c  setting = General_One_CRM_Settings__c.getInstance('OpptyPrimaryContactBatchSize'); 
            Database.executebatch(opp,Integer.valueOf(setting.Value__c));
            */
            List<AsyncApexJob> apexJobList = [Select id ,Status, ApexClass.Name,CreatedDate from AsyncApexJob where ApexClass.Name ='OpptyPrimaryContact' and Status IN ('Processing','Queued') Order By CreatedDate DESC];
            if(apexJobList.size()==0){
                OpptyPrimaryContact opp = new OpptyPrimaryContact();
                General_One_CRM_Settings__c  setting = General_One_CRM_Settings__c.getInstance('OpptyPrimaryContactBatchSize'); 
                Database.executebatch(opp,Integer.valueOf(setting.Value__c));
            }
            else {
                // return;
                system.debug('Already a OpptyPrimaryContact job has been processing or queued');
            }
            // Commented and Modified by Navaneeth for CR-02857 - End
        }
        Catch(Exception e){
            System.debug(e);
        }
    }
    
}