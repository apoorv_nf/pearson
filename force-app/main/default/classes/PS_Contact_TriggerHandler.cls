/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_Contact_TriggerHandler.cls
   Description:     Visual force controller class for PS_PreChat page
   Date             Version              Author                          Summary of Changes
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  16-Nov-2015         0.1              Rashmi Prasad                 RD-01473-Create account record
  3/18/2016           0.2              KP                            updated method onBeforeInsert
  13-May-2016         0.3              NB                            Added logic for AccountRequestCreation RecordType
  8/2/2016            0.4              PP                            R6-Added a logic to update Account Market 
  3/24/2017           0.5              C.Perez                       Added Try-Catch for DML Exception notification
  7/2/2018                             Neuraflash (MM)               Updated API Version to 43
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class PS_Contact_TriggerHandler {
    
    /**
     * Description : To create account record when Contact is created
     * @param contactList
     * @return NA
     * @throws NA
    **/
 public void onBeforeInsert(List < Contact > conList) {  
     System.debug('entering on before insert @22');
    Schema.DescribeFieldResult dfr = Contact.sSelfServiceCreated__c.getDescribe();   
    Map<Contact,Account> oMapAccounts = new Map<Contact,Account>();
     System.debug('entering here in contact trigger' + dfr.isAccessible());
     if(dfr.isAccessible() == true){
        String sLicenseName='';
        if(!Test.isRunningTest()){
            List<User> ouserList = [select profile.UserLicense.name from user where id =:UserInfo.getUserId() limit 1];
            sLicenseName=ouserList[0].profile.UserLicense.name;
        }
        for (Contact con : conList) {     
           //if (con.sSelfServiceCreated__c == 'true'){ 
            
           if (con.accountid == null){
               con.sSelfServiceCreated__c = 'true';
               Map <String, PS_ContactRole__c > smapRole = PS_ContactRole__c.getAll();    
               if (smapRole.containsKey(con.role__c)) {      
                   if(smapRole.get(con.role__c).RoleDetail__c != null){
                       con.Role_Detail__c = smapRole.get(con.role__c).RoleDetail__c;
                   }
                   con.role__c = smapRole.get(con.role__c).RoleName__c;               
               }
               
          //     if(sLicenseName != System.Label.Community_User_License && con.ACR_Check_For_GetSupport__c == false){
               if(sLicenseName != System.Label.Community_User_License ){      
                  Id AccCreationReqRecordType = Schema.SObjectType.account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
                  Account oAcc = new Account(Name = con.firstname + ' ' + con.lastname, RecordTypeid = AccCreationReqRecordType,SelfServiceCreated__c=true); 
                  if(con.Market__c != null){
                      oAcc.Market2__c = con.Market__c;
                  }
                  oMapAccounts.put(con,oAcc);
               }               
             }    
        }    //end for
        try{  
            if (!oMapAccounts.isEmpty()){
                insert oMapAccounts.values();
                }   
            for (Contact con: conList){  
               
                if (oMapAccounts.containskey(con)){
                        con.AccountId = oMapAccounts.get(con).Id;            
                }
            }       
        }   
        catch (System.DmlException e) {  OrgWideEmailAddress owa = [select id, DisplayName, Address from OrgWideEmailAddress limit 1];         
            for (Integer i = 0; i < e.getNumDml(); i++) {
                /* CP 04/03/2017 removed lines below so the test coverage would happen
                 String eMessage = e.getDmlMessage(i);
                 ID errorContactID = e.getDmlId(i);*/
                // Send email to the user
                // be sure the user  is '005' and that his profile name does not contain 'Integration' and that the error is Physical Address
                if((string.valueof(UserInfo.getUserId()).startswith('005')) && !(UserInfo.getName().contains('integration')) && e.getDmlMessage(i).contains('Please enter a Physical Street, Physical City, and Physical Zip/Postal Code'))
                {
                    //CP 04/03/2017 removed lines below so the test coverage would happen
                    //String contactURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e.getDmlId(i);
                    //String emailBody  = Label.Physical_Address_Error_Email1 + URL.getSalesforceBaseUrl().toExternalForm() + '/' + e.getDmlId(i) + Label.Physical_Address_Error_Email2;
                    emailHelper.sendEmailToUser(Label.Physical_Address_Error_Email1 + URL.getSalesforceBaseUrl().toExternalForm() + '/' + e.getDmlId(i) + Label.Physical_Address_Error_Email2, e.getDmlId(i) , owa.ID);
                    
                }else{
                    // if it is an integration profile then through the e
                    throw (e);  
                }
                 
            }
            
        } //end catch            
     }//end if
 }//end method
}//end class