/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_EmailMessage_TrggrSequalCtrl.cls 
Description:     Sequence Controller for Email Message Trigger
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
16/11/2015         1.0                Payal Popat                         Initial Release 
24/12/2015         1.1                Sakshi Agarwal                      Removed functions and updated the class for checking trigger context
25/12/2015         1.2                Payal Popat                         Updated to call Case reopened functions
2/15/2016          1.3                KP                                  Amended code for RD-01530
4/19/2016          1.4                Payal Popat                         Put null check for subject in after Insert event
09/05/2017       1.5               MAYANK LAL                Changed the logic for capturing Cc address in incoming mails. CR-01471  
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
Public with sharing class PS_EmailMessage_TrggrSequalCtrl{

    /**
    * Description : Performing all  before Insert operation
    * @param NA
    * @return NA
    * @throws NA
    **/
    public static void beforeInsert(List<EmailMessage> triggeredEmails){
        Set<id> sCaseId = new Set<id>();
        Set<String> sEmailTo = new Set<String>();
        
        List<id> incomingCaseIds = new List<id>();
        Map<Id,String> MapEmailAddresses = new Map<Id,String>();
      
        
        //Check for outgoing emails for case
        for (EmailMessage newEmail: triggeredEmails) {
         if((newEmail.CcAddress!=null && newEmail.CcAddress.length()>0) && (newEmail.ToAddress!=null && newEmail.ToAddress.length()>0)){
            
            newEmail.EmailTo_and_CC_Address__c=newEmail.ToAddress+';'+newEmail.CcAddress;
            }
            else if((newEmail.CcAddress==null /*&& newEmail.CcAddress.length()==0*/) && (newEmail.ToAddress!=null && newEmail.ToAddress.length()>0)){
            newEmail.EmailTo_and_CC_Address__c=newEmail.ToAddress;
            }
            else if((newEmail.CcAddress!=null && newEmail.CcAddress.length()>0) && (newEmail.ToAddress==null /*&& newEmail.ToAddress.length()==0*/)){
            newEmail.EmailTo_and_CC_Address__c=newEmail.CcAddress;
            }
            
            //Check for outgoing emails for case
           //if(newEmail.Incoming == False && newEmail.Status != 'Sent' && String.valueOf(newEmail.ParentId).startsWith('500')){
           if(newEmail.Incoming == False && String.valueOf(newEmail.ParentId)!=null && String.valueOf(newEmail.ParentId).startsWith('500')){
              sCaseId.add(newEmail.ParentId);
              sEmailTo.add(newEmail.ToAddress);
           }           
            //Check for incoming emails for case
            
             if(newEmail.Subject != '' && newEmail.Subject!= null){
                if(newEmail.Incoming == TRUE && newEmail.Subject.contains('ref') && String.valueOf(newEmail.ParentId)!=null && String.valueOf(newEmail.ParentId).startsWith('500'))
                {
                  incomingCaseIds.add(newEmail.ParentId);
                  MapEmailAddresses.put(newEmail.ParentId,newEmail.FromAddress);
                } 
             
             else if(newEmail.Incoming == TRUE && newEmail.HtmlBody!=null && newEmail.HtmlBody != '' && newEmail.HtmlBody.contains('ref') && String.valueOf(newEmail.ParentId)!=null && String.valueOf(newEmail.ParentId).startsWith('500') ) {
                 incomingCaseIds.add(newEmail.ParentId);
                  MapEmailAddresses.put(newEmail.ParentId,newEmail.FromAddress);
             
             }
             else if(newEmail.Incoming == TRUE && newEmail.TextBody!=null && newEmail.TextBody!= ''  && newEmail.TextBody.contains('ref') && String.valueOf(newEmail.ParentId)!=null && String.valueOf(newEmail.ParentId).startsWith('500')) {
                 incomingCaseIds.add(newEmail.ParentId);
                  MapEmailAddresses.put(newEmail.ParentId,newEmail.FromAddress);
             
             }
           }            
        }        
        if(sCaseId.size() > 0){
           //SA: 24/12/2015: Check the validations before sending email to customer
           PS_EmailMessage_TriggerOperations.mValidationonEmailFromAddress(triggeredEmails,sCaseId,sEmailTo);
 
        }  
        if(incomingCaseIds.size() > 0){
          //moved from after trigger to before trigger because failing due to validation rules on reopen cases
           //PP: 25/12/2015: RD-1502: function to reopen case based on 7 days criteria
            String sReturnVal = PS_CaseReopen.mReopenCases(incomingCaseIds,MapEmailAddresses);  
        }           
    }


    /**
    * Description : Performing all  After Insert operation
    * @param NA
    * @return NA
    * @throws NA
    **/ 
    public static void AfterInsert(List<EmailMessage> triggeredEmails){
        Set<id> sCaseId = new Set<id>();
       // List<id> caseIds = new List<id>();
        Map<id,String> MapEmailAddresses = new Map<id,String>();
        Map<id,String> MapEmailAddress = new Map<id,String>();
        List<EmailMessage> emailMessageList = new List<EmailMessage>();
        List<EmailMessage> emailMessageLists = new List<EmailMessage>();
        Map<Id,String> mapCcEmailAddress = new Map<Id,String>();
        
       /* CP 03/02/2017 Removed this code because the validation rules kick before the case is reopened making it fail
         we will move it to the before Insert on the email 
        //PP: 25/12/2015: RD-1502: Checking incoming emails for existing case
        for (EmailMessage newEmail: triggeredEmails) {
            if(newEmail.Subject != '' && newEmail.Subject!= null){
                if(newEmail.Incoming == TRUE && newEmail.Subject.contains('ref') && String.valueOf(newEmail.ParentId).startsWith('500'))
                {
                  CaseIds.add(newEmail.ParentId);
                 // emailMessageLists.add(newEmail);
                  MapEmailAddresses.put(newEmail.ParentId,newEmail.FromAddress);
                } 
             }          
        }
        if(CaseIds.size() > 0){
            //PP: 25/12/2015: RD-1502: function to reopen case based on 7 days criteria
            String sReturnVal = PS_CaseReopen.mReopenCases(CaseIds,MapEmailAddresses);

        }  */     
        

        //SA: 24/12/2015: RD-1530: Checking email to create a case
        for(EmailMessage newEmail: triggeredEmails) {
             if(newEmail.Incoming == TRUE && String.valueOf(newEmail.ParentId)!=null && String.valueOf(newEmail.ParentId).startsWith('500'))
             {
              sCaseId.add(newEmail.ParentId);
              emailMessageList.add(newEmail);
              //MapEmailAddress.put(newEmail.ParentId,newEmail.ToAddress);
              //Added by Mayank for CR-01471
                if(newEmail.ToAddress !=null && newEmail.ToAddress !=''){
                     MapEmailAddress.put(newEmail.ParentId,newEmail.ToAddress);
                 }   
                 if(newEmail.CcAddress !=null && newEmail.CcAddress !=''){
                     mapCcEmailAddress.put(newEmail.ParentId,newEmail.CcAddress);
                 }
             }           
        }
        //SA: 24/12/2015: RD-1530: Calling method to update the To Adress in Email Message
        if(sCaseId.size() > 0){
            PS_EmailMessage_TriggerOperations.mpopulateLatestToAddress(emailMessageList,sCaseId,MapEmailAddress,mapCcEmailAddress);
            PS_EmailMessage_TriggerOperations.mSendEmailWhenCaseIdNotPresent(emailMessageList);
        }
    }  
}//end class