/*****************************************************************************************************************************/
/*CR-0368: Update Opportunity Primary Contact
/*Author:Cognizant
/*Description:This class is getting called from Opportunity trigger for before update and will update the Opportunity primary
Contact.
Date             Version              Author                          Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
06-June-2017         0.1              MAYANK LAL                 Changes for CR-1360
 30-May-2018         0.2             Krishnapriya                Changes for CR-01908- Req 67 to make contact role as primary if there is only one contact role in the Opportunity contact role related list
/*****************************************************************************************************************************/
Public without sharing Class PS_OpptyPrimaryContactHandler
{
    public static void updatePrimary(List<Opportunity> oppList)
    {
        set<id> oppids = new set<id>();
        set<id> ocrids = new set<id>();
        Map<id,id> oppidcontactidmap = new map<id,id>();
        List<OpportunityContactRole> contactRoles = new List<OpportunityContactRole>();
        Boolean checkPrimaryRole = true;
        map<id,List<OpportunityContactrole>> oppConRoleMap = new map<id,List<OpportunityContactrole>>();
        map<id,map<String, List<OpportunityContactRole>>> OppandRoleOpptyConRoleMap = new map<id,map<String, List<OpportunityContactRole>>>();
        List<Primary_Contact_Field_Hierarchy__c> customSetList = new List<Primary_Contact_Field_Hierarchy__c>();
        //Condition to check the closed dates of Opportunities
        for(Opportunity o : oppList){
           // per conversation with Ray and Lori we do not want to restrict based on close date
           // if(o.CloseDate >= Date.today()){                
                oppids.add(o.id);
           // }
        }
        
        // Get Contact Roles
        contactRoles = [Select ContactID, Contact.Name, OpportunityID,isPrimary,role from OpportunityContactRole where OpportunityID in :oppids];
        customSetList = [SELECT ID,Contact_Role__c,Role_level_Number__c,name FROM Primary_Contact_Field_Hierarchy__c  where Object_API_Name__c = 'Opportunity' and IsActive__c=true ORDER BY Role_level_Number__c ASC];
        
        //Check for Primary Opportunity Contact Roles and set the respective Contact to Primary Contact.
        if(contactRoles.size() > 0 && contactRoles !=null) {
            for(OpportunityContactrole ocr:contactRoles){
                //check for primary opportunity contact role
                
                if(!oppConRoleMap.containsKey(ocr.OpportunityId)){
                        List<OpportunityContactrole> lstOppConRole = new List<OpportunityContactrole>();
                        lstOppConRole.add(ocr);
                        oppConRoleMap.put(ocr.OpportunityId, lstOppConRole);
                    }
                    else {
                        List<OpportunityContactrole> lstOppConRole2 = oppConRoleMap.get(ocr.OpportunityId);
                        lstOppConRole2.add(ocr);
                        oppConRoleMap.put(ocr.OpportunityId, lstOppConRole2);
                    }
                    
                
            }
            
            for(id opptyids :oppConRoleMap.keySet()){
                
                //Checking of Opp has single Opp Con Role
                if(oppConRoleMap.get(opptyids).size()==1 && !oppConRoleMap.get(opptyids).isempty()) {
                    for(OpportunityContactrole singleocr:oppConRoleMap.get(opptyids)){
                        oppidcontactidmap.put(singleocr.OpportunityId,singleocr.ContactId);
                    }
                }
                //Checking of Opp has Mutliple Opp Con Role
                else if(oppConRoleMap.get(opptyids).size()>1 && !oppConRoleMap.get(opptyids).isempty()) {
                    
                    map<String, List<OpportunityContactRole>> roleOpptyConRoleMap = new map<String, List<OpportunityContactRole>>();
                    for(OpportunityContactrole singleocr:oppConRoleMap.get(opptyids)){
                        List<OpportunityContactRole> oppConRolList = new List<OpportunityContactRole>();
                        //Change start - 12062017
                        if(singleocr.IsPrimary) {
                            oppidcontactidmap.put(singleocr.OpportunityId, singleocr.ContactId);
                            checkPrimaryRole= false;
                            //roleOpptyConRoleMap.remove(singleocr.Role);
                            if(OppandRoleOpptyConRoleMap.get(singleocr.OpportunityId) !=null ) {
                                OppandRoleOpptyConRoleMap.remove(singleocr.OpportunityId);
                            }
                            break; // to come out of for.   
                            
                        }
                        else {
                            if(roleOpptyConRoleMap.containsKey(singleocr.Role)) {
                                oppConRolList = roleOpptyConRoleMap.get(singleocr.Role);
                            }
                            oppConRolList.add(singleocr);
                            roleOpptyConRoleMap.put(singleocr.Role, oppConRolList); // map of role & list of OppConRole
                        }
                        OppandRoleOpptyConRoleMap.put(opptyids, roleOpptyConRoleMap); //map<id,map<String, List<OpportunityContactRole>>>
                        //Change end - 12062017
                        }
                    
                }
            }// for end id oppty id
            
            
            for(id oppRectoUpdate:OppandRoleOpptyConRoleMap.keySet()){
                map<String, List<OpportunityContactRole>> roleOpptyConMap = new map<String, List<OpportunityContactRole>>();
                roleOpptyConMap = OppandRoleOpptyConRoleMap.get(oppRectoUpdate);
                boolean contactSet = False;
                
                for (Primary_Contact_Field_Hierarchy__c custSetTemp : customSetList) { 
                    if(roleOpptyConMap.containsKey(custSetTemp.Contact_Role__c) && !contactSet){
                        oppidcontactidmap.put(oppRectoUpdate, roleOpptyConMap.get(custSetTemp.Contact_Role__c)[0].ContactId);
                        contactSet = true;
                    }
                }
            }
            
        }
        
        
        
        // Update Primary Contact for Single Opp Con Role/Primary Opp Con Role
        for(Opportunity o : oppList){
            //If condition verifies that there is a contact Role assigned to this Opportunity
            if(oppidcontactidmap.get(o.id) != null)        
            {
                //Assign the custom Contact__c field to the contact ID related to the contactRole of the opportunity
                o.Primary_Contact__c = oppidcontactidmap.get(o.id);
                ocrids.add(oppidcontactidmap.get(o.id));
            }
        }
        
    //Added by Krishnapriya for the CR-01908 Req 67
        List<OpportunityContactRole> opptycr =[SELECT Id, IsPrimary FROM OpportunityContactRole where ContactId in :ocrids and IsPrimary = false ];
        for(OpportunityContactRole opptyocr :opptycr )
        {
          opptyocr.IsPrimary=True;          
        }
       update opptycr;  
       
    } //Method Ends
   }