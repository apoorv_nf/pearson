@isTest(seealldata=true)
public class PS_SetAccountOnLeadConversion_Test
{   
/*RecordType rt1 = [SELECT Id, Name FROM RecordType WHERE SobjectType='Account' and RecordType = 'Corporate'];
RecordType rt2 = [SELECT Id, Name FROM RecordType WHERE SobjectType='Account' and RecordType = 'Higher Education'];
RecordType rt3 = [SELECT Id, Name FROM RecordType WHERE SobjectType='Account' and RecordType = 'School'];
RecordType rt4 = [SELECT Id, Name FROM RecordType WHERE SobjectType='Account' and RecordType = 'Professional'];
RecordType rt5 = [SELECT Id, Name FROM RecordType WHERE SobjectType='Account' and RecordType = 'Organisation'];
RecordType rt6 = [SELECT Id, Name FROM RecordType WHERE SobjectType='Account' and RecordType = 'Learner'];
*/


static testMethod void testSearchTermCleansing() {
    test.StartTest();
        List<User> lstWithTestUser = TestDataFactory.createUser(UserInfo.getProfileId(),1);
             
            for(User newUser : lstWithTestUser)
            {
                newUser.Product_Business_Unit__c = 'CTIMGI';
                newUser.Geography__c='North America';
                newUser.Line_of_Business__c ='Higher Ed';
                newUser.Market__c = 'US'; 
                             
            }          
        insert lstWithTestUser;
        system.RunAs(lstWithTestUser[0]){
    List<Account> AccList= new List<Account>();
        Account newAccount = new Account();
        newAccount.Name='Test-001';
        newAccount.Abbreviated_Name__c = 'Test-1';
        newAccount.Business_unit_from_Lead__c = 'Test'; 
        newAccount.Group_from_Lead__c  = 'Test';
        newAccount.Lead_Pearson_ID_Number__c ='12345';
        newAccount.Lead_Sponsor_Type__c = 'Test';
newAccount.Lead_Sponsor_Address_City__c = 'Test';
newAccount.Lead_Sponsor_Address_State_Province__c = 'KERALA';
newAccount.Lead_Sponsor_Address_Street__c = 'Test';
newAccount.Lead_Sponsor_Address_ZIP_Postal_Code__c = '122332';
newAccount.Lead_Sponsor_Address_Country__c = 'INDIA';
newAccount.BillingStreet = 'Test';  
newAccount.BillingPostalCode = '12345';
newAccount.BillingCountryCode = 'IN';
newAccount.BillingCountry = 'india';
newAccount.BillingCity = 'Test';
newAccount.BillingStateCode= 'AP';
 newAccount.Is_created_from_case__c=false;
newAccount.Lead_Street__c = 'Test'; 
newAccount.Lead_State__c = 'AP'; 
newAccount.Lead_PostalCode__c = '12345'; 
newAccount.Lead_Country__c= 'IN';
newAccount.Lead_City__c = 'Bangalore';
newAccount.Lead_Conversion_Record_Type__c='B2B';
newAccount.Organisation_Type__c = 'Government/Public';
newAccount.IsCreatedFromLead__c=true;
insert newAccount;
AccList.add(newAccount);
PS_SetAccountOnLeadConversion.setAccountFieldsBeforeInsert(AccList);
          //system.RunAs(lstWithTestUser[0]){         
       
}
test.StopTest();
}

static testMethod void testSearchTermCleansing1() {
    List<Account> AccList2= new List<Account>();
Account newAccount1 = new Account();
newAccount1.Name='Test';
newAccount1.Abbreviated_Name__c = 'Test';
newAccount1.Organisation_Type__c = 'School';
newAccount1.Business_unit_from_Lead__c = 'Test'; 
newAccount1.Group_from_Lead__c  = 'Test';
newAccount1.Lead_Pearson_ID_Number__c ='12345';
newAccount1.Business_unit__c  = 'test1';
newAccount1.Group__c ='Test1';
newAccount1.Lead_Pearson_ID_Number__c ='12345';
newAccount1.Lead_Conversion_Record_Type__c='D2L';
newAccount1.ShippingStreet = 'Test';  
newAccount1.ShippingPostalCode = '12345';
newAccount1.ShippingCountryCode = 'IN';
newAccount1.ShippingCountry = 'india';
newAccount1.ShippingCity = 'Test';
newAccount1.ShippingStateCode= 'AP';
newAccount1.Market2__c='UK';
newAccount1.IsCreatedFromLead__c=true;
newAccount1.Type__c = 'test';
insert newAccount1;
 AccList2.add(newAccount1);
 PS_SetAccountOnLeadConversion.setAccountFieldsBeforeInsert(AccList2);
}

static testMethod void testSearchTermCleansing2() {
    List<Account> AccList2 = new List<Account>();
Account newAccount2 = new Account();
newAccount2.Name='Test';
newAccount2.Abbreviated_Name__c = 'Test';
newAccount2.Organisation_Type__c = 'Government/Public';
newAccount2.Business_unit_from_Lead__c = 'Test'; 
newAccount2.Group_from_Lead__c  = 'Test';
newAccount2.Lead_Pearson_ID_Number__c ='12345';
newAccount2.Business_unit__c  = 'test1';
newAccount2.Group__c ='Test1';
newAccount2.Lead_Pearson_ID_Number__c ='12345';
newAccount2.Lead_Conversion_Record_Type__c='B2B';
newAccount2.ShippingStreet = 'Test';  
newAccount2.ShippingPostalCode = '12345';
newAccount2.ShippingCountryCode = 'IN';
newAccount2.ShippingCountry = 'india';
newAccount2.ShippingCity = 'Test';
newAccount2.ShippingStateCode= 'AP';
newAccount2.Market2__c='UK';
newAccount2.IsCreatedFromLead__c=true;
newAccount2.Type__c = 'test';
insert newAccount2;
    AccList2.add(newAccount2);
        //system.RunAs(lstWithTestUser[0]){
        
        //PS_SetAccountOnLeadConversion PS_Set = new PS_SetAccountOnLeadConversion();
   PS_SetAccountOnLeadConversion.setAccountFieldsBeforeInsert(AccList2);
}

static testMethod void testSearchTermCleansing3() {
Account parentaccount = new Account();
parentaccount.Name='Test-002123';
parentaccount.Abbreviated_Name__c = 'Test';
parentaccount.Business_unit_from_Lead__c = 'Test'; 
parentaccount.Business_unit__c  = 'test2';
parentaccount.Market2__c='UK';
parentaccount.IsCreatedFromLead__c=true;
insert parentaccount;
List<Account> AccList3 = new List<Account>();

Account newAccount3 = new Account();
newAccount3.Name='Test-002';
newAccount3.Abbreviated_Name__c = 'Test';
newAccount3.Lead_Primary_Account_Y_N__c = True;
newAccount3.Lead_System_Account_Indicator__c = False;
newAccount3.Lead_Parent_Account_ID__c = parentaccount.id; 
newAccount3.Lead_Line_Of_Business__c = 'Schools';
newAccount3.Lead_Region__c = 'US'; 
newAccount3.Organisation_Type__c = 'Higher Education';
newAccount3.Business_unit_from_Lead__c = 'Test'; 
newAccount3.Business_unit__c  = 'test2';
newAccount3.Group__c ='Test2';   
newAccount3.Lead_Pearson_ID_Number__c ='12345';
newAccount3.Lead_Conversion_Record_Type__c='New_Account_Request';
newAccount3.Lead_Account_Record_Type__c='Professional';   
newAccount3.BillingStreet = 'Test';  
newAccount3.BillingPostalCode = '12345';
newAccount3.BillingCountryCode = 'IN';
newAccount3.BillingCountry = 'india';
newAccount3.BillingCity = 'Test';
newAccount3.BillingStateCode= 'AP'; 
newAccount3.Lead_Physical_Street__c = 'Test';  
newAccount3.Lead_Physical_Postal_Code__c = '12345';
newAccount3.Lead_Physical_Country__c = 'United States';
newAccount3.Lead_Physical_City__c = 'test';
newAccount3.Lead_Physical_State__c= 'California';    
newAccount3.Market2__c='UK';
newAccount3.IsCreatedFromLead__c=true;
newAccount3.Type__c = 'test';
newAccount3.Lead_Primary_Selling_Account__c=parentaccount.id;
newAccount3.Lead_System_Account__c=parentaccount.id;
newAccount3.Pearson_Account_Number__c='';
insert newAccount3;
AccList3.add(newAccount3);
PS_SetAccountOnLeadConversion.setAccountFieldsBeforeInsert(AccList3);
}
}