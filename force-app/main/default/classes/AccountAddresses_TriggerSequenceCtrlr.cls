/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            AccountAddresses_TriggerSequenceCtrlr.cls
   Description:     On Insert/Update of Account Addresses record
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------       -----------------------------------------------------------------------------------------------
    22-Sep-2017        1.0          Navaneeth                   Created the Class for calling another class to Populate Customized Account Address Look Up fieldUdpate Logic on Account Level
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class AccountAddresses_TriggerSequenceCtrlr{

 /**
    * Description : Performing Update Operation on Account Record for Updating Billing Address and Shipping Address lookup fields
    * @param NA
    * @return NA
    * @throws NA
 **/
 public static void afterInsertOrUpdate( List<Account_Addresses__c> AccountAddressesList, Boolean isInsert, Boolean isUpdate )
    {
        Set<Id> UniqueAccountIdSet = new Set<Id>();
            for(Account_Addresses__c AccAdd : AccountAddressesList) 
            {          
                if(AccAdd.Account__c!=null)
                {
                    UniqueAccountIdSet.add(AccAdd.Account__c);
                }
            }
        AccountAddressesUpdateonAccount.afterInsertOrUpdateAccountAddresses(UniqueAccountIdSet);    
    }

/**
    * Description : Performing Update Operation on Account Address Records to Uncheck Primary Flag for old records
    * @param NA
    * @return NA
    * @throws NA
**/
 public static void beforeInsertOrUpdate( List<Account_Addresses__c> AccountAddressesList, Boolean isInsert, Boolean isUpdate )
    {
        /*
        Set<Id> UniqueAccountIdSet = new Set<Id>();
            for(Account_Addresses__c AccAdd : AccountAddressesList) 
            {          
                if(AccAdd.Account__c!=null)
                {
                    UniqueAccountIdSet.add(AccAdd.Account__c);
                }
            }
        //AccountAddressesUpdateonAccount.afterInsertOrUpdateAccountAddresses(UniqueAccountIdSet);
        */
        AccountAddressesUpdate.beforeInsertOrUpdateAccountAddresses(AccountAddressesList);
            
    }
    
}