/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_UserTriggerHandler.class
Description:     On insert/Update of User record 
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
20/01/2015         1.0            Neha Karpe                       R4 - JIT User Insert Handler RD-1619
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public with sharing class PS_UserTriggerHandler{
@future
    public static void addPermSet(String userId) 
    {
        List<PermissionSet> permSetLst= [SELECT ID, Name from PermissionSet];
        User u = [Select ID, PS_ContactRoleOnUser__c from User WHERE ID =: userId];
        Map<String, ID> permSetMap = new Map<String, ID>();
        try{
        for(PermissionSet p: permSetLst)
        {
            permSetMap.put(p.Name,p.ID);
        }
        
        if(!permSetMap.isEmpty())
        {
            if(u.PS_ContactRoleOnUser__c=='Partner')
            {
                PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetID=permSetMap.get('Authenticated_School_Help_Desk') , AssigneeID = userID);
                insert psa;
            }
            else if(u.PS_ContactRoleOnUser__c=='Educator')
            {
                PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetID=permSetMap.get('Authenticated_Professor') , AssigneeID = userID);
                insert psa;
            }
        }
       
        }catch(Exception e){
        }
    }
   @future
   public static void addCaseViewCommunityPermSet(Set<Id> usersId) {
   List<PermissionSetAssignment> permissionSetList = new List<PermissionSetAssignment>();
   PermissionSet psa = [SELECT ID, Name from PermissionSet where name =: System.label.Pearson_Community_Case_view_details];
   for(Id usid :usersId){
   PermissionSetAssignment permissionSetAssign = new PermissionSetAssignment();
   permissionSetAssign.PermissionSetId = psa.id;
   permissionSetAssign.AssigneeId = usid;
   permissionSetList.add(permissionSetAssign);
   } 
    insert permissionSetList;
}
}