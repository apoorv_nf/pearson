/************************************************************************************************************
* Apex Interface Name : PS_FieldChangeValidation
* Version             : 1.0 
* Created Date        : 14 Dec 2015
* Function            : Generic utility which encapsulates logic to detect invalid field changes based on supplied SObject and FieldSet
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Tom Carman            14/Dec/2015            Initial version
-------------------------------------------------------------------------------------------------------------
* Rahul Boinepally      21/Jan/2016       ERP_Order_Number__c field permission added for Order product modifications.   
                                          Term__c field permission added for Order object.  
* Abhinav Srivastava    19/May/2016       Modified class for adding the OrderNum_LineNum__c field to the list of permitted field for editing.

* Anurutheran           08/July/2019      INC4937516-Order_Ship_to_Contact_Email__c field permission added for Order product modifications. 
************************************************************************************************************/




public class PS_FieldChangeValidation {

    private static Map<SObjectType, Set<Schema.DescribeFieldResult>> allObjectFields = new Map<sObjectType, Set<Schema.DescribeFieldResult>>();
    private static Map<String, Set<String>> allEditableFields = new Map<String, Set<String>>();


    public static final String ORDER_EDIT_FIELDS = 'OrderFieldsEditableAfterSubmission';
    public static final String ORDER_ITEM_EDIT_FIELDS = 'OrderItemFieldsEditableAfterSubmission';


    public static List<String> checkForInvalidFieldChanges(SObject newRecord, SObject oldRecord, String fieldSetName) {

        List<String> changedFields = new List<String>();

        Set<String> editableFields = getEditableFields(newRecord.getSObjectType(), fieldSetName);
        
        for(Schema.DescribeFieldResult field : getWriteableFieldNamesForObject(newRecord.getSObjectType())) {

            if(newRecord.get(field.getName()) != oldRecord.get(field.getName())) {
                if(!editableFields.contains(field.getName())) {
                    changedFields.add(field.getLabel());
                }
            }
        }
        return changedFields;

    }




    private static Set<Schema.DescribeFieldResult> getWriteableFieldNamesForObject(Schema.sObjectType objectType) {

        if(!allObjectFields.containsKey(objectType)) {

            Set<Schema.DescribeFieldResult> fieldNames = new Set<Schema.DescribeFieldResult>();

            Map<String, Schema.SObjectField> fieldMap = objectType.getDescribe().fields.getMap();

            for(Schema.SObjectField field : fieldMap.values()) {
                if(field.getDescribe().isUpdateable()) {
                    fieldNames.add(field.getDescribe());
                }
            }

            allObjectFields.put(objectType, fieldNames);
        }
        return allObjectFields.get(objectType);

    }



    // Typically use fieldset, but Order / OrderItem do not current support fieldset.
    private static Set<String> getEditableFields(SObjectType sobjectType, String fieldSetName) {

        String objectFieldSetName = String.valueOf(sObjectType) + fieldSetName;

        if(!allEditableFields.containsKey(objectFieldSetName)) {

            Set<String> editableFields = new Set<String>();

            if(sObjectType == Order.SObjectType && fieldSetName == ORDER_EDIT_FIELDS) {

                editableFields.add('ShippingCountryCode');
                editableFields.add('ShippingStateCode');
                editableFields.add('ShippingPostalCode');
                editableFields.add('ShippingStreet');
                editableFields.add('ShippingCity');
                editableFields.add('Order_Address_Type__c');
                editableFields.add('Purchase_Order_Number__c');
                editableFields.add('Do_Not_Ship_Before_Date__c');
                editableFields.add('Shipping_Instructions__c');
                editableFields.add('Packing_Instructions__c');
                editableFields.add('Shipping_Method__c');
                editableFields.add('Term__c');
                editableFields.add('EndDate');
                editableFields.add('Integration_Request__c');
                
            }

            else if(sObjectType == OrderItem.SObjectType && fieldSetName == ORDER_ITEM_EDIT_FIELDS) {

                editableFields.add('Quantity');
                editableFields.add('Status__c');
                editableFields.add('StatusReason__c');
                editableFields.add('Product2');
                editableFields.add('ERP_Order_Number__c');
                editableFields.add('OrderNum_LineNum__c');
                editableFields.add('Status_Description__c');
                editableFields.add('ERP_Order_Line_Number__c');
                editableFields.add('ServiceDate');
                editableFields.add('EndDate');
                editableFields.add('Scheduled_Ship_Date__c');
                editableFields.add('Item_Number__c');
                editableFields.add('Order_Ship_to_Contact_Email__c');  //INC4937516
            }

            else {

                List<Schema.FieldSetMember> fieldSet = sobjectType.getDescribe().FieldSets.getMap().get(fieldSetName).getFields();

                for(Schema.FieldSetMember field : fieldSet) {
                    editableFields.add(field.getFieldPath());
                }

            }
            
            allEditableFields.put(objectFieldSetName, editableFields);
        }
        

        return allEditableFields.get(objectFieldSetName);

    }


    public static List<String> createErrorMessagesFromFieldLabel(List<String> errorFields) {

        List<String> messages = new List<String>();

        for(String errorField : errorFields ) {
            messages.add('You do not have permission to change \'' + errorField + '\'');
        }

        return messages;

    }



}