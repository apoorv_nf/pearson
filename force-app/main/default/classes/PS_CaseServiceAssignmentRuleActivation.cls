/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_CaseServiceAssignmentRuleActivation.cls 
   Description:     Execute case assignment rules using DMLOPTIONS
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
    10/2/2016         1.0            Payal Popat                       Initial Release 
    16/02/2016        1.1            Rohit Kulkarni                    RD-1563 Escaltion Assignment Rule(mActiveEscalationAssignmentRule)
    26/02/2016        1.2            Rohit Kulkarni                    D-4293 added condition for country change
    7/3/2016          1.3            Payal Popat                       Added condition to activate assignment rule for Sales User Cases
    15/7/2016         1.4            Payal Popat                       Added code to bypass UK Cases
    05/10/2017        1.5            Vishista Madati                   Added condition to activate assignment rule for Inside Sales and Non-Sales User Cases 
-------------------------------------------------------------------------------------------------------------------------------------------------------------*/  
public without sharing class PS_CaseServiceAssignmentRuleActivation{
    public static AssignmentRule oAR; 
    public static Database.DMLOptions dmlOpts; 

    /**
    * Description : Method to check Service Case and activate assignment rule for all cases except Email-to-Case
    * @param List<Case> newCases
    * @return NA
    * @throws NA
    **/
    public static void mActivateAssignmentRule(List<Case> triggeredcases,boolean isInsert,boolean isUpdate,Map<Id,Case> oldMap){        
        //Checking for Service Cases
        List<Case> newcases =  CommonUtils.getCaseServiceRecordType(System.Label.PS_ServiceRecordTypes, triggeredcases);  
        if (newcases != null){
            List<Id> lstCaseId = new List<Id>();//To capture cases other than created through emails.
            //Get Profile Id of "Pearson Sales User" profile
            //CR-01634: Get Profile Ids for "Pearson InsideSales Users" and "Pearson Non-Sales Users" profile
            List<Profile> oProfileId = [Select Id from profile where Name IN (:Label.Pearson_Sales_User_OneCRM, :Label.Pearson_InsideSales_User_OneCRM, :Label.Pearson_Non_Sales_User_USHE)];
            map<String,String> oProfileIds = new map<String,String>();
            for(Profile p: oProfileId){
                oProfileIds.put(p.id,p.id);
            }
            if(isInsert){
                for(Case c: newcases){
                     //Fetch Email//Web origin cases from application or escalated cases
                     if(c.Origin == 'Web'|| c.origin == 'Web-Phone' || (c.Escalation_Point__c != 'Tier 1' && c.Escalation_Point__c != '' && c.Escalation_Point__c != null)||
                        (oProfileIds.containskey(UserInfo.getProfileId()))){
                            System.Debug ('Origin=' + c.Origin + 'Owner=' + c.Owner + ' Case number=' + c.CaseNumber);
                        lstCaseId.add(c.Id);
                    }
                }//end for
            }
            if(isUpdate){
                
                for(Case c: newcases){
                   system.debug('after update test:'+c.origin+';'+c.To_email_origin_address__c+';'+oldMap.get(c.Id).To_email_origin_address__c);                                       
                   if(c.Escalation_Point__c != 'Tier 1' && (c.Escalation_Point__c != oldMap.get(c.Id).Escalation_Point__c || c.Global_Country__c != oldMap.get(c.Id).Global_Country__c) &&
                       c.Escalation_Point__c != '' && c.Escalation_Point__c != null ){
                       lstCaseId.add(c.Id);
                   }
                }//end for
            }
            if(lstCaseId.size()>0){
                mCallAssignmentRule(lstCaseId,isInsert,isUpdate);
            }
        }// end service case to process
     }//end method definition
    /**
    * Description : Method to activate assignment rule for all cases except Email-to-Case
    * @param List<Id> lstCaseId,boolean isInsert,boolean isUpdate
    * @return NA
    * @throws NA
    **/
    public static void mCallAssignmentRule(List<Id> lstCaseId,boolean isInsert,boolean isUpdate){
        String sAssignRuleName= '';
        
        List<Case> listCases=new List<Case>([Select id,RecordType.name,origin,ownerid,CreatedBy.Profile.Name,Account.Market2__c,Escalation_Point__c,to_email_origin_address__c,accountid 
                                            from case where Id in:lstCaseId]);
        //Check the name of assignment rule to be fired based on origin of case
        for(Case objCase: listCases){
            //check for Case Account Market
            if((objCase.Account.Market2__c != System.Label.PS_UKMarket) || 
            (objCase.Account.Market2__c == System.Label.PS_UKMarket && Label.R6AssignmentSwitch.equalsIgnoreCase('off'))){ 
                //check for Cases Created by Pearson Sales User               
                if(isInsert && (objCase.Createdby.Profile.Name == Label.Pearson_Sales_User_OneCRM || objCase.Createdby.Profile.Name == Label.Pearson_InsideSales_User_OneCRM || objCase.Createdby.Profile.Name == Label.Pearson_Non_Sales_User_USHE)){
                        sAssignRuleName = Label.PS_CaseSalesAssigmentRuleName;
                }else{
                    if(objCase.Origin == 'Web' && isInsert && objCase.RecordType.name!='School Assessment')
                        sAssignRuleName = Label.PS_CaseSelfServiceAssignmentRuleName;
                    if(objCase.Origin == 'Web-Phone' && isInsert)
                        sAssignRuleName = Label.PS_CaseSelfServiceAssignmentRuleName;
                    if(objCase.Escalation_Point__c != 'Tier 1'  && objCase.Escalation_Point__c != '' && objCase.Escalation_Point__c != null){
                       sAssignRuleName = Label.PS_CaseEscalationAssignmentRuleName;
                    }
               } 
            }//end not UK market
         }//end for   
        
        //Query Assignment Rule
        if (sAssignRuleName != ''){
            System.debug('Case Assignment Rule:'+sAssignRuleName);
            list<AssignmentRule> assignmentRuleList = [select id from AssignmentRule where SobjectType = 'Case' and 
                                                  Name =: sAssignRuleName limit 1]; 
            if(!assignmentRuleList.isEmpty() && assignmentRuleList != null){
              if(assignmentRuleList[0] != null){
                  oAR= new AssignmentRule();
                  oAR= assignmentRuleList[0];
                  dmlOpts = new Database.DMLOptions();
                  dmlOpts.assignmentRuleHeader.assignmentRuleId= oAR.Id;
              }
            }         
            if(dmlOpts != null){
                for(Case c: listCases){
                    //if(c.Account.Market2__c != System.Label.PS_UKMarket){
                    if((c.Account.Market2__c != System.Label.PS_UKMarket) || 
                    (c.Account.Market2__c == System.Label.PS_UKMarket && Label.R6AssignmentSwitch.equalsIgnoreCase('off'))){                     
                        c.setOptions(dmlOpts);
                     }
                }
                try{
                    if(listCases.size()>0)
                        update listCases;
                }    
                catch(exception e){
                    throw e;}        
            }
        }
   }//end method mCallAssignmentRule
}