public class PS_Article_Vote {
    
      @AuraEnabled
    public static Id createArticle(Integer type, String artId, String name, String comment) { 
        Id ProId =  Id.valueOf(artId);
 		Id Prod = [Select KnowledgeArticleId FROM Customer_Support__kav WHERE PublishStatus = 'Online' AND Language = 'en_US' AND Id =: ProId].KnowledgeArticleId;
        String prodStr = String.valueOf(Prod);
        list<Article_Vote_Summary__c> artSummary = [Select Id,KB_Article_ID__c FROM Article_Vote_Summary__c WHERE KB_Article_ID__c =: prodStr];
        if(artSummary.isEmpty())
        {
            Article_Vote_Summary__c artSum = new Article_Vote_Summary__c();
            artSum.Name = name;
            artSum.KB_Article_ID__c = String.valueOf(Prod);
            try{
                insert artSum;    
            } catch(Exception e)
            {
                System.debug('Error while master data creation');
            }
            
            Id artlId = artSum.Id;  
            return(createChild(artlId,type,comment,artId));
        } else{
            return(createChild(artSummary.get(0).Id,type,comment,artId));
        }
        }

     private static Id createChild(Id kId,integer vot,String comment,String versionId) {
        Article_Votes__c artVote = new Article_Votes__c();
        artVote.Name = versionId;
        artVote.Feedback__c = comment;
        artVote.Vote__c	= Integer.valueOf(vot);
        artVote.KB_Article__c = kId;
         try{
            	insert artVote;   
            } catch(Exception e)
            {
                System.debug('Error while child data creation');
            }
         return artVote.Id;
    }
    
    @AuraEnabled
    public static Id updateChild(String comment, String cid,Integer vote) {
        Id chiId = Id.valueOf(cid);
        Article_Votes__c avlist = [SELECT Id,Name,Vote__c FROM Article_Votes__c where Id =: chiId];
        avlist.Feedback__c = comment;
        avlist.Vote__c = Integer.valueOf(vote);
        update avlist;
        return avlist.Id;
    }
}