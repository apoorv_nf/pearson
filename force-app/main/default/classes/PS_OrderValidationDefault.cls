/************************************************************************************************************
* Apex Interface Name : PS_OrderValidationDefault
* Version             : 1.0 
* Created Date        : 14 Dec 2015
* Function            : Default implementation of PS_OrderValidationInterface for global Orders
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Tom Carman            14/Dec/2015            Initial version
* KP                    7/21/2016              R6 - UK submitted orders. As PS is misspelled was unable to submit order and sync IR id back to order.  
* CP                    7/27/2016              R6 - bypass the submit order validation during IR creation 
* RJ                    8/09/2016              R6 - fix for defect CRMSFDC-2035.          
************************************************************************************************************/


public class PS_OrderValidationDefault implements PS_OrderValidationInterface {
    
    private Map<String, SObject> inRecords;
    private Map<Id, SObject> oldRecords;
    private User contextUser;
    
    private Map<Id, Order> submittedOrders;
    private Map<Id, Order> unSumbittedOrders;
    
    
    
    public void initialize(Map<String, Order> inRecords, Map<Id, Order> oldRecords, User contextUser) {
        
        this.inRecords = inRecords;
        this.oldRecords = oldRecords;
        this.contextUser = contextUser;
        
        submittedOrders = new Map<Id, Order>();
        unSumbittedOrders = new Map<Id, Order>();
        
        if(oldRecords != null) {
            sortOrdersBySubmissionStatus(inRecords, oldRecords);
        }
        
    }
    
    
    public Boolean validateInsert(Map<String,List<String>> exceptions) {
        system.debug('Validating insert operation');
        validateOrderStreetAddress(exceptions);
        return exceptions.isEmpty();
    }
    
    
    public Boolean validateUpdate(Map<String,List<String>> exceptions) {        
        validateSubmittedOrders(exceptions);
        validateOrderStreetAddress(exceptions);
        return exceptions.isEmpty();
        
    }
    
    
    public Boolean validateDelete(Map<String,List<String>> exceptions) {
        return true;
    }
    
    
    /* Sort submitted vs unsubmitted orders based on status */
    
    private void sortOrdersBySubmissionStatus(Map<String, Order> inOrders, Map<Id, Order> oldOrders) {
        
        for(Order order : inOrders.values()) {
            if(PS_Constants.ORDER_SUBMITTED_STATUSES.contains(order.Status) && PS_Constants.ORDER_SUBMITTED_STATUSES.contains(oldOrders.get(order.Id).Status)){ 
               // submittedOrders.put(order.Id, order);
                // CP : bypass the submit order validation during IR creation
                if(!(order.Integration_Request__c != null && (oldOrders.get(order.Id).Integration_Request__c == null))){  
                    submittedOrders.put(order.Id, order);
                 }
            } else {
                unSumbittedOrders.put(order.Id, order);
            }
            
        }
    }
    
    
    
    
    private void validateSubmittedOrders(Map<String,List<String>> exceptions) {
        
        //Can this user edit Orders?
        Boolean permissionToEdit = getUserPermissionToEditSubmittedOrders();
        
        
        for(String orderKey : submittedOrders.keySet()) {
            
            
            List<String> errors = new List<String>();
                if(permissionToEdit) {
                    errors = validateForInvalidFieldChanges((SObject)submittedOrders.get(orderKey), oldRecords.get(orderKey));
                } else {        
                    errors.add(Label.PS_ORDER_EDIT_PERMISSION); 
                }
              
            if(!errors.isEmpty()) {
                exceptions.put(orderKey, errors);
            }
        }
    }
    
    
    private List<String> validateForInvalidFieldChanges(SObject newRecord, SObject oldRecord) {
        
        List<String> invalidFieldsChanged = PS_FieldChangeValidation.checkForInvalidFieldChanges(newRecord, oldRecord, PS_FieldChangeValidation.ORDER_EDIT_FIELDS);
        return PS_FieldChangeValidation.createErrorMessagesFromFieldLabel(invalidFieldsChanged);
        
        
    }
    
    
    private Boolean getUserPermissionToEditSubmittedOrders() {
        
        return (PS_Util.hasUserPermissionSet(contextUser.Id, PS_Constants.PERMISSIONSET_BACKEND_REVENUE) || 
                PS_Util.hasUserPermissionSet(contextUser.Id, PS_Constants.PERMISSIONSET_BACKEND_SAMPLE));
    }
    
    //Method to validate order street address when user submits the order manually
    Private void validateOrderStreetAddress(Map<String,List<String>> exceptions)
    {
        List<String> lstWithErrors = new List<String>();
        String errors;
        for(String orderKey : inRecords.keySet()) 
        {
            errors = validateOrderStreetAddress(inRecords.get(orderKey));
            if(errors != '' && errors != null) 
            {
                lstWithErrors.add(errors);
                exceptions.put(orderKey,lstWithErrors);
            }
        }    
    }
    
    Private String validateOrderStreetAddress(Sobject newOrder)
    {
        List<String> ShippingStreetSplitArray = new List<String>();
        Integer streetSplitSize = 0;
        String errorMsg = '';
        Integer streetLinesCounter = 1;
        String appendComma = '';
        String finalConcatenatedErrorStr ='';
        //R6 bypass of address validation
        Map<Id,Schema.RecordTypeInfo> objOrdRecTypesById = Order.SObjectType.getDescribe().getRecordTypeInfosById();     
        Order ord = (Order) newOrder;
        String recType = objOrdRecTypesById.get(ord.RecordTypeId).getName(); 
        system.debug('order record type:='+recType);
        
        if (recType  != System.Label.GlobalRevOrder && recType != System.Label.GlobalSampleOrder && recType != 'Global Rights Sales Contract'){
        if(newOrder.get('Shippingstreet') != null && newOrder.get('Shippingstreet') != '')
        {
            ShippingStreetSplitArray = ((String)newOrder.get('Shippingstreet')).Split('\n');
            
            if(ShippingStreetSplitArray.size() > 0)
            {
                streetSplitSize = ShippingStreetSplitArray.Size();
                for(String streetAddressSplit : ShippingStreetSplitArray)
                {
                    if((streetAddressSplit.trim()).length() > 30 && streetLinesCounter <= 4)
                    {
                        if(appendComma != '')
                        {
                            errorMsg = errorMsg+appendComma;
                        }
                        errorMsg = errorMsg+String.ValueOf(streetLinesCounter);
                        appendComma = ',';
                    }
                    
                    if(errorMsg != '')
                    {
                        finalConcatenatedErrorStr = 'Line(s)'+' '+errorMsg+' '+Label.PS_OrderStreetAddressBaseErrorMsg;
                    }
                    
                    if(streetLinesCounter >= 5)
                    {
                        if(appendComma != '')
                        {
                            finalConcatenatedErrorStr = finalConcatenatedErrorStr+',';
                        }
                        finalConcatenatedErrorStr = finalConcatenatedErrorStr+Label.PS_OrderStreetGreaterThanFourLines;
                    }
                    
                    streetLinesCounter = streetLinesCounter+1;
                }
                
                system.debug('@@@@@@@@@@ShippingStreetSplitArray'+ShippingStreetSplitArray+'$$$$$$streetSplitSize:'+streetSplitSize+'#####streetLinesCounter'+streetLinesCounter+'!!!!!!!finalConcatenatedErrorStr'+finalConcatenatedErrorStr);
            }   
        }
        }
        return finalConcatenatedErrorStr; 
    }
    
}