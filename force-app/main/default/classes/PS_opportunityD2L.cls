//KP:7-sep-2015:Class to implement methods from opportunity validation interface
//KP:Class holds logic to lock opportunities, Quote/Proposal, Opportunity Products,Product In Use, Events/Tasks on closed D2L opportunities
//RK:added validation rule for registration campus to opportunty D2L record types D - (30/09/2015) 
//KP : added validation rule for locking Activities on Closed opportunity.
//CP:16/10/2015 validation for receiving signed declaration from student before closing opportunity for D2L opportunity
//RK:8th JULY 2016 Modified For DR-0823
//Darshan:17/10/2019-As part of Apttus Decommission, Commented all apttus referenced objects,fields and replaced with Non-Apttus
public class PS_opportunityD2L implements PS_OpportunityValidationInterface{
    //Map variables to hold opportunity new and old map
     @TestVisible private List<Opportunity> newopptys;
     @TestVisible private Map<Id, opportunity> oldopportunities;
     @TestVisible private Map<Id, opportunity> inopportunities;

    //Map variables to hold quote/proposal new and old map
    /* Comented - Darshan
     @TestVisible private Map<Id, Apttus_Proposal__Proposal__c> inproposals;
     @TestVisible private Map<Id, Apttus_Proposal__Proposal__c> Oldproposals;  
    */
    //Apttus-Darshan-START
     @TestVisible private Map<Id, Quote> inproposals;
     @TestVisible private Map<Id, Quote> Oldproposals; 
    //END
    //Map variables to hold opportunity products new and old map
     @TestVisible private Map<Id, OpportunityLineItem> inopptylineitems;
     @TestVisible private Map<Id, OpportunityLineItem> inOldopptylineitems;
    
    //Abhinav:Map variables to hold Events new and old map
     @TestVisible private Map<Id, Event> inNewEvents;
     @TestVisible private Map<Id, Event> inOldEvents;  
     @TestVisible private List<Event> evlists;
     
    //Map variables to hold assets new and old map
     @TestVisible private List<Asset> newAssets;
     @TestVisible private Map<Id, Asset> inNewAssets;
     @TestVisible private Map<Id, Asset> inOldAssets;     
    
    //Map variables to hold tasks new and old map
     @TestVisible private List<Task> newTasks;
     @TestVisible private Map<Id, Task> inNewTasks;
     @TestVisible private Map<Id, Task> inOldTasks;  
    
     @TestVisible private string sEntityType; //variable to hold the entity that is validated
     @TestVisible private string sOperation; //variable to capture DML operation
     public static Map<Id, Opportunity> oppMap; //variable to capture opportunity to be validated
    
    //Oppty validation initialization
    public void opptyinitialize(List<Opportunity> newoppylist,Map<Id,opportunity> inopportunity, Map<Id, opportunity> inOldopportunity){
        newopptys=newoppylist;
        inopportunities=inopportunity;
        oldopportunities=inOldopportunity;
        sEntityType='Opportunity';
        
    }
    
    //Quote/Proposal validation initialization
   /* Commented-Darshan
     public void proposalopptyinitialize(Map<Id,Apttus_Proposal__Proposal__c> inproposal,Map<Id, Apttus_Proposal__Proposal__c> inOldproposal){//,User userContext){
        inproposals=inproposal;
        Oldproposals=inOldproposal;
        sEntityType='Proposal';
        getOpportunityInfo();
        
    }
    */
    //Apttus-Darshan-START
     public void proposalopptyinitialize(Map<Id,Quote> inproposal,Map<Id, Quote> inOldproposal){//,User userContext){
        inproposals=inproposal;
        Oldproposals=inOldproposal;
        sEntityType='Proposal';
        getOpportunityInfo();
        
    }
    //END   
    
    //Opportunity Product validation initialization
    public void opptylineiteminitialize(Map<Id,OpportunityLineItem > inopptylineitem,Map<Id, OpportunityLineItem> inOldopptylineitem,String operation){//,User userContext){
        inopptylineitems=inopptylineitem;
        inOldopptylineitems=inOldopptylineitem;
        sEntityType='OpportunityLineItem';
        sOperation=operation;
        getOpportunityInfo();
        
    }
    
    //Oppty PIU validation initialization
    public void opptyPIUinitialize(Map<Id,Asset> inNewAsset,Map<Id, Asset> inOldAsset,String operation){//,User userContext){
        inNewAssets=inNewAsset;
        inOldAssets=inOldAsset;
        sEntityType='Asset';
        sOperation=operation;
        getOpportunityInfo();
        
    }
        
    //Abhinav: Oppty Event initialization
     public void opptyeventinitialize(List<Event> evlist,Map<Id,Event > inNewEvent,Map<Id, Event> inOldEvent,String operation){//,User userContext){
        evlists = evlist;
        inNewEvents=inNewEvent;
        inOldEvents=inOldEvent;
        sEntityType='Event';
        sOperation=operation;
        getOpportunityInfo();
        
     }
     
     //Oppty Tasks validation initialization   
     public void opptytaskinitialize(List<Task> newTask,Map<Id,Task> inNewTask,Map<Id, Task> inOldTask,String operation){
        newTasks=newTask;
        inNewTasks=inNewTask;
        inOldTasks=inOldTask;
        sEntityType='Task';
        sOperation=operation;
        getOpportunityInfo();        
     }     
     
     //method to pull out Parent opportunity details for Quote/Proposal, Opportunity Products, PIU, Tasks and Events.
    public void getOpportunityInfo(){
        Set<Id> opptoQuery=new Set<Id>();
        if(oppMap == null){
            oppMap = new Map<Id,Opportunity>();
        }
        else
            oppMap.clear();
        //opptoQuery.clear();
       /* Commented-Darshan
       if (sEntityType == 'Proposal'){
            for(Apttus_Proposal__Proposal__c prop:inproposals.values()){
                if (!oppMap.containsKey(prop.Apttus_Proposal__Opportunity__c))
                    opptoQuery.add(prop.Apttus_Proposal__Opportunity__c);
            }
        }
        */
        //Apttus-Darshan-START
        if (sEntityType == 'Proposal'){
            for(Quote prop:inproposals.values()){
                if (!oppMap.containsKey(prop.opportunityId ))
                    opptoQuery.add(prop.opportunityId );
            }
        }
        //END
        else if (sEntityType == 'OpportunityLineItem'){
            if (sOperation == 'Update'){
                for(OpportunityLineItem oli:inopptylineitems.values()){
                    if (!oppMap.containsKey(oli.OpportunityId))
                        opptoQuery.add(oli.OpportunityId);
                }
            }
            else{
                for(OpportunityLineItem oli:inOldopptylineitems.values()){
                    if (!oppMap.containsKey(oli.OpportunityId))
                        opptoQuery.add(oli.OpportunityId);
                }            
            }
        }
        else if (sEntityType == 'Asset'){
            for(Asset piu: inNewAssets.values()){
                if (piu.opportunity__c != null){
                    if (!oppMap.containsKey(piu.opportunity__c))
                        opptoQuery.add(piu.opportunity__c);
                }
            }
                    
        }
        else if (sEntityType == 'Event'){
            if (sOperation == 'Update'|| sOperation == 'Insert'){
                for(Event ev : evlists){
                    if (ev.WhatId != null){
                    if(String.ValueOf((ev.WhatId)).left(3) == '006'){
                        if (!oppMap.containsKey(ev.WhatId))
                            opptoQuery.add(ev.WhatId);
                    }}
                }
            }
            else{
                for(Event ev:inOldEvents.values()){
                    if (ev.WhatId != null){
                    if(String.ValueOf((ev.WhatId)).left(3) == '006'){
                        if (!oppMap.containsKey(ev.WhatId))
                            opptoQuery.add(ev.WhatId);
                    }}
                }            
            }
        }
        else if (sEntityType == 'Task'){
            if (sOperation == 'Update' || sOperation == 'Insert'){
                for(Task act: newTasks){
                    if (act.WhatId != null){
                    if(String.ValueOf((act.WhatId)).left(3) == '006'){
                        if (!oppMap.containsKey(act.WhatId))
                            opptoQuery.add(act.WhatId);
                    }}
                }
            }
            else{
                for(Task act:inOldTasks.values()){
                    if (act.WhatId != null){
                    if(String.ValueOf((act.WhatId)).left(3) == '006'){
                        if (!oppMap.containsKey(act.WhatId))
                            opptoQuery.add(act.WhatId);
                    }}
                }            
            }
        }           
        //Fetch opportunity details        
        if (!opptoQuery.isEmpty()){
            List<Opportunity> opplist = [select id,isClosed,stagename,recordtypeId,recordtype.name from opportunity where id in : opptoQuery];
            if (opplist != null)
                oppMap.putAll(new Map<Id,Opportunity>(opplist));
        }
    }
    
    //Method to validate opportunity
    public Boolean opptyvalidateUpdate(Map<String,List<String>> exceptions){
        Boolean isSalesUser=PS_Util.isUserPearsonSalesUser();
        if (sEntityType == 'Opportunity'){
                if (inopportunities != null){
                    for(Id Opptykey:inopportunities.keySet()){
                        List<String> errors = validateUpdateSingleOppty(Opptykey,'Opportunity',isSalesUser);  
                        if (!errors.isEmpty()){
                            exceptions.put(Opptykey, errors);
                        }
                    } 
                }   
                else{
                   for(integer i=0;i<newopptys.size();i++){
                        List<String> errors = validateUpdateSingleOppty(i,'Opportunity',isSalesUser);
                        if (!errors.isEmpty()){
                            exceptions.put(string.valueof(i), errors);
                        }                    
                   }                    
                }   
        } 
        else{       
        if (!oppMap.isEmpty()){
       
            if (sEntityType == 'Proposal'){   
                for(Id propKey: inproposals.keySet()){
                    List<String> errors = validateUpdateSingleOppty(propKey,'Proposal',isSalesUser);
                    if (!errors.isEmpty()){
                        exceptions.put(propKey, errors);
                    }                
                }  
            }     
            else if (sEntityType == 'OpportunityLineItem'){   
                if (sOperation == 'Update'){
                    for(Id oliKey: inopptylineitems.keySet()){
                        List<String> errors = validateUpdateSingleOppty(oliKey,'OpportunityLineItem',isSalesUser);
                        if (!errors.isEmpty()){
                            exceptions.put(oliKey, errors);
                        }                
                    }  
                }
                else{
                    for(Id oliKey: inOldopptylineitems.keySet()){
                        List<String> errors = validateUpdateSingleOppty(oliKey,'OpportunityLineItem',isSalesUser);
                        if (!errors.isEmpty()){
                            exceptions.put(oliKey, errors);
                        }                
                    }
                }
            }     
            else if (sEntityType == 'Asset'){   
                for(Id piuKey: inNewAssets.keySet()){
                        List<String> errors = validateUpdateSingleOppty(piuKey,'Asset',isSalesUser);
                        if (!errors.isEmpty()){
                            exceptions.put(piuKey, errors);
                        }                
                 }
             }             
             
            else if (sEntityType == 'Event'){   
                if (sOperation == 'Update'){
                    for(Id evKey: inNewEvents.keySet()){               
                        List<String> errors = validateUpdateSingleOppty(evKey,'Event',isSalesUser);
                        if (!errors.isEmpty())
                            exceptions.put(evKey, errors);                
                    }  
                }
                else if (sOperation == 'Insert'){
                    for(integer i=0;i<evlists.size();i++){
                        List<String> errors = validateUpdateSingleOppty(i,'Event',isSalesUser);
                        if (!errors.isEmpty()){
                            exceptions.put(string.valueof(i), errors);
                        }                    
                    }            
                }
                else{                
                    for(Id evKey: inOldEvents.keySet()){
                        List<String> errors = validateUpdateSingleOppty(evKey,'Event',isSalesUser);
                        if (!errors.isEmpty()){
                            exceptions.put(evKey, errors);
                        }                
                    }
                }
            }                  
            else if (sEntityType == 'Task'){
                if (sOperation == 'Update'){                
                    for(Id actKey: inNewTasks.keySet()){
                        List<String> errors = validateUpdateSingleOppty(actKey,'Task',isSalesUser);
                        if (!errors.isEmpty()){
                            exceptions.put(actKey, errors);
                        }                
                    }  
                }
                else if (sOperation == 'Insert'){
                    for(integer i=0;i<newTasks.size();i++){
                        List<String> errors = validateUpdateSingleOppty(i,'Task',isSalesUser);
                        if (!errors.isEmpty()){
                            exceptions.put(string.valueof(i), errors);
                        }                    
                    }
                }
                else{
                    for(Id actKey: inOldTasks.keySet()){
                        List<String> errors = validateUpdateSingleOppty(actKey,'Task',isSalesUser);
                        if (!errors.isEmpty()){
                            exceptions.put(actKey, errors);
                        }                
                    }
                }
            }                    
        }//end oppty map chk
        }
        return true;
    }
    @TestVisible private List<String> validateUpdateSingleOppty(integer key,String sEntityType,Boolean usertoexec){
        List<String> errors =  new List<String>();
        // Raushan kumar : Adding validation for Registration campus
        if (sEntityType == 'Opportunity'){
                Opportunity updatedoppt=newopptys[key];
                //Opportunity oldoppt = oldopportunities.get(key);
                if((updatedoppt.Received_Signed_Registration_Contract__c==TRUE)&&(updatedoppt.PS_Registration_Campus__c == NULL)&&(updatedoppt.Type=='New Business')&& (updatedoppt.RecordTypeId==PS_Util.fetchRecordTypeByName(Schema.getGlobalDescribe().get('Opportunity'), 'D2L')))
                errors.add('Please select a Registration Campus for saving the record');
           // Chaitra 16/10/2015  Adding validation for recieving signed declaration before opportunity is closed    
                if(updatedoppt.StageName =='Closed' && updatedoppt.Signed_Enrolement_Declaration_Recieved__c==false && updatedoppt.Market__c =='ZA' && (updatedoppt.RecordTypeId==PS_Util.fetchRecordTypeByName(Schema.getGlobalDescribe().get('Opportunity'), 'D2L')))
                errors.add('The opportunity cannot be closed unless Signed Enrolement Declaration is recieved');
                
        } 
                
        if (!usertoexec){
           /* if (sEntityType == 'Task'){
                Task newtasktochk = newTasks[key];
                if (oppMap.containsKey(newtasktochk.whatId)){
                    if (oppMap.get(newtasktochk.whatId).IsClosed == true && oppMap.get(newtasktochk.whatId).recordtype.name=='D2L')
                        errors.add('You cannot add Tasks associated to a closed Opportunity. Please use the Amendment or Returning Opportunity respective to your business scenario.');
                }
            }*/     // Commented By Raushan for DR-0823 8th July 
            if (sEntityType == 'Event'){
                Event newevtochk = evlists[key];
                if (oppMap.containsKey(newevtochk.whatId)){
                    if (oppMap.get(newevtochk.whatId).IsClosed == true && oppMap.get(newevtochk.whatId).recordtype.name=='D2L')
                        errors.add('You cannot add Events associated to a closed Opportunity. Please use the Amendment or Returning Opportunity respective to your business scenario.');
                }
            }            
        }
        return errors;
    }
    
    //Method definition to support update/delete events
    @TestVisible private List<String> validateUpdateSingleOppty(Id key,String sEntityType,Boolean usertoexec){
        List<String> errors =  new List<String>();
        // Raushan kumar : Adding validation for Registration campus
        if (sEntityType == 'Opportunity'){
                Opportunity updatedoppt = inopportunities.get(key);
                Opportunity oldoppt = oldopportunities.get(key);
                if((updatedoppt.Received_Signed_Registration_Contract__c==TRUE)&&(updatedoppt.PS_Registration_Campus__c == NULL)&&(updatedoppt.Type=='New Business')&& (updatedoppt.RecordTypeId==PS_Util.fetchRecordTypeByName(Schema.getGlobalDescribe().get('Opportunity'), 'D2L')))
                errors.add('Please select a Registration Campus for saving the record');
                // Chaitra 16/10/2015  Adding validation for recieving signed declaration before opportunity is closed
                if(updatedoppt.StageName =='Closed' && updatedoppt.Signed_Enrolement_Declaration_Recieved__c==false && updatedoppt.Market__c =='ZA'&& (updatedoppt.RecordTypeId==PS_Util.fetchRecordTypeByName(Schema.getGlobalDescribe().get('Opportunity'), 'D2L')))
                errors.add('The opportunity cannot be closed unless Signed Enrolement Declaration is recieved');
        }
        if (!usertoexec){
            if (sEntityType == 'Opportunity'){
                Opportunity updatedopp = inopportunities.get(key);
                Opportunity oldopp = oldopportunities.get(key);
                
                if (oldopp.IsClosed == true && (updatedopp.void__c==oldopp.void__c) && 
                    updatedopp.RecordTypeId==PS_Util.fetchRecordTypeByName(Schema.getGlobalDescribe().get('Opportunity'), 'D2L'))
                    errors.add('You cannot edit a closed Opportunity. Please use the Amendment or Returning Opportunity respective to your business scenario.');                
                
            }
            /* Commented-Darshan
            else if (sEntityType == 'Proposal'){   
                Apttus_Proposal__Proposal__c updatedprop = inproposals.get(key);
                Apttus_Proposal__Proposal__c oldprop = Oldproposals.get(key);
                if(oppMap.containsKey(updatedprop.Apttus_Proposal__Opportunity__c)){
                    if (oppMap.get(updatedprop.Apttus_Proposal__Opportunity__c).IsClosed == true && 
                        oppMap.get(updatedprop.Apttus_Proposal__Opportunity__c).recordtype.name=='D2L')
                        errors.add('You cannot edit an existing Quote on a closed Opportunity. Please use the Amendment or Returning Opportunity respective to your business scenario.');
                }
            }
            */
            //Apttus-Darshan-START
            else if (sEntityType == 'Proposal'){   
                Quote updatedprop = inproposals.get(key);
                Quote oldprop = Oldproposals.get(key);
                if(oppMap.containsKey(updatedprop.opportunityId )){
                    if (oppMap.get(updatedprop.opportunityId ).IsClosed == true && 
                        oppMap.get(updatedprop.opportunityId ).recordtype.name=='D2L')
                        errors.add('You cannot edit an existing Quote on a closed Opportunity. Please use the Amendment or Returning Opportunity respective to your business scenario.');
                }
            }
            //END
            else if (sEntityType == 'OpportunityLineItem'){   
                OpportunityLineItem updatedoli;
                if (inopptylineitems != null)
                    updatedoli = inopptylineitems.get(key);
                OpportunityLineItem oldoli = inOldopptylineitems.get(key);
                if (sOperation == 'Update'){
                    if (oppMap.get(updatedoli.OpportunityId).IsClosed == true && oppMap.get(updatedoli.OpportunityId).recordtype.name=='D2L')
                        errors.add('You cannot edit existing line items associated to a closed Opportunity. Please u use the Amendment or Returning Opportunity respective to your business scenario.');
                }
                else{
                    if (oppMap.get(oldoli.OpportunityId).IsClosed == true && oppMap.get(oldoli.OpportunityId).recordtype.name=='D2L')
                        errors.add('You cannot delete existing line items associated to a closed Opportunity. Please use the Amendment or Returning Opportunity respective to your business scenario.');
                }
            }
            else if (sEntityType == 'Asset'){   
                Asset updatedpiu = inNewAssets.get(key);
                if (updatedpiu.opportunity__c != null){
                    if (oppMap.get(updatedpiu.opportunity__c).IsClosed == true && 
                        oppMap.get(updatedpiu.opportunity__c).recordtype.name=='D2L')
                        errors.add('You cannot edit products associated to a closed Opportunity. Please use the Amendment or Returning Opportunity respective to your business scenario.');
                }
            }            
            else if (sEntityType == 'Event'){   
                Event updatedevnt;
                Event oldevnt;
                if (inNewEvents != null)
                    updatedevnt = inNewEvents.get(key);
                    //newevnt = evlist.get(key);
                if (inOldEvents != null)
                    oldevnt = inOldEvents.get(key);
                if (sOperation == 'Update'){
                    if(oppMap.containsKey(updatedevnt.WhatId)){
                    if (oppMap.get(updatedevnt.WhatId).IsClosed == true && oppMap.get(updatedevnt.WhatId).recordtype.name=='D2L')
                        errors.add('You cannot edit Events associated to a closed Opportunity. Please use the Amendment or Returning Opportunity respective to your business scenario.');
                    }
                }
                else{
                    if(oppMap.containsKey(oldevnt.WhatId)){
                    if (oppMap.get(oldevnt.WhatId).IsClosed == true && oppMap.get(oldevnt.WhatId).recordtype.name=='D2L')
                        errors.add('You cannot delete Events products associated to a closed Opportunity. Please use the Amendment or Returning Opportunity respective to your business scenario.');
                    }
                }
            }           
            else if (sEntityType == 'Task'){   
                Task updatedact;
                Task oldact;
                if (inNewTasks != null)
                    updatedact= inNewTasks.get(key);
                if (inOldTasks != null)
                    oldact = inOldTasks.get(key);
                if (sOperation == 'Update'){
                    if(oppMap.containsKey(updatedact.WhatId)){
                    if (oppMap.get(updatedact.WhatId).IsClosed == true && oppMap.get(updatedact.WhatId).recordtype.name=='D2L')
                        errors.add('You cannot edit Tasks associated to a closed Opportunity. Please use the Amendment or Returning Opportunity respective to your business scenario.');
                    }
                }
                else{
                    if(oppMap.containsKey(oldact.WhatId)){
                    if (oppMap.get(oldact.WhatId).IsClosed == true && oppMap.get(oldact.WhatId).recordtype.name=='D2L')
                        errors.add('You cannot delete Tasks associated to a closed Opportunity. Please use the Amendment or Returning Opportunity respective to your business scenario.');
                    }
                }
            }                           
        }//end user profiles to execute
        return errors;
    }
}