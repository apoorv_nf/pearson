@isTest
    public with sharing class PS_OpportunityValidationFactoryTest{ 
        
        Static testMethod void Eventtest()
        {
            Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
            sAccount.BillingCountry         = 'Australia';
            sAccount.BillingState           = 'Victoria';
            sAccount.BillingCountryCode     = 'AU';
            sAccount.BillingStateCode       = 'VIC';
            sAccount.ShippingCountry        = 'Australia';
            sAccount.ShippingState          = 'Victoria';
            sAccount.ShippingCountryCode    = 'AU';
            sAccount.ShippingStateCode      = 'VIC';
            sAccount.IsCreatedFromLead__c   = True;
            sAccount.Market__c        ='AU';
            sAccount.Line_of_Business__c    = 'Clinical';
            sAccount.Geography__c           = 'Core';
            insert sAccount;
            
            List<string>StrLst = new list<string>{'a','b','c'};
            Map<String,List<String>> exceptions = new Map<String,List<String>>();
            exceptions.put('a',StrLst);
            
            
            map<id,DateTime> csmap=new map<id,DateTime>();
            map<id,String> cobmap=new map<id,string>();
            map<Id, DateTime> cemap=new map<Id, DateTime>();
            map<id,String> cotomap=new map<id,String>();
             
                       
                map<Id,Event> mapev =new map<Id,Event>();
                list<Event> oldevent=new list<Event>();
                list<Event> updatedEvent=new list<Event>();
                Event sEvent1= new Event();         
                Event sEvent2= new Event();          
                sEvent1.EndDateTime = system.now().addHours(3);
                sEvent1.StartDateTime = system.now().addHours(2);
                sEvent1.DurationInMinutes = null;
                sEvent1.Status__c = 'Completed' ;
                
                sEvent1.OwnerId = userinfo.getUserId();
                sEvent1.Location = 'USA';
                insert sEvent1;
                
                mapev.put(sEvent1.Id,sEvent1);
                oldevent.add(sEvent1);
              
                sEvent2.EndDateTime = system.now().addHours(3);
                sEvent2.StartDateTime = system.now().addHours(2);
                sEvent2.DurationInMinutes = null;
                sEvent2.Status__c = 'Completed' ;
                //sEvent2.whatId = c.id;
                sEvent2.OwnerId = userinfo.getUserId();
                sEvent2.Location = 'USA';
                insert sEvent2;
                mapev.put(sEvent2.Id,sEvent2);
                oldevent.add(sEvent2);
                
                upsert oldevent;

                sEvent1.Location = 'test';
                sEvent1.EndDateTime = system.now().addHours(5);
                sEvent1.StartDateTime = system.now().addHours(3);
                //sEvent1.whatId = c.id;
                update sEvent1;
                 updatedevent.add(sEvent1);
                 update(updatedevent);
               
                 map<id,event> utilmap=new map<id,event>();
                 PS_Util.addErrors_1(utilmap,exceptions );
                
                PS_Util.addErrors_1(mapev,exceptions );
               
              EventTriggerHandler tests = new EventTriggerHandler(true,1);
              system.assertequals(tests.bIsVisualforcePageContext,false);
              system.assertequals(tests.bIsWebServiceContext,false);
              system.assertequals(tests.bIsExecuteAnonymousContext,false);
              system.assertequals(tests.bIsTriggerContext,true);
              Test.startTest();
              EventTriggerHandler e =new EventTriggerHandler(true,1);
              e.OnUndelete(oldevent); 
              e.OnAfterUpdate(oldevent,updatedEvent,mapev);
              e.OnBeforeDelete(updatedEvent,mapev);
              
              PS_OpportunityValidationFactory.OpptyEventValidations(updatedEvent,mapev,mapev,'Insert');
              
              Test.stopTest();
            delete sEvent1; 
             undelete sEvent1;  
            }
            
            static testMethod void OpportunityandLineItemTriggersTest()
            {
            List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
            insert usrLst;
            Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
            insert byp;    
                System.runas(usrLst[0]){
                
                TestClassAutomation.FillAllFields = true;
                RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'School'];
                Account sAccount        = (Account)TestClassAutomation.createSObject('Account');
                sAccount.BillingCountry      = 'Australia';
                sAccount.BillingState      = 'Victoria';
                sAccount.BillingCountryCode    = 'AU';
                sAccount.BillingStateCode    = 'VIC';
                sAccount.ShippingCountry    = 'Australia';
                sAccount.ShippingState      = 'Victoria';
                sAccount.ShippingCountryCode  = 'AU';
                sAccount.ShippingStateCode    = 'VIC';
                sAccount.Lead_Sponsor_Type__c   = null;
                sAccount.IsCreatedFromLead__c = true;
                sAccount.ShippingPostalCode = '234543';
                sAccount.ShippingStreet = 'TestStreet';
                sAccount.ShippingCity = 'Vns';
                sAccount.recordtypeId=OrgAcc.Id ;
                sAccount.PS_AccountSegment1__c='2';
                
                insert sAccount;
                
                List<Opportunity> Optlt = new list<Opportunity>();
                Map<id,Opportunity> OptMp = new Map<id,Opportunity>();            
                Map<id,Opportunity> oldOptMp = new Map<id,Opportunity>();
                
                Opportunity sOpportunity= (Opportunity)TestClassAutomation.createSObject('Opportunity');
                sOpportunity.AccountId= sAccount.Id;
                sOpportunity.Enquiry_Type__c = '';
                sOpportunity.RecordTypeId= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
                /*
                sOpportunity.Selling_Period__c = 'None';
                sOpportunity.Cadence__c = 'Monthly';
                sOpportunity.Adoption_Type__c = 'Committee';
                sOpportunity.Bundled_Pricing__c=True;
                */
                insert sOpportunity;
                
                //Optlt.add(sOpportunity);
                /*
                Opportunity sOpportunity1= (Opportunity)TestClassAutomation.createSObject('Opportunity');
                sOpportunity1.AccountId= sAccount.Id;
                sOpportunity1.Enquiry_Type__c = '';
                sOpportunity1.RecordTypeId= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('D2L').getRecordTypeId();
                sOpportunity1.Selling_Period__c = 'Summer';
                sOpportunity.Cadence__c = 'Monthly';
                sOpportunity1.Adoption_Type__c = 'Test2';
                sOpportunity1.Bundled_Pricing__c=False;
                Optlt.add(sOpportunity1);
                //insert sOpportunity1;
                */
                //insert Optlt;
                /*
                Opportunity sOpportunity2= (Opportunity)TestClassAutomation.createSObject('Opportunity');
                sOpportunity2.AccountId= sAccount.Id;
                sOpportunity2.Enquiry_Type__c = '';
                sOpportunity2.RecordTypeId= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
                sOpportunity2.Selling_Period__c = 'Spring';
                sOpportunity.Cadence__c = 'Monthly';
                sOpportunity2.Adoption_Type__c = 'Test1';
                sOpportunity2.Bundled_Pricing__c=False;
                insert sOpportunity2;
                */
                OptMp.put(sOpportunity.id,sOpportunity);
                oldOptMp.put(sOpportunity.id,sOpportunity);
                
                //Product2 sProduct= (Product2)TestClassAutomation.createSObject('Product2');
                
                Product2 sProduct= TestDataFactory.createProduct(1)[0];
                sProduct.RecordTypeId= Schema.SObjectType.Product2.getRecordTypeInfosByName().get('MDM Global Product').getRecordTypeId();
                insert sProduct;
                
                
                PriceBookEntry sPriceBookEntry      = (PriceBookEntry)TestClassAutomation.createSObject('PriceBookEntry');
                sPriceBookEntry.IsActive        = true;
                sPriceBookEntry.Product2Id        = sProduct.Id;
                sPriceBookEntry.Pricebook2Id      = Test.getStandardPricebookId();
                sPriceBookEntry.UnitPrice        = 34.95;
                insert sPriceBookEntry;
                
                Pricebook2 sPriceBookANZ = new Pricebook2();
                sPriceBookANZ.Description = 'ANZ Price Book - Used to restrict ANZ visibility to only the Non-One CRM products';
                sPriceBookANZ.IsActive = true;
                sPriceBookANZ.Name = 'ANZ Price Book';
                insert sPriceBookANZ;
                            
                Test.startTest();
                
                List<OpportunityLineItem > Optitemlt = new list<OpportunityLineItem >();
                Map<id,OpportunityLineItem > OptitemMp = new Map<id,OpportunityLineItem >();
                
                Map<id,OpportunityLineItem > oldOptitemMp = new Map<id,OpportunityLineItem >();
                
                OpportunityLineItem sOLI      = new OpportunityLineItem();
                sOLI.OpportunityId          = sOpportunity.Id;
                sOLI.PricebookEntryId        = sPriceBookEntry.Id;
                sOLI.TotalPrice            = 200;
                sOLI.Quantity            = 2;
                soLI.Override_Primary_Product_Logic__c = true;
                Optitemlt.add(soLI);
                
                OpportunityLineItem sOLI1      = new OpportunityLineItem();
                sOLI1.OpportunityId          = sOpportunity.Id;
                sOLI1.PricebookEntryId        = sPriceBookEntry.Id;
                sOLI1.TotalPrice            = 300;
                sOLI1.Quantity            = 3;
                soLI1.Override_Primary_Product_Logic__c = true;
                Optitemlt.add(soLI1); 
                
                insert Optitemlt;           
                
                OpportunityLineItem sOLI2      = new OpportunityLineItem();
                sOLI2.OpportunityId          = sOpportunity.Id;
                sOLI2.PricebookEntryId        = sPriceBookEntry.Id;
                sOLI2.TotalPrice            = 400;
                sOLI2.Quantity            = 1;
                soLI2.Override_Primary_Product_Logic__c = true;
                
                insert sOLI2;
                
                OptitemMp.put(sOLI2.id,sOLI2);
                oldOptitemMp.put(sOLI2.id,sOLI2);
                
                PS_OpportunityValidationFactory.CreateOpptyValidations(Optlt,OptMp,oldOptMp);
                PS_OpportunityValidationFactory.OpptyLineItemValidations(OptitemMp,oldOptitemMp,'Insert');            
                
                Test.stopTest();
            }
        }
       
        
        static testMethod void TaskTest() { 
           
            List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
            insert usrLst;
            Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true);
            insert byp;
            
            system.runas(usrLst[0]){
                Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School',Market__c='US',Phone = '9989887687');
                acc.ShippingStreet = 'TestStreet';
                acc.ShippingCity = 'Vns';
                acc.ShippingState = 'Delhi';
                acc.ShippingPostalCode = '234543';
                acc.ShippingCountry = 'India';
                acc.IsCreatedFromLead__c = TRUE;
                insert acc;
                
                Contact con = new Contact(FirstName='AssetHandler',LastName ='Test',Phone='9999888898',Email='AssetHandler.Test@testclass.com', AccountId = acc.Id,
                                          Preferred_Address__c = 'Mailing Address', MailingCountry = 'United Kingdom', MailingStreet = '1 Street', MailingPostalCode = 'NE27 0QQ', MailingCity  = 'City');
                insert con;
                
                Opportunity ro = new Opportunity(Name= 'OpTest', AccountId = acc.id, StageName = 'Solutioning', Type = 'New Business', Academic_Vetting_Status__c = 'Un-Vetted', Academic_Start_Date__c = System.Today(),CloseDate = System.Today(),International_Student__c = true);
                insert ro;
                
                list<Task> newTakls = new list<Task>();
                Map<id,Task> OptitemtaskMp = new Map<id,Task>();           
                Map<id,Task> oldOptitemtaskMp = new Map<id,Task>();
                
                Test.startTest();   
                
                Task ts = new Task(Subject ='Email',Priority ='Normal', Status ='Not Started', WhatId = ro.Id,Ownerid = userinfo.getUserId());
                insert ts;
                newTakls.add(ts);
                
                OptitemtaskMp.put(ts.id,ts);
                oldOptitemtaskMp.put(ts.id,ts);
                
                PS_OpportunityValidationFactory.OpptyTaskValidations(newTakls,OptitemtaskMp,oldOptitemtaskMp,'Insert');

                Test.stopTest();    
            
            }    
             
        }
    }