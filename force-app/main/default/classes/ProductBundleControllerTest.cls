@isTest(SeeAllData = false)                   
public class ProductBundleControllerTest{
     
     static testMethod void myTest1() { 
 
            Account acc = new Account();
            acc.Name = 'CTI Bedfordview Campus';
            acc.Line_of_Business__c= 'Higher Ed';
            acc.Geography__c = 'North America';
            acc.Market2__c = 'US';
            acc.Pearson_Campus__c=true;
            acc.Pearson_Account_Number__c='Campus';
            insert acc;
         
        List<Product2> listProduct = new List<Product2>();
            
        Product2 prod = new Product2(Name = 'Bachelor of Commerce', 
                                        PIHE_Repeater__c='true',
                                        Semester__c ='First Semester',
                                        PIHE_Conferer__c='PIHE',
                                        PIHE_Qualification_Year__c='1',
                                        PIHE_Qualification__c='Pre-degree Foundation Programme (Social Sciences)',
                                        Module_Type__c='Core',
                                        Module_Code__c='FPIP021',
                                        Configuration_Type__c='Option',
                                        Auto_Selected__c=true,                                                                                 
                                        Family = 'Best Practices', IsActive = true);
           
        //listProduct.add(prod);
        
        
        Prod.Semester__c ='Full Year';
     //   Prod.Semester__c ='Second Semester';
        insert prod;
    
       /**  Product2 prod = new Product2(Name = 'Bachelor of Commerce',PIHE_Repeater__c='true',
                                            Semester__c ='First Semester',
                                            PIHE_Conferer__c='CTI',
                                            PIHE_Qualification_Year__c='1', 
                                            Family = 'Best Practices', IsActive = true);
            //insert prod;
      listProduct.add(prod);
            
            Product2 prod1 = new Product2(Name = 'Bachelor of Commerce',PIHE_Repeater__c='true',
                                            Semester__c ='Full Year',
                                            PIHE_Conferer__c='CTI',
                                            PIHE_Qualification_Year__c='1', 
                                            Family = 'Best Practices', IsActive = true);
            insert prod1;
      //listProduct.add(prod1); **/
            
           /** Product2 prod2 = new Product2(Name = 'Bachelor of Commerce',PIHE_Repeater__c='true',
                                            Semester__c ='Second Semester',
                                            PIHE_Conferer__c='CTI',
                                            PIHE_Qualification_Year__c='1', 
                                            Family = 'Best Practices', IsActive = true);
          insert prod2; 
      //listProduct.add(prod2);
      //insert listProduct; **/
            
         Id standardPBId = Test.getStandardPricebookId();
            PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, 
                                                            Product2Id = prod.Id,
                                                            UnitPrice = 10000, 
                                                            IsActive = true);
            insert standardPBE;
         
          Opportunity opp = new opportunity();
          opp.AccountId = acc.id;    
          opp.Name = 'OppTest';
          opp.StageName = 'Need Analysis';
          //CP 07/24/2018 opp.conferrer__c='CTI';
          //CP 07/24/2018 opp.Qualification_Picklist__c='Bachelor of Commerce';
          //Mani 07/26/2018 opp.Level__c=1;
          opp.PriceBook2Id = standardPBId; 
          opp.CloseDate = system.today();
          
          insert opp;
          
         Quote_Settings__c qs = new Quote_Settings__c ();
          qs.Name='System Properties';
          qs.Academic_Start_Date_Term_1__c=system.today();
          qs.Academic_Start_Date_Term_2__c=system.today()+14;
          qs.Academic_Start_Date_Term_3__c=system.today()+1;
          qs.Academic_Start_Date_Term_4__c=system.today()+2;
          qs.Default_Registration_Fee_New_Business__c=800;
          qs.Default_Registration_Fee_ReturnBusiness__c=0;
          qs.Earlybird_Securing_Fee_New_Business__c=1700;
          qs.Earlybird_Securing_Fee_Return_Business__c=800;
          qs.Early_Bird__c=system.today()+4;
          //qs.Early_Bird_Price_List_Id__c='a2s6E0000007VKo';
          //qs.Non_Early_Bird_Price_List_Id__c='a2s6E0000007VKt';
          qs.Administration_Fee_Rate__c=17;
          insert qs;          
                
          Quote quote = new quote();
          PageReference pageRef = Page.CreateStandardQuote;
          pageRef.getParameters().put('oppid', opp.id);
          Id recordtypeid =  Schema.SObjectType.Quote.getRecordTypeInfosByName().get('HE Quote').getRecordTypeId();
          pageRef.getParameters().put('RecordType',recordtypeid);
          Test.setCurrentPage(pageRef);
          ApexPages.StandardController stdQuote = new ApexPages.StandardController(quote);
          CreateStandardQuote controller = new CreateStandardQuote(stdQuote);
          //controller.Quote_create();
          
          Test.StartTest();
          //Id recordtypeid =  Schema.SObjectType.Quote.getRecordTypeInfosByName().get('HE Quote').getRecordTypeId();
          //Quote quo = [SELECT ID,Name,OpportunityId From Quote LIMIT 1];
             //quo.Qualification_Name_opp__c='Bachelor of Arts in Public Relations';
             // quo.Qualification_Level_Name_opp__c=2;
            Quote quo = new quote();
            quo.Name='Test Quote Creation';
            //quo.Pricebook = 
            quo.OpportunityId = opp.id;
            quo.Conferrer__c='PIHE';
            quo.Repeater__c=true;
            quo.First_Payment_Date__c=System.today();
            quo.Payment_Period_In_Month__c='3';
            quo.Deposit__c=1000.00;
            quo.Payment_Type__c='Monthly Payment';
            quo.Qualification_Level__c='1';
            quo.Qualification__c='Bachelor of commerce';
            quo.Qualification_Campus__c=acc.Id;      
            insert quo;
            
            
            
         List<QuoteLineItem> qltList = new List<QuoteLineItem>();   
         QuoteLineItem qliliner = new QuoteLineItem(Product2Id=prod.id,Outside_Module__c=true,
                                                    QuoteId=quo.id,PriceBookEntryID=standardPBE.Id,Quantity=4, 
                                                    UnitPrice =50);
          qltList.add(qliliner);                                                        
          //insert qltList;
          //Product2 pdt2 = new Product2();
          ApexPages.currentPage().getParameters().put('quoId',quo.Id);
          ApexPages.currentPage().getParameters().put('prd2Id',prod.Id);
          PageReference pageRef1 = Page.ProductBundlePage;
          pageRef1.getParameters().put('quoId', quo.id);
          ApexPages.StandardController stdQuote1 = new ApexPages.StandardController(prod);
          ProductBundleController prdBundle = new ProductBundleController(stdQuote1);
          prdBundle.searchProducts();
          prdBundle.addProductstoCart();          
          
          try{  
              prdBundle.getQualYearOptions();
          }catch(Exception e){
          }
                    
          prdBundle.cancelUrl();
          Test.StopTest();
          
     }
    static testMethod void myTest2() { 
 
          Account acc = new Account();
          acc.Name = 'CTI Bedfordview Campus';
          acc.Line_of_Business__c= 'Higher Ed';
          acc.Geography__c = 'North America';
          acc.Market2__c = 'US';
          acc.Pearson_Campus__c=true;
          acc.Pearson_Account_Number__c='Campus';
          insert acc;
     
          Opportunity opp = new opportunity();
          opp.AccountId = acc.id;    
          opp.Name = 'OppTest';
          opp.StageName = 'Need Analysis';
          opp.CloseDate = system.today();
          insert opp;
          
          
          Quote quote = new quote();
          PageReference pageRef = Page.CreateStandardQuote;
          pageRef.getParameters().put('oppid', opp.id);
          Id recordtypeid =  Schema.SObjectType.Quote.getRecordTypeInfosByName().get('HE Quote').getRecordTypeId();
          pageRef.getParameters().put('RecordType',recordtypeid);
          Test.setCurrentPage(pageRef);
          ApexPages.StandardController stdQuote = new ApexPages.StandardController(quote);
          CreateStandardQuote controller = new CreateStandardQuote(stdQuote);
          //controller.Quote_create();
          
          Test.StartTest();
          //Id recordtypeid =  Schema.SObjectType.Quote.getRecordTypeInfosByName().get('HE Quote').getRecordTypeId();
          Quote quo = new quote();
            quo.Name='Test Quote Creation';
            //quo.Pricebook = 
            quo.OpportunityId = opp.id;
            quo.Conferrer__c='PIHE';
            quo.Repeater__c=true;
            quo.First_Payment_Date__c=System.today();
            quo.Payment_Period_In_Month__c='3';
            quo.Deposit__c=1000.00;
            quo.Payment_Type__c='Monthly Payment';
            quo.Qualification_Level__c='1';
            quo.Qualification__c='Bachelor of commerce';
            quo.Qualification_Campus__c=acc.Id;      
            insert quo;
             
          Product2 pdt2 = new Product2();
          Product2 prod = new Product2(Name = 'Bachelor of Commerce',PIHE_Repeater__c='true',Semester__c ='First Semester',
                                       PIHE_Conferer__c='PIHE',
                                       PIHE_Qualification_Year__c='1', Family = 'Best Practices', IsActive = true);
            insert prod;
            
          Id standardPBId = Test.getStandardPricebookId();
          PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
            insert standardPBE;
            
          ApexPages.currentPage().getParameters().put('quoId',quo.Id);
          ApexPages.currentPage().getParameters().put('prd2Id',prod.Id);
          PageReference pageRef1 = Page.ProductBundlePage;
          pageRef1.getParameters().put('quoId', quo.id);
          ApexPages.StandardController stdQuote1 = new ApexPages.StandardController(prod);
          ProductBundleController prdBundle = new ProductBundleController(stdQuote1);
          prdBundle.searchProducts();
          prdBundle.addProductstoCart();
        try{  
        prdBundle.getQualYearOptions();
        }catch(Exception e){
            
        }
          //prdBundle.WrapProduct();
          Test.StopTest();
     }
    static testMethod void myTest3() { 
 
            Account acc = new Account();
            acc.Name = 'CTI Bedfordview Campus';
            acc.Line_of_Business__c= 'Higher Ed';
            acc.Geography__c = 'North America';
            acc.Market2__c = 'US';
            acc.Pearson_Campus__c=true;
            acc.Pearson_Account_Number__c='Campus';
            insert acc;
         
        List<Product2> listProduct = new List<Product2>();
            
        Product2 prod = new Product2(Name = 'Bachelor of Commerce', 
                                        PIHE_Repeater__c='true',
                                        Semester__c ='Second Semester',
                                        PIHE_Conferer__c='PIHE',
                                        PIHE_Qualification_Year__c='1',
                                        PIHE_Qualification__c='Pre-degree Foundation Programme (Social Sciences)',
                                        Module_Type__c='Core',
                                        Module_Code__c='FPIP021',
                                        Configuration_Type__c='Option',
                                        Auto_Selected__c=true,                                                                                 
                                        Family = 'Best Practices', IsActive = true);
           
        //listProduct.add(prod);
        
        
       
        insert prod;
    
                  
         Id standardPBId = Test.getStandardPricebookId();
            PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPBId, 
                                                            Product2Id = prod.Id,
                                                            UnitPrice = 10000, 
                                                            IsActive = true);
            insert standardPBE;
         
          Opportunity opp = new opportunity();
          opp.AccountId = acc.id;    
          opp.Name = 'OppTest';
          opp.StageName = 'Need Analysis';
          //CP 07/24/2018 opp.conferrer__c='CTI';
          //CP 07/24/2018 opp.Qualification_Picklist__c='Bachelor of Commerce';
          //Mani 07/26/2018 opp.Level__c=1;
          opp.PriceBook2Id = standardPBId;
          opp.CloseDate = system.today();
          
          insert opp;
          
         Quote_Settings__c qs = new Quote_Settings__c ();
          qs.Name='System Properties';
          qs.Academic_Start_Date_Term_1__c=system.today();
          qs.Academic_Start_Date_Term_2__c=system.today()+14;
          qs.Academic_Start_Date_Term_3__c=system.today()+1;
          qs.Academic_Start_Date_Term_4__c=system.today()+2;
          qs.Default_Registration_Fee_New_Business__c=800;
          qs.Default_Registration_Fee_ReturnBusiness__c=0;
          qs.Earlybird_Securing_Fee_New_Business__c=1700;
          qs.Earlybird_Securing_Fee_Return_Business__c=800;
          qs.Early_Bird__c=system.today()+4;
          //qs.Early_Bird_Price_List_Id__c='a2s6E0000007VKo';
          //qs.Non_Early_Bird_Price_List_Id__c='a2s6E0000007VKt';
          qs.Administration_Fee_Rate__c=17;
          insert qs;
          
          Quote quote = new quote();
          PageReference pageRef = Page.CreateStandardQuote;
          pageRef.getParameters().put('oppid', opp.id);
          Id recordtypeid =  Schema.SObjectType.Quote.getRecordTypeInfosByName().get('HE Quote').getRecordTypeId();
          pageRef.getParameters().put('RecordType',recordtypeid);
          Test.setCurrentPage(pageRef);
          ApexPages.StandardController stdQuote = new ApexPages.StandardController(quote);
          CreateStandardQuote controller = new CreateStandardQuote(stdQuote);
          //controller.Quote_create();
          
          Test.StartTest();
          //Id recordtypeid =  Schema.SObjectType.Quote.getRecordTypeInfosByName().get('HE Quote').getRecordTypeId();
         Quote quo = new quote();
            quo.Name='Test Quote Creation';
            //quo.Pricebook = 
            quo.OpportunityId = opp.id;
            quo.Conferrer__c='PIHE';
            quo.Repeater__c=true;
            quo.First_Payment_Date__c=System.today();
            quo.Payment_Period_In_Month__c='3';
            quo.Deposit__c=1000.00;
            quo.Payment_Type__c='Monthly Payment';
            quo.Qualification_Level__c='1';
            quo.Qualification__c='Bachelor of commerce';
            quo.Qualification_Campus__c=acc.Id;      
            insert quo;
            
            
         List<QuoteLineItem> qltList = new List<QuoteLineItem>();   
         QuoteLineItem qliliner = new QuoteLineItem(Product2Id=prod.id,Outside_Module__c=true,
                                                    QuoteId=quo.id,PriceBookEntryID=standardPBE.id,Quantity=4, 
                                                    UnitPrice =50);
          qltList.add(qliliner);                                                        
          //insert qltList;
          //Product2 pdt2 = new Product2();
          ApexPages.currentPage().getParameters().put('quoId',quo.Id);
          ApexPages.currentPage().getParameters().put('prd2Id',prod.Id);
          PageReference pageRef1 = Page.ProductBundlePage;
          pageRef1.getParameters().put('quoId', quo.id);
          ApexPages.StandardController stdQuote1 = new ApexPages.StandardController(prod);
          ProductBundleController prdBundle = new ProductBundleController(stdQuote1);
          prdBundle.searchProducts();
          prdBundle.addProductstoCart();          
          
          try{  
              prdBundle.getQualYearOptions();
          }catch(Exception e){
          }
                    
          prdBundle.cancelUrl();
          Test.StopTest();
     }

}