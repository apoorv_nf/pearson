@isTest(seeAllData=False)
public class ProductsToAssetsTest {
     public static testMethod void newclasstestProdtoAsset(){
        Bypass_Settings__c bypass = new Bypass_Settings__c();
        bypass.SetupOwnerId=userInfo.getUserId();
        bypass.Disable_Validation_Rules__c=true;
        bypass.Disable_Triggers__c=true;
        bypass.Disable_process_builder__c=true;
        bypass.Name='Skip Triggers';
        //insert bypass;
        Account a1,a2;
        Opportunity o1,o2;
        PageReference classnew0;  
        PageReference classnew1;
        PageReference classnew2;
        integer Mode;
        //Boolean toggleFlag;
        UniversityCourse__c uc1;
        OpportunityUniversityCourse__c ouc1;
                
        List<User> listWithUser = new List<User>();       
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Support Profile'].Id,1);
        insert listWithUser;   
        System.runAs(listWithUser[0])
        {
            Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Validation_Rules__c=true,Disable_Triggers__c=true,Disable_Process_Builder__c=true,Disable_Workflow_Rules__c=true);
            insert byp;     
            // Create and Insert Account
            a1 = new Account(Name='CTest',Abbreviated_Name__c='CTest',BillingStreet = 'Street1',
                             BillingCity = 'London', BillingPostalCode = 'ABC DEF', BillingCountry = 'Australia',
                             Vista_Account_Number__c = 'xyz',Territory_Code_s__c='CC2');
            insert a1;
            a2 = new Account(Name='CTest',Abbreviated_Name__c='CTest',BillingStreet = 'Street1',
                             BillingCity = 'London', BillingPostalCode = 'ABC DEF', BillingCountry = 'Australia',
                             Vista_Account_Number__c = 'xyz',Territory_Code_s__c='CC2');
            insert a2;
            
            // Create and Insert Opportunity
            o1 = new Opportunity(Name='CTestOpp', AccountId=a1.Id, StageName='Need Analysis', CloseDate=Date.today(),Pricebook2Id =Test.getStandardPricebookId(),
                                 RecordTypeId ='012b0000000Djc0AAC',Re_engagement_Date__c = system.today().addmonths(1), Renewal_Date__c = system.today().addmonths(8)); 
            insert o1;
            o2 = new Opportunity(Name='CTestOpp', AccountId=a2.Id, StageName='Need Analysis', CloseDate=Date.today(),Pricebook2Id =Test.getStandardPricebookId(),
                                 RecordTypeId ='012b0000000Djc0AAC',Re_engagement_Date__c = system.today().addmonths(1), Renewal_Date__c = system.today().addmonths(8)); 
            insert o2;
            
             
            List<Opportunity> sOpportunity = TestDataFactory.createOpportunity(1,'Opportunity');
            sOpportunity[0].AccountId          = a1.Id;
            sOpportunity[0].StageName          = 'Closed';   
            sOpportunity[0].CurrencyIsoCode= 'GBP';
            sOpportunity[0].Pricebook2Id =Test.getStandardPricebookId();
            sOpportunity[0].RecordTypeId='012b0000000Djc0AAC';
            insert sOpportunity;
            
            List<Product2> sProduct = TestDataFactory.createProduct(1);
            sProduct[0].Market__c = 'US';
            sProduct[0].Line_of_Business__c= 'Higher Ed';
            sProduct[0].Business_Unit__c = 'CTIPIHE';
            //GK
            sProduct[0].Author__c = 'Talent'; 
            sProduct[0].Copyright_Year__c = '2005';
            sProduct[0].Competitor_Product__c = true;
            insert sProduct;
            
           
            
            PriceBookEntry newPriceBookEntry = new PriceBookEntry();
            newPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();//listWithPriceBook[0].Id;
            newPriceBookEntry.UnitPrice = 10.0;
            newPriceBookEntry.Product2Id = sProduct[0].Id;
            newPriceBookEntry.CurrencyIsoCode= 'GBP';
            newPriceBookEntry.IsActive = true;
            insert newPriceBookEntry;
            
            OpportunityLineItem sOLI1      = new OpportunityLineItem();
            sOLI1.OpportunityId          = o1.Id;
            sOLI1.PricebookEntryId        = newPriceBookEntry.Id;    
            sOLI1.UnitPrice = 10.0;      
            sOLI1.Quantity  = 3;      
            insert sOLI1;
            
            Competitor_Product__c cp = new Competitor_Product__c();
            cp.Opportunity__c = o1.id;
            insert cp;
            
            // Run with Valid OID and Valid AID
            classnew0 = new PageReference('/apex/ProductsToAssets?oid='+ o1.Id +'&aid='+a1.Id+'&Mode='+0 );
            Test.setCurrentPage(classnew0);
            
            /* classnew2 = new PageReference('/apex/ProductsToAssets?oid='+o1.Id+'&aid='+a1.Id+'&Mode='+3 );
            Test.setCurrentPage(classnew2);*/
            
            ProductsToAssets classnew14=new ProductsToAssets();       
            ProductsToAssets classnew26=new ProductsToAssets(true);
              PageReference convertPage2 = classnew26.convert();
             ProductsToAssets classnew216=new ProductsToAssets(false);
             ProductsToAssets classnew206=new ProductsToAssets(null);
             ProductsToAssetsUtil classutil121=new ProductsToAssetsUtil();
            ProductsToAssetsUtil classutil=new ProductsToAssetsUtil(); 
            ProductsToAssetsUtil.getConvertedAssets(sOpportunity[0].Id);
            ProductsToAssetsUtil.getOlis(sOpportunity[0].Id);
            ProductsToAssetsUtil.getComplis(sOpportunity[0].Id);
            ProductsToAssetsUtil.getConvertedAssetsMap(sOpportunity[0].Id);
            ProductsToAssetsUtil.getTotalOfConvertedAssets(sOpportunity[0].Id);
            Test.startTest();
           
            
            
            classnew26.getOIDQueryParameter();  
            classnew26.setOLIS();
            classnew26.toggleFlag= true;
            classnew26.setToggleButtonHeader();
            classnew26.setAssetW();
            classnew26.init();
            classnew26.setCompLis();
            classnew26.setConvertedAssets();
            //classnew.selectDeselectAll();
            
            
            
            
            Opportunity o = [SELECT Id, Name FROM Opportunity LIMIT 1][0];
            
            //classnew14.assetW.add(assetToConvert);
            classnew14.toggleFlag= true;
            classnew14.toggleView();
            classnew14.setAssetLists();
            classnew14.selectAll();
            classnew14.deselectAll();
            classnew14.selectAllStatus();
            classnew14.setOLIS();
            classnew14.setOUCS();
            classnew14.cancel();
            
            classnew26.toggleFlag= false;
            classnew26.toggleView();
            classnew26.getOpportunityID();
            classnew26.getAccount();
            classnew26.getOpportunity();
            classnew26.getAccountID();
            classnew26.getSectionHeader();
            classnew26.getAssetStatus();
            classnew26.getToggleFlag();
            classnew26.getToggleButtonHeader();
            classnew26.getOlis();
            classnew26.getConvertedAssetsCount();
            classnew26.setThrowException(true);
            classnew26.getAssetW();
            //classnew26.convert();
            classnew26.getConvertedAssets();        
            classnew26.selectAll();
            classnew26.deselectAll();
            classnew26.selectAllStatus();    
           //  classnew.getcompval();
            Test.stopTest();       
            classnew14.setAssetWrapperList1();
        }             
     }
}