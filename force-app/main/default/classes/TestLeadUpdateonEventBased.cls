/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:         TestLeadUpdateonEventBased.cls 
   Description:  Test Class For Lead
                
   Date             Version     Author                  Tag     Summary of Changes 
   -----------      -------     -----------------       ---     ------------------------------------------------------------------------
   26/08/2015       0.1         Accenture               None    Intial draft
   08/03/2016       0.2        Abhinav                          Populating the National Identity Number field on Lead with the new syntax.
  -------------------------------------------------------------------------------------------------------------------------------------------------------- */
@isTest(seealldata=true)
private class TestLeadUpdateonEventBased
{
  /*
   *  Test the LeadUpdateonEventBased class 
   */  
  static testMethod void myUnitTest1()
  {
    List<User> listWithUser = new List<User>();
    listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator' ].Id,1);
    listwithuser[0].Market__c='US';
    insert listwithuser;
      Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listwithuser[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp;  
        System.runAs(listWithUser[0]){
    Test.startTest();
    
    TestClassAutomation.FillAllFields = true;
 //   Account acc = new Account(name='Test1',IsCreatedFromLead__c = True,Line_of_Business__c='Higher Ed',Geography__c='Growth',Phone='+9100000',ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037',Market2__c ='ZA',RecordTypeId='012b0000000Djbm');
 //  insert acc;
    List<Account> accountList = TestDataFactory.createAccount(1,'Professional');       
         accountList[0].Market2__c = 'US';
        accountList[0].Organisation_Type__c='School';
        insert accountList ;
        
        List<Lead> LeadList = TestDataFactory.createLead(1, 'B2B');
        LeadList[0].Market__c = listWithUser[0].market__c;
        LeadList[0].Institution_Organisation__c =accountList[0].Id;
        insert LeadList;
            
  //  Lead sLead = (Lead)TestClassAutomation.createSObject('Lead');
  //  sLead.Country = 'Australia';
  //  sLead.CountryCode = 'AU';
  //    sLead.StateCode = 'VIC';
  //  sLead.State = 'Victoria';
  //  sLead.Sponsor_Email__c = 'test@gm.com';
  //  sLead.Institution_Organisation__c = acc.id;
    //sLead.Institution_Organisation__c = accountList[0].id;
    //Abhinav : populating the National Identification Number  on Lead.
  //  sLead.Identification_Number__c = '9412055708083';

    
  //  insert sLead;
    
  // update sLead;
    
  //  delete sLead;
    
  //  undelete sLead;  
  //  insert LeadList;
    update LeadList;
    delete LeadList;
    undelete LeadList;
   
    TestClassAutomation.FillAllFields = true;
    Id recordTypeIdVal = [Select id from RecordType where sObjectType = 'Event' AND RecordType.Name='Interview'].id;
    Event sEvent = (Event)TestClassAutomation.createSObject('Event');
    sEvent.WhoId = LeadList[0].ID;            
    sEvent.ActivityDateTime = system.now();
    sEvent.StartDateTime = sEvent.ActivityDateTime;
    sEvent.EndDateTime = system.now().addHours(1);
    sEvent.DurationInMinutes = null;
    sEvent.IsRecurrence = false;
    sEvent.RecurrenceStartDateTime = null;
    sEvent.RecurrenceEndDateOnly = null;
    sEvent.RecurrenceInterval = null;
    sEvent.RecurrenceDayOfWeekMask = null;
    sEvent.RecurrenceDayOfMonth = null;
    sEvent.RecurrenceMonthOfYear = null;
    sEvent.RecurrenceInstance = null;
    sEvent.RecurrenceEndDateOnly = null;
    sEvent.RecurrenceType = null;
    sEvent.RecurrenceTimeZoneSidKey = null;
    sEvent.Reason_for_Cancellation__c = 'Student Cancels';
    sEvent.RecordTypeid = recordTypeIdVal;
    sEvent.Status__c = 'Cancelled' ;
       sEvent.Call_Outcome__c = '';    
           sEvent.isdc_dialer_call_type__c = '';
    insert sEvent;
             
    update sEvent;
        
    delete sEvent;
            
    undelete sEvent;
     
    Test.stopTest();
  } 
  }
  /*
   * Execute bullk 200 lead creation   
   */
  static testMethod void myUnitTest2()
  { 
  List<User> listWithUser = new List<User>();
    listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        System.runAs(listWithUser[0]){   
    List<Event> events = TestDataFactory.createEvent(200);
    
    Test.startTest();
    
    insert events;
        
    Test.stopTest();    
  }   
}
}