/*******************************************************************************************************************
* Apex Class Name   : PS_NewAccountRequest_Test
* Created Date      : 26th Aug 2015
* Description       : Test class for PS_NewAccountRequestController Class
* ABhinav -         : Modified class for test fix.
*******************************************************************************************************************/

@isTest(SeeAllData = true)

public class PS_NewAccountRequest_Test
{
    public Static testMethod void testsave()
    {   List<User> listWithUser = new List<User>();
         AssignmentRule AR = new AssignmentRule();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        listWithUser[0].market__c='ZA';
        insert listWithUser;
     Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Triggers__c=true,
                                                        Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
       insert byp;
        System.runAs(listWithUser[0]){
        RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
        Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
        acc.ShippingStreet = 'TestStreet';
        acc.ShippingCity = 'Vns';
        acc.ShippingState = 'Delhi';
        acc.ShippingPostalCode = '234543';
        acc.ShippingCountry = 'India';
        acc.recordtypeId=OrgAcc.Id ;
        acc.Academic_Achievement__c='High';
        acc.Market2__c = 'ZA';
        acc.Organisation_Type__c='School';
        acc.IsCreatedFromLead__c = true;
        
        insert acc;
         List<Lead> lstLeadRecords = new List<Lead>();
        lstLeadRecords = TestDataFactory.createLead(1,'New_Account_Request');
        lstLeadRecords[0].Market__c = listWithUser[0].market__c;
        lstLeadRecords[0].Institution_Organisation__c =acc.Id;
        lstLeadRecords[0].email ='test@tt.com';
        lstLeadRecords[0].Home_Phone__c='';
        lstLeadRecords[0].Other_Phone__c='';
        lstLeadRecords[0].MobilePhone='';
        
        /*account acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', Line_of_Business__c= 'Schools', Geography__c= 'Core', ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
        acc.Pearson_Campus__c=true;
        insert acc;
        Lead testLead = new Lead(Company = 'TestC',Phone = '123456780',Home_Phone__c = '0987654321',Other_Phone__c ='231456721',Primary_Email__c = 'test@tt.com',Secondary_Email__c = 'testSec@tt.com',LastName = 'testName',Status ='open', Country_of_Origin__c = 'Thailand',Institution_Organisation__c = acc.id);
        insert testLead;*/
        system.debug('market value is :' +lstLeadRecords[0].Market__c);
        Test.StartTest();
        Database.insert(lstLeadRecords);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(lstLeadRecords[0]);
        PS_NewAccountRequestController cont = new PS_NewAccountRequestController(stdLead);
        PageReference testPage = page.PS_NewAccountRequest;
        Test.setCurrentPage(testPage);
        cont.Leadvalues = lstLeadRecords[0];
        cont.Save();
        Test.StopTest();
        }   
    }
    public Static testMethod void testsave_Positive()
    {
        List<User> listWithUser = new List<User>();
         AssignmentRule AR = new AssignmentRule();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        listWithUser[0].market__c='UK';
        insert listWithUser;
        RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional'];
        Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
        acc.ShippingStreet = 'TestStreet';
        acc.ShippingCity = 'Vns';
        acc.ShippingState = 'Delhi';
        acc.ShippingPostalCode = '234543';
        acc.ShippingCountry = 'India';
        acc.recordtypeId=OrgAcc.Id ;
        acc.Academic_Achievement__c='High';
        acc.Market2__c = 'UK';
        acc.Organisation_Type__c='School';
        acc.IsCreatedFromLead__c = true;
        
        insert acc;
        System.runAs(listWithUser[0]){
        List<Lead> lstLeadRecords = new List<Lead>();
        lstLeadRecords = TestDataFactory.createLead(1,'New_Account_Request');
        //Lead testLead = new Lead(Company = 'TestC',Phone = '123456780', Home_Phone__c = '0987654321',Other_Phone__c ='231456721',Primary_Email__c = 'test@tt.com',Secondary_Email__c = 'testSec@tt.com',LastName = 'testName',Status ='open');
        //insert testLead;
        //cont.Leadvalues.Phone =
        lstLeadRecords[0].Academic_Achievement__c='Medium';
        lstLeadRecords[0].Market__c = listWithUser[0].market__c;
        lstLeadRecords[0].Institution_Organisation__c =acc.Id;
        Test.StartTest();
        //Database.insert(lstLeadRecords);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(lstLeadRecords[0]);
        PS_NewAccountRequestController cont = new PS_NewAccountRequestController(stdLead);
        PageReference testPage = page.PS_NewAccountRequest;
        Test.setCurrentPage(testPage);
        cont.Leadvalues = lstLeadRecords[0];
        cont.Save();
        Test.StopTest();
        }
    }
}