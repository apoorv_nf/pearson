/*****************************************************************************
IN:    12/8/2016:    R6-Updated recursion trigger based on Object Id - CRMSFDC-3334 
*****************************************************************************/
public class checkRecurssion {
    public static boolean run = true;
    public static boolean runOnce(){
     if(run){
     run=false;
     return true;
    }else{
        return run;
    }
    }    
    //IN - Added for CRMSFDC-3334 
    private static set<Id> executedIds = new Set<Id>();
    public static List<sObject> filterNotExecutedRecords(List<sObject> objectsToProcess){
        List<sObject> filteredObjects = new List<sObject>();
        for (sObject sObj : objectsToProcess){
            Id objectId = (Id)sObj.get('Id');
            if (objectId != null && !executedIds.contains(objectId)){
                filteredObjects.add(sObj);
                executedIds.add(objectId);
            }
        }
        return filteredObjects;
    }    
}