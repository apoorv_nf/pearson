@isTest
class TestcreateTask {

  static testMethod void createtask() {
     User usr;
     usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Price_List__c= 'Math & Science');

        usr.market__c='AU';
        usr.Business_Unit__c='Schools';
        usr.Line_of_Business__c='Schools';
        usr.Group__c='Primary';
        insert usr;

  Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Validation_Rules__c=true);
    insert byp;       
    System.assert(usr.id != null, 'User creation failed');   
  
List<Task>task1 = new List<Task>();
 TestClassAutomation.FillAllFields = true;
        
        Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
        sAccount.recordtypeid           = Schema.SObjectType.Account.getRecordTypeInfosByName().get('School').getRecordTypeId();
        sAccount.BillingCountry         = 'Australia';
        sAccount.BillingState           = 'Victoria';
        sAccount.BillingCountryCode     = 'AU';
        sAccount.BillingStateCode       = 'VIC';
        sAccount.ShippingCountry        = 'Australia';
        sAccount.ShippingState          = 'Victoria';
        sAccount.ShippingCountryCode    = 'AU';
        sAccount.ShippingStateCode      = 'VIC';
        sAccount.Lead_Sponsor_Type__c   = null;
        sAccount.PS_AccountSegment1__c = '1';
        system.debug('KP Account Segment:'+sAccount.PS_AccountSegment1__c+'RecordType:'+sAccount.recordtypeid);
        system.runas(usr){
        insert sAccount;
        Test.startTest();
createtask s = new createtask();
  s.createTaskOnLead(sAccount.id);
  Test.stopTest();
   }
  
   }
}