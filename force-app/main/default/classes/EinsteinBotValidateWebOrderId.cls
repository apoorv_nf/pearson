/*
 * Author: Apoorv Saxena (Neuraflash)
 * Date: 28/08/2019
 * Description: Checks if the String being passed is a valid ISBN.
 * Test Class: EinsteinBotValidateWebOrderIdTest
 */
public with sharing class EinsteinBotValidateWebOrderId{

    @InvocableMethod(label='Einstein SMS Bot - Validate Web Order Id Format')
    public static List<Boolean> verifyWebOrderIdFormat(List<String> weborderId) {
    
        try{
            String orderId = weborderId[0];
            if(!String.isBlank(orderId)){
                orderId = orderId.trim();
                String orderIdRegex = '^[Ww][Ee][Bb][Ee]*\\d{14}';
                Pattern myPattern = Pattern.compile(orderIdRegex);
                Matcher myMatcher = myPattern.matcher(orderId);

                if (myMatcher.matches()){ 
                    return new List<Boolean>{true};
                }
            }
        } catch(Exception ex){}
        return new List<Boolean>{false};
	}
}