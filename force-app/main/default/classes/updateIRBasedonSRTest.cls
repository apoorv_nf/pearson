/*******************************************************************************************************************
* Apex Class Name  : updateIRBasedonSRTest
* Version          : 1.0 
* Created Date     : 6/8/2017
* Function         : 
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*                                      
*******************************************************************************************************************/
@isTest  
public with sharing class updateIRBasedonSRTest {    
        
     /*************************************************************************************************************
  * Name               : testSAIntegrationReq
  * Description        : 
  * Date Created       :  06/22/2017
  * Author             : Jaydip B 
  *************************************************************************************************************/
  
  static testMethod void testSAIntegrationReq() {
  
      
      //created custom setting record for CS_Response_IR__c
      CS_Response_IR__c csRespIR=new CS_Response_IR__c(Name='Enrol Student', Systems__c='eVision;NaVision');
      insert csRespIR;

    //Create IR record
      Integration_Request__c IRRec=new Integration_Request__c(Direction__c='Outbound',Event__c='Enrol Student',Geo__c='Growth',LoB__c='HE',
                                                              Market__c='ZA',Object_Id__c='006b000000OKItnAAH',Object_Name__c='Opportunity',Status__c='Received');
      
        
     insert IRRec;


    //created SR record
    
    
    System_Response__c SRRec=new System_Response__c(External_System__c='NaVision',Integration_Request__c=IRRec.Id,Status__c='Completed');
    
     insert SRRec;
     
     Integration_Request__c IRExist=[Select Status__c from Integration_Request__c where Id=:IRRec.Id];
     
     System.assertEquals (IRExist.Status__c,'Ready For Submission');
    
     
     
     
     
     
     System_Response__c SRRecNew=new System_Response__c(External_System__c='eVision',Integration_Request__c=IRRec.Id,Status__c='Functional Error');
     insert SRRecNew;
     
     SRRecNew.Status__c='Completed';
     update SRRecNew;
     
     
     //Integration_Request__c IRExistNew=[Select Status__c from Integration_Request__c where Id=:IRRec.Id];
     
     //System.assertEquals (IRExistNew.Status__c,'Functional Error');
    
     //System_Response__c SRRecNewSuccess=new System_Response__c(External_System__c='eVision',Integration_Request__c=IRRec.Id,Status__c='Completed');
     //insert SRRecNewSuccess;
     
     //Integration_Request__c IRExistSuccess=[Select Status__c from Integration_Request__c where Id=:IRRec.Id];
     
     //System.assertEquals (IRExistSuccess.Status__c,'Completed');
       
    
   }

}