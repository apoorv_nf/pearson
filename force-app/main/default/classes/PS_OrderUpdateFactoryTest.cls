/************************************************************************************************************
* Apex Interface Name : PS_OrderUpdateFactoryTest
* Version             : 1.0 
* Created Date        : 9/2015 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Accenture          9/2015              R2.1 version
* Raushan            3/2016              R3.2 version          changed the start.test()
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

@isTest
public with sharing class PS_OrderUpdateFactoryTest 
{
  static testMethod void testMethod1()
  { 
   
    List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
    insert usrLst;
     System.runas(usrLst[0])
    {
    List<PermissionSetAssignment> lstWithPermissionSetAssignment = new List<PermissionSetAssignment>();
    List <PermissionSet> permList = [SELECT Id FROM PermissionSet WHERE name = 'Pearson_Backend_Order_Creation' or name = 'Pearson_Global_Backend_Revenue' or name = 'Pearson_Global_Backend_Sample' OR name = 'Pearson_Manage_Orders'];  
    system.debug('ira'+permList );
    if(permList.size()>0 )
    {
        for(PermissionSet insertPermSet : permList)
        { 
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.AssigneeId = usrLst[1].Id;
            psa.PermissionSetId = insertPermSet.Id;
            lstWithPermissionSetAssignment.add(psa);
        }
        insert lstWithPermissionSetAssignment;
    }
    }
     Test.startTest(); // moved top
      Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true);
     insert byp;
    System.runas(usrLst[0])
    {  
    Account acc = (Account)TestClassAutomation.createSObject('Account');
    acc.Name = 'Test';
    insert acc;
     
    contact con = (Contact)TestClassAutomation.createSObject('Contact');
    con.Lastname= 'testcon';
    con.Firstname= 'testcon1';
    con.MobilePhone = '9999';  
    con.Preferred_Address__c = 'Other Address';
    con.OtherCountry  = 'India';
    con.OtherStreet = 'Test';
    con.OtherCity  = 'Test';
    con.OtherPostalCode  = '123456';
    con.accountId = acc.Id;
    con.salutation = 'Mr.';
    con.First_Language__c = 'English';
    insert con;     
    
    Opportunity opp = (Opportunity)TestClassAutomation.createSObject('Opportunity');
    opp.AccountId = acc.Id;
    opp.CurrencyIsoCode = 'GBP';
    opp.Lost_Reason_Other__c = 'XXX';
    insert opp;
    
    Product2 prod = (Product2)TestClassAutomation.createSObject('Product2');
    insert prod;

    /*Pricebook2 priceBook = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
    priceBook.IsActive = true;
    update priceBook;*/
    Id priceBookId = Test.getStandardPricebookId();
        
    PriceBookEntry sPriceBookEntry = (PriceBookEntry)TestClassAutomation.createSObject('PriceBookEntry');
    sPriceBookEntry.IsActive = true;
    sPriceBookEntry.Product2Id = prod.Id;
    sPriceBookEntry.Pricebook2Id = priceBookId;
    sPriceBookEntry.UnitPrice = 34.95;
    sPriceBookEntry.CurrencyIsoCode = 'GBP';
    insert sPriceBookEntry;
     OpportunityLineItem oli1 = new OpportunityLineItem();
    oli1.OpportunityId = opp.Id;
    oli1.PricebookEntryId = sPriceBookEntry.Id;
    oli1.TotalPrice = 200;
    oli1.Quantity = 1;
            
    insert oli1;
   //  Test.startTest();                             //made changes for release 3.2-Raushan 
    order sampleorder = new order();
    sampleorder.OpportunityId=opp.Id;
    sampleorder.Accountid = acc.Id;
    sampleorder.EffectiveDate = system.today();
    sampleorder.status = 'Open';
    sampleorder.Pricebook2Id = priceBookId;
    sampleorder.CurrencyIsoCode = 'GBP';
    sampleorder.Packing_Instructions__c = 'Packing Instructions';
    sampleorder.Shipping_Instructions__c = 'Shipping instructions';
    sampleorder.Salesperson__c = usrLst[0].Id;
    sampleorder.ShippingCity = 'Patna';
    sampleorder.ShippingCountry = 'India';
    sampleorder.ShippingState = 'Bihar';
    sampleorder.ShippingStreet = 'Test';
    sampleorder.ShippingPostalCode = '1111';
    sampleorder.Line_of_Business__c = 'Higher Ed';
    sampleorder.Geography__c = 'Growth';
    insert sampleorder;
    
    Orderitem oi = new OrderItem();
    oi.orderid=sampleorder.id;
    oi.Shipped_Product__c = prod.id;
    oi.Quantity = 6;
    oi.pricebookentryid= sPriceBookEntry.id;
    oi.unitprice =0;
    oi.status__c = 'Cancelled';
    //GK
    oi.Shipping_Method__c = 'Ground'; 
    insert oi;  
    
    Orderitem oi1 = new OrderItem();
    oi1.orderid=sampleorder.id;
    oi1.Shipped_Product__c = prod.id;
    oi1.Quantity = 6;
    oi1.pricebookentryid= sPriceBookEntry.id;
    oi1.unitprice =0;
    oi1.status__c = 'Shipped';
    //GK    
    oi1.Shipping_Method__c = 'Ground';    
    insert oi1;  
     
    List <OrderItem> orderItems = new List<OrderItem>();
    orderItems.add(oi); 
    orderItems.add(oi1);
    
    
    
    List<PS_OrderUpdateInterface> updates = PS_OrderUpdateFactory.createOrderUpdates(orderItems);
    for(PS_OrderUpdateInterface updateInterface: updates)
    {
     updateInterface.updateOrders();
    }
    
    oi1.status__c = 'Cancelled';
    update oi1;
    updates = PS_OrderUpdateFactory.createOrderUpdates(orderItems);
    for(PS_OrderUpdateInterface updateInterface: updates)
    {
     updateInterface.updateOrders();
    }
    
    oi1.status__c = 'On Hold';
    update oi1;
    updates = PS_OrderUpdateFactory.createOrderUpdates(orderItems);
    for(PS_OrderUpdateInterface updateInterface: updates)
    {
     updateInterface.updateOrders();
    }
    
    //More order senarios    
    oi1.status__c = 'Closed';
    update oi1;
    updates = PS_OrderUpdateFactory.createOrderUpdates(orderItems);
    for(PS_OrderUpdateInterface updateInterface: updates)
    {
     updateInterface.updateOrders();
    }
    
    oi1.status__c = 'ClosedPartial';
    update oi1;
    updates = PS_OrderUpdateFactory.createOrderUpdates(orderItems);
    for(PS_OrderUpdateInterface updateInterface: updates)
    {
     updateInterface.updateOrders();
    }
    
    oi1.status__c = 'Booked';
    update oi1;
    updates = PS_OrderUpdateFactory.createOrderUpdates(orderItems);
    for(PS_OrderUpdateInterface updateInterface: updates)
    {
     updateInterface.updateOrders();
    }
    
    oi1.status__c = 'Filled';
    update oi1;
    updates = PS_OrderUpdateFactory.createOrderUpdates(orderItems);
    for(PS_OrderUpdateInterface updateInterface: updates)
    {
     updateInterface.updateOrders();
    }
    //End -More order senarios
    oi.status__c = 'Shipped';
    update oi; 
    oi1.status__c = 'Shipped';
    update oi1;   
    updates = PS_OrderUpdateFactory.createOrderUpdates(orderItems);
    for(PS_OrderUpdateInterface updateInterface: updates)
    {
     updateInterface.updateOrders();
    }
        
    oi.status__c = 'Booked';
    update oi; 
    oi1.status__c = 'Booked';
    update oi1;   
    updates = PS_OrderUpdateFactory.createOrderUpdates(orderItems);
    for(PS_OrderUpdateInterface updateInterface: updates)
    {
     updateInterface.updateOrders();
    }    
        
    List <OrderItem> emptyOrderItems = new List<OrderItem>();
    updates = PS_OrderUpdateFactory.createOrderUpdates(emptyOrderItems);
    for(PS_OrderUpdateInterface updateInterface: updates)
    {
      updateInterface.updateOrders();
    }
    
    Test.stopTest();
    }
  } 
}