/*****************************************************************************************************************************************************
 * Class Name   : UKTEPWrapperResponseClass
 * Author       : Navaneeth
 * Date Created : 03-Jan-2019
 * Description  : Class stores the data for Header Level Status, Error Message and Line Level [QuoteLineItem] details
 ******************************************************************************************************************************************************
    Date            Version         ModifiedBy                      Description
 03-Jan-2019         1.0           Navaneeth                       Created the WrapperClass
 ******************************************************************************************************************************************************/
public class UKTEPWrapperResponseClass{

   public String processResponseStatus;
   public String processResponseMessage;
   public List < QuoteLineItem > processResponseQuoteLineItemList;

}