@isTest//(seeAlldata=true)
private class  Community_NotificationsTest {
    
    private static testMethod void TestActiveNotificatons() {
        Test.startTest();     
        DateTime dtn = system.now(); 
        //Network commNetwork = [select id,name from network where name = 'US Clinical Assessments'];
        Community_Notification__c cn = New Community_Notification__c(Name='CN', Message__c = 'Test', Start_Date_Time__c = dtn, Stop_Date_Time__c = dtn + 4,Community_Header__c = 'usclinical');
        insert cn;
        List<Community_Notification__c> cns = [Select Id from Community_Notification__c where Active__c = true and Community_Header__c = 'usclinical'];  
        Integer ct = cns.size();
        system.assertEquals(ct,1); 
        Community_Notifications.activeNotifications();    
        Test.stopTest();  
    }
    
    private static testMethod void TestAllNotificatons() {
        Test.startTest();     
        DateTime dtn = system.now(); 
        Community_Notification__c cn = New Community_Notification__c(Name='CN', Message__c = 'Test', Start_Date_Time__c = dtn + 1, Stop_Date_Time__c = dtn + 4,Community_Header__c = 'usclinical');
        insert cn;
        List<Community_Notification__c> cns = [Select Id from Community_Notification__c where  Community_Header__c = 'usclinical'];  
        Integer ct = cns.size();
        system.assertEquals(ct,1); 
        Community_Notifications.findAll();    
        Test.stopTest();  
        
        
    }
    
    private static testMethod void TestSAActiveNotificatons() {
        Test.startTest();     
        DateTime dtn = system.now(); 
        Community_Notification__c cn = New Community_Notification__c(Name='CN', Message__c = 'Test', Start_Date_Time__c = dtn + 1, Stop_Date_Time__c = dtn + 4,Community_Header__c = 'usclinical');
        insert cn;
        List<Community_Notification__c> cns = [Select Id from Community_Notification__c where  Community_Header__c = 'usclinical'];  
        Integer ct = cns.size();
        system.assertEquals(ct,1); 
        Community_Notifications.activeNotification('School Assessment', 'Notifications');    
        Test.stopTest();  
    }
}