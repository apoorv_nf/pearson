@isTest
private class TestSmsApiService {

	@testSetup
	private static void setup() {
		SMS_API_Settings__c settings = new SMS_API_Settings__c();
		settings.API_Key__c = 'abcdef';
		settings.Username__c = 'username123';
		settings.Password__c = 'password123';
		settings.SSO_Token__c = 'token1234';

		insert settings;
	}

	@isTest
	private static void it_should_return_sms_api_when_access_code_is_valid() {
		Test.setMock(HttpCalloutMock.class, new SmsApiMock());

		Test.startTest();
		SmsApi.AccessCodeDetails accessCodeDetails = new SmsApiService().getAccessCode('valid');
		Test.stopTest();

		System.assertEquals('N', accessCodeDetails.cashedIn);
		System.assertEquals(Date.valueOf('2018-05-22 00:00:00'), accessCodeDetails.cashedInDate);
		System.assertEquals(Datetime.valueOf('2018-03-22 00:00:00'), accessCodeDetails.creationDate);
		System.assertEquals(Datetime.valueOf('2019-08-04 00:00:00'), accessCodeDetails.expirationDate);
		System.assertEquals(100, accessCodeDetails.numCashins);
		System.assertEquals(2, accessCodeDetails.numTimesCashedIn);
		System.assertEquals('N', accessCodeDetails.poReqd);
		System.assertEquals('Found', accessCodeDetails.status);
		System.assertEquals(Date.valueOf('2018-03-22 00:00:00'), accessCodeDetails.validFromDate);
	}

	@isTest
	private static void it_should_return_sms_api_when_access_code_is_invalid() {
		Test.setMock(HttpCalloutMock.class, new SmsApiMock());

		Test.startTest();
		SmsApi.AccessCodeDetails accessCodeDetails = new SmsApiService().getAccessCode('invalid');
		Test.stopTest();

		System.assertEquals(4, accessCodeDetails.errorCode);
		System.assertEquals('Not found', accessCodeDetails.status);
	}

	@isTest
	private static void it_should_return_sms_api_when_token_is_expired() {
		Test.setMock(HttpCalloutMock.class, new SmsApiMock());

		Test.startTest();
		SmsApi.AccessCodeDetails accessCodeDetails = new SmsApiService().getAccessCode('token_expired');
		Test.stopTest();

		SMS_API_Settings__c settings = SMS_API_Settings__c.getInstance();

		System.assertEquals('token1234', settings.SSO_Token__c);
	}

	@isTest
	private static void it_should_return_products() {
		Test.setMock(HttpCalloutMock.class, new SmsApiMock());

		Test.startTest();
		SmsApi.CurrentGenProduct products = new SmsApiService().getProducts('product_token');
		Test.stopTest();

		System.assert(!products.productIds.isEmpty());
		System.assertEquals('8161', products.productIds[0]);
		System.assertEquals('31237', products.productIds[1]);
		System.assertEquals('31529', products.productIds[2]);
	}

	@isTest
	private static void it_should_return_course() {
		Test.setMock(HttpCalloutMock.class, new SmsApiMock());

		Test.startTest();
		SmsApi.CurrentGenCourse course = new SmsApiService().getCourse('courseId');
		Test.stopTest();

		System.assertEquals('Tiernan CERT SMS QA Course 101', course.title);
		System.assert(!course.products.isEmpty());
		System.assertEquals(8161, course.products[0].id);
		System.assertEquals(32743, course.products[1].id);
	}
}