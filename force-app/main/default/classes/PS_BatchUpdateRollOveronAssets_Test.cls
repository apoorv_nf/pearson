/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_BatchUpdateRollOveronAssets_Test.cls 
   Description:     Test Class for PS_BatchUpdateRollOveronAssets. 
   CR Info:         CR-00275
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  24/01/2018         1.0            MAYANK LAL                       Initial Design 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
@IsTest
public class PS_BatchUpdateRollOveronAssets_Test {
    
    @testSetup static void CreateAssetData() {
        //Insert Account 
         Account acc = new Account();
        //acc.RecordTypeId = '012b0000000DpIM'; 
        acc.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
        acc.Name = 'Test Account';
        acc.Geography__c = 'Growth';
        acc.Line_of_Business__c = 'Higher Ed';
        acc.Market2__c = 'Global';
        
        insert acc;
        
        List<Contact> lstCon = TestDataFactory.createContacts(1);
        lstCon[0].accountid = acc.id;
        insert lstCon;
        
        //Insert Base Product
        Product2 PIUprod = new Product2();
           PIUprod.Configuration_Type__c='Option'; 
           PIUprod.Duration__c = 'D10';
           PIUprod.name = 'NA Territory Product1';
           PIUprod.Line_of_Business__c = 'Higher Ed';
           PIUprod.Market__c ='US';
           PIUprod.Business_Unit__c='US Field Sales';
           PIUprod.Binding__c = 'Access Code Card'; 
           PIUprod.Edition__c = '10';
           PIUprod.Platform__c = 'Plat1';
           PIUprod.Relevance_Value__c = 10;
           PIUprod.Brand__c = 'Mastering';
           PIUprod.Medium2__c= 'Digital';
           PIUprod.Category2__c='CourseSmart';
           PIUprod.CurrencyIsoCode = 'USD';   
    insert PIUprod;
        
        //Insert Product Family Next Edition
        Product2 prodFamilyNextEdition = new Product2();
           prodFamilyNextEdition.Configuration_Type__c='Bundle'; 
           prodFamilyNextEdition.Market__c ='US';
           prodFamilyNextEdition.Line_of_Business__c='Higher Ed';
           prodFamilyNextEdition.Business_Unit__c   ='US Field Sales';
           prodFamilyNextEdition.name = 'NA Test Product Family';
           prodFamilyNextEdition.Author__c = 'Test Author'; 
           prodFamilyNextEdition.Edition__c = '10';
           prodFamilyNextEdition.Status__c = 'PUB';
           prodFamilyNextEdition.Relevance_Value__c = 20;
           prodFamilyNextEdition.Brand__c = 'MyLab + Note Taking Guide';
        insert prodFamilyNextEdition;
        
        //Insert Product Family
        Product2 prodFamily = new Product2();
           prodFamily.Configuration_Type__c='Bundle'; 
           prodFamily.Market__c ='US';
           prodFamily.Line_of_Business__c='Higher Ed';
           prodFamily.Business_Unit__c   ='US Field Sales';
           prodFamily.name = 'NA Test Product Family';
           prodFamily.Author__c = 'Test Author'; 
           prodFamily.Edition__c = '10';
           prodFamily.Status__c = 'PUB';
           prodFamily.Relevance_Value__c = 11;
           prodFamily.Brand__c = 'MyLab + Note Taking Guide';
           prodFamily.Next_Edition__c  = prodFamilyNextEdition.id;  
        insert prodFamily;
        
        
        //Relating Product Family and Product
        RelatedProduct__c relProdandProdFamily = new RelatedProduct__c();
        relProdandProdFamily.CurrencyIsoCode = 'USD';
        relProdandProdFamily.Product__c = prodFamily.id;
        relProdandProdFamily.RelatedProduct__c = PIUprod.id;
        relProdandProdFamily.AssociationCategory__c = 'Alternate Binding';
        insert relProdandProdFamily;    
        
        //Insert Asset
       Asset asset = new Asset();
       asset.name = 'TestAsset';
       asset.Product2Id = PIUprod.id;
       asset.Primary__c = True;
       asset.Status__c = 'Active';
       asset.ContactId = lstCon[0].id;
       insert asset; 
        
       //Updating Relevance Value of Next Edition to 10
       prodFamilyNextEdition.Relevance_Value__c = 10;
       update  prodFamilyNextEdition;
    }
    
    static testmethod void batchRollOverCheck_WithProdFamily() {
        PS_BatchUpdateRollOveronAssets psrollBatch = new PS_BatchUpdateRollOveronAssets();
        Database.executeBatch(psrollBatch);
        
    }
    
    static testmethod void batchRollOverCheck_WithOutProdFamily() {
        RelatedProduct__c relProd  = [select id,CurrencyIsoCode,Product__c,RelatedProduct__c,AssociationCategory__c from RelatedProduct__c Limit 1];
        Product2 tempProd = [select id from product2 where Configuration_Type__c = 'Option'];
        relProd.Product__c = tempProd.id;
        relProd.AssociationCategory__c = ''; //Removing the parent family relationship 
        update relProd;
        PS_BatchUpdateRollOveronAssets psrollBatch = new PS_BatchUpdateRollOveronAssets();
        Database.executeBatch(psrollBatch);
        
    }

}