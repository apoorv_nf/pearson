/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            AutocreatedRegHandler1449048088690.cls 
   Description:     SAML JIT handler
   Test class:      - 
   Date             Version              Author                          Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  02-Dec-2015         0.1              Neha Karpe                 SAML JIT SSO Enablement
  28-Sept-2016        0.2              Payal popat                Added code for Contact and User Market as UK for R6
  13-March-2017       0.3              Manikanta Nagubilli        Added code for populating the LocaleSidKey,LanguageLocaleKey,TimeZoneSidKey,EmailEncodingKey from custom setting when the Country is "United States"
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
/*This class provides logic for inbound just-in-time provisioning of single sign-on 
users in the Salesforce organization*/
global class AutocreatedRegHandler1449048088690 implements Auth.SamlJitHandler {
    Private Contact c;
    /*Exception Handling - Auto created method by JIT Handler*/
    private class JitException extends Exception{}
    
    /*Best Practises - Breaking the handlerUser method into muliple smaller methods - handling the address fields*/
    @TestVisible private void handleUserKeyDetails(boolean create, User u, Map<String, String> attributes,
        String federationIdentifier, boolean isStandard)
    {
        /*
         * If you are updating Contact or Account object fields, you cannot update the following User fields at the same time.
         * If your identity provider sends these User fields as attributes along with Contact 
         * or Account fields, you must modify the logic in this class to update either these 
         * User fields or the Contact and Account fields.
         */
        if(attributes.containsKey('User.IsActive')) {
            String IsActiveVal = attributes.get('User.IsActive');
            u.IsActive = '1'.equals(IsActiveVal) || Boolean.valueOf(IsActiveVal);
        }
        if(attributes.containsKey('User.ForecastEnabled')) {
            String ForecastEnabledVal = attributes.get('User.ForecastEnabled');
            u.ForecastEnabled = '1'.equals(ForecastEnabledVal) || Boolean.valueOf(ForecastEnabledVal);
        }
       /* if(attributes.containsKey('User.Profile.Name')) {
            String profileName = attributes.get('User.Profile.Name');
            Profile p = [SELECT Id, name FROM Profile WHERE name=:profileName LIMIT 1];
            u.ProfileId = p.Id;
        }*/
         Profile p = [SELECT Id, name FROM Profile WHERE name=:Label.Pearson_Self_Service_Label LIMIT 1];
                u.ProfileId = p.Id;
        if(attributes.containsKey('User.UserRoleId')) {
        System.debug('entering 44');
            String userRole = attributes.get('User.UserRoleId');
            System.debug('entering 46' + userRole );
            UserRole r = [SELECT Id FROM UserRole WHERE Id=:userRole LIMIT 1];
            u.UserRoleId = r.Id;
        }
        //Handle custom fields here
    }
    /*Best Practises - Breaking the handlerUser method into muliple smaller methods - handling the address fields*/
    @TestVisible private void handleUserAddress(boolean create, User u, Map<String, String> attributes,
        String federationIdentifier, boolean isStandard)
    {
    if(attributes.containsKey('User.Street')) {
            u.Street = attributes.get('User.Street');
        }
        if(attributes.containsKey('User.State')) {
            u.State = attributes.get('User.State');
        }
        if(attributes.containsKey('User.City')) {
            u.City = attributes.get('User.City');
        }
        if(attributes.containsKey('User.Zip')) {
            u.PostalCode = attributes.get('User.Zip');
        }
        if(attributes.containsKey('User.Country')) {
            //u.Country = attributes.get('User.Country');
        }
    }
    /*Best Practises - Breaking the handlerUser method into muliple smaller methods-handling the User info*/
    @TestVisible private void handleUserPhone(boolean create, User u, Map<String, String> attributes,
        String federationIdentifier, boolean isStandard)
    {
    if(attributes.containsKey('User.MobilePhone')) {
            u.MobilePhone = attributes.get('User.MobilePhone');
        }
    if(attributes.containsKey('User.Phone')) {
            u.Phone = attributes.get('User.Phone');
        }
    }
    /*Best Practises - handleCurrent User Details as a part of HandleUser*/
    @TestVisible private void handleCurentUserDetails(boolean create, User u, Map<String, String> attributes,
        String federationIdentifier, boolean isStandard)
        {
        String uid = UserInfo.getUserId();
        User currentUser = 
                [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id=:uid LIMIT 50];
        if(attributes.containsKey('User.LocaleSidKey')) {
            u.LocaleSidKey = attributes.get('User.LocaleSidKey');
        } else{ 
            u.LocaleSidKey = currentUser.LocaleSidKey;
        }
        if(attributes.containsKey('User.LanguageLocaleKey')) {
            u.LanguageLocaleKey = attributes.get('User.LanguageLocaleKey');
        } else{
            u.LanguageLocaleKey = currentUser.LanguageLocaleKey;
        }
        if(attributes.containsKey('User.Alias')) {
            u.Alias = attributes.get('User.Alias');
        //} else(create) 
        } else{
            String alias = '';
            if(u.FirstName == null||u.FirstName == '') {
                alias = u.LastName;
            } else {
                alias = u.FirstName.charAt(0) + u.LastName;
            }
            if(alias.length() > 5) {
                alias = alias.substring(0, 5);
            }
            u.Alias = alias;
        }
        if(attributes.containsKey('User.TimeZoneSidKey')) {
            u.TimeZoneSidKey = attributes.get('User.TimeZoneSidKey');
        } else{
            u.TimeZoneSidKey = currentUser.TimeZoneSidKey;
        }
        if(attributes.containsKey('User.EmailEncodingKey')) {
            u.EmailEncodingKey = attributes.get('User.EmailEncodingKey');
        } else{ 
            u.EmailEncodingKey = currentUser.EmailEncodingKey;
        }
    }
    
    /*Best Practises - Breaking the handlerUser method into muliple smaller methods-handling the User info*/
    @TestVisible private void handleUserDetails(boolean create, User u, Map<String, String> attributes,
        String federationIdentifier, boolean isStandard)
    {
        if(attributes.containsKey('User.Email')) {
            u.Email = attributes.get('User.Email');
            u.Username = u.Email+System.Label.PS_Support_Community_User_Suffix; //added 4.20 to suffix username with community     
            
        }
        if(attributes.containsKey('User.LastName')) {
            u.LastName = attributes.get('User.LastName');
        }
        if(attributes.containsKey('User.FirstName')) {
            u.FirstName = attributes.get('User.FirstName');
            if(u.FirstName==null||u.FirstName==''){ //4.20 Added to handle for required fName
            u.FirstName=u.LastName;
            system.debug('**handleUser - Fname**'+ u.FirstName);
            }
        }
        if(attributes.containsKey('User.Title')) {
            u.Title = attributes.get('User.Title');
        }
        if(attributes.containsKey('User.CompanyName')) {
            u.CompanyName = attributes.get('User.CompanyName');
        }
        if(attributes.containsKey('User.AboutMe')) {
            u.AboutMe = attributes.get('User.AboutMe');
        }
        if(attributes.containsKey('User.CommunityNickname')) {
            u.CommunityNickname = attributes.get('User.CommunityNickname');
        }
        
    }
    /*Method for Handling User creation/Updation*/
    @TestVisible private void handleUser(boolean create, User u, Map<String, String> attributes,
        String federationIdentifier, boolean isStandard, Id communityId) {
        
        if(create && attributes.containsKey('User.Username')) {
            //u.Username = attributes.get('User.Username'); //commented to update mappings 4.18 BY
            u.IdamUserName__c =  attributes.get('User.Username'); //added to store new mapping to custom field 4.18 BY
            system.debug('*****in the loop to create user: *IDAM UserName* '+u.IdamUserName__c+' *SFDC UserName* '+u.Username);

        }
        if(create) {
            if(attributes.containsKey('User.FederationIdentifier')) {
                u.FederationIdentifier = attributes.get('User.FederationIdentifier');
                system.debug('*****in the loop to create user: FedID '+u.FederationIdentifier+' *IDAM UserName* '+u.IdamUserName__c+' *SFDC UserName* '+u.Username);
            } else {
                u.FederationIdentifier = federationIdentifier;
                //u.Username = federationIdentifier; //added to store federationId as username 4.18 BY
            }
            //PP:Adding code for R6 Contact to update market as UK
            //Checking for community
            if(communityId != null){
                Network myNetwork = [SELECT Name FROM Network WHERE Id =: communityId];
                //added the below code to populate the originating community on the user_CR-02492
                if(myNetwork.Name == 'Pearson UK Support'){
                    u.Market__c = Label.PS_UKMarket;
                    u.Originating_Community__c = 'Pearson UK Support'; 
                }
                else if(myNetwork.Name == 'Pearson Support'){
                    u.Originating_Community__c = 'Pearson Support';
                }
            }
        }
        handleUserDetails(true, u, attributes, federationIdentifier, true);
        handleUserAddress(true, u, attributes, federationIdentifier, true);
        handleUserPhone(true, u, attributes, federationIdentifier, true);
        string country;
        system.debug(attributes.keyset()+'****PRINT MAPPED***'+attributes.values());
        if(attributes.containsKey('User.Country'))
            country = attributes.get('User.Country');
            if (country==null || country== '') {
                country='US';
            }
        //if(country != null && country == 'United States')
        //{
            if(country !=null){
            Community_User_Geographical_Settings__c cugs = Community_User_Geographical_Settings__c.getInstance(country);
            if(cugs!=null) {
            u.Country = cugs.Country__c;
            u.LocaleSidKey = cugs.LanguageLocaleKey__c;
            if(u.LanguageLocaleKey == null)
            u.LanguageLocaleKey = cugs.LanguageLocaleKey__c;
            if(u.TimeZoneSidKey == null)
            u.TimeZoneSidKey = cugs.TimeZone__c;
            u.EmailEncodingKey = cugs.Email_Encoding__c;
           // u.License_Pool__c ='OPM (Online Programme Management)';
            if(attributes.containsKey('User.Alias')) {
            u.Alias = attributes.get('User.Alias');
            } else{
                String alias = '';
                if(u.FirstName == null||u.FirstName == '') {
                    alias = u.LastName;
                } else {
                    alias = u.FirstName.charAt(0) + u.LastName;
                }
                if(alias.length() > 5) {
                    alias = alias.substring(0, 5);
                }
                u.Alias = alias;
            }
            
          }
          else {
          //Meaning no custom setting record exists for the country specified
              handleCurentUserDetails(true, u, attributes, federationIdentifier, true);
          }
          }
       // }
       else {
        //Meaning country is not passed from IDAM
        handleCurentUserDetails(true, u, attributes, federationIdentifier, true);
        
        }
        handleUserKeyDetails(true, u, attributes, federationIdentifier, true);
                
                
                
        if(!create) {
        try{
            Database.update(u);
            }catch(Exception e){
                            system.debug('** JIT Insert Contact Error** '+e);
            }
        }
            system.debug('User***'+u);
    }
    
    /*Best practise - Breaking the code to create Contact*/
    @TestVisible private void handleContactCreate(boolean create, String AccountId, User u, Map<String, String> attributes,Id communityId)
    {
        boolean newContact;
        system.debug('Inside if(Create)---------------------------->');
                /*
                if(attributes.containsKey('User.Contact')) {
                    system.debug('Inside if(Create)1---------------------------->'+attributes.containsKey('User.Contact'));
                    String contact = attributes.get('User.Contact');
                     system.debug('Inside if(Create)2---------------------------->'+attributes.get('User.Contact'));
                    //c = [SELECT Id, AccountId FROM Contact WHERE Id=:contact];
                    c = [SELECT Id, role__c FROM Contact WHERE Id=:contact LIMIT 1];
                    system.debug('Inside if(Create)3---------------------------->'+c);
                    u.ContactId = contact;
                } */
            if(attributes.get('Contact.Email') != NULL)
                {
                    /*******/
                    system.debug('Exisitng contact loop***'); 
                   system.debug('Contact.Email***'+attributes.get('Contact.Email'));
                   List<Contact> lookForExisitngCon = [Select ID, email from Contact WHERE email =: attributes.get('Contact.Email') LIMIT 50];
                   if(lookForExisitngCon.size()>0)
                   {
                       String contactEmail = attributes.get('User.email');
                       c = new Contact();
                       newContact = false;
                       u.ContactId = lookForExisitngCon[0].ID;
                   }
                    system.debug('New Contact Creation***');
                }
            else 
                {
                  
                     List<Contact> lstExistingUserEmailContact = [Select ID, email from Contact WHERE email =: attributes.get('User.Email') LIMIT 50];
                       if(lstExistingUserEmailContact.size()>0)
                       {
                            u.ContactId = lstExistingUserEmailContact[0].ID;
                       }
                        else
                        {
                            c = new Contact();
                            system.debug('FirstName***'+User.FirstName);
                            c.LastName = attributes.get('User.LastName'); 
                            c.FirstName = attributes.get('User.FirstName');
                            if(c.FirstName == null||c.FirstName ==''){
                            c.FirstName = c.LastName;
                            system.debug('** ExistingContact FirstName Defaulted***'+User.FirstName);
                            }
                            //Added By Manikanta for prepopulating the country
                              string Cntry;
                             /* if(!attributes.containsKey('User.Country')) {//Commented By Manikanta
                                Cntry='United States';
                                }
                                else {
                                    Cntry=attributes.get('User.Country');
                                }*/
                              if(attributes.containsKey('User.Country'))//Added By Manikanta 
                                Cntry = attributes.get('User.Country');
                                if (Cntry ==null || Cntry == '') {
                                    Cntry='US';
                                }
                            if(Cntry!=null)
                            {
                                //Added By Manikanta
                                
                                Community_User_Geographical_Settings__c cugs = Community_User_Geographical_Settings__c.getInstance(Cntry);
                                if(cugs!=null) {
                                    c.Country_of_Origin__c = cugs.Country__c;//attributes.get('User.Country');
                                    c.First_Language__c = cugs.Language__c;
                                }
                                /*List<CS_Country_ContactUs_Mapping__c> ccm = new List<CS_Country_ContactUs_Mapping__c>();//Commented By Manikanta
                                string language;
                                if(c.Country_of_Origin__c != null)
                                ccm = [Select id,Name,Country__c,Language__c from CS_Country_ContactUs_Mapping__c where Country__c =: c.Country_of_Origin__c limit 1];
                                if(!ccm.IsEmpty())
                                {
                                    if(ccm[0].Language__c != null && ccm[0].Language__c.contains(','))
                                    {
                                        string[] languagesplit = ccm[0].Language__c.split(',');
                                        c.First_Language__c = languagesplit[0];
                                    }  
                                    else if(ccm[0].Language__c != null && !ccm[0].Language__c.contains(','))
                                    {
                                        c.First_Language__c = ccm[0].Language__c;
                                    }                                    
                                }*/
                            }
                            c.Phone = attributes.get('User.Phone');
                            c.email = attributes.get('User.Email');
                             //As per Paul request for role coming from IAM
                            String Role = attributes.get('Contact.Role__c');
                            String[] RoleArr = Role.split('\\.');
                            String RoleSFDC = RoleArr[1];
                            System.debug('++++' + RoleSFDC);
                              c.role__c = RoleSFDC;
                              //Added by Rakshik on March 11th 2019 to handle No Role being passed by Hybris
                              /*if (RoleSFDC != null){
                                  System.debug('++++ Inside IF' + RoleSFDC);  
                                  c.role__c = RoleSFDC;  
                              }
                              else{
                                   System.debug('++++ Inside ELSE' + RoleSFDC);
                                   c.role__c = 'Learner';
                                   }*/                                     
                          //  c.role__c = attributes.get('Contact.Role__c');
                            newContact = true;
                            c.sSelfServiceCreated__c = System.label.PS_true;
                            c.role_detail__c = System.label.PS_RoleDetailDefaultSS; //add 4.14 BY for SS validation
                            //PP:Adding code for R6 Contact to update market as UK
                            //Checking for community
                            
                            if(communityId != null){
                                Network myNetwork = new Network();
                                myNetwork = [SELECT Name FROM Network WHERE Id =: communityId];
                                if(myNetwork.Name == 'Pearson UK Support'){
                                    c.Market__c = Label.PS_UKMarket;
                                }
                            }
                            try{
                            insert c;
                            }catch(Exception e){
                            system.debug('** JIT Insert Contact Error** '+e);
                            }
                            Contact con = [Select Id,FirstName, LastName,AccountId from Contact where Id =: c.Id limit 1];
                            System.debug('New Contact***'+c);
                            System.debug('ContactID***'+c.ID);
                             System.debug('AccountId***'+con.AccountId);
                            u.ContactId = con.ID;
                        }
                   
                }
    }
    /*Method to handle creation/updating Contact on Insert of
    New User*/
    @TestVisible private void handleContact(boolean create, String AccountId, User u, Map<String, String> attributes, Id communityId) {
        boolean newContact;
        //create = true;
        string account = '';
        if(create) {
        system.debug('***************insideCreateContact');
        handleContactCreate(true, account , u, attributes, communityId);
        } 
        else 
        {
            system.debug('***************insideELSEContact');
            if(attributes.containsKey('User.Contact')) {
                String contact = attributes.get('User.Contact');
                c = [SELECT Id FROM Contact WHERE Id=:contact LIMIT 1];
                if(u.ContactId != c.Id) {
                    throw new JitException('Cannot change User.ContactId');
                }
            } 
            else
            {
                String contact = u.ContactId;
                c = [SELECT Id FROM Contact WHERE Id=:contact LIMIT 1];
            }
        } 
        system.debug('con***'+c);
    }
    /*Method for Handling Account creation before insert/update of User- Commented the code since we are inserting Account using trigger
    /* Code Commented for future usage*/
    /*@TestVisible private String handleAccount(boolean create, User u, Map<String, String> attributes) {
        Account a;
        boolean newAccount = false;
       
        if(create) {
            system.debug('Inside Create Account------------------------->');
            if(attributes.containsKey('User.Account')) {
                String account = attributes.get('User.Account');
                a = [SELECT Id FROM Account WHERE Id=:account LIMIT 1];
            } else {
                system.debug('Inside Create Account Else------------------------->');
                if(attributes.containsKey('User.Contact')) {
                    String con = attributes.get('User.Contact');
                    Contact c = [SELECT AccountId FROM Contact WHERE Id=:con LIMIT 1];
                    String account = c.AccountId;
                    a = [SELECT Id FROM Account WHERE Id=:account LIMIT 1];
                } else {
                    system.debug('Inside Create Account lse 2------------------------->');
                    a = new Account();
                    newAccount = true;
                }
            }
        }
        
    else {
            if(attributes.containsKey('User.Account')) {
            String account = attributes.get('User.Account');
                a = [SELECT Id FROM Account WHERE Id=:account LIMIT 1];
            } else {
                if(attributes.containsKey('User.Contact')) {
                    String contact = attributes.get('User.Contact');
                    Contact c = [SELECT Id, AccountId, role__c FROM Contact WHERE Id=:contact LIMIT 1];
                    if(u.ContactId != c.Id) {
                        throw new JitException('Cannot change User.ContactId');
                    }
                    String account = c.AccountId;
                    a = [SELECT Id FROM Account WHERE Id=:account LIMIT 1];
                } else {
                    throw new JitException('Could not find account');
                }
            }
        }        
        if(newAccount) {
            Database.insert(a);
        } else {
            Database.update(a);
        }
        system.debug('a*****cc:'+a);
        return a.Id;
    } */
    /*Main method for JIT which calls various methods to insert/update user, contact and Account*/
    @TestVisible private void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
            System.debug('*****Inside Handle JIT Attributes Values:'+attributes.values());
            System.debug('*****Inside Handle JIT Attributes:'+attributes);
            System.debug('*****CommunityID'+communityID);
            System.debug('*****portalId'+portalId);
        if(communityId != null || portalId != null) 
        {
            //String account = handleAccount(create, u, attributes);
            string account = '';
            handleContact(create, account , u, attributes, communityId);
            //handleContact(create, u, attributes);
            handleUser(create, u, attributes, federationIdentifier, false,communityId);
        } 
        else 
        {
            handleUser(create, u, attributes, federationIdentifier, true,communityId);
        }
    }
    /*Method for User Creation*/
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        User u = new User();
        try{
  
            //System.debug('Contact Inserted***'+conID);
            System.debug('*****Attributes:'+attributes.values());
            handleJit(true, u, samlSsoProviderId, communityId, portalId,
                      federationIdentifier, attributes, assertion);
            System.debug('UserId' + u.id);
            System.debug('CreateUser ******User:'+u);
            }catch(Exception e){
            system.debug('**JIT Create User Global Error** '+e);
            }
            
             return u;
                        
        
    }
    /*Method for Updating User*/
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        system.debug(userId+'****PRINTING USER ID***');
        User u = [SELECT Id, FirstName, ContactId FROM User WHERE Id=:userId LIMIT 1];
        if(u.id == null)
        handleJit(false, u, samlSsoProviderId, communityId, portalId,
            federationIdentifier, attributes, assertion);
    } 
}