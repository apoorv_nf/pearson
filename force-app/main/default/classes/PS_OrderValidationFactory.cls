/************************************************************************************************************
* Apex Interface Name : PS_OrderValidationFactory
* Version             : 1.0 
* Created Date        : 10 Jul 2015
* Function            : Class responsible to have the logic that will be used to determine what implementation of 
*                       PS_OrderValidationInterface should be called.
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Davi Borges              10/Jul/2015            Initial version
* -----------------------------------------------------------------------------------------------------------
* Karan Khanna             04/Dec/2015            Added Market check for US
* Tom Carman                14/12/2015            Added sortOrder() method to dispatch to correct validation implementation.
* Rony Joseph              03/May/2016            Added Market check for Canada using custom label.
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

public class PS_OrderValidationFactory {
    
    public static Map<String, Order> usOrders;
    public static Map<String, Order> globalOrders;
    
    
    
    public static List<PS_OrderValidationInterface> CreateValidations(Map<String,Order> orders,Map<Id,Order> oldOrders, User contextUser )
    {
        
        //At this point R2 all order are North America Sampling Orders
        //R3: Adding check for US Orders
        
        usOrders = new Map<String, Order>();
        globalOrders = new Map<String, Order>();
        sortOrders(orders);
        
        List<PS_OrderValidationInterface> validators = new List<PS_OrderValidationInterface>();
        
        System.debug('#### orders '+orders);
        if(!usOrders.isEmpty()) {
            PS_OrderValidationB2BSampling validatorB2B = new PS_OrderValidationB2BSampling();
            validatorB2B.initialize(usOrders, oldOrders, contextUser);
            validators.add(validatorB2B);
        }
        
        
        if(!globalOrders.isEmpty()) {
            PS_OrderValidationDefault validatorDefault = new PS_OrderValidationDefault();
            validatorDefault.initialize(globalOrders, oldOrders, contextUser);
            validators.add(validatorDefault);
        }
        
        return validators;
    }
    
    
    
    public static void sortOrders(Map<String, Order> orders) {
        
        for(Order ord : orders.values()) {
            if(ord.Market__c == Label.PS_USMarket||ord.Market__c== Label.PS_CAMarket) {
                usOrders.put(ord.Id, ord);
            } else {
                globalOrders.put(ord.Id, ord);
            }
        }
        
    }
}