/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_DeactivateDuplicateProductsBatchTest
   Description:     Test class for PS_DeactivateDuplicateProductsBatch
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   16 Feb 2016      1.0           Accenture NDC                                         Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
 
@isTest 
public class PS_DeactivateDuplicateProductsBatchTest
{
    static testmethod void testDeactivationWithUserInputDate()
    {
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        List<Product2> listWithNewProd = new List<Product2>();
        List<Product2> listWithOldProd = new List<Product2>();
        listWithOldProd = TestDataFactory.createProduct(1); 
        listWithNewProd = TestDataFactory.createProduct(1); 
        List<General_One_CRM_Settings__c> lstWithCustomSettingRecord = new List<General_One_CRM_Settings__c>();
        General_One_CRM_Settings__c newSetting = new General_One_CRM_Settings__c();
        newSetting.Name = 'Duplicate Product Batch Last Run Date';
        newSetting.Category__c = 'Test';
        newSetting.Description__c = 'Test';
        newSetting.Duplicate_Product_Batch_Last_Run_Date__c = System.Today();
        newSetting.Value__c = 'Today';
        lstWithCustomSettingRecord.add(newSetting);
        if(listWithNewProd != null)
        {
            listWithNewProd[0].Business_Unit__c = 'Higher Ed';
            listWithNewProd[0].Market__c = 'UK';
            listWithNewProd[0].ISBN__c = '1234567890';
            listWithNewProd[0].DM_Identifier__c = 'EPM-1234';
            listWithNewProd[0].IsActive = True;
        }
        if(listWithOldProd != null)
        {
            listWithOldProd[0].Business_Unit__c = 'Higher Ed';
            //listWithOldProd[0].Apttus_Config2__ConfigurationType__c='Option';   
              listWithOldProd[0].Configuration_Type__c='Bundle';
            listWithOldProd[0].ISBN__c = '1234567890';
            listWithOldProd[0].DM_Identifier__c = 'BM-1234';
            listWithOldProd[0].Market__c = 'UK';
            listWithOldProd[0].IsActive = True;
        }
        test.startTest();
        insert listWithUser;
        system.runAs(listWithUser[0])
        {
            insert lstWithCustomSettingRecord;
            insert listWithNewProd;
            insert listWithOldProd;
            database.executeBatch(new PS_DeactivateDuplicateProductsBatch(System.Today(),null));
        }
        test.stopTest();   
    }
    static testmethod void testDeactivationWithCustomSettingDate()
    {
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        List<Product2> listWithNewProd = new List<Product2>();
        List<Product2> listWithOldProd = new List<Product2>();
        List<General_One_CRM_Settings__c> lstWithCustomSettingRecord = new List<General_One_CRM_Settings__c>();
        General_One_CRM_Settings__c newSetting = new General_One_CRM_Settings__c();
        newSetting.Name = 'Duplicate Product Batch Last Run Date';
        newSetting.Category__c = 'Test';
        newSetting.Description__c = 'Test';
        newSetting.Duplicate_Product_Batch_Last_Run_Date__c = System.Today();
        newSetting.Value__c = 'Today';
        lstWithCustomSettingRecord.add(newSetting);
        listWithOldProd = TestDataFactory.createProduct(1); 
        listWithNewProd = TestDataFactory.createProduct(1); 
        if(listWithNewProd != null)
        {
            listWithNewProd[0].Business_Unit__c = 'Higher Ed';
            listWithNewProd[0].ISBN__c = '1234567890';
            listWithNewProd[0].DM_Identifier__c = 'EPM-1234';
            listWithNewProd[0].Market__c = 'AU';
            listWithNewProd[0].IsActive = True;
        }
        if(listWithOldProd != null)
        {
            listWithOldProd[0].Business_Unit__c = 'Higher Ed';
            listWithOldProd[0].ISBN__c = '1234567890';
            listWithOldProd[0].DM_Identifier__c = 'BM-1234';
            listWithOldProd[0].Market__c = 'AU';
            listWithOldProd[0].IsActive = True;
           // listWithOldProd[0].Apttus_Config2__ConfigurationType__c='Option';  
             listWithOldProd[0].Configuration_Type__c='Bundle';
        }
        test.startTest();
        insert listWithUser;
        system.runAs(listWithUser[0])
        {
            insert lstWithCustomSettingRecord;
            insert listWithNewProd;
            insert listWithOldProd;
            database.executeBatch(new PS_DeactivateDuplicateProductsBatch(null,null));
        }
        test.stopTest();   
    }
    static testmethod void testBMScenario()
    {
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        List<Product2> listWithNewProd = new List<Product2>();
        List<Product2> listWithOldProd = new List<Product2>();
        List<General_One_CRM_Settings__c> lstWithCustomSettingRecord = new List<General_One_CRM_Settings__c>();
        General_One_CRM_Settings__c newSetting = new General_One_CRM_Settings__c();
        newSetting.Name = 'Duplicate Product Batch Last Run Date';
        newSetting.Category__c = 'Test';
        newSetting.Description__c = 'Test';
        newSetting.Duplicate_Product_Batch_Last_Run_Date__c = System.Today();
        newSetting.Value__c = 'Today';
        lstWithCustomSettingRecord.add(newSetting);
        listWithOldProd = TestDataFactory.createProduct(1); 
        listWithNewProd = TestDataFactory.createProduct(1); 
        if(listWithNewProd != null)
        {
            listWithNewProd[0].Business_Unit__c = 'Higher Ed';
            listWithNewProd[0].ISBN__c = '1234567890';
            listWithNewProd[0].DM_Identifier__c = 'BM-1234';
            listWithNewProd[0].IsActive = True;
            listWithNewProd[0].Market__c = 'AU';
        }
        if(listWithOldProd != null)
        {
            listWithOldProd[0].Business_Unit__c = 'Higher Ed';
            listWithOldProd[0].ISBN__c = '1234567890';
            listWithOldProd[0].DM_Identifier__c = 'EPM-1234';
            listWithOldProd[0].Market__c = 'AU';
            listWithOldProd[0].IsActive = True;
           // listWithOldProd[0].Apttus_Config2__ConfigurationType__c='Option';
             listWithOldProd[0].Configuration_Type__c='Bundle';
        }
        test.startTest();
        insert listWithUser;
        system.runAs(listWithUser[0])
        {
            insert lstWithCustomSettingRecord;
            insert listWithNewProd;
            insert listWithOldProd;
            database.executeBatch(new PS_DeactivateDuplicateProductsBatch(null, null));
        }
        test.stopTest();   
    }
    
    static testmethod void testExceptionLoggerScenario()
    {
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        List<Product2> listWithNewProd = new List<Product2>();
        List<Product2> listWithOldProd = new List<Product2>();
        List<General_One_CRM_Settings__c> lstWithCustomSettingRecord = new List<General_One_CRM_Settings__c>();
        General_One_CRM_Settings__c newSetting = new General_One_CRM_Settings__c();
        newSetting.Name = 'Duplicate Product Batch Last Run Date';
        newSetting.Category__c = 'Test';
        newSetting.Description__c = 'Test';
        newSetting.Duplicate_Product_Batch_Last_Run_Date__c = System.Today();
        newSetting.Value__c = 'Today';
        lstWithCustomSettingRecord.add(newSetting);
        listWithOldProd = TestDataFactory.createProduct(1); 
        listWithNewProd = TestDataFactory.createProduct(1); 
        if(listWithNewProd != null)
        {
            listWithNewProd[0].Business_Unit__c = 'Higher Ed';
            listWithNewProd[0].ISBN__c = '1234567890';
            listWithNewProd[0].DM_Identifier__c = 'SSS-1234';
            listWithNewProd[0].IsActive = True;
            listWithNewProd[0].Market__c = 'AU';
        }
        if(listWithOldProd != null)
        {
            listWithOldProd[0].Business_Unit__c = 'Higher Ed';
            listWithOldProd[0].ISBN__c = '1234567890';
            listWithOldProd[0].DM_Identifier__c = 'HHH-1234';
            listWithOldProd[0].Market__c = 'AU';
            listWithOldProd[0].IsActive = True;
           // listWithOldProd[0].Apttus_Config2__ConfigurationType__c='Option';
             listWithOldProd[0].Configuration_Type__c='Bundle';
        }
        test.startTest();
        insert listWithUser;
        system.runAs(listWithUser[0])
        {
            insert lstWithCustomSettingRecord;
            insert listWithNewProd;
            insert listWithOldProd;
            database.executeBatch(new PS_DeactivateDuplicateProductsBatch(null, null));
        }
        test.stopTest();   
    }
    
    static testmethod void testBMScenarioForUK()
    {
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        List<Product2> listWithNewProd = new List<Product2>();
        List<Product2> listWithOldProd = new List<Product2>();
        listWithOldProd = TestDataFactory.createProduct(1); 
        listWithNewProd = TestDataFactory.createProduct(1); 
        List<General_One_CRM_Settings__c> lstWithCustomSettingRecord = new List<General_One_CRM_Settings__c>();
        General_One_CRM_Settings__c newSetting = new General_One_CRM_Settings__c();
        newSetting.Name = 'Duplicate Product Batch Last Run Date';
        newSetting.Category__c = 'Test';
        newSetting.Description__c = 'Test';
        newSetting.Duplicate_Product_Batch_Last_Run_Date__c = System.Today();
        newSetting.Value__c = 'Today';
        lstWithCustomSettingRecord.add(newSetting);
        if(listWithNewProd != null)
        {
            listWithNewProd[0].Business_Unit__c = 'Higher Ed';
            listWithNewProd[0].ISBN__c = '1234567890';
            listWithNewProd[0].DM_Identifier__c = 'BM-1234';
            listWithNewProd[0].IsActive = True;
            listWithNewProd[0].Market__c = 'UK';
        }
        if(listWithOldProd != null)
        {
            listWithOldProd[0].Business_Unit__c = 'Higher Ed';
            listWithOldProd[0].ISBN__c = '1234567890';
            listWithOldProd[0].DM_Identifier__c = 'EPM-1234';
            listWithOldProd[0].Market__c = 'UK';
            listWithOldProd[0].IsActive = True;
           // listWithOldProd[0].Apttus_Config2__ConfigurationType__c='Option';
             listWithOldProd[0].Configuration_Type__c='Bundle';
        }
        test.startTest();
        insert listWithUser;
        system.runAs(listWithUser[0])
        {
            insert lstWithCustomSettingRecord;
            insert listWithNewProd;
            insert listWithOldProd;
            database.executeBatch(new PS_DeactivateDuplicateProductsBatch(System.Today(), null));
        }
        test.stopTest();   
    }
    static testmethod void testBMScenarioForUK1()
    {
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        List<Product2> listWithNewProd = new List<Product2>();
        List<Product2> listWithOldProd = new List<Product2>();
        listWithOldProd = TestDataFactory.createProduct(1); 
        listWithNewProd = TestDataFactory.createProduct(1); 
        List<General_One_CRM_Settings__c> lstWithCustomSettingRecord = new List<General_One_CRM_Settings__c>();
        General_One_CRM_Settings__c newSetting = new General_One_CRM_Settings__c();
        newSetting.Name = 'Duplicate Product Batch Last Run Date';
        newSetting.Category__c = 'Test';
        newSetting.Description__c = 'Test';
        newSetting.Duplicate_Product_Batch_Last_Run_Date__c = System.Today();
        newSetting.Value__c = 'Today';
        lstWithCustomSettingRecord.add(newSetting);
        if(listWithNewProd != null)
        {
            listWithNewProd[0].Business_Unit__c = 'Higher Ed';
            listWithNewProd[0].ISBN__c = '1234567890';
            listWithNewProd[0].DM_Identifier__c = 'BM-1234';
            listWithNewProd[0].IsActive = True;
            listWithNewProd[0].Market__c = 'UK';
        }
        if(listWithOldProd != null)
        {
            listWithOldProd[0].Business_Unit__c = 'Higher Ed';
            listWithOldProd[0].ISBN__c = '1234567890';
            listWithOldProd[0].DM_Identifier__c = 'EPM-1234';
            listWithOldProd[0].Market__c = 'UK';
            listWithOldProd[0].IsActive = True;
            //listWithOldProd[0].Apttus_Config2__ConfigurationType__c='Option';
            listWithOldProd[0].Configuration_Type__c='Bundle';
        }
        test.startTest();
        insert listWithUser;
        system.runAs(listWithUser[0])
        {
            insert lstWithCustomSettingRecord;
            insert listWithNewProd;
            insert listWithOldProd;
            database.executeBatch(new PS_DeactivateDuplicateProductsBatch(System.Today(), null));
        }
        test.stopTest();   
    }
    static testmethod void testExceptionLoggerForUK()
    {
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        List<Product2> listWithNewProd = new List<Product2>();
        List<Product2> listWithOldProd = new List<Product2>();
        listWithOldProd = TestDataFactory.createProduct(1); 
        listWithNewProd = TestDataFactory.createProduct(1); 
        List<General_One_CRM_Settings__c> lstWithCustomSettingRecord = new List<General_One_CRM_Settings__c>();
        General_One_CRM_Settings__c newSetting = new General_One_CRM_Settings__c();
        newSetting.Name = 'Duplicate Product Batch Last Run Date';
        newSetting.Category__c = 'Test';
        newSetting.Description__c = 'Test';
        newSetting.Duplicate_Product_Batch_Last_Run_Date__c = System.Today();
        newSetting.Value__c = 'Today';
        lstWithCustomSettingRecord.add(newSetting);
        if(listWithNewProd != null)
        {
            listWithNewProd[0].Business_Unit__c = 'Higher Ed';
            listWithNewProd[0].ISBN__c = '1234567890';
            listWithNewProd[0].DM_Identifier__c = 'SSS-1234';
            listWithNewProd[0].IsActive = True;
            listWithNewProd[0].Market__c = 'UK';
        }
        if(listWithOldProd != null)
        {
            listWithOldProd[0].Business_Unit__c = 'Higher Ed';
            listWithOldProd[0].ISBN__c = '1234567890';
            listWithOldProd[0].DM_Identifier__c = 'HHH-1234';
            listWithOldProd[0].Market__c = 'UK';
            listWithOldProd[0].IsActive = True;
            //listWithOldProd[0].Apttus_Config2__ConfigurationType__c='Option';
              listWithOldProd[0].Configuration_Type__c='Bundle';
        }
        test.startTest();
        insert listWithUser;
        system.runAs(listWithUser[0])
        {
            insert lstWithCustomSettingRecord;
            insert listWithNewProd;
            insert listWithOldProd;
            database.executeBatch(new PS_DeactivateDuplicateProductsBatch(System.Today(), null));
        }
        test.stopTest();   
    }
}