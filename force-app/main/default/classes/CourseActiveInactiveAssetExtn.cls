public class CourseActiveInactiveAssetExtn {
    public Id posId       {get;set;}
    public String votId       {get;set;}
    public String status;
    public String uName     {get;set;}
    public Id aId       {get;set;}
    public Id arId      {get;set;}
    public list<Asset> assetdel {get;set;}
    public Boolean nullQtyError {get;set;}
    public Map<String, Schema.SObjectField> fieldMap {get;set;}
    public string selOptions1 {get;set;}
    public string sortOptions1 {get;set;}
    public string selOptions2 {get;set;}
    public string sortOptions2 {get;set;}
    public string soql;
    public Boolean isSearch {get;set;}
    public Boolean isSort {get;set;}
    public String searchStr {get;set;}
    Public List<SelectOption> condOptions {get;set;}
    Public List<SelectOption> options {get;set;}
    Public String courseType;
    Public Date last1095Days; 
    public boolean addNewFilterLink {get;set;}
      
    public String selectedFilterbyValue {get;set;}
        
    public Integer selectedRow {get;set;}
       
    public Integer rowNumber {get;set;}
      
      public List < dynamicAddFilterSearch > listWithSelectOptions {
        get {
            return listWithSelectOptions;
        }
        set;
    }
    public List < SelectOption > searchKey {get;set;}
    public List < SelectOption > searchType {get;set;}
    public Integer rowToRemove {get;set;}
    
    public Integer rowNumber1 {get;set;}
    public integer loadtime{get;set;}
    public Boolean toDisplayLookup {get;set;}
    public String selectedSearchType {get;set;} 
    public String selectedCondition {get;set;}
    public Integer rowToRemove1 {get;set;}
    Public List<SelectOption> condOptions1 {get;set;}
    public String JsonMap{get;set;} 
    Public String selectedValueFromPopup {get;set;}
    public Boolean butType {get;set;}
    public Map<String,SChema.DisplayType> dataTypeMap {get;set;}
     public List < dynamicAddFilterSearch1 > listWithSelectOptions1 {
          get {
            return listWithSelectOptions1;
        }
        set;
    }
     
    
    public CourseActiveInactiveAssetExtn(ApexPages.StandardController controller) {
        
               //  posId = controller.getRecord().Id; 
        posId = ApexPages.currentPage().getParameters().get('CourseId');
        courseType = ApexPages.currentPage().getParameters().get('Type');
        if(courseType == 'Active')
        {
            butType = true;
        }else
        {
            butType = false;
        }
        isSearch = false;
        isSort = false;
        nullQtyError = false;
        last1095Days  = System.Today()-1095;
        addNewFilterLink = true;
        loadtime = 0; 
        this.ViewData();
        this.fetchUniv();
        getField();
        getCondOperations();
        getCondOperations1();
        listWithSelectOptions = new List < dynamicAddFilterSearch > ();
        listWithSelectOptions1 = new List < dynamicAddFilterSearch1 > (); 
        
        SObjectType productType = Schema.getGlobalDescribe().get('Asset');
        fieldMap = productType.getDescribe().fields.getMap();
        dataTypeMap = new Map<String,SChema.DisplayType>();
        dataTypeMap.put('--None--',Schema.DisplayType.STRING);
         for (String fieldName: fieldMap.keySet()) { 
            dataTypeMap.put(fieldName,fieldMap.get(fieldName).getDescribe().getType());
         }
        JsonMap = JSON.serialize(datatypemap);       
        addNewFilter();
        addNewFilter1();
    }
    
    public void getCondOperations1() {
     condOptions1  = new List<SelectOption>(); 
     condOptions1.add(new SelectOption('--None--','--None--'));
     condOptions1.add(new SelectOption('=','equals'));
     condOptions1.add(new SelectOption('!=','not equal to'));
     condOptions1.add(new SelectOption('like %','starts with'));
     condOptions1.add(new SelectOption('like %%','contains'));
     condOptions1.add(new SelectOption('like','does not contain'));
    }
    
    public void searchResults() {
        assetdel= new List<Asset>();
        searchStr = '';
        try{
            if(listWithSelectOptions1.size()>0){
                Boolean flag = false;
                Integer count = 0;
                if(courseType == 'Inactive'){
                 soql = 'select id, Name, Competitor_Product_in_Use__c, Product_Author__c, Product_Edition__c,Copyright_Year__c,Contact.Name,Usage__c,Competitor_Publisher__c,Rollover2__c,Status__c,InstallDate from Asset where Course__c = '+'\''+posId+'\''+' and Status__c= '+'\''+'Inactive'+'\''+' AND Inactive_Status_Date__c >= '+String.ValueOf(last1095Days)+' AND ';
               } else
               {
                soql = 'select id, Name, Competitor_Product_in_Use__c, Product_Author__c, Product_Edition__c,Copyright_Year__c,Contact.Name,Usage__c,Competitor_Publisher__c,Rollover2__c,Status__c,InstallDate from Asset where Course__c = '+'\''+posId+'\''+' and Status__c= '+'\''+'Active'+'\''+' AND ';
               }
            for(dynamicAddFilterSearch1 wrapper : listWithSelectOptions1){
                    String selCondOptions1 = wrapper.selectedCondition;
                    String selectedField = wrapper.selectedProductFilterValue;
                    String searchText1 = wrapper.searchText;
                    String searchText11 = '';
                    //Date dateToCompare;
                    Boolean doesNotContains = false;                    
                    
                    if (selCondOptions1 == '--None--' || wrapper.selectedProductFilterValue == '--None--') {
                       count=count+1;
                       continue; 
                   } 
                    Schema.DisplayType dataType = Schema.DisplayType.STRING;
                    if(fieldMap.containsKey(selectedField)){
                     dataType = fieldMap.get(selectedField).getDescribe().getType();
                     }
                    String operator = '';
                   //soql = soql+selectedField;
                    
                    if(selCondOptions1 == 'like %')
                    {
                        if(searchText1 == '')
                            searchText11 = '= \''+searchText1+'\'';
                        else
                          searchText11 = 'LIKE \''+searchText1+'%\'';
                        
                    }else if(selCondOptions1 == 'like %%')
                    {
                        if(searchText1 == '')
                            searchText11 = '= \''+searchText1+'\'';
                        else
                          searchText11 = 'LIKE \'%'+searchText1+'%\'';
                    }else if(selCondOptions1 == 'like')
                    {
                        if(searchText1 == '')
                              searchText11 = '<> \''+searchText1+'\'';
                        else{
                        searchText11 = ' ( NOT '+selectedField+' LIKE \'%'+searchText1+'%\' )';
                        doesNotContains = true;        
                          }
                    
                    }else if(selCondOptions1 == '=')
                    {  
                      if (dataType == Schema.DisplayType.Integer || dataType == Schema.DisplayType.Boolean || dataType == Schema.DisplayType.Double)                        
                      {
                          if(searchText1 == '')
                              searchText11 = '= null';
                          else
                              searchText11 = '= '+searchText1;
                      }
                       else if(dataType == Schema.DisplayType.DATE )  
                       {
                           if(searchText1 == '')
                               searchText11 = '= null';  
                           else
                               searchText11 = '= '+convertToDate(searchText1);
                       }
                       else
                          searchText11 = '= \''+searchText1+'\'';   
                      
                    }else if(selCondOptions1 == '!=')
                    {
                       if(dataType == Schema.DisplayType.Integer || dataType == Schema.DisplayType.Boolean || dataType == Schema.DisplayType.Double)                        
                       {
                           if(searchText1 == '')
                               searchText11 = '<> null';
                           else
                             searchText11 = '<> '+searchText1;
                       }
                       else if(dataType == Schema.DisplayType.DATE )
                       {
                           if(searchText1 == '')
                               searchText11 = '<> null';
                           else
                               searchText11 = '<> '+convertToDate(searchText1);
                       }                           
                       else
                          searchText11 = '<> \''+searchText1+'\'';     
 
                                          
                    }else if(selCondOptions1 == '<')
                    {
                        if(dataType == Schema.DisplayType.Integer || dataType == Schema.DisplayType.Double)  
                        {
                            if(searchText1 == '')
                                   searchText11 = '<> null';
                            else
                                searchText11 = '< '+searchText1;
                        }
                        else if(dataType == Schema.DisplayType.DATE ) 
                        {
                            if(searchText1 == '')
                                 searchText11 = '<> null';
                            else
                                searchText11 = '< '+convertToDate(searchText1);
                        }
                    }
                    else if(selCondOptions1 == '>')
                    {
                         if(dataType == Schema.DisplayType.Integer || dataType == Schema.DisplayType.Double)                        
                         {
                             if(searchText1 == '')
                                   searchText11 = '<> null';
                             else
                                    searchText11 = '> '+searchText1;
                         }
                        else if(dataType == Schema.DisplayType.DATE )                           
                        {
                            if(searchText1 == '')
                                 searchText11 = '<> null';
                            else
                                searchText11 = '> '+convertToDate(searchText1);
                        }
                     }
                                        
                    if(!flag){
                    if(doesNotContains)
                    searchStr = searchStr + ' ' + searchText11;
                    //soql = soql+' '+searchText11;
                    else
                    searchStr = searchStr + selectedField+' '+searchText11;
                    //soql = soql+selectedField+' '+searchText11;
                    
                    flag = true;
                    }
                    else{
                    if(doesNotContains)
                    searchStr = searchStr + ' AND '+' '+searchText11;
                    //soql = soql+' AND '+' '+searchText11;    
                    else
                    searchStr = searchStr + ' AND '+selectedField+' '+searchText11;
                    //soql = soql+' AND '+selectedField+' '+searchText11;    
                    }
                    
                    }
                    
                                
                if(listWithSelectOptions1.size() == count){
                    if(isSort)
                  {
                    isSearch = false;
                  } else
                  {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please select the search criteria'));
                  }
                }else{
                    if(!isSort)
                  {
                    isSearch = true;   
                    soql = soql + searchStr + ' ORDER BY Contact.Name asc,InstallDate desc LIMIT 999';
                    //soql = soql + ' LIMIT 999';
                    assetdel= Database.query(soql); 
                    soql = '';
                  }
                   
                   }                
            }
            
        }
        catch(Exception e){
            
        }
        
        
    }
    
    public String convertToDate(String searchText){
                    string[] dateStr = searchText.split('/');
                    String dateToCompare = String.valueOf(date.newInstance(Integer.valueOf(dateStr[2]), Integer.valueOf(dateStr[0]), Integer.valueOf(dateStr[1]))); // yyyy-mm-dd                              
                    return dateToCompare;  
 
         }
         
   
     public void getField() {
     options  = new List<SelectOption>(); 
     options.add(new SelectOption('--None--','--None--'));
     options.add(new SelectOption('name','Product In Use'));
     options.add(new SelectOption('product_author__c','Product Author'));
     options.add(new SelectOption('product_edition__c','Product Edition'));
     options.add(new SelectOption('copyright_year__c','Copyright Year'));
     options.add(new SelectOption('contact.name','Contact'));
     options.add(new SelectOption('usage__c','Usage'));
     options.add(new SelectOption('competitor_publisher__c','Competitor Publisher'));
     options.add(new SelectOption('status__c','Status'));
     options.add(new SelectOption('installdate','Install Date'));
     options.add(new SelectOption('Rollover2__c','Rollover'));         
     }
    public void getCondOperations() {
     condOptions  = new List<SelectOption>(); 
     condOptions.add(new SelectOption('--None--','--None--'));
     condOptions.add(new SelectOption('ASC','Ascending'));
     condOptions.add(new SelectOption('DESC','Descending'));
     }    
    public void sortData()
    {  
        Integer count = 0;
        isSort = true;
        isSearch = true;
        this.searchResults();
       
        if(isSearch == TRUE)
        {
                if(courseType == 'Inactive')
                {
                    
                    soql = 'select id, Name, Competitor_Product_in_Use__c, Product_Author__c, Product_Edition__c,Copyright_Year__c,Contact.Name,Usage__c,Competitor_Publisher__c,Rollover2__c,Status__c,InstallDate from Asset where '+searchStr+' AND '+'Course__c = '+'\''+posId+'\''+' and Status__c= '+'\''+'Inactive'+'\''+' AND Inactive_Status_Date__c >= '+String.ValueOf(last1095Days)+' ORDER BY ';
                }
                else
                {
                    soql = 'select id, Name, Competitor_Product_in_Use__c, Product_Author__c, Product_Edition__c,Copyright_Year__c,Contact.Name,Usage__c,Competitor_Publisher__c,Rollover2__c,Status__c,InstallDate from Asset where '+searchStr+' AND '+'Course__c = '+'\''+posId+'\''+' and Status__c= '+'\''+'Active'+'\''+' ORDER BY ';
                }
        } else
        {
                if(courseType == 'Inactive')
                {
                    
                    soql = 'select id, Name, Competitor_Product_in_Use__c, Product_Author__c, Product_Edition__c,Copyright_Year__c,Contact.Name,Usage__c,Competitor_Publisher__c,Rollover2__c,Status__c,InstallDate from Asset where Course__c = '+'\''+posId+'\''+' and Status__c= '+'\''+'Inactive'+'\''+' AND Inactive_Status_Date__c >= '+String.ValueOf(last1095Days)+' ORDER BY ';
                }
                else
                {
                    soql = 'select id, Name, Competitor_Product_in_Use__c, Product_Author__c, Product_Edition__c,Copyright_Year__c,Contact.Name,Usage__c,Competitor_Publisher__c,Rollover2__c,Status__c,InstallDate from Asset where Course__c = '+'\''+posId+'\''+' and Status__c= '+'\''+'Active'+'\''+' ORDER BY ';
                }
        }
        if(listWithSelectOptions.size()>0){
                Boolean flag = false;
               
        for(dynamicAddFilterSearch fObject : listWithSelectOptions){
            String sField = fObject.selField;
            String kField = fObject.serField;
            if(sField == '--None--' || kField == '--None--')
            {
                count = count + 1;
                continue;
            }
            
         if(!flag){
                    soql = soql + sField +' '+kField;
                    flag = true;
            }
         else
            {
                    soql = soql +','+ sField +' '+kField;         
            }
        }}
        if(listWithSelectOptions.size() == count){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please select the Sort criteria'));
         }else
            {
                
                   soql = soql + ' LIMIT 999';
                   assetdel = Database.query(soql);
                   soql = '';
            }
        isSort = false;//Veera
}
   public void fetchUniv()
   {
       UniversityCourse__c uniDetl = [SELECT Id,Name, Account__c,Account__r.Id FROM UniversityCourse__c where Id=:posId];
       uName = uniDetl.Name;
       aId = uniDetl.Account__c;
       arId = uniDetl.Account__r.Id;
   }
   public PageReference delVote()
   {
       try{
           
       
       Asset toDel=new Asset(id=votId);
  
        delete todel;
  
        ViewData();
   
     return null;
           }
       catch(Exception e)
       {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'System Error'));
           nullQtyError = true; 
           return null;
       }
   }
   public PageReference cancel()
   {
       return new PageReference('/'+posId);
   }
   public void ViewData() {
        if(courseType == 'Inactive')
        {
            
            assetdel = [select id, Name, Competitor_Product_in_Use__c, Product_Author__c, Product_Edition__c,Copyright_Year__c,Contact.Name,Usage__c,Competitor_Publisher__c,Rollover2__c,Status__c,InstallDate from Asset where Course__c=:posId and Status__c= 'Inactive' AND Inactive_Status_Date__c >=:last1095Days ORDER BY Contact.Name asc,InstallDate desc LIMIT 999];
        } else
        {
             assetdel = [select id, Name, Competitor_Product_in_Use__c, Product_Author__c, Product_Edition__c,Copyright_Year__c,Contact.Name,Usage__c,Competitor_Publisher__c,Rollover2__c,Status__c,InstallDate from Asset where Course__c=:posId and Status__c= 'Active'  ORDER BY Contact.Name asc,InstallDate desc LIMIT 999];
        }
   }
     /**
* Description : Inner class for values Added For Custom Filter Search
* @param String
* @return String
* @throws NA
**/
    public class dynamicAddFilterSearch {
        
        public List < SelectOption > searchType {
            get;
            set;
        }
        public List < SelectOption > searchKey {
            get;
            set;
        }
        public String selField {
            get;
            set;
        } 
        public String serField {
            get;
            set;
        } 
        public dynamicAddFilterSearch(List < SelectOption > searchfType, List < SelectOption > searchfKey, String selfField, String serfField) {
            searchType = searchfType;
            searchKey = searchfKey;
            selField = selfField;
            serField = serfField;
        }
    }
    /**
* Description : Retrieve Code (4 char) values from Multi - Select picklist
* @param String
* @return String
* @throws NA
**/
    public pageReference addNewFilter() {
        if (listWithSelectOptions.size() <= 4) {
            rowNumber = listWithSelectOptions.size();            
            listWithSelectOptions.add(new dynamicAddFilterSearch(options, condOptions, '--None--', '--None--'));
        } else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'You can have only five filter(s)');
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
    
     /**
* Description : Remove filter functionality 
* @param String
* @return String
* @throws NA
**/
    public void removeFilter() {
        listWithSelectOptions = removeFilterRow(rowToRemove, listWithSelectOptions);
    }
    
     /**
* Description : Remove selected filter functionality 
* @param String
* @return String
* @throws NA
**/
    public static List < dynamicAddFilterSearch > removeFilterRow(Integer rowToRemove, List < dynamicAddFilterSearch > listWithSelectOptionss) {
        listWithSelectOptionss.remove(rowToRemove);
        return listWithSelectOptionss;
    }
    
    
    public class dynamicAddFilterSearch1 {
        public List < SelectOption > productFieldLists {
            get;
            set;
        }
        public List < SelectOption > conditionLists {
            get;
            set;
        }
        public List < SelectOption > searchType {
            get;
            set;
        }
        public String searchText {
            get;
            set;
        } 
        public String searchTextDate {
            get;
            set;
        }
        public String selectedCondition {
            get;
            set;
        }
        public String selectedProductFilterValue {
            get;set;
        }
        public String selectedSearchType {
            get;
            set;
        } 
        public Boolean toDisplayLookup {
            get;
            set;
        } 
        public Integer rowNumber {
            get;
            set;
        } 
        public dynamicAddFilterSearch1(String s,List < SelectOption > productFieldLstWrapperCons, List < SelectOption > conditionLstWrapperCons, List < SelectOption > searchTypeLstWrapperCons, String selectedSearchTypeInput, String searchTextInput, String selectedConditionInput, boolean toDisplayLookupInput, Integer rowNumberInput) {
            productFieldLists = productFieldLstWrapperCons;
            conditionLists = conditionLstWrapperCons;
            searchText = searchTextInput;
            rowNumber = rowNumberInput;
            selectedCondition = selectedConditionInput;
            selectedProductFilterValue = s; 
                       
        }
    }
    
     /**
* Description : Retrieve Code (4 char) values from Multi - Select picklist
* @param String
* @return String
* @throws NA
**/
    public pageReference addNewFilter1() {
        toDisplayLookup = false;
        loadtime = loadtime+1;
         String searchText = ''; 
        if (listWithSelectOptions1.size() <= 4) {
            rowNumber1 = listWithSelectOptions1.size();
            getCondOperations1();
            
            listWithSelectOptions1.add(new dynamicAddFilterSearch1('',options, condOptions1, searchType, selectedSearchType, searchText, selectedCondition, toDisplayLookup, rowNumber1));    
        } else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'You can have only five filter(s)');
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
/**
* Description : in Serach, if user selects date type then display search text of date etc
* @param String
* @return String
* @throws NA
**/
    public void changeSerachTextType() {
        try{
        selectedFilterbyValue = Apexpages.currentPage().getParameters().get('selectedFilterbyValue');
        selectedRow = Integer.valueOf(Apexpages.currentPage().getParameters().get('selectedRow'));
        for (dynamicAddFilterSearch1 wrapperClass: listWithSelectOptions1) {
            String selectedField = wrapperClass.selectedProductFilterValue;
            if(selectedField != '--None--' && selectedField != '' && selectedField != null){
                Schema.DisplayType dataType = fieldMap.get(selectedField).getDescribe().getType();
                 if (( dataType == schema.DisplayType.DATE || dataType == schema.DisplayType.INTEGER || dataType == schema.DisplayType.DOUBLE) && wrapperClass.rowNumber == selectedRow) {
                wrapperClass.toDisplayLookup = false;                    
                condOptions1 = new List < SelectOption > ();
                condOptions1.add(new SelectOption('=','equals'));
                condOptions1.add(new SelectOption('!=','not equal to'));
                condOptions1.add(new SelectOption('<','less than'));
                condOptions1.add(new SelectOption('>','greater than'));
                wrapperClass.conditionLists = condOptions1;
            }else if((dataType == SChema.DisplayType.BOOLEAN) && wrapperClass.rowNumber == selectedRow)
            {
                wrapperClass.toDisplayLookup = true;
                condOptions1 = new List < SelectOption > ();
                condOptions1.add(new SelectOption('=','equals'));
                condOptions1.add(new SelectOption('!=','not equal to'));
                wrapperClass.conditionLists = condOptions1;
            }else if((dataType == SChema.DisplayType.PICKLIST) && wrapperClass.rowNumber == selectedRow){
            
                wrapperClass.toDisplayLookup = true;
                getCondOperations1();
                wrapperClass.conditionLists = condOptions1;
            }else if(wrapperClass.rowNumber == selectedRow){
                wrapperClass.toDisplayLookup = false; 
                getCondOperations1();
                wrapperClass.conditionLists = condOptions1;
            }
             }else if(wrapperClass.rowNumber == selectedRow){
                wrapperClass.toDisplayLookup = false; 
                getCondOperations1();
                wrapperClass.conditionLists = condOptions1;
                    
                }
            
            }
        }
        catch(Exception e){
        }
      }
      
       /**
* Description : Remove filter functionality 
* @param String
* @return String
* @throws NA
**/
    public void removeFilter1() {
        listWithSelectOptions1 = removeFilterRow1(rowToRemove1, listWithSelectOptions1);
    }
    
  
     /**
* Description : Remove selected filter functionality 
* @param String
* @return String
* @throws NA
**/
    public static List < dynamicAddFilterSearch1 > removeFilterRow1(Integer rowToRemove, List < dynamicAddFilterSearch1 > listWithSelectOptionss) {        listWithSelectOptionss.remove(rowToRemove);
        return listWithSelectOptionss;
    }

  /**
* Description : fetch status pikclist field values dynamiccally
* @param NA
* @return list
* @throws NA
**/
    public List < SelectOption > getStatuses() {
        String fieldName = Apexpages.currentPage().getParameters().get('fieldName');
        List < SelectOption > options = new List < SelectOption > ();
        Schema.DescribeFieldResult fieldResult = fieldMap.get(fieldName).getDescribe();
        
        if(fieldResult.getType() == SChema.DisplayType.PICKLIST){
        List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }        
        }
        else if(fieldResult.getType() == SChema.DisplayType.BOOLEAN){
             options.add(new SelectOption('True', 'True'));
             options.add(new SelectOption('False', 'False'));
        }
          return options;
    }
}