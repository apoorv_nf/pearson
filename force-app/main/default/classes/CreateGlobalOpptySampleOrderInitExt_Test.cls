/************************************************************************************************************
* Apex Class Name   : CreateGlobalOpptySampleOrderInitExt_Test.cls 
* Version           : 1.0 
* Created Date      : 26 Sept 2018
* Function          : test class for CreateGlobalOpptySampleOrderInitExt 
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Shiva Nampally           26/09/2018              test class for CreateGlobalOpptySampleOrderInitExt
************************************************************************************************************/



@isTest(seeAllData=False)
Public class CreateGlobalOpptySampleOrderInitExt_Test
{
    @testSetup static void setup()
    { 
        Test.StartTest();
        Profile ProfileNameadmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
 
        List<User> u1 = TestDataFactory.createUser(ProfileNameadmin.Id,1);
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu1'; 
            u1[0].Email ='CreateSampleOrderTest@lexpro.com'; 
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='TestUser'; 
            u1[0].LanguageLocaleKey ='en_US'; 
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = ProfileNameadmin.Id; 
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName = 'CreateSampleOrderTest@lexpro.com';
            u1[0].Market__c ='AU';
            u1[0].Business_Unit__c = 'Higher Ed';
            u1[0].line_of_Business__c = 'Higher Ed';
            u1[0].Product_Business_Unit__c = 'Higher Ed';
            u1[0].Geography__c = 'Core';
            insert u1;
        }  
        
        Bypass_Settings__c settings = new Bypass_Settings__c();
        settings.Disable_Validation_Rules__c = true;
        settings.SetupOwnerId = u1[0].id;         
        insert settings;
        
        list<opportunity> opplist = new list<opportunity>();
        //list<OpportunityLineItem> ol = new list<OpportunityLineItem>();
        //list<product2> prodlist =  new list<product2>();          
        //list<PricebookEntry> pblist =new list<PricebookEntry>();          
        
        List<Account> acc = TestDataFactory.createAccount(1,'Higher Ed');
        if(acc  != null && !acc .isEmpty()){
            acc[0].Name = 'OpptyProposalAccTest1'; 
            acc[0].Line_of_Business__c='Higher Ed'; 
            acc[0].CurrencyIsoCode ='GBP'; 
            acc[0].Geography__c = 'Core'; 
            acc[0].Organisation_Type__c = 'Higher Education'; 
            acc[0].Type = 'School'; 
            acc[0].Phone = '9989887687';
            acc[0].ShippingStreet = 'TestStreet';
            acc[0].ShippingCity = 'Vns';
            acc[0].ShippingState = 'Delhi';
            acc[0].ShippingPostalCode = '234543';
            acc[0].ShippingCountry = 'India';
            acc[0].Market2__c ='AU';
            acc[0].IsCreatedFromLead__c = True;
            insert acc;
        }
            
        List <Contact> contactRecord = TestDataFactory.createContact(1);
        if(contactRecord != null && !contactRecord.isEmpty()){
            contactRecord[0].FirstName='TestopptypropFirstname';
            contactRecord[0].LastName='TestopptypropLastname';
            contactRecord[0].AccountId=Acc[0].Id ;
            contactRecord[0].Salutation='MR.';
            contactRecord[0].Email='Testopptypropemail@email.com'; 
            contactRecord[0].Phone='1112223334';
            contactRecord[0].Preferred_Address__c='Mailing Address';
            contactRecord[0].MailingCity = 'Newcastle';
            contactRecord[0].MailingState='Northumberland';
            contactRecord[0].MailingCountry='United Kingdom';
            contactRecord[0].MailingStreet='1st Street' ;
            contactRecord[0].MailingPostalCode='NE28 7BJ';
            contactRecord[0].National_Identity_Number__c = '9412055708083' ;
            contactRecord[0].Passport_Number__c = 'ABCDE12345';
            contactRecord[0].Home_Phone__c = '9999';
            contactRecord[0].Birthdate__c = Date.newInstance(1900 , 10,10);
            contactRecord[0].Ethnic_Origin__c = 'Asian' ;
            contactRecord[0].Marital_Status__c = 'Single';
            contactRecord[0].First_Language__c = 'English'; 
            insert contactRecord;
            
        }
            
        List<Opportunity> op = TestDataFactory.createOpportunity(1,'Global Opportunity');
         Id pricebookId = Test.getStandardPricebookId();
        if( op != null && !op.isEmpty()){
            op[0].Name= 'OpQuote create Test1'; 
            op[0].AccountId = acc[0].id; 
            op[0].Primary_Contact__c = contactRecord[0].Id;
            op[0].Ownerid = u1[0].Id;
            op[0].StageName = 'Prequalification'; 
            op[0].CloseDate = System.Today();
            op[0].Type = 'New Business'; 
            op[0].Academic_Vetting_Status__c = 'Un-Vetted'; 
            op[0].Academic_Start_Date__c = System.Today();
            op[0].Business_unit__c = 'Higher Ed';
            op[0].pricebook2Id = pricebookId ;
            op[0].Channel__c = 'Direct';
            op[0].Business_Unit__c = 'Higher Ed' ;
            op[0].Line_Of_Business__c = 'Higher Ed';
            op[0].market__C = 'AU';
            op[0].Group__c='primary';
            //op[0].RecordType = 'Global Opportunity';
            
            insert op;
        }   
        Test.StopTest();            
    }
     
  static testMethod void myUnitTest(){ 
         
      Test.StartTest();
        
         User u1 = [Select Id From User where UserName ='CreateSampleOrderTest@lexpro.com'];
                
        list<opportunity> opplist=[select id,name from opportunity where name='OpQuote create Test1']; 
        
        System.runAs(u1)
        {   
            ApexPages.StandardController sc = new ApexPages.StandardController(opplist[0]);
            
            CreateGlobalOpptySampleOrderInitExt testAccPlan = new CreateGlobalOpptySampleOrderInitExt(sc);
            testAccPlan.Opptysampleordervalidation();  
            testAccPlan.closePopup();    
        }               
          
        PermissionSet ps = [SELECT ID From PermissionSet WHERE Name ='Pearson_Global_Opportunity_SOrder_Management'];
        insert new PermissionSetAssignment(AssigneeId = u1.id, PermissionSetId = ps.Id );
          
        System.runAs(u1)
        {   
            ApexPages.StandardController sc = new ApexPages.StandardController(opplist[0]);
            
            CreateGlobalOpptySampleOrderInitExt testAccPlan = new CreateGlobalOpptySampleOrderInitExt(sc);
            testAccPlan.Opptysampleordervalidation();  
            testAccPlan.closePopup();    
        }   
           
      Test.StopTest();
     }
 }