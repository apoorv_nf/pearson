@isTest
public class MassUpdatePublishedArticlesTest {
    @isTest
    private static void testMassUpdatePublishedArticles(){
        List<Customer_Support__kav> articles = new List<Customer_Support__kav>();
        Customer_Support__kav article = new Customer_Support__kav();
        article.Title = 'Test Title';
        article.UrlName = 'Test-Title-xyz';
        articles.add(article);
        
        Customer_Support__kav article2 = new Customer_Support__kav();
        article2.Title = 'Test Title2';
        article2.UrlName = 'Test-Title-xyz2';
        articles.add(article2);
        
        insert articles;
        
        List<Customer_Support__kav> updatedArticles = [select Id, RecordTypeId, KnowledgeArticleId from Customer_Support__kav where Id IN :articles];
        
        KbManagement.PublishingService.publishArticle(updatedArticles[0].KnowledgeArticleId, True);
        KbManagement.PublishingService.publishArticle(updatedArticles[1].KnowledgeArticleId, True);
        
        KbManagement.PublishingService.editOnlineArticle(updatedArticles[0].KnowledgeArticleId, false);
        
        Test.startTest();
        Database.executeBatch(new MassUpdatePublishedArticles(), 200);
        Test.stopTest();
    }
}