@isTest(seeAllData=true)
private class TestOrderAddress { 

	private static testMethod void OrderAddressTest() {
		
		List<Contact> scontact         = new List<contact>();
		scontact = TestDataFactory.createContact(1);
		if(scontact != null && !scontact.isEmpty()){
		    scontact[0].MailingStreet = 'Test Mailing street1; \r\n Test Mailing street2 \r\n Test Mailing street3; \r\n Test Mailing street4';
		    scontact[0].MailingCity = 'Test MailingCity';
		    scontact[0].MailingState = 'Alabama';
		    //scontact[0].MailingStateCode = '12345';
		    scontact[0].MailingCountry = 'United States'; 
		    //scontact[0].MailingCountryCode = '12345';
		    scontact[0].MailingPostalCode = '12345';
		    
		    scontact[0].OtherStreet = 'Test OtherStreet1; \r\n Test OtherStreet2 \r\n Test OtherStreet3; \r\n Test OtherStreet4';
		    scontact[0].OtherCity = 'Test otherCity';
		    scontact[0].OtherState = 'Alabama';
		    //scontact[0].OtherStateCode = 
		    scontact[0].OtherCountry = 'United States';
		    //scontact[0].OtherCountryCode
		    scontact[0].OtherPostalCode =  '12345';
		    insert scontact;
		}

			Order sOrder1			    	 = new Order();
			system.debug('AccountId :'+scontact[0].AccountId);
			sOrder1.AccountId				 = scontact[0].AccountId;
			sOrder1.Contact__c               = scontact[0].Id;
			sOrder1.Order_Address_Type__c    = 'Contact Mailing';
			sOrder1.Status   				 = 'New';
			sOrder1.ShippingStreet           ='shipstreet';
      sOrder1.ShippingCity             ='shipcity';
      sOrder1.ShippingState            ='Andhra Pradesh';
      sOrder1.ShippingPostalCode       ='shipstreet';
      sOrder1.ShippingCountry          ='India';
			sOrder1.ShipToContactId          =  scontact[0].Id;
			sOrder1.EffectiveDate            = system.today();
			sOrder1.Market__c                = 'AU'; 
			sOrder1.Line_of_Business__c      = 'Schools'; 
			sOrder1.Business_Unit__c         = 'DTC';
			
			Order sOrder2			    	 = new Order();
			system.debug('AccountId :'+scontact[0].AccountId);
			sOrder2.AccountId				 = scontact[0].AccountId;
			sOrder2.Contact__c               = scontact[0].Id;
			sOrder2.Order_Address_Type__c    = 'Contact Other';
			sOrder2.Status   				 = 'New';
			sOrder2.ShippingStreet           ='shipstreet';
      sOrder2.ShippingCity             ='shipcity';
      sOrder2.ShippingState            ='Andhra Pradesh';
      sOrder2.ShippingPostalCode       ='shipstreet';
      sOrder2.ShippingCountry          ='India';
			sOrder2.ShipToContactId          =  scontact[0].Id;
			sOrder2.EffectiveDate            = system.today();
      sOrder2.Market__c                = 'AU'; 
      sOrder2.Line_of_Business__c      = 'Schools'; 
      sOrder2.Business_Unit__c         = 'DTC';

			
			Order sOrder3			    	 = new Order();
			system.debug('AccountId :'+scontact[0].AccountId);
			sOrder3.AccountId				 = scontact[0].AccountId;
			sOrder3.Contact__c               = scontact[0].Id;
			sOrder3.Order_Address_Type__c    = 'Account';
			sOrder3.Status   				 = 'New';
      sOrder3.ShippingStreet           ='shipstreet';
      sOrder3.ShippingCity             ='shipcity';
      sOrder3.ShippingState            ='Andhra Pradesh';
      sOrder3.ShippingPostalCode       ='shipstreet';
      sOrder3.ShippingCountry          ='India';			
			sOrder3.EffectiveDate            = system.today();
      sOrder3.Market__c                = 'AU'; 
      sOrder3.Line_of_Business__c      = 'Schools'; 
      sOrder3.Business_Unit__c         = 'DTC';

			
			List<Order> LOrders = new List<order>();
			
			LOrders.add(sOrder1);
			LOrders.add(sOrder2);
			LOrders.add(sOrder3);
			insert LOrders;
		Test.startTest();
		OrderAddress OA = new OrderAddress();
		OA.OrderAddresshandler(LOrders);
		Test.stopTest();

	}

}