/**************************************************************************
 * Class Name   : UKTEPPricingCustomUtility
 * Author       : Aman Sawhney
 * Date Created : 16 / Nov / 2018
 * Description  : A utility class for utility methods
 * ************************************************************************/
public class UKTEPPricingCustomUtility{
    
    //this method returns the double equivalent of string number
    //and handles the exception
    public static Double getDouble(String numberString){
        
        Double returnable = null;
        
        if(numberString==null){
            returnable = 0;  
        }
        else{
            
            try{
                returnable = Double.valueOf(numberString);
            }
            catch(Exception e){
                returnable = 0;
            }
            
        }
        
        return returnable;
    }
    
    //this method returns the Integer equivalent of string number
    //and handles the exception
    public static Integer getInteger(String numberString){
        
        Integer returnable = null;
        
        if(numberString==null){
            returnable = 0;  
        }
        else{
            
            try{
                returnable = Integer.valueOf(numberString);
            }
            catch(Exception e){
                returnable = 0;
            }
            
        }
        
        return returnable;
    }

}