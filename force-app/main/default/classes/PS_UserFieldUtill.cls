/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_UserFieldUtill .cls 
   Description:     On Before insert of multiple objects
   More Info:-      This Class should be called from befoere inset of required objects trigger events , if logged in user value is required to set call
                    setUserFieldValues(List<sObject> sObjectLst) this method , if specific user field value needs to be set then call setUserFieldValues(List<sObject> sObjectLst , String userFld).
                    Eg
                    ------
                    Trigger 
                    PS_UserFieldUtill.setUserFieldValues(Trigger.New);//or pass required list from handler class if logged in user value is required
                    PS_UserFieldUtill.setUserFieldValues(Trigger.New , 'Ownerid');//or pass required list from handler class if Ownerid value is required , same goes for other fields also
                    PS_UserFieldUtill.setUserFieldValues(Trigger.New , 'SalesUser__c');//or pass required list from handler class if Ownerid value is required , same goes for other fields also
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
   06/12/2015         1.0           Leonard Victor                      Initial Release
   22/04/2016         1.1           Kyama Rajeshwari                    Added Group to string FIELDS_TO_TAG to map user group to oppty on insert.
   2/8/2016           1.2           Payal Popat                         Updated setUserFieldValues(List<object) to set values if null
   7/2/2019                         Neuraflash (Matt Mitchener)         Updated API version to 43
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class PS_UserFieldUtill {
    //Feilds user for processing , add additional value to this set. Make sure objects field and user field have same API
     @TestVisible private static final Set<String> FIELDS_TO_TAG = new Set<String>{'Market__c', 'Line_of_Business__c', 'Business_Unit__c', 'Geography__c','Group__c'};
     @TestVisible private static Map<Id,User> mapUser =new Map<Id,User>();
     
    
    /*----------------------------------------------------------------------------------------------------------------------------
    This Method is called when Sepcific user field value is required , checks for 005 becasue some records can have queue as owner
    -------------------------------------------------------------------------------------------------------------------------------*/
    public static void setUserFieldValues(List<sObject> sObjectLst , String userFld){
        //Fetching Record Owner
        Set<Id> userSet = new Set<Id>();
        for(sObject dynamicObj : sObjectLst){
            if((String.valueof(dynamicObj.get(userFld)).substring(0,3))=='005' && !mapUser.containsKey((Id)dynamicObj.get(userFld))){
                userSet.add((Id)dynamicObj.get(userFld));
            }
        }
        if(!userSet.isEmpty()){
            fetchUser(userSet);
        }
        for(sObject dynamicObj : sObjectLst){
            for(String fldsToMap : FIELDS_TO_TAG){
              if(hasSObjectField(fldsToMap.toLowerCase(), dynamicObj)){
                    dynamicObj.put(fldsToMap , mapUser.get((Id)dynamicObj.get(userFld)).get(fldsToMap));
                }    
            }
    
        }

    }
    /*----------------------------------------------------------------------------------------------------------------------------
    This Method is called when logged in user fields value are required
    -------------------------------------------------------------------------------------------------------------------------------*/
    public static void setUserFieldValues(List<sObject> sObjectLst){
        //Fetching Record Owner
        Id loggedUserid = Userinfo.getUserid();
        Set<Id> loggerUserSet = new Set<Id>();
        if(!mapUser.containsKey(loggedUserid)){
            loggerUserSet.add(loggedUserid);
        }
        if(!loggerUserSet.isEmpty()){
            fetchUser(loggerUserSet);
        }
        for(sObject dynamicObj : sObjectLst){
            for(String fldsToMap : FIELDS_TO_TAG){ 
                if(hasSObjectField(fldsToMap.toLowerCase(), dynamicObj)){ 
                    String sfldvalue = (String) dynamicObj.get(fldsToMap); 
                    if(String.IsBLANK(sfldvalue))
                    {
                      dynamicObj.put(fldsToMap , mapUser.get(loggedUserid).get(fldsToMap));
                    }
                }
            }
        }
    }
    /*----------------------------------------------------------------------------------------------------------------------------
    This Method is called from any of the setUserFieldValues() methods , this used to fetch all the key fields value, also it doesn't 
    add any duplicate user to map or to the set used for SOQL.
    -------------------------------------------------------------------------------------------------------------------------------*/
    public static void fetchUser(Set<Id> userToFecth){
        List<String> fieldList = new List<String>();
        fieldList.addAll(FIELDS_TO_TAG);
        
        List<User> userList = (Database.query('SELECT ' + String.join(fieldList, ',') + ' FROM User WHERE Id IN:userToFecth'));
        
        for(User userObj : userList){
            if(!mapUser.containsKey(userObj.id)){
                mapUser.put(userObj.id,userObj);
            }
            
            
        }
    }
    
    /*----------------------------------------------------------------------------------------------------------------------------
    This Method checks a field exists before you get/put it
    -------------------------------------------------------------------------------------------------------------------------------*/
    public static boolean hasSObjectField(String fieldName, SObject so)
    {     
      return so.getSobjectType().getDescribe().fields.getMap().keySet().contains(fieldName);
    }
    
}