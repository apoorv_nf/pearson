/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_FeedItem_TriggerHandler.cls
   Description:     On insert of feeditem record 
   Date             Version              Author                          Summary of Changes
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  11-Jan-2016         0.1              Sudhakar Navuluri               Initial Release 
  07/Mar/2016         0.2              Sudhakar Navuluri       Changed the code as per review comments 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class PS_FeedItem_TriggerHandler
{ 
public static void afterInsert(List<FeedItem> feedList)
    {
        PS_FeedItem_TriggerHelper.onafterinsert(feedList);
    }
    
}