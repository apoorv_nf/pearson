/**
 * Name : PS_SendEmailAlertsForOrderTest 
 * Author : Accenture_Karan
 * Description : Test class used for testing PS_SendEmailAlertsForOrder and GlobalRevenueUpdateOppty
 * Date : 01/12/15 
 * Version : <intial Draft>  
 * Replaced Qualification__c field by QuoteMigration_Qualification__c as part of ZA Apttus Decomm by Mani - 07/25/2018
 */

@isTest(seeAllData=true)
public class PS_SendEmailAlertsForOrderTest
{
    static testMethod void testR6ordernotification(){
        // Test data preparation                            
        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,3); 
        usList[0].Business_Unit__c = 'Higher Ed';
        usList[0].Market__c = 'UK';
        usList[1].Business_Unit__c = 'Higher Ed';
        usList[1].Market__c = 'AU';
        usList[2].Business_Unit__c = 'PCTA-affiliated';
        usList[2].Market__c = 'NZ';
        insert usList;
        
        Bypass_Settings__c settings = new Bypass_Settings__c();
        settings.Disable_Validation_Rules__c = true;
        settings.Disable_Triggers__c = false;
        settings.Disable_Process_Builder__c = true;
        settings.SetupOwnerId = usList[0].id;
        insert settings;         
             Account  acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', Line_of_Business__c= 'Schools', Geography__c= 'Core', ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
            acc.Pearson_Campus__c=true;
            insert acc;
            Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true); 
            insert customPB;
            
            Opportunity opp = new opportunity ( Name = 'OpporName1',AccountId = acc.Id,CloseDate = System.TODAY() + 30,StageName = 'OpporStageName',Channel__c='Direct', QuoteMigration_Qualification__c = 'Graphic Design', pricebook2Id=customPB.Id);
            insert opp;
            system.runAs(usList[0]){
            
            test.startTest();
            Contact con = new Contact(FirstName='TestContactFirstname1', LastName='TestContactLastname',AccountId=opp.accountid ,Salutation='MR.', Email='happy' + '@email.com',MobilePhone='111222333' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
            insert con;
            OpportunityContactRole oc = new OpportunityContactRole( opportunityId =  opp.Id,contactId = con.id ,Role =  'Business User');
            insert oc;
            //String userId = UserInfo.getUserId();
            String userId= usList[0].id;
            list<User> currentUser = [SELECT Id, Market__c, Line_of_Business__c, Product_Business_Unit__c FROM User WHERE Id =: userId];
           product2  prod = new product2(name='test', ISBN__c = '9781292099545', Market__c = currentUser[0].Market__c, Line_of_Business__c = currentUser[0].Line_of_Business__c, Business_Unit__c = currentUser[0].Product_Business_Unit__c );
           
            insert prod;
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry standardPrice = new PricebookEntry( Pricebook2Id = pricebookId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
            insert standardPrice;

            PricebookEntry pbe = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id, UnitPrice = 0, currencyIsocode = userinfo.getDefaultCurrency());
            insert pbe;  
            List<Order> ordlist = new List<Order>();
            Id sRtId = PS_Util.fetchRecordTypeByName(Order.SobjectType,PS_Constants.ORDER_SAMPLE_ORDER);
            order sampleorder1 = new order(
                Accountid=opp.accountid,
                RecordTypeId =sRtId ,
                ShipToContactid=con.id,Opportunityid=opp.id,
                EffectiveDate=system.today(),Status='New',
                Pricebook2Id=customPB.id,CurrencyIsoCode = userinfo.getDefaultCurrency(),
                type=System.Label.Order_Type, ShippingCountry = 'India', ShippingCity = 'Bangalore', 
                ShippingStreet = 'BNG1', ShippingPostalCode = '5600371', ShippingState = 'Karnataka', 
                Salesperson__c = usList[0].id,
                Business_Unit__c='Higher Ed',Market__c = 'UK',group__c = null
                ); 
            order sampleorder2 = new order(
                Accountid=opp.accountid,
                RecordTypeId =sRtId ,
                ShipToContactid=con.id,Opportunityid=opp.id,
                EffectiveDate=system.today(),Status='New',
                Pricebook2Id=customPB.id,CurrencyIsoCode = userinfo.getDefaultCurrency(),
                type=System.Label.Order_Type, ShippingCountry = 'India', ShippingCity = 'Bangalore', 
                ShippingStreet = 'BNG2', ShippingPostalCode = '5600372', ShippingState = 'Karnataka', 
                Salesperson__c = usList[1].id,
                Business_Unit__c='Higher Ed',Market__c = 'AU',group__c = null
                );  
            order sampleorder3 = new order(
                Accountid=opp.accountid,
                RecordTypeId =sRtId ,
                ShipToContactid=con.id,Opportunityid=opp.id,
                EffectiveDate=system.today(),Status='New',
                Pricebook2Id=customPB.id,CurrencyIsoCode = userinfo.getDefaultCurrency(),
                type=System.Label.Order_Type, ShippingCountry = 'India', ShippingCity = 'Bangalore', 
                ShippingStreet = 'BNG3', ShippingPostalCode = '5600373', ShippingState = 'Karnataka', 
                Salesperson__c = usList[2].id,
                Business_Unit__c='PCTA-affiliated',Market__c = 'NZ',group__c = null
                );          
        ordlist.add(sampleorder1);
        ordlist.add(sampleorder2);
        ordlist.add(sampleorder3);
        insert ordlist;
        
            List<Order> ordup = new List<Order>();
            for(Order ord: [select id,status from order where id=:ordlist]){
                ord.status='Open';
                ordup.add(ord);
            }
            System.debug('GK before update');
            update ordup;
            System.debug('GK After update');
        test.stopTest();
        }
       }
        
    
    /*static testMethod void testOpenOrderNotification3(){
    
        // Test data preparation                            
        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        insert usList;
        test.startTest();
            Order ord = TestDataFactory.returnorder();
            ord.Salesperson__c = usList[0].Id; 
            ord.Status = 'Open';            
            update ord;             
        test.stopTest();            
    }*/
}