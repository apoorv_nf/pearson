@isTest
private class OpportunityTriggerTest {
    
    @isTest static void TestDeleteOpportunity() {
        
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        // 
        User usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', 
                            CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', 
                            LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
                            ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Price_List__c= 'Math & Science');

        usr.market__c='UK';
        usr.Business_Unit__c='Schools';
        usr.Line_of_Business__c='Schools';
        usr.Group__c='Primary';
        usr.license_Pools__c='US HE Sales (SP)';
        //usr.License_Pools__c='Test User';
        insert usr;
       
        System.assert(usr.id != null, 'User creation failed');        


        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp;
        
        
        RecordType accrt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account' AND Name ='School'];    
        Account acct = new Account(Name='Test B2B R6 School',Line_of_Business__c='Schools',Business_Unit__c='Schools',Group__c='Primary',Organisation_Type__c ='School',Type__c='Secondary',Sub_Type__c='Academy Second',
                        Phone='+91000001', ShippingCountry = 'United kingdom',ShippingState = 'greater london', ShippingCity = 'london', ShippingStreet = 'BNG', ShippingPostalCode = 'SE27 0AA',billingstreet='BNG', billingcity='London', billingstate='Greater london',billingcountry='United Kingdom',billingpostalcode='SE27 0AA');  

        acct.recordtypeid=accrt.Id;
        acct.ShippingCountry='United kingdom';
        acct.CurrencyIsoCode='USD';
        
        insert acct;
        byp.Disable_Triggers__c=false;
        Update byp;
        
        system.runas(usr){
        RecordType opprt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name ='Global Opportunity']; 
        Opportunity opp = new Opportunity(Name=acct.Name + ' Opportunity',
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=acct.Id,
                                       CurrencyIsoCode='USD',
                                       RecordTypeId=opprt.Id);
            
        insert opp;
        //Insert 2 opprtunitylineitems to delete one first, and then the opportunity
               
        List<Product2> productList = new List<Product2>();
        List<PriceBookEntry> pbe = new List<PriceBookEntry>();
        List<OpportunityLineItem> oli = new List<OpportunityLineItem>();   
            
                  
        //Creating Test Products
        Product2 prod = new Product2(Name = 'ENTERPRISE DESKTOP',IsActive = true,CurrencyIsoCode = 'USD');        
        productList.add(prod);            
        productList.add(new Product2(name='Test2', isActive=true, CurrencyIsoCode='USD'));
        productList.add(new Product2(name='Test3', isActive=true, CurrencyIsoCode='USD'));
       //Checking for List size
       if(productList.size() > 0)
       {
        insert productList;     
       }
       
            
       //Creating test Price Book
      // PriceBook2 pb = [select id from Pricebook2 where IsStandard = true limit 1];  
       
       //Creating Price Book entries
       for(Product2 prd : productList)
       {
        pbe.add(new PriceBookEntry(PriceBook2Id= Test.getStandardPricebookId(), product2Id= prd.Id, CurrencyISOCode = 'USD' ,UnitPrice=0, isActive=true,useStandardPrice=false));       
       }
       //Checking for List size
       if(pbe.size() > 0){
        insert pbe;        
       }       
       
       for(PriceBookEntry entries : pbe)
       {
        oli.add(new OpportunityLineItem(OpportunityId= opp.id,PriceBookEntryId= entries.Id, UnitPrice=5, Quantity=1));
        system.debug('AFTER FOR' +OpportunityLineItem.PriceBookEntryId);
        
       }   
     
       //Checking for List size
       if(oli.size() > 0)
       {
        insert oli; 
           
        update oli;
        
       }     
       
       // Perform test
        Test.startTest();
        Database.delete(oli.get(0).id);
        //System.assertEquals(2, oli.size());
            
        Database.DeleteResult result = Database.delete(opp, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have published a event,
        // so verify 
        System.assert(result.isSuccess());
        System.assert(result.getErrors().size()<=0);
        //System.assertEquals(EventPublished);
    	}
    
	}
}