/* -------------------------------------------------------------------------------------------------------------------
   Name:            PS_LeadConvertUtility.cls
   Description:     Handles the repair of object following a lead conversion
   Date             Version         Author               Summary of Changes 
   -----------      ----------      -----------------    -------------------------------------------------------------
   01/12/2015       1.0             Ron Matthews         Initial Release 
------------------------------------------------------------------------------------------------------------------- */
public without sharing class PS_LeadConvertUtility 
{  
  
  // holds the new account id created for a lead
  private Map<Id, Id> newAccounts = new Map<id, Id>();
  
  // holds a list of accounts requiring an account team share created
  private List<Id> accountsforSharingUpdate = new List<Id>();
  
  // holds a list of userids requiring an account team share created
  private List<Id> userIds = new List<Id>();
  
  // Map user and record id
  private static Map<Id,Id> mapUsrObj = new Map<Id,Id>();
  
  // methods for Account manipulation 
  
  /*
   * this method orchestrates the correction of any account records
   */
  public void rollbackIncorrectAccountConversion(Map<Id, Account> newAccounts, Map<Id, Account> oldAccounts)
  {
    Map <Id, Account> convertedAccounts = checkIfBeingConverted(newAccounts);
    if(convertedAccounts.size() > 0)
    {
      Map <Id, Account> incorrectAccounts = checkForIncorrectConversion(convertedAccounts);
      if(incorrectAccounts.size() > 0)
      {
        rollBackValues(incorrectAccounts, oldAccounts);
      }
    }    
  }
  
  /*
   * This method checks if the isBeingConverted__c field is true and 
   * if so add the account to a map of accounts to be processed
   */   
  private Map<Id, Account> checkIfBeingConverted(Map<Id, Account> accounts)
  {
    Map<Id, Account> convertedAccounts = new Map<Id, Account>();
     
    // loop through each account and if the isBeingConvertedTo__c field is true add it to the map
    for(Account acc : accounts.values())
    {      
      if(acc.isBeingConvertedToText__c == 'True')
      {
        acc.isBeingConvertedToText__c = '';
        convertedAccounts.put(acc.Id, acc);
      }
    } 
    
    return convertedAccounts;
  }
  
  /*
   * This method checks to see if the account from the conversion is oncorrect and 
   * add the incorrect accounts to a map
   */
  private Map<Id, Account> checkForIncorrectConversion(Map<Id, Account> accounts)
  {
    // the map of accounts that are incorrect and requirfe updating
    Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
    
    // get the B2B record type
    Id B2BLeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(System.Label.PS_B2BLeadRecordType).getRecordTypeId();
    
    // the map of leads that relate to the accounts to bve processed
    Map <Id, Lead> leads = getRelatedLeads(accounts);
    
    for(Account acc : accounts.values())
    {
      // get the lead for the account
      Lead l = leads.get(acc.Converted_Lead_Id__c);
      
      // if the account id does not match the account specified on the lead we need to correct it
      if(l != null && l.recordtypeId == B2BLeadRecordTypeId)
      {
        if(l.Institution_Organisation__c != null && l.Institution_Organisation__c != acc.Id)
        {
          accountsToUpdate.put(acc.Id, acc);  
        }
        // we could be creating a new company so check that it matches
        else if(l.Company != null && l.Company != acc.name)
        {       
          accountsToUpdate.put(acc.Id, acc);  
        }
      }
    }
    return accountsToUpdate;
  }
  
  /*
   * This method restores the previous values as the account was incorrectly used
   * by the conversion  
   */
  private void rollBackValues(Map<Id, Account> newAccounts, Map<Id, Account> oldAccounts)
  {
    Map <String, String> fields = getFieldMappings();

    for(Account acc : newAccounts.values())
    {
      for(String fieldName : fields.values())
      {
        if(!String.isBlank(fieldName))
        {
          Account oldAccount = oldAccounts.get(acc.Id);
          acc.put(fieldName, oldAccount.get(fieldName));
        }
      }  
    }    
  }
  
  /*
   * Gets the lead object that relates to the account from the conversion process
   */
  private Map<Id, Lead> getRelatedLeads(Map<Id, Account> accounts)
  {
    Set <String> accountsForLead = new Set<String>();

    // build a set of leads to retrieve
    for(Account acc : accounts.values())
    {    
      accountsForLead.add(acc.Converted_Lead_Id__c);      
    }
    
    // build a map of the leads for the accounts we are processing
    Map<Id, Lead> leads = new Map<Id, Lead>([SELECT Id, Institution_Organisation__c, Company, recordtypeId FROM Lead where Id IN :accountsForLead]);
    return leads;
  }
  
  // methods for lead manipulation

  /*
   * This method coordinates fixing the remaining objects
   */
  public void fixIncorrectLeadConversion(Map<Id, Lead> leads, Map<Id, Lead> oldleads)
  {
    Map<Id, Lead> incorrectLeads = filterIncorrectlyConvertedLeads(leads);
    if(incorrectLeads.size() > 0)
    {
      reparentIncorrectlyParentedOpportunity(incorrectLeads);
      createMissingAccountContact(incorrectLeads, oldleads);
      mapLeadValuesToCorrectAccount(incorrectLeads);
    }
  }
  
  /*
   * The method returns the map of leads that need fixing
   */
  private Map<Id, Lead> filterIncorrectlyConvertedLeads(Map<Id, Lead> leads)
  {
    // the map of accounts that are incorrect and requirfe updating
    Map<Id, Lead> leadsToUpdate = new Map<Id, Lead>();
    Set <Id> accountsForCompany = new Set<Id>();
    List <Account> createdAccounts = new List<Account>();
    Id B2BLeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(System.Label.PS_B2BLeadRecordType).getRecordTypeId();    
    //Added by priya for lead conversion issue
    boolean isB2BLead  = false;
     
    Map <Id, Account> convertedAccounts = new Map<Id,Account>(); 
   //end by priya for lead conversion issue  
    for(Lead l : leads.values())
    {      
      if(l.company != null)
      {
        accountsForCompany.add(l.ConvertedAccountId);
      }
      if(l.recordtypeId == B2BLeadRecordTypeId )
      {
          isB2BLead = true;
      }  
    }
    
 	  if( isB2BLead == true ) //Added by priya for lead conversion issue
     convertedAccounts = new Map<Id, Account>([SELECT Id, Name FROM Account where Id IN :accountsForCompany]);
         
    for(Lead l : leads.values())
    {      
      // if the account id does not mat the account specified on the lead we need to correct it
      // if the ConvertedAccountId then we are not in a conversion situation
      if(l.ConvertedAccountId != null && l.recordtypeId == B2BLeadRecordTypeId)
      {
        if(l.Institution_Organisation__c != null && l.ConvertedAccountId != l.Institution_Organisation__c)
        {
          leadsToUpdate.put(l.Id, l);  
        }
        // we could be creating a new company so check that it matches
        else if(l.Company != null)
        {          
          Account convertedAccount = convertedAccounts.get(l.ConvertedAccountId);
          if(convertedAccount != null && convertedAccount.Name != l.Company)
          { 
            Account newAccount = createNewAccount(l); 
            createdAccounts.add(newAccount);  
            //l.Institution_Organisation__c = newAccount.Id;    
            leadsToUpdate.put(l.Id, l);   
          }
        }
      }
    }
    
    if(createdAccounts != null && createdAccounts.size() > 0)
    {
      insert createdAccounts; 
      
      List<AccountTeamMember> accountTeamMembers = new List<AccountTeamMember>();
      for(Account acc : createdAccounts)
      {
        Lead l = leadsToUpdate.get(acc.Converted_Lead_Id__c);
        AccountTeamMember atm = addTeamMember(l, acc.Id);
       accountTeamMembers.add(atm);
      } 
      
      if(accountTeamMembers != null && accountTeamMembers.size() > 0)
      {
        insert accountTeamMembers;
        updateAccountTeamShare();
      }  
      if(!mapUsrObj.isEmpty()){
          //chatter post the team member their account has been created
          PS_ChatterPostUtilities.createPostOnAccount(mapUsrObj);
      }
    }
    
    // so we can find the account that was created for the lead later on as we are in the after trigger and cannot update the lead object
    for(Account acc : createdAccounts)
    {
      newAccounts.put(acc.Converted_Lead_Id__c, acc.Id); 
    }
    
    return leadsToUpdate;
  }
  
  /*
   * This method corrects the opportunity object
   */
  private void reparentIncorrectlyParentedOpportunity(Map<Id, Lead> leads)
  {
    Map <Id, Opportunity> opportunities = getRelatedOpportunities(leads);
    List <Opportunity> opportunitiesToUpdate = new List<Opportunity>();
    
    for(Lead l : leads.values())
    {
      Opportunity o = opportunities.get(l.ConvertedOpportunityId);
      if(o != null)
      {
        Id accId = null;
        if(l.Institution_Organisation__c != null)
        {
          accId = Id.valueOf(l.Institution_Organisation__c);
        }
        else
        {
          accId = newAccounts.get(l.id);
        }
        
        if(o.AccountId != accId)
        {
          o.AccountId = accId; 
          opportunitiesToUpdate.add(o);         
        }
      }    
    }
    
    if(opportunitiesToUpdate != null && opportunitiesToUpdate.size() > 0)
    {
      update opportunitiesToUpdate;
    }    
  }
  
  /*
   * Creates a new account
   */
  @TestVisible private Account createNewAccount(Lead l)
  {
    Account acc = new Account();
    acc.Name = l.Company;
    acc.Converted_Lead_Id__c = l.id;
    acc.shippingStreet = l.Street;
    acc.ShippingCity = l.city;
    acc.ShippingCountry = l.Country;    
    acc.shippingPostalCode = l.postalCode;
    acc.IsCreatedFromLead__c = true;
    acc.isBeingConvertedToText__c = 'True';
    acc.isBeingConvertedTo__c = true;
    return acc;
  }
  
  /*
   * This method creates the missing account contacts
   */
  private void createMissingAccountContact(Map<Id, Lead> leads, Map<Id, Lead> oldleads)
  {
    Map<String, AccountContact__c> existingAccountContacts = getRelatedAccountContacts(leads);

    List<AccountContact__c> accountContactsToInsert = new List<AccountContact__c>(); 
    
    for(Lead l : leads.values())
    {
      // build the key to check it the AccountContact__c object already exists
      Id accId = null;
      if(l.Institution_Organisation__c != null)
      {
        accId = Id.valueOf(l.Institution_Organisation__c);
      }
      else
      {
        accId = newAccounts.get(l.id);
      }
      String key = String.valueOf(accId) + String.valueOf(l.ConvertedContactId);
      // if the AccountContact__c object already exists for this account and contact combination dont create a new one

      if(!existingAccountContacts.containsKey(key))
      {
        // add the missing AccountContact
        ContactUtils createAccCon = new ContactUtils();
        AccountContact__c ac = new AccountContact__c();
        ac.Contact__c = l.ConvertedContactId;
        ac.Account__c = accId; 
        ac.AccountRole__c = l.Role__c;
        ac.Role_Detail__c = l.Role_Detail__c;
        ac.Other_Role_Detail__c = l.Other_Role_Detail__c;
        ac.Additional_Responsibilities__c = l.Additional_Responsibilities__c;
        ac.Discipline_Of_Interest_Multi_Select__c = l.Discipline_Of_Interest_Multi_Select__c;
        ac.Sub_Discipline_of_Interest__c = l.Sub_Discipline_of_Interest__c;
        ac.Decision_Making_Level__c = l.Decision_Making_Level__c;
        ac.Sync_In_Progress__c = TRUE;
        ac.Financially_Responsible__c = (oldleads.get(l.id).Sponsor_Type__c == 'Self') ? TRUE : FALSE;
        ac.Primary__c = true;   

        accountContactsToInsert.add(ac);                 
      }  
    }
    
    if(accountContactsToInsert != null && accountContactsToInsert.size() > 0)
    {     
      insert accountContactsToInsert;      
    }
  }
  
  /*
   * This method corrects the account object
   */
  private void mapLeadValuesToCorrectAccount(Map<Id, Lead> leads)
  {    
    Map<String, String> mappingFields = getFieldMappings();
    Map<Id, Account> relatedAccounts = getRelatedAccounts(leads, mappingFields);
    Map<String, String> reverseMappingFields = new Map<String, String>();
    List<Account> accountsToUpdate = new List<Account>();
    
    // create a reverse map
    for(String s : mappingFields.keyset())
    {
      String value = mappingFields.get(s);
      reverseMappingFields.put(value, s);
    }
    
    Boolean changedAcc = false;
    for(Lead l : leads.values())
    {
      Id key = l.Institution_Organisation__c != null ? l.Institution_Organisation__c : newAccounts.get(l.id);
      Account acc = relatedAccounts.get(key);

      for(String fieldName : mappingFields.values())
      {        
        changedAcc = false;
        if(acc.get(fieldName) == null)
        {
          changedAcc = true;
          acc.put(fieldName, l.get(reverseMappingFields.get(fieldName)));          
        }
      }
      if(changedAcc)
      {
        accountsToUpdate.add(acc);
      }
    }    
    
    if(accountsToUpdate != null && accountsToUpdate.size() > 0)
    {
      update accountsToUpdate;
    }
  }
  
  /*
   * This method returns the list pof opportinities for the leads
   */
  private Map<Id, Opportunity> getRelatedOpportunities(Map<Id, Lead> leads)
  {
    Set<Id> convertedOpportunities = new Set<Id>();
    for(Lead l : leads.values())
    {
      convertedOpportunities.add(l.ConvertedOpportunityId);  
    }
    Map<Id, Opportunity> opportunities = new Map<Id, Opportunity>([SELECT Id, AccountId FROM Opportunity where Id IN :convertedOpportunities]);
    return opportunities;  
  } 
  
  /*
   * This method returns the map of contacts for the leads
   */
  private Map<String, AccountContact__c> getRelatedAccountContacts(Map<Id, Lead> leads)
  {
     // list of accounts from the lead to be used in duplicate AccountContact__C    
    List<Id> accountListForDuplicateChecking = new List<Id>();
    // list of contacts from the lead to be used in duplicate AccountContact__C
    List<Id> contactListForDuplicateChecking = new List<id>();        
    
    for(Lead l : leads.values())
    {
      accountListForDuplicateChecking.add(l.ConvertedAccountId);
      contactListForDuplicateChecking.add(l.ConvertedContactId);
    }
    
    // Addd the AccountContact__c objects to a map to be used when checking for duplicates
    // The key is a contcatination of the accountId and contactId
    Map <String, AccountContact__c> existingAccounContacts = new Map <String, AccountContact__c>(); 
    for(AccountContact__c accCon : [SELECT Id, Account__c, Contact__c from AccountContact__c WHERE Account__c IN :accountListForDuplicateChecking and Contact__c IN :contactListForDuplicateChecking])
    {
      String key = String.valueOf(accCon.Account__c) + String.valueOf(accCon.Contact__c);
      existingAccounContacts.put(key, accCon);  
    }      
    
    return existingAccounContacts;  
  }
  
  /*
   * This smethod returns a map of the accounts for the leads
   */
  private Map<Id, Account> getRelatedAccounts(Map<Id, Lead> leads,  Map<String, String> fieldMappings)
  {
    Set<Id> convertedAccounts = new Set<Id>();
    for(Lead l : leads.values())
    {
      if(l.Institution_Organisation__c != null)
      {     
        convertedAccounts.add(l.Institution_Organisation__c);
      }
      else
      {
        // it could be a new account
        convertedAccounts.add(newAccounts.get(l.id));
      }  
    }
    
    // Changed for D-3745
    //String accountFieldsList = Utils_allfields.getCreatableFieldsList('Account');
    String accountFieldsList = String.join(fieldMappings.values(), ',') ;
    
    String soql = 'SELECT '+ accountFieldsList  + ' FROM ACCOUNT WHERE Id IN :convertedAccounts';    
    List<Account> accounts = Database.query(soql);
    
    Map<Id, Account> accountsToReturn = new Map<Id, Account>();
    for(Account a : accounts)
    {
      accountsToReturn.put(a.id, a);  
    }
      
    return accountsToReturn;  
  }
  
  /*
   * This method returns the filed mappings from custom metadata that mirror the lead field mappings 
   * used in the lead conversion process
   */
  private Map<String, String> getFieldMappings()
  {
    // create the map for lead field and account field    
    Map<String, String> fieldNames = new Map<String, String>();
    
    // select from the custom metadata the field mappings that relate to the prefix parameter
    for(Custom_Lead_Field_Mapping__mdt lfm : [SELECT Lead_field__c, Account_Field__c FROM Custom_Lead_Field_Mapping__mdt])
    {      
      fieldNames.put(lfm.Lead_field__c, lfm.Account_Field__c);
    }
    
    return fieldNames;
  }
  
  @TestVisible private AccountTeamMember addTeamMember(Lead convertedLead, Id accId)
  {   
    AccountTeamMember accTeamMember = new AccountTeamMember();
    accTeamMember.AccountId = accId;
    accTeamMember.TeamMemberRole = 'Team Member';
    accTeamMember.userid = convertedLead.ownerid;

    accountsforSharingUpdate.add(accId);
    userIds.add(accTeamMember.userid);
    mapUsrObj.put(accId,convertedLead.ownerid);  
    return accTeamMember;      
  }
    
  public void updateAccountTeamShare()
  {
    List<AccountShare> accountSharesToUpdate = new List<AccountShare>();
    //Retrieving and updating Access Level Permissions
    
    List<AccountShare> addAccountShare = [select Accountid, userorgroupid,AccountAccessLevel, OpportunityAccessLevel, CaseAccessLevel,ContactAccessLevel from AccountShare where Accountid IN : accountsforSharingUpdate and UserOrGroupId IN : UserIds];
    for(AccountShare aas : addAccountShare)
    {      
      if(aas.AccountAccessLevel != null && !aas.AccountAccessLevel.equals('All'))
      {
        aas.AccountAccessLevel = 'Edit';
      }
      
      aas.OpportunityAccessLevel = 'Edit';
      aas.CaseAccessLevel = 'Edit';
      aas.ContactAccessLevel = 'Edit';               
      accountSharesToUpdate.add(aas);          
    }
    update accountSharesToUpdate;
  }
}