/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_Opportunity_TriggerSequenceCtrlr .cls 
   Description:     On insert/update/delete of Opportunity record 
   Date             Version         Author                            Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  04/2015         1.0           Karthik.A.S                      Initial Release
  06/2015         1.1           Stacey Walter                      Adjustment to Bypass functions
  06/07/2015      1.2           Davi Borges                       Change to submissionvalidation method signature to pass old opportunities
  24/08/2015      1.3          Leonard Victor                     RD-01257     R3 Code Stream Line
  24/08/2015      1.4           Gousia                            Recordtype change from B2b to b2B2L 
  3/11/2015       1.5           Leonard Victor                    Reverted changes relted to B2B to B2B2L 
  14/12/2015      1.6           Rahul Boinepally                  RD-01254 - Amended not to call PS_INT_IntegrationRequestController method if              Supress_integration flag is True
  17/12/2015      1.7          Leonard Victor                     RD-1649 - Invoking Genric Method for setting User Values on Oppty
  12/02/2016      1.8           Kyama                             RD-01686 Get oppty owner Territory code update territory code field  
    December 05-2016 1.9          Jaydip B                        Workflow code to update Enrollment status to Enrolled for D2L Oppty  need to be moved to Trigger scope
 01/11/2017       2.0        Saritha Singamsetty                  CR-01644 added code to call a method to update SPR owners whenever the respective opportunity gets updated. 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class PS_Opportunity_TriggerSequenceCtrlr {
    
     /**
    * Description : Performing all  Before Update Operations
    * @param NA
    * @return NA
    * @throws NA
    **/
    public static void beforeUpdate(List<Opportunity> newOpportunitys){
       
       //KP:Added method for opportunity validation
       if (System.Label.PS_EnableOpportunityLock.equalsIgnoreCase('On'))
       {
           PS_OpportunityOperations.beforeUpdatevalidation(newOpportunitys,(Map<Id,Opportunity>)Trigger.newMap,(Map<Id,Opportunity>)Trigger.oldMap);
       }            
        //D2L Operations
        List<Opportunity> D2Llist = OpptyUtils.getOpportunityByRecordType(PS_Constants.OPPORTUNITY_D2L_RECCORD,newOpportunitys);
        
        if(D2Llist.size()>0)
        {
            for(Opportunity oppList : newOpportunitys)
            {
                
                if(!oppList.supress_notification__c)
                {                   
                    PS_INT_IntegrationRequestController.submissionValidation(newOpportunitys,(Map<Id,Opportunity>)trigger.oldMap);                  
                }
            }
            
             
            PS_CreateAssetbasedStudentRegistration.OpportunityStage((Map<Id,Opportunity>)trigger.newMap,newOpportunitys,(Map<Id,Opportunity>)trigger.oldMap);
            PS_CreateAssetbasedStudentRegistration.ValidationOnTask((Map<Id,Opportunity>)trigger.newMap,newOpportunitys);
            PS_CreateAssetbasedStudentRegistration.ValidationOnTaskAndEvent((Map<Id,Opportunity>)trigger.newMap,newOpportunitys);       
        }
    }
    
    /**
    * Description : New method to update Enrollment status to Enrolled
    * @param NA
    * @return NA
    * @throws NA
    **/
    public static void updateToEnrolled(List<Opportunity> newOpptList, MAP<ID,Opportunity> oldOpptyMap) {
       string D2LRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('D2L').getRecordTypeId();
        
       for(Opportunity opptyRec:newOpptList) {
         if(opptyRec.RecordTypeID==D2LRecordTypeId && opptyRec.stageName=='Closed' && oldOpptyMap!=null && opptyRec.stageName !=oldOpptyMap.get(opptyRec.Id).stageName) {
               opptyRec.Enrolment_Status__c='Enrolled';          
         
            }
         } 
     }
    
    
    
    /**
    * Description : Performing all  before Insert operation
    * @param NA
    * @return NA
    * @throws NA
    **/
    public static void beforeInsert(List<Opportunity> newOpportunitys){
        //KP:Added method for opportunity validation
       
        if (System.Label.PS_EnableOpportunityLock.equalsIgnoreCase('On'))
        {
            PS_OpportunityOperations.beforeUpdatevalidation(newOpportunitys,(Map<Id,Opportunity>)Trigger.newMap,null);
        }   
        PS_UserFieldUtill.setUserFieldValues(newOpportunitys , 'Ownerid');

        PS_OpportunityOperations.setPricebookforglobaloppty(newOpportunitys);
        
    }
    
    /**
    * Description : Performing all  After Update Operations
    * @param NA
    * @return NA
    * @throws NA
    **/
    public static void afterUpdate(List<Opportunity> newOpportunitys, Map<Id,Opportunity> oppOldMap)
    {
           PS_OpportunityB2BNAActions.cdsOpportunityClosingNotification(newOpportunitys,oppOldMap); // RD: 652 | Pooja |3rd June
           
        //Added by Kyama For for RD-01686.3.2 release
        List<Opportunity> B2BList = OpptyUtils.getOpportunityByRecordType(PS_Constants.OPPORTUNITY_B2B_RECCORD,newOpportunitys);
        if(B2BList.size()>0)
        {
          PS_OpportunityB2BNAActions.updateOpptyownerTerritoryCode(B2BList,oppOldMap);//getOpptyOwnerTerrCode
          PS_OpportunityB2BNAActions.updateOpptyowneronSPR(B2BList,oppOldMap);  // Added for CR-01644 by Saritha Singamsetty  
        }//Ended Kyama Logic
        
        //D2L Operations
        
        List<Opportunity> D2Llist = OpptyUtils.getOpportunityByRecordType(PS_Constants.OPPORTUNITY_D2L_RECCORD,newOpportunitys);
        
        if(D2Llist.size()>0)
        {
            List<Opportunity> convertedList = OpptyUtils.getLeadConvertedOpp(D2Llist);
            List<Opportunity> nonConvertedList = OpptyUtils.getLeadNonConvertedOpp(D2Llist);
            
            PS_opportunity_UpdateAccountTeamAdmin.updateadmin(D2Llist);
            for(Opportunity oppList : newOpportunitys)
            {
                if(!oppList.supress_notification__c)
                {    
                    PS_INT_IntegrationRequestController.createIntegrationRequestOpp(D2Llist, Trigger.oldMap);                   
                }
            }
            
            PS_CreateAssetbasedStudentRegistration.createTaskOnPsychology(D2Llist);
            sendEmailToOpptyContact.OppOutboundNotification(D2Llist,Trigger.oldMap,true);

            
            if(nonConvertedList.size()>0)
            {
                PS_CreateAssetbasedStudentRegistration.createTask_Method1(D2Llist); 
                PS_CreateAssetbasedStudentRegistration.ReturningBuisinesscreateTask(nonConvertedList);
            }
        }
        
       // PS_OpportunityOperations.createContract(newOpportunitys); 
        
    }
    
    /**
    * Description : Performing all  After Insert operation
    * @param NA
    * @return NA
    * @throws NA
    **/
    public static void afterInsert(List<Opportunity> newOpportunitys){
        
        List<Opportunity> D2Llist = OpptyUtils.getOpportunityByRecordType(PS_Constants.OPPORTUNITY_D2L_RECCORD,newOpportunitys);
        //Added by Kyama For for RD-01686.3.2 release
        List<Opportunity> B2BList = OpptyUtils.getOpportunityByRecordType(PS_Constants.OPPORTUNITY_B2B_RECCORD,newOpportunitys);
        if(B2BList.size()>0)
        {
          PS_OpportunityB2BNAActions.getOpptyOwnerTerritoryCode(B2BList);//getOpptyOwnerTerrCode
        }//Ended Kyama Logic
        
        /* Code change for CR-01908-Req-79(CRMSFDC-2943) -- Start --Added by Srikanth*/
        String enterPriseOppRecType = Label.Enterprise_Opportunity_Record_Type;
        List<Opportunity> EnterPriseList = OpptyUtils.getOpportunityByRecordType(enterPriseOppRecType,newOpportunitys);
        if(EnterPriseList.size()>0){
          PS_OpportunityB2BNAActions.getOpptyOwnerTerritoryCode(EnterPriseList);
        }
        /* Code change for CR-01908-Req-79(CRMSFDC-2943) -- End*/
        
        if(D2Llist.size()>0)
        {
            List<Opportunity> convertedList = OpptyUtils.getLeadConvertedOpp(D2Llist);
            List<Opportunity> nonConvertedList = OpptyUtils.getLeadNonConvertedOpp(D2Llist);    
            
            PS_opportunity_UpdateAccountTeamAdmin.updateadmin(D2Llist); 
            PS_CreateAssetbasedStudentRegistration.createTask(D2Llist);
            //chaitra 10/2015 removed the condition that lets opportunity that are not converted from lead to have contact roles set on opportunity creation
            PS_opportunity_MapAccountcontactRole.MapAccountcontactRole(D2Llist);
            if(nonConvertedList.size()>0)
            {
                //PS_opportunity_MapAccountcontactRole.MapAccountcontactRole(nonConvertedList);
                PS_CreateAssetbasedStudentRegistration.ReturningBuisinesscreateTask(nonConvertedList);
                PS_CreateAssetbasedStudentRegistration.createTask_Method(nonConvertedList);  
            }
            
         }
         //Moved Code from Opportunity Trigger Handler as part of R3 stream line
         PS_OpportunityOperations.copyFieldsOnOpportuity(newOpportunitys);
    }
}