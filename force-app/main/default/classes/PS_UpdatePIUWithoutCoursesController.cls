/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_UpdatePIUWithoutCoursesController
   Description:     Allows user to update PIU(s)
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   15 oct 2015      1.0           Accenture NDC                                         Created
   09 Sep 2019      1.3           Anurutheran             Modified class for September CI CR-02850: Adding Channel detail picklist 
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
 

public class PS_UpdatePIUWithoutCoursesController
{
    public string opportunityID;
    public string accountID;
    public String orderId;
    public string selectedThirdPartyLMSVal{get;set;}
    public string selectedChannelVal{get;set;}
    public string selectedAssetId{get;set;}
    public string selectedFieldVal{get;set;}
    public List<Asset> lstWithAsset{get;set;}
    public List<innerPIUWrapper> lstWithWraperRec;
    private integer counter=0;  //keeps track of the offset
    private integer list_size=10; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list 
    public String selectedUsageVal{get;set;}
    public string selectedModeOfDelVal{get;set;} 
    public Date selectedInstallDateVal{get;set;}
    public Date selectedExpiryDateVal{get;set;}
    public string selectedChannelDetailVal{get;set;}//CR-02850
    public String depFieldJsonMap{get;set;}  //CR-02850
    public PS_UpdatePIUWithoutCoursesController()
    {
        if(ApexPages.currentPage().getParameters().get('opportunityId') != null)
        {
            opportunityID = ApexPages.currentPage().getParameters().get('opportunityId');
        }
        accountID = ApexPages.currentPage().getParameters().get('accountId'); 
        if(ApexPages.currentPage().getParameters().get('orderId') != null)
        {
            orderId = ApexPages.currentPage().getParameters().get('orderId');
        }
        lstWithAsset = new List<Asset>();
        lstWithWraperRec = new List<innerPIUWrapper>();
        total_size = [select count() from Asset where AccountId =: accountID];    
        depFieldJsonMap=JSON.serialize(PS_GlobalPickListUtilities.assetChannelDetail()); //CR-02850
    }
    
    public List<Asset> getAssetDetails()
    {
        try
        {
            lstWithAsset = [select id,product2.Name,Usage__c,Quantity,Status__c,Mode_of_Delivery__c,Third_Party_LMS__c,Channel__c,InstallDate,Expiry_Date__c,Channel_Detail__c from Asset where AccountId =: accountID AND (Status__c='Active' OR Status__c='Pending') ORDER BY InstallDate DESC LIMIT : list_size OFFSET: counter];
        }catch(Exception e)
        {
            ApexPages.addMessages(e);  
        }
        return lstWithAsset;
    }
    
    //get Status picklist values from Asset object using describe method
    public List<SelectOption> getStatus()
    {
        return PS_GlobalPickListUtilities.assetStatus();
    }
    
    //get Usage picklist values from  Asset object using describe method
    public List<SelectOption> getUsage()
    {
        return PS_GlobalPickListUtilities.assetUsage();    
    }
    
    //get Mode Of Delivery picklist values from Asset object using describe method
    public List<SelectOption> getModeOfDelivery()
    {
        return PS_GlobalPickListUtilities.assetModeOfDelivery();
    }
    
     //get Third Party LMS picklist values from Asset object using describe method
    public List<SelectOption> getThirdPartyLMS()
    {
        return PS_GlobalPickListUtilities.assetThirdPartyLMS();
    }
    //get Channel picklist values from Asset object using describe method
    public List<SelectOption> getChannel()
    {
        return PS_GlobalPickListUtilities.assetChannel();
    }
    
    //CR-02850: Changes Start
    //get Channel Detail picklist values from Asset object using describe method
    public List<SelectOption> getChannelDetail()
    {    
        Map<Object,List<String>> DependentFieldValMap=PS_GlobalPickListUtilities.assetChannelDetail();
        List<String>ple=new List<String>();
        for(List<String> pleList:DependentFieldValMap.Values())
        {
            for(string p:pleList)
            {
                if(!ple.Contains(p))
                {
                    ple.add(p);
                }
            }
        }
        return PS_GlobalPickListUtilities.selectOptionUtility(ple);
    }
    //CR-02850: Changes End
    
    public PageReference Beginning() 
    { 
        //user clicked beginning
        counter = 0;
        return null;
    }

    public PageReference Previous() 
    { 
        //user clicked previous button
        counter -= list_size;
        return null;
    }

    public PageReference Next() 
    { 
        //user clicked next button
        counter += list_size;
        return null;
    }

    public PageReference End() 
    { 
        //user clicked end
        counter = total_size - math.mod(total_size, list_size);
        return null;
    }

    public Boolean getDisablePrevious() 
    { 
        //this will disable the previous and beginning buttons
        if (counter>0) 
        {
            return false;
        }else 
        {
            return true;
        }
    }

    public Boolean getDisableNext() 
    { 
        //this will disable the next and end buttons
        if (counter + list_size < total_size) 
        {
            return false; 
        }else 
        {
            return true;
        }
    }

    public Integer getTotal_size() 
    {
        return total_size;
    }

    public Integer getPageNumber() 
    {
        return counter/list_size + 1;
    }

    public Integer getTotalPages() 
    {
        if(math.mod(total_size, list_size) > 0) 
        {
            return total_size/list_size + 1;
        }else 
        {
            return (total_size/list_size);
        }
    }
    
    public pageReference cancel()
    {
         pageReference returnToOppty;
        if(opportunityID != null && opportunityID != '')
        {
            returnToOppty =new pageReference('/'+opportunityID);
        }
        if(orderId != null && orderId != '')
        {
            returnToOppty =new pageReference('/'+orderId );
        }
        return returnToOppty ;
    }
    
    public void createNewWrapperRecord()
    {
        try
        {
        Integer counter =0;
        boolean counterFlag = false;
        Integer wrapperIndex;
        boolean recordNotFound = false;
        if(selectedAssetId != null)
        {
           if(!lstWithWraperRec.isEmpty() && lstWithWraperRec != null)
           {
               for(innerPIUWrapper innerPIU : lstWithWraperRec)
               {
                   if(innerPIU.innerAssetID == selectedAssetId)
                   {
                       wrapperIndex = counter;
                       counterFlag = true;
                       break;
                   }else
                   {
                       recordNotFound = true;
                   }
                            counter = counter+1;
               }
           }else
           {
               lstWithWraperRec.add(new innerPIUWrapper(selectedAssetId,selectedFieldVal,selectedUsageVal,selectedModeOfDelVal,selectedThirdPartyLMSVal,selectedChannelVal,selectedInstallDateVal,selectedExpiryDateVal,selectedChannelDetailVal));
           }  
                    
           if(counterFlag && wrapperIndex != null)
           {
               lstWithWraperRec[wrapperIndex].innerStatusVal = selectedFieldVal;
               lstWithWraperRec[wrapperIndex].innerUsageVal = selectedUsageVal;
               lstWithWraperRec[wrapperIndex].innerModeOfDelivery = selectedModeOfDelVal;
               lstWithWraperRec[wrapperIndex].innerThirdPartyLMS = selectedThirdPartyLMSVal;
               lstWithWraperRec[wrapperIndex].innerChannel = selectedChannelVal;
               lstWithWraperRec[wrapperIndex].innerChannelDetail = selectedChannelDetailVal;//CR-02850
               lstWithWraperRec[wrapperIndex].innerInstallDate = selectedInstallDateVal;
               lstWithWraperRec[wrapperIndex].innerExpiryDate = selectedExpiryDateVal;
           }else 
           if(recordNotFound)
           {
               lstWithWraperRec.add(new innerPIUWrapper(selectedAssetId,selectedFieldVal,selectedUsageVal,selectedModeOfDelVal,selectedThirdPartyLMSVal,selectedChannelVal,selectedInstallDateVal,selectedExpiryDateVal,selectedChannelDetailVal)); 
           }
           }
        }catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
            ApexPages.addMessage(myMsg); 
        }
    }
    
    public pageReference updatePIU()
    {
        pageReference pageToRedirect;
        List<Asset> lstWithAsset = new List<Asset>();
        Asset newAsset;
        Set<Id> setWithUpdatingAssetId = new Set<Id>();
        if(!lstWithWraperRec.isEmpty() && lstWithWraperRec!= null)
        {
            for(innerPIUWrapper assetRec : lstWithWraperRec)
            {
                newAsset = new Asset();
                newAsset.id = assetRec.innerAssetId;
                newAsset.Status__c = assetRec.innerStatusVal;
                newAsset.Usage__c = assetRec.innerUsageVal;
                newAsset.Mode_of_Delivery__c = assetRec.innerModeOfDelivery;
                newAsset.Third_Party_LMS__c = assetRec.innerThirdPartyLMS;
                newAsset.Channel__c = assetRec.innerChannel; 
                newAsset.Channel_Detail__c = assetRec.innerChannelDetail; //CR-02850
                newAsset.InstallDate = assetRec.innerInstallDate;
                newAsset.Expiry_Date__c = assetRec.innerExpiryDate;
                lstWithAsset.add(newAsset);
                setWithUpdatingAssetId.add(assetRec.innerAssetId);
            }
            if(!lstWithAsset.isEmpty() && lstWithAsset !=null)
            {
                try
                {
                    update lstWithAsset;
                    system.debug(lstWithAsset);
                    Map<Id,Set<Id>> mapWithContactProductInUse = new Map<Id,Set<Id>>();
                    Contact_Product_In_Use__c  contactProd;
                    List<Contact_Product_In_Use__c> lstWithContactProd = new List<Contact_Product_In_Use__c>();
                    if(setWithUpdatingAssetId.size()>0 && !setWithUpdatingAssetId.isEmpty())
                    {              
                        for(Contact_Product_In_Use__c contactAsset : [select id,Product_in_Use__c,Status__c from Contact_Product_In_Use__c where Product_in_Use__c IN : setWithUpdatingAssetId])
                        {
                            if(mapWithContactProductInUse.containsKey(contactAsset.Product_in_Use__c))
                            {
                                mapWithContactProductInUse.get(contactAsset.Product_in_Use__c).add(contactAsset.Id);
                            }else
                            {
                                Set<ID> setWithContactID = new Set<ID>();
                                setWithContactID.add(contactAsset.Id);
                                mapWithContactProductInUse.put(contactAsset.Product_in_Use__c,setWithContactID);
                            }
                        }
                    }
                    if(!mapWithContactProductInUse.isEmpty())
                    {
                    for(Asset assetUpdt : lstWithAsset)
                    {
                        for(Id contactDet : mapWithContactProductInUse.get(assetUpdt.Id))
                        {
                            contactProd = new Contact_Product_In_Use__c();
                            contactProd.Id = contactDet;
                            contactProd.Status__c = assetUpdt.Status__c;
                            lstWithContactProd.add(contactProd);
                        }
                    }
                    }
                    if(!lstWithContactProd.isEmpty() && lstWithContactProd != null)  
                    {
                        update lstWithContactProd; 
                        system.debug(lstWithContactProd);
                    }   
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Updated Successfully');
                    ApexPages.addMessage(myMsg); 
                    if(opportunityID != null && opportunityID != '')
                    {
                        pageToRedirect = new pageReference('/'+opportunityID);
                    }else
                    if(orderId != null && orderId != '')
                    {
                        pageToRedirect = new pageReference('/'+orderId);    
                    }
                }catch(Exception e)
                {
                     if(e.getMessage().contains('Value does not exist or does not match filter criteria.: [Product2Id]'))
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'You cannot create PIU for product(s) that does not belong to your market.');
                        ApexPages.addMessage(myMsg);
                    }else
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,e.getMessage());
                        ApexPages.addMessage(myMsg);
                    }
                    return null; 
                }    
            }
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Please change atleast one row to proceed' );
            ApexPages.addMessage(myMsg); 
            return null;
        } 
        return pageToRedirect;   
    }
    
    public class innerPIUWrapper
    {
        public string innerAssetId;
        public string innerStatusVal;
        public string innerUsageVal;
        public string innerModeOfDelivery;
        public string innerThirdPartyLMS;
        public string innerChannel;
        public string innerChannelDetail;//CR-02850
        public date innerInstallDate;
        public date innerExpiryDate;
        public innerPIUWrapper(string assetDetail,string statusVal,string usageVal,string modeOfDel,string ThirdPartyLMSVal,string channelVal,date installDateVal,Date expiryDateVal,String channelDetailVal)
        {
            innerAssetId = assetDetail;
            innerStatusVal = statusVal;
            innerUsageVal = usageVal;
            innerModeOfDelivery = modeOfDel;
            innerThirdPartyLMS = ThirdPartyLMSVal;
            innerChannel = channelVal;
            innerChannelDetail=channelDetailVal;//CR-02850
            innerInstallDate = installDateVal;
            innerExpiryDate = expiryDateVal;
        }
    }
}