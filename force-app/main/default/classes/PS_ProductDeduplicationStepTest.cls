/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_ProductDeduplicationStepTest.cls
   Description:     Test  class for class PS_ProductDeduplicationStep

   Test Class:      
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   23/02/2016         1.0           Davi Borges             RD-01702                         Created
                                         
-----------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------- */

@isTest
public class PS_ProductDeduplicationStepTest {
	
		@isTest static void success() {
				
		Test.startTest();

		List<PS_ProductDeduplicationStep.PS_ProductDeduplicationStepRequest> requests = new 
						List<PS_ProductDeduplicationStep.PS_ProductDeduplicationStepRequest>();

		requests.add(new PS_ProductDeduplicationStep.PS_ProductDeduplicationStepRequest());

		requests.add(new PS_ProductDeduplicationStep.PS_ProductDeduplicationStepRequest());
		

		List<PS_ProductDeduplicationStep.PS_ProductDeduplicationStepResponse> responses = PS_ProductDeduplicationStep.invokeJob(requests);

		Test.stopTest();

		for(PS_ProductDeduplicationStep.PS_ProductDeduplicationStepResponse response : responses){

      		System.assertEquals(0,response.status);
      	}
     }
}