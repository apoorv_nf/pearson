/************************************************************************************************************
* Apex Interface Name : AutocreatedRegHandler1449048088690
* Version             : 1.1 
* Created Date        : 05/02/2016 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Neha Karpe            05/02/2016             
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

/*Test method for SAML JIT SSO handler*/
@isTest
public class PS_JITAutocreatedRegHandlerTest {

/*Method for creation of new user*/
public static testMethod void testNewUser(){
        /* String communityUserProfileName = 'Pearson Self-Service User';
        List<User> listWithUser = new List<User>();
        UserRole tempUserRole;
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        listWithUser = TestDataFactory.createUser([select Id from Profile where Name =: communityUserProfileName].Id,1);
        
        listWithUser[0].UserRoleid=tempUserRole.id;*/
          String communityUserProfileName = 'Pearson Self-Service User';
        List<User> listWithUser = new List<User>();
        listWithUser = TestDataFactory.createUser([select Id from Profile where Name =: communityUserProfileName].Id,1);
        Map<String, String> attributes = new Map<String, String>();
        String assertion ='';
        attributes.put('User.IsActive', 'true');
    attributes.put('User.ForecastEnabled','');
      attributes.put('User.ForecastEnabled','');
      attributes.put('User.Role__c','onecrm.learner');
     // attributes.put('User.UserRoleId','');
      attributes.put('User.Street','');
      attributes.put('User.State','');  
    attributes.put('User.City','');
      attributes.put('User.PostalCode','');
    attributes.put('User.Country','');
    attributes.put('User.MobilePhone','');
    attributes.put('User.Phone','');
    attributes.put('User.Alias','');
    attributes.put('User.LanguageLocaleKey','');
    attributes.put('User.LocaleSidKey','');
    attributes.put('User.TimeZoneSidKey','');
    attributes.put('User.EmailEncodingKey','');
    attributes.put('User.LastName','');
    attributes.put('User.LocaleSidKey','');
    attributes.put('User.FirstName','');
    attributes.put('User.Title','');
    attributes.put('User.FirstName','');
    attributes.put('User.FirstName','');attributes.put('User.CompanyName','');
    attributes.put('User.AboutMe','');
    attributes.put('User.CommunityNickname','');    
    attributes.put('Contact.Role__c','onecrm.learner');
     AutocreatedRegHandler1449048088690 obj = new AutocreatedRegHandler1449048088690();
        List<SamlSsoConfig> samlSso = [SELECT ID from SamlSsoConfig where loginURL LIKE '%SSO%'];
        List<RecordType> rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account'];
        Account account = new Account();
                  account.recordTypeId = rt[0].Id;
        Community_User_Geographical_Settings__c cugs = new Community_User_Geographical_Settings__c();
        cugs.Name = 'United States';
        cugs.Country__c = 'United States';
        cugs.Email_Encoding__c = 'UTF-8';
        cugs.Language__c = 'English';
        cugs.LanguageLocaleKey__c = 'en_US';
        cugs.TimeZone__c = 'America/New_York';
        insert cugs;
        
        Contact contact = new Contact();
                   contact.FirstName = 'fname';
                   contact.LastName = 'lname';
                   contact.Salutation = 'MR.';
                   contact.Email = 'sampleemailaddress@email.com';
                   contact.Phone = '111222333';      
                   contact.Preferred_Address__c = 'Mailing Address';
                   contact.MailingCity = 'Newcastle';
                   contact.MailingState='Northumberland';
                   contact.MailingCountry='United Kingdom';
                   contact.MailingStreet='1st Street' ;
                   contact.MailingPostalCode='NE28 7BJ';
                   contact.National_Identity_Number__c = '9412055708083';
                   contact.Passport_Number__c = 'ABCDE12345';
                   contact.Home_Phone__c = '9999';
                   contact.Birthdate__c = Date.newInstance(1900 , 10,10);
                   contact.Ethnic_Origin__c = 'Asian' ;
                   contact.Marital_Status__c = 'Single';
                   contact.First_Language__c = 'English';

        insert contact;
        
        //String communityUserProfileName = 'Pearson Self-Service User';

        
       // List<Profile> pID = [select Id from Profile where Name =: communityUserProfileName];
        User usr = new User();
            /*      u.FirstName = 'test';
                  u.LastName = 'happy';
                  u.alias = 'happy'; 
                  u.Email = 'h@gmail.com';  
                  u.Username='test1234@gmail.com';
                  u.CommunityNickname = 'happy';
                  u.LanguageLocaleKey='en_US'; 
                  u.TimeZoneSidKey='America/New_York';
                  u.LocaleSidKey='en_US';
                  u.EmailEncodingKey='ISO-8859-1';
                  u.ProfileId = pID[0].ID;              
                  u.Geography__c = 'Growth';
                  u.Market__c = 'ZA';
                  u.Line_of_Business__c = 'Higher Ed';
                  u.Business_Unit__c = 'CTIMGI';
                  u.ContactId = contact.Id;
                  
                  u.UserRoleId=tempUserRole.Id; 
                  insert u;*/

        //user to avoid Mixed DML error
       //usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIMGI', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
       //insert usr;
    
      
                                  
        ID samlSsoProviderId = samlSso[0].ID;
        ID communityId = '0DB8E0000008OIt';
        ID portalId = '0DB8E0000008OIt';
       // ID AccountID = listWithAccount[0].ID;
       
        String federationIdentifier = 'jitTestUser@testclass.com';
        boolean create = true;
        boolean isStandard = true;
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
        Test.StartTest();
        
        /*u = obj.createUser(samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        obj.handleJit(create, u, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        obj.handleUser(create, u, attributes, federationIdentifier, isStandard);
        obj.handleContact(create, AccountId, u, attributes);*/
        System.debug('*****Attributes:'+attributes.values());
            System.debug('*****Attributes:'+samlSsoProviderId);
        //obj.updateUser(listWithUser[0].ID, samlSsoProviderId,communityId, portalId,federationIdentifier, attributes,assertion);
        obj.createUser(samlSsoProviderId,communityId, portalId,federationIdentifier, attributes,assertion);
     
        Test.StopTest();
        
        }
        }
    /*Method for creation of new UK user*/
public static testMethod void testUKNewUser(){
        String communityUserProfileName = 'Pearson Self-Service User';
        List<User> listWithUser = new List<User>();
        listWithUser = TestDataFactory.createUser([select Id from Profile where Name =: communityUserProfileName].Id,1);
        
        Community_User_Geographical_Settings__c cugs = new Community_User_Geographical_Settings__c();
        cugs.Name = 'US';
        cugs.Country__c = 'United States';
        cugs.Email_Encoding__c = 'UTF-8';
        cugs.Language__c = 'English';
        cugs.LanguageLocaleKey__c = 'en_US';
        cugs.TimeZone__c = 'America/New_York';
        insert cugs;
        
        CS_Country_ContactUs_Mapping__c cccm = new CS_Country_ContactUs_Mapping__c ();
        cccm.Name = '1';
        cccm.Country__c = 'United Kingdom';
        cccm.Language__c = 'English';
        insert cccm;
        
        Map<String, String> attributes = new Map<String, String>();
        String assertion ='';
        attributes.put('User.IsActive', 'true');
    attributes.put('User.ForecastEnabled','');
      attributes.put('User.ForecastEnabled','');
      attributes.put('User.Role__c','onecrm.learner');
     // attributes.put('User.UserRoleId','');
      attributes.put('User.Street','');
      attributes.put('User.State','');  
    attributes.put('User.City','');
      attributes.put('User.PostalCode','');
    attributes.put('User.Country','United Kingdom');
    attributes.put('User.MobilePhone','');
    attributes.put('User.Phone','7438343488433488348438');
    //attributes.put('User.Alias','');
    attributes.put('User.LanguageLocaleKey','');
    attributes.put('User.LocaleSidKey','');
    attributes.put('User.TimeZoneSidKey','');
    attributes.put('User.EmailEncodingKey','');
    attributes.put('User.LastName','JitTest');
    attributes.put('User.LocaleSidKey','');
    attributes.put('User.FirstName','');
    attributes.put('User.Title','');
    attributes.put('User.FirstName','');
    attributes.put('User.FirstName','');attributes.put('User.CompanyName','');
    attributes.put('User.AboutMe','');
    attributes.put('User.CommunityNickname',''); 
    attributes.put('User.Email','a.a@a.com');    
        attributes.put('Contact.Role__c','onecrm.learner');
    attributes.put('User.Username','Test');
    AutocreatedRegHandler1449048088690 obj = new AutocreatedRegHandler1449048088690();
    List<SamlSsoConfig> samlSso = [SELECT ID from SamlSsoConfig where loginURL LIKE '%SSO%'];
        ID samlSsoProviderId = samlSso[0].ID;
        ID portalId = '0DB8E0000008OIt';
       // ID AccountID = listWithAccount[0].ID;
       
       
        String federationIdentifier = 'jitTestUser@testclass.com';
        boolean create = true;
        boolean isStandard = true;
       System.runAs(new User(Id=UserInfo.getUserId()))
        {
        Test.StartTest();
         obj.createUser(samlSsoProviderId,null, portalId,federationIdentifier, attributes,assertion);
        Test.StopTest();
        
        }
        }
     /*Method for creation of new UK user*/
public static testMethod void testUKCurrentUserDetails(){
        String communityUserProfileName = 'Pearson Self-Service User';
        List<User> listWithUser = new List<User>();
        listWithUser = TestDataFactory.createUser([select Id from Profile where Name =: communityUserProfileName].Id,1);
        List<RecordType> rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account'];
        Account account = new Account();
                  account.recordTypeId = rt[0].Id;
        Community_User_Geographical_Settings__c cugs = new Community_User_Geographical_Settings__c();
        cugs.Name = 'US';
        cugs.Country__c = 'United States';
        cugs.Email_Encoding__c = 'UTF-8';
        cugs.Language__c = 'English';
        cugs.LanguageLocaleKey__c = 'en_US';
        cugs.TimeZone__c = 'America/New_York';
        insert cugs;
        
        
        CS_Country_ContactUs_Mapping__c  cccm = new CS_Country_ContactUs_Mapping__c ();
        cccm.Name = '1';
        cccm.Country__c = 'United Kingdom';
        cccm.Language__c = 'English,France';
        insert cccm;
        
        List<Contact> contact = TestDataFactory.createContact(1);
        insert contact;
        
        Map<String, String> attributes = new Map<String, String>();
        String assertion ='';
        attributes.put('User.IsActive', 'true');
    attributes.put('User.ForecastEnabled','');
      attributes.put('User.ForecastEnabled','');
      attributes.put('User.Role__c','onecrm.learner');
     // attributes.put('User.UserRoleId','');
      attributes.put('User.Street','');
      attributes.put('User.State','');  
    attributes.put('User.City','');
      attributes.put('User.PostalCode','');
    attributes.put('User.Country','');
    attributes.put('User.MobilePhone','');
    attributes.put('User.Phone','7438343488433488348438');
    attributes.put('User.LastName','JitTest');
    attributes.put('User.FirstName','');
    attributes.put('User.Title','');
    attributes.put('User.FirstName','Test1');
    attributes.put('User.FirstName','');attributes.put('User.CompanyName','');
    attributes.put('User.AboutMe','');
    attributes.put('User.CommunityNickname',''); 
    attributes.put('User.Email',contact[0].email);    
        attributes.put('Contact.Role__c','onecrm.learner');
    attributes.put('User.Username','Test');
    AutocreatedRegHandler1449048088690 obj = new AutocreatedRegHandler1449048088690();
    List<SamlSsoConfig> samlSso = [SELECT ID from SamlSsoConfig where loginURL LIKE '%SSO%'];
        ID samlSsoProviderId = samlSso[0].ID;
        ID portalId = '0DB8E0000008OIt';
       // ID AccountID = listWithAccount[0].ID;
       
        String federationIdentifier = 'jitTestUser@testclass.com';
        boolean create = true;
        boolean isStandard = true;
       System.runAs(new User(Id=UserInfo.getUserId()))
        {
        Test.StartTest();
         obj.createUser(samlSsoProviderId,null, portalId,federationIdentifier, attributes,assertion);
        
        Test.StopTest();
        
        }
        }
}