/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            Lead_TriggerSequenceCtrlr.cls 
Description:     On insert/update/delete of Lead record 
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
03/2015         1.0            Kalidass                       Initial Release 
10/2015         1.2            Kamal Chandran           Included createMapping method to update the record type of account/contact and opportunity,
13/10/2015       1.3            Rahul Boinepally         Modified all methods to accept parameters from Lead Triggers.
Inserted new call to PS_LeadIntegrationUtilities class.     
09/11/2015       1.4            Kamal Chandran           Modified 'afterInsert' method to call 'assignIntegrationLeads' 
13/11/2015       1.5            Ron Matthews              Moved bypass logic to Lead_TriggerSequenceCtrlr for D-2472    
21/0/2016        1.2            Ron Matthews            Moved bypass logic back to trigger for D-3446   
02/03/2016       1.3            Kamal Chandran          Added the hashing functionality 
19/02/2016       1.6            Karthik.A.S             Included Method to check valid id on lead       
25/05/2016       1.7            Karthik.A.S              fixed leadimport wizrd issue                                   
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public with sharing class Lead_TriggerSequenceCtrlr 
{       
    /**
* Description : Performing all  Before Update Operations
* @param NA
* @return NA
* @throws NA
**/
    public static void beforeUpdate(List<Lead> newLeads)
    { 
        
        //PS_leadidvalidation.leadidvalidation(newLeads);       
        PS_Lead_RecordTagging_Ctrlr ldup = new PS_Lead_RecordTagging_Ctrlr();
        ldup.updateDateforQualifiedLead(newLeads, (map<id,lead>)Trigger.oldMap);
        //PS_PreventChangingOwner.PreventChangingLeadOwner(newLeads, (map<id,lead>)Trigger.oldMap); 
        //Call the hashing functionality if email is changed 
        List<Lead> listWithEmailChangedLeads = new List<Lead>();
        List<Lead> listWithNationalidChangedLeads = new List<Lead>();
        List<Lead> listWithownerChangedLeads = new List<Lead>();
        Map<Id, String> ownerMap = new Map<Id, String>();
        for(Lead newLead : newLeads)
        {
            
            ownerMap.put(newLead.OwnerId, null);
            
            if(newLead.Email != null)
            {
                if(newLead.Email != Trigger.oldMap.get(newLead.Id).get('Email'))
                {
                    listWithEmailChangedLeads.add(newLead);
                }
            }
            /** Commented by bekkam RajGopalaRao as part of CR-02923 
            if(newLead.Identification_Number__c != null&&Trigger.oldMap.get(newLead.Id).get('Identification_Number__c')!=null)
                
            {   
                if(newLead.Identification_Number__c != Trigger.oldMap.get(newLead.Id).get('Identification_Number__c'))
                    
                {
                    
                    listWithNationalidChangedLeads.add(newLead);
                }
            }**/                                                    
            
            
            
            if(Trigger.oldMap.get(newLead.Id).get('OwnerId') != UserInfo.getUserId() && newLead.OwnerId!= null)
                
            {  
                
                if(newLead.OwnerId != Trigger.oldMap.get(newLead.Id).get('OwnerId'))
                    
                {
                    if(Trigger.oldMap.get(newLead.Id).get('OwnerId') !=null ){
                        
                        listWithownerChangedLeads.add(newLead);
                    }
                }
                
            }
            
        }
        if(listWithEmailChangedLeads.size() > 0)
        {
            PS_HashingUtility.extractEmailHash(listWithEmailChangedLeads,'Lead');  
        }
        //Commented by Raja Gopalarao B As part of CR-02923   
       /** if(listWithNationalidChangedLeads.size() > 0)
        {
            PS_leadidvalidation.leadidvalidation(listWithNationalidChangedLeads);  
        } **/
        if(listWithownerChangedLeads.size() > 0)
        {
            PS_PreventChangingOwner.PreventChangingLeadOwner(newLeads, (map<id,lead>)Trigger.oldMap);  
        } 
        
    }
    
    
    /**
* Description : Performing all  before Insert operation
* @param NA
* @return NA
* @throws NA
**/
    public static void beforeInsert(List<Lead> newLeads)
    {  
        //PS_leadidvalidation.leadidvalidation(newLeads);  //Commented by Raja Gopalarao B As part of CR-02923        
        PS_Lead_UpdateStudentId.beforelead(newLeads);
        PS_Lead_RecordTagging_Ctrlr ldup = new PS_Lead_RecordTagging_Ctrlr();
        ldup.leadRecordTagging(Trigger.new);
        PS_LeadIntegrationUtilities.mapRecordType(newLeads);
        //CR-01558: Replacing the hardcoded 'Lead' value to Label.
        PS_HashingUtility.extractEmailHash(newLeads,Label.PS_SObjectLead); 
    }
    
    /**
* Description : Performing all  After Update Operations
* @param NA
* @return NA
* @throws NA
**/
    public static void afterUpdate(List<Lead>newLeads, Map<ID,Lead>oldLeadMap)
    {
        list<lead>leadconvert=new list<lead>();
        // PS_Lead_UpdateStudentId.beforelead1(newLeads);
        for(lead l: newLeads) {
            if(l.isconverted ||Test.isRunningTest()){ //Laxman added running test for CR-01794 seq controller test class
                
                leadconvert.add(l);
                
                
            }
        }
        
        if( leadconvert.size()>0){
            
            PS_lead_createContact.createAccountContact(newLeads, oldLeadMap);
            //RD-01200        
            PS_LeadConversionMapping.createMapping(newLeads, oldLeadMap); 
            PS_LeadConvertUtility plu = new PS_LeadConvertUtility();
            plu.fixIncorrectLeadConversion((Map<Id, Lead>)Trigger.newMap, oldleadMap);        
        }
    }
    
    /**
* Description : Performing all  After Insert operation
* @param NA
* @return NA
* @throws NA
**/          
    public static void afterInsert(List<Lead> newLeads)
    {     
        // PS_Lead_UpdateStudentId.beforelead(newLeads);
        PS_LeadIntegrationUtilities.createCampaignMember(newLeads);
        PS_LeadIntegrationUtilities.assignIntegrationLeads(newLeads);
        // CR-01794 Changes_Asha_14-11-2018 - Start
        //Id NARRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Account Request').getRecordTypeId();
        
        //Added the Below line for INC4944325 and changed API version from 33 to 43 for using the below method
        Id NARRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('New_Account_Request').getRecordTypeId();        
        List<Id> NARLeadsIds = new List<Id>();
        for(Lead l:newLeads){
            if(l.recordtypeId == NARRecordTypeId && l.Run_assignment_rules__c == True)
            {
                NARLeadsIds.add(l.Id);
            }
        }
        if(NARLeadsIds.size()>0){ 
            AssignLeadsUsingAssignmentRules.LeadAssign(NARLeadsIds);
        }
        // CR-01794 Changes_Asha_14-11-2018 - End
    }
}