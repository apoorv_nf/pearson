/************************************************************************************************************
* Apex Interface Name : PS_AccountContactCreationTest
* Version             : 1.1 
* Created Date        : 2/2/2016  
* Modification Log    : 
* Date              Version            Author                             Summary of Changes 
-----------       ----------      -----------------    ---------------------------------------------------------------------------------------------------
 15/4/2016           1.0            Sudhakar Navuluri                     Initial Release 
 21/4/2016           2.0            Sudhakar Navuluri                     Amended Code
 24/6/2016           3.0            Sneha                                 Modified for test class fix
 28/06/2016          4.0           Pooja                                      //modified to run the test class
-------------------------------------------------------------------------------------------------------------------------------------------------------------*/  
@isTest
public class PS_AccountContactCreationTest
{
    static testMethod void accountContactPostive()
    {
        Account AccountRec = new account( Name='Test Account1', Phone='+9100000' ,IsCreatedFromLead__c = True, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037');
        insert AccountRec;
        Account AccountRec1 = new account( Name='Test Account1', Phone='+9100000' ,IsCreatedFromLead__c = True, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037');
        insert AccountRec1;
        Account AccountRec2 = new account( Name='Test Account1', Phone='+9100000' ,IsCreatedFromLead__c = True, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037');
        insert AccountRec2;
       // insert lstAccount ;
        
        List<Contact> lstContact = TestDataFactory.createContact(1);  // modified by Pooja to run the test class
       
     //   lstContact[1].MailingState='England'; 
        insert lstContact; 
        
 
        //Modified for test class fix: Sneha
        AccountContact__c AccConRecord = TestDataFactory.createAccountContact(AccountRec2.id ,lstContact[0].id);
        insert AccConRecord;

        
        PS_ContactRole__c setting = new PS_ContactRole__c();
        setting.Name = 'College Educator';
        setting.RoleName__c='Educator';
        setting.RoleDetail__c='Instructor';
        insert setting; 
          
        List<case> cases = TestDataFactory.createCase(1,'School Assessment');
        cases[0].AccountId=AccountRec.id;
        cases[0].ContactId=lstContact[0].id;  
        //cases[0].ContactId=AccConRecord[0].id;
        cases[0].Contact_Type__c='College Educator';      
        Test.StartTest(); 
        insert cases;
        cases[0].AccountId= AccountRec1.id;
        Update cases;        
        Test.StopTest();
     }
}