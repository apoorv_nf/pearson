/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            ResponseQuoteOrderProcessing
   Description:     TEP Response pricing Wrapper class.
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   01 Aug 2016      1.0           Sachin Kadam                                        TEP Quote Price (Response) to One CRM 
---------------------------------------------------------------------------------------------------------------------------------------------------------- *///

public class ResponseQuoteOrderProcessing{
   public cls_SubmitOrderResponse SubmitOrderResponse;
   public class cls_SubmitOrderResponse {
      public cls_Header Header;
      public cls_Status Status;
   }
   public class cls_Header {
      public String HeaderVersion;  //1.0.0
      public cls_Initiator Initiator;
      public cls_Service Service;
      public cls_Event Event;
   }
   public class cls_Initiator {
      public String ApplicationID;  //OrgId Of SalesForce Instance
      public String DispatchTimestamp; //2005-02-02T23:00:00
   }
   public class cls_Service {
      public String Version;  //1.0.0
   }
   public class cls_Event {
      public String EventName;   //ONE-CRM PRICING ORDER
      public String SubEventName;   //
   }
   public class cls_Status {
      public String IsSuccessful;   //true
      public String Message;  //
      public String Reason;   //Success
      Public String Code; // code
      public String DestinationOrderId;   //
      public String BusinessOrderId;   //
      public String OrderHold;   //
      public String FreightCharge;
      public cls_TotalPrice TotalPrice;
      public cls_Promotions[] Promotions;
      public cls_LineItemStatuses LineItemStatuses;
   }
   public class cls_TotalPrice {
      public String DealSpecificDiscPercent; //
      public String PreApprovedDiscPercent;  //
      public String DealSpecificDiscAmt;  //
      public String PreApprovedDiscAmt;   //
      public String BasePrice;   //
      public String ListPrice;   //
      public String NetDiscountAmt; //
      public String NetDiscountPercent;   //
      public String NetPrice; //
      public String TaxPercent;  //
      public String TaxAmt;   //
   }
   public class cls_Promotions {
      public String AdjustmentCode; //
      public String AdjustmentStatus;  //
      public String AdjustmentValidity;   //
      public String AdjustmentAmount;  //
   }
   public class cls_LineItemStatuses {
      public cls_LineItemStatus[] LineItemStatus;
   }
   public class cls_LineItemStatus {
      public String SourceLineItemId;  //
      public String DestinationLineItemId;   //
      public String OrderedQuantity;   //
      public cls_Price Price;
      public String LineItemHold;   //
      public cls_Adjustments[] Adjustments;
      public String NetAdjustmentPercent; //
      public cls_OrderedProduct OrderedProduct;
   }
   public class cls_Price {
      public String UnitSellingPrice;  //Apttus_Config2__NetUnitPrice__c
      public String NetPrice; // Apttus_Config2__NetPrice__c
      public String TaxPercent;  //
      public String TaxAmt;   //
      public String ListPrice;   //
      public String ListPriceType;  //
      public String BasePrice;   //
      public String PreApprovedDiscPercent;  //
      public String PreApprovedDiscAmt;   //
      public String DealSpecificDiscPercent; //
      public String DealSpecificDiscAmt;  //
      public String ExtendedListPrice; //
   }
   public class cls_Adjustments {
      public String AdjustmentType; //
      public String AdjustmentDescriptor; //
      public String AdjustmentAmount;  //
      public String AdjustmentDescription;   //
      public String AdjustmentLevel;   //
   }
   public class cls_OrderedProduct {
      public String SourceId; //
   }
   public static ResponseQuoteOrderProcessing parse(String json){
      return (ResponseQuoteOrderProcessing) System.JSON.deserialize(json, ResponseQuoteOrderProcessing.class);
   }
}