@isTest(SeeAllData = false)                   
public class IntegrationRequestControllerTest{
     
     static testMethod void myTest() { 
 
            
          Test.StartTest();
          
              Opportunity opp = new Opportunity();
              opp.name = 'Test';
              opp.StageName = 'Needs Analysis';
              opp.CloseDate = System.Today();
              insert opp;             
          
              Integration_Request__c ir = new Integration_Request__c ();
              ir.Object_Id__c = opp.id;
              ir.check_responses__c =false;
              ir.CurrencyIsoCode  = 'ZAR';
              ir.Direction__c =   'Outbound';
              ir.Event__c = 'External Escalation';    
              ir.Geo__c   = 'Core';
              ir.Object_Name__c = 'Opportunity';  
              ir.Object_Record_Type__c='  Technical Support';
              ir.Service_End_Point__c ='LO3';
              ir.Status__c ='Completed';  
              ir.Sub_Event__c ='  Initial Escalation';
              insert ir;
              IntegrationRequestController.getIntegrationRequests(opp.id);
               
          
          Test.StopTest();
          
     }
    

}