/*******************************************************************************************************************
* Apex Class Name  : AssignmentRuleActivationTestClass
* Version          : 1.0 
* Created Date     : 08 May 2015
* Function         : Test Class of the AssignmentRuleActivation Class
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*                      08/05/2015              Created Initial Version of AssignmentRuleActivationTestClass
*******************************************************************************************************************/

@isTest(SeeAllData=true)
public class AssignmentRuleActivationTestClass {
    
    /*************************************************************************************************************
    * Name        : VerifyactivateAssignmentRule
    * Description : Verify the Activation Rules are getting activated      
    * Input       : 
    * Output      : 
    *************************************************************************************************************/
    static List<Case> caseList = new List<Case>();
    static List<Id> CaseIds = new List<Id>();
    static  Map<id,case> caseMap = new Map<id,case>();
    
    
    static testMethod void VerifyactivateAssignmentRuleonInsert()
    {
    
    Profile ProfileNameadmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        List<User> u1 = TestDataFactory.createUser(ProfileNameadmin.Id,1);
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu1'; 
            u1[0].Email ='Opptyproposaltest11@testorg.com'; 
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='Opptyproposaltest12'; 
            u1[0].LanguageLocaleKey ='en_US'; 
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = ProfileNameadmin.Id; 
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName ='standarduserQuoteCreation1@testorg.com';
            u1[0].Market__c ='UK';
            u1[0].Business_Unit__c = 'Higher Ed';
            u1[0].line_of_Business__c = 'Higher Ed';
            u1[0].Product_Business_Unit__c = 'Higher Ed';
            insert u1;
        }
        System.runAs(u1[0]){  
         Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u1[0].id,Disable_Triggers__c=true,Disable_Process_Builder__c=true);
        insert byp;
        
            Account acc = new Account();
          acc.Name = 'CTI Bedfordview Campus';
          acc.Line_of_Business__c= 'Higher Ed';
          acc.Geography__c = 'North America';
          acc.Market2__c = 'US';
          acc.Pearson_Campus__c=true;
          acc.Pearson_Account_Number__c='Campus';
          insert acc;
            
        FeedItem fi = new FeedItem();
              fi.ParentId =acc.id; 
            fi.Body = 'Testing the test data';
            fi.Type = 'PollPost';
            System.assert(true,fi); 
        insert fi;
        caseList  = TestDataFactory.createCase(1, 'General');
             
        system.debug('@@before for caselist-->'+caselist);
        for(Case c : caseList){
        c.type = 'Request Account Update';
          
             //c.type = ' Request New Department';
            
           
                           
        }
        
        
        insert caseList;
            /*Account Acc = new Account (
            Name= 'test');
            insert Acc ; 
            FeedItem feed = new FeedItem (
            parentid = Acc.id,
            type = 'ContentPost',
            Body = 'Hello');
            insert feed ;*/ 
        
          for(Case c1 : caseList)
          {
           c1.status = 'onHold'; 
           casemap.put(c1.id,c1);               
        }
        System.debug('caseid-->'+CaseIds);
        System.debug('@@casemap-->'+casemap);
        CaseList[0].status = 'onHold';
        Update CaseList ;
       
       System.debug('@@casemap-->'+casemap);
       System.debug('@@caselist-->'+caseList);
     //  casemap.put(CaseIds ,caselist);
       AssignmentRuleActivation.activateAssignmentRule(caseList,true,true,caseMap);
       
       
        System.assert(! CaseList.Isempty());
        system.debug('\n\n################# AssignmentRuleActivationTestClass : caseList '+caseList+'\n\n');
        
     /*    fi.ParentId = CaseList[0].id; 
            fi.Body = 'Testing the test data';
            fi.Type = 'PollPost';
            System.assert(true,fi); 
        insert fi;
        System.debug('@@@@ insert feeditem-->' +fi);
        for(Case c : caseList){
        CaseList[0].type = 'Request Account Access';
        }
        update caseList;*/
        }
    }
}