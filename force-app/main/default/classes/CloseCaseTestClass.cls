/*******************************************************************************************************************
* Apex Class Name  : CloseCaseTestClass
* Version          : 1.0 
* Created Date     : 07 May 2015
* Function         : Test Class of the CloseCase Class
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*                      07/05/2015              Created Initial Version of CloseCaseTestClass
*******************************************************************************************************************/
@isTest(Seealldata = true)
public class CloseCaseTestClass {

    static testMethod void VerifyCannotCloseCase(){
            
        List<Case> cases = TestDataFactory.createCase(1,'General');      
        List<Id> CaseIds = new List<Id>();
         System.debug('@@@ casesize'+cases);
        for(Case c : cases)
            c.Status = 'In Progress';
            
        Database.SaveResult[] sr = Database.insert(cases);
        System.debug('@@@ casesize'+sr);
        for(Database.SaveResult s : sr){
            CaseIds.add(s.id);
        }
        
        Integer i =0;
       // for(i=0 ; sr.size()>0 ;i++ )
            for(Case c : cases)
                if(c.Id != null){
                    c.id = sr[0].id;
                    c.status = 'Closed';
                    //sr.remove(0);
                    continue;
                }
        system.debug('############# Testclass : cases' + cases);
        System.assert(!Cases.isEmpty()); 
       
        PS_Case_TriggerSequenceCtrlr.beforeInsert(cases);        
        List<Case> Caselist = [SELECT id, Description from Case where Id = :CaseIds];
        update(cases);
    }
    
   /*  static testmethod void unittest()
   {
   List<Case> Caselist = [SELECT id, Description from Case where Id = :CaseIds];///// 

if(!CaseList.isEmpty()) 
{ 
for(Case c : CaseList) 
{ 
c.Description ='New Case'; 
} 
 


} 
 
update (CaseList);

} */
   
   
  /*  List<Case> case1 = TestDataFactory.createCase(1,'General');
    Insert(case1);
    update(case1);
    } */
}