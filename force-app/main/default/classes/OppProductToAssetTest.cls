/*
* Modification History 
* Date              Developer               Description
* 18 Dec 2015       Karan Khanna            Removed reference of TestClassAutomation and used TestDataFactory because TestClassAutomation was populating default values in not required fields which was causing failure
* 13 oct 2016       Manikanta Nagubilli     For INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 19,47) 
*/

@isTest
private class OppProductToAssetTest {

    static testMethod void testSearchTermCleansing() {
    	User newUser =  [select Id, username from User where market__c = 'US' and Profile.name = 'System Administrator' and isActive = true Limit 1];

        Bypass_Settings__c settings = new Bypass_Settings__c();
        settings.Disable_Triggers__c = true;
        //settings.SetupOwnerId = lstWithTestUser[0].id;
        settings.SetupOwnerId =newUser.id;
        insert settings;
        
          //GK
        newUser.Product_Business_Unit__c = 'CTIPIHE';
        newUser.Market__c = 'US';
        newUser.Line_of_Business__c = 'Higher Ed';
        newUser.Business_Unit__c = 'CTIPIHE';
        update newUser;
        
        system.RunAs(newUser){
        
        
    /*TestClassAutomation.FillAllFields = true;
    
    Account sAccount        = (Account)TestClassAutomation.createSObject('Account');
    */
    List<Account> sAccount = TestDataFactory.createAccount(1,'Organisation');
    sAccount[0].BillingCountry      = 'Australia';
    sAccount[0].BillingState      = 'Victoria';
    sAccount[0].BillingCountryCode    = 'AU';
    sAccount[0].BillingStateCode    = 'VIC';
    sAccount[0].ShippingCountry    = 'Australia';
    sAccount[0].ShippingState      = 'Victoria';
    sAccount[0].ShippingCountryCode  = 'AU';
    sAccount[0].ShippingStateCode    = 'VIC';
    sAccount[0].Lead_Sponsor_Type__c = null;
    
    insert sAccount;
    
    // Product2 sProduct = (Product2)TestClassAutomation.createSObject('Product2');
    List<Product2> sProduct = TestDataFactory.createProduct(1);
    sProduct[0].Market__c = 'US';
    sProduct[0].Line_of_Business__c= 'Higher Ed';
    sProduct[0].Business_Unit__c = 'CTIPIHE';
    //GK
    sProduct[0].Author__c = 'Talent'; 
    sProduct[0].Copyright_Year__c = '2005';
    sProduct[0].Competitor_Product__c = true;
    insert sProduct;
     
    PriceBookEntry newPriceBookEntry = new PriceBookEntry();
    newPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();//listWithPriceBook[0].Id;
    newPriceBookEntry.UnitPrice = 10.0;
    newPriceBookEntry.Product2Id = sProduct[0].Id;
    newPriceBookEntry.CurrencyIsoCode= 'GBP';
    newPriceBookEntry.IsActive = true;
    insert newPriceBookEntry;

    
    //Opportunity sOpportunity        = (Opportunity)TestClassAutomation.createSObject('Opportunity');
    List<Opportunity> sOpportunity = TestDataFactory.createOpportunity(1,'Opportunity');
    sOpportunity[0].AccountId          = sAccount[0].Id;
    sOpportunity[0].StageName          = 'Closed';
    //sOpportunity.RecordTypeId       = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity').getRecordTypeId();
   // sOpportunity.Pricebook2Id       = Test.getStandardPricebookId();
    sOpportunity[0].CurrencyIsoCode= 'GBP';
    insert sOpportunity;
    
    UniversityCourse__c course = new UniversityCourse__c(Name= 'TestCourse',Account__c = sAccount[0].Id, Catalog_Code__c = 'C01',Course_Name__c = 'TestCourse');
    insert course;
    
    OpportunityUniversityCourse__c ouc = new OpportunityUniversityCourse__c(Opportunity__c = sOpportunity[0].Id, UniversityCourse__c = course.Id,Account__c = sAccount[0].Id,Opportunity_Name__c = sOpportunity[0].Id, Close_Date__c = system.Today(),Opportunity_University_Course_Amount__c = 100);
    insert ouc;
    
     List<Contact> contactList=TestDataFactory.createContact(1);
    contactList[0].AccountId = sAccount[0].id;
    insert contactList;
   
    //GK
    Competitor_Product__c compProd = new Competitor_Product__c( 
                                                                Opportunity__c = sOpportunity[0].Id,
                                                                Product__c= sProduct[0].id
                                                              );
     insert compProd;        
        
         
    //Test.startTest();
    
      OpportunityLineItem sOLI      = new OpportunityLineItem();
      sOLI.OpportunityId          = sOpportunity[0].Id;
      sOLI.PricebookEntryId        = newPriceBookEntry.Id;
      //sOLI.TotalPrice            = 200;
      sOLI.UnitPrice = 10.0;      
      sOLI.Quantity            = 1;
      
      insert sOLI;
      Test.StartTest();
        try{
            OppProductToAsset.setOLIS();          
        }catch(Exception e){}  
        
        try{
            OppProductToAsset.setAssetList();
        }catch(Exception e){} 
        try{
            OppProductToAsset.convert();
        }catch(Exception e){} 
        try{
            OppProductToAsset.setOUCS();       
        }catch(Exception e){} 
        OppProductToAsset.convertToAsset(sOpportunity[0].Id,sAccount[0].Id);
         //GK
        //OpportunityContactRole ocr = new OpportunityContactRole(ContactId = contactList[0].Id, OpportunityId = sOpportunity[0].Id, IsPrimary = true, Role= 'Desicion Maker');
        //insert ocr;
        OppProductToAsset.markInactiveconvertToAsset(sOpportunity[0].Id,sAccount[0].Id);  
        try{
            OppProductToAsset.CompProdConvertToAsset(sOpportunity[0].Id,sAccount[0].Id);
        }  catch(Exception e){}    
         try{
            OppProductToAsset.MarkInactiveCompProdConvertToAsset(sOpportunity[0].Id,sAccount[0].Id);
        }  catch(Exception e){}  
              
        OppProductToAsset obj = new OppProductToAsset();
        
        
        OpportunityContactRole ocr = new OpportunityContactRole(ContactId = contactList[0].Id, OpportunityId = sOpportunity[0].Id, IsPrimary = true, Role= 'Desicion Maker');
        insert ocr;
        //GK
        OppProductToAsset.markInactiveconvertToAsset(sOpportunity[0].Id,sAccount[0].Id);  
        try{
            OppProductToAsset.CompProdConvertToAsset(sOpportunity[0].Id,sAccount[0].Id);
        }  catch(Exception e){}    
         try{
            OppProductToAsset.MarkInactiveCompProdConvertToAsset(sOpportunity[0].Id,sAccount[0].Id);
        }  catch(Exception e){}  
            
        OppProductToAsset.setAssetList();
        delete sOLI;
        List<OpportunityLineItem> li = obj.getOlis();
        OppProductToAsset.setOLIS();
        }test.StopTest();
    }
}