/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_AccountContactDeactivationHandler.Cls
   Description:     Handler class of PS_AccountContactDeactivationController
   Test Class:      PS_AccountContactDeactivationHandler_Test 
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   24/09/2015         0.1        Accenture - Karan Khanna                Created
                                
---------------------------------------------------------------------------------------------------------------------------------------------------------- */




public with sharing class PS_AccountContactDeactivationHandler {
    
    //Declaration of Common Variables
    public AccountContact__c accContactH;
    public Boolean resultAccountContactSuccess;
    public Boolean resultCourseContactSuccess;
    public Boolean resultPIUSuccess;
    public static final string statusInactiveValue= 'Inactive';
    
    //constructor
    public PS_AccountContactDeactivationHandler() {
        
        accContactH = new AccountContact__c();
        resultAccountContactSuccess = false;
        resultCourseContactSuccess = false;
        resultPIUSuccess = false;
    }
    
    /*---------------------------------------------------------------------------------------------------------------------------------
    Author:         Karan Khanna
    Company:        Accenture
    Description:    This method is used to control the sequence and execute methods
    Inputs:         AccountContact__c record
    Returns:        none
    
    <Date>          <Authors Name>      <Brief Description of Change> 
    24-Sept-2015    Accenture_Karan     Initial version of the code
     
    -------------------------------------------------------------------------------------------------------------------------------*/        

    public void initiateMethodsExecution(AccountContact__c accContact) {
                
        accContactH = accContact;
        Savepoint sp = Database.setSavepoint();
        resultAccountContactSuccess = deactivateAccountContact();
        resultCourseContactSuccess = deactivateCourseContact();
        resultPIUSuccess = updateProductsInUse();
        
        if(resultAccountContactSuccess && resultCourseContactSuccess && resultPIUSuccess) {
            
            ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,Label.PS_AccContDeact_SucessMsg);
            ApexPages.addMessage(msg);
        }
        else {
            
            ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Error,Label.PS_AccContDeact_ErrorMsg);
            ApexPages.addMessage(msg);
            Database.rollback(sp);
        }
    }   
    
    /*---------------------------------------------------------------------------------------------------------------------------------
    Author:         Karan Khanna
    Company:        Accenture
    Description:    This method is used to deactivate AccountContact__c record
    Inputs:         none
    Returns:        Boolean
    
    <Date>          <Authors Name>      <Brief Description of Change> 
    24-Sept-2015    Accenture_Karan     Initial version of the code
     
    -------------------------------------------------------------------------------------------------------------------------------*/        

    public boolean deactivateAccountContact() {
        
        System.Debug('### accContactH in deactivateAccountContact() ' + accContactH);
        try {
            accContactH.active__c = false;      
            update accContactH;
        }
        catch (exception e) {
            System.Debug('### Exception has occurred in method deactivateAccountContact ' + e);
            ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Error,e.getMessage());
            ApexPages.addMessage(msg);
            return false;           
        }
                        
        return true;
        
    } // end of deactivateAccountContact()
    
    /*---------------------------------------------------------------------------------------------------------------------------------
    Author:         Karan Khanna
    Company:        Accenture
    Description:    This method is used to deactivate UniversityCourseContact__c records. Assumption made here is UniversityCourseContact__c records will exists only for HED type.
    Inputs:         none
    Returns:        Boolean
    
    <Date>          <Authors Name>      <Brief Description of Change> 
    24-Sept-2015    Accenture_Karan     Initial version of the code
     
    -------------------------------------------------------------------------------------------------------------------------------*/        

    public boolean deactivateCourseContact() {          
        
        List<UniversityCourseContact__c> courseContactList = new List<UniversityCourseContact__c>();
        try {
            System.Debug('### accContactH ' + accContactH);
            // There is no direct relation between Account and UniversityCourseContact__c hence going thrugh UniversityCourse__c
            Set<Id> courseId = new Set<Id>(new Map<Id, UniversityCourse__c>([SELECT Id FROM UniversityCourse__c WHERE Account__c=:accContactH.Account__c]).keySet());
            courseContactList = [SELECT Id, Active__c, UniversityCourse__c, Contact__c FROM UniversityCourseContact__c WHERE UniversityCourse__c IN:courseId AND Contact__c =:accContactH.Contact__c AND Active__c = true];         
            System.Debug('### courseId ' + courseId);
            for(UniversityCourseContact__c courseCont : courseContactList) {
                
                courseCont.active__c = false;
            }
            System.Debug('### courseContactList ' + courseContactList);
            update courseContactList;
        }
        catch (exception e) {
            System.Debug('### Exception has occurred in method deactivateCourseContact ' + e.getMessage());         
            return false;           
        }
            
        return true;
        
    } // end of deactivateCourseContact()
    
    /*---------------------------------------------------------------------------------------------------------------------------------
    Author:         Karan Khanna
    Company:        Accenture
    Description:    This method is used to update the Status Contact Products In Use records to Inactive. 
    Inputs:         none
    Returns:        Boolean
    
    <Date>          <Authors Name>      <Brief Description of Change> 
    24-Sept-2015    Accenture_Karan     Initial version of the code
     
    -------------------------------------------------------------------------------------------------------------------------------*/        

    public boolean updateProductsInUse() {
            
        List<Contact_Product_In_Use__c> cPIUList = new List<Contact_Product_In_Use__c>();
        try {
            Set<Id> PIUIdSet = new Set<Id>(new Map<Id, Asset>([SELECT Id FROM Asset WHERE AccountId=:accContactH.Account__c]).keySet());    
            // There is no direct relation between Account and Contact_Product_In_Use__c hence going thrugh Asset           
            cPIUList = [SELECT Id, Status__c, Contact__c, Product_in_Use__c FROM Contact_Product_In_Use__c WHERE Contact__c=:accContactH.Contact__c AND Product_in_Use__c IN: PIUIdSet AND Status__c !=: statusInactiveValue];
            
            for(Contact_Product_In_Use__c cPIU : cPIUList) {
                
                cPIU.Status__c = statusInactiveValue;
            }
            System.Debug('### cPIUList ' + cPIUList);
            update cPIUList;
        }
        catch (exception e) {
            System.Debug('### Exception has occurred in method updateProductsInUse ' + e.getMessage());         
            return false;           
        }
            
        return true;        
    } // end of updateProductsInUse()
    
} // end of Class