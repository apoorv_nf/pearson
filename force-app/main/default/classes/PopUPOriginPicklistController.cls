/************************************************************************************************************
* Name                : PopUPOriginPicklistController
* Version             : 1.0 
* Created Date        : 03 May 2016
* Function            : Class responsible to display the Origin field picklist values in the lookup while 
                        searching a product or product family record based on the Origin value of the product.
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Abhinav Srivastava              03/May/2016            Initial version
************************************************************************************************************/

public class PopUPOriginPicklistController{
public String selectedValueFromPopup { get; set; }

 public List < SelectOption > getOrigins() {
        List < SelectOption > options = new List < SelectOption > ();
        Schema.DescribeFieldResult fieldResult = Product2.Origin__c.getDescribe();
        List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
}