/************************************************************************************************************
* Apex Interface Name : PS_assetTriggerTest
* Version             : 1.0  
* Created Date        : 7/1/2015 4:44 
* Modification Log    : 
* Developer                   Date                  Summary Of Changes
* -----------------------------------------------------------------------------------------------------------
* Madhu                     29/10/2015            Adding code coverage for PS_assetTrigger.   
* Vinoth                    16-Oct-2019           CR-02949 - Replaced the Apttus to Non-Apttus changes Apttus_Config2__ProductId__c object
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_assetTriggerTest
{
     

    @isTest static void myTest1(){
    
    
         Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
     
      //code for creating an User
      User u = new User();
      u.LastName = 'territoryuser';
      u.alias = 'terrusr'; 
      u.Email = 'territoryuser@gmail.com';  
      u.Username='territoryuser@gmail.com';
      u.LanguageLocaleKey='en_US'; 
      u.TimeZoneSidKey='America/New_York';
      u.Price_List__c='Humanities & Social Science';
      u.LocaleSidKey='en_US';
      u.EmailEncodingKey='ISO-8859-1';
      u.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
      u.Geography__c = 'Growth';
      u.Market__c = 'US';
      u.Business_Unit__c = 'US Field Sales';
      u.Product_Business_Unit__c = 'US Field Sales';
      u.Line_of_Business__c = 'Higher Ed';
      u.License_Pools__c ='Test User';
      u.isactive=true;
      u.CurrencyIsoCode='USD';
      insert u;       
             
        
      System.runAs(u){
      
       List<Account> Acc = TestDataFactory.createAccount(1, 'Organisation');
       Acc[0].Territory_Code_s__c = '2ZZ'; 
       Acc[0].Market2__c = 'US'; 
       Acc[0].Line_of_Business__c = 'Higher Ed'; 
       Acc[0].Geography__c = 'Growth'; 
       insert Acc; 
          //code for creating course
      List<UniversityCourse__c> courselist=new List<UniversityCourse__c>();
      for(integer i=0;i<1;i++){
          UniversityCourse__c course = new UniversityCourse__c();
          course.Name = 'TerritoryCourseNameandcode'+i;
          course.Account__c = Acc[0].id;
          course.Catalog_Code__c = 'Territorycoursecode'+i;
          course.Course_Name__c = 'Territorycoursename'+i;
          course.CurrencyIsoCode = 'USD';
          courselist.add(course);
      }
      insert courselist;
      List<Contact> contactList=TestDataFactory.createContact(1);
      insert contactList;
      Product2 pf = TestDataFactory.insertRelatedProducts();
      
      //  Added the below code for CR-02949 Apttus to Non-Apttus changes by Vinoth

      RelatedProduct__c repd = new RelatedProduct__c();
      repd.RelatedProduct__c = pf.id;
      insert repd;
          
      List<Product2> rp = [select id,name from Product2 where id in (select RelatedProduct__c from
      RelatedProduct__c where Product__c = :pf.id and RelatedProduct__r.name like 'NA Territory%')]; 
      //RelatedProduct__c where RelatedProduct__c = :pf.id and RelatedProduct__r.name like 'NA Territory%')];          
      
      /* Commented for CR-02949 Apttus to Non-Apttus changes by Vinoth - Start
      List<Product2> rp = [select id,name from Product2 where id in (select Apttus_Config2__RelatedProductId__c  from
        Apttus_Config2__RelatedProduct__c where Apttus_Config2__ProductId__c = :pf.id and Apttus_Config2__RelatedProductId__r.name like 'NA Territory%')];      
      Commented for CR-02949 Apttus to Non-Apttus changes by Vinoth - End       */  
      
      test.StartTest();
     List<Asset> asstlist=new List<Asset>();
      for(integer i=0;i<1;i++){
        Asset asset = new Asset();
       asset.name = 'TerritoryAsset'+i;
       asset.Product2Id = rp[0].id;
       asset.AccountId = Acc[0].id;
       asset.Course__c = courselist[0].id;
       asset.Primary__c = True;
       asset.Status__c = 'Active';
       asset.ContactId = contactList[0].Id;
       asstlist.add(asset);
      } 
      insert asstlist;
     
     asstlist[0].Quantity = 1;
     update asstlist[0];
       test.stopTest();
       }
      
      
     
    }
    
}