public with sharing class EinsteinBotCSCaseCreation {

    @InvocableMethod(label='Einstein Bot - Create Case For Customer Support')
    public static List<Case> createCase(List<CaseCreationRequest> caseRequests){
        try{
            CaseCreationRequest caseRequest = caseRequests[0];
            Case newCase = new Case();
            MessagingSession msgSession = [SELECT Id, MessagingEndUser.ContactId, MessagingEndUser.MessagingPlatformKey, MessagingEndUser.MessagingChannel.DeveloperName,   
                                            CaseId, MessagingEndUser.AccountId, MessagingEndUser.Contact.Email, MessagingEndUser.MessagingChannel.TargetQueue.DeveloperName  
                                                  FROM MessagingSession 
                                                    WHERE Id =: caseRequest.routableId LIMIT 1];

            newCase.RecordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Customer_Service').getRecordTypeId();
            newCase.Subject = 'SMS Chat Case';
            newCase.PS_Business_Vertical__c = 'Higher Education';
            newCase.Category__c = caseRequest.category;
            newCase.Subcategory__c = caseRequest.subcategory;
            newCase.Chat_Session_Key__c = caseRequest.routableId;
            newCase.ContactId = msgSession.MessagingEndUser.ContactId;
            newCase.AccountId = msgSession.MessagingEndUser.AccountId;
            newCase.Contact_Email__c = msgSession.MessagingEndUser.Contact.Email;
            newCase.Contact_Organization_School__c = caseRequest.institution;
            newCase.Origin = caseRequest.origin;
            newCase.SuppliedPhone = msgSession.MessagingEndUser.MessagingPlatformKey;
            newCase.ISBN__c = caseRequest.isbn;
            newCase.Web_Order_Id__c = caseRequest.webOrderID;
            newCase.Contact_Type__c = 'College Student';
            newCase.Description = caseRequest.message;
            if([
                SELECT Id
                    FROM UserServicePresence
                        WHERE ServicePresenceStatus.DeveloperName = 'Available_for_SMS'
                            AND OwnerId IN (
                                            SELECT UserOrGroupId
                                                FROM GroupMember
                                                    WHERE Group.DeveloperName = :msgSession.MessagingEndUser.MessagingChannel.TargetQueue.DeveloperName 
                                            )
                            AND IsCurrentState = true
                            AND CreatedDate = LAST_N_DAYS:2
            ].isEmpty()) {
                newCase.OwnerId = [SELECT Id FROM Group WHERE DeveloperName = 'NAUS_HECS_LM_FOLLOW_UP'].Id;
                newCase.Status = 'New';
            }

            insert newCase;

            if(newCase.Id != NULL) {
                
                msgSession.CaseId = newCase.Id;
                update msgSession;
                return [SELECT Id,CaseNumber FROM Case WHERE Id =: newCase.Id];
            }
        } catch(Exception ex){
        }
        return NULL;
    }

     public class CaseCreationRequest
    {
        @InvocableVariable
        public String routableId;

        @InvocableVariable
        public String email;

        @InvocableVariable
        public String institution;

        @InvocableVariable
        public String category;

        @InvocableVariable
        public String subcategory;

        @InvocableVariable
        public String message;

        @InvocableVariable
        public String origin;

        @InvocableVariable
        public String webOrderID;

        @InvocableVariable
        public String isbn;
    }
}