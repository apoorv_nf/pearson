public class AccountActiveInactiveCourseExtn {

    public Id posId       {get;set;}
    public String accId       {get;set;}
    public String status;
    public String uName     {get;set;}
    public Id aId       {get;set;}
    public Id arId      {get;set;}
    public list<UniversityCourse__c> accdel {get;set;}
    public Boolean nullQtyError {get;set;}
    public string selOptions1 {get;set;}
    public string sortOptions1 {get;set;}
    public string selOptions2 {get;set;}
    public string sortOptions2 {get;set;}
    public string[] fields;
    public string soql {get;set;}
    public string serOptions1 {get;set;}
    public string serCond1 {get;set;}
    public string serOptions2 {get;set;}
    public string serCond2 {get;set;}
    public string selandor {get;set;}
    Public List<SelectOption> condOptions {get;set;}
    Public List<SelectOption> options {get;set;}
    public string courseType;
    public boolean addNewFilterLink {get;set;}
    public String selectedFilterbyValue {get;set;} 
    public Integer selectedRow {get;set;} 
    public Integer rowNumber {get;set;}
    public boolean addedrow{get;set;}
    public integer loadtime{get;set;}
    public Boolean isSearch {get;set;}
    public String searchStr {get;set;}
    public String type {get;set;}
    public list<wrapperAsset> wrapList {get;set;}
     public List < dynamicAddFilterSearch > listWithSelectOptions {
          get {
            return listWithSelectOptions;
        }
        set;
    }
    public List < SelectOption > searchKey {get;set;}
    public List < SelectOption > searchType {get;set;}
    public Integer rowToRemove {get;set;}
    
    //second wrapper
    Public string colOptions1 {get;set;}
    public string codOptions1 {get;set;}
    public string colOptions2 {get;set;}
    public string codtOptions2 {get;set;}
    Public List<SelectOption> condOptions1 {get;set;}
    Public String searchText1 {get;set;}
    Public String searchText2 {get;set;}
    Public String selectedValueFromPopup {get;set;}
    public Integer rowToRemove1 {get;set;}
    public Map<String, Schema.SObjectField> fieldMap {get;set;}
    public List < dynamicAddFilterSearch1 > listWithSelectOptions1 {
          get {
            return listWithSelectOptions1;
        }
        set;
    }
    public List < SelectOption > searchKey1 {get;set;}
    public List < SelectOption > searchType1 {get;set;}
    public boolean addNewFilterLink1 {get;set;}
    public String selectedFilterbyValue1 {get;set;} 
    public Integer selectedRow1 {get;set;} 
    public Integer rowNumber1 {get;set;}
    public String posName {get;set;}
    public String selectedSearchType {get;set;}
    public String searchText {get;set;} 
    public String selectedCondition {get;set;}
    public Boolean toDisplayLookup {get;set;} 
    public String UserMessageTitle {get;set;}
    public Boolean ShowUserMessage {get;set;}
    public String UserMessage {get;set;}
    public String JsonMap{get;set;} 
    public Boolean isSort {get;set;}
    public Map<String,SChema.DisplayType> dataTypeMap {get;set;}
    //second wrapper ends
    public AccountActiveInactiveCourseExtn(ApexPages.StandardController controller) {
               //  posId = controller.getRecord().Id; 
        addNewFilterLink = true; 
        addedrow = false;
        loadtime = 0;
        isSearch = false;
        isSort = false;
        posId = ApexPages.currentPage().getParameters().get('AccountId');
        posName = ApexPages.currentPage().getParameters().get('AccountName');
        courseType = ApexPages.currentPage().getParameters().get('Type');
       
        nullQtyError = false;
        wrapList = new list<wrapperAsset>();
        this.ViewData();
        getField();
        getCondOperations();
        getCondOperations1();
        SObjectType productType = Schema.getGlobalDescribe().get('UniversityCourse__c');
        fieldMap = productType.getDescribe().fields.getMap();
        dataTypeMap = new Map<String,SChema.DisplayType>();
        dataTypeMap.put('--None--',Schema.DisplayType.STRING);
         for (String fieldName: fieldMap.keySet()) { 
            dataTypeMap.put(fieldName,fieldMap.get(fieldName).getDescribe().getType());
         }
        JsonMap = JSON.serialize(datatypemap); 
        listWithSelectOptions = new List < dynamicAddFilterSearch > ();
        listWithSelectOptions1 = new List < dynamicAddFilterSearch1 > ();        
        addNewFilter();
        addNewFilter1();
    } 
    public void getCondOperations1() {
     condOptions1  = new List<SelectOption>(); 
     condOptions1.add(new SelectOption('--None--','--None--'));
     condOptions1.add(new SelectOption('=','equals'));
     condOptions1.add(new SelectOption('!=','not equal to'));
     condOptions1.add(new SelectOption('like %','starts with'));
     condOptions1.add(new SelectOption('like %%','contains'));
     condOptions1.add(new SelectOption('like','does not contain'));
    }
    public void searchResults() {
        accdel = new List<UniversityCourse__c>();
        wrapList = new list<wrapperAsset>();
        isSearch = true;
        searchStr = '';
        try{
            if(listWithSelectOptions1.size()>0){
                Boolean flag = false;
                Integer count = 0;
                if(courseType == 'Inactive'){
                soql = 'select id,Account__r.Primary_Selling_Account__r.Id,Account__r.Primary_Selling_Account__r.Name,Account__r.Id,Account__r.Name,Account__r.Primary_Selling_Account__c,Account__r.Primary_Selling_Account_check__c, Name, Primary_Selling_Account__c, Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c,Fall_Front_List__c,Spring_Front_List__c,CourseSpecialty__c,Status__c from UniversityCourse__c where Account__c = '+'\''+posId+'\''+' AND'+' Status__c IN '+'('+'\''+'Remove'+'\''+','+'\''+'Inactive'+'\''+')'+' AND ';
                } else
                {
                soql = 'select id,Account__r.Primary_Selling_Account__r.Id,Account__r.Primary_Selling_Account__r.Name,Account__r.Id,Account__r.Name,Account__r.Primary_Selling_Account__c,Account__r.Primary_Selling_Account_check__c, Name, Primary_Selling_Account__c, Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c,Fall_Front_List__c,Spring_Front_List__c,CourseSpecialty__c,Status__c from UniversityCourse__c where Account__c = '+'\''+posId+'\''+' AND'+' Status__c= '+'\''+'Active'+'\''+' AND ';
                }
//                soql = 'select id, Name, Primary_Selling_AccountText__c, Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c,Fall_Front_List__c,Spring_Front_List__c,CourseSpecialty__c,Status__c from UniversityCourse__c where ';      
                for(dynamicAddFilterSearch1 wrapper : listWithSelectOptions1){
                    String selCondOptions1 = wrapper.selectedCondition;
                    String selectedField = wrapper.selectedProductFilterValue;
                    String searchText1 = wrapper.searchText;
                    String searchText11 = '';
                    //Date dateToCompare;
                    Boolean doesNotContains = false;                    
                    
                    if (selCondOptions1 == '--None--' || wrapper.selectedProductFilterValue == '--None--') {
                       count=count+1;
                       continue; 
                   } 
                    Schema.DisplayType dataType = fieldMap.get(selectedField).getDescribe().getType();
                    String operator = '';
                   // soql = soql+selectedField;

                    if(selCondOptions1 == 'like %')
                    {
                        if(searchText1 == '')
                            searchText11 = '= \''+searchText1+'\'';
                        else
                            searchText11 = 'LIKE \''+searchText1+'%\'';
                    }else if(selCondOptions1 == 'like %%')
                    {
                        if(searchText1 == '')
                            searchText11 = '= \''+searchText1+'\'';
                        else
                            searchText11 = 'LIKE \'%'+searchText1+'%\'';
                    }else if(selCondOptions1 == 'like')
                    {
                        if(searchText1 == '')
                              searchText11 = '<> \''+searchText1+'\'';
                        else
                        {  searchText11 = ' ( NOT '+selectedField+' LIKE \'%'+searchText1+'%\' )';
                                doesNotContains = true;
                        }
                    }else if(selCondOptions1 == '=')
                    { 
                        
                      if (dataType == Schema.DisplayType.Integer || dataType == Schema.DisplayType.Boolean || dataType == Schema.DisplayType.Double)                        
                      {
                          if(searchText1 == '')
                              searchText11 = '= null';
                          else
                              searchText11 = '= '+searchText1;
                      }
                        else
                            searchText11 = '= \''+searchText1+'\'';   
                      
                    }else if(selCondOptions1 == '!=')
                    {
                       if(dataType == Schema.DisplayType.Integer || dataType == Schema.DisplayType.Boolean || dataType == Schema.DisplayType.Double)                        
                       {
                           if(searchText1 == '')
                               searchText11 = '<> null';
                           else
                               searchText11 = '<> '+searchText1;       
                       }
                       else
                          searchText11 = '<> \''+searchText1+'\'';     
 
                                          
                    }else if(selCondOptions1 == '<')
                    {   
                        
                           
                        if(dataType == Schema.DisplayType.Integer  || dataType == Schema.DisplayType.Double)                        
                           {
                               if(searchText1 == '')
                                    searchText11 = '<> null';
                               else
                                    searchText11 = '< '+searchText1;       
                           }

                    }
                    else if(selCondOptions1 == '>')
                    {
                         if(dataType == Schema.DisplayType.Integer  || dataType == Schema.DisplayType.Double) 
                         {
                             if(searchText1 == '')
                                    searchText11 = '<> null';
                               else
                                    searchText11 = '> '+searchText1;
                         }
                     }
                                        
                    if(!flag){
                    if(doesNotContains)
                    //soql = soql+' '+searchText11;
                    searchStr = searchStr + ' ' + searchText11;
                    else
                    //soql = soql+selectedField+' '+searchText11;
                    searchStr = searchStr +selectedField+' '+searchText11;
                    
                    flag = true;
                    }
                    else{
                    if(doesNotContains)
                    //soql = soql+' AND '+' '+searchText11;    
                    searchStr = searchStr +' AND '+' '+searchText11;
                    else
                    //soql = soql+' AND '+selectedField+' '+searchText11;    
                    searchStr = searchStr+' AND '+selectedField+' '+searchText11;    
                    }
                    
                    }
                    
                                
                if(listWithSelectOptions1.size() == count){
                  if(isSort)
                  {
                    isSearch = false;
                  } else
                  {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please select the search criteria'));                        
                  }
                }else{
                    if(!isSort)
                    {
                    
                    soql = soql + searchStr+ ' ORDER BY CourseSpecialty__c desc,Name asc LIMIT 999';
                    isSearch = true; 
                   accdel = Database.query(soql); 
                     soql = '';
                    for(UniversityCourse__c course:accdel)
                         {
                             wrapperAsset tmpWrap = new wrapperAsset();
                             tmpWrap.wrapperAst = course;
                             if(course.Account__r.Primary_Selling_Account_check__c == TRUE)
                             {
                                 tmpWrap.sellingAccount = course.Account__r.Name;
                                 tmpWrap.sellingAccId = course.Account__r.Id;
                             } else if(course.Account__r.Primary_Selling_Account__c != null || course.Account__r.Primary_Selling_Account__c != '')
                             {
                                 tmpWrap.sellingAccount = course.Account__r.Primary_Selling_Account__r.Name;
                                 tmpWrap.sellingAccId = course.Account__r.Primary_Selling_Account__r.Id;
                             }
                             wrapList.add(tmpWrap);
                             
                         }
                                }}
            }
            
        }
        catch(Exception e){
            
        }
        
        
    }
    
     public void getField() {
     options  = new List<SelectOption>();  
     options.add(new SelectOption('--None--','--None--'));
     options.add(new SelectOption('name','Catalog Code - Course Name'));
     options.add(new SelectOption('Primary_Selling_Account__c','Primary Selling Account'));
     options.add(new SelectOption('fall_enrollment__c','Fall'));
     options.add(new SelectOption('spring_enrollment__c','Spring'));
     options.add(new SelectOption('summer_enrollment__c','Summer'));
     options.add(new SelectOption('winter_enrollment__c','Winter'));
     options.add(new SelectOption('fall_front_list__c','Fall Front List'));
     options.add(new SelectOption('spring_front_list__c','Spring Front List'));
     options.add(new SelectOption('coursespecialty__c','Specialty'));
     options.add(new SelectOption('status__c','Status'));
     }
    public void getCondOperations() {
     condOptions  = new List<SelectOption>(); 
     condOptions.add(new SelectOption('--None--','--None--'));
     condOptions.add(new SelectOption('ASC','Ascending'));
     condOptions.add(new SelectOption('DESC','Descending'));
     }    
    public void sortData()
    {
        accdel = new List<UniversityCourse__c>();
        wrapList = new list<wrapperAsset>();
        Integer count = 0;
        isSort = true;
        isSearch = true;
        this.searchResults();
        if(isSearch == TRUE)
        {
                if(courseType == 'Inactive'){
                soql = 'select id,Account__r.Primary_Selling_Account__r.Id,Account__r.Primary_Selling_Account__r.Name,Account__r.Id,Account__r.Name,Account__r.Primary_Selling_Account__c,Account__r.Primary_Selling_Account_check__c, Name, Primary_Selling_Account__c, Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c,Fall_Front_List__c,Spring_Front_List__c,CourseSpecialty__c,Status__c from UniversityCourse__c where '+searchStr+' AND '+'Account__c = '+'\''+posId+'\''+' and Status__c IN '+'('+'\''+'Remove'+'\''+','+'\''+'Inactive'+'\''+')'+' ORDER BY ';
                } else
                {
                soql = 'select id,Account__r.Primary_Selling_Account__r.Id,Account__r.Primary_Selling_Account__r.Name,Account__r.Id,Account__r.Name,Account__r.Primary_Selling_Account__c,Account__r.Primary_Selling_Account_check__c, Name, Primary_Selling_Account__c, Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c,Fall_Front_List__c,Spring_Front_List__c,CourseSpecialty__c,Status__c from UniversityCourse__c where '+searchStr+' AND '+'Account__c = '+'\''+posId+'\''+' and Status__c= '+'\''+'Active'+'\''+' ORDER BY ';
                }
        } else
        {
                if(courseType == 'Inactive'){
                soql = 'select id,Account__r.Primary_Selling_Account__r.Id,Account__r.Primary_Selling_Account__r.Name,Account__r.Id,Account__r.Name,Account__r.Primary_Selling_Account__c,Account__r.Primary_Selling_Account_check__c, Name, Primary_Selling_Account__c, Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c,Fall_Front_List__c,Spring_Front_List__c,CourseSpecialty__c,Status__c from UniversityCourse__c where Account__c = '+'\''+posId+'\''+' and Status__c IN '+'('+'\''+'Remove'+'\''+','+'\''+'Inactive'+'\''+')'+' ORDER BY ';
                } else
                {
                soql = 'select id,Account__r.Primary_Selling_Account__r.Id,Account__r.Primary_Selling_Account__r.Name,Account__r.Id,Account__r.Name,Account__r.Primary_Selling_Account__c,Account__r.Primary_Selling_Account_check__c, Name, Primary_Selling_Account__c, Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c,Fall_Front_List__c,Spring_Front_List__c,CourseSpecialty__c,Status__c from UniversityCourse__c where Account__c = '+'\''+posId+'\''+' and Status__c= '+'\''+'Active'+'\''+' ORDER BY ';
                }
        }
        if(listWithSelectOptions.size()>0){
                Boolean flag = false;
               
        for(dynamicAddFilterSearch fObject : listWithSelectOptions){
            String sField = fObject.selField;
            
            String kField = fObject.serField;
            if(sField == '--None--' || kField == '--None--')
            {
                count = count + 1;
                continue;
            }
            
         if(!flag){
                    soql = soql + sField +' '+kField;
                    flag = true;
            }
         else
            {
                    soql = soql +','+ sField +' '+kField;         
            }
        }}
        
        if(listWithSelectOptions.size() == count){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please select the sort criteria'));
         }else
            {
                   soql = soql + ' LIMIT 999';
                   accdel = Database.query(soql); 
                   soql = '';
                //isSort = false;
                for(UniversityCourse__c course:accdel)
                     {
                         wrapperAsset tmpWrap = new wrapperAsset();
                         tmpWrap.wrapperAst = course;
                         if(course.Account__r.Primary_Selling_Account_check__c == TRUE)
                         {
                             tmpWrap.sellingAccount = course.Account__r.Name;
                             tmpWrap.sellingAccId = course.Account__r.Id;
                         } else if(course.Account__r.Primary_Selling_Account__c != null || course.Account__r.Primary_Selling_Account__c != '')
                         {
                             tmpWrap.sellingAccount = course.Account__r.Primary_Selling_Account__r.Name;
                             tmpWrap.sellingAccId = course.Account__r.Primary_Selling_Account__r.Id;
                         }
                         wrapList.add(tmpWrap);
                     }
                        }
        isSort = false;
    }
   public PageReference delCourse()
   {
       try{
           
       
     UniversityCourse__c toDel=new UniversityCourse__c(id=accId);
  
        delete todel;
  
        ViewData();
   
     return null;
       }
       catch(Exception e)
       {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'could not be deleted because it is associated with the another products'));
           nullQtyError = true; 
           return null;
       }
   }
   public void ViewData() {
       if(courseType == 'Inactive'){
                accdel = [select id,Account__r.Primary_Selling_Account__r.Id,Account__r.Primary_Selling_Account__r.Name,Account__r.Id,Account__r.Name,Account__r.Primary_Selling_Account__c,Account__r.Primary_Selling_Account_check__c, Name, Primary_Selling_Account__c, Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c,Fall_Front_List__c,Spring_Front_List__c,CourseSpecialty__c,Status__c from UniversityCourse__c where Account__c=:posId and Status__c IN ('Inactive','Remove') ORDER BY CourseSpecialty__c desc,Name asc LIMIT 999];
           } else
           {
                accdel = [select id,Account__r.Primary_Selling_Account__r.Id,Account__r.Primary_Selling_Account__r.Name,Account__r.Id,Account__r.Name,Account__r.Primary_Selling_Account__c,Account__r.Primary_Selling_Account_check__c, Name, Primary_Selling_Account__c, Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c,Fall_Front_List__c,Spring_Front_List__c,CourseSpecialty__c,Status__c from UniversityCourse__c where Account__c=:posId and Status__c = 'Active' ORDER BY CourseSpecialty__c desc,Name asc limit 999];               
           }
     for(UniversityCourse__c course:accdel)
     {
         wrapperAsset tmpWrap = new wrapperAsset();
         tmpWrap.wrapperAst = course;
         if(course.Account__r.Primary_Selling_Account_check__c == TRUE)
         {
             tmpWrap.sellingAccount = course.Account__r.Name;
             tmpWrap.sellingAccId = course.Account__r.Id;
         } else if(course.Account__r.Primary_Selling_Account__c != null || course.Account__r.Primary_Selling_Account__c != '')
         {
             tmpWrap.sellingAccount = course.Account__r.Primary_Selling_Account__r.Name;
             tmpWrap.sellingAccId = course.Account__r.Primary_Selling_Account__r.Id;
         }
         wrapList.add(tmpWrap);
     }
   }
    public class wrapperAsset {
        public UniversityCourse__c wrapperAst {get;set;}
        
        public wrapperAsset() {
            wrapperAst = new UniversityCourse__c();
        }
        public String sellingAccount {get;set;}
       public Id sellingAccId {get;set;}
    }
     /**
* Description : Inner class for values Added For Custom Filter Search
* @param String
* @return String
* @throws NA
**/
    public class dynamicAddFilterSearch {
        
        public List < SelectOption > searchType {
            get;
            set;
        }
        public List < SelectOption > searchKey {
            get;
            set;
        }
        public String selField {
            get;
            set;
        } 
        public String serField {
            get;
            set;
        } 
        public dynamicAddFilterSearch(List < SelectOption > searchfType, List < SelectOption > searchfKey, String selfField, String serfField) {
            searchType = searchfType;
            searchKey = searchfKey;
            selField = selfField;
            serField = serfField;
        }
    }
    /**
* Description : Retrieve Code (4 char) values from Multi - Select picklist
* @param String
* @return String
* @throws NA
**/
    public pageReference addNewFilter() {
        if (listWithSelectOptions.size() <= 4) {
            rowNumber = listWithSelectOptions.size();            
            listWithSelectOptions.add(new dynamicAddFilterSearch(options, condOptions, '', ''));
        } else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'You can have only five filter(s)');
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
    
     /**
* Description : Remove filter functionality 
* @param String
* @return String
* @throws NA
**/
    public void removeFilter() {
        listWithSelectOptions = removeFilterRow(rowToRemove, listWithSelectOptions);
    }
    
     /**
* Description : Remove selected filter functionality 
* @param String
* @return String
* @throws NA
**/
    public static List < dynamicAddFilterSearch > removeFilterRow(Integer rowToRemove, List < dynamicAddFilterSearch > listWithSelectOptionss) {
        listWithSelectOptionss.remove(rowToRemove);
        return listWithSelectOptionss;
    }
   
     public class dynamicAddFilterSearch1 {
        public List < SelectOption > productFieldLists {
            get;
            set;
        }
        public List < SelectOption > conditionLists {
            get;
            set;
        }
        public List < SelectOption > searchType {
            get;
            set;
        }
        public String searchText {
            get;
            set;
        } //{searchText ='Pearson';}
        public String searchTextDate {
            get;
            set;
        } //{searchText ='Pearson';} 
        public String selectedCondition {
            get;
            set;
        }
        public String selectedProductFilterValue {
            get;set;
        }
        public String selectedSearchType {
            get;
            set;
        } 
        public Boolean toDisplayLookup {
            get;
            set;
        } 
        public Integer rowNumber {
            get;
            set;
        } 
        public dynamicAddFilterSearch1(String s,List < SelectOption > productFieldLstWrapperCons, List < SelectOption > conditionLstWrapperCons, List < SelectOption > searchTypeLstWrapperCons, String selectedSearchTypeInput, String searchTextInput, String selectedConditionInput, boolean toDisplayLookupInput, Integer rowNumberInput) {
            productFieldLists = productFieldLstWrapperCons;
            conditionLists = conditionLstWrapperCons;
            //searchType = searchTypeLstWrapperCons;
            searchText = searchTextInput;
            //selectedCondition = selectedConditionInput;
            //selectedProductFilterValue = '';
            rowNumber = rowNumberInput;
            
                selectedCondition = selectedConditionInput;
            //if(loadtime == 1)     
            //selectedProductFilterValue = 'coursespecialty__c';
            //else
                selectedProductFilterValue = s; 
            
            //selectedSearchType = selectedSearchTypeInput;
            //toDisplayLookup = toDisplayLookupInput;
            
        }
    }
    /**
* Description : Retrieve Code (4 char) values from Multi - Select picklist
* @param String
* @return String
* @throws NA
**/
    public pageReference addNewFilter1() {
        toDisplayLookup = false;
        loadtime = loadtime+1;
        String searchText = ''; 
        if (listWithSelectOptions1.size() <= 4) {
            rowNumber1 = listWithSelectOptions1.size();
            getCondOperations1();
            if(loadtime == 1)
            listWithSelectOptions1.add(new dynamicAddFilterSearch1('coursespecialty__c',options, condOptions1, searchType, selectedSearchType, searchText,'like %%', toDisplayLookup, rowNumber1));
            else
            listWithSelectOptions1.add(new dynamicAddFilterSearch1('',options, condOptions1, searchType, selectedSearchType, searchText, selectedCondition, toDisplayLookup, rowNumber1));    
        } else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'You can have only five filter(s)');
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
    
     /**
* Description : Remove filter functionality 
* @param String
* @return String
* @throws NA
**/
    public void removeFilter1() {
        listWithSelectOptions1 = removeFilterRow1(rowToRemove1, listWithSelectOptions1);
    }
    
     /**
* Description : Remove selected filter functionality 
* @param String
* @return String
* @throws NA
**/
    public static List < dynamicAddFilterSearch1 > removeFilterRow1(Integer rowToRemove, List < dynamicAddFilterSearch1 > listWithSelectOptionss) {        listWithSelectOptionss.remove(rowToRemove);
        return listWithSelectOptionss;
    }
    
      /**
* Description : in Serach, if user selects date type then display search text of date etc
* @param String
* @return String
* @throws NA
**/
    public void changeSerachTextType() {
        try{
        selectedFilterbyValue = Apexpages.currentPage().getParameters().get('selectedFilterbyValue');
        selectedRow = Integer.valueOf(Apexpages.currentPage().getParameters().get('selectedRow'));
        for (dynamicAddFilterSearch1 wrapperClass: listWithSelectOptions1) {
            String selectedField = wrapperClass.selectedProductFilterValue;
            if(selectedField != '--None--' && selectedField != '' && selectedField != null){
            Schema.DisplayType dataType = fieldMap.get(selectedField).getDescribe().getType();
        if (( dataType == schema.DisplayType.INTEGER || dataType == schema.DisplayType.DOUBLE) && wrapperClass.rowNumber == selectedRow) {
            wrapperClass.toDisplayLookup = false;                    
            condOptions1 = new List < SelectOption > ();
            condOptions1.add(new SelectOption('=','equals'));
            condOptions1.add(new SelectOption('!=','not equal to'));
            condOptions1.add(new SelectOption('<','less than'));
            condOptions1.add(new SelectOption('>','greater than'));
            wrapperClass.conditionLists = condOptions1;
        }else if((dataType == SChema.DisplayType.BOOLEAN) && wrapperClass.rowNumber == selectedRow){
            wrapperClass.toDisplayLookup = true;
            condOptions1 = new List < SelectOption > ();
            condOptions1.add(new SelectOption('=','equals'));
            condOptions1.add(new SelectOption('!=','not equal to'));
            wrapperClass.conditionLists = condOptions1;
        }else if((dataType == SChema.DisplayType.PICKLIST) && wrapperClass.rowNumber == selectedRow){
            
            wrapperClass.toDisplayLookup = true;
            getCondOperations1();
            wrapperClass.conditionLists = condOptions1;
        }else if(wrapperClass.rowNumber == selectedRow){
            wrapperClass.toDisplayLookup = false; 
            getCondOperations1();
            wrapperClass.conditionLists = condOptions1;
        }
         }else if(wrapperClass.rowNumber == selectedRow){
            wrapperClass.toDisplayLookup = false; 
            getCondOperations1();
            wrapperClass.conditionLists = condOptions1;
                
            }
    }
        }
        catch(Exception e){
        }
    }
    
    
     /**
* Description : fetch status pikclist field values dynamiccally
* @param NA
* @return list
* @throws NA
**/
    public List < SelectOption > getStatuses() {
        String fieldName = Apexpages.currentPage().getParameters().get('fieldName');
        List < SelectOption > options = new List < SelectOption > ();
        Schema.DescribeFieldResult fieldResult = fieldMap.get(fieldName).getDescribe();
        
        if(fieldResult.getType() == SChema.DisplayType.PICKLIST){
        List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }        
        }
        else if(fieldResult.getType() == SChema.DisplayType.BOOLEAN){
             options.add(new SelectOption('True', 'True'));
             options.add(new SelectOption('False', 'False'));
        }
          return options;
    }
        
}