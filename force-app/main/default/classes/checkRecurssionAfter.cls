/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            checkRecurssionAfter.cls
   Description:     CheckRecurssionAfter Class
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  27/03/2015         1.0            Rashmi                 Added method runServiceIntOnce for R4 servicenow Integration
  22/8/2016          1.1            Payal                  Added apex class sharing context
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class checkRecurssionAfter {  
  
@TestVisible
    static Set<String> executedTriggerName = new Set<String>();
    static Set<Id> oupCaseId=new Set<Id>();
    static Set<Id> oflowCaseId=new Set<Id>();
    static Set<Id> oSensitiveCaseId=new Set<Id>();
    Static integer i=0;
    Static integer j=0;
    public static boolean run {
      get;
      set {
         if(value) executedTriggerName = new Set<String>();
      }
   } 

    public static boolean runOnce(String triggerName, String operation){    
        if(executedTriggerName.contains(triggerName+operation)){
            if(triggerName+operation=='PS_LeadAfterTriggerInsert'){
                return true;
            }

            i++;
            return false;
        }else{
            j++;
            executedTriggerName.add(triggerName+operation);            
            return true;
        }
  
    }
    public static boolean runServiceIntOnce(Set<Id> newCaseId,String event){
        if (event == 'Service Integration'){
        if (oupCaseId.containsAll(newCaseId))   
            return false;
         else{
             oupCaseId.addAll(newCaseId);
             return true;
         }
         }
         else if(event == 'Flow'){
        if (oflowCaseId.containsAll(newCaseId))   
            return false;
         else{
             oflowCaseId.addAll(newCaseId);
             return true;
         }         
         }
         else{
        if (oSensitiveCaseId.containsAll(newCaseId))   
            return false;
         else{
             oSensitiveCaseId.addAll(newCaseId);
             return true;
         }         
         }
    }      

}