/************************************************************************************************************
* Apex Class Name : PS_OrderUpdate
* Version             : 1.0 
* Created Date        : 20 Jul 2015
* Function            : Implementation of PS_OrderUpdateInterface for Orders
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Accenture                   20/Jul/2015            Initial version
* Rony Joseph                 28/Dec/2015            Updated Error Hndling Mechanism.  
* Rony Joseph                 04/Jul/2016            Implemented Logic according to SUS632 
* Abhishek Goel                19/Apr/2018           5913 - added US & CA market for updating order header status [USCAMarket]
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
public class PS_OrderUpdate implements PS_OrderUpdateInterface 
{
  private List<OrderItem> orderItemsUpdated;
  
  private Map<Id, Order> orderMap;
  public String USCAMarket;
 /*************************************************************************************************************
  * @Name        : initialize
  * @Description : used to initilize the class 
  * 
  * @Todo        : 
  * @Input       : inOrderItems: list of order to be validated
                   inOldOrderItem: old version of the order 
                   userContext: user as context for the validation process    
  * @Output      : N/A
  *************************************************************************************************************/
  public void initialize(List<OrderItem> inOrderItemsUpdated)
  {
    this.orderItemsUpdated = inOrderItemsUpdated;
          
    getOrderInfo();
  }
 
  public void updateOrders()
  {    
    List <Order> ordersToUpdate = new List<Order>();
    
    if(orderMap.isEmpty())
    {
      return;
    }
    //Gk get recordTypeID for R6 Orders
    //Retrieve the describe result for Order
    DescribeSObjectResult result = Schema.getGlobalDescribe().get('Order').getDescribe();
    //Generate a map of tokens for all the Record Types for the desired object
    Map<String,Schema.RecordTypeInfo> recordTypeInfo = result.getRecordTypeInfosByName();
    //Retrieve the record type id by name
    ID recordTypeIdglobRevOrd = recordTypeInfo.get('Global Revenue Order').getRecordTypeId(); 
    ID recordTypeIdglobSampOrd = recordTypeInfo.get('Global Sample Order').getRecordTypeId();

    for(String ordKey : orderMap.keySet()){
      Order ord = orderMap.get(ordKey);  
      Integer numCancelled = 0;
      Integer numComplete = 0;
      Integer numOnHold = 0;
      Integer numBooked = 0; 
      String ErpNumber = '';  
      //Gk R6 Added status
      Integer numClosed  = 0;
      Integer numClosedPartial = 0;
      Integer numShipped = 0;     
      for(OrderItem oi : ord.OrderItems){
        if(oi.Market__c == 'US' || oi.Market__c == 'CA')
        {
            USCAMarket = oi.Market__c;
        } 
        if(oi.status__c != null){
          if(!oi.status__c.equals(System.Label.Processed) && !oi.status__c.equals(System.Label.Cancelled) &&
             !oi.status__c.equals(System.Label.Shipped) && !oi.status__c.equals(System.Label.On_Hold)&&
             !oi.status__c.equals(System.Label.Booked) && !oi.status__c.equals(System.Label.Closed) && 
             !oi.status__c.equals(System.Label.ClosedPartial)){
            // break as there are OrderItems still awaiting an update with a status which means the order item is complete;
            break;              
          }
          else if(oi.status__c.equals(System.Label.On_hold))
          {
            ++numOnHold;
            //GK CRMSFDC-3603-Stamp ERP Order Number
            ErpNumber = oi.ERP_Order_Number__c;  
          }
          else if(oi.status__c.equals(System.Label.Booked))
          {
            ++numBooked;
            ErpNumber = oi.ERP_Order_Number__c;
          }
          else if(oi.status__c.equals(System.Label.Cancelled))
          {
            ++numCancelled;
            //GK CRMSFDC-3603-Stamp ERP Order Number
            ErpNumber = oi.ERP_Order_Number__c;    
          }
          else if(oi.status__c.equals(System.Label.Closed)) //Gk R6
          {
            ++numClosed;  
            ErpNumber = oi.ERP_Order_Number__c;          
          }
          else if(oi.status__c.equals(System.Label.ClosedPartial)) //Gk R6
          {
            ++numClosedPartial; 
            //GK CRMSFDC-3603-Stamp ERP Order Number
            ErpNumber = oi.ERP_Order_Number__c;  
          }
          else if(oi.status__c.equals(System.Label.Shipped)) //Gk R6
          {
            ++numShipped;               
            ErpNumber = oi.ERP_Order_Number__c;           
            ++numComplete;//existing  
          }  
          else{++numComplete;}
        }   //end OI status       
      }//end OI for loop
      system.debug('GK Order : order id = '+ ord.id +'Hold Counts:'+numOnHold+'Booked Counts:'+numBooked+'Can Counts:'+numCancelled+'Closed Counts:'+numClosed+'ClosedPar Counts:'+numClosedPartial+'Ship Counts:'+numShipped);
      //Gk Booked (All) – check for R6b record type ids
      if(numBooked == ord.OrderItems.size() && (ord.RecordTypeId == recordTypeIdglobRevOrd || ord.RecordTypeId == recordTypeIdglobSampOrd || USCAMarket == 'US' || USCAMarket == 'CA')) 
      {
        ord.status = System.Label.Booked;
        //GK- stamp only if value is present  
        if(ErpNumber != null && ErpNumber != '')  
          ord.OrderNumber__c = ErpNumber;
        ordersToUpdate.add(ord);
      } //Gk Shipped (All) – only for R6
      else if(numShipped == ord.OrderItems.size() && (ord.RecordTypeId == recordTypeIdglobRevOrd || ord.RecordTypeId == recordTypeIdglobSampOrd || USCAMarket == 'US' || USCAMarket == 'CA')) 
      {
        ord.status = System.Label.Shipped;
        //GK- stamp only if value is present  
        if(ErpNumber != null && ErpNumber != '')    
          ord.OrderNumber__c = ErpNumber;
        ordersToUpdate.add(ord);
      }  
      else if(numCancelled == ord.OrderItems.size())// if all of the order items are cancelled set the order status to cancelled
      {
        ord.status = System.Label.Cancelled;
        //GK CRMSFDC-3603-Stamp ERP Order Number
        //GK- stamp only if value is present  
        if(ErpNumber != null && ErpNumber != '')    
          ord.OrderNumber__c = ErpNumber;
        ordersToUpdate.add(ord);
      } //Gk On Hold (if any 1) – need to check for R6 record type ids     
      else if(numOnHold > 0 && (ord.RecordTypeId == recordTypeIdglobRevOrd || ord.RecordTypeId == recordTypeIdglobSampOrd || USCAMarket == 'US' || USCAMarket == 'CA'))
      {
        ord.status = System.Label.On_Hold;
        //GK CRMSFDC-3603-Stamp ERP Order Number 
        //GK- stamp only if value is present  
        if(ErpNumber != null && ErpNumber != '')   
          ord.OrderNumber__c = ErpNumber;
        ordersToUpdate.add(ord);
      }//Gk R6- Closed or Closed Partial (All of either status or mix of two) – only for R6 Closed
      else if((numClosed  + numClosedPartial + numCancelled == ord.OrderItems.size())  
                && (ord.RecordTypeId == recordTypeIdglobRevOrd || ord.RecordTypeId == recordTypeIdglobSampOrd || USCAMarket == 'US' || USCAMarket == 'CA')) 
      {
        ord.status = System.Label.Closed;
        //GK CRMSFDC-3603-Stamp ERP Order Number
        //GK- stamp only if value is present  
        if(ErpNumber != null && ErpNumber != '')    
          ord.OrderNumber__c = ErpNumber;
        ordersToUpdate.add(ord);
      } //Gk Booked (If any 1 and none on hold) – only for R6
      else if(numBooked > 0  && numOnHold == 0  
                && (ord.RecordTypeId == recordTypeIdglobRevOrd || ord.RecordTypeId == recordTypeIdglobSampOrd || USCAMarket == 'US' || USCAMarket == 'CA')) 
      {
        ord.status = System.Label.Booked;
        //GK- stamp only if value is present  
        if(ErpNumber != null && ErpNumber != '')    
          ord.OrderNumber__c = ErpNumber;
        ordersToUpdate.add(ord);
      }//Gk Shipped (if any 1 and none on hold or booked) – only for R6
      else if(numShipped > 0  &&  numOnHold == 0  && numBooked == 0
                && (ord.RecordTypeId == recordTypeIdglobRevOrd || ord.RecordTypeId == recordTypeIdglobSampOrd || USCAMarket == 'US' || USCAMarket == 'CA')) 
      {
        ord.status = System.Label.Shipped;
        //GK CRMSFDC-3603-Stamp ERP Order Number
        //GK- stamp only if value is present  
        if(ErpNumber != null && ErpNumber != '')    
          ord.OrderNumber__c = ErpNumber;
        ordersToUpdate.add(ord);
      }    
       //Gk -bypass for r6 orders.   
      else if(numCancelled + numComplete == ord.OrderItems.size() && !(ord.RecordTypeId == recordTypeIdglobRevOrd || ord.RecordTypeId == recordTypeIdglobSampOrd))
      {
       ord.status = System.Label.Filled;
     	
        ordersToUpdate.add(ord);
      } 
        System.debug('GK Order : order id = '+ ord.id + ' Order status = '+ord.status);
    }//end ord loop
    
    if(ordersToUpdate != null && ordersToUpdate.size() > 0)
    {
         Database.SaveResult[] lstOrderUpdateResult =Database.update(ordersToUpdate,false);
         if (lstOrderUpdateResult!= null){
                inserterrorlog(lstOrderUpdateResult,'PS_OrderUpdate Update');
         }
    }
  }  
    public void inserterrorlog(Database.SaveResult[] DMLResult,String InterfaceName)
    {
        List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
        for (Database.SaveResult sr : DMLResult) {
            String ErrMsg='';
            if (!sr.isSuccess() || Test.isRunningTest()){
                PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                errlogger.InterfaceName__c= InterfaceName;
                errlogger.ApexClassName__c='PS_OrderUpdate';
                errlogger.CallingMethod__c='updateOrders';
                errlogger.UserLogin__c=UserInfo.getUserName(); 
                errlogger.recordid__c=sr.getId();
                for(Database.Error err : sr.getErrors()) 
                    ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage() + ': '+err.getFields(); 
                errlogger.ExceptionMessage__c=  ErrMsg;  
                errloggerlist.add(errlogger);    
            }
        }
        if(errloggerlist.size()>0){insert errloggerlist;}
    }   
    
  private void getOrderInfo()
  {
    if(orderMap == null)
    {
      orderMap = new Map<Id, Order>();
    }
    
    Set <Id> orderIds = new Set<Id>(); 
      
    for(OrderItem oi : orderItemsUpdated)
    {
      if(!orderIds.contains(oi.OrderId))
      {
        orderIds.add(oi.OrderId);
      }
    }  
    
    List<Order> orders = new  List<Order>();     
      
    if(!orderIds.isEmpty())
    { //Gk Pulled recortype for R6
      orders = [SELECT o.Status,recordTypeid, o.market__c, (SELECT Status__c,ERP_Order_Number__c,ERP_Order_Line_Number__c,Market__c  FROM OrderItems) FROM Order o WHERE Id IN :orderIds];
        
    }
      
    if(!orders.isEmpty())
    {
      orderMap.putAll(new Map<Id, Order>(orders));
    }     
  }        
}