/************************************************************************************************************
* Apex Interface Name : PS_INT_IntegrationRequestControllerTest
* Version             : 1.1 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------   
* KP                3/18/2016               Updateded verifyCreateIntegrationRequestOrder  for 101 SOQL issue
* Rohit             5/03/2016               Updated verifySubmisionValidationForCases Method to increase code coverge
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

@isTest (seeAllData=true)
public with sharing class PS_INT_IntegrationRequestControllerTest 
{    
  /*************************************************************************************************************
  * Name        : verifySubmisionValidation
  * Description : Verify the submissionValidation method   
  * Input       :  
  * Output      :  
  *************************************************************************************************************/
  static testMethod void verifySubmisionValidation()
  {  
   
    List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId(),1);
    insert usrLst;
    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
    insert byp;    
    System.runas(usrLst[0]){
        
    Account acc = (Account)TestClassAutomation.createSObject('Account');
    acc.Name = 'Test';
    acc.Pearson_Campus__c =true;
    acc.IsCreatedFromLead__c = true;
    insert acc; 
    
    Account_Correlation__C ac = new Account_Correlation__C(Account__C = acc.Id, External_ID_Name__c = 'eVision Learner Number', External_ID__c = 'External ID');
    insert ac; 
     
    contact con = (Contact)TestClassAutomation.createSObject('Contact');
    con.Lastname= 'testcon';
    con.Firstname= 'testcon1';
    con.MobilePhone = '9999'; 
    con.Preferred_Address__c = 'Other Address';
    con.OtherCountry  = 'India';
    con.OtherStreet = 'Test';
    con.OtherCity  = 'Test';
    con.OtherPostalCode  = '123456';
    con.Salutation ='Mr.';
    con.First_Language__c = 'English';
    con.accountid=acc.id;
    insert con;     
    
    Pricebook2 priceBook = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
    priceBook.IsActive = true;
    update priceBook;
    
    Opportunity opp = (Opportunity)TestClassAutomation.createSObject('Opportunity');
    opp.AccountId = acc.Id;
    opp.CurrencyIsoCode = 'GBP';
    opp.Lost_Reason_Other__c = 'XXX';
    opp.Academic_Vetting_Status__c = 'XXXXX';
    //CP 07/25/2018 For ZA: opp.Qualification_Campus__c = acc.Id;
    opp.Type = 'New Business';
    opp.StageName = 'Negotiation';
    opp.Pricebook2Id = priceBook.Id;
    opp.Enrolment_Type__c = 'Provisional Enrolment';
    insert opp;
    
    /* CR-02949 - Commented the Apttus to Non-Apttus changes by Vinoth
    Apttus_Proposal__Proposal__c propObj = TestDataFactory.createProposal(opp,acc,'Proposal');
    insert propObj;
    */
    
    Opportunity oldopp = (Opportunity)TestClassAutomation.createSObject('Opportunity');
    oldopp.AccountId = acc.Id;
    oldopp.CurrencyIsoCode = 'GBP';
    oldopp.Lost_Reason_Other__c = 'XXX';
    oldopp.Academic_Vetting_Status__c = 'XXXXX';
    //CP 07/25/2018 For ZA: oldopp.Qualification_Campus__c = acc.Id;
    oldopp.Type = 'New Business';
    oldopp.StageName = 'Negotiation';
    oldopp.Pricebook2Id = priceBook.Id;
    oldopp.Enrolment_Type__c = 'Provisional Enrolment';
    oldopp.Initial_stage_change_to_Negotiation__c=System.today();
    insert oldopp;
    
    /* CR-02949 - Commented the Apttus to Non-Apttus changes by Vinoth
    Apttus_Proposal__Proposal__c propObj1 = TestDataFactory.createProposal(oldopp,acc,'Proposal');
    insert propObj1;
    */
    /*
     Opportunity oldopp1 = (Opportunity)TestClassAutomation.createSObject('Opportunity');
    oldopp1.AccountId = acc.Id;
    oldopp1.CurrencyIsoCode = 'GBP';
    oldopp1.Lost_Reason_Other__c = 'XXX';
    oldopp1.Academic_Vetting_Status__c = 'XXXXX';
    //CP 07/25/2018 For ZA: oldopp1.Qualification_Campus__c = acc.Id;
    oldopp1.Type = 'New Business';
    oldopp1.StageName = 'Negotiation';
    oldopp1.Pricebook2Id = priceBook.Id;
    oldopp1.Enrolment_Type__c = 'Provisional Enrolment';
    insert oldopp1;
    */
      
    Product2 prod = (Product2)TestClassAutomation.createSObject('Product2');
    insert prod;        
        
    PriceBookEntry sPriceBookEntry = (PriceBookEntry)TestClassAutomation.createSObject('PriceBookEntry');
    sPriceBookEntry.IsActive = true;
    sPriceBookEntry.Product2Id = prod.Id;
    sPriceBookEntry.Pricebook2Id = priceBook.Id;
    sPriceBookEntry.UnitPrice = 34.95;
    sPriceBookEntry.CurrencyIsoCode = 'GBP';
    insert sPriceBookEntry;

    /* CR-02949 - Commented the Apttus to Non-Apttus changes by Vinoth
    Apttus_Proposal__Proposal_Line_Item__c apptLineObj = new Apttus_Proposal__Proposal_Line_Item__c();
    apptLineObj.Apttus_Proposal__Product__c = prod.id;
    apptLineObj.Apttus_QPConfig__OptionId__c = prod.id;
    apptLineObj.Apttus_Proposal__Proposal__c = propObj.id;
    insert apptLineObj;
    */
    /*OpportunityLineItem oli1 = new OpportunityLineItem();
    oli1.OpportunityId = opp.Id;
    oli1.PricebookEntryId = sPriceBookEntry.Id;
    oli1.TotalPrice = 200;
    oli1.Quantity = 1;
    insert oli1;

    OpportunityLineItem oli2 = new OpportunityLineItem();
    oli2.OpportunityId = oldopp.Id;
    oli2.PricebookEntryId = sPriceBookEntry.Id;
    oli2.TotalPrice = 200;
    oli2.Quantity = 1;
    insert oli2;*/
    
    list<OpportunityContactRole>ocrlist = new list<OpportunityContactRole>();
    OpportunityContactRole ocr = new OpportunityContactRole();
    ocr.ContactId = con.id;
    ocr.Role = 'Business User';
    ocr.IsPrimary = True;
    ocr.OpportunityId = opp.id;     
    ocrlist.add(ocr);
     
    OpportunityContactRole ocr1 = new OpportunityContactRole();
    ocr1.ContactId = con.id;
    ocr1.Role = 'Primary Sales Contact';
    ocr1.IsPrimary = True;
    ocr1.OpportunityId = opp.id;
    ocrlist.add(ocr1);
    insert ocrlist;    
    List <Opportunity> opps = new List<Opportunity>();
    Map<Id, Opportunity> oldOpps = new Map<Id, Opportunity>();
   // opps.add(opp);
   //oldopps.put(opp.id, oldopp);
    
    Quote quo = new Quote();
       quo.Name= 'Test Quote';   
       quo.First_Payment_Date__c=System.today();
       quo.Payment_Period_In_Month__c='3';
       quo.Deposit__c=1000.00;
       quo.Payment_Type__c='Monthly Payment';
       quo.Opportunityid=opp.id;
       quo.Pricebook2Id=priceBook.Id;
       quo.Registration_Fee__c=800;
       quo.Total_Early_Bird_Securing_Fee_Payments__c=1700;
       //quo.IsSyncing=true;
       quo.Status='Approved';
       
       //test.startTest();
       
       insert quo;
       
       opp.SyncedQuoteId = quo.Id;
       update opp;
       opps.add(opp);
       oldopps.put(opp.id, oldopp);
   
        List<QuoteLineItem> qltList = new List<QuoteLineItem>();   
        QuoteLineItem qliliner = new QuoteLineItem(Product2Id=prod.id,Outside_Module__c=true,
                                                       QuoteId=quo.id,PriceBookEntryID=sPriceBookEntry.Id,Quantity=4, 
                                                       UnitPrice =50);
                                                       
        //test.startTest();
        insert qliliner;
          
 
    //test.startTest();
    
    PS_INT_IntegrationRequestController.submissionValidation(opps, oldOpps);
    
    con.Passport_Number__c = '12345';
    con.National_Identity_Number__c = '9412055708083';
    update con;
    ocr.ContactId = con.id;
    update ocr;
    PS_INT_IntegrationRequestController.submissionValidation(opps, oldOpps);
  
    opp.Type = 'New Business';
    opp.StageName = 'Closed';
    opp.CloseDate = System.Today();
    update opp;
  
    con.MobilePhone = null;
    con.Phone = '9999';
    con.Salutation ='Mr.';
    con.First_Language__c = 'English';
    con.MobilePhone = '9999'; 
    update con;
    PS_INT_IntegrationRequestController.submissionValidation(opps, oldOpps);
    
    opp.Type = 'Amendment';
    opp.StageName = 'Closed';
    opp.CloseDate = System.Today();
    
    test.startTest();
    
    update opp;
    
    con.Phone = null;
    con.Home_Phone__c = '9999';
    update con;
    PS_INT_IntegrationRequestController.submissionValidation(opps, oldOpps);
    
    PS_INT_IntegrationRequestController.createIntegrationRequestOpp(opps, oldopps);
    
    opp.recordtypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('D2L').getRecordTypeId();
    update opp;    
    PS_INT_IntegrationRequestController.createIntegrationRequestOpp(opps, oldopps);
    
    opp.Type = 'New Business';
    opp.Academic_Vetting_Status__c = 'Vetted - Approved';
    opp.StageName = 'Lost';
    opp.Lost_Reason__c = 'Other';
    update opp;
    PS_INT_IntegrationRequestController.createIntegrationRequestOpp(opps, oldopps);
    
    opp.StageName = 'Invoiced';
    update opp;
    PS_INT_IntegrationRequestController.createIntegrationRequestOpp(opps, oldopps);
    
    test.stopTest();
    }
  }
  
  /*************************************************************************************************************
  * Name        : verifySubmisionValidationForCases
  * Description : Verify the submissionValidationForCases method   
  * Input       : 
  * Output      : 
  *************************************************************************************************************/
  static testMethod void verifySubmisionValidationForCases()
  {  
    List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId(),1);
    insert usrLst;
     List <Case> cases = new List<Case>();
     List<Case> generatedCases  = new List<Case>();

    system.runas(usrLst[0]){
    generatedCases = generateTestCases(1, false);  
    system.debug('@@CaseList1--->'+generatedCases.size());  
    cases.add(generatedCases.get(0));
    
    
    
    test.startTest();   
    PS_INT_IntegrationRequestController.submissionValidation(cases);    
    // code is commented which is here    
    test.stopTest();
    }
  }
  
  /*************************************************************************************************************
  * Name        : verifyCreateIntegrationRequestOrder
  * Description : Verify the createIntegrationRequestOrder method   
  * Input       : 
  * Output      : 
  *************************************************************************************************************/
  
static testMethod void verifyCreateIntegrationRequestOrder()
  { 
    
    List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId(),2);
    insert usrLst;
    usrLst[1].Market__c = 'UK';
    update usrLst[1];
    OrderItem OrdItm = new OrderItem();
/*
    Integration_Framework_Configuration__c Intgrconf = new Integration_Framework_Configuration__c();
    Intgrconf = [ SELECT id,Name,EVENT__c ,Sub_Event__c,MARKET__c,BU__c,LoB__c  FROM Integration_Framework_Configuration__c where EVENT__c ='New Order' limit 1];
*/    
    
    List <PermissionSet> permList = [SELECT Id FROM PermissionSet WHERE name = 'Pearson_Backend_Order_Creation'];  
    PermissionSetAssignment psa = new PermissionSetAssignment();
    psa.AssigneeId = usrLst[1].Id;
    psa.PermissionSetId = permList[0].Id;
    insert psa;
    
    permList = [SELECT Id FROM PermissionSet WHERE name = 'Pearson_Global_Backend_Revenue'];  
    psa = new PermissionSetAssignment();
    psa.AssigneeId = usrLst[1].Id;
    psa.PermissionSetId = permList[0].Id;
    insert psa;
    
    permList = [SELECT Id FROM PermissionSet WHERE name = 'Pearson_Manage_Orders'];  
    psa = new PermissionSetAssignment();
    psa.AssigneeId = usrLst[1].Id;
    psa.PermissionSetId = permList[0].Id;
    insert psa;
    test.startTest();     
    System.runas(usrLst[1])
    {
    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[1].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
    insert byp;    
    
    Account acc = (Account)TestClassAutomation.createSObject('Account');
    acc.Name = 'Test';
    acc.IsCreatedFromLead__c=true;
    insert acc;
    
    Integration_Framework_Configuration__c testRec = new Integration_Framework_Configuration__c(Name='test',Event__c='Modify Order Line Item',LoB__c='HE',Market__c='UK',Geo__c='NA');
    insert testRec; 
    
    Integration_Framework_Configuration__c testRecuk = new Integration_Framework_Configuration__c(Name='testuk',Event__c='New Order',LoB__c='HE',Market__c='UK',Geo__c='Core',BU__c = 'ERPI');
    insert testRecuk; 

    Integration_Framework_Configuration__c testRecus = new Integration_Framework_Configuration__c(Name='testus',Event__c='New Order',LoB__c='HE',Market__c='US',Geo__c='Core',BU__c ='Rights & Royalties');
    insert testRecus;
    
    Integration_Framework_Configuration__c testRecusscl = new Integration_Framework_Configuration__c(Name='testussch',Event__c='New Order',LoB__c='Schools',Market__c='US',Geo__c='Core',BU__c ='Rights & Royalties');
    insert testRecusscl;
        
    contact con = (Contact)TestClassAutomation.createSObject('Contact');
    con.Lastname= 'testcon';
    con.Firstname= 'testcon1';
    con.MobilePhone = '9999';  
    con.Preferred_Address__c = 'Other Address';
    con.OtherCountry  = 'India';
    con.OtherStreet = 'Test';
    con.OtherCity  = 'Test';
    con.OtherPostalCode  = '123456';
    con.accountid=acc.id;
    con.Contact_Status__c =True;
    insert con;   
    
    Pricebook2 priceBook = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
    
    Opportunity opp = (Opportunity)TestClassAutomation.createSObject('Opportunity');
    opp.AccountId = acc.Id;
    opp.CurrencyIsoCode = 'GBP';
    opp.Lost_Reason_Other__c = 'XXX';
    opp.Pricebook2Id = priceBook.Id;
    opp.Enrolment_Type__c = 'Provisional Enrolment';
    insert opp;
    
    Product2 prod = (Product2)TestClassAutomation.createSObject('Product2');
    insert prod;

   /* Pricebook2 priceBook = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
    priceBook.IsActive = true;
    update priceBook;*/
        
    PriceBookEntry sPriceBookEntry = (PriceBookEntry)TestClassAutomation.createSObject('PriceBookEntry');
    sPriceBookEntry.IsActive = true;
    sPriceBookEntry.Product2Id = prod.Id;
    sPriceBookEntry.Pricebook2Id = priceBook.Id;
    //sPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();
    sPriceBookEntry.UnitPrice = 34.95;
    sPriceBookEntry.CurrencyIsoCode = 'GBP';
    insert sPriceBookEntry;
    
   /* OpportunityLineItem oli1 = new OpportunityLineItem();
    oli1.OpportunityId = opp.Id;
    oli1.PricebookEntryId = sPriceBookEntry.Id;
    oli1.TotalPrice = 200;
    oli1.Quantity = 1;
            
    insert oli1;*/
    
    Id revenueOrderRTID = PS_Util.fetchRecordTypeByName(Order.SobjectType,PS_Constants.ORDER_REVENUE_ORDER);
    order ord = new order();
    ord.OpportunityId=opp.Id;
    ord.Accountid = acc.Id;
    ord.EffectiveDate = system.today();
    ord.status = 'Open';
    ord.Pricebook2Id = Test.getStandardPricebookId();
    ord.CurrencyIsoCode = 'GBP';
    ord.Market__c = 'UK';
    ord.Packing_Instructions__c = 'Packing Instructions';
    ord.Shipping_Instructions__c = 'Shipping instructions';
    ord.ShippingStreet = 'Shiping Street';
    ord.ShippingCity = 'Shiping City';
    ord.shippingCountry = 'Australia';
    ord.shippingState = 'Victoria';
    ord.shippingPostalCode = '1234';
    ord.RecordTypeId = revenueOrderRTID;
    ord.Salesperson__c = usrLst[1].id;
    insert ord;
    
    List<OrderItem> oilist=new List<OrderItem>();
    Orderitem oi = new OrderItem();
    oi.orderid=ord.id;
    oi.Shipped_Product__c = prod.id;
    oi.Quantity = 6;
    oi.pricebookentryid= sPriceBookEntry.id;
    oi.unitprice = 0.00;  
    oi.status__c='New';
    insert oi;
    OrdItm = [select id,market__c,status__c from orderitem where id=: oi.id];
    
  
    Orderitem oldoi = new OrderItem();
    oldoi.orderid=ord.id;
    oldoi.Shipped_Product__c = prod.id;
    oldoi.Quantity = 6;
    oldoi.pricebookentryid= sPriceBookEntry.id;
    oldoi.unitprice = 0.00;
    oldoi.Status__c = 'New';
    oilist.add(oldoi); 
    //insert oilist;    
  
   //createIntegrationRequestOrderLine 
   //for cancelled status
   Map<Id,orderItem> OldOIMap = new Map<Id,orderItem>();
   List<OrderItem>NewOrderItm = new  List<OrderItem>();
   OldOIMap.put(OrdItm.id,oldoi);
   OrdItm.Status__c = 'Cancelled';
   update OrdItm;
   NewOrderItm.add(OrdItm);  
   
    List<Order> orders = new List<Order>();
    orders.add(ord);   
    Map<Id, Order> oldorders = new Map<Id, Order>();
    oldorders.put(ord.id,ord);        

    
    PS_INT_IntegrationRequestController.createIntegrationRequestOrder(orders,oldorders);
    PS_INT_IntegrationRequestController.createIntegrationRequestOrderLine(NewOrderItm,OldOIMap);
    //Approved status
    NewOrderItm.clear();
    OldOIMap.clear();
    
    OldOIMap.put(OrdItm.id,oldoi);
    OrdItm.Status__c = 'Approved';
    update OrdItm;
    OrdItm = [select id,market__c,status__c from orderitem where id=: oi.id];
    NewOrderItm.add(OrdItm);
     PS_INT_IntegrationRequestController.createIntegrationRequestOrderLine(NewOrderItm,OldOIMap); 
     
     //Resend Request
     NewOrderItm.clear();
    OldOIMap.clear();
    OldOIMap.put(OrdItm.id,oldoi);
    OrdItm.Status__c = 'Resend Request';
    update OrdItm;
    OrdItm = [select id,market__c,status__c from orderitem where id=: oi.id];
    NewOrderItm.add(OrdItm);
     PS_INT_IntegrationRequestController.createIntegrationRequestOrderLine(NewOrderItm,OldOIMap); 
             
    test.stopTest();
    

/*

    Id GRLRTID = PS_Util.fetchRecordTypeByName(Order.SobjectType,PS_Constants.ORDER_GLOBAL_RIGHTS_SALES_CONTRACT);
    order ord112 = new order();
    ord112.OpportunityId=opp.Id;
    ord112.Accountid = acc.Id;
    ord112.EffectiveDate = system.today();
    ord112.status = 'Open';
    ord112.Pricebook2Id = Test.getStandardPricebookId();
    ord112.CurrencyIsoCode = 'USD';
    ord112.Market__c = 'US';
    ord112.Packing_Instructions__c = 'Packing Instructions';
    ord112.Shipping_Instructions__c = 'Shipping instructions';
    ord112.ShippingStreet = 'Shiping Street';
    ord112.ShippingCity = 'Shiping City';
    ord112.shippingCountry = 'Australia';
    ord112.shippingState = 'Victoria';
    ord112.shippingPostalCode = '1234';
    ord112.RecordTypeId = GRLRTID ;
    ord112.Salesperson__c = usrLst[3].id;
    ord112.Line_of_Business__c = 'Higher Ed';
    ord112.Business_Unit__c ='Rights & Royalties';
    
    insert ord112;
     
    List<Order> orders11 = new List<Order>();
    orders11.add(ord112);
    Map<Id, Order> oldorders11 = new Map<Id, Order>();
    oldorders11.put(ord112.id,ord112); 
    PS_INT_IntegrationRequestController.createIntegrationRequestOrder(orders11,null);


    order ord112sch = new order();
    ord112sch.OpportunityId=opp.Id;
    ord112sch.Accountid = acc.Id;
    ord112sch.EffectiveDate = system.today();
    ord112sch.status = 'Open';
    ord112sch.Pricebook2Id = Test.getStandardPricebookId();
    ord112sch.CurrencyIsoCode = 'USD';
    ord112sch.Market__c = 'US';
    ord112sch.Packing_Instructions__c = 'Packing Instructions';
    ord112sch.Shipping_Instructions__c = 'Shipping instructions';
    ord112sch.ShippingStreet = 'Shiping Street';
    ord112sch.ShippingCity = 'Shiping City';
    ord112sch.shippingCountry = 'Australia';
    ord112sch.shippingState = 'Victoria';
    ord112sch.shippingPostalCode = '1234';
    ord112sch.RecordTypeId = sampleOrderRTID ;
    ord112sch.Salesperson__c = usrLst[3].id;
    ord112sch.Line_of_Business__c = 'Schools';
    ord112sch.Business_Unit__c ='Rights & Royalties';
    
    insert ord112sch;
 
    List<Order> ord112sch1= new List<Order>();
    ord112sch1.add(ord112sch);
    Map<Id, Order> oldorders11sch = new Map<Id, Order>();
    oldorders11sch.put(ord112sch.id,ord112sch); 
    PS_INT_IntegrationRequestController.createIntegrationRequestOrder(ord112sch1,null);
  */  
    }
  }
  
  
  static testMethod void verifyCreateIntegrationRequestSampleOrder()
  {  
    
    List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId(),3);
    insert usrLst;
    usrLst[1].Market__c = 'UK';
    update usrLst[1];
    OrderItem OrdItm = new OrderItem();
/*
    Integration_Framework_Configuration__c Intgrconf = new Integration_Framework_Configuration__c();
    Intgrconf = [ SELECT id,Name,EVENT__c ,Sub_Event__c,MARKET__c,BU__c,LoB__c  FROM Integration_Framework_Configuration__c where EVENT__c ='New Order' limit 1];
*/    
    
    List <PermissionSet> permList = [SELECT Id FROM PermissionSet WHERE name = 'Pearson_Backend_Order_Creation'];  
    PermissionSetAssignment psa = new PermissionSetAssignment();
    psa.AssigneeId = usrLst[1].Id;
    psa.PermissionSetId = permList[0].Id;
    insert psa;
    
    permList = [SELECT Id FROM PermissionSet WHERE name = 'Pearson_Global_Backend_Revenue'];  
    psa = new PermissionSetAssignment();
    psa.AssigneeId = usrLst[1].Id;
    psa.PermissionSetId = permList[0].Id;
    insert psa;
    
    permList = [SELECT Id FROM PermissionSet WHERE name = 'Pearson_Manage_Orders'];  
    psa = new PermissionSetAssignment();
    psa.AssigneeId = usrLst[1].Id;
    psa.PermissionSetId = permList[0].Id;
    insert psa;
    test.startTest();    
    System.runas(usrLst[1])
    {
    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[1].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
    insert byp;    
    
    Account acc = (Account)TestClassAutomation.createSObject('Account');
    acc.Name = 'Test';
    acc.IsCreatedFromLead__c=true;
    insert acc;
    
    Integration_Framework_Configuration__c testRec = new Integration_Framework_Configuration__c(Name='test',Event__c='Modify Order Line Item',LoB__c='HE',Market__c='UK',Geo__c='NA');
    insert testRec; 
    
    Integration_Framework_Configuration__c testRecuk = new Integration_Framework_Configuration__c(Name='testuk',Event__c='New Order',LoB__c='HE',Market__c='UK',Geo__c='Core',BU__c = 'ERPI');
    insert testRecuk; 

    Integration_Framework_Configuration__c testRecus = new Integration_Framework_Configuration__c(Name='testus',Event__c='New Order',LoB__c='HE',Market__c='US',Geo__c='Core',BU__c ='Rights & Royalties');
    insert testRecus;
    
    Integration_Framework_Configuration__c testRecusscl = new Integration_Framework_Configuration__c(Name='testussch',Event__c='New Order',LoB__c='Schools',Market__c='US',Geo__c='Core',BU__c ='Rights & Royalties');
    insert testRecusscl;
        
    contact con = (Contact)TestClassAutomation.createSObject('Contact');
    con.Lastname= 'testcon';
    con.Firstname= 'testcon1';
    con.MobilePhone = '9999';  
    con.Preferred_Address__c = 'Other Address';
    con.OtherCountry  = 'India';
    con.OtherStreet = 'Test';
    con.OtherCity  = 'Test';
    con.OtherPostalCode  = '123456';
    con.accountid=acc.id;
    insert con;   
    
    Pricebook2 priceBook = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
    
    Opportunity opp = (Opportunity)TestClassAutomation.createSObject('Opportunity');
    opp.AccountId = acc.Id;
    opp.CurrencyIsoCode = 'GBP';
    opp.Lost_Reason_Other__c = 'XXX';
    opp.Pricebook2Id = priceBook.Id;
    opp.Enrolment_Type__c = 'Provisional Enrolment';
    insert opp;
    
    Product2 prod = (Product2)TestClassAutomation.createSObject('Product2');
    insert prod;

   /* Pricebook2 priceBook = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
    priceBook.IsActive = true;
    update priceBook;*/
        
    PriceBookEntry sPriceBookEntry = (PriceBookEntry)TestClassAutomation.createSObject('PriceBookEntry');
    sPriceBookEntry.IsActive = true;
    sPriceBookEntry.Product2Id = prod.Id;
    sPriceBookEntry.Pricebook2Id = priceBook.Id;
    //sPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();
    sPriceBookEntry.UnitPrice = 34.95;
    sPriceBookEntry.CurrencyIsoCode = 'GBP';
    insert sPriceBookEntry;
    
    /*OpportunityLineItem oli1 = new OpportunityLineItem();
    oli1.OpportunityId = opp.Id;
    oli1.PricebookEntryId = sPriceBookEntry.Id;
    oli1.TotalPrice = 200;
    oli1.Quantity = 1;
            
    insert oli1;*/
    
    Id sampleOrderRTID = PS_Util.fetchRecordTypeByName(Order.SobjectType,PS_Constants.ORDER_SAMPLE_ORDER);
    order ord1 = new order();
    ord1.OpportunityId=opp.Id;
    ord1.Accountid = acc.Id;
    ord1.EffectiveDate = system.today();
    ord1.status = 'Open';
    ord1.Pricebook2Id = Test.getStandardPricebookId();
    ord1.CurrencyIsoCode = 'GBP';
    ord1.Market__c = 'UK';
    ord1.Packing_Instructions__c = 'Packing Instructions';
    ord1.Shipping_Instructions__c = 'Shipping instructions';
    ord1.ShippingStreet = 'Shiping Street';
    ord1.ShippingCity = 'Shiping City';
    ord1.shippingCountry = 'Australia';
    ord1.shippingState = 'Victoria';
    ord1.shippingPostalCode = '1234';
    ord1.RecordTypeId = sampleOrderRTID;
    ord1.Salesperson__c = usrLst[2].id;
    
    insert ord1;
        
    List<Order> orders1 = new List<Order>();
    orders1.add(ord1);
    Map<Id, Order> oldorders1 = new Map<Id, Order>();
    oldorders1.put(ord1.id,ord1); 
    PS_INT_IntegrationRequestController.createIntegrationRequestOrder(orders1,oldorders1);
    
    test.stopTest();
     
     
    }
  }
 
  /*************************************************************************************************************
  * Name        : generateTestCases
  * Description : Generate Cases records
  * Input       : NumOfCases - Number of case records to generate
  * Output      : List of the Case records generated
  *************************************************************************************************************/
  private static List<Case> generateTestCases(Integer numOfCases, Boolean setExternalId)
  {
    List<Case> casesToInsert = new List<Case>();
    
    //Generate an account
    Account acc = new Account(Name = 'Account 1');
    if(setExternalId)
    {
      acc.External_Account_Number__c = 'External';  
    }
    insert acc;
        
    //Generate contact
    Contact con = new Contact(LastName = 'Contact 11121', FirstName = 'fn4444', Email = 'test@testraj.com', AccountId = acc.Id,First_Language__c  = 'English',MobilePhone  = '4987693425',Salutation = 'MR.',Preferred_Address__c = 'Other Address' ,OtherCountry  = 'India',OtherStreet = 'Test',OtherCity  = 'Test',OtherPostalCode  = '123456');
    insert con;
    
    AccountContact__c accCon = new  AccountContact__c(Account__c = acc.Id, Contact__c = con.Id, AccountRole__c = 'Role', Primary__c = true, Financially_Responsible__c = True);
    insert accCon;
    
    Account_Correlation__C ac = new Account_Correlation__C(Account__C = acc.Id, External_ID_Name__c = 'eVision Learner Number', External_ID__c = 'External ID');
    insert ac;
     
    RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Case' and DeveloperName = 'Loan_Bursary_Request'];                        
    for(Integer i=0; i<numOfCases; i++)
    {    
      Case caseToInsert = new Case( AccountId = acc.Id, RecordTypeid = rt.id, Type ='General', ContactId = con.Id, Sponsor_name__c = con.Id, Reason_if_Other__c = 'Other');          
      casesToInsert.add(caseToInsert);
    }
        
    if(casesToInsert.size()>0)
    {
      insert casesToInsert;
    }
     
    return casesToInsert;
  
  }
  
   static testMethod void ChatterComponentTest()
  {  
        
        List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId(),1);
        insert usrLst;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
        insert byp;         
        System.runas(usrLst[0]){
        Account acc = (Account)TestClassAutomation.createSObject('Account');
        acc.Name = 'Test';
        insert acc; 
        Contact con = new Contact(LastName = 'Contact 1', FirstName = 'fn', Email = 'test@test.com.demo', AccountId = acc.Id,First_Language__c  = 'English',MobilePhone  = '498763425',Salutation = 'MR.',Preferred_Address__c = 'Other Address' ,OtherCountry  = 'India',OtherStreet = 'Test',OtherCity  = 'Test',OtherPostalCode  = '123456');
        insert con;
        Case cas = new Case( AccountId = acc.Id, Type ='General', ContactId = con.Id, Sponsor_name__c = con.Id, Reason_if_Other__c = 'Other',Error_Message__c='Other - Please enter error code in box');
        insert cas;
        FeedItem fi = new FeedItem();
        fi.type ='TextPost';
        fi.ParentId = cas.id;
        fi.body = 'PS_ServiceNow_Integration_Body';
        insert fi;
            
        List<FeedItem> fi1 = new List<FeedItem>();
        fi1.add(fi);
        Map<Id,Case> feedtoCsMap = new Map<Id,Case>();   
        feedtoCsMap.put(fi.id,Cas);
        Test.startTest();
        PS_INT_IntegrationRequestController.createIntegrationRequestFeedItem(fi1,feedtoCsMap);
        
        Test.stopTest();
        }
        
    }
    static testMethod void VerifycreateIntegrationRequestCase()
{  
   /*List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
    usrLst[0].Market__c = 'UK';
    insert usrLst;*/
     User u = new User(Alias = 'happy', Email='standarduser1178@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='happy', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = UserInfo.getProfileId(), Sample_Approval__c= true,Order_Approver__c= UserInfo.getUserId(),
                          TimeZoneSidKey='America/Los_Angeles', UserName='test1234'+Math.random()+'@gmail.com', License_Pools__c ='Test User',Geography__c = 'Growth',Market__c = 'UK',Line_of_Business__c = 'Higher Ed',Business_Unit__c='Higher Ed'); 
        insert u ;
    
    Bypass_Settings__c TestUser = Bypass_Settings__c.getOrgDefaults();
    
    TestUser.Disable_Triggers__c = true;
    upsert TestUser; 
    System.runas(u)
    {     
    List<Contact> ConList =  TestDataFactory.createContact(5);
    insert ConList;
    
    List<case> CaseLst = TestDataFactory.createCase(6,'Cancellation Request');
    CaseLst[0].Status ='Closed';
    caseLst[1].Current_Campus__c ='Nelspruit';
    caseLst[1].Proposed_Campus__c='â€‹Bedfordview';
    caseLst[1].Validate_Transfer__c = true;
    caseLst[2].Current_Campus__c ='Nelspruit';
    caseLst[2].Proposed_Campus__c='â€‹Bedfordview';
    caseLst[2].Validate_Transfer__c = true;    
    CaseLst[0].Sponsor_name__c =ConList[0].id;
    CaseLst[1].Sponsor_name__c =ConList[1].id;
    CaseLst[2].Sponsor_name__c =ConList[2].id;
    CaseLst[3].Sponsor_name__c =ConList[3].id;
    caseLst[5].Case_Resolution_Category__c ='Referrals';
    

    CaseLst[1].RecordtypeId=Schema.SObjectType.case.getRecordTypeInfosByName().get('Change Request').getRecordTypeId();
    CaseLst[1].Status ='Closed';   
    
    CaseLst[2].RecordtypeId=Schema.SObjectType.case.getRecordTypeInfosByName().get('Change Request').getRecordTypeId();
    CaseLst[2].Status ='Closed';
    CaseLst[2].Type ='Change Campus';
    
    CaseLst[3].RecordtypeId=Schema.SObjectType.case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
    CaseLst[3].IsEscalated =true;
    
    CaseLst[4].RecordtypeId=Schema.SObjectType.case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
    CaseLst[4].Escalation_External_ID__c='Complete';
    CaseLst[4].IsEscalated =true;
    CaseLst[4].Escalation_External_Status__c='Resolved';
    
    CaseLst[5].RecordtypeId=Schema.SObjectType.case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
    CaseLst[5].Escalation_External_ID__c='Complete';
    CaseLst[5].status ='Closed';
    
    insert CaseLst;
    


    case ca = [select id,status,type,Current_Campus__c,Proposed_Campus__c,Sponsor_name__c from case where id=:CaseLst[1].id];   
    caseLst.remove(1);      


    ca.Type = 'Change Sponsor';
    update ca;
   
    CaseLst.add(ca);

        PS_INT_IntegrationRequestController.createIntegrationRequestCase(CaseLst); 
    }
}

  
}