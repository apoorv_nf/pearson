/************************************************************************************************************
* Apex Interface Name : ProcessBuilderBypassDummyClassTest 
* Version             : 1 
* Created Date        : 24/1/2017
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Christwin                 24/1/2017
-------------------------------------------------------------------------------------------------------------
Class to Activate Bypass Setting for Process Builder
************************************************************************************************************/
@isTest
public class ProcessBuilderBypassDummyClassTest {
    
    static testMethod void test_dummyFunction (){
        ProcessBuilderBypassDummyClass.dummyFunction();
    }
        
}