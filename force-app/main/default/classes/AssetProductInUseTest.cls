@isTest
private class AssetProductInUseTest{

static  UniversityCourse__c course;
static Account a;
    
/*static testmethod void testAssetSave(){

    //insert test data
    Account a = TestData.buildAccount('Test Account');
    a.Geography__c = 'North America';
    a.Region__c = 'United States';
    a.BillingState = 'QueensLand';
    a.Line_of_Business__c ='Schools';
    a.Market2__c = 'US';
    insert a;
    
    UniversityCourse__c course = TestData.buildUniversityCourse(a.id);
    insert course;
    
    PageReference testPage = Page.AssetNewProductInUse; 
    testPage.getParameters().put('Account_lkid', a.id); //Page requires an object id (Opportunity or University Course)
    testPage.getParameters().put('CF00Nb0000009hWmz_lkid', course.id);  //0 indicates opportunity, 1 indicates Universtity Course
        
        //Set the page as the one we'll be using for these tests
    Test.setCurrentPageReference(testPage);
    Apexpages.StandardController stdController = new Apexpages.StandardController(new Asset());
    AssetProductInUse ctrl = new AssetProductInUse(stdController);
    ctrl.newAsset.name = 'Test';
    PageReference pageref = ctrl.assetSave();
    System.debug('pageref  '+pageref );
    //System.assertEquals('/'+ctrl.newAsset.Course__c,String.valueOf(pageref) );

}*/

/*static testmethod void testNoData(){

    //insert test data
    createTestData();    
    
    PageReference testPage = Page.AssetNewProductInUse; 
    testPage.getParameters().put('Account_lkid', null); //Page requires an object id (Opportunity or University Course)
    testPage.getParameters().put('CF00Nb0000009hWmz_lkid', null);  //0 indicates opportunity, 1 indicates Universtity Course
        
        //Set the page as the one we'll be using for these tests
    Test.setCurrentPageReference(testPage);
    Apexpages.StandardController stdController = new Apexpages.StandardController(new Asset());
    AssetProductInUse ctrl = new AssetProductInUse(stdController);
    PageReference pageref = ctrl.assetSave();
    System.debug('pageref  '+pageref );
    System.assertEquals(null,String.valueOf(pageref) );
}*/

static testmethod void testSearchSOSL(){
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);       
        insert lstWithTestUser;
     Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c =true,
                                                     Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
        insert byp; 
        System.runAs(lstWithTestUser[0]) 
            {
      //  Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c =true,Disable_Validation_Rules__c=true);
       // insert byp; 
    createTestData();
     List<Account> accountList = new List<Account>();
    accountList = TestDataFactory.createAccount(1,'Organisation');    
    insert accountList; 
    System.debug(accountList);
    System.debug(accountList[0].Id);
    List<Contact> contactsList = TestDataFactory.createContacts(3); 
    contactsList[0].AccountId= accountList[0].Id;
    insert contactsList;
    
    List<UniversityCourseContact__c> courseConList = new List<UniversityCourseContact__c >();
    UniversityCourseContact__c courseCon = new UniversityCourseContact__c();
    courseCon.universityCourse__c = course.id;
    courseCon.contact__c = contactsList[0].id;
    courseConList.add(courseCon);
    insert courseConList;
    
    AssetProductInUse ctrl = getController();
    
    ctrl.soslSearchText = 'Test';
    ctrl.searchSOSl();
    ctrl.selContacts.add(contactsList[0].id);
    ctrl.newAsset.name = 'Test';
    PageReference pageref = ctrl.assetSave();
    }
}
    static testMethod void testClearFilters(){
    Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);       
        insert lstWithTestUser;
     Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c =true,
                                                     Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
        insert byp; 
        System.runAs(lstWithTestUser[0]) 
            {
        createTestData();        
        AssetProductInUse ctrl = getController();
        ctrl.clearFilters();   
        ctrl.tableTitle = 'Search Results';
        ctrl.filterResultMap = new Map<Id,AssetProductInUse.ContactWrapper>();
        ctrl.clearFilters();
       }
    }
    
    static testMethod void testClearSearchResults(){
    Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);       
        insert lstWithTestUser;
     Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c =true,
                                                     Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
        insert byp; 
        System.runAs(lstWithTestUser[0]) {
        createTestData();
        AssetProductInUse ctrl = getController();
        ctrl.clearSearchResults();         
        ctrl.backToNewPIU();
        ctrl.nextPage();}
    }
   
    static testMethod void testAttachSelectedContacts(){
    Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);       
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c =true,
                                                        Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
        insert byp;
        System.runAs(lstWithTestUser[0]) 
            {
       // Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c =true,Disable_Validation_Rules__c=true);
        // insert byp;
        createTestData();
         List<Account> accountList = new List<Account>();
    accountList = TestDataFactory.createAccount(1,'Organisation');    
                insert accountList;
        List<Contact> contactsList = TestDataFactory.createContacts(3);    
        contactsList[0].firstName = 'Test';
        contactsList[0].AccountId = accountList[0].Id;
        contactsList[1].firstName = 'Test1';
        contactsList[1].AccountId = accountList[0].Id;
        insert contactsList;
        
        AssetProductInUse ctrl = getController();
        ctrl.conWrapperMap.put(contactsList[0].id,new AssetProductInUse.ContactWrapper(contactsList[0]));
        ctrl.currentContact = contactsList[0].id;
        ctrl.attachSelectedContacts(); 
        ctrl.attachSelectedContacts();
        ctrl.currentContact = '';
        ctrl.attachSelectedContacts();
        ctrl.removeSelectedContacts();
        }
    }
    
    static testMethod void testRemoveSelectedContacts(){
     Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);       
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c =true,
                                                        Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
        insert byp;
        System.runAs(lstWithTestUser[0]) 
            {
        createTestData();
         List<Account> accountList = new List<Account>();
    accountList = TestDataFactory.createAccount(1,'Organisation');    
                insert accountList; 
        List<Contact> contactsList = TestDataFactory.createContacts(3);    
        contactsList[0].firstName = 'Test';
        contactsList[0].AccountId = accountList[0].Id;
        contactsList[1].firstName = 'Test1';
        contactsList[1].AccountId = accountList[0].Id;
        insert contactsList;
        
        AssetProductInUse ctrl = getController();
        ctrl.conWrapperMap.put(contactsList[0].id,new AssetProductInUse.ContactWrapper(contactsList[0]));
        ctrl.selContacts.add(contactsList[0].id);
        Integer mapSize = ctrl.conWrapperMapSize;
        ctrl.removeSelectedContacts();
        }
    }
    
    static testMethod void testSelectAllContacts(){
     Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);       
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c =true,
                                                        Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
        insert byp;
        System.runAs(lstWithTestUser[0]) 
            {
        createTestData();
        List<Account> accountList = new List<Account>();
    accountList = TestDataFactory.createAccount(1,'Organisation');    
                insert accountList;
        List<Contact> contactsList = TestDataFactory.createContacts(3);    
        contactsList[0].firstName = 'Test';
        contactsList[0].AccountId = accountList[0].Id;
        contactsList[1].firstName = 'Test1';
        contactsList[1].AccountId = accountList[0].Id;
        insert contactsList;
        
        AssetProductInUse ctrl = getController();
        ctrl.conWrapperMap.put(contactsList[0].id,new AssetProductInUse.ContactWrapper(contactsList[0]));
        ctrl.conWrapperMap.put(contactsList[1].id,new AssetProductInUse.ContactWrapper(contactsList[1]));
        ctrl.isSelectAll = true;
        ctrl.selectAllContacts();
        ctrl.isSelectAll = false;
        ctrl.selectAllContacts();
        }
    }
    
    static testMethod void testPagination(){
    Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);       
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c =true,
                                                        Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
        insert byp;
        System.runAs(lstWithTestUser[0]) 
            {
        createTestData();
        AssetProductInUse ctrl = getController();
        ctrl.pageCount();
        ctrl.FirstRecords();
        ctrl.LastRecords();
        ctrl.PreviousPage();
        ctrl.NextPage2();}
    }
    
    static testMethod void testWithNoFilters(){
     Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);       
        insert lstWithTestUser;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c =true,
                                                        Disable_Process_Builder__c=true, Disable_Validation_Rules__c=true);
        insert byp;
        System.runAs(lstWithTestUser[0]) 
            {
        createTestData();        
        List<Account> accountList = new List<Account>();
        accountList = TestDataFactory.createAccount(1,'Organisation');    
        insert accountList;
        List<Contact> contactsList = TestDataFactory.createContacts(2);    
        contactsList[0].AccountId = accountList[0].Id;
        insert contactsList;
        
        AssetProductInUse ctrl = getController();
        ctrl.conWrapperMap.put(contactsList[0].id,new AssetProductInUse.ContactWrapper(contactsList[0])); 
        ctrl.conWrapperMap.put(contactsList[1].id,new AssetProductInUse.ContactWrapper(contactsList[1])); 
        //ctrl.conWrapperMap.put(contactsList[2].id,new AssetProductInUse.ContactWrapper(contactsList[2])); 
        ctrl.soslSearchText = '';
        ctrl.clearAllFilterText();
        ctrl.applyFilters();
        
        AssetProductInUse ctrl1 = getController();
        ctrl1.conWrapperMap.put(contactsList[0].id,new AssetProductInUse.ContactWrapper(contactsList[0])); 
        ctrl1.conWrapperMap.put(contactsList[1].id,new AssetProductInUse.ContactWrapper(contactsList[1])); 
        //ctrl1.conWrapperMap.put(contactsList[2].id,new AssetProductInUse.ContactWrapper(contactsList[2]));
        ctrl1.firstNameText = 'Test';
        ctrl1.lastNameText = '123';
        ctrl1.PSAText = 'Account';
        ctrl1.phoneText = '111';
        ctrl1.acctNameText = 'Test';
        ctrl1.primaryEmailText = 'Test123';
        ctrl1.applyFilters();
        
        ctrl1.firstNameText = 'Test';
        ctrl1.lastNameText = '0123';
        ctrl1.applyFilters();
               
    }
    }
    
public static void createTestData()
{
    a = TestData.buildAccount('Test Account');
    a.Geography__c = 'North America';
    a.Region__c = 'United States';
    a.BillingState = 'QueensLand';
    a.Line_of_Business__c ='Schools';
    a.Market2__c = 'US';
    insert a;
    
    course = TestData.buildUniversityCourse(a.id);
    insert course;
}
 
    public static List<Contact> createContacts(){
      List<Contact> contacts = new List<Contact>();
      for(Integer i=0; i<3; i++){
      Contact contact = new Contact();
      contact.FirstName = 'Test'+i;
      contact.LastName = '123'+i;
      contact.Salutation = 'MR.';
      contact.Email = 'Test123'+i+ '@email.com';
      contact.Phone = '111222333' + i;      
      contact.Preferred_Address__c = 'Mailing Address';
      contact.MailingCity  =  'Newcastle';
      contact.MailingState = 'Northumberland';
      contact.MailingCountry = 'United Kingdom';
      contact.MailingStreet = '1st Street' ;
      contact.MailingPostalCode = 'NE28 7BJ';
      //contact.AccountId = a.id;
      //Abhinav on 3/2016 for DR-0312 : Populating Mobile and First Language on Contact
      contact.MobilePhone  = '498763425';
      contact.First_Language__c  = 'English';
      contacts.add(contact);
        }
        return contacts;
    }
    public static AssetProductInUse getController(){
        PageReference testPage = Page.AssetNewProductInUse; 
        testPage.getParameters().put('Account_lkid', a.id); //Page requires an object id (Opportunity or University Course)
        testPage.getParameters().put('CF00Nb0000009hWmz_lkid', course.id);  //0 indicates opportunity, 1 indicates Universtity Course
        
         //start of changes by priya for CR-1472
         
        // Account acc = new Account(Name = 'Account 1');
         List<Account> createAccount1 = TestDataFactory.createAccount(3,'Learner');            
        insert createAccount1;
        System.debug('CreateAccount:'+createAccount1 );
        System.debug('CreateAccount:'+createAccount1[0].Id);
        List<Contact> contactsList1 = TestDataFactory.createContacts(3);    
        contactsList1[0].firstName = 'Test1';
        contactsList1[0].AccountId = createAccount1[0].Id; 
        System.debug('contactsList1[0].AccountId:'+contactsList1[0].AccountId);
        System.debug('CreateAccount:'+createAccount1[0].Id); 
      //  contactsList1[1].firstName = 'Test12';
        insert contactsList1;
        
       
        
        List<Product2> createProduct1 = TestDataFactory.createProduct(1);        
        Product2 Productlst = new Product2();
        Productlst.Name='ProdcTest';
        Productlst.market__c='Global';
        Productlst.Line_of_Business__c='Higher Ed';
        Productlst.Business_Unit__c='US Field Sales';        
        insert Productlst;
        
        List<UniversityCourse__c> createCourse1 = TestDataFactory.insertCourse();    
        UniversityCourse__c course = new UniversityCourse__c();
        course.Name = 'TerritoryCourseNameandcode';
        course.Catalog_Code__c = 'Territorycoursecode';
        course.Course_Name__c = 'Territorycoursename';
        course.CurrencyIsoCode = 'USD';
        course.Account__c= createAccount1[0].id;
        createCourse1.add(course);
        insert course;
        
        List<Asset> createAsset1 = TestDataFactory.insertAsset(1,createCourse1[0].Id,contactsList1[0].Id,createProduct1[0].Id,createAccount1[0].Id);
        Integer numasset;
        Asset asset = new Asset();
        asset.Name = 'TerritoryAsset';
        asset.Product2Id = createProduct1[0].Id;
        asset.AccountId = createAccount1[0].Id;
        asset.Course__c = createCourse1[0].Id;
        asset.Primary__c = True;
        asset.Status__c = 'Active';
        asset.ContactId = contactsList1[0].Id;        
        insert asset;
        
        testPage.getParameters().put('id', asset.Id);
        //end of changes by priya for CR-1472
        
        //Set the page as the one we'll be using for these tests
        Test.setCurrentPageReference(testPage);
        Apexpages.StandardController stdController = new Apexpages.StandardController(new Asset());
        AssetProductInUse ctrl = new AssetProductInUse(stdController);
        //start of changes by priya for CR-1472   
        ctrl.SaveNew();
        ctrl.Savefuntion();
        //end of changes by priya for CR-1472
        return ctrl;
        
        
    }

}