/*
    *  Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 32,70) 
    *  Modified by BoopathyRaja Elangovan on 06/01/2017 for CR-00955 ->(Added Lines- 34,73) 
    *  Modified by Manimekalai on 10/22/2019 for CR-02949 (replaced apttus with non apttus objects)
*/
@isTest//(SeeAllData=True)
//class for covering the test class
private class PS_PotentialTargetOpptyTempTest 
{
   static  Profile pfile = [Select Id,name from profile where name = 'System Administrator']; 
   static  User u = new User(); 
    // to test with US market user values
    static testMethod void myBatchTest(){ 
      
     
      //code for creating an User
       u = new User();
      u.LastName = 'territoryuser';
      u.alias = 'terrusr'; 
      u.Email = 'territoryuser@pearson.com';  
      u.Username='territoryuser@pearson.com';
      u.LanguageLocaleKey='en_US'; 
      u.TimeZoneSidKey='America/New_York';
      u.Price_List__c='Humanities & Social Science';
      u.LocaleSidKey='en_US';
      u.EmailEncodingKey='ISO-8859-1';
      u.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
      u.Geography__c = 'Growth';
      u.Market__c = 'US';
      u.Business_Unit__c = 'US Field Sales';
      u.Line_of_Business__c = 'Higher Ed';
      u.isactive=true;
      u.CurrencyIsoCode='USD';
      u.Product_Business_Unit__c = 'CTIPIHE';
      u.Sample_Approval__c=true;
      u.License_Pools__c ='ERPI';
      insert u;  
          system.runas(u){  
            PS_PotentialTargetOpptyTempTest opsopty = new PS_PotentialTargetOpptyTempTest();
             opsopty.creationofTestData(); 
           Test.startTest();
                
             //code for covering the batch class 
            PS_PotentialTargetOpptyCreationBatch ptoc = new PS_PotentialTargetOpptyCreationBatch('Test');
              // creating custom metadata instance
           PS_PotentialTargetSellingPeriod__mdt  osellingperiod = new PS_PotentialTargetSellingPeriod__mdt();
               Map < Id, Generate_Potential_Target__c > qualifyAllPotentialTargets = new Map < Id, Generate_Potential_Target__c > ();
            PS_MarketSpecialPermission__mdt  ops_bypassSampling = new  PS_MarketSpecialPermission__mdt();  
            Database.executeBatch(ptoc);
            Test.stopTest();   
           }
    } 
    
    // to test CA  market user values
    static testMethod void myBatchCATest(){ 
         //code for creating CA User
       u = new User();
      u.LastName = 'territoryuser';
      u.alias = 'terrusr'; 
      u.Email = 'territoryuser@pearson.com';  
      u.Username='territoryuser@pearson1.com';
      u.LanguageLocaleKey='en_US'; 
      u.TimeZoneSidKey='America/New_York';
      u.Price_List__c='Humanities & Social Science';
      u.LocaleSidKey='en_US';
      u.EmailEncodingKey='ISO-8859-1';
      u.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
      u.Geography__c = 'Growth';
      u.Market__c = 'CA';
      u.Business_Unit__c = 'Higher Ed';
      u.Line_of_Business__c = 'Higher Ed';
      u.isactive=true;
      u.CurrencyIsoCode='USD';
      u.Product_Business_Unit__c = 'CTIPIHE';
      u.Sample_Approval__c=true;
      u.License_Pools__c ='ERPI';  
      insert u;  
      system.runas(u){  
          PS_PotentialTargetOpptyTempTest opsopty = new PS_PotentialTargetOpptyTempTest();
           opsopty.creationofTestData();
           Test.startTest();         
           
          // creating custom metadata instance
           PS_PotentialTargetSellingPeriod__mdt  osellingperiod = new PS_PotentialTargetSellingPeriod__mdt();
            //code for covering the batch class 
            PS_PotentialTargetOpptyCreationBatch ptoc = new PS_PotentialTargetOpptyCreationBatch('Test');
             Map < Id, Generate_Potential_Target__c > qualifyAllPotentialTargets = new Map < Id, Generate_Potential_Target__c > ();
            PS_MarketSpecialPermission__mdt  ops_bypassSampling = new  PS_MarketSpecialPermission__mdt(); 
            Database.executeBatch(ptoc);
            Test.stopTest();   
           }
          }    
    
    //generic method created for US and CA 
    private void creationofTestData(){
        
      //Product set up
        Product2 pf2 = TestDataFactory.insertRelatedProducts(); 
        Product2 pf = TestDataFactory.insertRelatedProducts();
        pf.Next_Edition__c = pf2.id;
        
        System.debug('Test.getStandardPricebookId(): '+Test.getStandardPricebookId());
        Id StandardPricebook =[Select id from pricebook2 where isStandard = true LIMIT 1].Id ;
        system.debug('@@@ StandardPricebook : '+StandardPricebook );
        List<PricebookEntry> pbe = new List<PricebookEntry>();
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(),Product2Id = pf.id,
                IsActive=true,UnitPrice = 100,currencyisocode='USD');
        pbe.add(pbe1);
        
        List<Product2> rp = [select id,name from Product2 limit 5 /* where id in (select RelatedProduct__c  from
        RelatedProduct__c where Product__c = :pf.id and PSAM__c=true and  RelatedProduct__r.name like 'NA Territory%')*/];
        for(Product2 prodObj : rp){
            prodObj.Medium2__c = 'Digital';
            prodObj.Category2__c='CourseSmart (Review Only)' ;
            
            prodObj.Market__c  = u.Market__c;
            prodObj.Business_Unit__c  = u.Product_Business_Unit__c;
            prodObj.Line_of_Business__c  = u.Line_of_Business__c;
            prodObj.currencyisocode = 'USD';
            prodObj.IsActive = true; 
            prodObj.relevance_value__c = 10;
            
        }
        update rp;
                  
                  
        for(Product2 prod:rp){
            PricebookEntry pbe2 = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(),Product2Id = prod.id,
                IsActive=true,UnitPrice = 100,currencyisocode='USD');
            pbe.add(pbe2);
        }
        insert pbe;
       
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u.id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
        insert byp;

       //category set up
    ProductCategory__c cat = new ProductCategory__c(Name='TestClassification',HierarchyLabel__c='TestClassification',
                                            CurrencyISOCode='USD');
    insert cat; 
        
    /* 
    ProductCategory__c prodCat = new ProductCategory__c (Name='TestHierarchy',HierarchyLabel__c='Hierarchy',
                                                        CurrencyISOCode='USD', Category_ExternalID__c= cat.Id);
    insert prodCat;
    */
        
    Hierarchy__c cathier = new Hierarchy__c(Name='TestClassificationHier',
                                                             Label__c='TestClassificationHier',ProductCategory__c=cat.id, 
                                                             Market__c = 'US', currencyISOCode='USD');
    insert cathier;
    /*
    Hierarchy__c hier = new Hierarchy__c (Name='TestClassificationHier', Label__c='TestClassificationHier',
                                          ProductCategory__c=prodCat.id, Market__c = 'US', currencyISOCode='USD',
                                          CategoryHierarchy_ExternalID__c = cathier.Id);
    insert hier;
        */
    HierarchyProduct__c procat = new HierarchyProduct__c
                            (ProductCategory__c=cathier.id);
    insert procat;
    
    /*HierarchyProduct__c hierProd = new HierarchyProduct__c(ProductCategory__c=hier.id,Product__c=pf.id, 
                                                           ProductClassification_ExternalID__c=procat.id);
     insert hierProd;
        */
       
    //Account and contact set up        
      List<Account> Acc = TestDataFactory.createAccount(1, 'Organisation');
      Acc[0].Territory_Code_s__c = '2ZZS'; 
      insert Acc; 
     
      List<Contact> con = TestDataFactory.createContact(1); 
      con[0].AccountId = Acc[0].id; 
      insert con;  
        
      UniversityCourse__c course = new UniversityCourse__c();
          course.Name = 'TerritoryCourseNameandcode';
          course.Account__c = Acc[0].id;
          course.Catalog_Code__c = 'Territorycoursecode';
          course.Course_Name__c = 'Territorycoursename';
          course.CurrencyIsoCode = 'USD';
          course.Fall_Enrollment__c = 90;
          course.Spring_Enrollment__c = 80;
          course.Status__c = 'Active';
          course.mass_sampling__c=true;
          insert course; 

          Pearson_Course_Equivalent__c pce = new Pearson_Course_Equivalent__c();
          pce.Active__c = true;   
          pce.Course__c =course.id; 
          pce.Primary__c = true;         
          pce.Pearson_Course_Code_Hierarchy__c = cathier.id;
          //pce.Pearson_Course_Code_Hierarchy__r.CategoryHierarchy_ExternalID__c = cathier.id;
          
          insert pce; 
        
        
        UniversityCourseContact__c ucc = new UniversityCourseContact__c();
        ucc.Contact__c = con[0].id;
        ucc.UniversityCourse__c = course.id;
        ucc.Contact_Role__c = 'Business User';
        ucc.Active__c = true;
        insert ucc;
        
        
           Asset asset = new Asset();
           asset.name = 'TerritoryAsset';
           asset.Product2Id = rp[0].id;                //'01tg0000002j1jK';
           //asset.ContactId = con.id;
           asset.AccountId = Acc[0].id;
           asset.Course__c = course.id;
           asset.Primary__c = True;
           asset.Status__c = 'Active';
           insert asset;
           
        Product2 prod1 = new Product2();
        prod1.name = 'TerritoryProduct2';
        prod1.Competitor_Product__c = false; 
        prod1.Next_Edition__c = null;
        prod1.Relevance_Value__c = 10;
        prod1.Business_Unit__c = u.Business_Unit__c;
        prod1.Market__c = u.Market__c;
        prod1.Line_of_Business__c = u.Line_of_Business__c;
        insert prod1; 
        
        Product2 prod2 = new Product2();
        prod2.name = 'TerritoryProduct2';
        prod2.Competitor_Product__c = false; 
        prod2.Next_Edition__c = rp[0].id;
        prod2.Relevance_Value__c = 10;
        prod2.Business_Unit__c = u.Business_Unit__c;
        prod2.Market__c = u.Market__c;
        prod2.Line_of_Business__c = u.Line_of_Business__c;
        insert prod2;  
          
         //Do not bypass triggers anymore
        Delete byp;
        
           //code written for creating Generating Potential Targets
            List<Generate_Potential_Target__c> gptlist = new List<Generate_Potential_Target__c>();
            Generate_Potential_Target__c gpt = new Generate_Potential_Target__c();
            gpt.Account__c = Acc[0].id;
            gpt.Action__c ='Create';
            gpt.Course__c = course.id;
            gpt.Opportunity_Type__c= 'Existing Business';
            gpt.Processed__c = false;
            gpt.Product_In_Use_Publisher__c= 'Pearson';
            gpt.Product_In_Use__c = asset.id;
            gpt.Product__c = rp[0].id;
            gpt.Status__c = 'In Progress';
            gpt.TakeAway_Multiple_Frontlist__c = false;
            gpt.CurrencyIsoCode='USD';
             gptlist.add(gpt);
          
            Generate_Potential_Target__c gpt1= new Generate_Potential_Target__c();
            gpt1.Account__c = Acc[0].id;
            gpt1.Action__c ='Create';
            gpt1.Course__c = course.id;
            gpt1.opportunity_Type__c= 'Takeaway';
            gpt1.processed__c = false;
            gpt1.product_In_Use_Publisher__c= 'Pearson';
            gpt1.product_In_Use__c = asset.id;
            gpt1.Product__c = rp[0].id;
            gpt1.Status__c = 'In Progress';
            gpt1.TakeAway_Multiple_Frontlist__c = true;
            gpt1.CurrencyIsoCode='USD';
            gptlist.add(gpt1);  
            
            Generate_Potential_Target__c gpt5= new Generate_Potential_Target__c();
            gpt5.Account__c = Acc[0].id;
            gpt5.Action__c ='Create';
            gpt5.Course__c = course.id;
            gpt5.opportunity_Type__c= 'Takeaway';
            gpt5.processed__c = false;
            gpt5.product_In_Use_Publisher__c= 'Pearson';
            gpt5.product_In_Use__c = asset.id;
            gpt5.Product__c = prod1.id;
            gpt5.TakeAway_Product_Family__c=pf.id;
            gpt5.Status__c = 'In Progress';
            gpt5.TakeAway_Multiple_Frontlist__c = true;
            gpt5.CurrencyIsoCode='USD';
            gptlist.add(gpt5);  
            
            Generate_Potential_Target__c gpt2= new Generate_Potential_Target__c();
            gpt2.Account__c = Acc[0].id;
            gpt2.Action__c ='Create';
            gpt2.Course__c = course.id;
            gpt2.opportunity_Type__c= 'Rollover';
            gpt2.processed__c = false;
            gpt2.product_In_Use_Publisher__c= 'Pearson';
            gpt2.product_In_Use__c = asset.id;
            gpt2.Product__c = prod2.id;
            gpt2.Status__c = 'In Progress';
            gpt2.TakeAway_Multiple_Frontlist__c = true;
            gpt2.CurrencyIsoCode='USD';
            gptlist.add(gpt2);  
            
            Generate_Potential_Target__c gpt4= new Generate_Potential_Target__c();
            gpt4.Account__c = Acc[0].id;
            gpt4.Action__c ='Create';
            gpt4.Course__c = course.id;
            gpt4.opportunity_Type__c= 'Rollover';
            gpt4.processed__c = false;
            gpt4.product_In_Use_Publisher__c= 'Pearson';
            gpt4.product_In_Use__c = asset.id;
            gpt4.Product__c = prod1.id;
            gpt4.Status__c = 'In Progress';
            gpt4.TakeAway_Multiple_Frontlist__c = true;
            gpt4.CurrencyIsoCode='USD';
            gptlist.add(gpt4);  
            
            Generate_Potential_Target__c gpt3= new Generate_Potential_Target__c();
            gpt3.Account__c = Acc[0].id;
            gpt3.Action__c ='Create';
            gpt3.Course__c = null;
            gpt3.processed__c = false;
            gpt3.product_In_Use_Publisher__c= 'Pearson';
            gpt3.product_In_Use__c = asset.id;
            gpt3.Product__c = rp[0].id;
            gpt3.Status__c = 'In Progress';
            gpt3.TakeAway_Multiple_Frontlist__c = true;
            gpt3.CurrencyIsoCode='USD';
            gptlist.add(gpt3); 
           
            insert gptlist;  
        
           Territory2 tt = TestDataFactory.createTerritory();
            tt.Territory_Code__c='TCA';
            insert tt; 
           
           //code written to assign a user to territory
           UserTerritory2Association ut2a = new UserTerritory2Association();
           ut2a.Territory2Id = tt.id;    //'0MIg00000000A4b';    //testTerritory.id;
           ut2a.UserId = u.id;
           insert ut2a;
                 
        
    }

}