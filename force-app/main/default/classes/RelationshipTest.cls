/*******************************************************************************************************************
* Trigger Name     : Relationship
* Version          : 1.0 
* Created Date     : 11 May 2015
* Function         : Test Class of the Relationship
* Modification Log :
*
* Developer                                Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*   Accenture IDC                          11/05/2015              Created Initial Version
*******************************************************************************************************************/

@isTest             
public class RelationshipTest{
 static testMethod void myTest() { 
 
 //OpportunityLineItemTriggerHandler opl = new OpportunityLineItemTriggerHandler();
     
     Test.startTest();
     Account acc = new Account();
     acc.Name = 'Test';
     acc.Line_of_Business__c= 'Higher Ed';
     acc.Geography__c = 'North America';
     acc.Market2__c = 'US';
     insert acc;
     
     Account acc1 = new Account();
     acc1.Name = 'Test';
     acc1.Line_of_Business__c= 'Higher Ed1';
     acc1.Geography__c = 'North America1';
     acc1.Market2__c = 'US';
     insert acc1;
     
     Relationship__c r = new Relationship__c();
     r.Account__c  = acc.id;
     r.Related_To__c = acc1.id;
     insert r;
     
     r.Other_Type__c = 'test';
     update r;
     delete r;
     undelete r;
     Test.stopTest();
  }
  }