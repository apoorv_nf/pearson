/**
* @description : Utility class to work with BusinessHours.
* @unitTest    : EinsteinBotCheckBusinessHoursTest
* @Author      : Apoorv (Neuraflash)  
* @CreatedDate : Aug 29, 2019
*/
public without sharing class BusinessDaysUtil {
	
	public BusinessHours bHours;
    
    //Paramaterized constructor
    public BusinessDaysUtil(String businessHoursName){
        if(!String.isBlank(businessHoursName)){
            bHours = [SELECT Id FROM BusinessHours WHERE Name =: businessHoursName];
        }
    }
     
    public BusinessDaysUtil(){
        //If no business hours name provided in paramaterized constructor, use deafault hours
        bHours = [SELECT Id FROM BusinessHours WHERE IsDefault = TRUE];
    }

    /**
      * @description: Checks if the datetime passed is within working hours or not
      * @param dt   : Current datetime
      */
    public Boolean isBusinessDay(Datetime dt){
        return BusinessHours.isWithin(bHours.Id, dt);
    }
}