/**

  * Controller : OpportunityProbabilityUpdationBatch
  * Description: Controller class  Batch Class to process Opportunities for  Probability Calculations

  **/  


/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            OpportunityProbabilityUpdationBatch.Cls
   Description:      
   Date             Version           Author                                Summary of Changes 
   -----------      ----------   -----------------      ---------------------------------------------------------------------------------------
   9/17/2016         0.1          Kyama                             Batch Class to process Opportunities for  Probability Calculations
---------------------------------------------------------------------------------------------------------------------------------------------------*/



global class OpportunityProbabilityUpdationBatch implements Database.Batchable < sObject > , Database.Stateful,Schedulable {
    public string batchQuery; //Query for Opportunities
     
     //Constructor for Batch Class.
    public OpportunityProbabilityUpdationBatch() {
        String recordLimit = '';
        Boolean settingFound = false;
        String customSettingDate='';
        batchQuery ='SELECT Id, Name,Probability,StageName,Account.Pearson_Customer_Number__c, (select id, Campaign__c, opportunity__c from Campaign_Opportunities__r),'+
            '(select id,StageName,Probability,RecordType.Name from Opportunities__r) from opportunity where Include_in_Probablity_Calculations__c = true '+
            'and (StageName!=\'Closed Won\' AND StageName!=\'Closed Lost\' AND StageName!=\'Awarded\') and '+
            'Enquiry_Type__c =\'Qualifications\' and '+
            'RecordType.Name=\'Global Opportunity\'';        
        General_One_CRM_Settings__c objSetting = General_One_CRM_Settings__c.getInstance('MasterOppProbabilityCalculation');
        if (objSetting != null)
            customSettingDate = objSetting.PS_Integration_Delete_Date__c+''; 
        if (customSettingDate != ''){
            customSettingDate=customSettingDate.substring(0,customSettingDate.indexOf(' '))+'T00:00:00Z'; 
            batchQuery = batchQuery +' and createddate >= '+customSettingDate;
        }
       
    }//End Of Constructor
    
     //Batch start method
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(batchQuery);
    }//end of start method
    
    
    global void execute(Database.BatchableContext BC, List <Opportunity> scope) {
        Map < Id, Opportunity > opportunities = new map < Id, Opportunity >(scope);//List of opportunities to be processed.
        //Invokes a Handlerclass to implement the logic for opp probability calculations.
        OpportunityProbabilityHandler.mCategorizeOpportunities(opportunities);
        
    }//End Of execute method.
    
    
    
    //<This method gets executed acutomatically when the batch job is execution mode.>
    global void finish(Database.BatchableContext BC) {
    }
    
    global void execute(SchedulableContext sc) {
        database.executebatch(new OpportunityProbabilityUpdationBatch());
    }
    
}//End Of Class