/************************************************************************************************************
* Apex Class Name   : AccountContact_Primary_Financial.cls
* Version           : 1.1
* Function          : Primary Logic check for Account Contacts
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Leonard Victor              20/08/2015              Updated code as part of R3 code stream lining
************************************************************************************************************/


public class AccountContact_Primary_Financial {
    
    public static void checkPrimaryAndFinancialFlags(List<AccountContact__c> triggeredAccountcontacts) {
        List<ID> accountRecordIDs = new List<ID>();
        List<AccountContact__c> relatedAccountContactRecords = new List<AccountContact__c>();
        Id learnerRTId;
        
        // Build the related account list
        for(AccountContact__c ac : triggeredAccountcontacts) { 
            accountRecordIDs.add(ac.Account__c);
        } // End for
        
        // Retrieve the related accounts contact records
        if (!accountRecordIDs.isEmpty())
            relatedAccountContactRecords = [SELECT Id, Account__c, Account__r.RecordTypeId, Financially_Responsible__c, Primary__c 
                                            FROM AccountContact__c WHERE Account__c IN :accountRecordIDs];
        //Get RecordType Id for Learner Account Type      
        //Using Util class to fetch record type instead of SOQL
        if(PS_Util.recordTypeMap.containsKey(PS_Constants.ACCOUNT_OBJECT) && PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).containsKey(PS_Constants.ACCOUNT_LEARNER_RECCORD))                          
            learnerRTId = PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).get(PS_Constants.ACCOUNT_LEARNER_RECCORD);   
        else
            learnerRTId = PS_Util.fetchRecordTypeByName(Account.SobjectType,Label.PS_LearnerRecordType);     
        //End of Record type fix                          
        
        // RD-00287
        // - An account contact must have at least 1 financially responsible contact, if not raise error
        // - An account contact must have at least 1 primary contact, if not raise error
        
        // Iterate through the triggered account contact records and check the field values based on the following business logic
        for(AccountContact__c ac : triggeredAccountcontacts) { 
            // Check for primary account contacts
            if (!ac.Primary__c && !ac.Sync_In_Progress__c ) {
                //Added Size check to restrict extra lines of codes being executed R3 code stream line
                if (!relatedAccountContactRecords.isEmpty() && numberOfOtherPrimaryAccountContacts(relatedAccountContactRecords, ac.Account__c, ac.Id) == 0 && ac.Account__r.RecordTypeId==learnerRTId ) {
                    // Ignore validation if there are no other records to allow the user to create an account contact for the first time
                    if (relatedAccountContactRecords.size() > 1)
                        ac.Primary__c.addError(Label.AC001_Primary_Contact_Error);
                }
            }
            // Check for FS account contacts
            if (!ac.Financially_Responsible__c && !ac.Sync_In_Progress__c ) {
                //Added Size check to restrict extra lines of codes being executed if list is empty R3 code stream line
                if (!relatedAccountContactRecords.isEmpty() && numberOfOtherFSAccountContacts(relatedAccountContactRecords, ac.Account__c, ac.Id) == 0 && ac.Account__r.RecordTypeId==learnerRTId) {
                    // Ignore validation if there are no other records to allow the user to create an account contact for the first time
                    if (relatedAccountContactRecords.size() > 1)
                        ac.Financially_Responsible__c.addError(Label.AC001_Financial_Responsible_Person_Error);
                }
            }
            // Reset the account sync flag
            ac.Sync_In_Progress__c = FALSE;
        } // End for
        
    }
    
    private static Integer numberOfOtherFSAccountContacts(List<AccountContact__c> relatedAccountContactRecords,
                                                          String strAccountID,
                                                          String strTriggeredAccountContactID) {
                                                              Integer intCount = 0;
                                                              
                                                              for (AccountContact__c rac : relatedAccountContactRecords) {
                                                                  if (rac.Account__c == strAccountID) {
                                                                      if (rac.Id != strTriggeredAccountContactID && rac.Financially_Responsible__c)
                                                                          intCount++;
                                                                  }
                                                              } // End for
                                                              
                                                              return intCount;
                                                          }
    
    
    private static Integer numberOfOtherPrimaryAccountContacts(List<AccountContact__c> relatedAccountContactRecords,
                                                               String strAccountID,
                                                               String strTriggeredAccountContactID) {
                                                                   Integer intCount = 0;
                                                                   
                                                                   for (AccountContact__c rac : relatedAccountContactRecords) {
                                                                       if (rac.Account__c == strAccountID) {
                                                                           if (rac.Id != strTriggeredAccountContactID && rac.Primary__c)
                                                                               intCount++;
                                                                       }
                                                                   } // End for
                                                                   
                                                                   return intCount;
                                                               }
    
    // Sahir // Removing actCont.Account_Name__c || 
    public static void checkPrimaryAccountContact(List<AccountContact__c> relatedAccountContactRecords)
    {
        for(AccountContact__c actCont: relatedAccountContactRecords )
        {
            if(actCont.Primary_Account__c)
                actCont.addError(Label.AC001_Delete_Primary_Contact_Error);
        }
    }
    
    Public Static void updateLearnerContact(List<AccountContact__c> triggeredAccountcontacts)
    {
        
        for(AccountContact__c actCont: triggeredAccountcontacts)
        {
            if(actCont.AccountRole__c != null && actCont.AccountRole__c == PS_Constants.ACCOUNT_LEARNER_RECCORD){
                actCont.Learner_Contact__c = actCont.Account__c;  
                actCont.Non_Learner_Contact__c = null;
            }
            else if(actCont.AccountRole__c != PS_Constants.ACCOUNT_LEARNER_RECCORD || actCont.AccountRole__c == null || actCont.AccountRole__c == ''){
                actCont.Learner_Contact__c = null;  
                actCont.Non_Learner_Contact__c = actCont.Account__c;            
            }
            
        }
        
    }
    
}