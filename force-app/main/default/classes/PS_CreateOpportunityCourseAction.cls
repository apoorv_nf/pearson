/************************************************************************************************************
* Apex Class Name   : PS_CreateOpportunityCourseAction.cls
* Version           : 1.0

* Developer                   Date                    Description
*
* Leonard Victor              13/10/2015            Controller Class for Creating opportunity and required child from chatter action
* Chaitra                     18/05/2016            D-5175 Associating PrimaryContact to opportunity
* Pooja Damani                05/07/2016           INC2676197-Associating only active contacts to opportunity 
* RG                          03/08/2016           INC2740129 
* CP                          04/19/2018            INC3362346 - changing the Pipeline hardcode for a custom setting
* Manimala                    03/15/2018            CR-1459-Req-2 - validation rule on Course ERPI Users will create Opportunities from Global Course page layout using the quick action 
* Cristina Perez                08/22/2018              Changes for Global Opportunity as a global process and to work on lightning
-----------------------------------------------------------------------------------------------------------
************************************************************************************************************/

public class PS_CreateOpportunityCourseAction {

    //for constructor
    public ID courseId;
    public String command;
    public Id oppId;
    
    //for course details
    public Map<ID,List<UniversityCourseContact__c>> mapCourseContat = new Map<ID,List<UniversityCourseContact__c>>();
    public Map<ID,Double> mapCourseIntake = new Map<ID,Double>();
    public UniversityCourse__c currentCourseObj = new UniversityCourse__c();
    public static boolean recordsFetched = false;
    public Id courseRecordTypeId = Schema.SObjectType.UniversityCourse__c.getRecordTypeInfosByName().get('Global Course').getRecordTypeId(); //added by Shiva Nampally for #CR-02899 
    
    //for User details
    public static User loggedUserObj;
    public String sellingSeason = '';
    public Integer startDateMonth;
    public Integer startDateFallMonth;
    public Integer endDateMonth;
    public Integer startDateDay;
    public Integer endDateDay;
    
    //for create Oppty
    public Opportunity Oppty_new;
    public Date closedDate;
    public OpportunityUniversityCourse__c new_OpportunityUniversityCourse;
    
    //Constructor
    public PS_CreateOpportunityCourseAction(){
        courseId =  ApexPages.currentPage().getParameters().get('id');
        oppId =  ApexPages.currentPage().getParameters().get('opptyid');
        command = ApexPages.currentPage().getParameters().get('command');
        if(courseId == null && command == null) {  
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: No course Id ');
                        ApexPages.addMessage(myMsg);
        } 
     }

    
    public PageReference onLoad_CourseOpptyEdit(){
        
        
        getUserDetails();
        if(!recordsFetched){
            getCourseDetails(courseId);
        }
        if(command == 'Opportunitycreate'){
           String redirectURL = createOppty();
           if(redirectURL != null){
               PageReference pg = new PageReference(redirectURL);
               pg.setRedirect(true);
               return pg;
           }
        }
        /*
        if(command =='DeleteOpportunity'){
            String redirectURL = DeleteOppty();
            if(redirectURL != null){
                PageReference pg = new PageReference(redirectURL);
                pg.setRedirect(true);
                return pg;
            }
        }
        */
        return null;
    }
    
    // method to get all user details, and based on the user Market calculate selling season
    public void getUserDetails(){
    //Fetching Logged in user fields
     if(!PS_Util.isFetchedUserRecord){
           PS_Util.getUserDetail(Userinfo.getUserId());
     }

     loggedUserObj = PS_Util.loggedInUser;
     startDateFallMonth = 0;
     startDateDay = 1;
     endDateDay = 31;
     if(loggedUserObj!=null){
            if((loggedUserObj.Market__c).equalsIgnoreCase(PS_Constants.COUNTRY_AU) || (loggedUserObj.Market__c).equalsIgnoreCase(PS_Constants.COUNTRY_NZ) ||((loggedUserObj.Market__c).equalsIgnoreCase(PS_Constants.COUNTRY_CA) && (loggedUserObj.Business_Unit__c=='ERPI')) )
            {
                if(system.Date.today().month() >=6 && system.Date.today().month() <= 12){
                        startDateMonth = 1;
                        endDateMonth = 6;
                        sellingSeason = string.valueof(system.Date.today().year()+1)+' - '+PS_Constants.SELLING_SEMESTER1;
                }else if(system.Date.today().month() >= 1 && system.Date.today().month() <= 5){
                        startDateMonth = 7;
                        endDateMonth = 12;
                        sellingSeason = string.valueof(system.Date.today().year())+' - '+PS_Constants.SELLING_SEMESTER2;
                }
            }
            //all markets but AU/NZ/CA/ERPI
            else { 
                if(system.Date.today().month() == 12 || (system.Date.today().month() >=1 && system.Date.today().month() <= 5)){
                    startDateMonth = 8;
                    endDateMonth = 12;
                    startDateFallMonth = 1;
                    if(system.Date.today().month() == 12){  
                       sellingSeason = string.valueof(system.Date.today().year()+1)+' - '+Label.TA_Selling_Period_Fall;
                    }else{
                        sellingSeason = string.valueof(system.Date.today().year())+' - '+Label.TA_Selling_Period_Fall;
                    }
                
                }else if(system.Date.today().month() >= 6 && system.Date.today().month() <= 11){
                        startDateMonth = 2;
                        endDateMonth = 7;
                        sellingSeason = string.valueof(system.Date.today().year()+1)+' - '+Label.TA_Selling_Period_Spring;
                }
            }
            
                          
            
            
          }
    }
    
    //method to get Course detail and related records Course Contacts
    public void getCourseDetails(Id courseId){
           
            String courseQuery = PS_Util.getDynamicSOQL(UniversityCourse__c.SobjectType , false);
            String whereClause = ' FROM UniversityCourse__c WHERE Id = \'' + courseId +'\'';
        
            String inTakeQuery = PS_Util.getDynamicSOQL(Intake__c.SobjectType , false);
            inTakeQuery+=' FROM Intakes__r WHERE University_Course__c =\'' + courseId +'\' AND ((CALENDAR_MONTH(start_Date__c)>='+ startDateMonth +' AND CALENDAR_MONTH(start_Date__c)<= ' + endDateMonth + ') OR CALENDAR_MONTH(start_Date__c)=' +startDateFallMonth + ') AND (DAY_IN_MONTH(Start_Date__c)>=' +startDateDay+ ' AND DAY_IN_MONTH(Start_Date__c)<=' +endDateDay + ')';
            
            String contactQuery = PS_Util.getDynamicSOQL(UniversityCourseContact__c.SobjectType , false);
            //query modified by Pooja for INC2676197 
            contactQuery +=' FROM University_Course_Contacts__r WHERE UniversityCourse__c  =\'' + courseId +'\' AND Active__c=true';
     
            courseQuery+= ',('+inTakeQuery+'),('+contactQuery+')'+whereClause;
                        
            List<UniversityCourse__c> queryCourse = Database.query(courseQuery);
            
            if (queryCourse.size() ==0){}
        
            for(UniversityCourse__c courseObj : queryCourse){
                currentCourseObj = courseObj;
               
                //////////////////////////////////////////////////////////////////////
                //Iteration for Intake
                for(Intake__c courseIntake : courseObj.Intakes__r){
                        if(mapCourseIntake.containsKey(courseObj.id)){
                            Double enrollMent = mapCourseIntake.get(courseObj.id);
                            enrollMent+=courseIntake.Enrollments__c;
                            mapCourseIntake.put(courseObj.id , enrollMent);

                        }
                        else { mapCourseIntake.put(courseObj.id , courseIntake.Enrollments__c);

                        }
                    }
                    ////////////////////////////////////////////////////////////////////////
                    //Iteration for Course Contact
                    for(UniversityCourseContact__c courseContact : courseObj.University_Course_Contacts__r){
                        if(mapCourseContat.containsKey(courseObj.id)){  
                            mapCourseContat.get(courseObj.id).add(courseContact);

                        }
                        else{
                                List<UniversityCourseContact__c> tempCourseLst = new List<UniversityCourseContact__c>();
                                tempCourseLst.add(courseContact);
                                mapCourseContat.put(courseObj.id , tempCourseLst);

                        }
                    }
            }
            recordsFetched = true;
            
     }

    public String createOppty(){
         
         Oppty_new = new Opportunity();
         Oppty_new.Name = currentCourseObj.Name;
         Oppty_new.AccountId = currentCourseObj.Account__c;
         List<Account> AccountPrimaryCheck = [Select Primary_Selling_Account_check__c from Account where Id=:currentCourseObj.Account__c limit 1];
         Oppty_new.StageName  = 'Prequalification';
         Oppty_new.ForecastCategoryName =  General_One_CRM_Settings__c.getInstance('OpptyCreationForecastCategory').value__c;
         Oppty_new.Recordtypeid  = PS_Util.fetchRecordTypeByName(Opportunity.SobjectType,PS_Constants.OPPORTUNITY_GLOBAL_RECCORD );
         Oppty_new.Type = '';
         List <String> sellSeasonLst =sellingSeason.split('-'); 
         Oppty_new.Selling_Period_Year__c = sellSeasonLst.get(0).trim();
         Oppty_new.Selling_Period__c = sellSeasonLst.get(1).trim();
          
         //////////////////////////////////////////
         //Closed Date Logic
         if(sellingSeason!=null){
             if((loggedUserObj.Market__c).equalsIgnoreCase(PS_Constants.COUNTRY_AU) || (loggedUserObj.Market__c).equalsIgnoreCase(PS_Constants.COUNTRY_NZ)|| ((loggedUserObj.Market__c).equalsIgnoreCase(PS_Constants.COUNTRY_CA) && (loggedUserObj.Business_Unit__c=='ERPI'))){
                    if((sellingSeason.split(' - ')[1].trim()).equalsIgnoreCase(PS_Constants.SELLING_SEMESTER1)){
                        closedDate = Date.newInstance (system.Date.today().year(), 11, 30);
                        Oppty_new.CloseDate = closedDate;
                        
                    }
                    else{
                        closedDate = Date.newInstance (system.Date.today().year(), 5, 31);
                        Oppty_new.CloseDate = closedDate;
                    }      
                                                                   
                        
             } 
             //All other markets including UK
             else {
                if((sellingSeason.split(' - ')[1].trim()).equalsIgnoreCase(Label.TA_Selling_Period_Fall)){
                        if(system.Date.today().Month()!=12){
                            closedDate = Date.newInstance (system.Date.today().year(), 6, 30);
                            Oppty_new.CloseDate = closedDate;
                        }
                        else{
                            closedDate = Date.newInstance (system.Date.today().year()+1, 6, 30);
                            Oppty_new.CloseDate = closedDate;
                        }
                        
                }
                 else{
                        closedDate = Date.newInstance (system.Date.today().year(), 11, 30);
                        Oppty_new.CloseDate = closedDate;
                }
            }   
         }
         Oppty_new.Enrolments__c = mapCourseIntake.get(currentCourseObj.id);
         
         Oppty_new.Amount = 0;
         Oppty_new.Market__c = loggedUserObj.Market__c;      
         Oppty_new.Adoption_Type__c = currentCourseObj.Adoption_Type__c;
         /****Condition added for CR-1459-Req9***/
         if(loggedUserObj.Business_Unit__c=='ERPI')
         {
           Oppty_new.Enrolments__c = currentCourseObj.Total_Enrollments__c;
         }
        //Asha changes for CR-02882 Begins
        if(loggedUserObj.Market__c=='AU' || loggedUserObj.Market__c=='NZ'){
            Oppty_new.Channel__c= 'Student Driven';  
            Oppty_new.Channel_Detail__c= 'Bookshop';  
        } //CR-02882 Ends
        
        //added by Shiva Nampally for #CR-02899 Start
        if(currentCourseObj.RecordTypeId == courseRecordTypeId){
            Oppty_new.Primary_Pearson_Course_Code__c = currentCourseObj.Primary_Course_Code__c;
            Oppty_new.Primary_Pearson_Course_Code_Name__c = currentCourseObj.Primary_Course_Code_Name__c;       
        }
        //added by Shiva Nampally for #CR-02899 End     
                    
            try{
                insert Oppty_new;
                 
            }catch(Exception ex){
                Throw ex;
                //return null;
            }
            
            String redirectURL = CreateOpportunityUniversityCourse(Oppty_new.Id);          
            
            return redirectURL;
        }

    /*
    public String DeleteOppty(){
        if(oppId != null && courseid != null){
            Opportunity oppty = new Opportunity(Id= oppId);
            try{
                delete oppty;
                return '/'+courseid;
            }catch(Exception ex){
                return null;
            }
        }
        return null;
    }
    */
    
    public String CreateOpportunityUniversityCourse(String oppId){
        if(currentCourseObj.id != null && oppId != null) {
             
             new_OpportunityUniversityCourse =  new OpportunityUniversityCourse__c();
             new_OpportunityUniversityCourse.Opportunity__c = oppId;
             new_OpportunityUniversityCourse.UniversityCourse__c = currentCourseObj.id;
             new_OpportunityUniversityCourse.Close_Date__c = closedDate;
             new_OpportunityUniversityCourse.Opportunity_University_Course_Amount__c = 0;
             new_OpportunityUniversityCourse.Account__c = currentCourseObj.Account__c;
             new_OpportunityUniversityCourse.Opportunity_Name__c = Oppty_new.Name;
             new_OpportunityUniversityCourse.stage__c = 'Pending';
             new_OpportunityUniversityCourse.Adoption_Type__c = currentCourseObj.Adoption_Type__c;
            
             try{
                insert new_OpportunityUniversityCourse;
               
                String redirectURL=createOpportunityContact(oppId);
                return redirectURL;
             }catch(Exception ex){
                    return null;
            }
        }
        return null;
    }
    
    public String createOpportunityContact(String OppId){
        if(!mapCourseContat.isEmpty()){
            List<OpportunityContactRole> opptContactLst = new List<OpportunityContactRole>();
            //List<UniversityCourseContact__c> PrimaryContact = new List<UniversityCourseContact__c>();
            //PrimaryContact = [select id,Contact__c from UniversityCourseContact__c where UniversityCourse__r.id =: courseId and PS_Contact_Role__c ='Decision Maker' and Active__c = true order by createddate limit 1 ]; //D-5175 Associating PrimaryContact to opportunity //RG: for INC2740129
            
            for(UniversityCourseContact__c eachUnivcoursecontact :mapCourseContat.get(currentCourseObj.id)){
                OpportunityContactRole tempopptycontractRole = new OpportunityContactRole();
                tempopptycontractRole.ContactId = eachUnivcoursecontact.Contact__c;
                tempopptycontractRole.OpportunityId = oppId;
                tempopptycontractRole.Role = eachUnivcoursecontact.PS_Contact_Role__c;
                if (eachUnivcoursecontact.PS_Contact_Role__c=='Decision Maker' && eachUnivcoursecontact.active__c == true) {
                    tempopptycontractRole.IsPrimary= true;
                } else {
                    tempopptycontractRole.IsPrimary= false;
                }
                /*if(PrimaryContact.size() > 0){ //RG: for INC2740129
                    if(eachUnivcoursecontact.contact__c == PrimaryContact[0].contact__c){  //D-5175 Associating PrimaryContact to opportunity
                        tempopptycontractRole.IsPrimary= true ;
                    }
                }*/
                opptContactLst.add(tempopptycontractRole);
            }
            try{
                insert opptContactLst;
                return '/'+oppId;
                
             }catch(Exception ex){
                    return null;
            }
        }
        //return null;
        return '/'+oppId; //added by Srikanth as per INC4834805 - Blank page after creating Opportunity from Course
      }
  
}