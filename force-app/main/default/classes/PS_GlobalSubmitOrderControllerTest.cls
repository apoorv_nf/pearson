/*--------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_GlobalSubmitOrderControllerTest
   Description:     Test class for PS_GlobalSubmitOrderController
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   09 Aug 2016      1.0           Accenture IDC                                         Created
   17 oct 2019      1.1           Rajesh Paleti                           Commented all Apttus related fields
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
 
@isTest
//@isTest (seeAllData=true)
public class PS_GlobalSubmitOrderControllerTest
{ 
    @testSetup
    static void dataSetup() {
         Profile profileId = [select id from profile where Name = 'System Administrator'];
         List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
         insert lstWithTestUser;
         system.runas(lstWithTestUser[0]){
         Bypass_Settings__c TestUser = new Bypass_Settings__c();
         TestUser.SetupOwnerId=UserInfo.getOrganizationId();
         TestUser.Disable_Triggers__c = true;
         TestUser.Disable_Process_Builder__c = true;
         insert TestUser;
         Order newOrder = new Order();
        list<order>OrderLst = new list<order>();
        newOrder = TestDataFactory.returnorder();
        Id OrdRecordTypeId = Schema.SObjectType.order.getRecordTypeInfosByName().get('Global Revenue Order').getRecordTypeId();
        Test.setCreatedDate(neworder.Id, DateTime.newInstance(2000,10,10)); 
        /* neworder.RecordTypeId = OrdRecordTypeId ;
        neworder.market__c = 'UK';
        neworder.Line_Of_Business__c ='Higher Ed';
        neworder.Business_Unit__c='Higher Ed'; */
        neworder.market__c = 'AU';
        neworder.Line_Of_Business__c ='Higher Ed';
        neworder.Business_Unit__c='Higher Ed'; 
        neworder.Sales_Channel__c='Direct';
        update neworder;
        List<opportunity>OppList = new list<opportunity>();
         List<Account>Acclist = TestDataFactory.createAccount(1,'Organisation');
         insert AccList;
         opportunity opp =[select id from opportunity where id=:newOrder.OpportunityId];
         OppList.add(opp);
         // Commented for CR-2949 Apttus(Changes) by Rajesh Paleti
         //Quote propRec = TestDataFactory.createProposal(opp,Acclist[0],'Quote/Proposal');
         //propRec.Quote_Channel__c = 'Direct';
         //propRec.Apttus_Proposal__Primary__c = false;
         //insert propRec;  
         //neworder.Quote_Proposal__c = propRec.id;
         update neworder;

         }
    
    }
    static testmethod void OrderValidation()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        User U = [Select id from user where LastName='happy'limit 1];
        system.runas(u){ //Shippingaddress
        //order neworder = [select id,Quote_Proposal__c,Line_Of_Business__c,Business_Unit__c,market__c,Accountid,ShipToContactid,Opportunityid,EffectiveDate,Status,Pricebook2Id,CurrencyIsoCode,type, Salesperson__c from order limit 1];
        order neworder = [select id,Line_Of_Business__c,Business_Unit__c,market__c,Accountid,ShipToContactid,Opportunityid,EffectiveDate,Status,Pricebook2Id,CurrencyIsoCode,type, Salesperson__c from order limit 1];
        Account Acc = [select id from account limit 1];
        //Quote propRec = [select id,Apttus_Proposal__Primary__c,Quote_Channel__c  from Quote limit 1];
         Financial_Account__c ShipTofinancialAcc  = new Financial_Account__c(Name='financialAcc',Account__c = Acc.id,ERP_Status__c='Inactive');
         insert ShipTofinancialAcc ;
         Financial_Account__c BillToFinancialAcc  = new Financial_Account__c(Name='financialAcc1',Account__c = Acc.id,ERP_Status__c='Inactive');
         insert BillToFinancialAcc;
         Account_Addresses__c  ShipToAccAddress = new Account_Addresses__c  (Account__c = Acc.id ,ERP_Address_Status__c='inactive',Country__c='United Kingdom',State__c='London',State_Code__c ='JJ',Zip_Code__c = 'JJ');
         insert ShipToAccAddress ;
         Account_Addresses__c  BillToAccAddress = new Account_Addresses__c  (Account__c= Acc.id ,ERP_Address_Status__c='inactive',Country__c='United Kingdom',State__c='Cardiff',State_Code__c ='JK',Zip_Code__c='JK');
         insert BillToAccAddress;
         neworder.Ship_To_Address__c = ShipToAccAddress.id;
         neworder.Bill_To_Address__c= BillToAccAddress.id;
         neworder.Ship_To_Financial_Account__c= ShipTofinancialAcc.id;
         neworder.Bill_To_Financial_Account__c= BillToFinancialAcc.id;
         update neworder;
          
          
//start      
         
         List<opportunity> OppList = new list<opportunity>();
         List<RecordType> rt1 = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name = 'Global Opportunity' LIMIT 1];  
         Opportunity oppklt = new opportunity ( Name = 'OpporName1',RecordTypeId= rt1[0].Id,AccountId = acc.Id,Selling_Period_Year__c='2018', Channel__c='Direct', Selling_Period__c='2018 - Spring',CloseDate = System.TODAY() + 30,StageName = 'OpporStageName', QuoteMigration_Qualification__c  = 'Graphic Design', Market__c ='AU', Line_of_Business__c = 'Higher Ed',Business_unit__c ='Higher Ed');
         OppList.add(oppklt);   
         
         Opportunity opp = new opportunity ( Name = 'OpporName1',AccountId = acc.Id,CloseDate = System.TODAY() + 30,StageName = 'OpporStageName',Channel__c='Direct', QuoteMigration_Qualification__c = 'Graphic Design');
          OppList.add(opp);
         insert OppList;  
         
         String userId = UserInfo.getUserId();
            list<User> currentUser = [SELECT Id, Market__c, Line_of_Business__c, Product_Business_Unit__c FROM User WHERE Id =:userId];
        
        product2  prod = new product2(name='test', ISBN__c = '9781292099545', Market__c = currentUser[0].Market__c, Line_of_Business__c = currentUser[0].Line_of_Business__c, Business_Unit__c = currentUser[0].Product_Business_Unit__c );
            insert prod;
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry standardPrice = new PricebookEntry( Pricebook2Id = pricebookId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true); 
            insert customPB;
            PricebookEntry pbe = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id, UnitPrice = 0, currencyIsocode = userinfo.getDefaultCurrency());
            insert pbe;
             /*order sampleorder = new order(Accountid=opp.accountid,ShipToContactid=con.id,Opportunityid=opp.id,EffectiveDate=system.today(),Status='Cancelled',Pricebook2Id=pricebookId,CurrencyIsoCode = userinfo.getDefaultCurrency(), type=System.Label.Order_Type,
                                          ShippingPostalCode = '1111', ShippingCountry = 'India', ShippingState = 'Bihar', ShippingStreet = 'Test', ShippingCity = 'Patna', Market__c = 'AU', Line_of_Business__c = 'Schools' , Business_Unit__c = 'DTC');
            insert sampleorder;
            orderitem item = new orderitem(orderid=sampleorder.id,Shipped_Product__c = prod.id,Quantity = 1,pricebookentryid= standardPrice.id, unitprice=standardPrice.unitprice,Shipping_Method__c= 'Ground' );
            insert item;*/
            order sampleorder1 = new order(Accountid=opp.accountid,Sales_Channel__c='Direct',Opportunityid=opp.id,EffectiveDate=system.today(),Status='New',Pricebook2Id=customPB.id,CurrencyIsoCode = userinfo.getDefaultCurrency(),type=System.Label.Order_Type, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371', ShippingState = 'Karnataka', Salesperson__c = UserInfo.getUserId());
            insert sampleorder1;
            orderitem item1 = new orderitem(orderid=sampleorder1.id,Shipped_Product__c = prod.id,Quantity = 1, pricebookentryid= pbe.id , unitprice=0,Shipping_Method__c= 'Ground' );
            insert item1; 
            
            //List<RecordType> assetrt1 = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType ='Asset' AND DeveloperName ='Global Products In Use' LIMIT 1];  
            Id RecordTypeIdasset = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Global Products In Use').getRecordTypeId();

            //Asset asset1 = new Asset(Name='Testasset1',Status__c = 'Active',AccountId = acc.Id,Product2Id = prod.id,Primary__c = True,RecordTypeId= assetrt1[0].Id);                
            Asset asset2 = new Asset();
            asset2.Name = 'Test Asset1';  //item1.PricebookEntry.Product2.Name;
            asset2.Product2Id = item1.PricebookEntry.Product2Id; 
            asset2.Status__c = 'Active'; 
            asset2.AccountId = acc.Id; 
            asset2.Quantity = item1.Quantity;
            asset2.Order__c = sampleorder1.ID;
            asset2.Order_Product_Id__c = item1.Id;
            asset2.Channel__c = sampleorder1.Sales_Channel__c;
            asset2.InstallDate = Date.Today();            
            asset2.recordtypeid = RecordTypeIdasset;            
            insert asset2;
            PS_Order_Creation_Mapping__mdt metaordercremap = [select Create_Asset_On_Order_Submission__c,Opp_Enquiry_Type__c,Oppty_Market__c,Oppty_LOB__c ,Oppty_BU__c,Oppty_Channel__c from PS_Order_Creation_Mapping__mdt where Opp_Enquiry_Type__c!=null limit 1];

//End
                           
       // controller  class call
        PageReference pageRef = Page.PS_GlobalSubmitOrder;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('orderId', neworder.Id);
        ApexPages.currentPage().getParameters().put('opportunityId', newOrder.OpportunityId);
        ApexPages.currentPage().getParameters().put('accountId', Acc.Id);
        ApexPages.currentPage().getParameters().put('contactId', newOrder.ShipToContactId);
        ApexPages.currentPage().getParameters().put('recordType','Global Revenue Order');
        ApexPages.currentPage().getParameters().put('orderSubmitted','0');
        Id asstId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Global Products In Use').getRecordTypeId();
        ApexPages.currentPage().getParameters().put('GlobalAssetRecordTypeId',asstId);
        ApexPages.StandardController sc = new ApexPages.StandardController(newOrder);
        PS_GlobalSubmitOrderController obj = new PS_GlobalSubmitOrderController(sc);
        // primaryquote = false
        obj.orderStartDate = Date.valueOf('2019-03-08');
        obj.displayPopUp = true;
        obj.Infopopup=true;
        obj.redirect();
        obj.courseCount=OppList.size();
        obj.mCreatePIURevenue(OppList[0].ID,OppList,sampleorder1.id,Acc.id);
        obj.courseCount=OppList.size();
        obj.mCreatePIURevenue('',OppList,sampleorder1.id,Acc.id);
        
        obj.closepopup();
        //propRec.Apttus_Proposal__Primary__c = True;
        //Test.setCreatedDate(propRec.Id, DateTime.newInstance(2000,10,10));
        //update propRec;
        neworder.status = 'Cancelled';
        neworder.Purchase_Order_Number__c='';
        test.starttest();
        update neworder;
        obj.redirect();
         neworder.status = 'New';
         update neworder;
         obj.displayPopUp = true;
         obj.Infopopup=true;
        obj.redirect();
          orderItem OI = [select id from orderItem where orderid=:neworder.id];
        OI.Digital_Email__c = 'abc@gmail.com' ;
        update OI;
        neworder.Purchase_Order_Number__c = null;
        update neworder;
        obj.redirect();
        neworder.Digital_Email__c ='abc@gmail.com' ; 
         update neworder;    
         obj.redirect();
         obj.closepopup();
         neworder.Purchase_Order_Number__c ='123'; 
         update neworder;
          obj.redirect();
           BillTofinancialAcc.ERP_Status__c='Active';
           update BillTofinancialAcc ;
            obj.redirect();
           ShipTofinancialAcc.ERP_Status__c='Active';
           update ShipTofinancialAcc;
            obj.redirect();
             shipToAccAddress.ERP_Address_Status__c ='Active';
            update shipToAccAddress;
            obj.redirect();
          BillToAccAddress.ERP_Address_Status__c ='Active';
          update BillToAccAddress;
          ApexPages.currentPage().getParameters().put('orderSubmitted','1');          
          obj.displayPopUp = true;
          obj.Infopopup=true;
           obj.redirect();
           obj.mCreatePIURevenue(OppList[0].ID,OppList,sampleorder1.id,Acc.id);
           obj.mCreatePIURevenue('',OppList,sampleorder1.id,Acc.id);
           obj.closepopup();
           
           neworder.market__c = 'UK';
           neworder.Line_Of_Business__c ='Schools';
           neworder.Business_Unit__c='Schools'; 
           neworder.Sales_Channel__c='Direct';           
           update neworder;
           obj.redirect();
         
        } 
      }
      
      static testmethod void testWithOpportunityScenario()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        User U = [Select id from user where LastName='happy'limit 1];
        system.runas(u){
        //Shippingaddress
        //order neworder = [select id,Quote_Proposal__c,Line_Of_Business__c,Business_Unit__c,market__c,Accountid,ShipToContactid,Opportunityid,Opportunity.accountId,EffectiveDate,Status,Pricebook2Id,CurrencyIsoCode,type,Salesperson__c from order limit 1];
         order neworder = [select id,Line_Of_Business__c,Business_Unit__c,market__c,Accountid,ShipToContactid,Opportunityid,Opportunity.accountId,EffectiveDate,Status,Pricebook2Id,CurrencyIsoCode,type,Salesperson__c from order limit 1];
        Account Acc = [select id from account limit 1];
        // Commented for CR-2949 Apttus(Changes) by Rajesh Paleti
        //Quote propRec = [select id,Apttus_Proposal__Primary__c,Quote_Channel__c  from Quote limit 1];
        //propRec.Apttus_Proposal__Primary__c = true;

        
         //opportunity opp =[select id,Selling_Period_Year__c,Channel__c,Selling_Period__c from opportunity where id=:newOrder.OpportunityId];
         //OppList.add(opp);
         List<opportunity> OppList = new list<opportunity>();
         List<RecordType> rt1 = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name = 'Global Opportunity' LIMIT 1];  
         Opportunity oppklt = new opportunity ( Name = 'OpporName1',RecordTypeId= rt1[0].Id,AccountId = acc.Id,Selling_Period_Year__c='2018', Channel__c='Direct', Selling_Period__c='2018 - Spring',CloseDate = System.TODAY() + 30,StageName = 'OpporStageName', QuoteMigration_Qualification__c  = 'Graphic Design', Market__c ='AU', Line_of_Business__c = 'Higher Ed',Business_unit__c ='Higher Ed');
         OppList.add(oppklt);   
         insert OppList;
         
                
        
        //update propRec;
          orderItem OI = [select id from orderItem where orderid=:neworder.id];
        OI.Digital_Email__c = 'abc@gmail.com' ;
        update OI;
        neworder.market__c='AU';
        neworder.Digital_Email__c = 'abc@gmail.com' ;
        neworder.Purchase_Order_Number__c ='123'; 
        update neworder;
        PageReference pageRef = Page.PS_GlobalSubmitOrder;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('orderId', neworder.Id);
        ApexPages.currentPage().getParameters().put('opportunityId', newOrder.OpportunityId);
        ApexPages.currentPage().getParameters().put('accountId', Acc.Id);
        ApexPages.currentPage().getParameters().put('contactId', newOrder.ShipToContactId);
        ApexPages.currentPage().getParameters().put('recordType','Global Revenue Order');
        ApexPages.currentPage().getParameters().put('orderSubmitted','0');
        Id asstId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Global Products In Use').getRecordTypeId();
        ApexPages.currentPage().getParameters().put('GlobalAssetRecordTypeId',asstId);
        ApexPages.StandardController sc = new ApexPages.StandardController(newOrder);
        PS_GlobalSubmitOrderController obj = new PS_GlobalSubmitOrderController(sc);
        // primaryquote = false
        obj.orderStartDate = Date.valueOf('2019-03-08');
        obj.displayPopUp = true;
        obj.Infopopup=true;
        obj.redirect();
        OpportunityContactRole OCR = new OpportunityContactRole(ContactId=newOrder.ShipToContactId,Role='Influencer',OpportunityId=newOrder.OpportunityId);
        Insert OCR;
        obj.redirect();
        ApexPages.currentPage().getParameters().put('opportunityId','');
        obj.redirect();
        
        //obj.redirect();
        obj.mCreatePIURevenue(newOrder.OpportunityId,OppList,'sOppId','sAccId');
        obj.closepopup();
        }
    } 
        
    
  }