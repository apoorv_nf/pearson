/*******************************************************************************************************************
* Apex Class Name   : ContactUtils.cls
* Version           : 1.0 
* Created Date      : 10 MARCH 2014
* Function          : Utility Class for Contact Object
* Modification Log  :
* Developer                   Date           Tag         Description
* ------------------------------------------------------------------------------------------------------------------
* Mitch Hunt                  10/03/2014               Created Initial Version of Utility Class
* Kamal Chandran (Acc NDC)    02/10/2015     1.1       Creation of AccountContact   
* Nikhil            3/16/2016    1.2       Updated access modfier to without  
* Matt Mitchener (Neuraflash) 7/18/2018                 Make delete exception for Automated Process user
*******************************************************************************************************************/

public without sharing class ContactUtils
{
  // preventDelete
  // If the current user is not the owner or an admin throw an error message on the page.
  public void preventDelete(Contact[] lstContactsToDelete)
  {  
    map<string, string> mapProfiles = GlobalUtils.getprofiles();
    id sProfileId = system.Userinfo.getProfileId();

        
    if(mapProfiles.get(sProfileId) != PS_ProfileNames.PEARSON_ADMINISTRATOR &&
      mapProfiles.get(sProfileId) != PS_ProfileNames.SYSTEM_ADMINISTRATOR &&
      mapProfiles.get(sProfileId) != PS_ProfileNames.PEARSON_DATA_ADMINISTRATOR &&
      mapProfiles.get(sProfileId) != PS_ProfileNames.PEARSON_LOCAL_ADMINISTRATOR &&
      !mapProfiles.containsKey(sProfileId))
    {
      for(Contact sContact : lstContactsToDelete)
      { if(sContact.OwnerId != system.Userinfo.getUserId())
        {
          sContact.addError(System.Label.NonAdminOwnerDelete);
        }
      }
    }
  }
    
  // createAccountContact
  // create an associated AccountContact__c for a list of contacts.
  public void createAccountContact(Contact[] lstContacts)
  {
    list<AccountContact__c> lstAccountContact = new list<AccountContact__c>();
    set<Id> accountSet = new set<Id>();        
    for(Contact lc : lstContacts)
    {
      accountSet.add(lc.AccountId);
    }
        
    list<AccountContact__c> existingAccountContacts = [SELECT Contact__c, Account__c FROM 
                                                       AccountContact__c where Account__c = :accountSet];
        
    map<Id, AccountContact__c> accountContactLookup = new map<Id, AccountContact__c>();
    for(AccountContact__c ac : existingAccountContacts)
    { accountContactLookup.put(ac.Account__c, ac);          
    }
         
    for(Contact sContact : lstContacts)
    {
      //to block accountcontact creation on lead conversion        
      if(sContact.isleadConvertedContact__c == false && sContact.AccountId != null)
      {
        if(!accountContactLookup.containsKey(sContact.AccountId))
        {
          lstAccountContact.add(new AccountContact__c(Account__c = sContact.AccountId, Contact__c = sContact.Id, 
                                                      Account_Name__c = true, 
                                                      AccountRole__c = sContact.Role__c,
                                                      Role_Detail__c = sContact.Role_Detail__c,
                                                      Primary__c = true,
                                                      Financially_Responsible__c = true));
                                                          
        }
        else
        {
          lstAccountContact.add(new AccountContact__c(Account__c = sContact.AccountId, Contact__c = sContact.Id,Account_Name__c = true, 
            AccountRole__c = sContact.Role__c, Role_Detail__c = sContact.Role_Detail__c, Primary__c = false, Financially_Responsible__c = false));
        } 
      }
    }
    
    if(!lstAccountContact.isEmpty() && lstAccountContact !=null)
    {
        insert lstAccountContact;
    }   
  }
  
  public void handleAfterInsert(Contact[] lstNewContacts) 
  {
    /* logic to avoid creation of Account contact if the contact is created from mobile */
    List<Contact> contactsNotbyMobile = new List<Contact>();
    for(Contact eachcontact : lstNewContacts)
    {
      if(eachcontact.Mobile_Last_Updated__c == null)
      {
        contactsNotbyMobile.add(eachcontact);
      }
    }
    if(contactsNotbyMobile != null && !contactsNotbyMobile.isEmpty())
    {
      createAccountContact(ContactsNotbyMobile);
    }     
  }
  
  public void handleBeforeInsert(Contact[] lstNewContacts) 
  {   
     PS_UserFieldUtill.setUserFieldValues(lstNewContacts);          
  }
  
    //kamal 29/09 - creation of accountcontact when B2B lead is converted - Tag 1.1 
  public AccountContact__c createAccountContact(Lead b2bLead,Map<id, lead> leadMap)
  {
    AccountContact__c newAccCon = new AccountContact__c();
    Id B2BLeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
    if(b2bLead != null)
        {
            if(b2bLead.recordtypeId == B2BLeadRecordTypeId && b2bLead.ConvertedContactId != null && b2bLead.ConvertedAccountId != null)
            {
                newAccCon.Contact__c = b2bLead.ConvertedContactId;
                newAccCon.Account__c = b2bLead.ConvertedAccountId; 
                newAccCon.AccountRole__c = b2bLead.Role__c;
                newAccCon.Role_Detail__c = b2bLead.Role_Detail__c;
                newAccCon.Other_Role_Detail__c = b2bLead.Other_Role_Detail__c;
                newAccCon.Additional_Responsibilities__c = b2bLead.Additional_Responsibilities__c;
                newAccCon.Discipline_Of_Interest_Multi_Select__c = b2bLead.Discipline_Of_Interest_Multi_Select__c;
                newAccCon.Sub_Discipline_of_Interest__c = b2bLead.Sub_Discipline_of_Interest__c;
                newAccCon.Decision_Making_Level__c = b2bLead.Decision_Making_Level__c;
                newAccCon.Sync_In_Progress__c = TRUE;
                newAccCon.Financially_Responsible__c = (leadMap.get(b2bLead.id).Sponsor_Type__c == 'Self')?TRUE:FALSE;
                newAccCon.Primary__c = true;                 
            }
           return newAccCon;
        }
    return null;     
  }
}