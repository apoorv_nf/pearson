/*
 * Author: Shantanu Srivastava (Neuraflash)
 * Date: 7/16/2018
 */
public without sharing class EinsteinBotCustomPreChatController {

	@AuraEnabled
	public static LoggedInUserDetails getLoggedInUser() {
		try{
			if(UserInfo.getUserType() == 'Guest') return new LoggedInUserDetails();

			Id userId = UserInfo.getUserId();
			User user = [SELECT Id, FirstName, LastName, Email, ContactId FROM User WHERE Id = :userId];
			Id userContactId = user.ContactId;
			if(userContactId == null){
				LoggedInUserDetails userRecord = new LoggedInUserDetails(user.Id, user.FirstName, user.LastName, user.Email);
				return userRecord;
			}
			Contact contact = [SELECT Id, FirstName, LastName, Email FROM Contact WHERE Id = :user.ContactId];
			LoggedInUserDetails userRecord = new LoggedInUserDetails(contact.Id, contact.FirstName, contact.LastName, contact.Email);

			return userRecord;
		}
		catch(Exception ex){
			system.debug('>> Exception while fetching user details : ' + ex);
			return new LoggedInUserDetails();
		}
	}

	@AuraEnabled
	public static Contact getContact(String email) {
		try {
			return [
				SELECT Id, FirstName, LastName
				FROM Contact
				WHERE Email = :email
				ORDER BY LastModifiedDate DESC
				LIMIT 1];
		} catch(Exception ex) {
			System.debug(ex.getMessage());
			return new Contact();
		}
	}

	public class LoggedInUserDetails {

		@auraEnabled public String userId{get;set;}
		@auraEnabled public String firstname{get;set;}
		@auraEnabled public String lastname{get;set;}
		@auraEnabled public String email{get;set;}

		public LoggedInUserDetails(){}

		public LoggedInUserDetails(String userId, String firstname, String lastname, String email){
			this.userId = userId;
			this.firstname = firstname;
			this.lastname = lastname;
			this.email = email;
		}
	}
}