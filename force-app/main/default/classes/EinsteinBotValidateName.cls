/*
 * Author: Apoorv Saxena (Neuraflash)
 * Date: 06/08/2019
 * Description: Checks if the String being passed is a valid name format - Developed for SMS Bot.
 */
public with sharing class EinsteinBotValidateName {

    @InvocableMethod(label='Einstein SMS Bot - Validate Name')
    public static List<Boolean> validateName(List<String> names) {
    
        try{
            String name = names[0];
            if(!String.isBlank(name)){
                name = name.trim();
                String nameRegex = '^[A-Za-z ]+$';
                Pattern myPattern = Pattern.compile(nameRegex);
                Matcher myMatcher = myPattern.matcher(name);

                if (myMatcher.matches()){ 
                    return new List<Boolean>{true};
                }
            }
        } catch(Exception ex){
        }
        return new List<Boolean>{false};
	}
}