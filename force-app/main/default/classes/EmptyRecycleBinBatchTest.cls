/************************************************************************************************************
Test class to cover EmptyRecycleBinBatch class
10/10/2016 - Rahul 
************************************************************************************************************/
@IsTest(SeeAllData=true)
public class EmptyRecycleBinBatchTest{

    private static testMethod void testDeleteSuccess()
    {
       Test.startTest();
        List<Account> Acc = TestDataFactory.createAccount(1, 'Organisation');
        insert Acc; 
        delete Acc;   
           EmptyRecycleBinBatch c = new EmptyRecycleBinBatch();
           Database.executeBatch(c);
       Test.stopTest();
   }


}