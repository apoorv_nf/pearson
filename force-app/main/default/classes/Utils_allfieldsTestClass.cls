/*******************************************************************************************************************
* Apex Class Name  : Utils_allfieldsTestClass
* Version          : 1.0 
* Created Date     : 08 January 2018
* Function         : Test Class of the Utils_allFields Class
* Modification Log :
*
* Developer                                Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*   CTS                                  31/03/2015              Created Initial Version
*******************************************************************************************************************/

@isTest        
public class Utils_allfieldsTestClass{
 static testMethod void myTest() { 
 
     test.starttest();
      Utils_allfields.customSettingName = 'System Properties';
    Utils_allfields.opType1 = 'Existing Business';
    Utils_allfields.opStage1 = 'Pending';
    Utils_allfields.opStage2 = 'Needs Analysis';
    Utils_allfields.opAcademic1 = 'Pre-Vetted';
    Utils_allfields.propStage1 = 'Draft';
    Utils_allfields.propDegree1 = 'Full-Time';
    Utils_allfields.propPayMethod1 = 'Direct Deposit';
    Utils_allfields.propPayType1 = '50% Deposit';
    Utils_allfields.propPayType2 = 'Monthly Payment';
    Utils_allfields.y = 'Yes';
    Utils_allfields.oppType2 = 'New Business';
    Utils_allfields.oppType3 = 'Returning Business';
    Utils_allfields.OppConRole1 = 'Business User';
    Utils_allfields.OppConRole2 = 'Primary Sales Contact';
    Utils_allfields.OppConRole3 = 'Other';    //CR-00331
    Utils_allfields.ConfigType = 'Bundle';
    Utils_allfields.ConfigLineType = 'Product/Service';
    Utils_allfields.ConfigStatus = 'Finalized';
    Utils_allfields.CustomField = 'Discount_Reference__c';
    Utils_allfields.errorVar = 'Error';
    Utils_allfields.warningVar = 'Warning';
    Utils_allfields.quoteVar = 'Quote';
    Utils_allfields.contractVar = 'Contract';
    Utils_allfields.DiscountRef1 = 'Academic Credit';
    Utils_allfields.DiscountRef2 = 'Financial Credit';
    Utils_allfields.AdjustmentType = '% Discount';
    String Fields = Utils_allfields.getCreatableFieldsList('Account');
    String qString = Utils_allfields.getCreatableFieldsSOQL('Account','Name like %Test%');
     test.stoptest();
 
 }
}