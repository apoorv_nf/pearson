public class checkRecurssionBefore {
    
    public static Set<String> executedTriggerName = new Set<String>();

    public static boolean run {
     get;
      set {
         if(value) executedTriggerName = new Set<String>();
      }
   }

    public static boolean runOnce(String triggerName, String operation){
    
        if(executedTriggerName.contains(triggerName+operation))
        {
            return false;
        }else
        {
            executedTriggerName.add(triggerName+operation);
            return true;
        }
  
    }

}