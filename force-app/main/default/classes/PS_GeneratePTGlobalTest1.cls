/*-------------------------------------------------------------------------------------------------------------
* Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 18,21,23,27,30,258) 
*/
@isTest
private class PS_GeneratePTGlobalTest1
{
 
    @testSetup static void createCustomSettingData() {
        General_One_CRM_Settings__c generalSetting = new General_One_CRM_Settings__c();
        generalSetting.Category__c = 'Batch Processing';
        generalSetting.Description__c = 'Limit of Potential Target that will be executed per cycle';
        generalSetting.Value__c = '2000';
        generalSetting.name = 'Potential Target Query Limit';
        insert generalSetting;
        
        List<User> userLst = TestDataFactory.createUser(Userinfo.getProfileid() , 5);
        userLst[0].Market__c = 'UK';
        userLst[0].Price_List__c = 'Humanities & Social Science';
        userLst[0].Product_Business_Unit__c = 'CTIPIHE';
        userLst[1].Market__c = 'UK';
        userLst[1].Price_List__c = 'Humanities & Social Science';
        userLst[1].Product_Business_Unit__c = 'CTIPIHE';
        userLst[2].Market__c = 'AU';
        userLst[2].Price_List__c = 'Humanities & Social Science';
        userLst[2].Product_Business_Unit__c = 'CTIPIHE';
        userLst[3].Market__c = 'AU';
        userLst[3].Price_List__c = 'Humanities & Social Science';
        userLst[3].Product_Business_Unit__c = 'CTIPIHE';
        userLst[4].Market__c = 'NZ';
        userLst[4].Price_List__c = 'Humanities & Social Science';
        userLst[4].Product_Business_Unit__c = 'CTIPIHE';
        insert userLst;
        List<PS_TerritoryMovedAddeddeleted__c> lstWithTerritoryCustomSetting = new List<PS_TerritoryMovedAddeddeleted__c>();
      lstWithTerritoryCustomSetting = TestDataFactory.createTerritoryCustomSettingRecord(); 
      insert lstWithTerritoryCustomSetting;  
    }
    static testMethod void targetGenerationUK()
    {
      PS_Util.isFetchedUserRecord  = false;
      List<User> userLstUK = [Select id,Market__c,Business_Unit__c,Line_of_Business__c,Product_Business_Unit__c  from User where Market__c = 'UK' and alias = 'happy'];
      
 
     /* Commented - Darshan
      List<Apttus_Config2__ClassificationName__c> cat = new List<Apttus_Config2__ClassificationName__c>();
      Apttus_Config2__ClassificationName__c catresult = New Apttus_Config2__ClassificationName__c();
       catresult.Name='TestClassification';
       catresult.Apttus_Config2__Active__c=true;
       catresult.Apttus_Config2__HierarchyLabel__c='TestClassification';
       cat.add(catresult);
       insert cat;   
      Apttus_Config2__PriceList__c priceList = new Apttus_Config2__PriceList__c(Name='Humanities & Social Science',Apttus_Config2__Active__c=true);                  
      insert  priceList;     
      Apttus_Config2__PriceListCategory__c pcat = new Apttus_Config2__PriceListCategory__c(Apttus_Config2__PriceListId__c=priceList.Id,Apttus_Config2__HierarchyId__c=cat[0].Id,Apttus_Config2__Sequence__c=1);
      insert pcat;
     */
     //Apttus Decommission - Darshan - Start_Date__c
     
      List<ProductCategory__c> cat = new List<ProductCategory__c>();
      ProductCategory__c catresult = New ProductCategory__c();
       catresult.Name='TestClassification';
      // catresult.isActive =true;
       catresult.HierarchyLabel__c='TestClassification';
       cat.add(catresult);
       insert cat;   
      Catalog__c priceList = new Catalog__c(Name='Humanities & Social Science',isActive__c  =true);                  
      insert  priceList;     
      Catalog_Offering__c pcat = new Catalog_Offering__c(Catalog__c=priceList.Id,ProductCategory__c=cat[0].Id,Sequence__c=1);
      insert pcat;
         
     
       List<Account> Acc = TestDataFactory.createAccount(1, 'Organisation');
       Acc[0].Territory_Code_s__c = '2ZZ'; 
       insert Acc;      
      PS_Util.isFetchedUserRecord  = false;
      System.runAs(userLstUK[0]){
   
          //code for creating course
      List<UniversityCourse__c> courselist=new List<UniversityCourse__c>();
      for(integer i=0;i<3;i++){
          UniversityCourse__c course = new UniversityCourse__c();
          course.Name = 'TerritoryCourseNameandcode'+i;
          course.Account__c = Acc[0].id;
          course.Catalog_Code__c = 'Territorycoursecode'+i;
          course.Course_Name__c = 'Territorycoursename'+i;
          course.CurrencyIsoCode = 'USD';
          courselist.add(course);
      }

      insert courselist;
      
      
      //code for inserting Category Hierarchy 
     /* Commented - Darshan
      Apttus_Config2__ClassificationHierarchy__c categoryhierarchy = new Apttus_Config2__ClassificationHierarchy__c();
      categoryhierarchy.Apttus_Config2__HierarchyId__c = cat[0].id;
      categoryhierarchy.Apttus_Config2__Label__c = 'TestCategoryHierarchy';
     */ 
     //Apttus Decommission - Start_Date__c
    
      Hierarchy__c categoryhierarchy = new Hierarchy__c();
      categoryhierarchy.ProductCategory__c = cat[0].id;
      categoryhierarchy.Label__c = 'TestCategoryHierarchy';
      categoryhierarchy.Name = 'TestCategoryHierarchy';
      insert categoryhierarchy; 
      
      List<Pearson_Course_Equivalent__c> pcelist=new List<Pearson_Course_Equivalent__c>();
      List<Intake__c> intakeLst=new List<Intake__c>();
      for(integer i=0;i<courselist.size();i++){
          Pearson_Course_Equivalent__c pce = new Pearson_Course_Equivalent__c();
          pce.Active__c = true;   
          pce.Course__c =courselist[0].id; 
          pce.Primary__c = true;
          pce.Pearson_Course_Code_Hierarchy__c= categoryhierarchy.id;
          pcelist.add(pce);
          
          
          if(i==0){
            for(Integer inTk = 0 ;inTk<=3 ; inTk++ ){
                Intake__c intakeObj = new Intake__c();
                intakeObj.Start_Date__c = date.newInstance(system.Date.today().year(),9,10);
                intakeObj.Enrollments__c = 100;
                intakeObj.University_Course__c = courselist[i].id;
                intakeLst.add(intakeObj);
            }
          }
          else{
            for(Integer inTk = 0 ;inTk<=3 ; inTk++ ){
                Intake__c intakeObj = new Intake__c();
                intakeObj.Start_Date__c = date.newInstance(system.Date.today().year(),5,10);
                intakeObj.Enrollments__c = 100;
                intakeObj.University_Course__c = courselist[i].id;
                intakeLst.add(intakeObj);
            }
              
          }
          
          
          
      }
      insert pcelist; 
      
      insert intakeLst;
      
      List<Product_Family__c> prodFamLst = Testdatafactory.insertProgramFamily(pcelist.size());
      insert prodFamLst;
      
      
      List<Hierarchy_Family__c> hierFamLst = Testdatafactory.insertHierarchyFamily(prodFamLst ,categoryhierarchy);
      insert hierFamLst;
      
      
      Product2 pf = TestDataFactory.insertRelatedProducts();

      /*Commented-Darshan
      List<Product2> rp = [select id,name from Product2 where id in (select Apttus_Config2__RelatedProductId__c  from
        Apttus_Config2__RelatedProduct__c where Apttus_Config2__ProductId__c = :pf.id and Apttus_Config2__RelatedProductId__r.name like 'NA Territory%')];
      */
      //Apttus Decommission - Darshan - Start 
     
      List<Product2> rp = [select id,name from Product2 where id in (select RelatedProduct__c  from
        RelatedProduct__c where Product__c = :pf.id /*and RelatedProduct__c.name like 'NA Territory%'*/)];
      //END   
        for(Product2 prodObj : rp){
            
            prodObj.Market__c  = userLstUK[0].Market__c;
            prodObj.Business_Unit__c  = userLstUK[0].Product_Business_Unit__c;
            prodObj.Line_of_Business__c  = userLstUK[0].Line_of_Business__c;
            
            
        }
        update rp;
        
    List<Opportunity> opptyLst = TestDataFactory.createOpportunity(10 , 'Global Opportunity');
    for(Opportunity opptyObj :opptyLst ){
        opptyObj.accountid = Acc[0].id;
        opptyObj.Selling_Period_Year__c = '2014';
    }
    insert opptyLst;

     Test.startTest();
     List<Asset> asstlist=new List<Asset>();
      for(integer i=0;i<courselist.size();i++){
        Asset asset = new Asset();
       asset.name = 'TerritoryAsset'+i;
       asset.Product2Id = rp[i].id;
       asset.AccountId = Acc[0].id;
       asset.Course__c = courselist[i].id;
       asset.Primary__c = True;
       asset.Status__c = 'Active';
       asset.Expiry_Date__c = date.newInstance(system.Date.today().year(),system.Date.today().month(),10);
       if(i<=2){
           asset.Opportunity__c = opptyLst[i].id;
           
       }
       asstlist.add(asset);
      } 
      insert asstlist;
      List<Course_Product_in_Use__c> coursePITLSt = new List<Course_Product_in_Use__c>();
      for(Asset asstObj : asstlist){
          Course_Product_in_Use__c coursePIUObj = new Course_Product_in_Use__c();
          coursePIUObj.Course__c = asstObj.Course__c;
          coursePIUObj.Product_in_Use__c = asstObj.id;
          coursePIUObj.Status__c = 'Active';
          coursePITLSt.add(coursePIUObj);
      }
      insert coursePITLSt;
      
      /*List<Apttus_Config2__ProductClassification__c> prdclasslist=new List<Apttus_Config2__ProductClassification__c>();
      for(integer i=0;i<courselist.size();i++){
     
      Apttus_Config2__ProductClassification__c pc = new Apttus_Config2__ProductClassification__c();
      pc.Apttus_Config2__ClassificationId__c = pcelist[i].Pearson_Course_Code__c;
      pc.CurrencyIsoCode = 'USD';
      pc.Apttus_Config2__ProductId__c = rp[i].id;
     
     
      prdclasslist.add(pc);
      }
      insert prdclasslist;*/
      
      // CR-02949-Apttus Decommission(Changes) RG:
      List<HierarchyProduct__c> prdclasslist=new List<HierarchyProduct__c>();
      for(integer i=0;i<courselist.size();i++){
     
      HierarchyProduct__c pc = new HierarchyProduct__c();
      pc.ProductCategory__c= pcelist[i].Pearson_Course_Code_Hierarchy__c;
      pc.CurrencyIsoCode = 'USD';
      pc.Product__c= rp[i].id;     
     
      prdclasslist.add(pc);
      }
      insert prdclasslist;
      
      //Test.startTest();  
      
      Territory2 tt = TestDataFactory.createTerritory();
      tt.Territory_Code__c='TCA';
      insert tt;      
      //code written to assign a user to territory
      UserTerritory2Association ut2a = new UserTerritory2Association();
      ut2a.Territory2Id = tt.id;    //'0MIg00000000A4b';    //testTerritory.id;
      ut2a.UserId = userLstUK[0].id;
      insert ut2a;

      
        Product2 prod2 = new Product2();
        prod2.name = 'TerritoryProduct2';
        prod2.Competitor_Product__c = false; 
        prod2.Next_Edition__c = rp[0].id;
        prod2.Relevance_Value__c = 10;
        prod2.Business_Unit__c = userLstUK[0].Business_Unit__c;
        prod2.Market__c = userLstUK[0].Market__c;
        prod2.Line_of_Business__c = userLstUK[0].Line_of_Business__c;
        prod2.Supplement_Type__c = 'Main Title';
        insert prod2;  
       
        List<Program_Family_Member__c> famMemeLst  = TestDataFactory.insertPrgFamilyMemebers(prodFamLst,prod2);
        insert famMemeLst;
       
        //code for creating PriceListItem
       /*Commented-Darshan
        Apttus_Config2__PriceListItem__c pli = new Apttus_Config2__PriceListItem__c();
        pli.Apttus_Config2__PriceListId__c = priceList.id;
        pli.Apttus_Config2__ProductId__c =  prod2.id; 
       */
       //Apttus Decommission-Darshan-Start
      
        CatalogPrice__c pli = new CatalogPrice__c();
        pli.Catalog__c = priceList.id;
        pli.Product__c =  prod2.id;              
        insert pli;
          
           List<User> userLst = TestDataFactory.createUser(Userinfo.getProfileid() , 5);
          insert userLst;
          UniversityCourse__c course = new UniversityCourse__c();
          course.Name = 'TerritoryCourseNameandcode1';
          course.Account__c = Acc[0].id;
          course.Catalog_Code__c = 'Territorycoursecode1';
          course.Course_Name__c = 'Territorycoursename1';
          course.CurrencyIsoCode = 'AUD';
          insert course;
          Generate_Potential_Target__c gpt = new Generate_Potential_Target__c();
          gpt.product__c=prod2.id;
           gpt.Status__c ='Validation In Progress';
             gpt.Opportunity_Type__c='New';
             gpt.Course__c=course.id;
            insert gpt;
        
              
        PS_GeneratePotentialTargetsGlobal gpt1 = new PS_GeneratePotentialTargetsGlobal(Acc[0].id);
        PS_GeneratePotentialTargetsGlobal.createPotentialTargets();
          PS_GeneratePotentialTargetsGlobal.Future_validatetakeaway(userLst[0].id, 'test');
          
          
        Set<ID> Accid = New Set<ID>();
        Set<ID> Catid = New Set<ID>();
        for(Account testacc : Acc){ 
            Accid.add(testacc.id);  
         }
       /*
         for(Apttus_Config2__ClassificationName__c testcat: cat){
            catid.add(testcat.id); 
         }
       */
       
       //Apttus Decommission-Darshan-Start
       for(ProductCategory__c testcat: cat){
            catid.add(testcat.id); 
         }
        System.enqueueJob(new PS_PotentialTargetQueuable(userLstUK[0].id,Accid,Catid)); 
        }// run as close
   Test.stopTest(); 

    }
    static testMethod void targetGenerationANZ()
    {
     
     PS_Util.isFetchedUserRecord  = false;
      //code for creating an User
      User u = new User();
      u.LastName = 'territoryuser';
      u.alias = 'terrusr'; 
      u.Email = 'territoryuser@gmail.com';  
      u.Username='territoryuser@gmail.com';
      u.LanguageLocaleKey='en_US'; 
      u.TimeZoneSidKey='America/New_York';
      u.Price_List__c='Humanities & Social Science';
      u.LocaleSidKey='en_US';
      u.EmailEncodingKey='ISO-8859-1';
      u.ProfileId = Userinfo.getProfileId();         
      u.Geography__c = 'Growth';
      u.Market__c = 'AU';
      u.Business_Unit__c = 'US Field Sales';
      u.Line_of_Business__c = 'Higher Ed';
      u.isactive=true;
      u.License_Pools__c='Test User ';
      u.CurrencyIsoCode='AUD';
      u.Product_Business_Unit__c = 'CTIPIHE';
      insert u;       
      
      Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u.id,Disable_Triggers__c=true);
      insert byp; 
             
      /* Commented-Darshan
      List<Apttus_Config2__ClassificationName__c> cat = new List<Apttus_Config2__ClassificationName__c>();
      Apttus_Config2__ClassificationName__c catresult = New Apttus_Config2__ClassificationName__c();
       catresult.Name='TestClassification';
       catresult.Apttus_Config2__Active__c=true;
       catresult.Apttus_Config2__HierarchyLabel__c='TestClassification';
       cat.add(catresult);
       insert cat;   
      Apttus_Config2__PriceList__c priceList = new Apttus_Config2__PriceList__c(Name='Humanities & Social Science',Apttus_Config2__Active__c=true);                  
      insert  priceList;     
      Apttus_Config2__PriceListCategory__c pcat = new Apttus_Config2__PriceListCategory__c(Apttus_Config2__PriceListId__c=priceList.Id,Apttus_Config2__HierarchyId__c=cat[0].Id,Apttus_Config2__Sequence__c=1);
      insert pcat;
      */
      //Apttus Decommission-Darshan-Start
    
      List<ProductCategory__c> cat = new List<ProductCategory__c>();
      ProductCategory__c catresult = New ProductCategory__c();
       catresult.Name='TestClassification';
      // catresult.isActive__c  =true;
       catresult.HierarchyLabel__c='TestClassification';
       cat.add(catresult);
       insert cat;   
      Catalog__c priceList = new Catalog__c(Name='Humanities & Social Science',isActive__c=true);                  
      insert  priceList;     
      Catalog_Offering__c pcat = new Catalog_Offering__c(Catalog__c=priceList.Id,ProductCategory__c=cat[0].Id,Sequence__c=1);
      insert pcat;
     
         
     
   
       List<Account> Acc = TestDataFactory.createAccount(1, 'Organisation');
       Acc[0].Territory_Code_s__c = '2ZZ'; 
       insert Acc;      
      PS_Util.isFetchedUserRecord  = false;
      System.runAs(u){
   
          //code for creating course
      List<UniversityCourse__c> courselist=new List<UniversityCourse__c>();
      for(integer i=0;i<3;i++){
          UniversityCourse__c course = new UniversityCourse__c();
          course.Name = 'TerritoryCourseNameandcode'+i;
          course.Account__c = Acc[0].id;
          course.Catalog_Code__c = 'Territorycoursecode'+i;
          course.Course_Name__c = 'Territorycoursename'+i;
          course.CurrencyIsoCode = 'AUD';
          courselist.add(course);
      }

      insert courselist;
    //  Apttus_Config2__ClassificationName__c  Cn = [Select id,name from Apttus_Config2__ClassificationName__c where name=:u.Price_List__c];
      
      //code for inserting Category Hierarchy 
      /*
      Apttus_Config2__ClassificationHierarchy__c categoryhierarchy = new Apttus_Config2__ClassificationHierarchy__c();
      categoryhierarchy.Apttus_Config2__HierarchyId__c = cat[0].id;   //'a1cg00000003ePw';
      categoryhierarchy.Apttus_Config2__Label__c = 'TestCategoryHierarchy';
      */
      //Apttus Decommission-Darshan-Start
    
      Hierarchy__c categoryhierarchy = new Hierarchy__c();
      categoryhierarchy.ProductCategory__c = cat[0].id;   //'a1cg00000003ePw';
      categoryhierarchy.Label__c = 'TestCategoryHierarchy';      
      categoryhierarchy.Name = 'TestCategoryHierarchy';
      insert categoryhierarchy; 
      
      List<Pearson_Course_Equivalent__c> pcelist=new List<Pearson_Course_Equivalent__c>();
      List<Intake__c> intakeLst=new List<Intake__c>();

      for(integer i=0;i<courselist.size();i++){
          Pearson_Course_Equivalent__c pce = new Pearson_Course_Equivalent__c();
          pce.Active__c = true;   
          pce.Course__c =courselist[i].id; 
          pce.Primary__c = true;
          pce.Pearson_Course_Code_Hierarchy__c= categoryhierarchy.id;
          pcelist.add(pce);
           Intake__c intakeObj = new Intake__c();
          if(i==0){
            intakeObj.Start_Date__c = date.newInstance(system.Date.today().year(),2,10);
          }
          else{
            intakeObj.Start_Date__c = date.newInstance(system.Date.today().year(),8,10);
              
          }
          intakeObj.Enrollments__c = 100;
          intakeObj.University_Course__c = courselist[i].id;
          intakeLst.add(intakeObj);
      }
      insert pcelist; 
      insert intakeLst;
      
      Product2 pf = TestDataFactory.insertRelatedProducts();

    /* Commented-Darshan   
    List<Product2> rp = [select id,name from Product2 where id in (select Apttus_Config2__RelatedProductId__c  from
        Apttus_Config2__RelatedProduct__c where Apttus_Config2__ProductId__c = :pf.id and Apttus_Config2__RelatedProductId__r.name like 'NA Territory%')];
     */  
    //Apttus Decommission-Darshan-Start
   
    List<Product2> rp = [select id,name from Product2 where id in (select RelatedProduct__c  from
        RelatedProduct__c where Product__c = :pf.id /*and RelatedProduct__c.name like 'NA Territory%'*/)];
      
        for(Product2 prodObj : rp){
            
            prodObj.Market__c  = u.Market__c;
            prodObj.Business_Unit__c  = u.Product_Business_Unit__c;
            prodObj.Line_of_Business__c  = u.Line_of_Business__c;
            
            
        }
        update rp;
        
    List<Opportunity> opptyLst = TestDataFactory.createOpportunity(10 , 'Global Opportunity');  //RG
    for(Opportunity opptyObj :opptyLst ){
        opptyObj.accountid = Acc[0].id;
        opptyObj.Selling_Period_Year__c = '2014';
    }
    insert opptyLst;
    Order orderObj = TestDataFactory.returnorder();
      Test.startTest();
     List<Asset> asstlist=new List<Asset>();
      for(integer i=0;i<courselist.size();i++){
        Asset asset = new Asset();
       asset.name = 'TerritoryAsset'+i;
       asset.Product2Id = rp[i].id;
       asset.AccountId = Acc[0].id;
       asset.Primary__c = True;
       asset.Status__c = 'Active';
       asset.Expiry_Date__c = date.newInstance(2016,03,31);
       if(i==2){
           asset.Opportunity__c = opptyLst[i].id;
           
       }
       if(i==1){
           
           asset.Order__c = orderObj.id;
       }
       asstlist.add(asset);
      } 
      insert asstlist;
      
      /* RG: List<Apttus_Config2__ProductClassification__c> prdclasslist=new List<Apttus_Config2__ProductClassification__c>();
      for(integer i=0;i<courselist.size();i++){
      
      Apttus_Config2__ProductClassification__c pc = new Apttus_Config2__ProductClassification__c();
      pc.Apttus_Config2__ClassificationId__c = pcelist[i].Pearson_Course_Code__c;
      pc.CurrencyIsoCode = 'USD';
      pc.Apttus_Config2__ProductId__c = rp[i].id;
      
     
      prdclasslist.add(pc);
      }
      insert prdclasslist; RG */
      
      // CR-02949-Apttus Decommission(Changes) RG:
      List<HierarchyProduct__c> prdclasslist=new List<HierarchyProduct__c>();
      for(integer i=0;i<courselist.size();i++){
      
      HierarchyProduct__c pc = new HierarchyProduct__c();
      pc.ProductCategory__c= pcelist[i].Pearson_Course_Code_Hierarchy__c;
      pc.CurrencyIsoCode = 'USD';
      pc.Product__c= rp[i].id;
      
     
      prdclasslist.add(pc);
      }
      insert prdclasslist;
      
     // Test.startTest();  
      
      Territory2 tt = TestDataFactory.createTerritory();
      tt.Territory_Code__c='TCA';
      insert tt;      
      //code written to assign a user to territory
      UserTerritory2Association ut2a = new UserTerritory2Association();
      ut2a.Territory2Id = tt.id;    //'0MIg00000000A4b';    //testTerritory.id;
      ut2a.UserId = u.id;
      insert ut2a;
      UserTerritory2Association ut2aquery=[select id,User.Name,Territory2.Name from UserTerritory2Association where id=:ut2a.id];
      
        Product2 prod2 = new Product2();
        prod2.name = 'TerritoryProduct2';
        prod2.Competitor_Product__c = false; 
        prod2.Next_Edition__c = rp[0].id;
        prod2.Relevance_Value__c = 10;
        prod2.Business_Unit__c = u.Business_Unit__c;
        prod2.Market__c = u.Market__c;
        prod2.Line_of_Business__c = u.Line_of_Business__c;
        insert prod2;  
       
          //code for mapping product to product classififcation
      /* Commented-Darshan
         Apttus_Config2__ProductClassification__c pc = new Apttus_Config2__ProductClassification__c();
         pc.Apttus_Config2__ClassificationId__c = pcelist[0].Pearson_Course_Code__c;
         pc.CurrencyIsoCode = 'USD';
         pc.Apttus_Config2__ProductId__c = prod2.id;
         insert pc; 
       */
        //Apttus Decommission-Darshan-Start
        
        HierarchyProduct__c pc = new HierarchyProduct__c();
         pc.ProductCategory__c = pcelist[0].Pearson_Course_Code_Hierarchy__c;
         pc.CurrencyIsoCode = 'USD';
         pc.Product__c = prod2.id;
         insert pc;
              
         
        //code for mapping the related products
       /* Commented-Darshan
        Apttus_Config2__RelatedProduct__c rps = new Apttus_Config2__RelatedProduct__c();
        rps.Apttus_Config2__ProductId__c = prod2.id; 
        rps.Apttus_Config2__RelatedProductId__c = prod2.id; 
        rps.CurrencyIsoCode = 'USD';
        rps.PSELL__c = true;
        insert rps; */
       
        //Apttus Decommission-Darshan-Start
        
        RelatedProduct__c rps = new RelatedProduct__c();
        rps.Product__c = prod2.id; 
        rps.RelatedProduct__c = prod2.id; 
        rps.CurrencyIsoCode = 'USD';
        rps.PSELL__c = true;
        insert rps; 
        //END
        
        //code for creating PriceListItem
      /* Commented-Darshan  
        Apttus_Config2__PriceListItem__c pli = new Apttus_Config2__PriceListItem__c();
        pli.Apttus_Config2__PriceListId__c = priceList.id;
        pli.Apttus_Config2__ProductId__c =  prod2.id; 
        insert pli;
       */
        //Apttus Decommission-Darshan-Start
        
        CatalogPrice__c pli = new CatalogPrice__c();
        pli.Catalog__c = priceList.id;
        pli.Product__c =  prod2.id; 
        insert pli;
           
             
        PS_GeneratePotentialTargetsGlobal gpt = new PS_GeneratePotentialTargetsGlobal(Acc[0].id);
        PS_GeneratePotentialTargetsGlobal.createPotentialTargets();
        
        Set<ID> Accid = New Set<ID>();
        Set<ID> Catid = New Set<ID>();
        for(Account testacc : Acc){ 
            Accid.add(testacc.id);  
         }
       /*Commented-Darshan
           for(Apttus_Config2__ClassificationName__c testcat: cat){
            catid.add(testcat.id); 
         }
       */
       //Apttus decommission-START
       
       for(ProductCategory__c testcat: cat){
            catid.add(testcat.id); 
         }
          
        System.enqueueJob(new PS_PotentialTargetQueuable(u.id,Accid,Catid)); 
        }// run as close
   Test.stopTest(); 

    }
}