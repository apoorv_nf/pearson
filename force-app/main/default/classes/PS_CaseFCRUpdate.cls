/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_CaseFCRUpdate.cls 
Description:     on Before Insert or update 
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
04/12/2015         1.1            AJAY                   RD-01600 - FCR on Cases
09/03/2016         1.2            AJAY                   Updated according to SME code review  
09/06/2019         1.3            Ramesh                 CR-02918 Incorrect FCR logic 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
Public Class PS_CaseFCRUpdate
{
   /**
    * Description : Method to update FCR field on Case Closure without ownership change from one agent to another.
    * @param     NA
    * @return    NA
    * @throws    NA
    **/
    public static void mSetFCROnCaseClosure(List<case> oNewCases,boolean isInsert,boolean isUpdate,Map<Id,case> oOldCaseMap){
        system.debug('IN mSetFCROnCaseClosure.PS_CaseFCRUpdate');
        List<Id> sClosedCaseId= new list<Id>();
        Map<String,Integer> sCaseOwnerCount = new Map<String,Integer>();
        Integer count=0,iCOTcount=0;
        String sCaseId='',sCaseOwner='',user='005';
        //PP:12/14/2015: Added to check Case Service Record types
        List<Case> oNewCaseList = new List<Case>();
        oNewCaseList = CommonUtils.getCaseServiceRecordType(System.Label.PS_ServiceRecordTypes, oNewCases ); 
        if (oNewCaseList!= null){
            if(isInsert){
              for(Case c: oNewCaseList){
                if(c.status==System.Label.PS_CaseClosureStatus){ 
                    sClosedCaseId.add(c.id);
                } 
              } //end of for loop
            }
            if(isUpdate){
              for(Case c: oNewCaseList){
                if((c.status!=oOldCaseMap.get(c.id).status) && c.status==System.Label.PS_CaseClosureStatus){ 
                    sClosedCaseId.add(c.id);
                } 
              } //end of for loop
            }
         }//end of if 

        if(!sClosedCaseId.IsEmpty()){            
            //Added by Ramesh for CR-02918 on 09/06/2019
            //Start - CR-02918
            list<string> userNames = new list<string>();
            list<user> lsuser = new list<user>();
            //if(!Test.isRunningTest()){
                 lsuser = [select Name from user where FCR_Bypass__c = true and IsActive = true limit 1000];
            //}
            for(User users: lsuser){
                userNames.add(users.Name);
            }
            //Ends - CR-02918
            
            for(AggregateResult sClosedCount :[select count(id) cnt, case__c c  from Case_Owner_Tracking__c where Agent_Name__r.Name not in:userNames and case__c in : sClosedCaseId group by case__c limit 1])  //Modified by Ramesh for CR-02918 on 09/06/2019
            {
                iCOTcount=(integer)sClosedCount.get('cnt');
                sCaseId=(String)sClosedCount.get('c');
                sCaseOwnerCount.put(sCaseId,iCOTcount);
            }//end of for
            for(Case c : oNewCaseList){
                sCaseOwner = (String) c.OwnerId;
                if(sCaseOwnerCount.containsKey(c.id)&& !sCaseOwnerCount.IsEmpty()){
                    count=integer.valueof(sCaseOwnerCount.get(c.id));
                    }
                else{
                    count=0;
                    }
                if (count <= 1 && sCaseOwner.startsWith(user) && c.isEscalated == false){
                    c.FCR__c = true;
                }   
            }//end of for
        }//end of if
        
    } //end method
}