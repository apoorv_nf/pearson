/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_UpdateCaseOwnerTracking.cls 
   Description:     on after update 
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  04/12/2015         1.1            IDC                    Creating CaseOwnerTracking On case update.
  10/03/2016         1.2            KP                     Amended Code for new enhancements.
  14/03/2016         1.3            AJAY                   Updated code according to SME Review.
  24/03/2016         1.4            AJAY                   Updated code for new case with closed status.
  20/07/2016         1.5            RG                     Updated code for DR-0774
------------------------------------------------------------------------------------------------------------------------------------------------------------- */
Public Class PS_UpdateCaseOwnerTracking
{
    //Variable declaration
    static Map<id,id> sNewCaseQueueId = new map<id,id>();//Init case Id
    static Map<id,id> sClosedCaseId= new map<id,id>(); //init closed case scenarios
    static Map<id,id> sResolvedCaseId= new map<id,id>(); //init re-opened cases
    static Set<ID> sUserCaseId = new set<ID> (); //init case id of changes from user to user
    static Map<Id,case> sQueueUserCase ; //init case for user to user ownership change
    static Map<id,Group> sCaseQueueRecord ; //init for queue details
    static Map<Id,Case> sUpdateCaseOwnerTrack;
    static Map<Id,Case> sRe_OpenedCaseOwnerTrack;    
    static List<Case_Owner_Tracking__c> sNewCaseOwnerTrack = new list<Case_Owner_Tracking__c>();
    static List<Case_Owner_Tracking__c> sUpCaseOwnerTrack = new list<Case_Owner_Tracking__c>();
    static Case_Owner_Tracking__c oNewCot = new Case_Owner_Tracking__c();
    static String USER='005',QUEUE='00G';
    static boolean bchatreopen=false;
   /**
    * Description : Method to process owner history from parent case
    * @param NA
    * @return NA
    * @throws NA
    **/    
    public static void mCreateCaseOwnerTracking(List<case> oNewcases,Map<Id,case> oOldCaseMap , Map<id,Case> oNewCaseMap){
        system.debug('Eneter mCreateCaseOwnerTracking');              
        List<Case> oNewCaseList= CommonUtils.getCaseServiceRecordType(System.Label.PS_ServiceRecordTypes, oNewcases ); 
        if (oNewCaseList != null){
            mInitCasetoProcess(oNewCaseList,oOldCaseMap,oNewCaseMap);
            mFetchData();
            if (oOldCaseMap != null){mProcessCaseforUpdate(oNewCaseList,oOldCaseMap,oNewCaseMap);}
            else{mProcessCaseforNew(oNewCaseList,oNewCaseMap);}
        }//end service case
        if(sNewCaseOwnerTrack.size()>0){
            try{
                 Database.SaveResult[] sNewCaseOwnerTrackResult = Database.Insert(sNewCaseOwnerTrack,false);
                     if (sNewCaseOwnerTrackResult != null){
                         List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
                         for (Database.SaveResult sr : sNewCaseOwnerTrackResult) {
                             String ErrMsg='';
                             if (!sr.isSuccess() || Test.isRunningTest()){
                                PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                                errlogger.InterfaceName__c='Case agent handle time';
                                errlogger.ApexClassName__c='PS_UpdateCaseOwnerTracking';
                                errlogger.CallingMethod__c='Execute';
                                errlogger.UserLogin__c=UserInfo.getUserName(); 
                                errlogger.recordid__c=sr.getId();
                                for(Database.Error err : sr.getErrors()) 
                                  ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage(); 
                                errlogger.ExceptionMessage__c=  ErrMsg;  
                                errloggerlist.add(errlogger);
                              }    
                          }
                        if(errloggerlist.size()>0){Insert errloggerlist;}
                     }
                }//end of Try
            catch(Exception e){
                ExceptionFramework.LogException('PS_UpdateCaseOwnerTracking','PS_UpdateCaseOwnerTracking','mCreateCaseOwnerTracking',e.getMessage(),UserInfo.getUserName(),'');
            }
        }// end of if
        if(sUpCaseOwnerTrack.size()>0 || Test.isRunningTest()){
        try{
             Database.SaveResult[] sUpCaseOwnerTrackResult = Database.update(sUpCaseOwnerTrack,false);
             if (sUpCaseOwnerTrackResult != null){
                    List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
                    for (Database.SaveResult sr : sUpCaseOwnerTrackResult) {
                        String ErrMsg='';
                        if (!sr.isSuccess() || Test.isRunningTest()){
                            PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                            errlogger.InterfaceName__c='Case agent handle time';
                            errlogger.ApexClassName__c='PS_UpdateCaseOwnerTracking';
                            errlogger.CallingMethod__c='Execute';
                            errlogger.UserLogin__c=UserInfo.getUserName(); 
                            errlogger.recordid__c=sr.getId();
                               for(Database.Error err : sr.getErrors()) 
                                  ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage(); 
                            errlogger.ExceptionMessage__c=  ErrMsg;  
                            errloggerlist.add(errlogger);
                        }    
                     }
                        if(errloggerlist.size()>0){update errloggerlist;}
              }//end of if
           }//end of try
        catch(Exception e){
            ExceptionFramework.LogException('PS_UpdateCaseOwnerTracking','PS_UpdateCaseOwnerTracking','mCreateCaseOwnerTracking',e.getMessage(),UserInfo.getUserName(),'');
        }
       }//end of if            
    }//end method mCreateCaseOwnerTracking
    
   /**
    * Description : Method to init variables
    * @param NA
    * @return NA
    * @throws NA
    **/    
    public static void mInitCasetoProcess(List<case> oNewCaseList,Map<Id,case> oOldCaseMap , Map<id,Case> oNewCaseMap){    
        sNewCaseOwnerTrack.clear();
        sUpCaseOwnerTrack.clear();
        sNewCaseQueueId.clear();
        sClosedCaseId.clear();
        sResolvedCaseId.clear();
        sUserCaseId.clear();
        if(oOldCaseMap != null){
            for(case c: oNewCaseList){
                system.debug(oOldCaseMap.get(c.id).Status+';'+oOldCaseMap.get(c.id).ownerid);
                system.debug(oNewCaseMap.get(c.id).Status+';'+oNewCaseMap.get(c.id).ownerid);
                //update ownership from queue to user
                if(string.ValueOf(oNewCaseMap.get(c.id).Ownerid).startsWith(USER) && 
                   string.ValueOf(oOldCaseMap.get(c.id).Ownerid).startsWith(QUEUE)) {   
                   system.debug('queue to user:'); 
                   sNewCaseQueueId.put(c.id,oOldCaseMap.get(c.id).Ownerid);
                   bchatreopen=true;
                }
                //update on case status to closure
                if((oNewCaseMap.get(c.id).Status != oOldCaseMap.get(c.id).Status) && 
                  oNewCaseMap.get(c.id).Status==Label.PS_CaseClosureStatus){
                   sClosedCaseId.put(c.id,c.ownerid);
                   system.debug('Values in case status to closure c.ownerid:->'+c.ownerid);
                   system.debug('Values in sClosedCaseId:->'+sClosedCaseId);
                }
                //update case ownership from user to user
                if(string.valueof(oOldCaseMap.get(c.id).Ownerid).startsWith(USER) && 
                  string.ValueOf(oNewCaseMap.get(c.id).Ownerid).startsWith(USER) && oOldCaseMap.get(c.id).Ownerid != oNewCaseMap.get(c.id).Ownerid){
                    system.debug('user to user:'); 
                    sUserCaseId.add(c.id);
                    bchatreopen=true;
                }     
                //if(oOldCaseMap.get(c.id).Status==Label.PS_CaseClosureStatus && oNewCaseMap.get(c.id).Reopened__c == true){
                if(oOldCaseMap.get(c.id).Status==Label.PS_CaseClosureStatus && (oNewCaseMap.get(c.id).Status != oOldCaseMap.get(c.id).Status||oNewCaseMap.get(c.id).Reopened__c == true)){
                   system.debug('closed to re-open'); 
                   if (bchatreopen)
                       sResolvedCaseId.put(c.id,oOldCaseMap.get(c.id).ownerid);
                   else
                       sResolvedCaseId.put(c.id,c.ownerid);
                   system.debug('Values in sResolvedCaseId:->'+sResolvedCaseId);
                }                                                                                 
            }//end for case loop
        }//end oldcase map    
    }//end method mInitCasetoProcess
    
    /**
    * Description : Method to fetch data from salesforce
    * @param NA
    * @return NA
    * @throws NA
    **/    
    public static void mFetchData(){
        if(sUserCaseId.size()>0){    
            sQueueUserCase = new Map<id,case>([select id,(select id,tier__c from Case_Owner_Trackings__r  
                                              order by createddate desc limit 1) from case where ID IN :sUserCaseId]);
            system.debug('Value is sQueueUserCase :->'+sQueueUserCase );                                  
        }     
        if(!sNewCaseQueueId.IsEmpty()){
            sCaseQueueRecord = new Map<id,Group>([Select Id,name From Group where Type = 'QUEUE' and id IN :sNewCaseQueueId.values()]); 
            system.debug('Value is sCaseQueueRecord :->'+sCaseQueueRecord );  
        }       
        if(!sClosedCaseId.IsEmpty()){
            sUpdateCaseOwnerTrack = new Map<Id,Case>([select id,(select id,Resolved__c,Agent_Name__c,tier__c from 
                                                    Case_Owner_Trackings__r where Agent_Name__c IN :sClosedCaseId.Values() ORDER BY CreatedDate DESC limit 1 ) 
                                                    from Case where Id in:sClosedCaseId.Keyset()]);
            system.debug('Value is sUpdateCaseOwnerTrack :->'+sUpdateCaseOwnerTrack);                                        
        }
        if(!sResolvedCaseId.IsEmpty()){
            sRe_OpenedCaseOwnerTrack = new Map<Id,Case>([select id,(select id,Resolved__c,Agent_Name__c,tier__c from 
                                                    Case_Owner_Trackings__r where Agent_Name__c IN :sResolvedCaseId.Values() ORDER BY CreatedDate DESC) 
                                                    from Case where Id in:sResolvedCaseId.Keyset()]);
            system.debug('Value is sRe_OpenedCaseOwnerTrack:->'+sRe_OpenedCaseOwnerTrack );                                        
        }             
    } //end method mFetchData
    
    /**
    * Description : Method to update owner history for case updates
    * @param NA
    * @return NA
    * @throws NA
    **/
    public static void mProcessCaseforUpdate(List<case> oNewCaseList,Map<Id,case> oOldCaseMap , Map<id,Case> oNewCaseMap){    
        Case_Owner_Tracking__c oCOT = new Case_Owner_Tracking__c () ;
        for(case c: oNewCaseList){
            if(oOldCaseMap.get(c.id).Ownerid != oNewCaseMap.get(c.id).Ownerid){//owner update
                if(sUserCaseId.Contains(c.id)){ //user to user update
                    Case_Owner_Tracking__c oOT = new Case_Owner_Tracking__c (Agent_Name__c =c.ownerid,  //,OwnerId =c.ownerid
                                                                            Case__c = c.id,Agent_Handle_Time__c=0);
                    if(sQueueUserCase.get(c.id).Case_Owner_Trackings__r.size()>0){
                        Case_Owner_Tracking__c oQueueUserCOT = sQueueUserCase.get(c.id).Case_Owner_Trackings__r;
                        if(oQueueUserCOT.Tier__c != null){ //for phone case when user converts to a user there will be no CaseOwnerTracking
                            oOT.Tier__c = oQueueUserCOT.Tier__c;
                        }
                    }
                    sNewCaseOwnerTrack.add(oOT); 
                }//end user to user change   
                if(sNewCaseQueueId.containsKey(c.id)){//Queue to a user                
                    String sTier=sCaseQueueRecord.get(oOldCaseMap.get(c.id).Ownerid).name;
                    Case_Owner_Tracking__c oOT = new Case_Owner_Tracking__c(Agent_Name__c =c.ownerid, //OwnerId =c.ownerid,
                                                 Case__c = c.id,Agent_Handle_Time__c=0,Tier__c=sTier);
                    sNewCaseOwnerTrack.add(oOT);
                }//end queue to use change                                
            }//end owner update scenarios
            
            if (sUpdateCaseOwnerTrack != null ){//case closure update            
                 if(sUpdateCaseOwnerTrack.containsKey(c.id)){
                     for(Case_Owner_Tracking__c oCOTClosed :sUpdateCaseOwnerTrack.get(c.id).Case_Owner_Trackings__r){
                        if (oCOTClosed.Agent_Name__c == c.ownerId){
                            oCOTClosed.Resolved__c =true;
                            oCOTClosed.CaseClosed__c=true;
                            sUpCaseOwnerTrack.add(oCOTClosed);
                            system.debug('Value in sUpCaseOwnerTrack:->'+sUpCaseOwnerTrack); 
                        }                         
                     }//end for
                 }//end map contains case
            }//end case closure
            if (sRe_OpenedCaseOwnerTrack != null){//case re-open
                if(sRe_OpenedCaseOwnerTrack.containsKey(c.id)){
                    String sTier='';
                    for(Case_Owner_Tracking__c oCOTCaseReOpen :sRe_OpenedCaseOwnerTrack.get(c.id).Case_Owner_Trackings__r){
                        if (oCOTCaseReOpen.Resolved__c==true){
                            oCOTCaseReOpen.Resolved__c =false;
                            sUpCaseOwnerTrack.add(oCOTCaseReOpen);
                            system.debug('Value in sUpCaseOwnerTrack:->'+sUpCaseOwnerTrack);    
                            sTier=oCOTCaseReOpen.Tier__c;
                        }
                    }//end for
                    if(!bchatreopen){
                        Case_Owner_Tracking__c oOT = new Case_Owner_Tracking__c(Agent_Name__c =c.ownerid, //OwnerId =c.ownerid,
                                                     Case__c = c.id,Agent_Handle_Time__c=0,Tier__c=sTier);                    
                        sNewCaseOwnerTrack.add(oOT);
                    }
                    system.debug('Value in sNewCaseOwnerTrack:->'+sNewCaseOwnerTrack);                      
                }
            }//end case reopen
        }//end new case loop    
    }//end method mProcessCaseforUpdate

    /**
    * Description : Method to update owner history for case updates
    * @param NA
    * @return NA
    * @throws NA
    **/
    public static void mProcessCaseforNew(List<case> oNewCaseList,Map<id,Case> oNewCaseMap){
        User oduser = [select id from user where alias=: Label.PS_Deployment_User and isactive=true limit 1];
                for(case c: oNewCaseList){
            if(string.valueof(c.ownerid).startsWith(USER) && c.ownerid != oduser.id && c.origin != 'Web'){
                oNewCot.Agent_Name__c =c.ownerid;
                oNewCot.Case__c = c.id;
                if(c.Status=='Closed'){
                oNewCot.Resolved__c=true;
                }
                sNewCaseOwnerTrack.add(oNewCot);
            }
        }
        
    }//end method   mProcessCaseforNew  
               
}//end class