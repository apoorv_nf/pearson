/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            Dynamiclookup.cls 
Description:     Lightning controller class for InputLookupDynamic
Test class:      NA 
Date             Version              Author                          Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
26-JUN-2017         1.0     Manikanta Nagubilli             Created to get the results for input lookup component
05-MAR-2018         2.0     Veera Athmakuri                 Updated code as per the CR-01782
08-AUG-2019         3.0     Raja Gopalarao Bekkam           Updated the Product search logic as per CR-02618
------------------------------------------------------------------------------------------------------------------------------------------------------------ */

public without sharing class Dynamiclookup
{
    @AuraEnabled
    public static List<sObject> getdataList(String Country,String searchValue, String searchField,string sobjectType,string sobjectFields,boolean isDefault,string lob)
    {
        Id AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
        List<sObject> sobjectList = new List<sObject>();
        String query;
        //UK Community -Start
        if(Country == 'UK'){
            //Modified on 08-AUG-19 as per CR-02618 by Bekkam Raja Gopalarao B
            query = 'SELECT '+sobjectFields+' FROM '+sobjectType+' WHERE    Business_Vertical__c= \''+ String.escapeSingleQuotes(lob)+'\''+' '+'AND'+' '+searchField+' LIKE \''+'%' + String.escapeSingleQuotes(searchValue.trim())+ '%\''+' '+'AND Active_in_Self_Service__c = True AND Product_Group__c=\'UK Product\' Order by Name ASC limit 500'; 
        }
        //UK Community -End
        else if(sobjectType != 'Account')
        {
            if(lob != null && lob == 'Clinical')
                //Modified on 08-AUG-19 as per CR-02618 by Bekkam Raja Gopalarao B
                query = 'SELECT '+sobjectFields+' FROM '+sobjectType+' WHERE Line_of_Bussiness__c = \''+ String.escapeSingleQuotes(lob)+'\''+' '+'AND'+' '+searchField+' LIKE \''+'%' + String.escapeSingleQuotes(searchValue.trim())+ '%\''+' '+'AND Active_in_Self_Service__c = True Order by Name ASC limit 500'; 
            else
                //Modified on 08-AUG-19 as per CR-02618 by Bekkam Raja Gopalarao B
                query = 'SELECT '+sobjectFields+' FROM '+sobjectType+' WHERE Product_Group__c!= \'Global ELT\' AND Line_of_Bussiness__c ='+' \''+System.Label.PS_LeadOrgTypeHigherEducation+'\' AND '+searchField+' LIKE \''+'%' + String.escapeSingleQuotes(searchValue.trim())+ '%\''+' '+'AND Active_in_Self_Service__c = True Order by Name ASC limit 500';
        /*    query = 'SELECT '+sobjectFields+' FROM '+sobjectType+' WHERE Product_Group__c!= \'Global ELT\' AND Line_of_Bussiness__c != \'Clinical\' AND '+searchField+' LIKE \'' + String.escapeSingleQuotes(searchValue.trim())+ '%\''+' '+'AND Active_in_Self_Service__c = True Order by Name ASC limit 500';     Commented code as per CR-01782*/
        }
        else if(sobjectType == 'Account' && Country == 'United States' && Country != null)
        query = 'SELECT '+sobjectFields+' FROM '+sobjectType+' WHERE '+searchField+' LIKE \'' + String.escapeSingleQuotes(searchValue.trim())+ '%\''+' '+'AND Primary_Selling_Account_check__c = True '+' '+'AND RecordTypeid !=\''+AccountRecordTypeId+'\''+' '+'Order by Name ASC'+' '+'Limit 1000';    
        else if(sobjectType == 'Account' && Country != 'United States' && Country != null && Country != '')
        query = 'SELECT '+sobjectFields+' FROM '+sobjectType+' WHERE '+searchField+' LIKE \'' + String.escapeSingleQuotes(searchValue.trim())+ '%\''+' '+'AND RecordTypeid !=\''+AccountRecordTypeId+'\''+' '+'Order by Name ASC Limit 1000';
        sobjectList = Database.query(query);        
        return sobjectList;
    }
    
    @AuraEnabled
    public static sObject getObjInfo(String sObjectType, String sObjectFields,string recordId)
    {
        system.debug(recordId);
        String[] originalId = recordId.split('id_');
        system.debug(originalId);
        String query;        
        query = 'SELECT '+sobjectFields+' FROM '+sobjectType+' where id = \''+originalId[1]+'\'';
        system.debug(query);
        sObject sobjectInfo = Database.query(query);        
        return sobjectInfo;
    }
}