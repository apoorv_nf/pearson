/************************************************************************************************************
* Apex Interface Name : PS_EmailMessage_TriggerOperationsTest
* Test Description    : Test Class for  PS_EmailMessage_TriggerOperations
* Test Inputs         : Insert different email messages 
* Test Outputs        : To check the behaviour of Case Feed on different variants of email messages inserted for a case.        
* Version             : 1.1 
* Created Date        : 29/1/2016 2:44 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Sakshi Agarwal         29/1/2016             
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

@isTest(SeeAllData = False)
public class PS_EmailMessage_TriggerOperationsTest{
    static testMethod void emailMessageTest(){
     Bypass_Settings__c byp = new Bypass_Settings__C(SetupOwnerId=UserInfo.getUserId(),Disable_Triggers__c=true,Disable_Process_Builder__c = true);
     insert byp;
     System.runAs(new User(Id=UserInfo.getUserId())){
        Test.StartTest();
        // create account
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        insert lstAccount ;
         //create contact for account
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].MailingState='England';
        lstContact[0].Email = 'customersupport.reply@pearson.com';//System.Label.CustomerSupportEmail;
        lstContact[0].accountid=lstAccount[0].id;
        insert lstContact;
         //create case for contact
        List<case> cases = TestDataFactory.createCase(1,'School Assessment');
        set<id> casesId = new set<id>();
        set<String> toEmailAddress = new set<String>();
        Map<id,String> mapEmailToAddress = new Map<id,String>();
        Map<id,String> mapEmailCCAddress = new Map<id,String>();
        cases[0].AccountId=lstAccount[0].id;
        cases[0].ContactId=lstContact[0].id; 
        cases[0].Origin ='Email'; 
        cases[0].contact=lstContact[0];
        cases[0].PS_Product__c = 'N/A';  
        insert cases;
        casesId.add(cases[0].id);
         //create incoming message to customersupport.reply with ref
        List<EmailMessage> emailMessageList = new List<EmailMessage>();
        String userName = UserInfo.getUserName();
        User activeUser = [Select Email From User where Username = : userName limit 1];
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.incoming = true;
        emailMessage.subject = 'ref:00test';
        emailMessage.toAddress = cases[0].Contact.Email;
        emailMessage.BccAddress=   'customersupport.reply@pearson.com';
        emailMessage.CcAddress=  'customersupport.reply@pearson.com';
        emailMessage.FromAddress = 'customersupport.reply@pearson.com';
        emailmessage.parentId = cases[0].id;
        emailMessageList.add(emailMessage); 
        //create incoming message to customersupport.reply with wrong ref
        EmailMessage emailMessage1 = new EmailMessage();
        emailMessage1.incoming = true;
        emailMessage1.subject = 'ref00test';
        emailMessage1.toAddress = cases[0].Contact.Email;
        emailMessage1.BccAddress=  'customersupport.reply@pearson.com';
        emailMessage1.CcAddress=  'customersupport.reply@pearson.com';
        emailMessage1.FromAddress = 'customersupport.reply@pearson.com';
        emailMessage1.parentId = cases[0].id;   
        emailMessageList.add(emailMessage1); 
        //Added by Saritha to overcome Null pointer exception   
        General_One_CRM_Settings__c genOneCrRM = new General_One_CRM_Settings__c();
        genOneCrRM.name = 'ReopenDays';
        genOneCrRM.Category__c = 'ReopenDays';
        genOneCrRM.Description__c = 'ReopenDays';
        genOneCrRM.Value__c = '14';     
        insert genOneCrRM;
        mapEmailToAddress.put(cases[0].id,cases[0].Contact.Email);    
        mapEmailCCAddress.put(cases[0].id,cases[0].Contact.Email);     
         //create incoming message to customersupport.reply with wrong ref
        EmailMessage emailMessage3 = new EmailMessage();
        emailMessage3.incoming = true;
        emailMessage3.subject = 'ref00test';
        emailMessage3.toAddress = 'customersupport.reply@pearson.com';
        emailMessage3.BccAddress=  'customersupport.reply@pearson.com';
        emailMessage3.CcAddress=  'customersupport.reply@pearson.com';
        emailMessage3.FromAddress = 'customersupport.reply@pearson.com';
        emailMessage3.parentId = cases[0].id;   
        emailMessageList.add(emailMessage3);    
        
        mapEmailToAddress.put(cases[0].id,cases[0].Contact.Email);    
        mapEmailCCAddress.put(cases[0].id,cases[0].Contact.Email);    
        //create message outgoing to/fromcustomersupport.reply with NO ref
        EmailMessage emailMessage2 = new EmailMessage();       
        emailMessage2.subject = 'test';  //ref00
        //emailMessage2.incoming = true;
        emailMessage2.toAddress = 'customersupport.reply@pearson.com'; //cases[0].Contact.Email;
        emailMessage2.BccAddress=  'customersupport.reply@pearson.com';
        emailMessage2.CcAddress=  'customersupport.reply@pearson.com';
        emailMessage2.FromAddress = 'customersupport.reply@pearson.com';
        emailMessage2.parentId = cases[0].id;   
        emailMessageList.add(emailMessage2); 
         byp.Disable_Triggers__c=false;
         byp.Disable_Process_Builder__c = false;
         update byp;

        insert emailMessageList; // this should trigger
        toEmailAddress.add(emailMessageList[0].toAddress);
        toEmailAddress.add(emailMessageList[2].toAddress);
        PS_EmailMessage_TriggerOperations.mSendEmailWhenCaseIdNotPresent(emailMessageList);
        PS_EmailMessage_TriggerOperations.mValidationonEmailFromAddress(emailMessageList, casesId, toEmailAddress);
        PS_EmailMessage_TriggerOperations.mpopulateLatestToAddress(emailMessageList, casesId, mapEmailToAddress,mapEmailCCAddress);
       
        Test.StopTest();
        //System.assertEquals( emailMessage2.toAddress,cases[0].Contact.Email);
        }
        }
        
 }