public with sharing class AssetProductInUse{
          public String pdoInUse       {get;set;}
          public Asset newAsset {get;set;}
        
           //Contact Lookup variables
        Public String soslSearchText {get;set;}
        Public String courseId;
        Public String firstNameText {get;set;}
        Public String lastNameText {get;set;}
        Public String PSAText {get;set;}
        Public String acctNameText {get;set;}
        Public String primaryEmailText {get;set;}
        Public String phoneText {get;set;}
        Public List<Contact> contactsList {get;set;}
        Public Map<Id,ContactWrapper> conWrapperMap {get;set;}
        Public Map<Id,ContactWrapper> selConWrapperMap {get;set;}
        Public Map<Id,ContactWrapper> searchResultMap;
        Public Map<Id,ContactWrapper> filterResultMap;
        Public String currentContact {get;set;}
        Public set<Id> selContacts {get;set;}
        Public List<contact> selContactsList {get;set;}
        Public Boolean isContactsLookup {get;set;}
        Public Boolean isnewPIU {get;set;}
        Public Boolean isEmpty {get;set;}
        Public String tableTitle {get;set;}
        Public Boolean isApplyFilters {get;set;}
        Public Boolean isSelectAll {get;set;}
        public map<Id,ContactWrapper> mapOfTempWrap = new map<Id,ContactWrapper>();
        
         //start of changes by priya&Kote for CR-1472                
            ApexPages.StandardController myStdController;
            public string AssetId{get;set;}
           // public Asset assetList{get;set;}
          List<asset> assetList = new List<asset>();    
             public Asset AstList{get;set;}
        //end of changes by priya&Kote for CR-1472
        
           
    public AssetProductInUse(ApexPages.StandardController controller)
    {  
    
    //start of changes by priya&Kote for CR-1472   
    AssetId= Apexpages.currentPage().getParameters().get('id');    
    assetList = [select id,name,Status__c,Product_Author__c,InstallDate,Product2Id,Primary__c,Product_Edition__c,Description,Legacy_Modified_Date__c,Usage__c,Copyright_Year__c,ISBN_10__c,IsCompetitorProduct ,Third_Party_LMS__c, Product_ISBN__c,Course__c,contactid,Competitive_Status__c, AccountId,UsageEndDate  from Asset where id=:AssetId FOR UPDATE];          
   
        //for save and new
        myStdController = controller;
        AstList=new Asset();
        if(assetList.size() > 0) 
        {
            AstList= assetList[0];
        }
    //end of changes by priya&Kote for CR-1472
    
    
     Id usHERecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get(Label.US_HE_Record_Type).getRecordTypeId();
      System.debug('+++++++++++usHERecordTypeId:' +usHERecordTypeId );
        newAsset = new Asset(RecordTypeId = usHERecordTypeId, Status__c = 'Active');
        mapOfTempWrap = new map<Id,ContactWrapper>();
        tableTitle = 'Related Course Contacts';
        isContactsLookup = false;
        isnewPIU = true;
        isEmpty = false;
        isSelectAll = false;
        newAsset.Accountid = ApexPages.currentPage().getParameters().get('Account_lkid');
        newAsset.Course__c = ApexPages.currentPage().getParameters().get('CF00Nb0000009hWmz_lkid');
        newAsset.Name = '.';
        courseId = newAsset.Course__c;
        selContacts = new set<Id>();
        selContactsList = new List<contact>();
        selConWrapperMap = new Map<Id,ContactWrapper>(); 
        searchResultMap = new Map<Id,ContactWrapper>();        
        System.debug('courseId : '+courseId);
        ipageSize = 25;
        PagingNo = 1; 
        poffset = 0;                                          //initialising current page number
        if(TotalPagingNo == null || TotalPagingNo == 0)           //initialising totalpage number
        TotalPagingNo = 1;
        showPreviousPage = false;
        showNextPage = false;
        if(courseId != null)
        fetchCourseContacts();

    }
    
    public pageReference assetSave()
    {
        try{
            //insert newAsset;
            List<Asset> newAssetList = new List<Asset>();
            system.debug('selContacts : '+selContacts);
            if(!selContacts.isEmpty()){                          
            for(Id conId : selContacts){
               system.debug('ContactObj : '+conId);
               Asset tempAsset = new Asset();
               tempAsset.name = newAsset.name;
               tempAsset.Product2Id= newAsset.product2Id; 
               tempAsset.AccountId = newAsset.accountId;
               tempAsset.Status__c = newAsset.Status__c;
               tempAsset.Product_Author__c = newAsset.Product_Author__c;
               tempAsset.InstallDate = newAsset.InstallDate;
               tempAsset.Primary__c = newAsset.Primary__c;
               tempAsset.Product_Edition__c = newAsset.Product_Edition__c;
               tempAsset.Usage__c = newAsset.Usage__c;
               tempAsset.Copyright_Year__c = newAsset.Copyright_Year__c;
               tempAsset.Third_Party_LMS__c = newAsset.Third_Party_LMS__c;                                                    
               tempAsset.Product_ISBN__c = newAsset.Product_ISBN__c;
               tempAsset.Course__c = newAsset.Course__c;
               tempAsset.Competitive_Status__c = newAsset.Competitive_Status__c;
               tempAsset.IsCompetitorProduct = newAsset.IsCompetitorProduct;
               tempAsset.Description = newAsset.Description;
               tempAsset.Legacy_Modified_Date__c = newAsset.Legacy_Modified_Date__c;
               tempAsset.contactId = conId;
               system.debug('tempAsset: '+tempAsset);
               newAssetList.add(tempAsset);
            }
            system.debug('newAssetList : '+newAssetList);
            if(!newAssetList.isEmpty())
            Insert newAssetList;            
           } else
               Insert newAsset;
        return new pageReference('/'+courseId);
        }catch(Exception e){
         system.debug('Exception occured while inserting the PIU records : '+e.getMessage());
        }
        return null;
    }
    
    public void searchSOSl(){
        
        PagingNo = 1;
        poffset = 0;
        conWrapperMap = new Map<Id,ContactWrapper>();
        isApplyFilters = false;
        try{
        if(soslSearchText != null && soslSearchText != ''){
            soslSearchText = String.escapeSingleQuotes(soslSearchText);
            tableTitle = 'Search Results';
            String sosl = 'FIND '+'\''+soslSearchText+'*'+'\''+' IN ALL FIELDS RETURNING Contact(Id, Name, FirstName, LastName, primary_Selling_Account__c, Parent_Account__c, Account.Name, Email, Phone, MobilePhone ORDER BY Name asc)';
            System.debug('SOSl : '+sosl);
            List<List<SObject>> searchList = search.query(sosl);
            contactsList = ((List<Contact>)searchList[0]);           
            System.debug('contactsList : '+contactsList);
            if(!ContactsList.isEmpty()){
            for(Contact con : contactsList){
               conWrapperMap.put(con.Id,new ContactWrapper(con));
            }
            filterResultMap = new Map<Id,ContactWrapper>();
            filterResultMap = conWrapperMap;
            mapOfTempWrap   = conWrapperMap;   
            applyPagination();
           }
       
        } else{
            tableTitle = 'Related Course Contacts';
            //conWrapperMap = searchResultMap;        
            mapOfTempWrap   = searchResultMap;   
            applyPagination();
        } 
        clearAllFilterText();
        System.debug('conWrapperMap  : '+conWrapperMap );
        }catch(Exception e){
          System.debug('Exception occured while doing global search '+e.getMessage());
        }
    }
    
    public void fetchCourseContacts(){
       try{
       List<UniversityCourseContact__c> courseContactsList = new List<UniversityCourseContact__c>();
       Set<Id> conIds = new Set<Id>();
       contactsList = new List<Contact>();
       courseContactsList = [Select Id, Name, Contact__r.FirstName, Contact__r.LastName, Contact__r.primary_Selling_Account__c, UniversityCourse__c, Contact__c, Contact__r.name, Contact__r.Account.Name, Contact__r.Email, Contact__r.Phone, Contact__r.MobilePhone from UniversityCourseContact__c where UniversityCourse__c = : courseId];         
       if(!courseContactsList.isEmpty()){
        for(UniversityCourseContact__c courseCon : courseContactsList){
           conIds.add(courseCon.contact__c);
           }
       }
       if(!conIds.isEmpty()){           
           contactsList = [Select Id, Name, FirstName, LastName, primary_Selling_Account__c, Parent_Account__c, Account.Name, Email, Phone, MobilePhone from contact where Id in : conIds ORDER BY Name asc];
       }
       System.debug('courseId : '+courseId );
       System.debug('courseContactsList  : '+courseContactsList );       
       conWrapperMap = new Map<Id,ContactWrapper>();
       if(!ContactsList.isEmpty()){
           for(Contact con : contactsList){
               conWrapperMap.put(con.Id,new ContactWrapper(con));
           }
       }
       searchResultMap = conWrapperMap;
      
       mapOfTempWrap = conWrapperMap;
       applyPagination();
       }
       catch(Exception e){
       System.debug('Exception occured while fetching course contacts '+e.getMessage());
       }
    } 
    
         // Inner class
        public class ContactWrapper{
        public boolean isSelected {get; set;}
        public Contact contactObj{get; set;}
        
        public contactWrapper(Contact con){
            isSelected = false;
            contactObj = con;
        }
    }
    
    public void applyFilters(){
        System.debug('firstNameText : '+firstNameText);
        PagingNo = 1;
        poffset = 0;
        try{
        if(firstNameText == '' && lastNameText == '' && PSAText == '' && acctNameText == '' && primaryEmailText  == '' && phoneText == '')
        {
            if(soslSearchText==null || soslSearchText=='')
            {
                tableTitle = 'Related Course Contacts';
                System.debug('searchResultMap : '+searchResultMap);
                mapOfTempWrap = searchResultMap;
                applyPagination();
            }
            return; 
        }
        isApplyFilters = true;
        if(conWrapperMap!=null && !conWrapperMap.isEmpty()){
        Map<Id,ContactWrapper> tempMap = new Map<Id,ContactWrapper>();
        mapOfTempWrap = new map<Id,ContactWrapper>();
            for(Id conId: conWrapperMap.keySet()){
                Contact contactObj = conWrapperMap.get(conId).contactObj;
                if(firstNameText!=null && firstNameText!=''){
                if(contactObj.FirstName!= null && String.valueOf(contactObj.FirstName).indexOf(firstNameText)!=-1)
                tempMap.put(conId,conWrapperMap.get(conId));
                else
                continue;
                }                
                
                if(lastNameText!=null && lastNameText!=''){
                if(contactObj.LastName==null || (contactObj.LastName!=null && String.valueOf(contactObj.LastName).indexOf(lastNameText)==-1)){
                    if(tempMap.containsKey(conId)){
                    tempMap.remove(conId); 
                    continue;}                                      
                }else
                tempMap.put(conId,conWrapperMap.get(conId));
                }
                
                 if(primaryEmailText != null && primaryEmailText !=''){
                if(contactObj.Email==null || (contactObj.Email!=null && String.valueOf(contactObj.Email).indexOf(primaryEmailText)==-1)){
                if(tempMap.containsKey(conId)){
                    tempMap.remove(conId); 
                    continue;}                                      
                }else
                tempMap.put(conId,conWrapperMap.get(conId));                  
                } 
                
                if(phoneText != null && phoneText !=''){
                if(contactObj.Phone==null || (contactObj.Phone!=null && String.valueOf(contactObj.Phone).indexOf(phoneText)==-1)){
                if(tempMap.containsKey(conId)){
                    tempMap.remove(conId); 
                    continue;}                                      
                }else
                tempMap.put(conId,conWrapperMap.get(conId));                  
                }
                
                if(PSAText != null && PSAText!=''){
                if(contactObj.Primary_Selling_Account__c==null || (contactObj.Primary_Selling_Account__c!=null && String.valueOf(contactObj.Primary_Selling_Account__c).indexOf(PSAText)==-1)){
                if(tempMap.containsKey(conId)){
                    tempMap.remove(conId); 
                    continue;}                                      
                }                
                else
                tempMap.put(conId,conWrapperMap.get(conId));                
                }
                
                if(acctNameText != null && acctNameText !=''){
                if(contactObj.Account.Name==null || (contactObj.Account.Name!=null && String.valueOf(contactObj.Account.Name).indexOf(acctNameText)==-1)){
                if(tempMap.containsKey(conId)){
                    tempMap.remove(conId); 
                    continue;}                                      
                }else
                tempMap.put(conId,conWrapperMap.get(conId));                  
                }              
                             
            }
                System.debug('tempMap : '+tempMap.size());
                //conWrapperMap = new Map<Id,ContactWrapper>();
                mapOfTempWrap = tempMap;
                applyPagination();
            
            }
            }catch(Exception e){
             system.debug('Exception occured while applying filters : '+e.getMessage());
            }
       }
       
    Public void clearFilters(){
        PagingNo = 1;
        poffset = 0;
        clearAllFilterText();
        isApplyFilters = false;
        if(tableTitle == 'Related Course Contacts')
        mapOfTempWrap = searchResultMap;
        else
        mapOfTempWrap = filterResultMap;   
        applyPagination();

    
    }
    
    public integer conWrapperMapSize { get { return mapOfTempWrap.size( ); } }

    public void clearSearchResults(){
       /*conWrapperMap = new Map<Id,ContactWrapper>();
       conWrapperMap = searchResultMap;*/
       PagingNo = 1;
       poffset = 0;
       clearAllFilterText();
       mapOfTempWrap = searchResultMap;
       System.debug('searchResultMap : '+searchResultMap.size() );
       System.debug('mapOfTempWrap  : '+mapOfTempWrap.size() );
       applyPagination();
       tableTitle = 'Related Course Contacts';  
       isApplyFilters = false;
       isSelectAll = false;
    }
    
    public void clearAllFilterText(){
        firstNameText = '';
        lastNameText = '';
        PSAText = '';
        acctNameText = '';
        primaryEmailText = '';
        phoneText = '';
        isSelectAll = false;
    }
    
    public void backToNewPIU(){
        isContactsLookup = false;
        isnewPIU = true;   
    }
     
    public void nextPage(){
        isContactsLookup = true;
        isnewPIU = false;
       // return new Pagereference('/apex/CustomContactLookup?courseId='+newAsset.course__c);
    
    }
    
    public void attachSelectedContacts(){
        System.debug('currentContact : '+currentContact);
        try{
        if(currentContact !=null && currentContact != ''){
            if(selContacts.contains(currentContact)){
              selContacts.remove(currentContact);  
              selConWrapperMap.remove(currentContact);
              if(conWrapperMap.containsKey(currentContact)){
              conWrapperMap.get(currentContact).isSelected = false;
              isSelectAll = false;
              }
              else if(mapOfTempWrap.containsKey(currentContact)){
              mapOfTempWrap.get(currentContact).isSelected = false;
              //isSelectAll = false;
              }
            }else{
              selContacts.add(currentContact); 
              ContactWrapper wrapper = new ContactWrapper(conWrapperMap.get(currentContact).contactObj);
              wrapper.isSelected = true;
              selConWrapperMap.put(currentContact,wrapper);               
            }
        }
        if(selContacts.size()>0)
        isEmpty = true;
        else
        isEmpty = false;   
        }catch(Exception e){
         system.debug('Exception occured while attaching the selected contacts : '+e.getMessage());
        } 
    }
    
    public void removeSelectedContacts(){
        try{
        for(Id conId : selContacts){
           if(conWrapperMap.containsKey(conId))
           conWrapperMap.get(conId).isSelected = false;        
        }
        selContacts = new Set<Id>();
        selConWrapperMap = new Map<Id,ContactWrapper>();
        isEmpty = false;
        isSelectAll = false;
        }catch(Exception e){
         system.debug('Exception occured while removing the selected contacts : '+e.getMessage());
        }
        
    }
    
    public void selectAllContacts(){
        try{
        if(!conWrapperMap.isEmpty() && !isSelectAll){
            for(ContactWrapper wrapper : conWrapperMap.values()){
              wrapper.isSelected = false;
              Id conId = wrapper.contactObj.Id;
              if(selContacts.contains(conId)){
              selContacts.remove(conId);  
              selConWrapperMap.remove(conId);
              }             
            }
            System.debug('selContacts '+selContacts.size());
            if(selContacts.size()>0)
            isEmpty = true;
            else
            isEmpty = false; 
        }
        else if(!conWrapperMap.isEmpty() && isSelectAll){
            System.debug('selContacts else if'+selContacts.size());
            for(ContactWrapper wrapper : conWrapperMap.values()){
              wrapper.isSelected = true;
              Id conId = wrapper.contactObj.Id;
              if(!selContacts.contains(conId)){
              selContacts.add(conId);  
              selConWrapperMap.put(conId,wrapper);             
                }
            }
            System.debug('selContacts '+selContacts.size());
            if(selContacts.size()>0)
            isEmpty = true;
            else
            isEmpty = false; 

        }
        }catch(Exception e){
         system.debug('Exception occured while selecting all contacts : '+e.getMessage());
        }
    }
    
      
    public void applyPagination(){
        try{
        conWrapperMap = new map<Id,ContactWrapper>();
        list<ContactWrapper> lstOfCntWrap = mapOfTempWrap.Values(); 
        list<ContactWrapper> lstOfCntWrapTemp = new list<ContactWrapper>();
        if(lstOfCntWrap != null && !lstOfCntWrap.isEmpty()){
            decimal PageCount = math.ceil((decimal)lstOfCntWrap.size()/(decimal)ipageSize);    //holds number of pages for total records 
            TotalPagingNo = Integer.valueOf(PageCount);   
            integer iTotalRecords = lstOfCntWrap.size();
            if(iTotalRecords > 0 ){
                integer iFromRecord = integer.valueOf(ipageSize*(PagingNo-1));            //holds range from
                integer iToRecord = integer.valueof((ipageSize*PagingNo)-1);              //holds range to
                if(iToRecord >= iTotalRecords)
                {
                    iToRecord=iTotalRecords-1;
                }
                integer ij = 0;
                for(integer iIndex=iFromRecord;iIndex<=iToRecord;iIndex++)
                {
                    lstOfCntWrapTemp.add(lstOfCntWrap[iIndex]);
                    if(!lstOfCntWrap[iIndex].isSelected)
                    isSelectAll = false;                    
                    ij ++;                    
                }
                showPreviousPage=(PagingNo==1)?false:true; 
                showNextPage=(PagingNo==TotalPagingNo)?false:true;
            }
            for(ContactWrapper wrapObj : lstOfCntWrapTemp){
                conWrapperMap.put(wrapObj.contactObj.Id,wrapObj);   
            }
        }   
        }catch(Exception e){
              System.debug('Exception occured while applying pagination to the records : '+e.getMessage());
          }
    }
    
    /* Start : Pagination*/
    public Integer TotalPagingNo{get;set;}                   
    public Integer PagingNo{get;set;}                        
    public boolean showPreviousPage{get;set;}                
    public boolean showNextPage{get;set;}                    
    public Integer NumRecords {get;set;} 
    public integer ipageSize{get;set;}
    public integer ipageCount{get;set;}
    public Integer poffset{get;set;}
    
    public Pagereference FirstRecords(){
        PagingNo = 1;
        poffset = 0;
        applyPagination();
        return null;
    }
    
    public Pagereference PreviousPage(){
        PagingNo--;
        if(poffset > 0)
            poffset = poffset - ipageSize;
        applyPagination();
        return null;
    }
    public void NextPage2(){
        PagingNo ++;
        poffset = poffset + ipageSize;
       //isSelectAll = false;
        applyPagination();
    }
    
    public Pagereference LastRecords(){
        PagingNo = TotalPagingNo;
        poffset = (PagingNo*ipageSize) - ipageSize;
        applyPagination();
        return null;
    }
    
    public Pagereference pageCount(){   
        NumRecords = ipageCount;
        return null;
    }
    
    //start of changes by priya&Kote for CR-1472   
    public PageReference SaveNew() 
    {
      //  myStdController.save();    
      update AstList;
          
        PageReference pr = new PageReference('/apex/PIUsaveandnew');
        pr.setRedirect(true);
        return pr;
    }
    
    // For save button
     public PageReference Savefuntion() 
    {
      //  myStdController.save();    
      update AstList;
      
      system.debug('*****AstList' +AstList);
      system.debug('*****AssetId' +AssetId);
          
      return new pageReference('/'+AssetId);
      
    }
    
   //end of changes by priya&Kote for CR-1472
    
    /* Start : Pagination*/
}