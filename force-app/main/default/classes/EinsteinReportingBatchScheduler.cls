/**
 * Apex Class Name : EinsteinReportingBatchScheduler
 * Version         : 1.0
 * Created On      : 12-07-2018
 * Function        : A Scheduler class to EinsteinBotReportingBatch to get the Reporting Metrics for Einstein Bot
 *                  Scheduled to run ee
 * Test Class      : EinsteinReportingBatchSchedulerTest
 */

public with sharing class EinsteinReportingBatchScheduler implements Schedulable {
    Public void execute(SchedulableContext ctx) {
        EinsteinBotReportingBatch batchObj = new EinsteinBotReportingBatch('NAUS_HETS_Chatbot_ACR');
        Database.executeBatch(batchObj,200);
    }

}