/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_BatchMarketingBlackoutAccountUpdate 
   Description:     Batch class to perform a 'marketing blackout' of a hierachy of Acounts, it is chained to PS_BatchMarketingBlackoutAccConUpdate
                    which handles updating the related AccountContacts.
   					WARNING - This batch performs a SOQL in a while-loop. It works with a maximum hierachy depth of 995,
   					but re-architecturing the solution should be considered if the hierachy ever exceeds a hierachy of 500.
   					
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   23/10/2015       1.0           Tom Carman - Accenture                              Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */



global class PS_BatchMarketingBlackoutAccountUpdate implements Database.Batchable<sObject>, Database.Stateful {
     
    // Stateful vars which will persist throughout batch execution
    global Set<Id> masterAccountIdSet = new Set<Id>(); // Id of all accounts processed
    global Map<Id, Id> accountToUltimateParent = new Map<Id, Id>(); // Map of account to its highest parent which triggered this batch
    global Map<Id, Boolean> ultimateParentToFlag = new Map<Id, Boolean>(); // Map of account that triggerd this batch to its current blackout flag statu
    global Map<Id, Date> ultimateParentToDate = new Map<Id, Date>(); // Map of account that triggerd this batch to its current blackout until date

    global Set<Id> allAccountIds = new Set<Id>();
    global Id accountId;

    global PS_BatchMarketingBlackoutAccountUpdate() {
        // standard constructor
    }

    global PS_BatchMarketingBlackoutAccountUpdate(Id accountId) {
        // constructor for blacking out a single account
        this.accountId = accountId;
    }


    global Database.QueryLocator start(Database.BatchableContext BC) {

        if(accountId != null) {
            // Single account invocation - via button on UI
            return Database.getQueryLocator([SELECT Id, Blackout_Flag_Account__c, Blackout_Until_Date__c FROM Account WHERE Id = :accountId]);
        } else {
            // Bulk query invocation - via nightly batch (predominently used to un-blackout once Blackount_Until date has been reached)
            return Database.getQueryLocator([SELECT Id, Blackout_Flag_Account__c, Blackout_Until_Date__c FROM Account WHERE Blackout_Flag_To_Be_Processed__c = TRUE]);
        }
    }


    global void execute(Database.BatchableContext BC, List<Account> scope) {

        Set<Id> accountsToBeProcessed = new Set<Id>();

        List<AccountContact__c> accountContacts = new List<AccountContact__c>();
        Set<Id> allChildrenAccounts = new Set<Id>();

        for(Account account : scope) {
            accountsToBeProcessed.add(account.Id);
            ultimateParentToFlag.put(account.Id, account.Blackout_Flag_Account__c);
            ultimateParentToDate.put(account.Id, account.Blackout_Until_Date__c);
            accountToUltimateParent.put(account.Id, account.Id); // this was one of the accounts to be processed, so functionaly has no parents
        }

        allChildrenAccounts = getAllTheAccounts(accountsToBeProcessed); // Queries down the hierachy to retrieve all Accounts
        updateBlackoutFlagOnChildAccounts(allChildrenAccounts);

        allAccountIds.addAll(accountsToBeProcessed);
        allAccountIds.addAll(allChildrenAccounts);

        // Mark all accounts as processed by setting Blackout_Flag_To_Be_Processed__c to FALSE
        // THis needs to be done both on the original accounts AND all children accounts,
        // because the workflow rule will have set the children accounts 'To be processed' when the Blackout flag was 
        // updated earlier in this batch. This is also the justification for making these changes in two seperate
        // DML operations.
        markAllAccountsAsProcessed(allAccountIds); 


    }

    
    global void finish(Database.BatchableContext BC) {

    	Database.executeBatch(new PS_BatchMarketingBlackoutAccConUpdate(allAccountIds, accountToUltimateParent, ultimateParentToFlag));

    }
  


    private Set<Id> getAllTheAccounts(Set<Id> accountsToBeProcessedIds) {

        Set<Id> allChildrenAccountsToReturn = new Set<Id>(); // used to store the children accounts involved in this execution of the batch

        Set<Id> bottomAccountIds = new Set<Id>(); // used to store the lowest level accounts we retrieve, so they can be used as an entry point for the next iteration

        bottomAccountIds = getChildAccountIds(accountsToBeProcessedIds, allChildrenAccountsToReturn); // soql down 5 levels of the hierachy

        while (bottomAccountIds.size() > 0) { // if we didnt hit the bottom of the hierachy, keep on querying
            bottomAccountIds = getChildAccountIds(bottomAccountIds, allChildrenAccountsToReturn);
        }

        
        System.debug('TOTAL SOQL: ' + Limits.getQueries());
        System.debug('TOTAL ACCOUNTS: ' + masterAccountIdSet.size());

        return allChildrenAccountsToReturn;

    }


    private Set<Id> getChildAccountIds(Set<Id> parentIds, Set<Id> allChildrenAccountsToReturn) {

        Set<Id> bottomAccountIds = new Set<Id>();

        List<Account> accounts = [SELECT Name, Id, 
                                        ParentId,
                                        Parent.ParentId,
                                        Parent.Parent.ParentId,
                                        Parent.Parent.Parent.ParentId,
                                        Parent.Parent.Parent.Parent.ParentId,
                                        Parent.Parent.Parent.Parent.Parent.ParentId 
                                    FROM Account WHERE 
                                        Id IN :parentIds OR
                                        ParentId IN :parentIds OR 
                                        Parent.ParentId IN :parentIds OR 
                                        Parent.Parent.ParentId IN :parentIds OR 
                                        Parent.Parent.Parent.ParentId IN :parentIds OR 
                                        Parent.Parent.Parent.Parent.ParentId IN :parentIds
                                    ];


        for(Account acc : accounts) {

            if(!masterAccountIdSet.contains(acc.Id)) { // has not already been processed

                allChildrenAccountsToReturn.add(acc.Id);
                
                masterAccountIdSet.add(acc.Id); // set used to prevent infinite loop / re-processing same records

                if(accountToUltimateParent.containsKey(acc.Id)) {// this was an entry account
                    accountToUltimateParent.put(acc.Id, acc.Id);
                } else if (accountToUltimateParent.containsKey(acc.ParentId)) { // this is child of entry account
                    accountToUltimateParent.put(acc.Id, accountToUltimateParent.get(acc.ParentId));
                } else if (accountToUltimateParent.containsKey(acc.Parent.ParentId)) {// this is a grand child of entry account;
                    accountToUltimateParent.put(acc.Id, accountToUltimateParent.get(acc.Parent.ParentId));
                } else if(accountToUltimateParent.keyset().contains(acc.Parent.Parent.ParentId)) { //this is a great grand child of entry account;
                    accountToUltimateParent.put(acc.Id, accountToUltimateParent.get(acc.Parent.Parent.ParentId)); 
                } else if(accountToUltimateParent.keyset().contains(acc.Parent.Parent.Parent.ParentId)) { //this is a great great grand child of entry account;
                    accountToUltimateParent.put(acc.Id, accountToUltimateParent.get(acc.Parent.Parent.Parent.ParentId)); 
                } else if(accountToUltimateParent.keyset().contains(acc.Parent.Parent.Parent.Parent.ParentId)) { // this is a great great great grand child of entry account;
                    accountToUltimateParent.put(acc.Id, accountToUltimateParent.get(acc.Parent.Parent.Parent.Parent.ParentId));
                }

                // get bottom level accounts to reprocess
                if(acc.Parent.Parent.Parent.Parent.ParentId != null) {
                    bottomAccountIds.add(acc.Id);
                }
                
            }
        }
            
        return bottomAccountIds;

    }



    private void updateBlackoutFlagOnChildAccounts(Set<Id> allChildrenAccounts) {

    	List<Account> accountsToUpdate = new List<Account>();

    	for(Id accountId : allChildrenAccounts) {

    		Account account = new Account();
    		account.Id = accountId;

            // set flag
    		if(ultimateParentToFlag.containsKey(accountToUltimateParent.get(accountId))){
    			account.Blackout_Flag_Account__c = ultimateParentToFlag.get(accountToUltimateParent.get(accountId));

                // set date
                if(ultimateParentToDate.containsKey(accountToUltimateParent.get(accountId))) {

                    if(!ultimateParentToFlag.get(accountToUltimateParent.get(accountId))) {
                        // flag false, blank date
                        account.Blackout_Until_Date__c = null;
                    } else {
                        account.Blackout_Until_Date__c = ultimateParentToDate.get(accountToUltimateParent.get(accountId));
                    }
                }

    			accountsToUpdate.add(account);
    		}

    	}

    	update accountsToUpdate;

    }


    private void markAllAccountsAsProcessed(Set<Id> accountsToBeProcessed) {

        List<Account> accountsToUpdate = new List<Account>();

        // Update Account to set To Be Processed as false, so they dont get processed in future batches.
   
        for(Id accountId : accountsToBeProcessed) {

            Account account = new Account();
            account.Id = accountId;
            account.Blackout_Flag_To_Be_Processed__c = false;

            accountsToUpdate.add(account);
        }

        update accountsToUpdate;

    }



}