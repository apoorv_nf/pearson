/*
 * Author: Apoorv Saxena (Neuraflash)
 * Date: 25/07/2019
 * Description: Checks for a matching contact based on email, if found associates it to Messaging User
 *               else creates a new contact and then associates it to Messaging User - Developed for SMS Bot.
 */
public with sharing class EinsteinBotContactCheck {

    @InvocableMethod(label='Einstein SMS Bot - Check & Associate Contact to Messaging User')
    public static void checkContactExistence(List<ContactDetails> contactDetails){
        try{
            String contactEmail = contactDetails[0].email;
            String msgSessionId = contactDetails[0].routableId;
            
            Id contactId;

            if(!String.isBlank(contactEmail)){
                for(Contact con: [SELECT Id FROM Contact 
                                    WHERE Email =: contactEmail 
                                        ORDER BY LastModifiedDate DESC 
                                            LIMIT 1]){
                    contactId = con.Id;
                }
            }
            if(contactId != NULL && !String.isBlank(msgSessionId)){
                EinsteinBotEventHandler.updateMsgUser(contactId, msgSessionId);
            } else {

                List<Einstein_Bot_Event__e> createContactEvents = new List<Einstein_Bot_Event__e>();

                Einstein_Bot_Event__e createContactEvent = new Einstein_Bot_Event__e();
                createContactEvent.Type__c = EinsteinBotEventHandler.CREATE_SMS_CONTACT;
                //createContactEvent.Role__c = contactRequest.role;
                createContactEvent.Email__c = contactEmail;
                createContactEvent.First_Name__c = contactDetails[0].firstName;
                createContactEvent.Last_Name__c = contactDetails[0].lastName;
                createContactEvent.Live_Agent_Session_Id__c = msgSessionId;
                //createContactEvent.Institution__c = contactRequest.institutionName;
                 createContactEvents.add(createContactEvent);
                
                List<Database.SaveResult> saveResults = EventBus.publish(createContactEvents);
            } 
        } catch(Exception ex){
        }
    }

    public class ContactDetails {
        @InvocableVariable(required=false)
        public String firstName;
        @InvocableVariable(required=false)
        public String lastName;
        @InvocableVariable(required=false)
        public String email;
        @InvocableVariable(required=false)
        public String routableId;
    }
}