/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            DeleteRecordsBatch.cls 
   Description:     Batch class to delete records in multiple objects based on Preprocess updates.
   Test Class:      DeleteRecordsBatchTest
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   08/13/2019        0.1        Mani Kagithoju                Created
   10/18/2019        0.2        Mani Kagithoju                Modified   Changed the if condition that checks the list size.
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

global class DeleteRecordsBatch implements Database.Batchable<string>, Database.Stateful  {
global boolean bReRun = false; //will be used to determine if batch has to re-run in case there are more that 10K of records
global Iterable<string> start(Database.BatchableContext ctx) {
    return new list<String> {'HierarchyProduct__c','RelatedProduct__c ','Rep_Locator_Result__c'}; 
}
global void execute(Database.BatchableContext ctx, list<string> lstsObjectName) {
    list<sObject> lstDeleteRecords = new list<sObject>();
    for(string strObjectName : lstsObjectName) {
        for(sObject objsObject : database.query('Select Id from ' + strObjectName + ' where Delete__c = TRUE LIMIT 5000' )) {
            if(lstDeleteRecords.size() < 4998)
                lstDeleteRecords.add(objsObject);
            else {
                bReRun = true;
                break;
            }
        }
    }
    lstDeleteRecords.sort();
    delete lstDeleteRecords;
    
}
global void finish(Database.BatchableContext ctx) {
    if(bReRun) {
         Database.executebatch(new DeleteRecordsBatch());
    }
}
}