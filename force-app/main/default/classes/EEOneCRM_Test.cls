@isTest
    public class EEOneCRM_Test {
        static testmethod void test() {
        
        //Insert User
         Profile ProfileNameadmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        List<User> u1 = TestDataFactory.createUser(ProfileNameadmin.Id,1);
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu1'; 
            u1[0].Email ='Opptyproposaltest11@testorg.com'; 
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='Opptyproposaltest12'; 
            u1[0].LanguageLocaleKey ='en_US'; 
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = ProfileNameadmin.Id; 
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName ='standarduserQuoteCreation1@testorg.com';
            u1[0].Market__c ='UK';
            u1[0].Business_Unit__c = 'Schools';
            u1[0].line_of_Business__c = 'Schools';
            insert u1;
        }
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=u1[0].id,Disable_Triggers__c=true);
        insert byp;
        //inserting casefeed
        LIST<Case> caseList=new LIST<Case>();
        System.runAs(u1[0])
        {
            CS_Update_Case_Feed__c icsupdatecasefeed = new CS_Update_Case_Feed__c (Name = 'Last Extract');
            insert icsupdatecasefeed ;
            //Create Accounts
            LIST<Account> AcctList=new LIST<Account>();
            AcctList=TestDataUtility_Class.createAccounts(1);
            insert AcctList; 
            //Create Contacts
            LIST<Contact> contactList=new LIST<Contact>();
            contactList=TestDataUtility_Class.createContacts(1,AcctList[0]);
            insert contactList;
            //Create Cases
            //LIST<Case> caseList=new LIST<Case>();
            caseList=TestDataUtility_Class.createCases(1,AcctList[0],contactList[0]);
            insert caseList;
         }
         Bypass_Settings__c byp1=[Select SetupOwnerId,Disable_Triggers__c from Bypass_Settings__c where SetupOwnerId=:u1[0].id];
         byp1.SetupOwnerId=u1[0].id;
         byp1.Disable_Triggers__c = false;
         update byp1;
         
         System.runAs(u1[0])
        {
            Case csRec=[Select  Exec_Escalation_Date_Time__c from Case where id=:caseList[0].Id];
            csRec.Exec_Escalation_Date_Time__c=system.now();
            update csRec;
           
            
            list<CaseHistory> lstcaseHistory = [Select id,caseid from CaseHistory where caseid =:csRec.id];
            //list<feeditem> lstfeeditem = [select id, parentid from feeditem where parentid =:iUtility.listCase[0].id];
             Test.startTest();//Moved the test.startTest() from line 19 to line 27
             EE_OneCRM cb = new EE_OneCRM();
            //Database.QueryLocator ql = cb.start(null);
            cb.execute(null,lstcaseHistory);
            //cb.Finish(null);
            //id batchProcessID = database.executebatch(c,200);
            EEOneCRM_Scheduler isc = new EEOneCRM_Scheduler();
            string sch= '0 0 23 * * ?';    
            system.schedule('TestTask',sch,isc);
            Test.stopTest(); 
        }
        }
    }