/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_PopulateFieldsOnOrder.Cls 
   Description:     Handler class for populating fields on Order from Order trigger
   Test Class:      PS_PopulateFieldsOnOrderTest
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   08/01/2016         0.1        Accenture - Karan Khanna                Created
                                
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class PS_PopulateFieldsOnOrder 
{
    // Common Variables
    public List<Order> newOrders = new List<Order>();
    public Final String standardPricebook = 'standardpricebook';
    
    // Constructor
    public PS_PopulateFieldsOnOrder(List<Order> newOrders)
    {        
        this.newOrders = newOrders;        
    }
    
     /*---------------------------------------------------------------------------------------------------------------------------------
    Author:         Karan Khanna
    Company:        Accenture
    Description:    This method is used to populate Pricebook on Order
    Inputs:         None
    Returns:        None
    
    <Date>          <Authors Name>      <Brief Description of Change> 
    08/01/2016      Accenture_Karan     Initial version of the code
     
    -------------------------------------------------------------------------------------------------------------------------------*/
    public void PopulatePriceBook() 
    {
        try
        {
            Map<String,Id> pricebookNameIdMap = FetchActivePricebooks();
            System.Debug('#### pricebookNameIdMap ' + pricebookNameIdMap);
            System.Debug('#### before PopulatePriceBook newOrders ' + newOrders);
            for(Order ord : newOrders)
            {
                for(Proposal_Pricelist_Mapping__mdt configOPB : [SELECT Rep_Market__c, Rep_Line_of_Business__c, Rep_Business_Unit__c, Oppty_Currency__c, Order_Price_Book__c FROM Proposal_Pricelist_Mapping__mdt])
                {
                    System.Debug('#### configOPB ' + configOPB);
                    if(ord.Market__c == configOPB.Rep_Market__c && ord.Line_of_Business__c == configOPB.Rep_Line_of_Business__c && ord.Business_Unit__c == configOPB.Rep_Business_Unit__c && ord.CurrencyIsoCode == configOPB.Oppty_Currency__c)
                    {
                        String configPBName = configOPB.Order_Price_Book__c.toLowercase();
                        configPBName = configPBName.deleteWhitespace();                        
                        if(ord.pricebook2Id == null && pricebookNameIdMap.get(configPBName) != null)
                        {
                            ord.pricebook2Id = (Id)pricebookNameIdMap.get(configPBName);
                        }   
                    }
                }
                //* Davi Borges 3918 -- Allow the user to select the pricebook
                /*if(ord.pricebook2Id == null)
                {
                    if(pricebookNameIdMap.get(standardPricebook) != null)
                    {
                        ord.pricebook2Id = (Id)pricebookNameIdMap.get(standardPricebook);
                    }               
                }*/
               
            }
            System.Debug('#### after PopulatePriceBook newOrders ' + newOrders);
        }
        catch (exception e) {
            System.Debug('### Exception has occurred in PopulatePriceBook() ' + e);                                        
        }
    }
    
    public Map<String,Id> FetchActivePricebooks()
    {
        Map<String,Id> pricebookNameIdMap = new Map<String,Id>();
        for(Pricebook2 pb : [SELECT Id, Name, IsActive FROM Pricebook2 WHERE IsActive = true])
        {
            String pbName = pb.Name.toLowercase();
            pbName = pbName.deleteWhitespace();
            pricebookNameIdMap.put(pbName, pb.Id);
        }
        return pricebookNameIdMap;
    }
}