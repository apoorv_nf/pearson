/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 10/07/2018
 * Update: Added logic to handle requests coming in from both channels (Web & SMS).
 *		   Logic could be optimized further (Time constraints). Apoorv - 07/08/2019
 */
public without sharing class AgentWorkHandler {

	public void afterInsert(List<AgentWork> agentWorks) {
		Map<Id, Id> transcriptAgentMap = new Map<Id, Id>();
		Map<Id, Id> msgSessionAgentMap = new Map<Id, Id>();
		Map<Id,Id> caseOwnerIdMap = new Map<Id,Id>();

		for(AgentWork agentWork : agentWorks) {
			if(agentWork.workItemId.getSObjectType() == Schema.LiveChatTranscript.SObjectType){
				transcriptAgentMap.put(agentWork.WorkItemId, agentWork.UserId);
			} else if(agentWork.workItemId.getSObjectType() == Schema.MessagingSession.SObjectType){
				msgSessionAgentMap.put(agentWork.WorkItemId, agentWork.UserId);
			}

		}

		List<Case> cases = new List<Case>();

		for(LiveChatTranscript transcript : [
			SELECT Id, CaseId FROM LiveChatTranscript
			WHERE Id = :transcriptAgentMap.keySet()
			AND CaseId != null]) {
			caseOwnerIdMap.put(transcript.CaseId, transcriptAgentMap.get(transcript.Id));
		}

		if(!msgSessionAgentMap.keySet().isEmpty()){
			
			for(MessagingSession msgSession : [SELECT Id, CaseId 
													FROM MessagingSession
														WHERE Id = :msgSessionAgentMap.keySet()
															AND CaseId != NULL]) {
				caseOwnerIdMap.put(msgSession.CaseId, msgSessionAgentMap.get(msgSession.Id));
			}
		}
		if(!caseOwnerIdMap.keySet().isEmpty()){
			for(Id caseId: caseOwnerIdMap.keySet()){
				String market;
				Id userId = caseOwnerIdMap.get(caseId);
				try {
					market = [SELECT Id, Market__c FROM User WHERE Id = :userId].Market__c;
				} catch(Exception ex) {}

				if (String.isEmpty(market)) {
					market = 'US';
				}
				Case newCase = new Case(Id = caseId,
									OwnerId = userId,
									Status = 'In Progress',
									Market__c = market);
				cases.add(newCase);
			}
		}
		update cases;
		System.debug(cases);
	}
}