@isTest
private class TEST_ClsOpptyeqlntEdit  
{    
    //   public string pceId{get;set;}
    public boolean isError{get;set;}
    public boolean PearsonCourseCodeNameisNull{get;set;}
    public Opportunity_Pearson_Course_Code__c pceList{get;set;}          
    
    //For Course Object
    public Pearson_Course_Equivalent__c  CourseList{get;set;}
    //   public boolean PearsonCourseCodeisNull{get;set;}
    public string pceCourseId{get;set;} 

     @isTest               
         private static void TEST_ClsOpptyeqlntEdit()
        {         
            List<Account> createAccount1 = TestDataFactory.createAccount(3,'Learner');            
            insert createAccount1;
            
            List<Opportunity> oppList = TestDataFactory.createOpportunity(3,'B2B');                                
            insert oppList;
            
            List<ProductCategory__c> CategryList = TestDataFactory.createClassification();                                
            insert CategryList ;
            
            
            List<Hierarchy__c> catHiercryList = TestDataFactory.createClassificationHierarchy(); 
            catHiercryList[0].ProductCategory__c=CategryList [0].Id;
            insert catHiercryList ;
            
            List<Opportunity_Pearson_Course_Code__c> PCEListinser = TestDataFactory.createOpptyPearsonCourseCode();  
            PCEListinser[0].Pearson_Course_Code_Hierarchy__c=catHiercryList [0].Id;
             PCEListinser[0].Opportunity__c=oppList[0].Id;
            insert PCEListinser ;
            
            ApexPages.StandardController myStdController;    
            Opportunity_Pearson_Course_Code__c acc = new Opportunity_Pearson_Course_Code__c(Opportunity__c=oppList[0].Id,Pearson_Course_Code_Hierarchy__c=catHiercryList [0].Id);
            insert acc;  
            
            ApexPages.StandardController sc = new ApexPages.standardController(acc );
            ClsOpptyeqlntEdit  controller = new ClsOpptyeqlntEdit(sc); 
            
            myStdController = sc;
            Opportunity_Pearson_Course_Code__c  pceList = new Opportunity_Pearson_Course_Code__c();
            Boolean PearsonCourseCodeNameisNull = false;
            String pceId ;
            
            if (Apexpages.currentPage().getParameters().get('id') != null && Apexpages.currentPage().getParameters().get('id') != '') 
            {
                 pceId = Apexpages.currentPage().getParameters().get('id');
            } else 
            {
                 pceId = '';
            }
             system.debug('pceid'+pceId ); 
             List<Opportunity_Pearson_Course_Code__c> opcc = new List<Opportunity_Pearson_Course_Code__c>();    
             opcc = [select id,Pearson_Course_Code_Hierarchy__r.name from Opportunity_Pearson_Course_Code__c where id=:pceId ];  
             if (opcc.size() > 0) 
             {
                 pceList = opcc[0];
             } 
             
                //For course Object
      Pearson_Course_Equivalent__c CourseList = new Pearson_Course_Equivalent__c();
       Boolean PearsonCourseCodeisNull = false;
        String pceCourseId ;
        if (Apexpages.currentPage().getParameters().get('id') != null && Apexpages.currentPage().getParameters().get('id') != '') 
        {
             pceCourseId = Apexpages.currentPage().getParameters().get('id');
        } else 
        {
             pceCourseId = '';
        }
         system.debug('pceCourseId '+pceCourseId); 
         List<Pearson_Course_Equivalent__c> CourseCC = new List<Pearson_Course_Equivalent__c>();    
         CourseCC  = [select id,name,Pearson_Course_Code_Hierarchy__r.name from Pearson_Course_Equivalent__c where id=:pceCourseId ];  
         if (CourseCC.size() > 0) 
         {
             CourseList = CourseCC[0];
         }                             
          }
    }