/*
 * Author: Apoorv Saxena (Neuraflash)
 * Date: 27/07/2019
 * Description: Used for getting Contact Details - Developed for SMS Bot.
 */
public with sharing class EinsteinBotGetContactDetails {

    @InvocableMethod(label='Einstein SMS Bot - Get Contact Details')
    public static List<Contact> getContactDetails(List<String> sessionIds){
        
        List<Contact> conList = new List<Contact>();
		try {
            String contactId;
            for(MessagingSession msgSession : [SELECT MessagingEndUserId, MessagingEndUser.ContactId 
                                                FROM MessagingSession
                                                    WHERE Id IN :sessionIds]){
                if(msgSession.MessagingEndUserId != NULL && msgSession.MessagingEndUser.ContactId != NULL){
                    contactId = msgSession.MessagingEndUser.ContactId;
                    system.debug('ContactId: '+contactId);
                }
            }
            if(!String.isBlank(contactId)){
                conList = [SELECT Id, FirstName, LastName, Email
                            FROM Contact 
                                WHERE Id =: contactId];
            }
	    } catch(Exception ex) {}
        system.debug('conList: '+conList);
		return conList;
	}
}