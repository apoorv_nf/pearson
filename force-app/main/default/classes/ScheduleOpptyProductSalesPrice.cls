/* -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            ScheduleOpptyProductSalesPrice.cls 
Description:    This class is a scheduler for OpptyProductSalesPriceUpdate batch class.

Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ------------------------------------------------------------------------------------------------------------------------------------
May 13 2019       1.0                Priya              For CR-02909 : Opportunity Product Sales price update.                                                    
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
global class ScheduleOpptyProductSalesPrice implements Schedulable {
 global void execute(SchedulableContext sc) {
       List<Selling_Period_Parameter__mdt> sellingperiodmdt =[SELECT BatchJobSize__c FROM Selling_Period_Parameter__mdt where BatchJobSize__c !=null];
      OpptyProductSalesPriceUpdate obj = new OpptyProductSalesPriceUpdate();
       if(Test.isRunningTest())
        database.executebatch(obj, 10);
       else
      database.executebatch(obj, Integer.ValueOf(sellingperiodmdt[0].BatchJobSize__c));
   }
}