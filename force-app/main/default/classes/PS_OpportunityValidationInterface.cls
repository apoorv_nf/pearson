//KP: interface that holds method for opportunity closed validation
//KP:10/2015: Method signature update to handle insert validation
//Rajesh Paleti:10/2019:Take out Apttus related objects&Fields and replaced with standard objects&Fields 
//////////////
public interface PS_OpportunityValidationInterface {
    //to init opportunity update validation
     void opptyinitialize(List<opportunity> newoppylist,Map<Id,opportunity> inopportunity, Map<Id, opportunity> inOldopportunity);//,User userContext);
     Boolean opptyvalidateUpdate(Map<String,List<String>> exceptions);
     //void proposalopptyinitialize(Map<Id,Apttus_Proposal__Proposal__c> inproposal,Map<Id, Apttus_Proposal__Proposal__c> inOldproposal);
     // Commented for CR-2949 Apttus(Take out Apttus related objects&Fields and replaced with standard objects&Fields) by Rajesh Paleti
     void proposalopptyinitialize(Map<Id,Quote> inproposal,Map<Id, Quote> inOldproposal);
     void opptylineiteminitialize(Map<Id,OpportunityLineItem > inopptylineitem,Map<Id, OpportunityLineItem> inOldopptylineitem,String operation);
     void opptyPIUinitialize(Map<Id,Asset> inNewAsset,Map<Id, Asset> inOldAsset,String operation);
     void opptyeventinitialize(List<Event> evlist,Map<Id,Event > inNewEvent,Map<Id, Event> inOldEvent,String operation);
     void opptytaskinitialize(List<Task> newTask,Map<Id,Task> inNewTask,Map<Id, Task> inOldTask,String operation);

}