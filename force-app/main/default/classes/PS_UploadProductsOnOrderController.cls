/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_UploadProductsOnOrderController 
   
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  09/11/2015         1.0            Accenture IDC                       Initial Release 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  28/01/2016         1.1            Karan                               Set Unit price 0 if it is Sample Order 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */


public with sharing class PS_UploadProductsOnOrderController {

     
    Public ID orderObjId;
    public String documentName {get;set;}
    //public order order1;
    public Blob csvFileBody{get;set;}
    public string csvAsString{get;set;}
    public String[] csvFileLines{get;set;}
    public String[] orderitemDataLines {get;set;}
    public List < orderitem > orderitemList {get;set;}
    public boolean readSuccess {get;set;}
    public List<String> lstFieldNames{get;set;}
    orderitem oliObj;
    String orderitemDataAsString;
    String fieldValue;
    Integer fieldNumber;
    Public List<string> fieldList{get;set;}
    public Id sampleOrderRTID;
  
  public PS_UploadProductsOnOrderController () {
  
        orderObjId = ApexPages.currentPage().getParameters().get('id');  
    
      
        documentName = '';
        readSuccess = FALSE;
        orderitemDataLines = new String[] {};
        orderitemList = new List < orderitem > ();
        lstFieldNames = new List<String>();
        sampleOrderRTID = PS_Util.fetchRecordTypeByName(Order.SobjectType,PS_Constants.ORDER_SAMPLE_ORDER);
  }
  
  public void readFromFile(){
        try{
            //Gk
            if (!Test.isRunningTest())
     			   orderitemDataAsString = csvFileBody.toString();
            readCSVFile();
        }
        catch(exception e){
            readSuccess = FALSE;
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR,Label.PS_CSV_Error);
            ApexPages.addMessage(errorMessage);
        }
    }    
    
  
   
       public void readCSVFile(){
          try{
           
           //Gk
            if ( Test.isRunningTest() ) {
     			csvAsString = '9781292092485,1'+'\n'+'9781292092485,4';
			}
            else   
          	 csvAsString = csvFileBody.toString();           
           
           csvFileLines = csvAsString.split('\n'); 
            Set <String> isbns = new Set<String>();
            for(Integer i=1;i<csvFileLines.size();i++)
            {           
               string[] csvRecordData = csvFileLines[i].split(',');
               isbns.add(csvRecordData[0]);
            }
			//GK
            List<order>orderlist = [select id,Pricebook2Id,currencyIsocode, RecordTypeID from order where id =:orderObjId ];
            Order ord = null;
            if(orderlist != null && orderlist.size() > 0)
            {
              ord = orderlist[0];
            }
            //GK
            List <PricebookEntry> pbes;
            if ( !Test.isRunningTest() ) {
               pbes = [SELECT id FROM PricebookEntry where currencyIsocode =: ord.currencyIsocode AND 
                                           product2id in ( select id from product2 where ISBN__c IN :isbns)  AND  PriceBook2.isStandard=true];
            }else{
                
               pbes = [SELECT id FROM PricebookEntry where currencyIsocode =: ord.currencyIsocode AND 
                                           product2id in ( select id from product2 where ISBN__c IN :isbns)]; 
            }                             
            List <PriceBookEntry> pbeWithISBN = [SELECT Id, unitprice , Product2.ISBN__c, Product2Id From PricebookEntry where Id in :pbes];
            Map<String, Id> pricebookmap = new Map<String, Id>();
            Map<Id, PriceBookEntry> pricebookmapForPrice = new Map<Id, PriceBookEntry>();
            // for each pricebook entry create a map keyed on ISBN
            for(PriceBookEntry p : pbeWithISBN)
            {
              pricebookmap.put(p.Product2.ISBN__c, p.Id);
              pricebookmapForPrice.put(p.Id, p);
            }
            
           Boolean errorFound = false; 
           for(Integer i=1;i<csvFileLines.size();i++)
           {
           
               
               //List<Pricebook2> lstPriceBook = [Select id from PriceBook2 where isStandard=true limit 1]; //--> Price book to be used as reference durin order creation 
              
               Map<integer,PricebookEntry> Pricebookentrymap = new Map<integer,PricebookEntry>();             
                
               orderitem oliObj = new orderitem() ;
               string[] csvRecordData = csvFileLines[i].split(',');
           
               oliobj.OrderId = orderObjId;
               Order o = null;
               if(orderlist != null && orderlist.size() > 0)
               {
                 o = orderlist[0];
                 string s = o.currencyIsocode;
               }
               if(o != null)
               {      
                 if(!pricebookmap.containsKey(csvRecordData[0]))
                 {
                   ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'There is no pricebook entry that matches the currency of the order');           
                   ApexPages.addMessage(errorMessage); 
                   errorFound = true;                  
                 }
                 else
                 {
                   oliobj.PricebookEntryId = pricebookmap.get(csvRecordData[0]);
                   oliobj.Quantity=  integer.ValueOf(csvRecordData[1].trim());               
                   PriceBookEntry p = pricebookmapForPrice.get(oliobj.PricebookEntryId);                  
                   if(o.RecordtypeId == sampleOrderRTID)
                        oliobj.UnitPrice = 0;
                    else
                        oliobj.UnitPrice = p.unitprice;
                   orderitemList .add(oliObj);   
                 }
               }
             }
          
           
            if(!errorFound)
            {         
              insert orderitemList ;
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,label.PS_uploadOrderProdSuccess));
            }
           }
           
           catch (Exception e){
                        
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,Label.PS_uploadOrderProdError);           
            ApexPages.addMessage(errorMessage);
           }  
   }


}