@IsTest(SeeAllData=true)
private class PS_getLeadProductsTest {

    static testMethod void LeadProdtest(){    
    
    Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        lstWithTestUser[0].market__c='UK';
        insert lstWithTestUser;
        Bypass_Settings__c bys = [Select id,SetupOwnerId,Disable_Validation_Rules__c,Disable_Process_Builder__c  from Bypass_Settings__c limit 1];
        bys.SetupOwnerId = lstWithTestUser[0].id;
        bys.Disable_Validation_Rules__c = true;
        bys.Disable_Process_Builder__c = true;
        bys.Disable_Triggers__c = true;
        upsert bys;
        AccountTeamMember newAccountTeamMember = new AccountTeamMember();
        List<Lead> lstLeadRecords = new List<Lead>();
        Id accountId;  
        System.runAs(lstWithTestUser[0]) 
        { 
            RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Professional' Limit 1];
            Account acc = new Account (Name='test',geography__c='Growth',Line_of_Business__c='Schools',Organisation_Type__c='School');
            acc.ShippingStreet = 'TestStreet';
            acc.ShippingCity = 'Vns';
            acc.ShippingState = 'Delhi';
            acc.ShippingPostalCode = '234543';
            acc.ShippingCountry = 'India';
            acc.recordtypeId=OrgAcc.Id ;
            acc.Academic_Achievement__c='High';
            acc.Market2__c = 'UK';
            acc.Organisation_Type__c='School';
            acc.IsCreatedFromLead__c = true;
            acc.SelfServiceCreated__c=false;
            acc.Abbreviated_Name__c='TestName';            
            insert acc;
            
            
            lstLeadRecords = TestDataFactory.createLead(1,'Web Request');
            
            for(Lead lstLead : lstLeadRecords)
            { 
                lstLead.Channel__c ='Direct';
                lstLead.Organisation_Type1__c = 'Large Corporate';
                lstLead.Role__c = 'Employee';
                lstLead.Trust_Level__c = 'Low';
                lstLead.PostalCode = '345678';
                lstLead.State = 'Bihar';
                lstLead.Street = 'TestStreet';
                lstLead.Institution_Organisation__c =acc.Id;
                lstLead.Market__c = 'US';   
                lstLead.Enquiry_Type__c ='Digital Access';
            }
            
            
            test.startTest();
            Database.insert(lstLeadRecords);
            Map<Id,Lead> mapWithLead = new Map<Id,Lead>();
            for(Lead newLead : lstLeadRecords)
            {
                mapWithLead.put(newLead.Id,newLead);
            }
            for(Lead newLead : lstLeadRecords)
            {
                Database.LeadConvert convertLead = new database.LeadConvert();
                convertLead.setLeadId(newLead.ID);
                convertLead.setDoNotCreateOpportunity(false);
                convertLead.setConvertedStatus('Validated');
                Database.LeadConvertResult lcr = Database.convertLead(convertLead);
                System.assert(lcr.isSuccess());
                accountId = lcr.getAccountId();
            }
    
        Lead_Product__c leprod = new Lead_Product__c ();
        leprod.Lead__c = lstLeadRecords[0].id;
        leprod.Status__c='Requested';
        leprod.Requested_Product__c='sdfsf';
        leprod.Requested_ISBN_10__c='342';
        leprod.Requested_Authors__c='342';
        leprod.Requested_ISBN_13__c='342';
        leprod.Requested_Publisher__c='342';
        leprod.Requested_Title_Edition__c='test';
        leprod.Course__c='fdsf';
        leprod.Availability__c='fds';
        leprod.Book_Adopted__c=false;
        leprod.Product_In_Use__c='fsdf';
        leprod.Consider_Adopting__c=false;
        leprod.Publisher_in_Use__c='fsdf';
        leprod.Term__c='Spring';
        leprod.Enrollment__c=12;
        leprod.Adoption_Type__c='Committee';
        insert leprod;
    
PageReference tpageRef = Page.PS_LeadProductDetail;
        Test.setCurrentPage(tpageRef);          
        ApexPages.currentPage().getParameters().put('Id', leprod.Id);
        
    PS_getLeadProducts Leadprds = new PS_getLeadProducts();
    test.stopTest();
    }
    
  }  
}