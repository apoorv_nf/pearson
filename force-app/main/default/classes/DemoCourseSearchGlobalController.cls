/*
 *Author: Matt Hime (tquila)
 *Date: 21/8/2012
 *Description:  The controller for the DemoCourseSearchGlobal page which allows users to specify search parameters to trawl through 
 *              Hierarchy__c data.  Results are displayed in DemoCourseSearchGlobalSearchResult components.
 *
 *              Users can attach selected items to the University Course or Opportunity they navigated from
 */
 
//A class extends another class using the keyword extends. A class can only extend one other class, but it can implement more than one interface. 
//Use the with sharing keywords when declaring a class to enforce the sharing rules that apply to the current user
/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Date             Version         Author                             Summary of Changes 
----            ---------       -----------                     ------------------------------
17-Oct-2019         1.2        Anurutheran                  CR-02949-Apttus Decommission(Changes)
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public with sharing class DemoCourseSearchGlobalController extends PageControllerBase{
    private User loggedUser = new User();
    private List<Id> catList = new List<Id>();
    
    //I've used a map to hold the results because it's an easy way to store the data as it gets generated
    private map<string, list<Hierarchy__c>> ResultSets { get; set; }
    
    //If a new type comes along,  you'll need to add a new component to the form and add a new one of these variables below
    public list<Hierarchy__c> DisciplineSearchResults {get{return ResultSets.get('Discipline');} private set;}
    public list<Hierarchy__c> ClusterSearchResults {get{return ResultSets.get('Cluster');}  private set;}
    public list<Hierarchy__c> CourseSearchResults {get{return ResultSets.get('Course');}  private set;}

    public class searchResults {
        public String ResultType { get; set; }
        public list<Hierarchy__c> resultSet { get; set; }
        
        public searchResults(String type, list<Hierarchy__c> results)
        {
            this.ResultType = type;
            this.resultSet = results;
        }
    }

    public list<searchResults> allResults { get; set; }

    //The helper class for the search pages,  does things like build junction objects from data selected,  etc..
    private DemoCourseSearchHelper helper;

    //These variables are hidden fields on the form that are populated when the user double clicks the treeview 
    public string CourseCode {get; set;}
    public string CourseName {get; set;}
    public string CourseType {get; set;}
    
    //The Course Type dropdown on the page has it's values dynamically assigned to future-proof the code against new types
    public list<SelectOption> PearsonCourseStructureTypes {get; private set;}
    
    //If an error is thrown, ShowUserMessage is set to TRUE and the message is put into UserMessage and a friendly title in UserMessageTitle
    public boolean ShowUserMessage {get; private set;}
    public string UserMessageTitle {get; private set;}
    public string UserMessage {get; private set;}
    
    //If no data is returned from the search,  this flag hides the results panel
    public boolean ShowResults {get; private set;}
    //Added to display error message on click of attach button
    public boolean pearsonCourseCodeError{get;set;}
    
    public boolean notToDisplayOnSerach {get;set;}     
    public boolean isDisplaySearchPanel {get;set;}
    public boolean displayOnBrowse{get;set;}
    //Getter setter for taking current node parent
    public String currParent {get;set;}     
    //Getter setter for checking if Grand =Parent is changed
    public Boolean isGrandParentChanged {get;set;}       
    //Getter Setter for Repeat rendering
    public String grandparentid {get;set;}
    public String parentid {get;set;}
    public String parentid1 {get;set;}
    public String parentid2 {get;set;}
    public String parentid3 {get;set;}
    public String parentid4 {get;set;}
    public String parentid5{get;set;}
    public String parentid6 {get;set;}
    public ID courseId {get;set;}
    public boolean nullQtyError {get;set;}
    public Set<String> selCourses {get;set;}
    public list<Catalog_Offering__c> plcList { get; set; }
    public Map < Id, List < Hierarchy__c >> mapChild { get; set; }
     //Map to check if child is parent
    public Map < Id, Boolean > mapChildHasNext { get; set; }
    //Controller Variable for Getting Child
    public List < Hierarchy__c > clasLst { get; set; }
    public Map<Id,Hierarchy__c> clasMap {get;set;}
    //From the helper class,  display existing courses on screen
    public list<Hierarchy__c> ExistingCourses{
        get {
              return helper.ExistingCourses;
        }
        
        private set;
    }
       //Added by priya
       public string Mode{get;set;}
       public Id PCEId{get;set;}       
       public list<Opportunity_Pearson_Course_Code__c>PCEBWCList{get;set;}
      // public List<Id>SelectedList{get;set;}
       /*For course object*/
       
       public list<Pearson_Course_Equivalent__c>PCECourseBWCList{get;set;}
       
       
       //Ended by priya
          
          
    //Sets the title of the return link to that of Opportunity or University course depending on what object this search originated from
    public string ReturnLinkTitle{
        get{
            string returnValue = 'Return to ';
            
            if(helper.OwnerRecordType == 0){
                returnValue += 'Opportunity';
            }
            else if(helper.OwnerRecordType == 1){
                returnValue += 'University Course';
            }
            else{
                returnValue += 'UNKNOWN';
            }
            
            return returnValue;
        }
        private set;
    }
    
    
    /*
     *The constructor loads up the helper class, populates the Course Type drop down and initialises module level variables
     */
    public DemoCourseSearchGlobalController()
    {
       // Start of Addition By Priya
        Mode = ApexPages.currentPage().getParameters().get('mode');
        PCEId = ApexPages.currentPage().getParameters().get('PCEId');
       // End of Addition by Priya
         
        //Instantiate the helper - it requires the Id and type values from the URL
        courseId = (Id)ApexPages.currentPage().getParameters().get('Id');
        helper = new DemoCourseSearchHelper((Id)ApexPages.currentPage().getParameters().get('Id'), integer.valueOf(ApexPages.currentPage().getParameters().get('type')),ApexPages.currentPage().getParameters().get('mode'),(Id)ApexPages.currentPage().getParameters().get('PCEId'));                     
        isDisplaySearchPanel = false;
        displayOnBrowse=true;
        nullQtyError =false;
        clasLst = new List < Hierarchy__c > ();
        clasMap = new Map<Id,Hierarchy__c> ();
        mapChild = new Map < Id, List < Hierarchy__c >> ();
        mapChildHasNext = new Map < Id, Boolean > ();
        selCourses = new set<String>();
        //populate the Course Type drop down data
        loadPearsonCourseStructureTypes();
        
        //Initialise all the important variables
        resetResults();
        
        //Don't start of with null search terms
        CourseCode = '';
        CourseName = '';
        pearsonCourseCodeError=false;
        if(!PS_Util.isFetchedUserRecord){
            PS_Util.getUserDetail(Userinfo.getUserid());
            loggedUser = PS_Util.loggedInUser;
        }
        getUserDetails();      
    }
    
    
    
    //added by pooja 13 march 2015
    public Map<String, ComponentControllerBase> getSearchResultComponentControllerMap(){
        return getComponentControllerMap();
    }
    
    
    public PageReference SearchCourses(){
        //Initialise existing data
        resetResults();
                
        //Clean the search terms
        CourseCode = AutoCorrectSearchTerm(CourseCode);
        CourseName = AutoCorrectSearchTerm(CourseName);
        
        //Make sure that we have enough to make a search
        if(CourseCode.replace('*', '').length() < 2 && CourseName.replace('*', '').length() < 2){
            ShowUserMessage = true;
            UserMessageTitle = Label.short_search_term;
            UserMessage = Label.short_course_code;
        }
        else{
            //Search...
            if(isDisplaySearchPanel)
            getResults();
        }

        return null;
    }
        
    /*
     *Removes unwanted / reserved characters from the string passed to it and returns a cleansed version of it.
     */
    private string AutoCorrectSearchTerm(string searchTerm){
        
        //User can enter anything EXCEPT
        //Percentage sign - we're going to use that
        string revisedSearchTerm = searchTerm.replace('%', '');
        
        //Double quotes
        revisedSearchTerm = revisedSearchTerm.replace('"', '');
        
        //Single quote
        revisedSearchTerm = revisedSearchTerm.replace('\'', '');
        
        //Backslash (oblique?)
        revisedSearchTerm = revisedSearchTerm.replace('\\', '');
        
        //Asterisks are wild cards,  but,  they can only appear at the beginning and end of the search term
        if(revisedSearchTerm.contains('*')){
                
            //Replace * at beginning and end with something we know the string does not contain (a %-sign)
            if(revisedSearchTerm.startsWith('*')){
                revisedSearchTerm = '%' + revisedSearchTerm.substring(1);
            }
    
                //If the last character in the search term is a * replace it with a %
            if(revisedSearchTerm.endsWith('*')){
                revisedSearchTerm = revisedSearchTerm.substring(0, revisedSearchTerm.lastIndexOf('*')) + '%';
            }
            
            //Replace all asterisks
            revisedSearchTerm = revisedSearchTerm.replace('*', '');
            
            //Replace any temporary asterisk holders with asterisks
            revisedSearchTerm = revisedSearchTerm.replace('%', '*');
        }
        
        return revisedSearchTerm;
    }
    
    /*
     *Clear down all the old search data / error messages in preparation for the new search
     */
    private void resetResults(){
        ResultSets = new map<string, list<Hierarchy__c>>();
        
        ShowResults = false;
        
        ShowUserMessage = false;
        UserMessageTitle = '';
        UserMessage = '';
    }
    // Added by Pooja on 16 Mar'15 | Rd- 225 (IDC Task: 45)
    // To get logged in user's pricelist and from that to get associted Categary from PriceList Category object.
    // used to Query data from Category Hierarchy associated with Logged in user's Pricelist.
    //Category and Price list are associated via PriceList Category object. 
    private void getUserDetails(){
        //Id userid = UserInfo.getUserId();
        //User priceListName = [select Price_List__c from User where id =: userid];
        plcList = new List<Catalog_Offering__c>();
        plcList = [select ProductCategory__c,ProductCategory__r.Id, ProductCategory__r.Name from Catalog_Offering__c where ProductCategory__c != null AND Catalog__r.Name =: loggedUser.Price_List__c];
        Id catrec;
        if(plcList.size() == 0){
             displayOnBrowse=false;
             ShowUserMessage = true;
             UserMessageTitle = Label.no_result;
             UserMessage = 'No Pearson Course Structures were found for the Logged In User';
            }
        
        for(Catalog_Offering__c plc : plcList){
            catrec = plc.ProductCategory__c;
            catList.add(catrec);
        }
    }
    private void getResults(){
/*
        This SOQL is what we're trying to emulate here with the 3 lines in the midddle as optional:
                
        Select p.Type__c, p.Pearson_Course_Structure_Name__c, p.Pearson_Course_Structure_Code__c, p.Name, p.Discipline__c, p.Cluster__c 
        From Hierarchy__c p
        where Active_Indicator__c = true
        
        and Pearson_Course_Structure_Code__c like '%BM0%'
        and Type__c = 'Course'
        and Pearson_Course_Structure_Name__c like 'Corporate%'
        
        order by Type__c, Name
*/      
        String userMarket = loggedUser.Market__c;
        string SOQL = 'select Type__c, Label__c, PearsonCourseStructureCode__c, Name, Discipline__c, Cluster__c';
        SOQL += ' From Hierarchy__c';
       // SOQL += ' where Active_Indicator__c = true';
       // SOQL += ' and Region__c = \'US\'';
        SOQL += ' where Market__c =:userMarket';       
        SOQL += ' and ProductCategory__c in: catList';
        //If no search term has been specified for Course Code,  don't include it in the SOQL
        if(CourseCode.length() > 0){
            SOQL += ' and PearsonCoursestructureCode__c' + applyWildcards(CourseCode);
        }
        
        //If no search term has been specified for Course Name,  don't include it in the SOQL
        if(CourseName.length() > 0){
            SOQL += ' and (Label__c' + applyWildcards(CourseName) + 
                 ' or Name' + applyWildcards(CourseName) + ')';
        }

        PearsonCourseStructureSearchTypes__c pcstruc = PearsonCourseStructureSearchTypes__c.getInstance('Course');
        CourseType = pcstruc.Name;
        
        // If type is not specified, reference PearsonCourseStructureSearchTypes__c to ensure we are not showing types we should not be
        /*if(CourseType == 'All'){
            Set<String> displayCourseTypes = PearsonCourseStructureSearchTypes__c.getAll().keySet();
            SOQL += ' and Type__c in: displayCourseTypes';
        } else {*/
            SOQL += ' and Type__c = \'' + CourseType + '\'';
        //}

        //SOQL += ' order by Type__c, Name';  
        SOQL += ' order by Type__c, PearsonCourseStructureCode__c'; 
        
        try{
            /*
             *Run the search and then check the result.  Possible outcomes are
             * 1. No data returned - in which case,  flash up amessage to let the user know we got nothing
             * 2. Too much data returned - if it's more than 1000 rows,  this process will crash.  Get the user to refine their search
             * 3. Goldilocks! (the 3rd bowl of porridge was just right...)  We've got a result,  process it
             */
            Hierarchy__c[] results = database.Query(SOQL);
            
            if(results.size() == 0){
                ShowUserMessage = true;
                UserMessageTitle = Label.no_result;
                UserMessage = Label.no_pearson_course_found;
            }
            else if(results.size() >= 1000){
                ShowUserMessage = true;
                UserMessageTitle = Label.many_results;
                UserMessage = Label.large_no_of_results;
            }
            else{
                /*
                 *Loop through the results set and split the data by type.  Use the map pattern here as it makes it nice and easy to tell
                 *when we're transitioning between types
                 */
                for(Hierarchy__c pcs : results){
                    
                    list<Hierarchy__c> resultsByType = ResultSets.get(pcs.Type__c);
                    if(resultsByType == null){
                        resultsByType = new list<Hierarchy__c>(); 
                    }
                    
                    resultsByType.add(pcs);
                    ResultSets.put(pcs.Type__c, resultsByType);
                }
                
                // now we have grouped all results, push them into a list of lists, sorted by the type specified on the custom setting
                allResults = new list<searchResults>();
                for (PearsonCourseStructureSearchTypes__c resultType : [Select Name 
                                                                        from PearsonCourseStructureSearchTypes__c 
                                                                        order by Search_Sort_Order__c])
                {
                    // ensure we are not showing an empty list on the page
                    if (ResultSets.containsKey(resultType.Name))
                    {
                        allResults.add(new searchResults(resultType.Name, ResultSets.get(resultType.Name)));
                    }
                    
                }
                
                ShowResults = true;
            }
        }
        catch(Exception ex){
            ShowUserMessage = true;
            UserMessageTitle = 'Fatal Error';
            UserMessage = 'An unexpected error has occured,  please try again.';
        }  
    }
    
    /*
     *If the search term contains a wildcard (*),  doctor the SOQL to use like (rather than =)
     *and replace the * with the SOQL wildcard (%)
     */
    private string applyWildcards(string searchTerm){
        string wildcardOutput;
        
        if(searchTerm.contains('*')){
            wildcardOutput = ' like \'';
            
            if(searchTerm.startsWith('*')){
                wildcardOutput += '%';
            }
            
            wildcardOutput += searchTerm;
            
            
            if(searchTerm.endsWith('*')){
                wildcardOutput += '%';
            }
            
            wildcardOutput += '\'';
        }
        else{
            wildcardOutput = ' = \'' + searchTerm + '\'';
        }
        
        return wildcardOutput.replace('*', '');
    }
    
    /*
     *Select distinct Type__c values from Hierarchy__c and use them to create SelectOptions for the Course Type dropdown
     */
    private void loadPearsonCourseStructureTypes(){
        PearsonCourseStructureTypes = new list<SelectOption>();
        
        //Have All as the first option
        PearsonCourseStructureTypes.add(new SelectOption('All', 'All'));
        
        /*
         *There's no SELECT DISTINCT phrase available in SOQL,  so,  select Type__c from Hierarchy__c and group by it
         *This does not return Hierarchy__c objects,  but,  AggregateResult objects.
         
        for(AggregateResult course : [select Type__c From Hierarchy__c group by Type__c order by Type__c]){
            string courseType = string.valueOf(course.get('Type__c'));
            PearsonCourseStructureTypes.add(new SelectOption(courseType, courseType));
        }
        */
        
        List<SelectOption> otherTypes = new list<SelectOption>();
        // Rather than looking at all types that we have stored in the database, we should only supply values that we are exposing via the custom setting
        for (String pcsst : PearsonCourseStructureSearchTypes__c.getAll().keySet())
        {
            otherTypes.add(new SelectOption(pcsst,pcsst));
        }
        
        otherTypes.sort();
        PearsonCourseStructureTypes.addAll(otherTypes);
        
        //Set the selected value to all
        CourseType = 'All';
    }

    /*
     *Each custom search result component represents a type.  So,  loop through the types to get the type name - this is the key for 
     *the corresponding component - and use it to get the search results for that type.  Interrogate the results to see which have been selected
     *Store the selections and pass them to the Attach method on the helper class.
     */
    public PageReference Attach(){
        //hold the selected data
        list<Hierarchy__c> selectedPCS = new list<Hierarchy__c>();

        //loop through each type
        for(SelectOption so : PearsonCourseStructureTypes){
            //Ignore 'All' as it's not really a type
            if(so.getValue() != 'All'){
                
                
                //added by pooja 13 march 2015

                if(getComponentControllerMap().keyset().contains(so.getValue())){ 
                    DemoCourseSearchGlobalSearchResultCntrlr componentCtrlr = (DemoCourseSearchGlobalSearchResultCntrlr) getComponentControllerMap().get(so.getValue()); 
                    
                    //loop through the results 
                    for(DemoCourseSearchGlobalSearchResultCntrlr.PearsonCourseStructureSelection pcss : componentCtrlr.Results){
                    
                        
                        //If the item has been selected by the user, store it. 
                        if(pcss.isSelected){ 
                            selectedPCS.add(pcss.PearsonCourseStructure); 
                            } 
                        } 
                    } 
                    


                //added end here
                
                
            }
        }

        Pagereference pr;

        try{
            if(selectedPCS!=null && selectedPCS.size()>0){
            //build junction objects for everything selected
            pr = helper.Attach(selectedPCS);
            
                 }
                else{
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Select Atleast one Course Code.'));
                 pearsonCourseCodeError=true;
                 }
        }
        catch (Exception ex){
       //Fatal error!  DO SOMETHING!
       throw(ex);        }

        return pr;
    }
    
    /*
     *Return to the page from which the search process was called
     */
    public PageReference ReturnToCaller(){
        return helper.ReturnToCaller();
    }
    
     public void browseOnDefault() {
         displayOnBrowse=true;        
         isDisplaySearchPanel = false;   
         resetResults();
        if(plcList.size() == 0){
             displayOnBrowse=false;
             ShowUserMessage = true;
             UserMessageTitle = Label.no_result;
             UserMessage = 'No Pearson Course Structures were found for the Logged In User';
        }
          
     }
     public void searchOnDefault() {
        isDisplaySearchPanel = true;
        displayOnBrowse=false;
        resetResults();
        
     }
    
    //Set current parent when user clicks on taxonomy  
    public void setCurrParentInClass() {
    if(selCourses.contains(currParent)){
       selCourses.remove(currParent); 
        }else{
       selCourses.add(currParent);
       }
       nullQtyError = false; 
      }
    
     public void showChildren() {
        nullQtyError = false; 
        List < Hierarchy__c > hasChildLsit = new List < Hierarchy__c > ();
        Set < Id > childId = new Set < Id > ();
       if (isGrandParentChanged) {
            mapChild.clear();
            mapChildHasNext.clear();
            clasLst = [select ProductCategory__c, Id, PearsonCourseStructureCode__c, Type__c, Name, AncestorId__c from Hierarchy__c where ProductCategory__c = : currParent and(AncestorId__c = null or AncestorId__c = '') And (Type__c != null Or Type__c != '')
                       order by PearsonCourseStructureCode__c asc];
        } else {
            clasLst = [select ProductCategory__c, Id, Type__c, PearsonCourseStructureCode__c, Name, AncestorId__c from Hierarchy__c where AncestorId__c = : currParent And (Type__c != null Or Type__c != '')
                       order by PearsonCourseStructureCode__c asc];
        }
        if (!clasLst.isEmpty()) {
            hasChildLsit = [select ProductCategory__c, Id, Name, Type__c, PearsonCourseStructureCode__c, AncestorId__c from Hierarchy__c where AncestorId__c = : clasLst And (Type__c != null Or Type__c != '')
                            order by PearsonCourseStructureCode__c asc];
            for (Hierarchy__c hasChildObj: hasChildLsit) {
                childId.add(hasChildObj.AncestorId__c);
                
            }
        }
        if (!clasLst.isEmpty()) {
            for (Hierarchy__c clasObj: clasLst) {
                if (isGrandParentChanged) {
                    if (mapChild.containsKey(clasObj.ProductCategory__c)) {
                        List < Hierarchy__c > tempLst = new List < Hierarchy__c > ();
                        Set < Hierarchy__c > avoidDuplicate = new Set < Hierarchy__c > ();
                        avoidDuplicate.addall(mapChild.get(clasObj.ProductCategory__c));
                        avoidDuplicate.add(clasObj);
                        tempLst.addall(avoidDuplicate);
                       tempLst.sort();
                        mapChild.put(clasObj.ProductCategory__c, tempLst);
                    } else {
                        List < Hierarchy__c > tempLst = new List < Hierarchy__c > ();
                        tempLst.add(clasObj);
                        mapChild.put(clasObj.ProductCategory__c, tempLst);
                    }
                } else {
                    if (mapChild.containsKey(clasObj.AncestorId__c)) {
                        List < Hierarchy__c > tempLst = new List < Hierarchy__c > ();
                        Set < Hierarchy__c > avoidDuplicate = new Set < Hierarchy__c > ();
                        avoidDuplicate.addall(mapChild.get(clasObj.AncestorId__c));
                        avoidDuplicate.add(clasObj);
                        tempLst.addall(avoidDuplicate);
                       // tempLst.sort();
                        mapChild.put(clasObj.AncestorId__c, tempLst);
                       
                    } else {
                        List < Hierarchy__c > tempLst = new List < Hierarchy__c > ();
                        tempLst.add(clasObj);
                       mapChild.put(clasObj.AncestorId__c, tempLst);
                    }
                }
                if (childId.contains(clasObj.id)) {
                    mapChildHasNext.put(clasObj.id, true);
                } else {
                    mapChildHasNext.put(clasObj.id, false);
                }
            }
        } else {
            List < Hierarchy__c > tempLst = new List < Hierarchy__c > ();
            mapChild.put(currParent, tempLst);
        }
    }
    
public PageReference attachCourseCode()
{
         /* Start of Changes By Priya  - CR #1475 */
         List<Id> Selected =new List<Id>();
         Id SelectedCourseId;
         Boolean dupcheck = false;
         /* End of Changes By Priya - CR #1475 */
         integer count = 0;
         List<Sobject> ucpcsList = new List<Sobject>();

        //CP Add a Map to be able to get the apttus category id that is M-D in PearsonCourseEquivalent
        //Map<ID, Hierarchy__c> newOldMap = new Map<ID, Hierarchy__c>([SELECT Id, CategoryHierarchy_ExternalID__c FROM Hierarchy__c WHERE Id in :selCourses]);// Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran
        
        if((mapChildHasNext.containsKey(currParent) && mapChildHasNext.get(currParent)) || (currParent == null || currParent == '' || selCourses.isEmpty()))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select a Course'));
            nullQtyError = true; 
            return null;
        }
        else if(currParent != null  && mapChild.containsKey(currParent) && mapChild.get(currParent).size()>0 )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select a Course'));
            nullQtyError = true; 
            return null;
        }             
        //   List<Opportunity_Pearson_Course_Code__c> pccList = new List<Opportunity_Pearson_Course_Code__c>();  //now
             if(ApexPages.currentPage().getParameters().get('id').startswith('a0F'))
              {
          // CP 12/05/2017 modified to support non-apttus objects 
               //List<Pearson_Course_Equivalent__c> pceList = [select id, name, Pearson_Course_Code_Hierarchy__c, Pearson_Course_Code__c,Pearson_Course_Code__r.id from Pearson_Course_Equivalent__c where Course__c =: courseId];  // Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran
               List<Pearson_Course_Equivalent__c> pceList = [select id, name,Pearson_Course_Code_Hierarchy__c,Pearson_Course_Code_Hierarchy__r.id from Pearson_Course_Equivalent__c where Course__c =: courseId]; // Added for CR-02949 Apttus Decommission(Changes) by Anurutheran               
                for(String pceId : selCourses)
                {
                boolean duplicate = false;
                if(!pceList.isEmpty())
                {
                    for(Pearson_Course_Equivalent__c pce : pceList )
                    {
 // CP 12/05/2017 modified to support non-apttus objects                
                        //if(pce.Pearson_Course_Code_Hierarchy__c == pceId )
                        if(pce.Pearson_Course_Code_Hierarchy__c == pceId )
                        {
                            duplicate = true;
                            dupcheck = true;
                            break;
                        }
                   }
               }
               if(!duplicate)
                {
                
                Pearson_Course_Equivalent__c ucpcs = new Pearson_Course_Equivalent__c();
                if(pceList.isEmpty() && count==0)
                {
                    ucpcs.Primary__c = true;
                    count = 1;   
                }
                ucpcs.Course__c = courseId;              
                
                // CP 12/05/2017 modified to support non-apttus objects                
                //ucpcs.Pearson_Course_Code_Hierarchy__c = (ID)pceId;
                //ucpcs.Pearson_Course_Code__c = newOldMap.get(pceId).CategoryHierarchy_ExternalID__c;// Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran
                //ucpcs.Pearson_Course_Code_Hierarchy__c = newOldMap.get(pceId).CategoryHierarchy_ExternalID__c;// Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran
                ucpcs.Pearson_Course_Code_Hierarchy__c = (ID)pceId;
                
                // Start of Changes By Priya - CR #1475      
                SelectedCourseId= (ID)pceId; 
                // end of Changes By Priya - CR #1475      
                ucpcs.Active__c = true; 
                ucpcsList.add(ucpcs) ;
            
                }
                }
                }
            else if(ApexPages.currentPage().getParameters().get('id').startswith('006'))
            {      
                List<Opportunity_Pearson_Course_Code__c> pccList = new List<Opportunity_Pearson_Course_Code__c>();  
                // CP 12/05/2017 modified to support non-apttus objects 
                //pccList = [select id, name, Pearson_Course_Code_Hierarchy__c, Pearson_Course_Code_Name__c,Pearson_Course_Code_Name__r.id from Opportunity_Pearson_Course_Code__c where  Opportunity__c =: courseId];// Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran
                pccList = [select id, name, Pearson_Course_Code_Hierarchy__c,Pearson_Course_Code_Hierarchy__r.id from Opportunity_Pearson_Course_Code__c where  Opportunity__c =: courseId];  // Added for CR-02949 Apttus Decommission(Changes) by Anurutheran                 
                for(String pccId : selCourses)
                {
                boolean duplicate = false;
                if(!pccList.isEmpty())
                {
                    for(Opportunity_Pearson_Course_Code__c pcc : pccList )
                    {
// CP 12/05/2017 modified to support non-apttus objects             
                     
                       //if(pcc.Pearson_Course_Code_Name__c == pccId )
                       if(pcc.Pearson_Course_Code_Hierarchy__c == pccId )
                        {
                            duplicate = true;
                            break;
                        }
                    }
                }
                if(!duplicate)
                {
                    Opportunity_Pearson_Course_Code__c opcs = new Opportunity_Pearson_Course_Code__c(); 
                    if(pccList.isEmpty() && count==0)
                    {
                        opcs.Primary__c = true;
                        count = 1;   
                    }
                opcs.Opportunity__c = courseId;    
// CP 12/05/2017 modified to support non-apttus objects             
                //opcs.Pearson_Course_Code_Name__c = (ID)pccId; 
                //opcs.Pearson_Course_Code_Name__c = newOldMap.get(pccId).CategoryHierarchy_ExternalID__c;// Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran 
                //opcs.Pearson_Course_Code_Hierarchy__c = newOldMap.get(pccId).CategoryHierarchy_ExternalID__c;// Added for CR-02949 Apttus Decommission(Changes) by Anurutheran 
                opcs.Pearson_Course_Code_Hierarchy__c = (ID)pccId;
                
                //Start of Changes By Priya - CR #1475      
                Selected.add((ID)pccId);        
                //End of Changes By Priya  - CR #1475      
                
                opcs.Active__c = true; 
                ucpcsList.add(opcs);    
                }
                 
                } 
            }
         // Start of Changes By Priya & Kote - CR #1475                   
         /* if(!ucpcsList.isEmpty())
             {
             Insert ucpcsList;
             }
        */
        if(!ucpcsList.isEmpty() && Mode=='Add')
         {
            Opportunity_Pearson_Course_Code__c ucpcs2 = new Opportunity_Pearson_Course_Code__c();
             List<Opportunity_Pearson_Course_Code__c> pccList = new List<Opportunity_Pearson_Course_Code__c>();  
        // CP 12/05/2017 modified to support non-apttus objects
        //pccList = [select id, name, Pearson_Course_Code_Hierarchy__c, Pearson_Course_Code_Name__c,Pearson_Course_Code_Name__r.id from Opportunity_Pearson_Course_Code__c where  Opportunity__c =: courseId];// Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran
        pccList = [select id, name, Pearson_Course_Code_Hierarchy__c,Pearson_Course_Code_Hierarchy__r.id from Opportunity_Pearson_Course_Code__c where  Opportunity__c =: courseId];  // Added for CR-02949 Apttus Decommission(Changes) by Anurutheran                
            if(pccList.isEmpty() && count==0)
            {
                ucpcs2.Primary__c = true;
                count = 1;   
            }   
            Insert ucpcsList; 
         }       
         else if(Mode=='Edit' && ApexPages.currentPage().getParameters().get('id').startswith('006') && ucpcsList.size()==1 && !dupcheck && Selected.size()==1 )
         {       
                //PCEBWCList=[select id,name,Pearson_Course_Code_Name__c, Pearson_Course_Code_Hierarchy__c from Opportunity_Pearson_Course_Code__c where id=:PCEId];// Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran
                PCEBWCList=[select id,name,Pearson_Course_Code_Hierarchy__c from Opportunity_Pearson_Course_Code__c where id=:PCEId];// Added for CR-02949 Apttus Decommission(Changes) by Anurutheran
                // CP 12/05/2017 modified to support non-apttus objects
               //PCEBWCList[0].Pearson_Course_Code_Name__c = Selected.get(0);  
                //List< Hierarchy__c > hierList = [select id,name, CategoryHierarchy_ExternalID__c from Hierarchy__c where id = :SelCourses.get(0)];
                PCEBWCList[0].Pearson_Course_Code_Hierarchy__c = Selected.get(0);
                //PCEBWCList[0].Pearson_Course_Code_Name__c = newOldMap.get(Selected.get(0)).CategoryHierarchy_ExternalID__c;// Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran
                //PCEBWCList[0].Pearson_Course_Code_Hierarchy__c = newOldMap.get(Selected.get(0)).CategoryHierarchy_ExternalID__c;// Added for CR-02949 Apttus Decommission(Changes) by Anurutheran
                //PCEBWCList[0].Pearson_Course_Code_Name__c = newOldMap.get(Selected.get(0)).CategoryHierarchy_ExternalID__c;
                // CP 12/19/2017 - fixed INC3875733  
                PCEBWCList[0].Active__c = true;
                update PCEBWCList;
                      
         }
          else if(Mode=='Edit' && ApexPages.currentPage().getParameters().get('id').startswith('a0F') && ucpcsList.size()==1 && !dupcheck)
         {
                //PCECourseBWCList=[select id,name,Pearson_Course_Code__c, Pearson_Course_Code_Hierarchy__c from Pearson_Course_Equivalent__c  where id=:PCEId];// Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran
                PCECourseBWCList=[select id,name,Pearson_Course_Code_Hierarchy__c from Pearson_Course_Equivalent__c  where id=:PCEId];// Added for CR-02949 Apttus Decommission(Changes) by Anurutheran
                // CP 12/05/2017 modified to support non-apttus objects
                /*String hierIDSelected = SelCourses.get(0);
                List< Hierarchy__c > hierList = [select id,name, CategoryHierarchy_ExternalID__c from Hierarchy__c where id = :hierIDSelected];*/
                //PCECourseBWCList[0].Pearson_Course_Code_Hierarchy__c = SelectedCourseId;
                PCECourseBWCList[0].Pearson_Course_Code_Hierarchy__c = SelectedCourseId;
               //PCECourseBWCList[0].Pearson_Course_Code__c = newOldMap.get(SelectedCourseId).CategoryHierarchy_ExternalID__c;// Commented for CR-02949 Apttus Decommission(Changes) by Anurutheran
             // CP 12/19/2017 - fixed INC3875733    
             PCECourseBWCList[0].Active__c = true;
                update PCECourseBWCList;
         }         
         // End of Changes By Priya & Kote - CR #   1475
           return helper.ReturnToCaller();        
        }     
}