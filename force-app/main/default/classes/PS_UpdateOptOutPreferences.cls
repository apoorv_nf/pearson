/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_UpdateOptOutPreferences.cls 
   Description:     Class to update Marketing preferences for the Contact. This class is called when 'Opt-out' or 'Re-opt' 
   					Case is updated to 'Approved' status from ESB.
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  07/01/2015         1.0            Rahul Boinepally                       Initial Release 
  ---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public with sharing class PS_UpdateOptOutPreferences {

	public static string optOut = 'Opt Out';
	public static string reOpt = 'Re-Opt In';


	public static void updateMarketingPref(List<Case> caseUpdates) 
	{

		PS_ExceptionLogger Logger = new PS_ExceptionLogger();
		Set<Id> contactSet = new Set<Id>();
		List<Contact> contactList = new List<Contact>();		
		Map<Id,Boolean> optedOutMap = new Map<Id,Boolean>();
		Map<Id,Boolean> globalUnsubMap = new Map<Id,Boolean>();				

		List<Case> filteredCases = filterValidCases(caseUpdates);

		for(Case cFor: filteredCases)
		{
			optedOutMap.put(cFor.ContactId, cFor.HasOptedOutOfEmail__c );
			globalUnsubMap.put(cFor.ContactId, cFor.Global_Marketing_Unsubscribe__c);
			contactSet.add(cFor.ContactId);
		}

				
		List<Contact> conList = [Select HasOptedOutOfEmail, Global_Marketing_Unsubscribe__c from Contact where ID IN : contactSet];		
		for(Contact contactUpdates : conList)
		{			
			contactUpdates.HasOptedOutOfEmail = optedOutMap.get(contactUpdates.Id);
			contactUpdates.Global_Marketing_Unsubscribe__c = globalUnsubMap.get(contactUpdates.Id);
			contactList.add(contactUpdates);
		}
		try
		{
			Update contactList;		
			
		}
		catch(exception e)
		{
			logger.addLog(e);
			logger.writeLogs(); // TC 28/01/2016
		}
		
	}

	private static List<Case> filterValidCases(List<Case> cases)
	{

		List<Case> caseList = new List<Case>();
		for(Case cc: cases)
		{

			
			if((cc.Type == optOut || cc.Type == reOpt) && (cc.Status == 'Closed') && ( ((case)(trigger.oldmap.get(cc.id))).status) == 'Submitted For Approval')

			{

				caseList.add(cc);

			}

		}
		return caseList;
	}

	public static void updateCaseStatus(List<Case> cases)
	{

		for(Case c : cases)
        {            
            if((c.Status == 'Approved')  && (c.Type == optOut || c.Type == reOpt) && (userInfo.getLastName() == 'ESB'))
            {
                c.Status = 'Closed';         
            }
        }                

	}
}