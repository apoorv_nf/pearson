/*******************************************************************************************************************
* Apex Class Name   :TestAccountContact_Primary_Financial
* Created Date      : 8 MAY 2014
* Description       : Test class for AccountContact_Primary_Financial class
*******************************************************************************************************************/
@isTest
private class TestAccountContact_Primary_Financial
{
    /* static testMethod void myUnitTest2()
    {
    TestClassAutomation.FillAllFields = true;
        
        List<Account> accountList=TestDataFactory.createAccount(2,'Learner');
            insert accountList;
    
        List<Contact> contactList=TestDataFactory.createContact(2);
            contactList[0].accountid=accountList[0].id;
            contactList[1].accountid = accountList[1].id;
            
        Test.startTest();
            
            insert contactList;
            
        checkRecurssion.run = true;  
        List<AccountContact__c> accountCntList=TestDataFactory.createAccountContact(1,accountList[0].id , contactList[0].id);
        insert accountCntList;
        
         AccountContact__c sAccountContact = [Select Id FROM AccountContact__c WHERE Contact__c in:contactList limit 1 ];
         //AccountContactRole sAccountConRole = [Select Id FROM AccountContactRole WHERE Contactid in:contactList limit 1];

            checkRecurssion.run = true;       
            update sAccountContact;
            checkRecurssion.run = true;
            
            // Modification for checkPrimaryAccountContact() -- Start --
            
            // Delete for function checkPrimaryAccountContact()
           // Database.DeleteResult DR_Dels = Database.delete(sAccountContact,false);
            
            // Setting Back the Account to the Backup for the normal function to be performed.
            contactList[0].AccountId = accountList[1].Id;
            update contactList;
            
            // Modification for checkPrimaryAccountContact() -- End --
            delete sAccountContact;
            //delete sAccountConRole;
           checkRecurssion.run = true;    

          undelete sAccountContact;
        
        Test.stopTest();
    }
     Static testmethod void myUnitTest_1(){
         TestClassAutomation.FillAllFields = true;
        
        Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
        sAccount.BillingCountry         = 'Australia';
        sAccount.BillingState           = 'Victoria';
        sAccount.BillingCountryCode     = 'AU';
        sAccount.BillingStateCode       = 'VIC';
        sAccount.ShippingCountry        = 'Australia';
        sAccount.ShippingState          = 'Victoria';
        sAccount.ShippingCountryCode    = 'AU';
        sAccount.ShippingStateCode      = 'VIC';
        
        insert sAccount;
        
        Contact sContact                = (Contact)TestClassAutomation.createSObject('Contact');
        sContact.AccountId              = sAccount.Id;
        sContact.OtherCountry           = 'Australia';
        sContact.OtherState             = 'Victoria';
        sContact.OtherCountryCode       = 'AU';
        sContact.OtherStateCode         = 'VIC';
        sContact.MailingCountry         = 'Australia';
        sContact.MailingState           = 'Victoria';
        sContact.MailingCountryCode     = 'AU';
        sContact.MailingStateCode       = 'VIC';
            
        Test.startTest();
            
           insert sContact;
         AccountContact__c sAccountContact  = new AccountContact__c(account__c=sAccount.Id,contact__c=sContact.Id,Primary__c=true,AccountRole__c='Learner');        
          insert sAccountContact; 
        // AccountContact__c sAccountContact = [Select Id FROM AccountContact__c WHERE Contact__c = : sContact.Id limit 1];
                    
         Test.stopTest();
            
     }  */
    static testMethod void myUnitTest3()
    {
    TestClassAutomation.FillAllFields = true;
        
        List<Account> accountList=TestDataFactory.createAccount(2,'Learner');
            insert accountList;
    
        List<Contact> contactList=TestDataFactory.createContact(1);
            contactList[0].accountid=accountList[0].id;
            
        Test.startTest();
            
            insert contactList;
            
        checkRecurssion.run = true;  
        List<AccountContact__c> accountCntList=TestDataFactory.createAccountContact(1,accountList[0].id , contactList[0].id);
            insert accountCntList;
        
         AccountContact__c sAccountContact = [Select Id FROM AccountContact__c WHERE Contact__c in:contactList limit 1];
         //AccountContactRole sAccountConRole = [Select Id FROM AccountContactRole WHERE Contactid in:contactList limit 1];

            checkRecurssion.run = true; 
            sAccountContact.Sync_In_Progress__c = false;
            sAccountContact.Primary__c = false;
            sAccountContact.AccountRole__c='Learner';
            update sAccountContact;
            checkRecurssion.run = true;
            
          Test.stopTest();
    }
}