/*******************************************************************************************************************
* Apex Class Name  : PS_Pearson_SupportFormController.cls
* Version          : 1.0
* Created Date     : 6 April 2016
* Function         : RD-1745 - Support Signpost Web Form 
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
* Payal Popat               6/04/2016              Initial documentation of the class.  
* 
*******************************************************************************************************************/

public class PS_Pearson_SupportFormController {    
    public string SelectProdRoute {get; set;}
    /**
    * Description : Method to get Country values from Custom Setting 
    * @param NA
    * @return List<SelectOption>
    * @throws NA
    **/
    //Get list of country from custom setting
    public List<SelectOption> getCountries()
    {
      List<SelectOption> CountryList= new List<SelectOption>();
      // Find all the countries in the custom setting
      Map<String, PS_SupportSignPost_Country__c > countries = PS_SupportSignPost_Country__c.getAll();
      // Sort them by name - this only works if the name field actually has the country name in there. Right now its sorting off the load order
      List<String> countryNames = new List<String>();
      countryNames.addAll(countries.keySet());
      countryNames.sort();
      // Create the Select Options.
      for (String countryName : countryNames) {
           PS_SupportSignPost_Country__c country = countries.get(countryName);
           CountryList.add(new SelectOption(country.Name, country.Name));
       }
      return CountryList;
    }
    /**
    * Description : Method to get weblinks from Custom Setting based on country and product selected 
    * @param NA
    * @return string
    * @throws NA
    **/
    //Get list of country from custom setting
    public PageReference getWebLinks()
    {
         PageReference ref = new PageReference(PS_SupportSignPost_Weblinks__c.getInstance(SelectProdRoute).Weblinks__c); 
         ref.SetRedirect(true);
         return ref;
    }
}