/************************************************************************************************************
* Apex Class Name   : AccountContactTriggerHandler.cls
* Version           : 1.0 
* Created Date      : 12 MARCH 2014
* Function          : Handler class for AccountContact__c Object Trigger
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Mitch Hunt                  12/03/2014              Created Default Handler Class Template
*                             19/03/2015              Updated to include references to the AccountContactSync apex class
*                             23/03/2015              Updated to include references to the B2CAccountSync apex class
* Leonard Victor              20/08/2015              Updated code as part of R3 code stream lining
* Cristina Perez			  05/31/2018			  Updated code to write to AccountContactRelation instead of AccountContactRole for LexProject
************************************************************************************************************/

public without sharing class AccountContactTriggerHandler
{
    private boolean m_bIsExecuting = false;
    private integer iBatchSize = 0;
    
    public AccountContactTriggerHandler(boolean bIsExecuting, integer iSize)
    {
        m_bIsExecuting = bIsExecuting;
        iBatchSize = iSize;
    }
    
    // Init Contact Utils
    AccountContactUtils utils = new AccountContactUtils();
    
    //Sync Account Contact Class
    AccountContactSync sync = new AccountContactSync();
    
    //Sync B2C Accounts Class
    B2CAccountSync B2CSync = new B2CAccountSync();
    
    // EXECUTE BEFORE INSERT LOGIC
    public void OnBeforeInsert(AccountContact__c[] lstNewAccountContacts)
    {
        //Moved this line from Trigger as part of R3 code stream line
        AccountContact_Primary_Financial.checkPrimaryAndFinancialFlags(lstNewAccountContacts);
        AccountContact_Primary_Financial.updateLearnerContact(lstNewAccountContacts);
        //Commenting this line since this method was not invoked from trigger , changes done as part of R3 code stream line
        //utils.preventDuplicates(lstNewAccountContacts);
    }
    
    public void onBeforeDelete( AccountContact__c[] lstNewAccountContacts )
    {
        AccountContact_Primary_Financial.checkPrimaryAccountContact(lstNewAccountContacts);
    }
    
    // EXECUTE AFTER INSERT LOGIC
    public void OnAfterInsert(AccountContact__c[] lstNewAccountContacts)
    {
        //System.debug('On trigger handler afterinsert - before sync');
         sync.InsertAccountContactRelationRecords(lstNewAccountContacts);
	    B2CSync.B2CAccountSyncWhenAccountContactRelationshipIsSet(lstNewAccountContacts);
        //Added As part of Code Streamline in R3
        //Commented for CR-02233
        //primarycheckonAccountContact.togglePrimaryAndFinancialFlags(lstNewAccountContacts, NULL);
    }
    
    // BEFORE UPDATE LOGIC
    //Modified the method signature as part of R3 code streamline
    public void OnBeforeUpdate(AccountContact__c[] lstUpdatedAccountContacts)
    {
          //Moved this line from Trigger as part of R3 code stream line
          AccountContact_Primary_Financial.checkPrimaryAndFinancialFlags(lstUpdatedAccountContacts);
          AccountContact_Primary_Financial.updateLearnerContact(lstUpdatedAccountContacts);
         //Commenting this line since this method was not invoked from trigger , changes done as part of R3 code stream line
         //sync.CheckMultipleAccountContactPrimaries(lstUpdatedAccountContacts);
         
    }
    
    // AFTER UPDATE LOGIC
    public void OnAfterUpdate(AccountContact__c[] lstOldAccountContacts, AccountContact__c[] lstUpdatedAccountContacts, map<ID, AccountContact__c> mapNewIDAccountContact, map<ID, AccountContact__c> mapOldIDAccountContact)
    {
        System.debug('On trigger handler afterupdate');
        //Moved togglePrimaryAndFinancialFlags invocation from trigger to handler
        //Commented for CR-02233
        //PrimarycheckonAccountContact.togglePrimaryAndFinancialFlags(lstUpdatedAccountContacts, mapOldIDAccountContact);        
        sync.UpdateAccountContactRelationRecords(lstUpdatedAccountContacts); 
        
        B2CSync.B2CAccountSyncWhenAccountContactRelationshipChange(lstUpdatedAccountContacts, mapNewIDAccountContact, mapOldIDAccountContact);
        PrimaryAccountUtilities.updateRoleInformationOnContact(lstOldAccountContacts, lstUpdatedAccountContacts, mapNewIDAccountContact,mapOldIDAccountContact);
    }
    
   
    // AFTER DELETE LOGIC
    public void OnAfterDelete(AccountContact__c[] lstDeletedAccountContacts, map<ID, AccountContact__c> mapIDAccountContact)
    {
        sync.DeleteAccountContactRelationRecords(lstDeletedAccountContacts);
        sync.DeleteCourseContactRecords(lstDeletedAccountContacts);
        
    }
    
    // AFTER UNDELETE LOGIC
    public void OnUndelete(AccountContact__c[] lstRestoredAccountContacts)
    {
        Sync.UndeleteAccountContactRelationRecords(lstRestoredAccountContacts);
    }
    
    public boolean bIsTriggerContext
    {
        get{ return m_bIsExecuting; }
    }
    
    public boolean bIsVisualforcePageContext
    {
        get{ return !bIsTriggerContext; }
    }
    
    public boolean bIsWebServiceContext
    {
        get{ return !bIsTriggerContext; }
    }
    
    public boolean bIsExecuteAnonymousContext
    {
        get{ return !bIsTriggerContext; }
    }

}