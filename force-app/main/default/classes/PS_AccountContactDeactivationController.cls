/**
  * VF Page    : PS_AccountContactDeactivationPage
  * Controller : PS_AccountContactDeactivationController
  * Description: Controller class to deactivate AccountContact__c and UniversityCourseContact__c records. 
  **/  


/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:          PS_AccountContactDeactivationController.Cls
   Description:   Controller class to deactivate AccountContact__c and UniversityCourseContact__c records.
   Test Class:    PS_AccountContactDeactivationController_Test 
   
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   24/09/2015         0.1        Accenture - Karan Khanna              Created
                                
---------------------------------------------------------------------------------------------------------------------------------------------------------- */




public with sharing class PS_AccountContactDeactivationController {

    //Declaration of Common Variables
    public Id accContactId;
    public AccountContact__c accContact;
    public Boolean confirmation {get;set;}
    public Boolean transactionFinish {get;set;}
    
    //Constructor    
    public PS_AccountContactDeactivationController(ApexPages.Standardcontroller controller) {
        
        accContactId = controller.getId();  
        accContact = new AccountContact__c();
        accContact = [SELECT Id, Active__c, Account__c, Contact__c FROM AccountContact__c WHERE Id=:accContactId];
        confirmation = false;
        transactionFinish = false;
        
    } // end of Constructor
    
    /*---------------------------------------------------------------------------------------------------------------------------------
    Author:         Karan Khanna
    Company:        Accenture
    Description:    This method is used to execute business logic in sequesnce when user confirms the deactivation
    Inputs:         None
    Returns:        PageReference
    
    <Date>          <Authors Name>      <Brief Description of Change> 
    24-Sept-2015    Accenture_Karan     Initial version of the code
     
    -------------------------------------------------------------------------------------------------------------------------------*/        

    public PageReference proceedDeactivation() {
        
        if(confirmation == false) {
            
              ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Error,label.PS_AccContDeact_SelectCheckbox);
              ApexPages.addMessage(msg);             
        }
        else {
                        
            PS_AccountContactDeactivationHandler accContHandler = new PS_AccountContactDeactivationHandler(); // Handler holds the business logic
            accContHandler.initiateMethodsExecution(accContact);
                        
            transactionFinish = true;
        }
        return null;
    } // end of proceedDeactivation()
    
} // end of Class