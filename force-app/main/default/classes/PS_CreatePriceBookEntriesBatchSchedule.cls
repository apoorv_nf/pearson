/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_CreatePriceBookEntriesBatchSchedule.Cls 
   Description:     Scheduler class for scheduling PS_CreatePriceBookEntriesBatch
   Test Class:      PS_CreatePriceBookEntriesBatchScheduleTest
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   15/01/2016         0.1        Accenture - Karan Khanna                Created
                                
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

global class PS_CreatePriceBookEntriesBatchSchedule implements schedulable
{
    global void execute(SchedulableContext sc)
    {
        PS_CreatePriceBookEntriesBatch b = new PS_CreatePriceBookEntriesBatch();
        database.executebatch(b);
    }
}