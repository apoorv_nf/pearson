/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_BatchMarketingBlackoutTest
   Description:     Unit tests for PS_BatchMarketingBlackoutAccountUpdate / PS_BatchMarketingBlackoutAccConUpdate
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   23/10/2015       1.0           Tom Carman - Accenture                              Created
   28/06/2016       2.0           Pooja                                      //modified to run the test class
---------------------------------------------------------------------------------------------------------------------------------------------------------- */


@isTest
private class PS_BatchMarketingBlackoutTest {


    
    static final String MASTER_ACCOUNT_SMALL = 'Master Small Account';
    static final String MASTER_ACCOUNT_SMALL_ALTERNATIVE = 'Master Small Alternative Account';
    static final String MASTER_ACCOUNT_BIG = 'Master Big Account';
    static final String MASTER_ACCOUNT_NO_BLACKOUT = 'Master NoBlackout Account';

    


    @isTest private static void testAccountContactsBlackedOut_smallHierachy() {

        // Build 
        initialiseSmallAccountHierachy();

        // Setup //
        Account masterAccount = [SELECT Id FROM Account WHERE Name =:MASTER_ACCOUNT_SMALL LIMIT 1];
        masterAccount.Blackout_Flag_Account__c = true;
        update masterAccount;


        // Execute //
        Test.startTest();
            Database.executeBatch(new PS_BatchMarketingBlackoutAccountUpdate());
        Test.stopTest();

        // Assert //
        List<AccountContact__c> accountContacts = [SELECT Id, Blackout_Flag_AccountContact__c FROM AccountContact__c];

        for(AccountContact__c accountContact : accountContacts) {
            System.assertEquals(true, accountContact.Blackout_Flag_AccountContact__c, 'The batch has processed a parent Account of this AccountContact, and blackout should be TRUE');
        }


    }
    
    
    @isTest private static void testAccountContactsBlackedOut_bigHierachy() {

        // Build //
        initialiseBigAccountHierachy();

        
        // Setup //
        Account masterAccount = [SELECT Id FROM Account WHERE Name = :MASTER_ACCOUNT_BIG LIMIT 1];
        masterAccount.Blackout_Flag_Account__c = true;
        update masterAccount;


        // Execute //
        Test.startTest();
            Database.executeBatch(new PS_BatchMarketingBlackoutAccountUpdate());
        Test.stopTest();


        // Assert //
        List<AccountContact__c> accountContacts = [SELECT Id, Blackout_Flag_AccountContact__c FROM AccountContact__c];

        for(AccountContact__c accountContact : accountContacts) {
            System.assertEquals(true, accountContact.Blackout_Flag_AccountContact__c, 'The batch has processed a parent Account of this AccountContact, and blackout should be TRUE');
        }


    }



    @isTest private static void testChildrenAccountsBlackedOut() {

        // Main tests are concerned with AccountContact. 
        // This test checks the parents of AccountContacts are also marked as blacked out

        // Build //
        initialiseSmallAccountHierachy();

        // Setup //
        Account masterAccount = [SELECT Id FROM Account WHERE Name =:MASTER_ACCOUNT_SMALL LIMIT 1];
        masterAccount.Blackout_Flag_Account__c = true;
        update masterAccount;


        // Execute //
        Test.startTest();
            Database.executeBatch(new PS_BatchMarketingBlackoutAccountUpdate());
        Test.stopTest();

        List<Account> accounts = [SELECT Id, Blackout_Flag_Account__c FROM Account];

        System.assertEquals(3, accounts.size());

        for(Account account : accounts) {
            System.assertEquals(true, account.Blackout_Flag_Account__c, 'The batch has processed a parent Account of this Account, and blackout should be TRUE');
        }       

    }


    
    @isTest private static void testContactsAreBlackedOut() {

        // Main tests are concerned with AccountContact. 
        // This test checks the Contacts are also marked as blacked out

        // Build //
        initialiseSmallAccountHierachy();

        // Setup //
        Account masterAccount = [SELECT Id FROM Account WHERE Name =:MASTER_ACCOUNT_SMALL LIMIT 1];
        masterAccount.Blackout_Flag_Account__c = true;
        update masterAccount;


        // Execute //
        Test.startTest();
            Database.executeBatch(new PS_BatchMarketingBlackoutAccountUpdate());
        Test.stopTest();

        List<Contact> contacts = [SELECT Id, opt_out_of_marketing__c FROM Contact];

        System.assertEquals(3, contacts.size());

        for(Contact contact : contacts) {
            System.assertEquals(true, contact.opt_out_of_marketing__c, 'The batch has processed a parent Account of this Contact, and blackout should be TRUE');
        }       

    }



    @isTest private static void testOtherAccountContactsAreNotBlackedOut() {

        // Build //

        // initialise two small hierachies
        initialiseSmallAccountHierachy();
        initialiseSmallAccountHierachy();


        // Setup //

        // change name of one, so we can later identify it
        Account accountNotToBlackout = [SELECT Id FROM Account WHERE Name =:MASTER_ACCOUNT_SMALL LIMIT 1];
        accountNotToBlackout.Name = MASTER_ACCOUNT_NO_BLACKOUT;
        update accountNotToBlackout;

        // get the otherone, and set it to blackout
        Account accountToBlackout = [SELECT Id FROM Account WHERE Name = :MASTER_ACCOUNT_SMALL LIMIT 1];
        accountToBlackout.Blackout_Flag_Account__c = true;
        update accountToBlackout;


        // Execute //

        Test.startTest();
            Database.executeBatch(new PS_BatchMarketingBlackoutAccountUpdate());
        Test.stopTest();


        // Assert //


        // Blacked out

        List<AccountContact__c> blackedOutAccountContacts = [SELECT Id, Blackout_Flag_AccountContact__c FROM AccountContact__c
                                                            WHERE Account__c = :accountToBlackout.Id
                                                            OR Account__r.ParentId = :accountToBlackout.Id
                                                            OR Account__r.Parent.ParentId = :accountToBlackout.Id];

        System.assertEquals(6, blackedOutAccountContacts.size());

        for(AccountContact__c accountContact : blackedOutAccountContacts) {
            System.assertEquals(true, accountContact.Blackout_Flag_AccountContact__c, 'These Account contacts should have been blacked out');

        }

        // NON Blacked out

        List<AccountContact__c> nonBlackedOutAccountContacts = [SELECT Id, Blackout_Flag_AccountContact__c FROM AccountContact__c
                                                                WHERE Account__c = :accountNotToBlackout.Id
                                                                OR Account__r.ParentId = :accountNotToBlackout.Id
                                                                OR Account__r.Parent.ParentId = :accountNotToBlackout.Id];

        System.assertEquals(6, nonBlackedOutAccountContacts.size());

        for(AccountContact__c accountContact : nonBlackedOutAccountContacts) {
            System.assertEquals(false, accountContact.Blackout_Flag_AccountContact__c, 'These Account contacts should NOT have been blacked out');
            
        }



    }



    @isTest private static void testWithMulitpleEntryAccounts() {

        // Build //

        // initialise two small hierachies
        initialiseSmallAccountHierachy();
        initialiseSmallAccountHierachy();


        // Setup //

        List<Account> accountsToBlackout = [SELECT Id FROM Account];

        for(Account account : accountsToBlackout) {
            account.Blackout_Flag_Account__c = true;
        }

        update accountsToBlackout;


        // Execute //

        Test.startTest();
            Database.executeBatch(new PS_BatchMarketingBlackoutAccountUpdate());
        Test.stopTest();


        // Assert //

        List<AccountContact__c> accountContacts = [SELECT Id, Blackout_Flag_AccountContact__c FROM AccountContact__c];

        for(AccountContact__c accountContact : accountContacts) {
            System.assertEquals(true, accountContact.Blackout_Flag_AccountContact__c, 'The batch has processed a parent Account of this AccountContact, and blackout should be TRUE');
        }

    }



    // Data Setup Methods //


    @isTest private static void initialiseSmallAccountHierachy() {

        /*
            Set up hierachy as below:
                                        
                |---MASTER ACCOUNT          
                    |
                    |---ACCOUNT 1 A
                    |   |--- ACCOUNT 2 AA
        
        */


        // Accounts //

        List<Account> accounts = TestDataFactory.createAccount(3, 'Organisation');

        accounts[0].Name = MASTER_ACCOUNT_SMALL;
        accounts[1].Name = 'Account 1 A';
        accounts[2].Name = 'Account 2 AA';

        insert accounts;

        // set hierachy
        accounts[1].ParentId = accounts[0].Id;
        accounts[2].ParentId = accounts[1].Id;

        update accounts;


        // Contacts //

        List<Contact> contacts = TestDataFactory.createContacts(3);
        contacts[0].accountid = accounts[0].Id;
        contacts[1].accountid = accounts[1].Id;
        contacts[2].accountid = accounts[2].Id;
        insert contacts;


        // AccountContacts //

        List<AccountContact__c> accountContacts = new List<AccountContact__c>();

        for(Integer i = 0; i<3; i++) {
            accountContacts.add(TestDataFactory.createAccountContact(accounts[i].Id, contacts[i].Id));
        }

        insert accountContacts;


    }


    @isTest private static void initialiseBigAccountHierachy() {
        
        // WARNING -- this method contains a DML in For Loop

        // Set up a hierachy that is 20 deep, 200 wide on each layer
        User usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Price_List__c= 'Math & Science');

        usr.market__c='US';
        usr.Business_Unit__c='Higher Ed';
        usr.Line_of_Business__c='Higher Ed';
        insert usr;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true);
        insert byp;
        
        system.runas(usr){
        List<Id> parentIds = new List<Id>();

        List<Account> masterAccount = TestDataFactory.createAccount(1, 'Organisation');
        masterAccount[0].Name = MASTER_ACCOUNT_BIG;
        insert masterAccount;

        parentIds.add(masterAccount[0].Id);

        Integer depth = 20; // WARNING - This value sets how many times the for-loop containing DML will fire
        Integer width = 10; // modified by Pooja to run the test class

        for(Integer i=0; i<depth; i++) {

            List<Account> childAccounts = TestDataFactory.createAccount(width, 'Organisation');

            Integer childAccountsSize = childAccounts.size();

            for(Integer j=0; j<childAccountsSize; j++) {

                if(parentIds.size() == 1) { // this is first iteration, all children should have the master account as parent
                    childAccounts.get(j).ParentId = parentIds.get(0); 
                } else {
                    childAccounts.get(j).ParentId = parentIds.get(j);
                }
            }

            insert childAccounts; // WARNING - DML IN FOR LOOP

            parentIds.clear();
            system.debug('KP...inserted child accounts');

            for(Account childAccount : childAccounts) {
                parentIds.add(childAccount.Id);
            }

        }
    }//end system run as
    }


}