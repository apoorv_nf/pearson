/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:         PS_AccountcontactTriggerhandler_Test.cls
   Description:  Test Class For AccountContactTriggerHandler
                
   Date             Version     Author                  Tag     Summary of Changes 
   -----------      -------     -----------------       ---     ------------------------------------------------------------------------
   13/09/2015        0.1         Accenture               None    Initial Version
   08/03/2016        0.2         Abhinav                 None    populating the National Identification Number on Contact with correct syntax. 
  -------------------------------------------------------------------------------------------------------------------------------------------------------- */
@isTest
private class PS_AccountcontactTriggerhandler_Test {
                
                    
Static testmethod void myUnitTest_3(){
         TestClassAutomation.FillAllFields = true;
         RecordType OrgAcc = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Account' AND Name = 'School'];
        Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
       
        sAccount.BillingCountry         = 'Australia';
        sAccount.BillingState           = 'Victoria';
        sAccount.BillingCountryCode     = 'AU';
        sAccount.BillingStateCode       = 'VIC';
        sAccount.ShippingCountry        = 'Australia';
        sAccount.ShippingState          = 'Victoria';
        sAccount.ShippingCountryCode    = 'AU';
        sAccount.ShippingStateCode      = 'VIC';
        sAccount.Lead_Sponsor_Type__c   = null; 
        sAccount.IsCreatedFromLead__c = True;       
        sAccount.recordtypeId        =OrgAcc.Id ;
        insert sAccount;
    
        Contact sContact                = (Contact)TestClassAutomation.createSObject('Contact');
        sContact.AccountId              = sAccount.Id;
       sContact.OtherCountry           = 'Australia';
        sContact.OtherState             = 'Victoria';
        sContact.OtherCountryCode       = 'AU';
        sContact.OtherStateCode         = 'VIC';
        sContact.MailingCountry         = 'Australia';
        sContact.MailingState           = 'Victoria';
        sContact.MailingCountryCode     = 'AU';
        sContact.MailingStateCode       = 'VIC';
        sContact.Mobile_Last_Updated__c = null;
        //Abhinav - populating Contact record with the correct syntax of National Identity Number field.
        //sContact.National_Identity_Number__c = '9412055708083';
        sContact.National_Identity_Number__c = '9004300344085';
        sContact.Market__c = 'ZA';
    
        Test.startTest();
        insert sContact;
          
         AccountContact__c sAccountContact = [Select Id,AccountContact__c.Primary__c  FROM AccountContact__c WHERE Contact__c = : sContact.Id limit 1];
         
       AccountContactTriggerHandler testa= new AccountContactTriggerHandler(true,5);

       AccountContact__c[] accnt;
       
       accnt = [Select Id,AccountContact__c.Primary__c,account__c,contact__c,AccountRole__c,Financially_Responsible__c  FROM AccountContact__c WHERE Contact__c = : sContact.Id limit 1];

       AccountContact__c[] accntol;

       accntol = [Select Id,AccountContact__c.Primary__c,account__c,contact__c,AccountRole__c,Financially_Responsible__c,Primary_Account__c, Role_Detail__c  FROM AccountContact__c WHERE Contact__c = : sContact.Id limit 1];
  
       map<id,AccountContact__c> acc =new map<id,AccountContact__c>();
       map<id,AccountContact__c> acccon =new map<id,AccountContact__c>();
       acccon.put(accntol.get(0).Id, accntol.get(0));
     
           System.assertEquals(true,  testa.bIsTriggerContext);  
           System.assertEquals(false,  testa.bIsVisualforcePageContext,false);   
           System.assertEquals(false, testa.bIsWebServiceContext,false);  
           System.assertEquals(false,  testa.bIsExecuteAnonymousContext,false);  
                  
         testa.OnAfterUpdate(accnt,accntol,acc,acccon);
                  
         testa.OnAfterdelete(accnt,acc);
         // testa.OnBeforedelete(accnt,acc);
         testa.OnBeforeUpdate(accnt);
         testa.OnUndelete(accnt);  
         AccountContactSync sync = new AccountContactSync();
         sync.SetAsNotPrimary(accnt); 
         try
         {
           testa.OnBeforeInsert(null);           
         }
         catch(Exception e)
         {  
           System.assert(e != null, 'The exception was not raised');           
         }
         
         try
         {           
           testa.OnAfterInsert(null);
         }
         catch(Exception e)
         {  
           System.assert(e != null, 'The exception was not raised');           
         }           
               
         Test.stopTest();}
    
    static testMethod void testdeleteCourseContact(){    

        Account AccountRec1 = new account( Name='Test AccountDlt', Phone='+9100000' ,IsCreatedFromLead__c = True, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560137');
        insert AccountRec1;
      
        Account AccountRec = new account( Name='Test AccountDlt1', Phone='+9100000' ,IsCreatedFromLead__c = True, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG', ShippingPostalCode = '560037');
        insert AccountRec;
        List<Contact> contactList= new List<Contact>();
        
        Contact con = new Contact(FirstName='Test', LastName='DltAccCon',
                                                AccountId=AccountRec.Id ,Salutation='MR.', Email='testDltAccCon@gmail.com',MobilePhone = '8884888472',
                                                Phone='111222333', Preferred_Address__c='Mailing Address', MailingCity = 'Newcastle', MailingState='Northumberland', 
                                                MailingCountry='United Kingdom',  MailingStreet='1st Street' , MailingPostalCode='NE28 7BJ',Passport_Number__c = 'ABCDE12345', 
                                                Home_Phone__c = '9999',Birthdate__c = Date.newInstance(1900 , 10,10),Ethnic_Origin__c = 'Asian' , Marital_Status__c = 'Single',First_Language__c = 'English');          
        contactList.add(con);
        insert contactList;
        //Create Course
        list<UniversityCourse__c> courList = TestDataFactory.insertCourse();
        courList[0].Account__c = AccountRec1.Id;
        insert courList[0];
        
        
        list<UniversityCourseContact__c> courContList = new list<UniversityCourseContact__c>();
        UniversityCourseContact__c tcoucontact = new UniversityCourseContact__c();        
        tcoucontact.UniversityCourse__c = courList[0].Id;
        tcoucontact.Contact__c = contactList[0].Id;
        tcoucontact.AccountContactRole__c = 'Parent';
        tcoucontact.AccountContactRoleDetail__c = 'Mother';
        courContList.add(tcoucontact);
        insert courContList;
        
        List<Product2> productList = Testdatafactory.createProduct(1);
        insert productList;
        List<Asset> assetList = TestDataFactory.insertAsset(1,courList[0].id,contactList[0].id,null,AccountRec1.Id);
        insert assetList;
        
        test.startTest();
            AccountContactTriggerHandler testa= new AccountContactTriggerHandler(true,5);
            map<id,AccountContact__c> acc =new map<id,AccountContact__c>();
            AccountContact__c[] accnt;
            accnt = [SELECT Id,AccountContact__c.Primary__c,account__c,contact__c,AccountRole__c,Financially_Responsible__c  FROM AccountContact__c WHERE Account__c=:AccountRec1.Id and contact__c=:contactList[0].Id];
            delete accnt;
            testa.OnAfterdelete(accnt,acc);
            undelete accnt;
            testa.OnUndelete(accnt);
        
        test.stopTest();

    }
 }