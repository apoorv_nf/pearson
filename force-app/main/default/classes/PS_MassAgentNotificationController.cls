/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_MassAgentNotificationController.cls 
   Description:     Visual force controller class for PS_MassAgentNotification page 
   Test class:      PS_MassAgentNotificationController_TEST.cls 
   Date             Version              Author                          Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  16-Nov-2015         0.1              Sudhakar Navuluri                 RD-01515-for send massagent task notification 
  3/11/2016                            Rashmi Prasad                     Updatde the tasksave method 
  20/06/2016                           RG                                D-5507 for Mass Notification Error 
  02-Nov-2016                          Manikanta Nagubilli               Added "getGroupMembers" method and Updated the "tasksave" method to create the tasks for Roles,Roles and internal subordinates and subgroups.
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public with sharing class PS_MassAgentNotificationController
{
    public Task taskRecord{get; set;} 
    public boolean isUserExcluded{ get; set; }  
    public boolean showAllGroups{ get; set; }
    public boolean showmessage{ get; set; }
    public boolean showgroupmessage{ get; set; }
    Public List<string> leftselected{get;set;}
    Public List<string> rightselected{get;set;}
    public Set<id> SubGroupIds{get;set;}
    Set<string> availset;
    Set<string> availset1= new Set<string>();
    Set<string> selectedset = new Set<string>();
    set<id> GroupIds = new Set<id>(); 
/**
*Description:Constructor Start - Will Initialise the groups/queues for the logged in user. 
*@param:NA
*@return:NA
*@throws:NA
**/ 
    public PS_MassAgentNotificationController(ApexPages.StandardController controller) 
    {
        availset= new Set<string>();
        GroupIds = new Set<id>(); 
        SubGroupIds = new Set<id>(); 
        showmessage=true;  
        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.PS_MassAgentNotification_Info));
        isUserExcluded= false;
        this.taskRecord=(Task)controller.getRecord(); 
        groupsbasedonuser();   
    }
     
/**
*Description:To dispaly the groups based on logegd in user 
*@param:NA
*@return:NA
*@throws:NA
**/
    public void groupsbasedonuser()
    {
        for(groupmember  groupMemberList :[select group.name ,group.Id from groupmember where UserOrGroupid=:UserInfo.getUserId() and group.type='Regular' limit 50000])
        availset.add(groupMemberList.group.name);  
    } 
  
/**
*Description:this method is used to add values that are selected by user to selected and clear the values from Available  
*@param:NA
*@return:NA
*@throws:NA
**/
    public PageReference selectclick()
    {   
        for (String selectedValues : leftselected)
        {
            availset.remove(selectedValues);
            selectedset.add(selectedValues);
        }
        return null;
    }   
   
/**
*Description:this method is used to add values that are selected by user to Avalible and clear the values from Selected   
*@param:NA
*@return:NA
*@throws:NA
**/
    public PageReference unselectclick()
    {
        for (String selectedValues : rightselected)
        {
            selectedset.remove(selectedValues );
            availset.add(selectedValues );
        }
        return null;
    }
    
/**
*Description:this method is used to retun list values to avliable groups based on current logged in user   
*@param:NA
*@return:NA
*@throws:NA
**/
    public List<SelectOption> getunSelectedValues()
    {
        List<SelectOption> options = new List<SelectOption>();
        List<string> tempList = new List<String>();
        
        tempList.addAll(availset);
        tempList.sort();
        for (string selectedValues : tempList)
            options.add(new SelectOption(selectedValues ,selectedValues));
        return options;
    }
    
/**
*Description:this method is used to add list groups to selected groups
*@param:NA
*@return:NA
*@throws:NA
**/
    public List<SelectOption> getSelectedValues()
    {
        List<SelectOption> options1 = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(selectedset);
        tempList.sort();
        for(String selectedValues : tempList)
            options1.add(new SelectOption(selectedValues ,selectedValues ));
        return options1;
    }
        
/**
*Description:this method is used to Load AllGroups.
*@param:NA
*@return:NA
*@throws:NA
**/  
    public void loadAllGroups()
    {                     
        availset= new Set<string>();
        if(showAllGroups){
        showgroupmessage=true;
        showmessage=false;       
        for(groupmember  groupMemberLst :[select group.name ,group.Id from groupmember where group.type='Regular' limit 50000]) {      
           availset.add(groupMemberLst.group.name);
           Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.PS_MassAgentNotification_Message));
        }
        }
        else{
        showgroupmessage=false;
        showmessage=true;
          groupsbasedonuser();
          Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.PS_MassAgentNotification_Info));
        }
        getunSelectedValues();    
     }
     
    /**
    *Description:this method is used to Seperate the Users and groups from group Members
    *@param:NA
    *@return:UserIds
    *@throws:NA
    **/ 
    private Set<ID> getGroupMembers(Set<String> selectedset)
    {
        set<id> myUserIds = new set<id>();
        Map<id,GroupMember> selectedGroupMembers= new Map<id,GroupMember>([SELECT UserOrGroupId FROM GroupMember WHERE (group.name IN :selectedset OR group.id IN :selectedset) and group.type='Regular' limit 50000]);
       
        for(GroupMember groupMemberIds : selectedGroupMembers.values()){
            if(((string)groupMemberIds.UserOrGroupId).startswith(Schema.SObjectType.User.getKeyPrefix()))
            {
                if (isUserExcluded){
                    if (groupMemberIds.UserOrGroupId != userinfo.getuserid())
                        myUserIds.add(groupMemberIds.UserOrGroupId);     
                }
                else
                    myUserIds.add(groupMemberIds.UserOrGroupId);
            } 
            else if(((string)groupMemberIds.UserOrGroupId).startswith(Schema.SObjectType.Group.getKeyPrefix()))
            {
                GroupIds.add(groupMemberIds.UserOrGroupId);
            }                            
        }  
        
        return myUserIds;
    } 
    
/**
*Description:this method is used to create massagent task notification
*@param:NA
*@return:NA
*@throws:NA
**/
    public void tasksave()
    {        
        showmessage=true;
        showgroupmessage=false;
        Set<ID> UserIds= new Set<ID>();
        Set<ID> userSet = new Set<ID>();
        Set<ID> RoleGroupIds = new Set<ID>();
        Set<ID> RoleIds = new Set<ID>();
        SubGroupIds = new Set<ID>();
        Set<ID> SubUsers = new Set<ID>();
        SubUsers = getGroupMembers(selectedset); //Getting the Direct Users from the selected Group
        if(!SubUsers.IsEmpty()){
            UserIds.addall(SubUsers);
        }
        
        /*Getting the Role ids*/
        if(!GroupIds.IsEmpty())
        {
            Set<id> PassRoleids = new Set<id>();
            Set<id> SubRoleUsersids = new Set<id>();
            Map<id,Group> GrpList = new Map<id,Group>([Select id,RelatedId,Type from Group where id IN: GroupIds]);
            if(!GrpList.Values().IsEmpty())
            for(group grp: GrpList.Values())
            {
                if(grp.Relatedid != null && ((string)grp.Relatedid).startswith(Schema.SObjectType.UserRole.getKeyPrefix()))
                {
                    Roleids.add(grp.RelatedId);
                    if(grp.type.contains('RoleAnd'))
                    {
                        PassRoleids.add(grp.RelatedId);
                    }
                }
                else if(grp.Relatedid == null && grp.Type == 'Regular')
                {
                    SubGroupIds.add(grp.id);
                }
            }
            SubRoleUsersids = RoleUtils.getAllSubRoleIds(PassRoleids);
            if(!SubRoleUsersids.IsEmpty())
            {
                Map<Id,User> users = new Map<Id, User>([Select Id, Name From User where UserRoleId IN :SubRoleUsersids]);
                UserIds.addall(users.Keyset());
            }
        }
        
        if(!Roleids.IsEmpty())
        {
            Map<Id,User> users = new Map<Id, User>([Select Id, Name From User where UserRoleId IN :Roleids]);
            UserIds.addall(users.Keyset());
            if(isUserExcluded && UserIds.contains(userinfo.getuserid()))
            UserIds.remove(userinfo.getuserid());
            
        }
        
        /*Passing the Inner Group IDS to create the tasks for the users in the innerGroups*/
        if(!SubGroupIds.IsEmpty())
        {
            Id batchInstanceId = Database.executeBatch(new AssignTasks(SubGroupIds,isUserExcluded,taskRecord), 50);
        }
        
        if (UserIds.IsEmpty()) 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.warning,Label.PS_MassAgentNotification_Warning));
        else
        {
            String count = RoleUtils.createTask(UserIds,taskRecord);
            if(count != null)
            {
                integer grpErrorCount = 0;
                integer errorCount = 0;
                integer sucessCount = 0;
                if(count != null && count.contains('grpError'))
                {
                    grpErrorCount = integer.valueof(count.substringAfter(','));
                }
                if(count != null && count.contains('Sucess'))
                {
                    sucessCount = integer.valueof(count.substringAfter(','));
                }
                if(count != null && count.contains('error'))
                {
                    errorCount = integer.valueof(count.substringAfter(','));
                }
                if(sucessCount >= 1 && errorCount == 0){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,Label.PS_MassAgentNotification_Success_Message)); 
                }else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,+errorCount+' ' +Label.PS_MassAgentNotification_Error_Message)); 
                }
            }                          
        }                
    }

/**
*Description:this method is used to reset the page
*@param:NA
*@return:PageReference
*@throws:NA
**/
    public PageReference reload() 
    {  
        PageReference pageRef = new PageReference('/apex/PS_MassAgentNotification');
        pageRef.setRedirect(true);
        return pageRef;
    }
}