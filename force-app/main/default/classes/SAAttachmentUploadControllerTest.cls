/*
 *Author: SA Offshore
 *Date: 04/5/2017
 *Description: A very simple test class to ensure that the output from the PS_SA_AttachmentUploadController is correct
 */
 
 
 @isTest
 public class SAAttachmentUploadControllerTest{
 
    static testmethod void createcase(){
        case caseobj = new case(subject = 'test');
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);        
        SA_AttachmentUploadController controller = new SA_AttachmentUploadController(sc);
        //PS_SA_AttachmentUploadController attachClass = new PS_SA_AttachmentUploadController();
        
        PageReference pageRef = Page.SANBPTSWebForm;
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('nbptsid', 'customersupport');   
             
        controller.createCase();
        
    } 
 
    static testmethod void createcase2(){
        case caseobj = new case(subject = 'test');
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        string ConEmail;
        ConEmail='test@gmail.com';
        SA_AttachmentUploadController controller = new SA_AttachmentUploadController(sc);
        //PS_SA_AttachmentUploadController attachClass = new PS_SA_AttachmentUploadController();
        
        PageReference pageRef = Page.SANBPTSWebForm;
        Test.setCurrentPage(pageRef);
       
        pageRef.getParameters().put('nbptsid', 'tpprogram');
        controller.createCase();
        controller.clearInputs();
        controller.checkEmail(ConEmail);
    }
    
    static testmethod void createcaseNegative(){       
             
      
        Account acc = new Account();
        acc.name = 'test';
        insert acc;
        
        Contact conObj = new Contact();
        conObj.lastName ='test';
         conObj.FirstName ='test2';
        conObj.email = 'test@gmail.com';
        conObj.accountid = acc.id;
        conObj.Role__c='Partner';
        insert conObj;       
        
        case caseobj = new case();
        caseobj.SuppliedEmail=conObj.email;
        
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        
        SA_AttachmentUploadController controller = new SA_AttachmentUploadController(sc);
        SA_AttachmentUploadController controller1 = new SA_AttachmentUploadController(sc);
        SA_AttachmentUploadController controller2 = new SA_AttachmentUploadController(sc);
        SA_AttachmentUploadController controller3 = new SA_AttachmentUploadController(sc);
        SA_AttachmentUploadController controller4 = new SA_AttachmentUploadController(sc);
        PageReference pageRef = Page.SANBPTSWebForm;
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('nbptsid', 'tpprogram');
        controller.ContactName = 'teest';        
        controller.createCase();
        
        controller2.ContactName = 'teest';
        controller2.ConEmail=conObj.email;        
        controller2.createCase();
        
        controller3.ContactName = 'teest';
        controller3.ConEmail=conObj.email;  
        controller3.caseSubject='test';      
        controller3.createCase();
        
        controller4.ContactName = 'teest';
        controller4.ConEmail=conObj.email;  
        controller4.caseSubject='test';      
        controller4.caseDescription ='test';
        //controller4.createCase();
        
        
    }
 
 
 }