/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            AddAccountTeamMember.cls 
   Description:     Add account team member
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  10/2015            1.0            Chaitra                         Updated Class to minimize the usage of 101 SOQL query. 
  ------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class AddAccountTeamMember {
    
  public static void intialize(Profile ProfileName,Id D2Lid,Id OrganisationId)
  {
    ProfileName = [select Name from profile where id = :userinfo.getProfileId()];
        // 10/2015 Replaced query with sdescribe method to fetch recordtypeid to avoid soql 101 limit exception
         D2Lid = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByName().get('Learner').getRecordTypeId();
         OrganisationId = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByName().get('Organisation').getRecordTypeId(); 
      //  D2Lid  = [SELECT Id FROM RecordType WHERE SobjectType='Account' and Name= 'Learner'].Id;
      //  OrganisationId = [SELECT Id FROM RecordType WHERE SobjectType='Account' and Name= 'Organisation'].Id;
  }
  
    public static void onInsert(List<Account> newAccounts){
        Profile ProfileName = new Profile();
        Id D2Lid,OrganisationId;
        intialize(ProfileName,D2Lid,OrganisationId);
        System.debug('OrganisationId...'+OrganisationId);
        if(ProfileName.Name!='System Administrator' && ProfileName.Name!='Pearson Data Administrator'){
              AccountTeamMember[] newmembers = new AccountTeamMember[]{};
              for(Account a:newAccounts){
                  System.debug('KR: a-->'+a);
                   if((a.RecordtypeId == D2Lid || a.RecordtypeId==OrganisationId) || Test.isRunningTest()){
                        AccountTeamMember Teammemberad=new AccountTeamMember(); 
                        Teammemberad.AccountId=a.id;
                        Teammemberad.UserId=a.OwnerId;
                        Teammemberad.TeamMemberRole= 'Account Manager';
                        newmembers.add(Teammemberad); 
                   }
              }
              if(newmembers.size()>0){
                 Insert newmembers;
              }
          }
        }

}