/*
Class Name : PS_MassAgentNotificationControllerTest
Version :1.1
Created Date: 1/28/2016
Modification log:
---------------------------------------------------------------------------------------------------------------------------
Developer       Date(MM/DD/YEAR)     Description
----------------------------------------------------------------------------------------------------------------------------
Raushan Kumar     2/9/2016                Test class to insert a task and group into a group member as input and then trying to invoke the functions of the  main class.
Rashmi Prasad   3/4/2016                Added Negative scenario      
*/

@isTest
Private class PS_MassAgentNotificationControllerTest{
 static testMethod void validatePS_MassAgentNotificationController(){
    
    task ts=new task();
    list<GroupMember> GMlist = new list<GroupMember>();
 
    TestDataFactory.inserttask(ts);
    user u1=[select id from user where id=:userinfo.getUserId()];  
    
    system.runAs(u1){
    id pid=[select id from profile where name='System Administrator'].id;
    
    list<user> u = TestDataFactory.createuser(pid,1);
    insert u;  
    
   //Group testGroup = Testdatafactory.creategroup();
    
    
     
    Group testGroup = new Group();
    testGroup.Name = 'TaskNotification';
    testGroup.DeveloperName = 'ABC';
   // testGroup.relatedid =u[0].id;
    INSERT testGroup;
  
     Group g1 = new Group(Name='group name', type='Queue');
     insert g1;
     
      Group g2 = new Group(Name='group name1', type='Queue');
     insert g2;
    
    GroupMember GM = new GroupMember();
    GM.GroupId = g1.id;
    GM.UserOrGroupId =u[0].id;
    insert GM;
    
     Group testGroup2 = new Group();
     testGroup2.Name = 'TaskNotification2';
     testGroup2.DeveloperName = 'ABCDE';
     INSERT testGroup2;
     GroupMember GM2 = new GroupMember();
     GM2.GroupId = g2.id;
     GM2.UserOrGroupId =u[0].id;
     insert GM2;
          
    } 
    
    id pid=[select id from profile where name='System Administrator'].id;
    list<user> u2 = TestDataFactory.createuser(pid,1);    
    system.runAs(u2[0]){
        test.startTest();
         Group testGroup1 = new Group();
         testGroup1.Name = 'TaskNotification1';
         testGroup1.DeveloperName = 'ABCD';
         INSERT testGroup1;
         
         Group testGroup4 = new Group(Name = 'TaskNotification4',DeveloperName = 'testGroup');
         INSERT testGroup4;
         
         list<GroupMember> gmulist = new list<GroupMember>();
        
         GroupMember GMuser = new GroupMember();
         GMuser.GroupId = testGroup1.id;
         GMuser.UserOrGroupId =u2[0].id;
         gmulist.add(GMuser);
         
         GroupMember GMgroup = new GroupMember();
         GMgroup.GroupId = testGroup1.id;
         GMgroup.UserOrGroupId =testGroup4.id;
         gmulist.add(GMgroup);
         insert gmulist;
         
         
         
        //id gid = testGroup.id;
        ApexPages.StandardController sc = new ApexPages.standardController(ts);
        PS_MassAgentNotificationController sample =new PS_MassAgentNotificationController(sc);
        sample.SubGroupIds.add(testGroup1.id);
        sample.showallgroups=True;
        sample.reload();
        sample.groupsbasedonuser();
        sample.leftselected  = new list<String>();
        sample.leftselected.add('TaskNotification');
        sample.selectclick();
        sample.isUserExcluded=True;
        sample.tasksave();    
        sample.rightselected  = new list<String>();
        sample.rightselected.add('TaskNotification');
        sample.unselectclick();
        sample.getSelectedValues();
        sample.loadAllGroups();
        sample.showallgroups=False;
        sample.loadAllGroups();
        
       // AssignTasks ast = new AssignTasks(sample.SubGroupIds,sample.isUserExcluded,ts);
       // Id batchInstanceId = Database.executeBatch(new AssignTasks(sample.SubGroupIds,sample.isUserExcluded,ts), 500);
      //  Id batchInstanceId1 = Database.executeBatch(ast);
        system.runAs(u1){
        sample.isUserExcluded = false;
        AssignTasks cb = New AssignTasks(sample.SubGroupIds,sample.isUserExcluded,ts);
        cb.execute(null,gmulist);
            cb.Roleids.add(userinfo.getUserroleid()); 
        cb.execute(null,gmulist);    
        cb.finish(null);
        }
        test.stopTest();
    }
 }

//Negative scenario
 static testMethod void NegativetestPS_MassAgentNotificationController(){
    task ts=new task();   
    user u1=[select id from user where id=:userinfo.getUserId()];
    
    system.runAs(u1){
        id pid=[select id from profile where name='System Administrator'].id;
        list<user> u = TestDataFactory.createuser(pid,1);
        insert u;  
      
        Group testGroup = Testdatafactory.creategroup();
        GroupMember GM = new GroupMember();
        GM.GroupId = testGroup.id;
        GM.UserOrGroupId = u[0].id;
        insert GM;
        
        ts.OwnerId = GM.id;
        try{        
        insert ts;
        }
        catch(DmlException e){
        system.debug('case exception'+e.getMessage());
        Boolean expectedExceptionThrown =  e.getMessage().contains('Insert failed. First exception on row 0; first error: FIELD_INTEGRITY_EXCEPTION, Assigned To ID: id value of incorrect type') ? true : false;
        System.AssertEquals(expectedExceptionThrown, true);
        System.assertEquals('FIELD_INTEGRITY_EXCEPTION' ,  e.getDmlStatusCode(0));
        }
    } 
    
    id pid=[select id from profile where name='System Administrator'].id;
    list<user> u2 = TestDataFactory.createuser(pid,1);    
    system.runAs(u2[0]){
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.standardController(ts);
        PS_MassAgentNotificationController sample =new PS_MassAgentNotificationController(sc);
        sample.showallgroups=True;
        sample.reload();
        sample.groupsbasedonuser();
        sample.leftselected  = new list<String>();
        sample.leftselected.add('TaskNotification');
        sample.selectclick();
        sample.isUserExcluded=True;
        sample.tasksave();    
        sample.rightselected  = new list<String>();
        sample.rightselected.add('TaskNotification');
        sample.unselectclick();
        sample.getSelectedValues();
        sample.loadAllGroups();
        sample.showallgroups=False;
        sample.loadAllGroups();
         sample.isUserExcluded = true;
        test.stopTest();
       
    }
   
 }
  static testMethod void validatePS_MassAgentNotification1(){
    
    task ts=new task();
    list<GroupMember> GMlist = new list<GroupMember>();
 
    TestDataFactory.inserttask(ts);
    user u1=[select id from user where id=:userinfo.getUserId()];  
    
    system.runAs(u1){
    id pid=[select id from profile where name='System Administrator'].id;
    
    list<user> u = TestDataFactory.createuser(pid,1);
    insert u;  
    
   //Group testGroup = Testdatafactory.creategroup();
    
    
     
    Group testGroup = new Group();
    testGroup.Name = 'TaskNotification';
    testGroup.DeveloperName = 'ABC';
   // testGroup.relatedid =u[0].id;
    INSERT testGroup;
  
     Group g1 = new Group(Name='group name', type='Queue');
     insert g1;
     
      Group g2 = new Group(Name='group name1', type='Queue', doesIncludeBosses = true);
     insert g2;
    
    GroupMember GM = new GroupMember();
    GM.GroupId = g1.id;
    GM.UserOrGroupId =u[0].id;
    insert GM;
    
     Group testGroup2 = new Group();
     testGroup2.Name = 'TaskNotification2';
     testGroup2.DeveloperName = 'ABCDE';
     INSERT testGroup2;
     GroupMember GM2 = new GroupMember();
     GM2.GroupId = g2.id;
     GM2.UserOrGroupId =u[0].id;
     insert GM2;
          
    } 
    
    id pid=[select id from profile where name='System Administrator'].id;
    list<user> u2 = TestDataFactory.createuser(pid,1);    
    system.runAs(u2[0]){
        test.startTest();
         Group testGroup1 = new Group();
         testGroup1.Name = 'TaskNotification1';
         testGroup1.DeveloperName = 'ABCD';
         INSERT testGroup1;
         
         Group testGroup4 = new Group(Name = 'TaskNotification4',DeveloperName = 'testGroup');
         INSERT testGroup4;
         
         list<GroupMember> gmulist = new list<GroupMember>();
        
         GroupMember GMuser = new GroupMember();
         GMuser.GroupId = testGroup1.id;
         GMuser.UserOrGroupId =u2[0].id;
         gmulist.add(GMuser);
         
         GroupMember GMgroup = new GroupMember();
         GMgroup.GroupId = testGroup1.id;
         GMgroup.UserOrGroupId =testGroup4.id;
         gmulist.add(GMgroup);
         insert gmulist;
         
         
         
        //id gid = testGroup.id;
        ApexPages.StandardController sc = new ApexPages.standardController(ts);
        PS_MassAgentNotificationController sample =new PS_MassAgentNotificationController(sc);
        sample.SubGroupIds.add(testGroup1.id);
        sample.showallgroups=True;
        sample.reload();
        sample.groupsbasedonuser();
        sample.leftselected  = new list<String>();
        sample.leftselected.add('TaskNotification');
        sample.selectclick();
        sample.isUserExcluded=True;
        sample.tasksave();    
        sample.rightselected  = new list<String>();
        sample.rightselected.add('TaskNotification');
        sample.unselectclick();
        sample.getSelectedValues();
        sample.loadAllGroups();
        sample.showallgroups=False;
        sample.loadAllGroups();
        
      
        system.runAs(u1){
        sample.isUserExcluded = true;
        AssignTasks cb = New AssignTasks(sample.SubGroupIds,sample.isUserExcluded,ts);
        cb.execute(null,gmulist);
        cb.Roleids.add(userinfo.getUserroleid()); 
        cb.execute(null,gmulist);    
        cb.finish(null);
        }
        test.stopTest();
    }
 }
}