/************************************************************************************************************
* Apex Class Name   : PS_GenricTriggerInterface.cls
* Version           : 1.0 
* Created Date      : 23 Sept 2015
* Function          : Genric Interface for genric handler
* Additional Info   : There no change required for any method definition
					  
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Leonard Victor           23/09/2015				Interface for PS_GenricTriggerHandler
************************************************************************************************************/
public interface PS_GenricTriggerInterface {
    void beforeInsert(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew);
    void afterInsert(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew);
    void beforeUpdate(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld);
    void afterUpdate(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld);
    void beforeDelete(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld);
    void afterDelete(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld);
    void afterUndelete(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew);
}