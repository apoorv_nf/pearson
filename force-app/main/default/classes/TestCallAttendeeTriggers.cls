/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:         TestCallAttendeeTriggers.cls 
                
   Date             Version     Author                   Tag     Summary of Changes 
   -----------      -------     -----------------       ---     ------------------------------------------------------------------------
   26/08/2015       0.1         Accenture               None    Intial draft
  08/03/2016        0.2        Abhinav                  None    populating the National Identification Number  on Lead with correct syntax. 
  09/03/2016        0.3        Sneha                    None    populating national identification number field of contact        
  -------------------------------------------------------------------------------------------------------------------------------------------------------- */
@isTest
private class TestCallAttendeeTriggers
{
    static testMethod void myUnitTest()
    {
        // TO DO: implement unit test
        
        TestClassAutomation.FillAllFields = true;
        
        Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
        sAccount.BillingCountry         = 'Australia';
        sAccount.BillingState           = 'Victoria';
        sAccount.BillingCountryCode     = 'AU';
        sAccount.BillingStateCode       = 'VIC';
        sAccount.ShippingCountry        = 'Australia';
        sAccount.ShippingState          = 'Victoria';
        sAccount.ShippingCountryCode    = 'AU';
        sAccount.ShippingStateCode      = 'VIC';
        sAccount.Lead_Sponsor_Type__c   = null;

        
        insert sAccount;
        
        Contact sContact                = (Contact)TestClassAutomation.createSObject('Contact');
        sContact.AccountId              = sAccount.ID;
        sContact.MailingCountry         = 'Australia';
        sContact.MailingState           = 'Victoria';
        sContact.MailingCountryCode     = 'AU';
        sContact.MailingStateCode       = 'VIC';
        sContact.OtherCountry           = 'Australia';
        sContact.OtherState             = 'Victoria';
        sContact.OtherCountryCode       = 'AU';
        sContact.OtherStateCode         = 'VIC';
        sContact.National_Identity_Number__c = '9412055708083';
        insert sContact;
    
        Call__c sCall                   = (Call__c)TestClassAutomation.createSObject('Call__c');
        sCall.Account__c                = sAccount.Id;
        sCall.Call_Start_Time__c        = system.now();
        sCall.Call_End_Time__c          = system.now().addHours(1);
        
        insert sCall;
            
        Test.startTest();
            
            CallAttendee__c sCallAttendee           = (CallAttendee__c)TestClassAutomation.createSObject('CallAttendee__c');
            sCallAttendee.Call__c                   = sCall.Id;
            sCallAttendee.Client__c                 = sContact.Id;
            sCallAttendee.Pearson_Employee__c       = null; // You can only have one or the other
            
            insert sCallAttendee;
            
            update sCallAttendee;
            
            delete sCallAttendee;
            
            undelete sCallAttendee;
                            
        Test.stopTest();
    }
}