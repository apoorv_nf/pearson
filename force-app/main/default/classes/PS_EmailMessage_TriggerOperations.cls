/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_EmailMessage_TriggerOperations.cls 
   Description:     This class contains methods for Email Messages Triggers
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  14/12/2015         1.0            Sakshi Agarwal                         Initial Release 
  2/16/2016          2.0            KP                                     commented mpopulateLatestToAddress and amended validation method for RD-01530
  4/19/2016          2.1            Payal Popat                            Put null check for subject filed in mSendEmailWhenCaseIdNotPresent method
  5/5/2017           2.2            Christwin                              For Checking for unsupported Email Changed Equal to Contains
  5/10/2017          2.3            Jaydip B                               Changed the logic to delete cases to include textBofy and html Body.  INC3418750 
  09/05/2017         2.4            MAYANK LAL                             Changed the logic for capturing Cc address in incoming mails. CR-01471
  15/12/2017         2.5            Navaneeth                              Changed the logic for mulitple To and CC address into Single Address for UK. R6:IRUK        
------------------------------------------------------------------------------------------------------------------------------------------------------------ */

Public with sharing class PS_EmailMessage_TriggerOperations{ 
  public static void mSendEmailWhenCaseIdNotPresent(List<EmailMessage> triggeredEmails){
      // Initation of all variables used
      List<Id> CaseId = new List<Id>();
      List<String> sendTo = new List<String>();
      List<Case> updateCaseLst= new List<Case>();
      List<Case> caseLstToDelete= new List<Case>();
      Set<Id> caseLstToDeleteSet= new Set<Id>();      
      Map<Id,String> MapEmailAddress =new Map<Id, String>(); 
      List<Outbound_Notification__c> outnotlistToInsert= new List<Outbound_Notification__c>();  
        for(EmailMessage newEmail: triggeredEmails){
            /* Below changes introduced on 5/10/2017  INC3418750 */
            boolean SubjRef=false;
            boolean txtBodyRef=false;
            boolean htmlBodyRef=false;
            if(newEmail.Incoming && newEmail.Subject != '' && newEmail.Subject!= null && newEmail.Subject.contains('ref:')) {
                SubjRef=true;
                
            }
            
            if(newEmail.Incoming && newEmail.textBody != '' && newEmail.textBody!= null && newEmail.textBody.contains('ref:')) {
                txtBodyRef=true;
                
            }
            
            if(newEmail.Incoming && newEmail.htmlBody != '' && newEmail.htmlBody!= null && newEmail.htmlBody.contains('ref:')) {
                htmlBodyRef=true;
                
            }
            
            
            
            
            //if(newEmail.Subject != '' && newEmail.Subject!= null){
                
                // Modified to include Contains to have list of usupported Emails 5/5/2017
                if(SubjRef==false && txtBodyRef==false && htmlBodyRef==false ){
                    /* end of changes on  INC3418750   */
                    
                    // Added to check for list of emails in School Assessments
                    boolean emailMatched = False;
                    string toEmailValue = '';
                    set<string> emailSet = new set<string>();
                    List<string> emailList = new List<string>();
                    String emailString = '';
                    emailString = System.label.SA_customerSupportEmail;
                    emailList = emailString.split(',');
                    for (string temp : emailList) {
                        string tempTrimed = temp.trim();
                        emailSet.add(tempTrimed);
                    }
                    
                    set<string> toEmailSet = new set<string>();
                    List<string> toEmailList = new List<string>();
                    String toEmailString = '';
                    
                    // Check for To Address
                    if (newEmail.ToAddress != null && newEmail.ToAddress != '') {
                        toEmailString = newEmail.ToAddress;
                        toEmailList = toEmailString.split(';');
                        for (string temp : toEmailList) {
                            string tempTrimed = temp.trim();
                            toEmailSet.add(tempTrimed);
                        }
                    }
                    
                    //Check for cc Address
                    if (newEmail.CcAddress != null && newEmail.CcAddress != '') { 
                        toEmailList.clear();
                        toEmailString = newEmail.CcAddress;
                        toEmailList = toEmailString.split(';');
                        for (string temp : toEmailList) {
                            string tempTrimed = temp.trim();
                            toEmailSet.add(tempTrimed);
                        }
                    }
                    
                    //Check for Bcc Address
                    if (newEmail.BccAddress != null && newEmail.BccAddress != '') { 
                        toEmailList.clear();
                        toEmailString = newEmail.BccAddress;
                        toEmailList = toEmailString.split(';');
                        for (string temp : toEmailList) {
                            string tempTrimed = temp.trim();
                            toEmailSet.add(tempTrimed);
                        }
                    }

                    for (string temp:toEmailSet) {
                        if (emailSet.contains(temp) || System.label.CustomerSupportEmail == temp) {
                            emailMatched = True;
                            toEmailValue = temp;
                            break;
                        }
                    }

                    // Added to check for list of emails in School Assessments End 
                    if (emailMatched) {
                        Outbound_Notification__c outnot = new Outbound_Notification__c();
                        outnot.Event__c = 'Case AutoResponse';                
                        outnot.Type__c = 'Response';
                        outnot.PS_ArticleRecipient__c = toEmailValue;
                        outnot.PS_CaseFromEmail__c = newEmail.FromAddress;
                        outnotlistToInsert.add(outnot);
                        caseLstToDeleteSet.add(newEmail.ParentId);
                    }
                }
                // Modified to include Contains to have list of usupported Emails 5/5/2017 End
            //}
        }
        caseLstToDelete = [Select id,CaseNumber from Case where id IN:caseLstToDeleteSet];
        if(!caseLstToDelete.isEmpty()){
            Delete caseLstToDelete;
        }
        if (outnotlistToInsert.size()>0) {
           Database.SaveResult[] srList = Database.insert(outnotlistToInsert,false);
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                }
                else {
                    // Operation failed, so get all errors               
                    for(Database.Error err : sr.getErrors()) {
                      
                    }
                }
            }
       }      
  }//end mSendEmailWhenCaseIdNotPresent
  
  
  // Method Added for RD-01530 to populate a case field with the email ToAdderess of the latest email message
  
    public static void mpopulateLatestToAddress(List<EmailMessage> triggeredEmails,Set<id> caseId,Map<id,String> MapEmailAddress,Map<Id,String> mapCcEmailAddress){     
        List<Case> caseList = new List<Case>();
        List<Case> updateCaselst = new List<Case>();
        String userName = UserInfo.getUserName();
        User activeUser = [Select Email From User where Username = : userName limit 1];
        String userEmail = activeUser.Email;        
        
        if(CaseId.size()>0){
        caseList = [Select id,To_Email_Origin_Address__c,Recordtype.name,Origin,Cc_Email_origin_Address__c from Case where id IN: CaseId];   
        }
        for (Case caseObj:caseList){
            //Checking ToEmail Address
            if(MapEmailAddress.containsKey(caseObj.id) && MapEmailAddress != null  && caseObj.Origin == 'Email'){
               caseObj.To_Email_Origin_Address__c = MapEmailAddress.get(caseObj.id);
            }
                //Checking Cc Email Address
            if(mapCcEmailAddress.containsKey(caseObj.id) && mapCcEmailAddress != null){
                caseObj.Cc_Email_Origin_Address__c = mapCcEmailAddress.get(caseObj.id);
            }
                //caseObj.LatestEmailToAddressFromCustomer__c = MapEmailAddress.get(caseObj.id);     
                updateCaselst.add(caseObj);
            //} // to address end
        }
        // Added by Navaneeth For R6 IRUK - Start
            List<Case> updateCaseFinalList = new List<Case>();
            updateCaseFinalList = setToCCEmailOriginAddressforUKMarket(updateCaselst);
        // Added by Navaneeth For R6 IRUK - End
        if(!updateCaselst.isEmpty()){
            // Commented and Modified by Navaneeth For R6 IRUK - Start
          //  Database.Update(updateCaselst, false);
            Database.Update(updateCaseFinalList, false);
            // Commented and Modified by Navaneeth For R6 IRUK - End
        }
    }
    
    /**
    * Description : Method to validate From Address on sent mails
    * @param NA
    * @return NA
    * @throws NA
    **/      
    public static void mValidationonEmailFromAddress(List<EmailMessage> triggeredEmails,Set<Id> caseId,Set<String> toEmailAddr){      
        //Fetch Case details
        Map<Id,Case> casemap;
        if(CaseId.size()>0){
            casemap = new Map<Id,Case>([Select id,Recordtype.name,Origin,To_Email_Origin_Address__c,Contact.Email from Case where 
                        id IN: CaseId]);
                        //id IN: CaseId and Origin='Email']);

        }    
        if(!casemap.IsEmpty()){   
            for (EmailMessage newEmail: triggeredEmails) {
             if (String.valueOf(newEmail.ParentId) != null){
                if (casemap.containsKey(newEmail.parentId)){
                    String sCustEmail= casemap.get(newEmail.parentId).Contact.Email;  
                    //CR-01777: Added code by Vishista
                   // if(Label.PS_ServiceRecordTypes.contains(casemap.get(newEmail.parentId).Recordtype.name) && sCustEmail != null){
                   if(Label.PS_ServiceRecordTypes.contains(casemap.get(newEmail.parentId).Recordtype.name)){
                        if(newEmail.ToAddress != null || newEmail.BccAddress != null || newEmail.CcAddress != null){
	                            if (newEmail.FromAddress == UserInfo.getUserEmail()){
	                                newEmail.parentId.addError(System.label.Validation_Message_For_Incorrect_Email_Address);
	                                break;
	                            }
                        }    
                       /*
                        if (newEmail.ToAddress != null){
                            if (newEmail.ToAddress.contains(sCustEmail) && newEmail.FromAddress == UserInfo.getUserEmail()){
                                newEmail.parentId.addError(System.label.Validation_Message_For_Incorrect_Email_Address);
                                break;
                            }
                        }
                        if (newEmail.BccAddress!= null){
                            if (newEmail.BccAddress.contains(sCustEmail) && newEmail.FromAddress == UserInfo.getUserEmail()){
                                newEmail.parentId.addError(System.label.Validation_Message_For_Incorrect_Email_Address);
                                break;
                            }
                        }   
                        if (newEmail.CcAddress!= null){
                            if (newEmail.CcAddress.contains(sCustEmail) && newEmail.FromAddress == UserInfo.getUserEmail()){
                                newEmail.parentId.addError(System.label.Validation_Message_For_Incorrect_Email_Address);
                                break;
                            }
                        }  */                                             
                    }
                } // end if email with case 
              }//parentid not null      
            }
        }   //end case id          
   }//end method mValidationonEmailFromAddress
   
   /**
    * Description : Method to Update the Multiple into Single Address for UK Cases - By Navaneeth For R6:IRUK
    * @param List<Case>
    * @return List<Case>
    * @throws NA
    **/ 
   
   public static List<Case> setToCCEmailOriginAddressforUKMarket(List<Case> newCases){
        // New Method Written By Navaneeth  
        List<UKEmailRouting_BusinessVerticalMapping__mdt> UKRoutingList = [SELECT Routing_Address__c, DeveloperName FROM UKEmailRouting_BusinessVerticalMapping__mdt];
        List<Case> newRetCaseList = new List<Case>();
        for(Case caseVar : newCases){
            boolean checkflag;
            String ToEmail = caseVar.To_Email_origin_Address__c;
            String CcEmail = caseVar.Cc_Email_Origin_Address__c;
            String[] ToEmailArray=new String[]{};
            String[] CcEmailArray=new String[]{};
            
            List<String> ToEmailList = new List<String>();
            List<String> CcEmailList = new List<String>();
            //
            if(ToEmail!=null)
            {
                //if(ToEmail.contains(';'))
                    ToEmailArray = ToEmail.split(';');
                //else
                    //ToEmailArray[0]=ToEmail;
            }   
            //
            if(CcEmail!=null)
            {
               // if(CcEmail.contains(';'))
                    CcEmailArray = CcEmail.split(';');
                //else
                   // CcEmailArray[0]=CcEmail;
            
            }
            //
            
            for(Integer i=0;i<ToEmailArray.size();i++){
            ToEmailList.add(ToEmailArray[i]);
            
            }
            for(Integer i=0;i<CcEmailArray.size();i++){
            CcEmailList.add(CcEmailArray[i]);
            }
            //
            Integer countSize=0;
            if(ToEmailList.size() > CcEmailList.size())
                countSize = ToEmailList.size();
            else
                countSize = CcEmailList.size();
                
            checkflag = false;
            boolean ToAddressMatch = false;
            boolean CcAddressMatch = false;
            if(countSize > 0){
           
                for(Integer i=0;i<countSize;i++){ 
                
                    for (UKEmailRouting_BusinessVerticalMapping__mdt UKRoute : UKRoutingList){ 
               
                        if((i < ToEmailList.size()) && (!ToAddressMatch)){
                            if(ToEmailList[i].trim()==UKRoute.Routing_Address__c){
                                ToAddressMatch = true;
                               //caseVar.To_Email_origin_Address__c = UKRoute.Routing_Address__c;
                               caseVar.To_Email_origin_Address__c =ToEmailList[i].trim();                               
                               
                                //checkflag = true;
                            }
                            
                        }
                        if((i < CcEmailList.size()) && (!CcAddressMatch)){
                            if(CcEmailList[i].trim()==UKRoute.Routing_Address__c){
                                CcAddressMatch = true;
                                // To Handle CC Alone Scenario Start
                                if(ToEmailList.size()==0)
                                caseVar.To_Email_origin_Address__c = 'xyz@test.com';
                                // To handle CC Alone Scenario End
                                //caseVar.Cc_Email_Origin_Address__c = UKRoute.Routing_Address__c;
                                caseVar.Cc_Email_origin_Address__c = CcEmailList[i].trim();                                                                
                                //checkflag = true;
                            }
                             
                        }
                       
                    }    
                    //if(!ToAddressMatch && CcAddressMatch)
                    //caseVar.To_Email_origin_Address__c = null;
                }
                    
            }
            newRetCaseList.add(caseVar);
        }
        return newRetCaseList;
    }       
   
}