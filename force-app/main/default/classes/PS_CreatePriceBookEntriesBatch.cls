/* --------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_CreatePriceBookEntriesBatch.Cls
Description:     Batch class for creating PricebookEntry records for Pricebooks other than Standard as integration team will be creating PBE records in Standard Pricebook and 
we need to replicate the records for matching entries in Proposal_Pricelist_Mapping__mdt for other Pricebooks
Test Class:      PS_CreatePriceBookEntriesBatchTest
Date             Version           Author                  Tag                                Summary of Changes 
-----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
15/01/2016         0.1        Accenture - Tom Carman and                 Created
Karan Khanna
08-AUG-19          0.2        Cognizant-Raja Gopalarao B                 Modified as part of CR-02947
-----------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------- */

global class PS_CreatePriceBookEntriesBatch implements Database.Batchable<sobject>, Database.Stateful{
    
    
    public Date lastProcessedDate;
    public List<Proposal_Pricelist_Mapping__mdt> priceBookMappings;
    public Set<String> markets;
    public Map<String,Id> pricebookNameIdMap;
    public string standardPricebook = 'standardpricebook';
    
    
    /* CONSTRUCTORS */
    
    // standard constructor
    global PS_CreatePriceBookEntriesBatch() {
        this((Date)null);
    }
    
    
    // supplied date constructor
    global PS_CreatePriceBookEntriesBatch(Date inputDate) {
        
        setBatchRunFromDate(inputDate);
        getPriceBookMappings();
        getMarkets();
        getpricebookNameIdMap();        
        
    }
    
    
    /* BATCH METHODS */
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        
        
        
        return Database.getQueryLocator([SELECT 
                                         Id,
                                         Product2Id,
                                         Product2.Market__c,
                                         product2.Line_of_Business__c,
                                         product2.Business_Unit__c,
                                         product2.CurrencyISOCode,
                                         product2.Competitor_Product__c,
                                         Pricebook2.Name,
                                         UnitPrice,
                                         IsActive,
                                         CurrencyIsoCode,
                                         CreatedDate,
                                         LastModifiedDate
                                         FROM
                                         PricebookEntry
                                         WHERE
                                         Product2.Market__c IN :markets AND
                                         Product2.Competitor_Product__c = FALSE AND
                                         Pricebook2Id = :pricebookNameIdMap.get(standardPricebook) AND
                                         LastModifiedDate >= :lastProcessedDate]);
        
        
    }
    
    
    global void execute(Database.BatchableContext BC, List<sobject>scope) {
        
        // Variable declaration
        List<PricebookEntry> allPBERecords = (List<PricebookEntry>)scope;
        system.debug('allPBERecords'+allPBERecords);
        List<PricebookEntry> newPBERecords = new List<PricebookEntry>();
        List<PricebookEntry> toBeUpsertedPBERecords = new List<PricebookEntry>();
        Map<Id,Map<Id,PricebookEntry>> oldStandardPBProductIdPBEMap = new Map<Id,Map<Id,PricebookEntry>>();
        
        try {
            
            /* Differentiate new PricebookEntry records and old PricebookEntry records 
so that new PricebookEntry records can be created for new ones and 
existing PricebookEntry records can be updated for old ones  */
            for(PricebookEntry pbe : allPBERecords) {
                system.debug('allPBERecords'+allPBERecords);
                if(pbe.CreatedDate >= lastProcessedDate) {
                    system.debug('lastProcessedDate' +lastProcessedDate);
                    newPBERecords.add(pbe);
                }
                else {
                    
                    if(oldStandardPBProductIdPBEMap.containsKey(pbe.Product2Id)) {
                        oldStandardPBProductIdPBEMap.get(pbe.Product2Id).put(pbe.Id, pbe);
                    }
                    else {                          
                        oldStandardPBProductIdPBEMap.put(pbe.Product2Id, new Map<Id,PricebookEntry>{pbe.Id => pbe});
                    }
                    
                }
            }
            
            // logic for new entries in PricebookEntry
            if(!newPBERecords.isEmpty()) {
                
                for(PricebookEntry pbe : newPBERecords) {
                    //Modified by Raja Gopalarao B on 08-AUG-19 as part of CR-02947 Inclided the where condition PP_Onboarded__c = false
                    for(Proposal_Pricelist_Mapping__mdt configOPB : [SELECT Rep_Market__c, Rep_Line_of_Business__c, Rep_Business_Unit__c, Oppty_Currency__c, Order_Price_Book__c FROM Proposal_Pricelist_Mapping__mdt WHERE PP_Onboarded__c = false]) {           
                        
                        // If the PricebookEntry has a matching record in Proposal_Pricelist_Mapping__mdt then only create new records for Pricebook mentioned in Proposal_Pricelist_Mapping__mdt
                        if(pbe.Product2.Market__c == configOPB.Rep_Market__c && pbe.product2.Line_of_Business__c == configOPB.Rep_Line_of_Business__c && pbe.product2.Business_Unit__c == configOPB.Rep_Business_Unit__c && 
                           pbe.product2.CurrencyISOCode == configOPB.Oppty_Currency__c && configOPB.Order_Price_Book__c != null) {
                               
                               String configPBName = configOPB.Order_Price_Book__c.toLowercase();
                               configPBName = configPBName.deleteWhitespace();                        
                               
                               if(pricebookNameIdMap.get(configPBName) != null) {
                                   
                                   PricebookEntry newpbe = new PricebookEntry();
                                   newpbe.Product2Id = pbe.Product2Id;
                                   newpbe.UnitPrice = pbe.UnitPrice;
                                   newpbe.IsActive = pbe.IsActive;
                                   newpbe.CurrencyIsoCode = pbe.CurrencyIsoCode;
                                   newpbe.pricebook2Id = (Id)pricebookNameIdMap.get(configPBName);
                                   
                                   toBeUpsertedPBERecords.add(newpbe);
                               }                       
                           }
                    }
                }
            }
            
            // logic for updated entries in PricebookEntry
            if(!oldStandardPBProductIdPBEMap.keySet().isEmpty()) {
                
                
                List<PricebookEntry> existingOtherPBPBERecords = new List<PricebookEntry>([SELECT Id, Product2Id, Pricebook2Id, UnitPrice, IsActive, CurrencyIsoCode, Pricebook2.Name, Product2.Market__c, Product2.Business_Unit__c, Product2.Line_of_Business__c FROM PricebookEntry WHERE product2Id IN :oldStandardPBProductIdPBEMap.keySet() AND 
                                                                                           Pricebook2Id != :pricebookNameIdMap.get(standardPricebook) and Pricebook2.Name != 'GRL Product Price Book']);
                
                List<Proposal_Pricelist_Mapping__mdt> pbList = [select Id, Order_Price_Book__c, PP_Onboarded__c, Rep_Market__c, Rep_Business_Unit__c, Rep_Line_of_Business__c, Oppty_Currency__c from Proposal_Pricelist_Mapping__mdt where PP_Onboarded__c = true ];
                
                
                for(PricebookEntry existingOtherPBPBE : existingOtherPBPBERecords) {
                    
                    if(oldStandardPBProductIdPBEMap.get(existingOtherPBPBE.Product2Id) != null) {
                        Map<Id,PricebookEntry> oldStandardPBPBEMap = oldStandardPBProductIdPBEMap.get(existingOtherPBPBE.Product2Id);
                        
                        for(PricebookEntry oldStandardPBE : oldStandardPBPBEMap.values()) {
                            
                            // Multiple PricebookEntry records can exists with Same Product and Pricebook but of different Currency
                            
                            if(oldStandardPBE.CurrencyIsoCode == existingOtherPBPBE.CurrencyIsoCode) {
                                //This logic is implemented as part of CR-2947 by Raja Gopalarao B
                                //Start CR-2947
                                if(pbList.size()>0){                        
                                    integer count=0;
                                    for(integer i=0; i<pbList.size();i++){  
                                        
                                        if(!(existingOtherPBPBE.CurrencyIsoCode == pbList[i].Oppty_Currency__c && existingOtherPBPBE.Pricebook2.Name == pbList[i].Order_Price_Book__c && existingOtherPBPBE.Product2.Market__c == pbList[i].Rep_Market__c && existingOtherPBPBE.Product2.Business_Unit__c == pbList[i].Rep_Business_Unit__c && existingOtherPBPBE.Product2.Line_of_Business__c == pbList[i].Rep_Line_of_Business__c )){                                            
                                            
                                            count++;
                                        }
                                        
                                    }
                                    
                                    if(count==pbList.size()){
                                        existingOtherPBPBE.UnitPrice = oldStandardPBE.UnitPrice;
                                        existingOtherPBPBE.IsActive = oldStandardPBE.IsActive;
                                        toBeUpsertedPBERecords.add(existingOtherPBPBE);
                                        
                                        count=0;
                                    }                                   
                                } 
                                else{
                                    existingOtherPBPBE.UnitPrice = oldStandardPBE.UnitPrice;
                                    existingOtherPBPBE.IsActive = oldStandardPBE.IsActive;
                                    toBeUpsertedPBERecords.add(existingOtherPBPBE);
                                }
                                // End CR-2947
                                
                                
                                
                            }
                        }                               
                    }                       
                }               
            }
            
            // Upsert the PricebookEntry records
            if(!toBeUpsertedPBERecords.isEmpty()) {
                
                List<Database.UpsertResult> results = Database.upsert(toBeUpsertedPBERecords, false);
                
                if(!results.isEmpty()) {
                    
                    PS_ExceptionLogger errlogger = new PS_ExceptionLogger();
                    boolean hasfailures = false;                    
                    
                    for(Database.UpsertResult result : results) {
                        
                        if(!result.isSuccess()) {
                            
                            errlogger.addLog(result, 'PS_CreatePriceBookEntriesBatch');
                            hasfailures = true;
                        }                  
                    }
                    
                    if(hasfailures) {
                        errlogger.writeLogs();
                    }                       
                }
            }
        }
        catch(Exception ex) {
            System.Debug('### Exception has occurred' + ex); 
        }
        
    }
    
    
    global void finish(Database.BatchableContext BC){
        
        
        
    }
    
    
    
    
    /* HELPERS */
    
    private void getPriceBookMappings() {
        
        priceBookMappings = new List<Proposal_Pricelist_Mapping__mdt>();
        //Modified by Raja Gopalarao B on 08-AUG-19 as part of CR-02947 Inclided the where condition PP_Onboarded__c = false
        priceBookMappings = [SELECT 
                             Id,
                             Oppty_Currency__c,
                             Price_Book__c,
                             Oppty_Record_Type__c,
                             Order_Price_Book__c,
                             Price_List_Name__c,
                             Rep_Business_Unit__c,
                             Rep_Line_of_Business__c,
                             Rep_Market__c
                             FROM 
                             Proposal_Pricelist_Mapping__mdt WHERE PP_Onboarded__c = false ];
    }
    
    
    private void getMarkets() {
        
        if(priceBookMappings == null) {
            getPriceBookMappings();
        }
        markets = new Set<String>();
        for(Proposal_Pricelist_Mapping__mdt priceBookMapping : priceBookMappings) {
            if(priceBookMapping.Rep_Market__c != null) {
                markets.add(priceBookMapping.Rep_Market__c);
            }
        }
        
    }
    
    private void setBatchRunFromDate(Date inputDate) {
        
        if(inputDate != null) {
            lastProcessedDate = inputDate;
        } else {
            // try and set to date to completetion date of last run of batch,
            //if not, use Date.today();
            lastProcessedDate = getLastProcessedDate();
            
            if(lastProcessedDate == null) {
                lastProcessedDate = Date.today();
            }
        }
        
        
        
        
    }
    
    private Date getLastProcessedDate() {
        
        List<AsyncApexJob> previousBatchRuns = [SELECT Id, ApexClass.Name, CompletedDate, CreatedDate 
                                                FROM AsyncApexJob 
                                                WHERE ApexClass.Name = 'PS_CreatePriceBookEntriesBatch' 
                                                AND Status = 'Completed' 
                                                ORDER BY CreatedDate DESC LIMIT 1];
        
        if(!previousBatchRuns.isEmpty()) {
            return previousBatchRuns[0].CompletedDate.date();
        } else {
            return null;
        }
        
    }
    
    private void getpricebookNameIdMap() {
        
        pricebookNameIdMap = new Map<String,Id>();
        
        for(Pricebook2 pb : [SELECT Id, Name, IsActive FROM Pricebook2 WHERE IsActive = true])
        {
            String pbName = pb.Name.toLowercase();
            pbName = pbName.deleteWhitespace();
            pricebookNameIdMap.put(pbName, pb.Id);
        }
    }
    
    
}