/*******************************************************************************************************************
* Apex Class Name  : PS_AmendmentOpportunityMainTest
* Version          : 1.0
* Created Date     : 24 November 2015
* Function         : Test Class for PS_AmendmentOpportunityMain 
* Modification Log :
*
* Developer                         Date                    Description
* ------------------------------------------------------------------------------------------------------------------
* indranil.debnath               26/11/2015              Test Class for PS_AmendmentOpportunityMain 
* --------------------------------------------------------------------------------------------------------------------
*******************************************************************************************************************/

@isTest
Public class PS_AmendmentOpportunityMainTest{          
    Static testmethod void myUnitTest(){ 
        Integer currentYear = system.today().year();
        User us = [select Market__c,Line_of_Business__c,Business_Unit__c from User where id =:UserInfo.getUserId() limit 1];
        date myDate = date.today();
        myDate = mydate.addyears(2);
        system.debug('myDate '+myDate );
        ID oid;
        List<opportunity> opp = Testdatafactory.createOpportunity(7,'D2L');
        List<product2> prod1 =Testdatafactory.createProduct(2);
        Product2 sProduct  = (Product2)TestClassAutomation.createSObject('Product2');
        sProduct.Market__c = us.Market__c;
        sProduct.Line_of_Business__c= us.Line_of_Business__c;
        sProduct.Business_Unit__c = us.Business_Unit__c ;
        insert sProduct;
        PriceBookEntry newPriceBookEntry = new PriceBookEntry();
        newPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();//listWithPriceBook[0].Id;
        newPriceBookEntry.UnitPrice = 10.0;
        newPriceBookEntry.Product2Id  = sProduct.Id;
        newPriceBookEntry.CurrencyIsoCode= 'GBP';
        newPriceBookEntry.IsActive = true;
        insert newPriceBookEntry;
        PS_AmendmentOpportunityMain OppMainPage = new PS_AmendmentOpportunityMain();
        //code to check lost opportunity scenario
        opp[0].stagename='lost';
        opp[0].Lost_Reason__c='Price\\Value';
        
        opp[1].stagename='Proposal/Quote';
        opp[1].Lost_Reason__c='Price\\Value';
        opp[1].PS_OppotunityIntegartionfield__c = true;
        Opp[1].External_ID__c='tester1';
        opp[1].Academic_Start_Date__c=date.newInstance(1983, 9, 15);
        
        opp[2].PS_OppotunityIntegartionfield__c = false;
        opp[2].stagename='closed';
        opp[2].Signed_Enrolement_Declaration_Recieved__c=true;
        opp[2].Lost_Reason__c='Price\\Value';
        opp[2].External_ID__c='testing1';
        
        opp[3].stagename='closed';
        opp[3].Signed_Enrolement_Declaration_Recieved__c=true;  
        
        opp[5].stagename='closed';
        opp[5].PS_OppotunityIntegartionfield__c = true ;
        opp[5].External_ID__c='';
        opp[5].External_ID__c=NULL;
        
        opp[6].stagename='closed';
        opp[6].Lost_Reason__c='Price\\Value';
        opp[6].Academic_Start_Date__c = myDate;
        date opptyyear =opp[6].Academic_Start_Date__c ;

        opp[7].stagename='Proposal/Quote';
        opp[7].Academic_Start_Date__c = myDate;
 		
        Test.startTest();
        insert opp;
        Test.stopTest();
        
        opp[1].External_Opportunity_Number__c=opp[1].id;
        opp[3].External_ID__c=opp[2].id;
        oid=opp[6].id;
        opp[7].External_Opportunity_Number__c=opp[6].id;
        
        Update opp;

        opportunity opp1 = [select id,Academic_Start_Date__c from opportunity where id=:opp[1].id];
        system.debug('Debug for Academic start date scenario:' + opp[1].Academic_Start_Date__c);
        PageReference pageRef = new PageReference('/apex/?id=' + opp[1].Id); 
        Test.setCurrentPageReference(pageRef);
        OppMainPage.openPage();
    }
    
    Static testmethod void myUnitTest1(){ 
        Integer currentYear = system.today().year();
        User us = [select Market__c,Line_of_Business__c,Business_Unit__c from User where id =:UserInfo.getUserId() limit 1];
        date myDate = date.today();
        myDate = mydate.addyears(2);
        system.debug('myDate '+myDate );
        ID oid;
        List<opportunity>opp = Testdatafactory.createOpportunity(1,'D2L');
        List<product2> prod1 =Testdatafactory.createProduct(2);
        Product2 sProduct  = (Product2)TestClassAutomation.createSObject('Product2');
        sProduct.Market__c = us.Market__c;
        sProduct.Line_of_Business__c= us.Line_of_Business__c;
        sProduct.Business_Unit__c = us.Business_Unit__c ;
        insert sProduct;
        PriceBookEntry newPriceBookEntry = new PriceBookEntry();
        newPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();//listWithPriceBook[0].Id;
        newPriceBookEntry.UnitPrice = 10.0;
        newPriceBookEntry.Product2Id  = sProduct.Id;
        newPriceBookEntry.CurrencyIsoCode= 'GBP';
        newPriceBookEntry.IsActive = true;
        insert newPriceBookEntry;
        
        opp[0].stagename='lost';
        opp[0].Lost_Reason__c='Price\\Value';
        Test.startTest();
        insert opp;
        Test.stopTest();
        PS_AmendmentOpportunityMain OppMainPage = new PS_AmendmentOpportunityMain();
        PageReference pageRef = new PageReference('/apex/?id=' + opp[0].Id); 
        Test.setCurrentPageReference(pageRef);
        OppMainPage.openPage();
    }
    
    Static testmethod void myUnitTest2(){ 
        ID oid;
        List<opportunity> opp = Testdatafactory.createOpportunity(1,'D2L');
        opp[0].stagename='Closed';
        opp[0].Lost_Reason__c='Price\\Value';
        opp[0].Academic_Start_Date__c = Date.Today();
        Test.startTest();
        insert opp;
        Test.stopTest();
        PS_AmendmentOpportunityMain OppMainPage = new PS_AmendmentOpportunityMain();
        PageReference pageRef = new PageReference('/apex/?id=' + opp[0].Id); 
        Test.setCurrentPageReference(pageRef);
        OppMainPage.openPage();
    }
}