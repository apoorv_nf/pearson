/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_FeedItem_TriggerHelper.cls
   Description:     Helper class for PS_FeedItemTrigger 
   Date             Version              Author                          Summary of Changes
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  11-Jan-2016         0.1              Sudhakar Navuluri              RD-1647-Create Integration request  record from FeedChat
  07/Mar/2016         0.2              Sudhakar Navuluri       Changed the code as per review comments
  3/30/2016           0.3              Rashmi                   Added to fetch Geography from Case and map to Integration request
    4/7/2016          0.4               KP                        Updated code to trigger feed item integration without any check on feed post content 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class PS_FeedItem_TriggerHelper
{   
    //Description:This method is used for adding cases and users based on condition
    public static void onafterinsert(List<FeedItem> feedList)
    {  
        String caseIdPrefix ='';
        Set<ID> caseId= new Set<ID>();
        Set<ID> userId = new Set<ID>();
        Map<ID,User> userMap;
        Map<ID,Case> caseMap;
        String casePrefix='500';           
        if (!feedList.isEmpty()) 
        {    
            for (FeedItem f: feedList) 
            { 
                caseIdPrefix = String.valueOf(f.ParentId).substring(0,3);   
                if(caseIdPrefix ==casePrefix)
                {
                    caseId.add(f.ParentId);
                    userId.add(f.CreatedById);
                }
            }
            
            if(!caseId.isEmpty())
            {
               Set<ID> caseRecordTypeId= new Set<ID>();             
               caseRecordTypeId.add(Schema.SObjectType.case.getRecordTypeInfosByName().get(Label.CaseHeTechSupportRecordType).getRecordTypeId());
               caseRecordTypeId.add(Schema.SObjectType.case.getRecordTypeInfosByName().get(Label.State_National).getRecordTypeId());
               caseRecordTypeId.add(Schema.SObjectType.case.getRecordTypeInfosByName().get(Label.CaseCSRecordType).getRecordTypeId());		
               userMap = new Map<ID,User>([SELECT ID, Name FROM User WHERE ID IN :userId]);
               caseMap = new Map<ID,Case>([SELECT ID,Escalation_External_ID__c,Geography__c FROM Case WHERE ID IN :caseId and RecordTypeID IN :caseRecordTypeId and Escalation_External_ID__c != null and Escalation_External_ID__c != 'Pending']);
            }
            
            if(userMap!=null && caseMap!=null)
            {
               //to filter out feed item for the desired cases 
               List<FeedItem> updatedFeedItem= new List<FeedItem>();
               Map<Id,Case> feedtoCaseMap = new Map<Id,Case>();
               for (FeedItem f: feedList) {
                 //  if (caseMap.containsKey(f.ParentId) && userMap.get(f.CreatedById).Name !=Label.PS_ServiceNow_Integration_User && 
                   // f.type =='TextPost' && f.body.contains(Label.PS_ServiceNow_Integration_Body)){
                   if (caseMap.containsKey(f.ParentId) && userMap.get(f.CreatedById).Name !=Label.PS_ServiceNow_Integration_User && 
                   (f.type =='TextPost' || f.type == 'ContentPost')){ 
                   
                        updatedFeedItem.add(f);
                        feedtoCaseMap.put(f.id,caseMap.get(f.ParentId));
                   }
               }
               if(!updatedFeedItem.isEmpty())
               {
                   PS_INT_IntegrationRequestController.createIntegrationRequestFeedItem(updatedFeedItem,feedtoCaseMap);
               }   
            }                                                                               
        }               
    }          
}