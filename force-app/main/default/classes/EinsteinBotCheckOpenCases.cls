/*
 * Author: Apoorv Saxena (Neuraflash)
 * Date: 06/08/2019
 * Description: Checks if last messaging session has an open case assoicated - Developed for SMS Bot.
 * Update: Bypassing trigger to avoid Read time out issues as a temporary workaround, 
 *         until the Pearson dev team optimizes trigger logic - Apoorv(Dec 11, 2019).
 */
public with sharing class EinsteinBotCheckOpenCases {

    @InvocableMethod(label='Einstein SMS Bot - Check Open Cases')
    public static List<Boolean> checkOpenCases(List<String> routableId){
        try{
            String msgSessionId = routableId[0];

            if(!String.isBlank(msgSessionId)){
                MessagingSession msgSession = [SELECT Id, MessagingEndUserId, CaseId 
                                                FROM MessagingSession 
                                                    WHERE Id =: msgSessionId];
                Id msgUserId = msgSession.MessagingEndUserId;

                for(MessagingEndUser msgUser: [SELECT Id, 
                                                   (SELECT Id, CaseId, Case.Status, Case.Re_engage__c  
                                                    FROM MessagingSessions
                                                    WHERE Id !=: msgSessionId
                                                    ORDER BY CreatedDate 
                                                    DESC LIMIT 1)
                                                        FROM MessagingEndUser 
                                                            WHERE Id =: msgUserId]){
                    if(msgUser.MessagingSessions[0].CaseId != NULL){
                        String prevCaseStatus = msgUser.MessagingSessions[0].Case.Status;
                        if(!String.isBlank(prevCaseStatus) && prevCaseStatus.toLowerCase() != 'closed'){
                            msgSession.CaseId = msgUser.MessagingSessions[0].CaseId;
                            update msgSession;
                            if(!msgUser.MessagingSessions[0].Case.Re_engage__c){
                                // Bypassing trigger to avoid Read time out issues as a temporary workaround until the trigger logic is optimized
                                PS_SMSBot_CaseTriggerHandler.bypassTrigger = true;
                                update new Case(Id = msgUser.MessagingSessions[0].CaseId, Re_engage__c = TRUE);
                            }
                            return new List<Boolean>{TRUE};
                        }
                    }   
                }
                return new List<Boolean>{FALSE};
            }
        } catch(Exception ex){}
        return new List<Boolean>{FALSE};
    }
}