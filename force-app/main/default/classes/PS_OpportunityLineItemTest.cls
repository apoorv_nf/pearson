/*******************************************************************************************************************
* Trigger Name  : OpportunityLineItem
* Version          : 1.0 
* Created Date     : 11 May 2015
* Function         : Test Class of the OpportunityLineItem
* Modification Log : 
*
* Developer                                Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*   Accenture IDC                          11/05/2015              Created Initial Version
*  Leonard Victor                           27/8/2015             Code Stream Line update R3
*  Mani                                    07/25/2018             Replaced Qualification__c field by QuoteMigration_Qualification__c as part of ZA Apttus Decomm
*******************************************************************************************************************/

@isTest        
public class PS_OpportunityLineItemTest{
 static testMethod void myTest() { 
 

 Account acc = new Account();
     acc.Name = 'Test';
     acc.Line_of_Business__c= 'Higher Ed';
     acc.Geography__c = 'North America';
     acc.Market2__c = 'US';
     insert acc;
     
  Test.startTest();  
  
 // List<Opportunity> opp= TestDataFactory.createOpportunity(3,'D2L');   
 // Insert opp;   
 
 
  Opportunity opp = new opportunity();
      opp.AccountId = acc.id;    
      opp.Name = 'OppTest';
      opp.StageName = 'Qualification';
      opp.Type = 'New Business';
      opp.Enrolment_Status__c = 'Enrolled';
      opp.CloseDate = system.today();
      opp.International_Student_Status__c = 'Yes';
      opp.QuoteMigration_Qualification__c = 'Psychology';
      opp.CurrencyIsoCode = 'AUD';
      opp.Market__c = 'US';
      //opp.recordtypeid = '012b0000000DcVk';
      insert opp; 
      
      Opportunity opp1 = [Select id,CurrencyIsoCode from Opportunity where Id =:opp.id];  //'00611000008x01cAAA'
      
  List<Product2> productList = new List<Product2>();
        List<PriceBookEntry> pbe = new List<PriceBookEntry>();
        List<OpportunityLineItem> oli = new List<OpportunityLineItem>();      
       
        //Creating Test Products
        Product2 prod = new Product2();        
        prod.Name = 'ENTERPRISE DESKTOP';
        prod.IsActive = true;
        prod.CurrencyIsoCode = 'AUD' ;//opp1.CurrencyIsoCode;    
        system.debug('prodcurrencycode'+ prod.CurrencyIsoCode);    
        productList.add(prod);            
       
       //Checking for List size
       if(productList.size() > 0)
       {
        insert productList;     
       }
       
            
       //Creating test Price Book
      // PriceBook2 pb = [select id from Pricebook2 where IsStandard = true limit 1];  
       
       //Creating Price Book entries
       for(Product2 prd : productList)
       {
        pbe.add(new PriceBookEntry(PriceBook2Id= Test.getStandardPricebookId(), product2Id= prd.Id, CurrencyISOCode = 'AUD' ,UnitPrice=0, isActive=true,useStandardPrice=false));       
       system.debug('^^^^^^^^' +prd.CurrencyISOCode);
       
       }
       //Checking for List size
       if(pbe.size() > 0){
        insert pbe;        
       }       
       
       for(PriceBookEntry entries : pbe)
       {
        oli.add(new OpportunityLineItem(OpportunityId= opp.id,PriceBookEntryId= entries.Id, UnitPrice=5, Quantity=1));
        system.debug('AFTER FOR' +OpportunityLineItem.PriceBookEntryId);
        
       }   
       
       Bypass_Settings__c bypass = Bypass_Settings__c.getInstance();
       bypass.Disable_Triggers__c  = true;
       
       Database.UpsertResult result = Database.upsert(bypass);  
       system.debug('result ------>'+result );
       
       //Checking for List size
       if(oli.size() > 0)
       {
        insert oli; 
        
        Update oli;
        
        delete oli;
        
        //undelete oli;
        
       }     
       
                       

      
Test.stopTest();
 
 }
}