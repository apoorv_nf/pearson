/************************************************************************************************************
* Apex Interface Name : PS_CSSContactPrint_Email
* Version             : 1.1 
* Created Date        : 05/02/2016
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Neha Karpe            05/02/2016             
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_CSSContactPrint_EmailTest {
   
    /*Method to test Article insert*/
    static testMethod void testCurrentUser(){
        
        String communityUserProfileName = 'Pearson Self-Service User';
        List<User> listWithUser = new List<User>();
        List<Customer_Support__kav> kavLst = new List<Customer_Support__kav>();
         
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: communityUserProfileName].Id,1);
        
        Test.StartTest();
        listWithUser[0] = PS_CSSContactPrint_Email.getCurrentUser();
        
        
        Test.StopTest();
        
    }
    /*Method to test outbound notification*/
    static testMethod void testOutNotif(){
        String communityUserProfileName = 'Pearson Self-Service User';
        List<User> listWithUser = new List<User>();
        listWithUser = TestDataFactory.createUser([select Id from Profile where Name =: communityUserProfileName].Id,1);
        String yourEmail =listWithUser[0].email;
        String url='https://www.google.com';
        String articleName='ArticleName';
        String recEmail='h@hotmail.com';
        String firstName=listWithUser[0].name;
        String recName='recName';
        /*List<Outbound_Notification__c> sendEmailList = new List<Outbound_Notification__c>();
        sendEmailList = PS_CSSContactPrint_EmailTest.createOutNot();
        insert sendEmailList;*/
        String str;
        
        Test.StartTest();
        PS_CSSContactPrint_Email.articleSendEmail(yourEmail,url,articleName,recEmail,firstName,recName);
        system.assertequals(firstName,listWithUser[0].name);
        
        Test.StopTest();
    }
      
     
  /*Method to test outbound notification*/
    static testMethod void negative_testOutNotif(){
        String communityUserProfileName = 'Pearson Self-Service User';
        List<User> listWithUser = new List<User>();
        listWithUser = TestDataFactory.createUser([select Id from Profile where Name =: communityUserProfileName].Id,1);
        String yourEmail =listWithUser[0].email;
        String url='https://www.google.com';
        String articleName='ArticleName';
        String recEmail='hotmail';
        String firstName=listWithUser[0].name;
        String recName='recName';
        /*List<Outbound_Notification__c> sendEmailList = new List<Outbound_Notification__c>();
        sendEmailList = PS_CSSContactPrint_EmailTest.createOutNot();
        insert sendEmailList;*/
        String str;
        
        Test.StartTest();
        try{
        PS_CSSContactPrint_Email.articleSendEmail(yourEmail,url,articleName,recEmail,firstName,recName);
            }
            catch(Exception e){
            system.debug('case exception'+e.getMessage());
           
         Boolean expectedExceptionThrown =  e.getMessage().contains('Insert failed. First exception on row 0; first error: ') ? true : false;
          System.AssertEquals(expectedExceptionThrown, true);
          
     
           System.assertEquals('INVALID_EMAIL_ADDRESS' ,  e.getDmlStatusCode(0));
            
            }   
        
        
        Test.StopTest();
    }
      
     
          
        
    }