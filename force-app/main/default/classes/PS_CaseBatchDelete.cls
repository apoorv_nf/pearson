/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_CaseBatchDelete .cls 
   Description:     This class contains logic to delete all cases with Record Type as Customer Support
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  14/12/2015         1.0            Sakshi Agarwal                        Initial Release 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */


global class PS_CaseBatchDelete implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        string caseRecordTypeId = string.escapeSingleQuotes(System.label.CustomerSupportRecordTypeId);
        String query = 'SELECT ID,OwnerId FROM Case where Case.RecordType.Name ='+'\''+String.escapeSingleQuotes(caseRecordTypeId)+'\'';
        //String query = 'SELECT Id,Name,OwnerId FROM Case where Case.OwnerId =: String.escapeSingleQuotes(caseOwnerID)';
        System.debug('query:'+query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Case> casesToBeDeleted) {
         List<Case> caseToDelete = new List<Case>();
         for(Case obj : casesToBeDeleted)
         {
            caseToDelete.add(obj);          
         }
         if(caseToDelete.size() > 0){
            delete caseToDelete;
         }
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}