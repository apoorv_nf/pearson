global class EmptyRecycleBinBatch implements Database.Batchable<sObject>, Schedulable {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String Query='Select Id, ObjectAPIName__c From EmptyRecycleBinBatch__c';
        return database.getquerylocator(Query);
   }

    global void execute(Database.BatchableContext BC,List<EmptyRecycleBinBatch__c> sObjRecords) {
        User userid= [SELECT Id,Name FROM User WHERE Name = 'One CRM Data Admin' AND IsActive = true LIMIT 1];
        String Query;
        
        for(EmptyRecycleBinBatch__c sOb: sObjRecords){ 
        
       
        Query = 'SELECT ID from '+sOb.ObjectAPIName__c+' where isDeleted = true AND CreatedById = \''+userid.Id+'\' all rows';
      
        Integer recordlimit = 10;      
        List<Sobject> sObjtocreate = new List<Sobject>();  
        if(Test.isRunningTest()){  
             Query = 'SELECT ID from '+sOb.ObjectAPIName__c+' where isDeleted = true AND CreatedById = \''+userid.Id+'\' LIMIT ' +
                 recordlimit + 'all rows';
             // sObjtocreate = Database.query(Query + ' LIMIT ' + recordlimit);
        }
              sObjtocreate = Database.query(Query); 
              
      if(sObjtocreate != null && !sObjtocreate.isEmpty()) { 
          
     try{
           Database.emptyRecycleBin(sObjtocreate); 
          /*  if(test.isRunningTest())
            {
                 throw new applicationException('Exception');
            } */
        }
        catch(Exception e){
        }
       }
      }
     }     
    public class applicationException extends Exception 
    {
    
    } 
    global void finish(Database.BatchableContext BC) {
        
    }
    global void execute(SchedulableContext sc) {
        database.executebatch(new EmptyRecycleBinBatch());
    }
}