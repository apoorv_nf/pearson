@istest
private class CancelGlobalRevenueOrderControllertest{
    public static testMethod void testMyController() {
     Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        Order newOrder = new Order();
        Order oldOrder = new Order();
        List<Opportunity> NewOpp = new list<Opportunity>();
        list<Account> Acc = new list<account>();
        //Apttus_Proposal__Proposal__c Proposal = new Apttus_Proposal__Proposal__c(); // Commented for CR-2949 Apttus(Changes) by Rajesh Paleti
        insert lstWithTestUser;
        Bypass_Settings__c settings = new Bypass_Settings__c();
        settings.Disable_Triggers__c = true;
        settings.SetupOwnerId = lstWithTestUser[0].id;
        insert settings;  
      
        System.runAs(lstWithTestUser[0]) 
        {
            newOrder = TestDataFactory.returnorder();
      
            NewOpp =TestDataFactory.createOpportunity(1,'Global Opportunity');
            Acc=TestDataFactory.createAccount(1,'Organisation');
            insert Acc;
            NewOpp[0].AccountId = Acc[0].id;
            NewOpp[0].Channel__c ='Direct';
            insert NewOpp[0];
            system.debug('@@@ NewOpp[0].Channel__c'+NewOpp[0].Channel__c);
       
       
           // Proposal = TestDataFactory.createProposal(NewOpp[0],Acc[0],'Global Quote/Proposal');
           // system.debug('@@@ NewOpp[0].Channel__c'+NewOpp[0].Channel__c+ '@@propo.channel'+proposal.Quote_Channel__c);
            //Proposal.Quote_Channel__c ='Direct';
            //insert proposal;
            
        
         
            if(newOrder != null)
            {
                PageReference pageRef = Page.GlobalRevenueCancelOrder;
                Test.setCurrentPage(pageRef);
                ApexPages.currentPage().getParameters().put('orderId',newOrder.id);
               // ApexPages.currentPage().getParameters().put('propId',proposal.id);
                ApexPages.StandardController sc = new ApexPages.StandardController(newOrder);
                CancelGlobalRevenueOrderController controllerObj = new CancelGlobalRevenueOrderController(sc);
                controllerObj.mConfirm();
                controllerObj.mCancelOrder();
                controllerObj.closePopup();
                 newOrder.status='open';
                 update neworder;
                PageReference pageRef1 = Page.GlobalRevenueCancelOrder;
                Test.setCurrentPage(pageRef1);
                ApexPages.currentPage().getParameters().put('orderId',neworder.id);
               //ApexPages.currentPage().getParameters().put('propId',proposal.id);
                ApexPages.StandardController sc1 = new ApexPages.StandardController(newOrder);
                CancelGlobalRevenueOrderController controllerObj1 = new CancelGlobalRevenueOrderController(sc1);
                controllerObj1.mConfirm();
                controllerObj1.mCancelOrder();
                controllerObj1.closePopup(); 
               
            }
        }
    
    }
    
   
}