public class Community_Notifications {
    
    @AuraEnabled
    public static List<Community_Notification__c> activeNotifications() {
        String commHeaderURL = '';
        String communityId=Network.getNetworkId();
        if(!Test.isRunningTest()) {
            System.debug('@@communityId: ' +communityId);
            System.debug('@@getSelfRegUrl: '+Network.getSelfRegUrl(communityId));
            commHeaderURL = Network.getSelfRegUrl(communityId);
        }
        else {
            commHeaderURL = '';
        }
        
        if(commHeaderURL.contains('usclinical')) {
            return [Select Id, Name, Message__c, Start_Date_Time__c, Stop_Date_Time__c, Entry_No__c FROM Community_Notification__c 
                    WHERE Active__c = true and Community_Header__c = 'usclinical'];
        } else {
            return [Select Id, Name, Message__c, Start_Date_Time__c, Stop_Date_Time__c, Entry_No__c  
                    FROM Community_Notification__c WHERE Active__c = true and Community_Header__c = 'getsupport'];
        }
    } 
    
    @AuraEnabled
    public static List<Community_Content_Detail__c> activeNotification(String communityType, String communityContentType) {
        List<Community_Content_Detail__c> details = new List<Community_Content_Detail__c>();
        details= [Select Id, Name, Message__c, Start_Date_Time__c, Stop_Date_Time__c from Community_Content_Detail__c 
                  where Community_Content__r.Community_Type__c = :communityType
                  and Community_Content__r.Community_Content_Type__c = :communityContentType
                  and Visible_in_Community__c = true and Active__c = true];
        return details;
    }
    
    @AuraEnabled
    public static List<Community_Notification__c> findAll() {
        return [SELECT Id, Name, Message__c, Start_Date_Time__c, Stop_Date_Time__c, Entry_No__c FROM Community_Notification__c];
    }
}