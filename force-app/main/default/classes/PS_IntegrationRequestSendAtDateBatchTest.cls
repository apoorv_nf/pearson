/************************************************************************************************************
* Apex Class Name   : PS_IntegrationRequestSendAtDateBatchTest.cls
* Version           : 1.0 
* Created Date      : 17 Feb 2016
* Function          : test class for PS_IntegrationRequestSendAtDateBatch 
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Sahir Ali                 02/17/2016              Created Test Class for PS_IntegrationRequestSendAtDateBatch
*
************************************************************************************************************/

@isTest
public class PS_IntegrationRequestSendAtDateBatchTest {
    
    public static testMethod void testBatchJob(){
        
        Profile ProfileNameadmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        List<User> u1 = TestDataFactory.createUser(ProfileNameadmin.Id,1);
        
        
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu1'; 
            u1[0].Email ='Opptyproposaltest11@testorg.com'; 
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='Opptyproposaltest12'; 
            u1[0].LanguageLocaleKey ='en_US'; 
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = ProfileNameadmin.Id; 
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName ='standarduserQuoteCreation1@testorg.com';
            u1[0].Market__c ='UK';
            u1[0].Business_Unit__c = 'Higher Ed';
            u1[0].line_of_Business__c = 'Higher Ed';
            insert u1;
        }
        
        User us = [Select id from User where id ='0050N000007kH8o' limit 1];
        Id spb = Test.getStandardPricebookId();
        System.runAs(us)
        {
            List<Account> acc = TestDataFactory.createAccount(1,'School');
            if(acc  != null && !acc .isEmpty()){
                acc[0].Name = 'OpptyProposalAccTest1'; 
                acc[0].Line_of_Business__c='Schools'; 
                acc[0].CurrencyIsoCode ='GBP'; 
                acc[0].Geography__c = 'Growth'; 
                acc[0].Organisation_Type__c = 'Higher Education'; 
                acc[0].Type = 'School'; 
                acc[0].Phone = '9989887687';
                acc[0].ShippingStreet = 'TestStreet';
                acc[0].ShippingCity = 'Vns';
                acc[0].ShippingState = 'Delhi';
                acc[0].ShippingPostalCode = '234543';
                acc[0].ShippingCountry = 'India';
                acc[0].Market2__c ='UK';
                acc[0].IsCreatedFromLead__c = True;
               insert acc;
            }
            
           List <Contact> contactRecord = TestDataFactory.createContact(1);
           if(contactRecord != null && !contactRecord.isEmpty()){
               contactRecord[0].FirstName='TestopptypropFirstname';
               contactRecord[0].LastName='TestopptypropLastname';
               contactRecord[0].AccountId=Acc[0].Id ;
               contactRecord[0].Salutation='MR.';
               contactRecord[0].Email='Testopptypropemail@email.com'; 
               contactRecord[0].Phone='1112223334';
               contactRecord[0].Preferred_Address__c='Mailing Address';
               contactRecord[0].MailingCity = 'Newcastle';
               contactRecord[0].MailingState='Northumberland';
               contactRecord[0].MailingCountry='United Kingdom';
               contactRecord[0].MailingStreet='1st Street' ;
               contactRecord[0].MailingPostalCode='NE28 7BJ';
               contactRecord[0].National_Identity_Number__c = '9412055708083' ;
               contactRecord[0].Passport_Number__c = 'ABCDE12345';
               contactRecord[0].Home_Phone__c = '9999';
               contactRecord[0].Birthdate__c = Date.newInstance(1900 , 10,10);
               contactRecord[0].Ethnic_Origin__c = 'Asian' ;
               contactRecord[0].Marital_Status__c = 'Single';
               contactRecord[0].First_Language__c = 'English'; 
               insert contactRecord;
               
           }
            
           List<Generate_Potential_Target__c> pt1 = TestDataFactory.createPotentialTarget(1,'B2B2L');
           if(pt1 != null && !pt1.isEmpty()){
              pt1[0].Account__c = acc[0].Id;
              pt1[0].Opportunity_Type__c = 'Existing Business';
              pt1[0].Status__c = 'In Progress';
              pt1[0].Generate_Proposal__c = True;
              insert pt1;
           }
           List<Opportunity> op = TestDataFactory.createOpportunity(1,'Global Opportunity');
           if( op != null && !op.isEmpty()){
                op[0].Name= 'OpQuote create Test1'; 
                op[0].AccountId = acc[0].id; 
                op[0].StageName = 'Awarded'; 
                op[0].Type = 'New Business'; 
                op[0].Academic_Vetting_Status__c = 'Un-Vetted'; 
                op[0].Academic_Start_Date__c = System.Today();
                op[0].CloseDate = System.Today();
                op[0].Business_unit__c = 'Higher Ed';
                op[0].Primary_Contact__c = contactRecord[0].Id;
                op[0].pricebook2Id = spb;
                op[0].Channel__c = 'Direct';
                op[0].Mass_Opportunity_Generation_Indicator__c = true;
                op[0].Related_Potential_Target__c =pt1[0].Id;
                insert op;
           } 
        
          List< Integration_Request__c>lstint = new list<Integration_Request__c>();
          Integration_Request__c iRrec =  new Integration_Request__c(Object_Id__c = op[0].Id, Object_Name__c = 'Opportunity', 
                    Direction__c ='Outbound', event__c='Enrol Student', sub_event__c = 'Change Module Choice', status__c = 'Send At DateTime', Batch_Job_Id__c = op[0].Id);
        lstint.add(iRrec);
           insert lstint;
           
            
           test.startTest();
            PS_IntegrationRequestSendAtDateBatch intBatchJob = new PS_IntegrationRequestSendAtDateBatch(op[0].Id);
            intBatchJob.execute(null,lstint);
            Id batchProcessId = database.executebatch(intBatchJob);
           test.stopTest();
        
        
        }
        
    }    

}