/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 12/05/2018
 */
@isTest
private class TestEinsteinBotCaseCreation {

	@testSetup
	private static void setup() {
		SMS_API_Settings__c settings = new SMS_API_Settings__c();
		settings.API_Key__c = 'abcdef';
		settings.Username__c = 'username123';
		settings.Password__c = 'password123';
		settings.SSO_Token__c = 'token1234';

		insert settings;
	}

    @isTest
    static void it_should_create_a_case() {
        User automatedProcess = [SELECT Id FROM User WHERE Name = 'Automated Process' LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new SmsApiMock());

        String chatKey = 'asdfasdf';

        Case cas = new Case();
        insert cas;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

		LiveChatTranscript transcript = new LiveChatTranscript(
			CaseId = cas.Id,
			LiveChatVisitorId = vistor.Id,
            LiveChatButtonId = [SELECT Id FROM LiveChatButton WHERE DeveloperName = 'NAUS_HETS_Chatbot_ACR'].Id,
			ChatKey = chatKey);
		insert transcript;

        EinsteinBotCaseCreation.CaseCreationRequest request = new EinsteinBotCaseCreation.CaseCreationRequest();
        request.email = 'name@email.com';
        request.institution = 'Pearson';
        request.liveAgentSessionId = chatKey;
        request.platform = 'Moodle';
        request.accessCode = 'abc';
        request.courseName = 'Course Name';
        request.errorMessage = 'Nothing';
        request.category = 'Access Code';
        request.subcategory = 'Error - Expired';
        request.message = 'Code was not manually expired by Pearson - Extension recommend';

        List<Case> caseList = new List<Case>();

        Test.startTest();
        System.runAs(automatedProcess) {
            EinsteinBotCaseCreation.createCase(new List<EinsteinBotCaseCreation.CaseCreationRequest>{request});
            Test.getEventBus().deliver();
        }
        Test.stopTest();

        System.assertEquals(1, [SELECT Count() FROM Case]);
        System.assertEquals(1, [SELECT Count() FROM FeedItem]);
    }
}