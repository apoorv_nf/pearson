@isTest
public class POC_CreateOnboardingUser_Test {
Public static testmethod void UserOnboarding(){
           User u1 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = UserInfo.getProfileId(), 
                              Sample_Approval__c= true,Order_Approver__c= UserInfo.getUserId(), 
                               TimeZoneSidKey='America/Los_Angeles', UserName='test1234'+Math.random()+'@pearson.com.testbau', Geography__c = 'Growth',
                               Market__c = 'US',Line_of_Business__c = 'Higher Ed',License_Pools__c = 'ANZ Sales');
            insert u1;  
    
    			User U = new user();
    			u.Alias = 'standt'; 
                u.Email='standarduser11@pearson.com'; 
                u.EmailEncodingKey='UTF-8'; 
                u.LastName='Testing'; 
                u.LanguageLocaleKey='en_US'; 
                u.LocaleSidKey='en_US';
                u.ProfileId = [SELECT id from profile where name = 'Pearson Service Cloud User OneCRM'].id; 
    			u.UserRoleId = [select id from userrole where name = 'ANZ Service Agent'].id;
                u.Sample_Approval__c= true;
    			//u.Order_Approver__c= UserInfo.getUserId(); 
                u.TimeZoneSidKey='America/Los_Angeles'; 
                u.UserName='test12345'+Math.random()+'@pearson.com.testbau'; 
                u.Geography__c = 'Growth';
                u.Market__c = 'US';
                u.Line_of_Business__c = 'Higher Ed';
                u.License_Pools__c = 'ANZ Sales';
                 insert u;
    			
    			POC_PermissionSetAssign_Test.UserPermissionSet(u.id);
    
    			Test.starttest();
    			       
    			License_Management__c lm = new License_Management__c();
    			lm.Name = 'Pearson ANZ';
    			lm.Market__c = 'ANZ';
    			lm.License_Pool__c = 'ANZ Sales';
    			insert lm;
    
    			User_License_Tracker__c ULT = new User_License_Tracker__c();
                                ULT.Exception__c = FALSE;
                                ULT.License_Pool__c = 'US HE Sales';
                                ULT.License_Type__c = 'Salesforce';
                                ULT.Market__c = 'UK';
    							ULT.License_Manage__c = lm.id;
                                //ULT.User__c = Usr.id;
                 				Insert ULT;
    
             system.runas(u1) {
                   
                 
                 				
                 	
                    List <id> UserOnboard = new List <id>();
					User_Onboarding_Request__c Usr = New User_Onboarding_Request__c();
                   			 Usr.Active__c = TRUE;
                        	 //Usr.Address__c = 
                             Usr.Alias__c = 'ttest';
                             Usr.Business_Unit__c = 'CTIPIHE';
                             Usr.Business_Vertical__c = 'Higher Education';
                             Usr.Bypass_Product_Lookup_Filter_On_Asset__c = FALSE;
                             Usr.Charge_Code__c = 'tes';
                             Usr.Comments__c = 'test purpose';
                             Usr.Company_Name__c = 'Test company';
                             Usr.CurrencyIsoCode = 'ZAR';
                             Usr.Currency__c = 'ZAR';
                             Usr.Department__c = 'Testing';
                             Usr.Division__c = 'Testing';
                             Usr.Email_Encoding__c = 'ISO-8859-1';
                             Usr.Email__c = 'test@test.com';
                             Usr.Exception__c = 'Test purpose';
                             Usr.Exempt_from_Deactivation_Protocol__c = FALSE;
                             //Usr.External_Rep__c
                             Usr.Federation_ID__c = 'test@test.com';
                             Usr.First_Name__c = 'test';
                             Usr.Flow_User__c = FALSE;
                             Usr.Function__c = 'TBC';
                             Usr.Geography__c = 'Core';
                              Usr.Group__c = 'Higher Education';
                              //Usr.Incident_Number__c = '';
                              //Usr.IsDeleted 
                              Usr.Job_Function__c = 'Business Systems';
                              Usr.Knowledge_User__c = FALSE;
                              Usr.Language__c = 'en_ZA';
                              ///Usr.License_Pool__c = Formula field
                              Usr.Line_of_Business__c = 'Higher Ed';
                              Usr.Live_Agent_User__c = TRUE;  
                              Usr.Locale__c = 'en_ZA'; 
                              //Usr.Manager__c =
                              Usr.Marketing_User__c = TRUE;
                              //Usr.Market__c = 'UK';
                              Usr.Mirror_User__c = u.Id;
                              Usr.Name = 'test test';
                              Usr.Offline_User__c = FALSE;
                              Usr.Onboarding_User__c = [Select id from user where name = 'Tamal Chakraborty'].id;
                              Usr.Physical_Address__c = 'test';
                              Usr.Physical_City__c = 'test';
                              Usr.Physical_State__c = 'test';
                                  Usr.Physical_Zip__c = '12345';
                                  Usr.Price_List__c = 'Education';
                                  Usr.Product_Business_Unit__c = 'CTIPIHE';
                                  //Usr.Profile_Name__c
                                  //Usr.Profile__c = [Select id from profile where name = 'System Administrator'].id;
                                  Usr.Quota_Carrying__c = FALSE;
                 				  Usr.RecordTypeId = [Select id from recordtype where name = 'Manual User Onboarding'].id;					
                                  Usr.Region__c = 'Midran';
                                  //Usr.Remaining_License__c Formula field
                                  Usr.Role__c = 'ANZ ELT';
                                  Usr.Sales_Force__c = 'Humanities & Social Sciences - 45';
                                  Usr.SAP_Account__c = 'TEST';
                                  Usr.Service_Cloud_User__c = TRUE;
                                  Usr.Specialty__c = 'ED';
                                  Usr.Special_Price_RM_Approval_Group__c = 'Approval – AU – Schools – Inside Sales';
                                  Usr.Status__c = 'New';
                                  Usr.Support_Tier__c = 'Tier1';
                                  Usr.Time_Zone__c = 'Pacific/Kiritimati';
                                  Usr.Title__c = 'Supervisor';
                                  Usr.Username__c = 'test@test.com.ams';
                                 Usr.User_Deactivation_Id__c = '';
                                 Usr.User_Deactivation_Input__c = 'User Id';
                                 Usr.Nickname__c = 'test.test';
                                 Usr.User_License_Tracker__c = ULT.id;
                                 //Usr.Deactivation_User_Id__c
                                 //Usr.User_License__c
                                
                 				 Database.Insert (Usr,false);
                 				UserOnboard.add(Usr.id);	
                 
                 				
                 				POC_CreateOnboardingUser_New.createUser(UserOnboard);
                 
                                
                      
              Test.stopTest();   					
			}
    							
}
}