/*************************************************************************************************************************************************************************

Created On: 06/08/2017
Purpose: Trigger Handler class from System_Response__c objecttrigger
Author: Jaydip B



***************************************************************************************************************************************************************************/

public class systemResponseTriggerHandler {

public void onAfterUpdate(LIST<System_Response__c> sysResponseListNew) {
    
    updateIRBasedonSR.SetStatusBasedOnSystemResponseSA(sysResponseListNew);


    }

public void onAfterInsert(LIST<System_Response__c> sysResponseListNew) {
    
    updateIRBasedonSR.SetStatusBasedOnSystemResponseSA(sysResponseListNew);


    }

}