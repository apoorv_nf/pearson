/*********************************************************************************************************************
Modified by: CTS
Modified On: 8/16/2016
Modificatoin Reason: DR-0921 - Handling the exception with user defined error message"Learner does not have a ledger entry for the relevant database."
Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 22) 


Modified by: Jaydip B
Modified On:02/07/2017
Purpose: Removed reference to certificate name hard coded in the class - added call to custom label instead


***********************************************************************************************************************/
public class PaymentInfoRetrival{

    public List<Paymentinfo> listWithDetail {get; set;}

    public String institution {get; set;}
    private String customerId;

     public PaymentInfoRetrival()
     {
         customerId = ApexPages.currentPage().getParameters().get('customerid');
     }

     public List<SelectOption> getInstitutions() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('CTI','CTI'));
            options.add(new SelectOption('PIHE','PIHE'));
         
            return options;
     }
     //added try-catch block for DR-0921 requirement
     public PageReference search() {
         try{
            listWithDetail = PaymentInfoProxyClass.getPaymentInfo(customerId, institution);
         }
         catch(Exception e)
       {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Learner does not have a ledger entry for the relevant database.'));
         }
         System.debug('listWithDetail -->'+listWithDetail);            
          return null;
     }
     
      public void retreiveinfo()
  {
  
        
             RetrievePaymentInfo  inforetrieve = new RetrievePaymentInfo ();
             RetrievePaymentInfo.Header_element headerelement =  new  RetrievePaymentInfo.Header_element();
             RetrievePaymentInfo.RetrievePaymentInfoRequest  request = new RetrievePaymentInfo.RetrievePaymentInfoRequest ();
             list< RetrievePaymentInfo.PaymentInfoItem>  paylist = new list< RetrievePaymentInfo.PaymentInfoItem>();
             RetrievePaymentInfo.RetrievePaymentInfoSOAP   response = new RetrievePaymentInfo.RetrievePaymentInfoSOAP ();
            
             headerelement.BusinessGeography='Growth';
             headerelement.Market='US';
             headerelement.AccountType='College'; 
             headerelement.OrganisationName='Pearson';  
          
             request.header = headerelement;    
             request.AccountNo = 'AccountNumber';
              //Jaydip B changed on 2/7/2017 to remove hard code certificate name and use custom label instead
             response.clientCertName_x  =Label.PS_INT_RetreivePayment_CN;//'SelfSignedCert_27Feb2015_033452';
            // response.clientCertPasswd_x = 'SelfSignedCert_27Feb2015_033452';
             response.timeout_x = 120000;
            
             if(!Test.isRunningTest()){ 
                 paylist = response.RetrievePaymentInfo(request.header,request.AccountNo );
             }
             system.debug('$$$$$$$$$$$$Response'+paylist);
         
  }
  

     /*public void setInstitution(String institution) {
            this.selectedInstitution = institution;
     }*/

 
}