/* -------------------------------------------------------------------------------------------------------------------
   Name:            LeadConversionChecker.cls 
   Description:     Used to determine if an account is being created from a lead conversion
   Date             Version         Author               Summary of Changes 
   -----------      ----------      -----------------    -------------------------------------------------------------
   01/12/2015       1.0             Ron Matthews         Initial Release  Of Class
------------------------------------------------------------------------------------------------------------------- */

public with sharing class LeadConversionChecker 
{
  public static Boolean inLeadConversion = false; // static variable which can be used to know if this is a conversion
 
  public static List<Account> setLeadConversionStatus(List<Account> accounts) 
  {     
    if(inLeadConversion == null || inLeadConversion == false)
    {
        if(accounts.size() > 0)
        {
            if(accounts[0].isBeingConvertedTo__c != null && (accounts[0].isBeingConvertedToText__c !=null || accounts[0].isBeingConvertedToText__c != ''))
            {
              inLeadConversion = accounts[0].isBeingConvertedTo__c || accounts[0].isBeingConvertedToText__c == 'True';
            }
        }
    }
    
    // remove the conversion flag to prevent recursion.
    // static var 'inLeadConversion' of this class can be used for further checks throughout the execution
    if(inLeadConversion) 
    {
      for(Account account : accounts) 
      {
        account.isBeingConvertedTo__c = false;
      }
    }

    return accounts;
  }
}