/************************************************************************************************************
* Apex Interface Name : PS_UpdateCaseOwnerTrackingTest
* Version             : 1.1 
* Created Date        : 2/2/2016 2:44
* Description          : Test class created for class PS_UpdateCaseOwnerTracking
* Expected Inputs      : cases having case owner history records and having agent name.
* Expected Outputs     : Case having agent name should get stamp
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Hasi Chakravarty         2/2/2016
* Kameswari Padmanaban     3/15/2016       
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_UpdateCaseOwnerTrackingTest{ 
    /*
     * Method to cover scenario where case owner history record is getting stamped.
     */
    static testMethod void validatePS_UpdateCaseOwnerTracking(){
        //user creation
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,2);
        insert listWithUser ;
        
        //contact creation
        List<Contact> conlist=TestDataFactory.createContact(1);
        insert conlist;
        
        List<case> cases = TestDataFactory.createCase(1,'School Assessment');
        cases[0].contactid=conlist[0].id;
        cases[0].accountid=conlist[0].accountid;
        cases[0].ownerid=listWithUser[0].id;
        cases[0].origin='Phone';
        //system.runas(listWithUser[0]){
        Test.startTest();
            insert cases;
        Case ca=[select id,ownerid,status,Case_Resolution_Category__c from case where id=:cases[0].id limit 1];
            //ca.ownerid=listWithUser[1].id;
            update ca;

        Case ca1=[select id,ownerid from case where id=:cases[0].id limit 1];
            ca.status='Closed';
            ca.Case_Resolution_Category__c='Customer - No Response';
            update ca;
        Test.stopTest();            
            List<Case_Owner_Tracking__c> oCOTlist = [select id,Agent_Name__c from Case_Owner_Tracking__c where case__c=:cases[0].id];
            //system.assert(oCOTlist.size()>1 ,'Owner is not a user');            
        
    }
    /*
     * Method to cover -ve scenario where case owner history record is getting stamped.
     */
    static testMethod void validatePS_UpdateCaseOwnerTrackingReopen(){
        //user creation
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,2);
        insert listWithUser ;
        
        //contact creation
        List<Contact> conlist=TestDataFactory.createContact(1);
        insert conlist;
        
        List<case> cases = TestDataFactory.createCase(1,'School Assessment');
        cases[0].contactid=conlist[0].id;
        cases[0].Status='Closed';
        cases[0].accountid=conlist[0].accountid;
        cases[0].ownerid=listWithUser[0].id;
        cases[0].origin='Phone';
        //system.runas(listWithUser[0]){
        Test.startTest();
            insert cases;

        Case ca1=[select id,ownerid from case where id=:cases[0].id];
            ca1.status='Closed';
            ca1.Case_Resolution_Category__c='Customer - No Response';
            ca1.Reopened__c = true;
            update ca1;
        Test.stopTest();            
            List<Case_Owner_Tracking__c> oCOTlist = [select id,Agent_Name__c from Case_Owner_Tracking__c where case__c=:cases[0].id];
            //system.assert(oCOTlist.size()>1 ,'Owner is not a user');            
        
    }
}//end class