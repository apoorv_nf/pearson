@isTest (seeAllData=false)
public with sharing class PS_Case_TriggerUpdateEscalateTest 
{   
    
    private static List<Case> generateTestCases(Integer numOfCases, Boolean setExternalId)
    {
        List<Case> casesToInsert = new List<Case>();
        
        
        //Generate an account
        Account acc = new Account(Name = 'Account 1');
        if(setExternalId)
        {
            acc.External_Account_Number__c = 'External';  
        }
        insert acc;
        
        //Generate contact
        Contact con = new Contact(LastName = 'Contact 1', FirstName = 'fn', Email = 'test@test.com.demo', AccountId = acc.Id,First_Language__c  = 'English',MobilePhone  = '498763425',Salutation = 'MR.',Preferred_Address__c = 'Other Address' ,OtherCountry  = 'India',OtherStreet = 'Test',OtherCity  = 'Test',OtherPostalCode  = '123456');
        insert con;
        
        AccountContact__c accCon = new  AccountContact__c(Account__c = acc.Id, Contact__c = con.Id, AccountRole__c = 'Role', Primary__c = true, Financially_Responsible__c = True);
        insert accCon;
        
        Account_Correlation__C ac = new Account_Correlation__C(Account__C = acc.Id, External_ID_Name__c = 'eVision Learner Number', External_ID__c = 'External ID');
        insert ac;
        
        // RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Case' and DeveloperName = 'Loan_Bursary_Request'];                        
        
        
        Id rt = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
        
        for(Integer i=0; i<numOfCases; i++)
        {    
            Case caseToInsert = new Case( Reopen_Reason__c = 'test',AccountId = acc.Id, RecordTypeid = rt, Type ='General', ContactId = con.Id, Sponsor_name__c = con.Id, Reason_if_Other__c = 'Other', Email_Initial_Response_Date__c = system.now(),ownerid=userinfo.getuserid());          
            casesToInsert.add(caseToInsert);
        }
        
        if(casesToInsert.size()>0)
        {
            insert casesToInsert;
        }
        
        return casesToInsert;
    }
    static testMethod void verifySubmisionValidationForCases()
    {  
        //List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
        //insert usrLst;
        
        //List <Case> cases = new List<Case>();
        //List<Case> generatedCases  = new List<Case>();
        
        User usrRec=[Select ID,Name from User where isActive=true and name='Deployment User' Limit 1];
        PermissionSetAssignment psa = new PermissionSetAssignment
            (PermissionSetId ='0PSb0000000Q6oH', AssigneeId = usrRec.ID);
        insert psa; 
        Case caseRecord = new Case();
        List<Case> childCaseList = new List<Case>();  
        User usrRe = new User();
        system.runas(usrRec){
            LIST<Account> acctList=new LIST<Account>();
            acctList=TestDataUtility_Class.createAccounts(2);
            insert acctList;
            System.debug('@@SOQL Count Account: '+Limits.getQueries());
            LIST<Contact> ConList=new LIST<Contact>();
            ConList=TestDataUtility_Class.createContacts(1,acctList[0]);
            LIST<Contact> ConList2=new LIST<Contact>();
            ConList2=TestDataUtility_Class.createContacts(1,acctList[1]);
            ConList.addAll(ConList2);
            insert ConList;
            System.debug('@@SOQL Count Contact: '+Limits.getQueries());
            //test.startTest();
            LIST<Case> CaseList=new LIST<Case>();
            CaseList=TestDataUtility_Class.createCases(3,acctList[0],ConList[0]);
            insert CaseList;
            System.debug('@@SOQL Count Cases: '+Limits.getQueries());
            test.startTest();
            Set<String> caseId = new Set<String>();
            
            Case caseRec=[Select AccountID,ContactID from Case where id=:CaseList[0].Id]; 
            
            
            caseRec.AccountID=acctList[1].Id;
            caseRec.ContactId=ConList[1].Id;
            update caseRec;
            
            System.debug('@@SOQL Count Update Cases: '+Limits.getQueries());
            childCaseList = TestDataFactory.createCase(3, 'School Assessment');
            childCaseList[0].ParentId=CaseList[0].id;
            childCaseList[0].createddate = system.Today() -8;
            
            childCaseList[1].Status=Label.PS_CaseClosureStatus;
            childCaseList[1].ParentId=CaseList[0].id;
            childCaseList[1].createddate = system.Today() -9;
            childCaseList[1].closedDate = system.Today() -8;
            
            insert childCaseList;    
            System.debug('@@SOQL Count childCaseList: '+Limits.getQueries());
            
            checkRecurssionAfter.executedTriggerName = new Set<String>();
            checkRecurssionBefore.executedTriggerName = new Set<String>();
            
            List<Case> caseRecords=[Select Status,OwnerId,Case_Resolution_category__c from Case where id IN :CaseList]; 
            
            insert new PS_QueueTiers__c(Name = 'QueueId',Tier2__c='00Gb0000002tDhLEAU',Tier2_5__c='00Gb0000002tDhUEAU',Tier3_1__c='00Gb0000002te8uEAA',Tier3_2__c='00Gb0000002tDhMEAU',Tier4__c='00Gb0000002tDhKEAU',Tier3Role__c='NA HE TS Agent Tier 3');
            List<PS_QueueTiers__c> ownr = PS_QueueTiers__c.getall().values();
            List<case> updateCases = new List<Case>();
            List<String> ownerId = new List<String>();
            ownerId.add(ownr[0].Tier2__c);
            ownerId.add(ownr[0].Tier2_5__c);
            ownerId.add('005b0000003SOCq');
            Integer i=0;
            for(Case caseObj:caseRecords)
            {
                caseObj.OwnerId= ownerId[i];
                updateCases.add(caseObj);
                i++;
            }
            System.debug('@@Soql Count Before test.stop: '+Limits.getQueries()); //49
           // update updateCases;
            System.debug('@@SOQL Count updateCases: '+Limits.getQueries());
            test.stopTest();
            
            checkRecurssionAfter.runOnce('testTrigger', 'Insert');
            checkRecurssionAfter.runOnce('testTrigger', 'Insert');
            checkRecurssionBefore.runOnce('testTrigger', 'Insert');
            checkRecurssionBefore.runOnce('testTrigger', 'Insert');
            Set<Id> caseIds=new Set<Id>();
            caseIds.add(caseRec.id);
            checkRecurssionAfter.runServiceIntOnce(caseIds, 'Update');
            
        }
    }   
}