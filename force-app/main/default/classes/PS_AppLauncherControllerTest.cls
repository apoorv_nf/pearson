/************************************************************************************************************
* Apex Interface Name : PS_AppLauncherControllerTest
* Version             : 1.1 
* Created Date        : 15-Feb-2016 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Hemangini                  15-Feb-2016  
* Rohit Kulkarni         04/03/2016           
-------------------------------------------------------------------------------------------------------------
* Test Name                   :Display Links BY Application Catalog
* Test Description            :Display Links BY Application Catalog
* Expected Inputs             :Pass parameters like Application name,static parameters,
* Expected Outputs            :Call the AppLauncher to dsplay page in new tab depending on sharing rule
-------------------------------------------------------------------------------------------------------------
* Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 37,91,143) 
************************************************************************************************************/
@isTest
public class PS_AppLauncherControllerTest{
        
    /*
     * Method to cover scenario where page parameters do not contain 'url'
     */
    static testMethod void test_createParameterizedUrl(){
        User usr;
        UserRole tempUserRole;
        List<AppCatalog__c> appCatalogList = new List<AppCatalog__c>();
        List<Application__c> appList = new List<Application__c>();
        List<AppParameter__c> appParamList = new List<AppParameter__c>();
        String appName;
        
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error and User validation errors
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){
            //DATA CREATION - START
            appCatalogList.add( new AppCatalog__c(name='First', Catalog__c='Catalog', APPLICATIONNAME__c='Application Catalog', STATICPARAM__c='Static',PASSTHROUGHPARAM__c='PassThrough', APPIDPARAM__c='AppId', CASEPARAM__c='case', PS_AppLauncherController__c='PS_AppLauncherController', CreateParameteizedURL__c='CreateParameteizedURL', True__c='True', UTF_8__c='UTF-8', url__c='url', http__c='http', https__c='https', TRACKNUMS__c='&trackNums='));
            insert appCatalogList;
            
            appList.add(new Application__c(URL__c='Test URL', App_Type__c='Console Tab'));          
            system.debug('appList++++'+appList);
            insert appList;
            appName = ''+[select name from Application__c where id=:appList[0].id].name;
            
            appParamList.add(new AppParameter__c(Application__c=appList[0].id, Param_Value__c='', Param_Type__c='Static', Name='Test AppParam'));
            appParamList.add(new AppParameter__c(Application__c=appList[0].id, Param_Value__c='', Param_Type__c='Pass Through', Name='Test AppParam'));
            appParamList.add(new AppParameter__c(Application__c=appList[0].id, Param_Value__c='', Name='Test AppParam'));
            insert appParamList;
            //DATA CREATION - END
        
        
            Test.startTest();
            PageReference pageRef = Page.PS_AppLauncher;
            Test.setCurrentPage(pageRef);
            PS_AppLauncherController appLauncherController = new PS_AppLauncherController();

            // Add parameters to page URL
            ApexPages.currentPage().getParameters().put('case', 'false');
            system.debug('appList[0].id++++'+appList[0].id);
            ApexPages.currentPage().getParameters().put('AppId', appName);
            
            // Instantiate a new appLauncherController with all parameters in the page
            appLauncherController = new PS_AppLauncherController();
            appLauncherController.link = '/apex/PS_AppLauncher';
            appLauncherController.createParameterizedUrl();
            System.assertEquals(appCatalogList[0].Catalog__c,'Catalog');
            Test.stopTest();
        }
    }
    
    /*
     * Method to cover scenario where page parameters contain 'url'
     */
    static testMethod void test_createParameterizedUrl2(){
        User usr;
        UserRole tempUserRole;
        List<AppCatalog__c> appCatalogList = new List<AppCatalog__c>();
        List<Application__c> appList = new List<Application__c>();
        List<AppParameter__c> appParamList = new List<AppParameter__c>();
        String appName;
        
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error and User validation errors
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){
            //DATA CREATION - START
            appCatalogList.add( new AppCatalog__c(name='First', Catalog__c='Catalog', APPLICATIONNAME__c='Application Catalog', STATICPARAM__c='Static',PASSTHROUGHPARAM__c='PassThrough', APPIDPARAM__c='AppId', CASEPARAM__c='case', PS_AppLauncherController__c='PS_AppLauncherController', CreateParameteizedURL__c='CreateParameteizedURL', True__c='True', UTF_8__c='UTF-8', url__c='url', http__c='http', https__c='https', TRACKNUMS__c='&trackNums='));
            insert appCatalogList;
            
            appList.add(new Application__c(URL__c='Test URL', App_Type__c='Console Tab'));          
            system.debug('appList++++'+appList);
            insert appList;
            appName = ''+[select name from Application__c where id=:appList[0].id].name;
            //DATA CREATION - END
        
        
            Test.startTest();
            PageReference pageRef = Page.PS_AppLauncher;
            Test.setCurrentPage(pageRef);
            PS_AppLauncherController appLauncherController = new PS_AppLauncherController();

            // Add parameters to page URL
            ApexPages.currentPage().getParameters().put('case', 'true');
            system.debug('appList[0].id++++'+appList[0].id);
            ApexPages.currentPage().getParameters().put('AppId', appName);
            ApexPages.currentPage().getParameters().put('url', 'www.testurl');
            ApexPages.currentPage().getParameters().put('trackNums', '10');
            
            // Instantiate a new appLauncherController with all parameters in the page
            appLauncherController = new PS_AppLauncherController();
            //appLauncherController.link = '/apex/PS_AppLauncher';
            appLauncherController.createParameterizedUrl();
            System.assertEquals(appCatalogList[0].Catalog__c,'Catalog');
            Test.stopTest();
        }
        }
    
    /*
     * Method to cover Negative scenario where page parameters contain 'url'
     */
    static testMethod void Negative_test_createParameterizedUrl2(){
    try{
        User usr;
        UserRole tempUserRole;
        List<AppCatalog__c> appCatalogList = new List<AppCatalog__c>();
        List<Application__c> appList = new List<Application__c>();
        List<AppParameter__c> appParamList = new List<AppParameter__c>();
        String appName;
        
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error and User validation errors
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){
            //DATA CREATION - START
            
            appCatalogList.add( new AppCatalog__c(Catalog__c='Catalog', APPLICATIONNAME__c='Application Catalog', STATICPARAM__c='Static',PASSTHROUGHPARAM__c='PassThrough', APPIDPARAM__c='AppId', CASEPARAM__c='case', PS_AppLauncherController__c='PS_AppLauncherController', CreateParameteizedURL__c='CreateParameteizedURL', True__c='True', UTF_8__c='UTF-8', url__c='url', http__c='http', https__c='https', TRACKNUMS__c='&trackNums='));
            insert appCatalogList;
            
            appList.add(new Application__c(URL__c='Test URL', App_Type__c='Console Tab'));          
            system.debug('appList++++'+appList);
            insert appList;
            appName = ''+[select name from Application__c where id=:appList[0].id].name;
            //DATA CREATION - END
        
        
            Test.startTest();
            PageReference pageRef = Page.PS_AppLauncher;
            Test.setCurrentPage(pageRef);
            PS_AppLauncherController appLauncherController = new PS_AppLauncherController();

            // Add parameters to page URL
            ApexPages.currentPage().getParameters().put('case', 'true');
            system.debug('appList[0].id++++'+appList[0].id);
            ApexPages.currentPage().getParameters().put('AppId', appName);
            ApexPages.currentPage().getParameters().put('url', 'www.testurl');
            ApexPages.currentPage().getParameters().put('trackNums', '10');
            
            // Instantiate a new appLauncherController with all parameters in the page
            appLauncherController = new PS_AppLauncherController();
            //appLauncherController.link = '/apex/PS_AppLauncher';
            appLauncherController.createParameterizedUrl();
            System.assertEquals(appCatalogList[0].Catalog__c,'');
            Test.stopTest();
        }
    }
    catch (DmlException e) {
            system.debug('case exception'+e.getMessage());
            Boolean expectedExceptionThrown =  e.getMessage().contains('Insert failed. First exception on row 0; first error: REQUIRED_FIELD_MISSING, Required fields are missing: [Name]: [Name]') ? true : false;
            System.AssertEquals(expectedExceptionThrown,true);
            System.assertEquals('REQUIRED_FIELD_MISSING' , e.getDmlStatusCode(0));

    }
    
    }
    
}