/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 02/07/2018
 */
global with sharing class Pearson_CustomThemeController {

	@AuraEnabled
	global static SnapIn_Settings__c getSnapInSettings() {
		return SnapIn_Settings__c.getInstance();
	}
}