public class TestDataUtility_Class{

public static List<Account> createAccounts(Integer numAccts) {

 List<Account> accts = new List<Account>();
 String accountRecordtypeid =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Learner').getRecordTypeId();
 for(Integer iCnt=0;iCnt<numAccts;iCnt++) {
 
    Account act = new Account(Name='AccountUtility_'+ iCnt,
    Recordtypeid = accountRecordtypeid , Line_of_Business__c = 'Schools',Geography__c = 'Core');    
    accts.add(act);
 
 }

 return accts;
 
 }

public static List<Contact> createContacts(Integer numContacts,Account AccRec) {

    List<Contact> cons = new List<Contact>();
    String contacttRecordtypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Global Contact').getRecordTypeId();
    
     for(Integer iCnt=0;iCnt<numContacts;iCnt++) {
         Contact cnt=new Contact(firstname='Test'+iCnt,lastname='Test'+iCnt,Recordtypeid = contacttRecordtypeid ,AccountId=AccRec.ID,MobilePhone = '7276234111');   
         cons.add(cnt);
     }
 return cons;
 
 }
 
 
 public static LIST<Case> createCases(Integer numCases,Account AccRec,Contact cntRec) {
 
    LIST<Case> caseList=new LIST<Case>();
     String caseRecordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
     for (integer iCnt=0;iCnt<numCases; iCnt++) {
         Case csRec=new Case(
         RecordTypeID=caseRecordtypeid, 
         AccountID=AccRec.ID,
         ContactID=cntRec.ID,
         Origin='Email',
         Category__c='Access Code', 
         Contact_Country__c='United States',
         Contact_Type__c='K-12 Student',
         Market__c='AU',
         Priority='Normal',
         Subject='Test',
         Platform__c='ActiveLearn',
         PS_Business_Vertical__c='Higher Education',
         Subcategory__c='Multiple Accounts',
         Request_Type__c='Career Colleges',
         Description='Test'+iCnt );
         caseList.add(csRec);
         
     }
     
  return caseList;
 }
 
}