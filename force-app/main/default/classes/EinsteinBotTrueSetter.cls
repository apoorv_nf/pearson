/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 02/06/2018
 */
global without sharing class EinsteinBotTrueSetter {

    @InvocableMethod(label='Einstein Bot - Set boolean as true')
    global static List<Boolean> setTrue() {
		return new List<Boolean>{true};
	}
}