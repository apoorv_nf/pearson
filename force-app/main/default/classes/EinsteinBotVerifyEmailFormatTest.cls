/*
 * Author: Apoorv (Neuraflash)
 * Date: 06/08/2019
 * Description: Test Class for EinsteinBotContactCheck.
*/
@isTest
private class EinsteinBotVerifyEmailFormatTest {
    
    @isTest
    private static void testVerifyEmailFormat(){

        Test.startTest();
            List<Boolean> isValid = EinsteinBotVerifyEmailFormat.verifyEmailFormat(new List<String>{'test@gmail.com'});
        Test.stopTest();

        System.assertEquals(TRUE, isValid[0]);

    }

    @isTest
    private static void testVerifyEmailFormatNegative(){

        Test.startTest();
            List<Boolean> isValid = EinsteinBotVerifyEmailFormat.verifyEmailFormat(new List<String>{'test.com'});
        Test.stopTest();

        System.assertEquals(FALSE, isValid[0]);

    }

}