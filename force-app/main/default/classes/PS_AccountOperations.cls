/*******************************************************************************************************************
* Apex Class Name  : PS_AccountOperations
* Version          : 1.0 
* Created Date     : 23 March 2015
* Function         : Class for performing operations on Account Object
* Modification Log :
*
* Developer                   Date                     Description
* ------------------------------------------------------------------------------------------------------------------
*Leonard Victor             21/8/2015            Code StreamLine for R3
*Chaitra                    10/2015              D-0761: Bypassed Update Primary Selling Account check for Accounts created from Lead Conversion 
* Madhu                     10/02/16             RD-01676/INC2247285 - fix.  
* Rahul                     11/03/16             RD-01676 message Formatting issue 
*Nikhil                    24/2/2016             D-4205: Commented Code for Std Field Update - Approved by Brandon
*Kyama                     27/09/2016            Implemented New Method to restore Account attributes from overriten when integration profile tries to update account.
* Neuraflash (MM)           28/06/2018           Using cached user and profile login data
*******************************************************************************************************************/

public class PS_AccountOperations{

    public static void inserAccountTeam(List<Account> accountLst){

        Id learnerRecordType;
        Id  OrgRecordType;
        Profile userProfile;
        List<AccountTeamMember> teamMemeberLst = new List<AccountTeamMember>();

         //Using Util class to fetch record type instead of SOQL
           if(PS_Util.recordTypeMap.containsKey(PS_Constants.ACCOUNT_OBJECT) && (PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).containsKey(PS_Constants.ACCOUNT_LEARNER_RECCORD) && PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).containsKey(PS_Constants.ACCOUNT_ORGANISATION_RECCORD))){                          
               learnerRecordType = PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).get(PS_Constants.ACCOUNT_LEARNER_RECCORD); 
               OrgRecordType = PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).get(PS_Constants.ACCOUNT_ORGANISATION_RECCORD);
           }
            else{
               learnerRecordType = PS_Util.fetchRecordTypeByName(Account.SobjectType,Label.PS_LearnerRecordType);
               OrgRecordType = PS_Util.fetchRecordTypeByName(Account.SobjectType,Label.PS_OrganisationRecordType);
            }
           //End of Record type fix  

        //Neuraflash - If Automated Process User, profile is not visible. So we default to System Administrator
        try {
            //Fecthing Profile Name for logged in User
            userProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        } catch(QueryException ex) {
            userProfile = [SELECT Name FROM Profile WHERE Name = 'System Administrator'];
        }

        if(userProfile.Name!=PS_ProfileNames.SYSTEM_ADMINISTRATOR  && userProfile.Name!=PS_ProfileNames.PEARSON_DATA_ADMINISTRATOR ){

            for(Account accObj:accountLst){
                Boolean b2bLeadAcc = accObj.Lead_Conversion_Record_Type__c != null && accObj.Lead_Conversion_Record_Type__c.equals(System.Label.Lead_B2B_RecordType);               
                if(accObj.RecordtypeId == learnerRecordType || accObj.RecordtypeId==OrgRecordType){
                     if(!b2bLeadAcc)
                     {
                       AccountTeamMember teamMemeber = new AccountTeamMember(); 
                       teamMemeber.AccountId=accObj.id;
                       teamMemeber.UserId=accObj.OwnerId;
                       teamMemeber.TeamMemberRole= 'Account Manager';
                       teamMemeberLst.add(teamMemeber);
                     }
                }
            }
        }
        
        if(!teamMemeberLst.isEmpty()){

            insert teamMemeberLst;
        }
              
    }
    
    //Added By Kyama For R6 Implementation to restore account values when integration profile updates.
     public static void restoreAccountFieldsOnELTuserUpdate(List<Account> accountLst, Map<Id,Account> oldTriggerMap){
        //Added as part of 101 SOQL error after SP,Lex and CI changes deploy
        //Profile userProfile = [select Name from profile where id = :userinfo.getProfileId()];
        User loggedUserObj = [Select id,name,market__c,Profile.Name from User where id = : Userinfo.getUserid()]; 
        if(loggedUserObj.Profile != null && loggedUserObj.Profile.Name == 'One CRM Integration' && loggedUserObj.market__c == 'UK') {
            for(Account accObj : accountLst){
                accObj.RecordtypeId = oldTriggerMap.get(accObj.Id).RecordtypeId;
                accObj.Organisation_Type__c = oldTriggerMap.get(accObj.Id).Organisation_Type__c;
                accObj.Sub_Type__c = oldTriggerMap.get(accObj.Id).Sub_Type__c;
                accObj.Type__c = oldTriggerMap.get(accObj.Id).Type__c;
                accObj.Line_of_Business__c = oldTriggerMap.get(accObj.Id).Line_of_Business__c;
                accObj.Geography__c = oldTriggerMap.get(accObj.Id).Geography__c;
                accObj.Business_Unit__c = oldTriggerMap.get(accObj.Id).Business_Unit__c;
                accObj.Group__c = oldTriggerMap.get(accObj.Id).Group__c; 
                accObj.Market2__c =  oldTriggerMap.get(accObj.Id).Market2__c;  
            }
        } 
    }
    
    public static void AccountTerritoryAssociationNotification(List<Account> accountLst, Map<Id,Account> oldTriggerMap){
        
        List<Account> acList = new List<Account>();
        
         for(Account accObj : accountLst){
                if(accObj.Territory_Code_s__c!= oldTriggerMap.get(accObj.Id).Territory_Code_s__c){
                        acList.add(accObj);
                 }
       }  
        
            if(acList != null){
            Set<String> newtrStr = new Set<String>();
            Set<String> oldtrStr = new Set<String>();
            Map<String,List<Account>> AddCodeAccountmap = new Map<String,List<Account>>();
            Map<String,List<Account>> RemovedCodeAccountmap = new Map<String,List<Account>>();
            Map<String,Id> territoryIDCOdeMap = new Map<String,Id>();
            Set<Id> accId=new Set<Id>();
            for(Account ac: acList){
                if (ac.Territory_Code_s__c != oldTriggerMap.get(Ac.Id).Territory_Code_s__c){
                    accId.add(ac.Id);
                    if(ac.Territory_Code_s__c != null){
                           for(String s: ac.Territory_Code_s__c.split(',')){
                            newtrStr.add(s);
                        }
                        if(oldTriggerMap.get(ac.Id).Territory_Code_s__c != null){
                            for(String s : oldTriggerMap.get(ac.Id).Territory_Code_s__c.split(',')){
                                oldtrStr.add(s);
                            }
                        }
                    }
                    else{
                        if(oldTriggerMap.get(ac.Id).Territory_Code_s__c != null){
                            for(String s : oldTriggerMap.get(ac.Id).Territory_Code_s__c.split(',')){
                                oldtrStr.add(s);
                            }
                        }
                    }
                }//end territory code change
                
                    if(newtrStr != null){
                    for(String ns : newtrStr){
                        if(!oldtrStr.Contains(ns)){
                            if(AddCodeAccountmap.containsKey(ns)){
                                List<Account> associatedAccList = AddCodeAccountmap.get(ns);
                                associatedAccList.add(ac);
                                AddCodeAccountmap.put(ns,associatedAccList);
                            }ELSE{
                                List<Account> associatedAccList = new List<Account>();
                                associatedAccList.add(ac);
                                AddCodeAccountmap.put(ns,associatedAccList);
                            }
                                        
                        }
                    }
                }
                if(oldtrStr != null){
                for(String aut : oldtrStr){
                    if(!newtrStr.Contains(aut)){
                        if(RemovedCodeAccountmap.containsKey(aut)){
                           List<Account> associatedAccList = RemovedCodeAccountmap.get(aut);
                            associatedAccList.add(ac);
                            RemovedCodeAccountmap.put(aut,associatedAccList);
                        }ELSE{
                            List<Account> associatedAccList = new List<Account>();
                            associatedAccList.add(ac);
                            RemovedCodeAccountmap.put(aut,associatedAccList);
                        }
                    }
                }
                }
            }               //}
        //}
            List<Territory2> trList = new List<Territory2>();
            If(RemovedCodeAccountmap != null && AddCodeAccountmap != null){
                trList = [select name,Territory_Code__c, ParentTerritory2Id,ParentTerritory2.Territory_Code__c,ParentTerritory2.Name,ParentTerritory2.ParentTerritory2Id,ParentTerritory2.ParentTerritory2.Territory_Code__c,ParentTerritory2.ParentTerritory2.Name,Territory2ModelId,Territory2Model.Name from Territory2 where  (Territory_Code__c IN: AddCodeAccountmap.keyset() OR Territory_Code__c IN: RemovedCodeAccountmap.keyset()) AND Territory2Model.state ='Active'];
            }
            List<Id> ptidList = new List<Id>();
            for(Territory2 t : trList){
                ptidList.add(t.ParentTerritory2Id);
            }
            //MR:9/3/2015:Commented the code for not sending notifications to Peer territory users - Incident INC1972969
            //List<Territory2> pTList = [select id, name,Territory_Code__c, ParentTerritory2Id,ParentTerritory2.Territory_Code__c,ParentTerritory2.Name,ParentTerritory2.ParentTerritory2Id,ParentTerritory2.ParentTerritory2.Territory_Code__c,ParentTerritory2.ParentTerritory2.Name,Territory2ModelId,Territory2Model.Name from Territory2 where ParentTerritory2Id IN: ptidList];
            Map<Id,Territory2> ctidMap = new Map<Id,Territory2>();
            Map<Id,String> ptidMap = new Map<Id,String>();
            Map<Id,String> pptidMap = new Map<Id,String>();
            Map<Id,Account> pAccdetails=new Map<Id,Account>([select id,parentid,parent.name from Account where id in :accId]);
            for(Territory2 t : trList){
                ctidMap.put(t.id,t);//current Territory
                ptidMap.put(t.ParentTerritory2Id,t.ParentTerritory2.Territory_Code__c);//Parent Territory
                pptidMap.put(t.ParentTerritory2.ParentTerritory2Id,t.ParentTerritory2.ParentTerritory2.Territory_Code__c);//Grant Parent
            }
            
            
            List<UserTerritory2Association> AssignedUsersToTerritory = new List<UserTerritory2Association>();
            //Map of aready assigned user's record with there territory Id.
            Map<String,List<UserTerritory2Association>> AssignedUsersToTerritoryMap = new Map<String,List<UserTerritory2Association>>();
            If(ctidMap.KeySet() != null && ptidMap.KeySet() != null && pptidMap.KeySet() != null){
                //AssignedUsersToTerritory = [select id,Territory2.Territory_Code__c,Territory2.ParentTerritory2Id,Territory2Id,Territory2.ParentTerritory2.ParentTerritory2Id,Territory2.Name,Territory2.Territory2ModelId,Territory2.Territory2Model.Name, UserId ,User.Name,User.IsActive, RoleInTerritory2 from UserTerritory2Association where ((RoleInTerritory2 = 'Learning Technology Specialist' OR RoleInTerritory2 = 'Customer Digital Success Agent' OR RoleInTerritory2  = 'Temporary Sales Representative' OR RoleInTerritory2 = 'Sales Representative') AND (Territory2Id IN: ctidMap.KeySet())) OR (RoleInTerritory2 = 'District Manager' AND Territory2Id IN: ptidMap.keyset()) OR (RoleInTerritory2 = 'Regional Manager' AND Territory2Id IN: pptidMap.keyset())];
                String[] usrrole=System.Label.PS_AccountTerritoryUserRole.split(',');
                AssignedUsersToTerritory = [select id,Territory2.Territory_Code__c,Territory2.ParentTerritory2Id,Territory2Id,Territory2.ParentTerritory2.ParentTerritory2Id,
                Territory2.Name,Territory2.Territory2ModelId,Territory2.Territory2Model.Name, UserId ,User.Name,User.IsActive, RoleInTerritory2 from 
                UserTerritory2Association where User.IsActive=true and
                ((RoleInTerritory2 in :usrrole AND Territory2Id IN: ctidMap.KeySet()) OR 
                (RoleInTerritory2 = :System.Label.PS_AccountParTerUserRole AND Territory2Id IN: ptidMap.keyset()) OR 
                (RoleInTerritory2 = : System.Label.PS_AccountGPTerUserRole AND Territory2Id IN: pptidMap.keyset()))];
            }
                      
                if(AssignedUsersToTerritory != null){
                for(UserTerritory2Association aut : AssignedUsersToTerritory){
                    if(AssignedUsersToTerritoryMap.containsKey(aut.Territory2.Territory_Code__c)){
                        List<UserTerritory2Association> utList = AssignedUsersToTerritoryMap.get(aut.Territory2.Territory_Code__c);
                        utList.add(aut);
                        AssignedUsersToTerritoryMap.put(aut.Territory2.Territory_Code__c,utList);
                    }ELSE{
                        List<UserTerritory2Association> utList = new List<UserTerritory2Association>();
                        utList.add(aut);
                        AssignedUsersToTerritoryMap.put(aut.Territory2.Territory_Code__c,utList);
                    }
                }
            }
        User useradmin;
        try{
            useradmin = [select Id, Name,Email,UserRoleId,UserRole.Name from User where UserRole.Name = :System.Label.PS_TerritoryUserAdminRole 
            AND Market__c =: System.Label.PS_TerritoryMarket and isactive=true limit 1];
        }catch(Exception e){
        }
          
            String msg = '';
            String adminmsg = '';
            Boolean flag = false;
            List<FeedItem> fiList = new List<FeedItem>();
            if(AssignedUsersToTerritoryMap != null){
             for(String st : AssignedUsersToTerritoryMap.KeySet()){//loop -1
                  if(AddCodeAccountmap != null && AddCodeAccountmap.containsKey(st)){
                  flag = false;
                  List<UserTerritory2Association>  lst =  AssignedUsersToTerritoryMap.get(st);
                  
                  List<UserTerritory2Association> alluser = new List<UserTerritory2Association>();
                      if(ptidMap.containsKey(lst[0].Territory2.ParentTerritory2Id) && AssignedUsersToTerritoryMap.get(ptidMap.get(lst[0].Territory2.ParentTerritory2Id)) != null){
                      alluser.addAll(AssignedUsersToTerritoryMap.get(ptidMap.get(lst[0].Territory2.ParentTerritory2Id)));
                       for(Territory2 cti : ctidMap.values()){ //loop -2
                        if(cti.ParentTerritory2Id==lst[0].Territory2.ParentTerritory2Id && AssignedUsersToTerritoryMap.get(cti.territory_Code__c) != null){
                            alluser.addAll(AssignedUsersToTerritoryMap.get(cti.territory_Code__c));
                           }
                      }
                  }
            
                  if(pptidMap.containsKey(lst[0].Territory2.ParentTerritory2.ParentTerritory2Id) && AssignedUsersToTerritoryMap.get(pptidMap.get(lst[0].Territory2.ParentTerritory2.ParentTerritory2Id))!= null){
                      alluser.addAll(AssignedUsersToTerritoryMap.get(pptidMap.get(lst[0].Territory2.ParentTerritory2.ParentTerritory2Id)));
                  }
                  for(UserTerritory2Association uat : alluser){
                   //Madhu : INC2247285 fix
                    for(String PterrCode:  ptidMap.Keyset()){
                   for(String GpterrCode:  pptidMap.Keyset()){
                   if(uat.User.IsActive == true &&  (AddCodeAccountmap.containsKey(uat.Territory2.Territory_Code__c) || uat.Territory2.Territory_Code__c == ptidMap.get(PterrCode) || uat.Territory2.Territory_Code__c == pptidMap.get(GpterrCode) ) ){  
                    FeedItem post = new FeedItem();
                    FeedItem adminpost = new FeedItem();
                    post.ParentId = uat.UserId; 
                    adminpost.ParentId = useradmin.Id;
                    msg = Label.PS_AccountTerritory5+' '+uat.User.Name+', \n '+Label.PS_AccountTerritory+' ' +uat.RoleInTerritory2+' ' + Label.PS_AccountTerritory7+' '+uat.Territory2.Name+'.';
                    adminmsg = Label.PS_AccountTerritory5+' '+useradmin.Name+', \n '+Label.PS_AccountTerritory+' ' +useradmin.UserRole.Name+'.';
                    adminmsg += '\n \n '+Label.PS_AccountTerritory3;
                    msg += '\n \n '+Label.PS_AccountTerritory3;
                    for(Account ac : AddCodeAccountmap.get(st)){                        
                        if (pAccdetails.containsKey(ac.Id)){
                            if (pAccdetails.get(ac.Id).parent.name != null){
                                msg +=' '+pAccdetails.get(ac.Id).parent.name+' '+'-'+' '+ac.name + ' \n';
                                adminmsg +=' '+pAccdetails.get(ac.Id).parent.name+' '+'-'+' '+ac.name + ' \n';
                            }
                            else{
                                msg +=' '+ac.name + ' \n';
                                adminmsg +=' '+ac.name + ' \n';
                            }
                        }   
                    }   
                    adminmsg += Label.PS_AccountTerritory4+' '+lst[0].Territory2.Name;
                    msg += Label.PS_AccountTerritory4+' '+ lst[0].Territory2.Name;
                    if(lst[0].UserId == uat.UserId){
                        msg +=' '+Label.PS_AccountTerritory9+' '+uat.RoleInTerritory2;
                    }
                    adminmsg +=' '+Label.PS_AccountTerritory8 +' '+ uat.Territory2.Territory2Model.Name+' '+Label.PS_AccountTerritory6;
                    msg += ' '+Label.PS_AccountTerritory8 +' '+ uat.Territory2.Territory2Model.Name+' '+Label.PS_AccountTerritory6;
                    msg += '\n \n '+Label.PS_AccountTerritory1;
                    post.Body = msg;
                    adminpost.body = adminmsg;
                    //Nikhil: Commented system field set post.CreatedDate = System.now();
                    //Nikhil: Commented system field set adminpost.CreatedDate = System.now();
                    fiList.add(post);
                    if(flag == false){
                        fiList.add(adminpost);
                   }
                   flag = true;
                  }
                }
                }
                }
                }   
                //}
                //for(String st : AssignedUsersToTerritoryMap.KeySet()){
                if(RemovedCodeAccountmap != null && RemovedCodeAccountmap.containsKey(st)){
                 flag = false;
                  List<UserTerritory2Association>  lst =  AssignedUsersToTerritoryMap.get(st);
                  List<UserTerritory2Association> alluser = new List<UserTerritory2Association>();
                  if(ptidMap.containsKey(lst[0].Territory2.ParentTerritory2Id) && AssignedUsersToTerritoryMap.get(ptidMap.get(lst[0].Territory2.ParentTerritory2Id)) != null){
                      alluser.addAll(AssignedUsersToTerritoryMap.get(ptidMap.get(lst[0].Territory2.ParentTerritory2Id)));
                      for(Territory2 cti : ctidMap.values()){
                        if(cti.ParentTerritory2Id==lst[0].Territory2.ParentTerritory2Id && AssignedUsersToTerritoryMap.get(cti.territory_Code__c) != null){
                            alluser.addAll(AssignedUsersToTerritoryMap.get(cti.territory_Code__c));
                        }
                      }
                  }
                  if(pptidMap.containsKey(lst[0].Territory2.ParentTerritory2.ParentTerritory2Id) && AssignedUsersToTerritoryMap.get(pptidMap.get(lst[0].Territory2.ParentTerritory2.ParentTerritory2Id)) != null){
                      alluser.addAll(AssignedUsersToTerritoryMap.get(pptidMap.get(lst[0].Territory2.ParentTerritory2.ParentTerritory2Id)));
                  }
                  for(UserTerritory2Association uat : alluser){
                   //Madhu : INC2247285 fix
                   for(String PterrCode:  ptidMap.Keyset()){
                   for(String GpterrCode:  pptidMap.Keyset()){
                     if(uat.User.IsActive == true &&  (RemovedCodeAccountmap.containsKey(uat.Territory2.Territory_Code__c) || uat.Territory2.Territory_Code__c == ptidMap.get(PterrCode) || uat.Territory2.Territory_Code__c == pptidMap.get(GpterrCode) ) ){    
                   
                        FeedItem post = new FeedItem();
                        FeedItem adminpost = new FeedItem();
                        post.ParentId = uat.UserId;
                        adminpost.ParentId = useradmin.Id;                      
                        msg = Label.PS_AccountTerritory5+' '+uat.User.Name+', \n '+Label.PS_AccountTerritory+' ' +uat.RoleInTerritory2+' '+Label.PS_AccountTerritory7+' '+uat.Territory2.Name+'.';
                        adminmsg = Label.PS_AccountTerritory5+' '+useradmin.Name+', \n '+Label.PS_AccountTerritory+' ' +useradmin.UserRole.Name+'.';
                        msg += '\n \n '+Label.PS_AccountTerritory3;
                        adminmsg += '\n \n '+Label.PS_AccountTerritory3;
                        for(Account ac : RemovedCodeAccountmap.get(st)){
                           if (pAccdetails.containsKey(ac.Id)){
                            if (pAccdetails.get(ac.Id).parent.name != null){
                                msg +=' '+pAccdetails.get(ac.Id).parent.name+' '+'-'+' '+ac.name + ' \n';
                                adminmsg +=' '+pAccdetails.get(ac.Id).parent.name+' '+'-'+' '+ac.name + ' \n';
                            }
                            else{
                                msg +=' '+ac.name + ' \n';
                                adminmsg +=' '+ac.name + ' \n';
                            }
                          }   
                        }   
                        msg += Label.PS_AccountTerritory10+' '+ lst[0].Territory2.Name;
                        adminmsg += Label.PS_AccountTerritory10+' '+lst[0].Territory2.Name;
                        if(lst[0].UserId == uat.UserId){
                            msg += ' '+Label.PS_AccountTerritory9+' '+uat.RoleInTerritory2;
                        }
                        adminmsg += ' '+Label.PS_AccountTerritory8+' '+ uat.Territory2.Territory2Model.Name+' '+Label.PS_AccountTerritory6;
                        msg += ' '+Label.PS_AccountTerritory8+' '+uat.Territory2.Territory2Model.Name+' '+Label.PS_AccountTerritory6;
                        //MR:09/04/15 message formated as per production change
                        msg += '\n \n '+Label.PS_AccountTerritory2;
                        post.Body = msg;
                        adminpost.body = adminmsg;
                        //Nikhil: Commented system field set post.CreatedDate = System.now();
                        //Nikhil: Commented system field set adminpost.CreatedDate = System.now();
                        fiList.add(post);
                        if(flag == false){
                            fiList.add(adminpost);
                        }
                        flag = true;
                        }
                    
                   }
                   } 
                  }
                }   
                //}
            }
            }
                
            if(fiList.size() > 0 && fiList.size() < 150){
                insert fiList;
            }
        }
    }// Pooja
    
   public static void updateCoursePrimaryAccount(List<Account> newAccountLst, Map<Id,Account> oldAccountMap){

       //Setting Primary Account on course if there is a update of primary account on Account
       List<Id> primaryModifiedAccLst = new List<Id>();
       for(Account accObj :newAccountLst){
           if(accObj.Primary_Selling_Account__c!=null && (accObj.Primary_Selling_Account__c!=oldAccountMap.get(accObj.id).Primary_Selling_Account__c)){
                primaryModifiedAccLst.add(accObj.id);
           }
       }
       if(!primaryModifiedAccLst.isEmpty() || test.isRunningTest()){
           List<UniversityCourse__c> courseLst = [Select id,Account__c,Account__r.name,Account__r.Primary_Selling_Account__c,Account__r.Primary_Selling_Account_check__c,Primary_Selling_AccountText__c,Account__r.Primary_Selling_Account__r.name,Account__r.Parent_Selling_Account__c from UniversityCourse__c where Account__c in :primaryModifiedAccLst];
           if(!courseLst.isEmpty()){
                for(UniversityCourse__c courseObj : courseLst){
                    if(courseObj.Account__r.Primary_Selling_Account_check__c){
                        courseObj.Primary_Selling_AccountText__c = courseObj.Account__r.name;
                    }
                    else if(courseObj.Account__r.Primary_Selling_Account__c!=null){
                        courseObj.Primary_Selling_AccountText__c = courseObj.Account__r.Primary_Selling_Account__r.name;
                    }
                    else{
                        courseObj.Primary_Selling_AccountText__c = '';
                    }
                    //courseObj.Primary_Selling_AccountText__c = courseObj.Account__r.Parent_Selling_Account__c;
                }
                Database.SaveResult[] lstResult =  Database.update(courseLst, false);
                List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
                for(Database.SaveResult resultObj:lstResult){
                        String ErrMsg='';
                        if (!resultObj.isSuccess()){
                              PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                               errlogger.InterfaceName__c='Course Primary Selling Account Mapping';
                               errlogger.ApexClassName__c='PS_AccountOperations';
                               errlogger.CallingMethod__c='updateCoursePrimaryAccount';
                               errlogger.UserLogin__c=UserInfo.getUserName(); 
                               errlogger.recordid__c=resultObj.getId();
                               for(Database.Error err : resultObj.getErrors())
                                        ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage(); 
                               errlogger.ExceptionMessage__c=  ErrMsg;  
                               errloggerlist.add(errlogger);    
                         }
                }
                if(errloggerlist.size()>0){
                          insert errloggerlist;
                }

           }

       }
       //End of Logic of setting primary account
   }
    
    
    

}