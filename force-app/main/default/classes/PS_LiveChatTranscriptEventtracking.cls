/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_LiveChatTranscriptEventtracking 
   Description:     On afterinsert of LiveChatTranscriptEvent
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
   02/12/2016          1.0          Karthik.A.S                      to change case owner while transfer 
   02/24/2016          2.0          KP                               Amended code 
   07/03/2016          3.0          SN                               Amended code 
   10/03/2016          4.0          KP                               Amended code 
   30/03/2016          5.0          SN                               Amended code 
   25/04/2016          6.0          SN                               Added logic for ChatTransfer__c
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
Public class PS_LiveChatTranscriptEventtracking
{   
    public static boolean transferred = false; //to handle multi-transfer scenario
    public static boolean execute= true; 
    public static datetime dChatStartTime; //chat start time
    public static datetime dChatEndTime;//chat end time
    public static String sChatEvent; //chat event
    public static String sAgentId; //chat agent
    
    /**
    * Description : Method to Identify chat event and init data
    * @param : new event list and new event map
    * @return NA
    * @throws NA
    **/
    public static void mInitChatEvent(List<LiveChatTranscriptevent> oChatEventlist,Map<Id,LiveChatTranscriptevent> oChatEventMap){
        Map<Id,LiveChatTranscriptevent> oChatTranstoEvent = new Map<Id,LiveChatTranscriptevent>();//Map transcriptid to event
        for(LiveChatTranscriptevent oChatEvent: oChatEventlist){
            sChatEvent=oChatEvent.Type;
            sAgentId = oChatEvent.AgentId;
            if (sChatEvent == Label.PS_ChatAccept){
                dChatStartTime=oChatEvent.Time;                
                oChatTranstoEvent.put(oChatEvent.LiveChatTranscriptId,oChatEvent);//Init map of Transscriptid to event
            }
            if (sChatEvent == Label.PS_ChatTransferAccept || Label.PS_ChatEnd.contains(sChatEvent)){
                dChatEndTime=oChatEvent.Time;
                oChatTranstoEvent.put(oChatEvent.LiveChatTranscriptId,oChatEvent);//Init map of Transscriptid to event
            }
        } //end for loop       
        if (oChatTranstoEvent.values().size() > 0){
        Map<Id,LiveChatTranscript> oChatTranscriptmap = new Map<Id,LiveChatTranscript>([select id,caseid,chatduration
                                                        from LiveChatTranscript where id in: oChatTranstoEvent.keySet()]);          
        Map<Id,LiveChatTranscriptevent> oCaseChatEvent;
        if(oChatTranscriptmap.values().size()>0)
        {
            oCaseChatEvent=new Map<Id,LiveChatTranscriptevent>();
            for(LiveChatTranscript oChatTranscript:oChatTranscriptmap.values())
            {
                oCaseChatEvent.put(oChatTranscript.caseid,oChatTranstoEvent.get(oChatTranscript.Id)); //Init map of caseid to event
            }
        }     
        if (oCaseChatEvent != null)
            mProcessChatEvent(oCaseChatEvent);     
            }    
    }//end method mInitChatEvent
    
    /**
    * Description : Method to process chat events
    * @param : new event list and new event map
    * @return NA
    * @throws NA
    **/  
    public static void mProcessChatEvent(Map<Id,LiveChatTranscriptevent> oCaseChatEvent){
        if (sChatEvent  == Label.PS_ChatAccept){//Initial chat Accept scenario
            mAcceptChat(oCaseChatEvent);
        }
        if (sChatEvent  == Label.PS_ChatTransferAccept && execute){//chat Transfer scenario
            mTransferChat(oCaseChatEvent);
        }
        if (Label.PS_ChatEnd.contains(sChatEvent) && execute){//chat end scenario
            mEndChat(oCaseChatEvent);
        }
    }//end method mProcessChatEvent
    
    /**
    * Description : Method to process Accept chat event
    * @param : map - caseid to chatevent
    * @return NA
    * @throws NA
    **/
    public static void mAcceptChat(Map<Id,LiveChatTranscriptevent> oCaseChatEvent){  
        List<Case> oCasetoUpdate=new List<Case>();
        List<Case_Owner_Tracking__c> oCaseOwnerTrackLst=new List<Case_Owner_Tracking__c>();    
        for(Case oCase:[select id,Ownerid,Status,ClosedDate,recordtype.name,BusinessHoursId,(select id,Agent_Handle_Time__c,Agent_Name__c from 
                                Case_Owner_Trackings__r order by createddate desc limit 1) from Case 
                                where id in :oCaseChatEvent.keyset()]){   
            if (Label.PS_ServiceRecordTypes.contains(oCase.recordtype.name)){ 
                  Integer noOfDays=0;
                  
                  if (oCase.ClosedDate != null){
                      DateTime  dT = oCase.ClosedDate ; 
                      Date closeDate = date.newinstance(dT.year(), dT.month(), dT.day()); 
                      noOfDays = closeDate.daysBetween(date.today());   
                  }              
                  if(oCase.Status == Label.PS_CaseClosureStatus && noOfDays > 7){
                      execute=false;
                  }
                  if (execute){ 
                      boolean bcaseupflg=false;
                      if(oCase.Ownerid != sAgentId){oCase.Ownerid =sAgentId;oCase.ChatTransfer__c=true;bcaseupflg=true;} 
                     //if(oCase.Status==Label.PS_CaseClosureStatus){oCase.Status=Label.PS_CaseInProgressStatus;bcaseupflg=true;}
                      if (bcaseupflg) 
                          oCasetoUpdate.add(oCase);                      
                  }

            }                     
        } //end for loop
        if (oCasetoUpdate.size()>0 && !Test.isRunningTest())
            update oCasetoUpdate;
        if (oCaseOwnerTrackLst.size()>0 && !Test.isRunningTest())   
            upsert oCaseOwnerTrackLst;       
    }//end method mAcceptChat
    
    /**
    * Description : Method to process Transfer chat event
    * @param : map - caseid to chatevent
    * @return NA
    * @throws NA
    **/
    public static void mTransferChat(Map<Id,LiveChatTranscriptevent> oCaseChatEvent){  
        List<Case> oCasetoUpdate=new List<Case>();
        List<Case_Owner_Tracking__c> oCaseOwnerTrackLst=new List<Case_Owner_Tracking__c>();    
        for(Case oCase:[select id,Status,ClosedDate,Ownerid,recordtype.name,BusinessHoursId,(select id,Agent_Handle_Time__c,Agent_Name__c from 
                                Case_Owner_Trackings__r order by createddate desc limit 1) from Case 
                                where id in :oCaseChatEvent.keyset()]){   
            if (Label.PS_ServiceRecordTypes.contains(oCase.recordtype.name)){              
                if (transferred ){
                Case_Owner_Tracking__c oCOT = new Case_Owner_Tracking__c(Agent_Name__c =sAgentId,case__c=oCase.id,Agent_Handle_Time__c=0);oCaseOwnerTrackLst.add(oCOT);}//ownerid=sAgentId
                transferred = true;
                if  (oCase.Case_Owner_Trackings__r != null){//update existing owner chat duration
                    if(!oCase.Case_Owner_Trackings__r.isEmpty() && oCase.Case_Owner_Trackings__r[0].Agent_Name__c == oCase.ownerid){
                        Case_Owner_Tracking__c oCOT = new Case_Owner_Tracking__c(Id=oCase.Case_Owner_Trackings__r[0].id);
                        Long duration=(BusinessHours.diff(oCase.BusinessHoursId,dChatStartTime,dChatEndTime))/(1000);
                        //11/07/2017 - Rakshik Bhan: Commenting this piece of code because it is adding the chat time to the total agent handle time.
                        //oCOT.Agent_Handle_Time__c=oCase.Case_Owner_Trackings__r[0].Agent_Handle_Time__c+duration; 
                        oCaseOwnerTrackLst.add(oCOT);                          
                    }   
                }     
                if(oCase.Ownerid != sAgentId){
                    oCase.Ownerid =sAgentId;   
                    oCase.ChatTransfer__c=true; 
                    oCasetoUpdate.add(oCase);             
                } 
                dChatStartTime=dChatEndTime;
            }
        }//end for loop       
        if (oCasetoUpdate.size()>0 && !Test.isRunningTest())
            update oCasetoUpdate;
        if (oCaseOwnerTrackLst.size()>0 && !Test.isRunningTest())   
            upsert oCaseOwnerTrackLst;               
    }  //end method mTransferChat 
    
    /**
    * Description : Method to process End chat event
    * @param : map - caseid to chatevent
    * @return NA
    * @throws NA
    **/       
    public static void mEndChat(Map<Id,LiveChatTranscriptevent> oCaseChatEvent){  
        List<Case> oCasetoUpdate=new List<Case>();
        List<Case_Owner_Tracking__c> oCaseOwnerTrackLst=new List<Case_Owner_Tracking__c>();    
        for(Case oCase:[select id,Ownerid,recordtype.name,BusinessHoursId,(select id,Agent_Handle_Time__c,Agent_Name__c from 
                        Case_Owner_Trackings__r order by createddate desc limit 1) from Case 
                                //Case_Owner_Trackings__r where Agent_Name__c = : sAgentId order by createddate desc limit 1) from Case 
                                where id in :oCaseChatEvent.keyset()]){   
            if (Label.PS_ServiceRecordTypes.contains(oCase.recordtype.name)){ 
                if (oCase.Case_Owner_Trackings__r != null && !oCase.Case_Owner_Trackings__r.isEmpty()){
                    Case_Owner_Tracking__c oCOT = new Case_Owner_Tracking__c(Id=oCase.Case_Owner_Trackings__r[0].id);
                    Long duration=(BusinessHours.diff(oCase.BusinessHoursId,dChatStartTime,dChatEndTime))/(1000);
                    //11/07/2017 - Rakshik Bhan: Commenting this piece of code because it is adding the chat time to the total agent handle time.
                    //oCOT.Agent_Handle_Time__c=oCase.Case_Owner_Trackings__r[0].Agent_Handle_Time__c+duration;
                    oCaseOwnerTrackLst.add(oCOT);
                }    
                if(oCase.Ownerid != sAgentId && sAgentId != null){
                    oCase.Ownerid =sAgentId;oCasetoUpdate.add(oCase);}                                                      
            }
        }//end for loop
        if (oCasetoUpdate.size()>0 && !Test.isRunningTest())
            update oCasetoUpdate;
        if (oCaseOwnerTrackLst.size()>0 && !Test.isRunningTest())   
            upsert oCaseOwnerTrackLst;         
   } //end method mEndChat    
}