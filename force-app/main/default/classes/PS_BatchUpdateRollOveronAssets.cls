/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_BatchUpdateRollOveronAssets.cls 
Description:     To perform one time data update [RollOver] for Asset. 
CR Info:         CR-00415
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
01/18/2018         1.0            MAYANK LAL                       Initial Design 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
global class PS_BatchUpdateRollOveronAssets implements Database.Batchable<sObject>,Database.stateful {
    Map<Id,Asset> mapOfProdIdFromAsst = null;
    List<RelatedProduct__c> lstprodFamily = null;
    Set<Id> setOfProdFamilyIds = null;
    Map<id,Boolean> mapOfProdAndRollOver = new map<Id,Boolean>();
    List<Asset> lstAssetTobeUpdated = null;
    transient List<Database.SaveResult> lstDBSaveResult = null;
    Map<Id,RelatedProduct__c> mapOfAssetandRelatedProdFam = new Map<Id,RelatedProduct__c>();
    List<RelatedProduct__c> lstWithProdFamily = null;
    transient List<Asset> lstAssetWithOutProdFamily = null;
    transient Set<Id> setofAssetProdIdWithOutProductFamily = null;

   
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(!Test.isRunningTest()) {
        	return Database.getQueryLocator([select id,Rollover2__c,Rollover__c,Product2Id from Asset where Product2Id != null and LastModifiedBy.Id != :UserInfo.getUserId() LIMIT 20000]);    
        }
        else {
            return Database.getQueryLocator([select id,Rollover2__c,Rollover__c,Product2Id from Asset where Product2Id != null  LIMIT 10]);    
        }
        
    }
    
    
    global void execute(Database.BatchableContext BC, List<Asset> lstAssets){
        mapOfProdIdFromAsst = new Map<Id,Asset>();
        lstprodFamily = new List<RelatedProduct__c>();
        setOfProdFamilyIds = new Set<Id>();
        lstAssetTobeUpdated = new List<Asset>();
        lstWithProdFamily = new List<RelatedProduct__c>();
        //lstAssetWithOutProdFamily = new List<Asset>();
        setofAssetProdIdWithOutProductFamily = new Set<Id>();
        
        //Get The product id from Assets
        for(Asset ast: lstAssets){
            mapOfProdIdFromAsst.put(ast.Product2Id,ast);
        }
        
        
        //Get the Product Family
        lstprodFamily = [select id,name,Product__c,RelatedProduct__c from RelatedProduct__c 
                         where  AssociationCategory__c != null and
                         RelatedProduct__c IN :  mapOfProdIdFromAsst.keySet()];
        
        for(RelatedProduct__c relPrds : lstprodFamily) {
            mapOfAssetandRelatedProdFam.put(relPrds.RelatedProduct__c, relPrds);
        }
        
        For(Id asstProdId : mapOfProdIdFromAsst.keySet()) {
            if(mapOfAssetandRelatedProdFam.containsKey(asstProdId)){
                lstWithProdFamily.add(mapOfAssetandRelatedProdFam.get(asstProdId));
            	}
   				else {
                    
                    setofAssetProdIdWithOutProductFamily.add(asstProdId);
                }
            }
                
        
        if(!lstWithProdFamily.isEmpty()) {
            // Get Product Family Id from Products
            for(RelatedProduct__c prodFamily: lstWithProdFamily)
            {
                setOfProdFamilyIds.add(prodFamily.Product__c);
            }
            
            //Get Next Edition Product from Product Family and Check for Relevance value 10 or 30
            List<Product2> lstNxtEditionProducts = [select id,Next_Edition__r.Relevance_Value__c from  product2 
                                                    where Id IN :setOfProdFamilyIds];
            
            for(RelatedProduct__c prodFam: lstWithProdFamily)
            {            
                for(Product2 Prod :lstNxtEditionProducts)
                {            
                    if(prodFam.Product__c == Prod.Id && (Prod.Next_Edition__r.Relevance_Value__c == 10 || Prod.Next_Edition__r.Relevance_Value__c == 30))
                    {                    
                        mapOfProdAndRollOver.put(prodFam.RelatedProduct__c, True);
                    }
                    else if(prodFam.Product__c == Prod.Id && (Prod.Next_Edition__r.Relevance_Value__c != 10 || Prod.Next_Edition__r.Relevance_Value__c != 30))
                    {                    
                        mapOfProdAndRollOver.put(prodFam.RelatedProduct__c, False);
                    }
                }
            }
        }
        
        if(!setofAssetProdIdWithOutProductFamily.isEmpty()) {
            //Get List of Next Edition Products
            List<Product2> lstNextEditionProd = [select id,Next_Edition__r.Relevance_Value__c,Next_Edition__c from  product2 
                                                 Where Id IN :setofAssetProdIdWithOutProductFamily];
            if(!lstNextEditionProd.isEmpty() ) {
                for(Product2 nxtEdiProd :lstNextEditionProd) {
                    //Check for Next Edition and Relevance Value 10/30
                    if((nxtEdiProd.Next_Edition__r.Relevance_Value__c == 10 || nxtEdiProd.Next_Edition__r.Relevance_Value__c == 30) && nxtEdiProd.Next_Edition__c != null) {
                        mapOfProdAndRollOver.put(nxtEdiProd.id, true);
                    }
                    //Check for Next Edition and Relevance Value NOT 10/30
                    else if((nxtEdiProd.Next_Edition__r.Relevance_Value__c != 10 || nxtEdiProd.Next_Edition__r.Relevance_Value__c != 30) && nxtEdiProd.Next_Edition__c != null) {
                        mapOfProdAndRollOver.put(nxtEdiProd.id, false);    
                    }
                    //Check for Next Edition Null
                    else if(nxtEdiProd.Next_Edition__c == null){
                        mapOfProdAndRollOver.put(nxtEdiProd.id, false); 
                    }
                    
                }
            }
        }
            
        
        for(Asset Asst2 : lstAssets)
        {              
            if (mapOfProdAndRollOver.containsKey(Asst2.Product2Id)) 
            {
                if (mapOfProdAndRollOver.get(Asst2.Product2Id))
                {
                    Asst2.Rollover2__c = mapOfProdAndRollOver.get(Asst2.Product2Id);
                    lstAssetTobeUpdated.add(Asst2);
                }  
                else {
                    Asst2.Rollover2__c = mapOfProdAndRollOver.get(Asst2.Product2Id);          
                    lstAssetTobeUpdated.add(Asst2);
                }
            }                                  
        }
        //Updated the Assets
        lstDBSaveResult = Database.Update(lstAssetTobeUpdated,false);  
        List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
        try{            
            for (integer i=0;i<lstAssetTobeUpdated.size();i++){
                Asset asst=lstAssetTobeUpdated[i];
                Database.SaveResult res=lstDBSaveResult[i];
                String ErrMsg='';
                if (!res.isSuccess() || Test.isRunningtest()){
                    PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                    errlogger.InterfaceName__c='StampParentDataOnChildCases';
                    errlogger.ApexClassName__c='PS_StampParentDataOnChildCases';
                    errlogger.CallingMethod__c='finish';
                    errlogger.UserLogin__c=UserInfo.getUserName(); 
                    errlogger.RecordId__c=asst.id;   
                    for(Database.Error err : res.getErrors()) {
                        ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage();
                    }   
                    errlogger.ExceptionMessage__c=ErrMsg;
                    errloggerlist.add(errlogger);                     
                }
            }
            if(errloggerlist.size()>0){insert errloggerlist;}
            
        }
        catch(DMLException e){
            throw(e);
        }
        catch(Exception e){
            ExceptionFramework.LogException('PS_BatchUpdateRollOveronAssets','PS_BatchUpdateRollOveronAssets','execute',e.getMessage(),UserInfo.getUserName(),'');
        }
        
        
        }
        
        global void finish(Database.BatchableContext BC){
            List<Asset> leftOutAsset = [select id,Rollover2__c,Rollover__c,Product2Id from Asset where Product2Id != null and LastModifiedBy.Id != :UserInfo.getUserId() LIMIT 1];
            if(!leftOutAsset.isEmpty()) {
                PS_BatchUpdateRollOveronAssets  updateAssetbatch = new PS_BatchUpdateRollOveronAssets();
				Database.executeBatch(updateAssetbatch);
            }
        }
        }