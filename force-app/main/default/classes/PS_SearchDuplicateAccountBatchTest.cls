@isTest

private class PS_SearchDuplicateAccountBatchTest {
    
        static testmethod void test() {

     User usrrec=[Select Id,Name from User where username like 'deploymentuser@pearsoncore.com%' limit 1 ];
     
     system.runas(usrrec) {
       // Create some test merchandise items to be deleted
       //   by the batch job.
       Account[] accCr = new List<Account>();
       for (Integer i=0;i<10;i++) {
           Account acc = new Account(
               Name='Account Master ' + i,
               Master_Account__c = True);
           
           accCr.add(acc);  
       }
       insert accCr;  
       
       
       Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = accCr[0].Id;
        Test.setFixedSearchResults(fixedSearchResults);
       
       
       
       Account[] accCrCh = new List<Account>();
       for (Integer i=0;i<10;i++) {
           Account accCh = new Account(
               Name='Account Child ' + i);
           accCrCh.add(accCh);
       }
       insert accCrCh;
       
       
        // The query used by the batch job.
       String query = 'SELECT Id,Name FROM Account where Master_Account__c = TRUE';

       Test.startTest();
       PS_SearchDuplicateAccountBatch c = new PS_SearchDuplicateAccountBatch();
       Database.executeBatch(c);
       Test.stopTest();  

       Integer i = [SELECT COUNT() FROM Account where Name like  'Account Child%' ];
       System.assertEquals(i,10);
    }
    }
    
}