/**************************************************************************
 * Class Name   : UKTEPPricingTestDataFactory
 * Author       : Aman Sawhney
 * Date Created : 16 / Nov / 2018
 * Description  : A test factory class for pricing record creation
 * ************************************************************************/
@isTest
public class UKTEPPricingTestDataFactory{

    public static Quote createQuoteWithoutSampleLines(){
        
        //pre-requisites for quote insertion
        Id pricebookId = Test.getStandardPricebookId();
        
        Account testAccount = new Account(Name='Test-Account');
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity(Name='Test-Account',AccountId = testAccount.Id,Pricebook2Id = pricebookId, 
        StageName='Prospecting', CloseDate = System.Today().addDays(50));
        insert testOpportunity;
        
        testOpportunity.Market__c=Label.PS_UKMarket;
        update testOpportunity;
        
        List<Product2> testProductList = new List<Product2>();
        testProductList.add(new Product2(Name='Product1',ProductCode='Product1',IsActive = true,PIHE_Qualification_Year__c='1'));
        testProductList.add(new Product2(Name='Product2',ProductCode='Product2',IsActive = true,PIHE_Qualification_Year__c='1'));
        insert testProductList;
        
        
        List<PricebookEntry> testPriceEntryList = new List<PricebookEntry>();
        testPriceEntryList.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = testProductList[0].Id,UnitPrice = 10000, IsActive = true));
        testPriceEntryList.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = testProductList[1].Id,UnitPrice = 20000, IsActive = true));
        insert testPriceEntryList;
        
        
        //quote insertion
        Quote testQuote = new Quote();
        Id schoolQuoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Schools Quote').getRecordTypeId();
        testQuote.RecordTypeId = schoolQuoteRecordTypeId;
        testQuote.Name = 'test-quote';
        testQuote.Status = Label.Quote_Price_Call_Status;   
        testQuote.OpportunityId = testOpportunity.Id;
        testQuote.Pricebook2Id = pricebookId;
        insert testQuote;
        
        //quote line items insertion
        List<QuoteLineItem> testQuoteLineItemList = new List<QuoteLineItem>();
        
        testQuoteLineItemList.add(new QuoteLineItem(Product2Id = testProductList[0].Id, QuoteId = testQuote.Id, 
        UnitPrice = 1000,PricebookEntryId=testPriceEntryList[0].Id,Quantity=1));
        
        testQuoteLineItemList.add(new QuoteLineItem(Product2Id = testProductList[1].Id, QuoteId = testQuote.Id, 
        UnitPrice = 2000,PricebookEntryId=testPriceEntryList[1].Id,Quantity=1));
        
        insert testQuoteLineItemList;
        
        //custom setting insertion
        General_One_CRM_Settings__c tepPricingCustomSetting = new General_One_CRM_Settings__c(
            Name = 'TEPPricing',
            Description__c = 'TEP Pricing Call',
            value__c = 'https://integration-eu.soa.pearson.com:443/Services/Sales/PricingRequest/v1',
            Category__c = 'SelfSignedCertProd_Feb2018'
        );
        insert tepPricingCustomSetting;
        
        return testQuote;
        
    }
    
    public static Quote createQuoteWithSampleLines(){
        
        //pre-requisites for quote insertion
        Id pricebookId = Test.getStandardPricebookId();
        
        Account testAccount = new Account(Name='Test-Account');
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity(Name='Test-Account',AccountId = testAccount.Id,Pricebook2Id = pricebookId, 
        StageName='Prospecting', CloseDate = System.Today().addDays(50));
        insert testOpportunity;
        
        testOpportunity.Market__c=Label.PS_UKMarket;
        update testOpportunity;
        
        List<Product2> testProductList = new List<Product2>();
        testProductList.add(new Product2(Name='Product1',ProductCode='Product1',IsActive = true,PIHE_Qualification_Year__c='1'));
        testProductList.add(new Product2(Name='Product2',ProductCode='Product2',IsActive = true,PIHE_Qualification_Year__c='1'));
        insert testProductList;
        
        
        List<PricebookEntry> testPriceEntryList = new List<PricebookEntry>();
        testPriceEntryList.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = testProductList[0].Id,UnitPrice = 10000, IsActive = true));
        testPriceEntryList.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = testProductList[1].Id,UnitPrice = 20000, IsActive = true));
        insert testPriceEntryList;
        
        
        //quote insertion
        Quote testQuote = new Quote();
        Id schoolQuoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Schools Quote').getRecordTypeId();
        testQuote.RecordTypeId = schoolQuoteRecordTypeId;
        testQuote.Name = 'test-quote';
        testQuote.Status = Label.Quote_Price_Call_Status;   
        testQuote.OpportunityId = testOpportunity.Id;
        testQuote.Pricebook2Id = pricebookId;
        insert testQuote;
        
        //quote line items insertion
        List<QuoteLineItem> testQuoteLineItemList = new List<QuoteLineItem>();
        
        testQuoteLineItemList.add(new QuoteLineItem(Product2Id = testProductList[0].Id, QuoteId = testQuote.Id, 
        UnitPrice = 1000,PricebookEntryId=testPriceEntryList[0].Id,Quantity=1,Sample__c=true));
        
        testQuoteLineItemList.add(new QuoteLineItem(Product2Id = testProductList[1].Id, QuoteId = testQuote.Id, 
        UnitPrice = 2000,PricebookEntryId=testPriceEntryList[1].Id,Quantity=1,Sample__c=true));
        
        insert testQuoteLineItemList;
        
        //custom setting insertion
        General_One_CRM_Settings__c tepPricingCustomSetting = new General_One_CRM_Settings__c(
            Name = 'TEPPricing',
            Description__c = 'TEP Pricing Call',
            value__c = 'https://integration-eu.soa.pearson.com:443/Services/Sales/PricingRequest/v1',
            Category__c = 'SelfSignedCertProd_Feb2018'
        );
        insert tepPricingCustomSetting;
        
        return testQuote;
        
    }
    
    public static Quote createQuoteWithMixSampleLines(){
        
        //pre-requisites for quote insertion
        Id pricebookId = Test.getStandardPricebookId();
        
        Account testAccount = new Account(Name='Test-Account');
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity(Name='Test-Account',AccountId = testAccount.Id,Pricebook2Id = pricebookId, 
        StageName='Prospecting', CloseDate = System.Today().addDays(50));
        insert testOpportunity;
        
        testOpportunity.Market__c=Label.PS_UKMarket;
        update testOpportunity;
        
        List<Product2> testProductList = new List<Product2>();
        testProductList.add(new Product2(Name='Product1',ProductCode='Product1',IsActive = true,PIHE_Qualification_Year__c='1'));
        testProductList.add(new Product2(Name='Product2',ProductCode='Product2',IsActive = true,PIHE_Qualification_Year__c='1'));
        testProductList.add(new Product2(Name='Product2',ProductCode='Product3',IsActive = true,PIHE_Qualification_Year__c='1'));
        testProductList.add(new Product2(Name='Product2',ProductCode='Product4',IsActive = true,PIHE_Qualification_Year__c='1'));
        insert testProductList;
        
        
        List<PricebookEntry> testPriceEntryList = new List<PricebookEntry>();
        testPriceEntryList.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = testProductList[0].Id,UnitPrice = 10000, IsActive = true));
        testPriceEntryList.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = testProductList[1].Id,UnitPrice = 20000, IsActive = true));
        testPriceEntryList.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = testProductList[2].Id,UnitPrice = 30000, IsActive = true));
        testPriceEntryList.add(new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = testProductList[3].Id,UnitPrice = 40000, IsActive = true));
        insert testPriceEntryList;
        
        
        //quote insertion
        Quote testQuote = new Quote();
        Id schoolQuoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Schools Quote').getRecordTypeId();
        testQuote.RecordTypeId = schoolQuoteRecordTypeId;
        testQuote.Name = 'test-quote';
        testQuote.Status = Label.Quote_Price_Call_Status;   
        testQuote.OpportunityId = testOpportunity.Id;
        testQuote.Pricebook2Id = pricebookId;
        insert testQuote;
        
        //quote line items insertion
        List<QuoteLineItem> testQuoteLineItemList = new List<QuoteLineItem>();
        
        testQuoteLineItemList.add(new QuoteLineItem(Product2Id = testProductList[0].Id, QuoteId = testQuote.Id, 
        UnitPrice = 1000,PricebookEntryId=testPriceEntryList[0].Id,Quantity=1,Sample__c=true));
        
        testQuoteLineItemList.add(new QuoteLineItem(Product2Id = testProductList[1].Id, QuoteId = testQuote.Id, 
        UnitPrice = 2000,PricebookEntryId=testPriceEntryList[1].Id,Quantity=1,Sample__c=true));
        
        testQuoteLineItemList.add(new QuoteLineItem(Product2Id = testProductList[1].Id, QuoteId = testQuote.Id, 
        UnitPrice = 2000,PricebookEntryId=testPriceEntryList[2].Id,Quantity=1,Sample__c=false));
        
        testQuoteLineItemList.add(new QuoteLineItem(Product2Id = testProductList[1].Id, QuoteId = testQuote.Id, 
        UnitPrice = 2000,PricebookEntryId=testPriceEntryList[3].Id,Quantity=1,Sample__c=false));
        
        insert testQuoteLineItemList;
        
        //custom setting insertion
        General_One_CRM_Settings__c tepPricingCustomSetting = new General_One_CRM_Settings__c(
            Name = 'TEPPricing',
            Description__c = 'TEP Pricing Call',
            value__c = 'https://integration-eu.soa.pearson.com:443/Services/Sales/PricingRequest/v1',
            Category__c = 'SelfSignedCertProd_Feb2018'
        );
        insert tepPricingCustomSetting;
        
        return testQuote;
        
    }
    
    public static Quote createQuoteWithoutLines(){
        
        //pre-requisites for quote insertion
        Id pricebookId = Test.getStandardPricebookId();
        
        Account testAccount = new Account(Name='Test-Account');
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity(Name='Test-Account',AccountId = testAccount.Id,Pricebook2Id = pricebookId, 
        StageName='Prospecting', CloseDate = System.Today().addDays(50));
        insert testOpportunity;
        
        testOpportunity.Market__c=Label.PS_UKMarket;
        update testOpportunity;
        
        //quote insertion
        Quote testQuote = new Quote();
        Id schoolQuoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Schools Quote').getRecordTypeId();
        testQuote.RecordTypeId = schoolQuoteRecordTypeId;
        testQuote.Name = 'test-quote';
        testQuote.Status = Label.Quote_Price_Call_Status;   
        testQuote.OpportunityId = testOpportunity.Id;
        testQuote.Pricebook2Id = pricebookId;
        insert testQuote;
        
        //custom setting insertion
        General_One_CRM_Settings__c tepPricingCustomSetting = new General_One_CRM_Settings__c(
            Name = 'TEPPricing',
            Description__c = 'TEP Pricing Call',
            value__c = 'https://integration-eu.soa.pearson.com:443/Services/Sales/PricingRequest/v1',
            Category__c = 'SelfSignedCertProd_Feb2018'
        );
        insert tepPricingCustomSetting;
        
        return testQuote;
        
    }
    
}