/*************************************************************************************************************
    * Name        : updateIRBasedonSR
    * Description : Class to update Integration Request record based on system response CR#01200
    * Input       : List of system response records
    * Date created: 06/08/2017
    * Author      : Jaydip B
    *************************************************************************************************************/
    public class updateIRBasedonSR {
    /*************************************************************************************************************
    * Name        : SetStatusBasedOnSystemResponseSA
    * Description : Method update Integration Request record based on system response CR#01200
    * Input       : List of system response records
    * Date created: 06/08/2017 
    * Author      : Jaydip B
    *************************************************************************************************************/
    public static void SetStatusBasedOnSystemResponseSA (List<System_Response__c> sysRespList)
    
    {
    
        LIST<ID> IntRqIDsList=new LIST<ID>();
       
        MAP<string,LIST<string>> mapIRSystems=new MAP<string,LIST<string>>();
        MAP<string,string> mapIRSystemStatus=new MAP<string,string>();
        for(System_Response__c sysResp: sysRespList) {
            IntRqIDsList.add(sysResp.Integration_Request__c);
        
        
        } 
    
       
       for(Integration_Request__c IRRec:[select ID,Event__c, Status__c from Integration_Request__c where Market__c ='ZA' and ID in :IntRqIDsList]) {
           if(CS_Response_IR__c.getInstance(IRRec.Event__c) !=null) {
           LIST<string>lstResps=new LIST<string>();
           lstResps=CS_Response_IR__c.getInstance(IRRec.Event__c).Systems__c.split(';');
           mapIRSystems.put(IRRec.Id,lstResps);
           
           }
       
       }
       
       for (System_Response__c sysResp:[Select Id, External_System__c, Error_Code__c, Error_Message__c, Status__c, Integration_Request__c from System_Response__c where Integration_Request__c in :IntRqIDsList order by createddate]) {
           mapIRSystemStatus.put(sysResp.Integration_Request__c+'-'+sysResp.External_System__c,sysResp.Status__c);
           
       
       }
       
       LIST<Integration_Request__c> IRReqUpdLst=new LIST<Integration_Request__c>();
       for (Integration_Request__c IRRec:[select ID,Event__c, Status__c from Integration_Request__c where Market__c ='ZA' and ID in :IntRqIDsList]) {
           boolean changed=false;
           boolean notAllComp=false;
           if(mapIRSystems.containsKey(IRRec.Id)) {
               string oldStatus='';
               for(string strSystem:mapIRSystems.get(IRRec.Id)){
                   //below if statement will prevent to update IS to completed, if at least one SR has error
                   string strStatus=mapIRSystemStatus.get(IRRec.Id+'-'+strSystem);
                   if(strStatus!=null){
                       if(!(strStatus=='Completed'  && oldStatus !='' && IRRec.Status__c !='Completed')){
                           IRRec.Status__c=strStatus;
                           changed=true;
                          }
                        } 
                   else {
                      //if one response did not come yet, no need to change status
                      notAllComp=true;
                   
                   }      
                  oldStatus=strStatus;      
                 }
           
               if(changed==true && notAllComp==false) {
                   IRReqUpdLst.add(IRRec);
               }
           } 
    
        }
    
    if(IRReqUpdLst.size()>0) {
    
        update IRReqUpdLst;
    
    }    
        
  }
  
  }