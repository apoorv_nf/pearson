@isTest(SeeAllData = false)
public class AccountAddressesUpdateonAccountTest {
    
    public static testmethod void unitTest(){
        Set<id> accids = new Set<id>();
        account acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'CA', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
        insert acc;
        accids.add(acc.Id);
        
        List<Account_Addresses__c> addtoInsert = new List<Account_Addresses__c>();
        Account_Addresses__c accadd = new Account_Addresses__c(Name='Test',Account__c=acc.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Billing__c=true, Primary__c =true);
        //insert accadd;
        addtoInsert.add(accadd);
        Account_Addresses__c accadd1 = new Account_Addresses__c(Name='Test1',Account__c=acc.id,Address_1__c='Test address1',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Shipping__c=true,PrimaryShipping__c=true);
        addtoInsert.add(accadd1);
        
        Account_Addresses__c accadd2 = new Account_Addresses__c(Name='Testad2',Account__c=acc.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'United Kingdom', State__c = 'London', City__c = 'London', Zip_Code__c = 'OX16 9HY',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Shipping__c=true,PrimaryShipping__c=true,Billing__c=true,Primary__c=true);
        addtoInsert.add(accadd2);
                
        //insert accadd1;
        
        insert addtoInsert;
        
        /*Account accobj = new Account();
        accobj.id = acc.Id;
        accobj.Billing_Address_Lookup__c = accadd.id;
        accobj.Shipping_Address_Lookup__c = accadd1.Id;
        update accobj;*/
        
        account acc12 = new Account(Name='Test Account2012', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371',Shipping_Address_Lookup__c=accadd2.id);
        insert acc12;
        accids.add(acc12.Id);

        
        AccountAddressesUpdateonAccount.afterInsertOrUpdateAccountAddresses(accids);
        //acad.pbillAccAddresses = accadd.Id;
        //acad.pshipAccAddresses = accadd1.Id;
    }
    
    public static testmethod void unitTest1(){
        Set<id> accids = new Set<id>();
        account acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'IN', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
        insert acc;
        accids.add(acc.Id);
        
        List<Account_Addresses__c> addtoInsert = new List<Account_Addresses__c>();
        Account_Addresses__c accadd = new Account_Addresses__c(Name='Test',Account__c=acc.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Billing__c=true);
        //insert accadd;
        addtoInsert.add(accadd);
        Account_Addresses__c accadd1 = new Account_Addresses__c(Name='Test1',Account__c=acc.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Billing__c=true);
        //insert accadd1;
        addtoInsert.add(accadd1);
        Account_Addresses__c accadd2 = new Account_Addresses__c(Name='Test2',Account__c=acc.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Billing__c=true);
        addtoInsert.add(accadd2);
        Account_Addresses__c accadd3 = new Account_Addresses__c(Name='Test3',Account__c=acc.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Billing__c=true);
        addtoInsert.add(accadd3);
        Account_Addresses__c accadd4 = new Account_Addresses__c(Name='Test4',Account__c=acc.id,Address_1__c='Test address1',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Shipping__c=true,PrimaryShipping__c=true);
        addtoInsert.add(accadd4);
        insert addtoInsert;
        
        AccountAddressesUpdateonAccount.afterInsertOrUpdateAccountAddresses(accids);
        
    }
    
    public static testmethod void unitTest8(){
        Set<id> accids = new Set<id>();
        account acc = new Account(Name='Test Account20', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
        insert acc;
        accids.add(acc.Id);
        
        List<Account_Addresses__c> addtoInsert = new List<Account_Addresses__c>();
        
        Account_Addresses__c accadd0 = new Account_Addresses__c(Name='Test',Account__c=acc.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'UK', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',ERP_DEF_BILL_Site_Use_ID__c='1437664',TEP_Operating_Unit__c='CA Pearson OU',ERP_Billing_Site_Use_ID__c='1437664',Shipping__c=true);
        //insert accadd;
        addtoInsert.add(accadd0);
        
        Account_Addresses__c accadd1 = new Account_Addresses__c(Name='Testadd1',Account__c=acc.id,Address_1__c='Test ad 123',Source__c='One CRM',Country__c = 'UK', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',ERP_DEF_BILL_Site_Use_ID__c='1437664',TEP_Operating_Unit__c='CA Pearson OU',ERP_Billing_Site_Use_ID__c='1437664',Billing__c=true);
        //insert accadd;
        addtoInsert.add(accadd1);
        Account_Addresses__c accadd2 = new Account_Addresses__c(Name='Testadd2',Account__c=acc.id,Address_1__c='Test ad 123',Source__c='One CRM',Country__c = 'UK', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',ERP_DEF_BILL_Site_Use_ID__c='1437664',TEP_Operating_Unit__c='CA Pearson OU',ERP_Billing_Site_Use_ID__c='1437664',Primary_Physical__c=true);
        //insert accadd;
        addtoInsert.add(accadd2);
        insert addtoInsert;
          
        AccountAddressesUpdateonAccount.afterInsertOrUpdateAccountAddresses(accids);
                
        /*
        List<Account_Addresses__c> addacctoInsert = new List<Account_Addresses__c>();
                
        Account_Addresses__c accttadd1 = new Account_Addresses__c(Name='Testad1',Account__c=acc.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'United Kingdom', State__c = 'London', City__c = 'London', Zip_Code__c = 'OX16 9HY',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Shipping__c=false,PrimaryShipping__c=false,Billing__c=true,Primary__c=true);
        addacctoInsert.add(accttadd1);
  
        Account_Addresses__c accttadd2 = new Account_Addresses__c(Name='Testad2',Account__c=acc.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'United Kingdom', State__c = 'London', City__c = 'London', Zip_Code__c = 'OX16 9HY',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Shipping__c=true,PrimaryShipping__c=true,Billing__c=false,Primary__c=false);
        addacctoInsert.add(accttadd2);

        insert addacctoInsert;

        Account_Addresses__c accttadd2 = new Account_Addresses__c(Name='Testad2',Account__c=acc.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'United Kingdom', State__c = 'London', City__c = 'London', Zip_Code__c = 'OX16 9HY',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Shipping__c=true,PrimaryShipping__c=true,Billing__c=true,Primary__c=true);
        
        insert accttadd2;
        accttadd2.Billing__c=false;
        accttadd2.Primary__c=false;
        update accttadd2;
        
        //Set<id> accids12 = new Set<id>();
        account acc12 = new Account(Name='Test Account2012', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371',Shipping_Address_Lookup__c=accttadd2.id);
        insert acc12;
        accids.add(acc12.Id);
        */        
        
    }       
    
    // Start - Vinoth
    public static testmethod void unitTest4(){
        Set<id> accids = new Set<id>();
        
        account acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'IN', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
        insert acc;
        accids.add(acc.Id);
        
        List<Account_Addresses__c> addtoInsert = new List<Account_Addresses__c>();
        Account_Addresses__c accadd = new Account_Addresses__c(Name='Test',Account__c=acc.id,Address_1__c='8416 7 Street SW1',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Billing__c=true,Primary__c=true,Physical__c=true,Primary_Physical__c=true,Shipping__c=true,PrimaryShipping__c=true);
        //insert accadd;
        addtoInsert.add(accadd);
        
        Account_Addresses__c accadd1 = new Account_Addresses__c(Name='Test1',Account__c=acc.id,Address_1__c='8416 7 Street SW2',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Billing__c=true,Primary__c=true, Physical__c=true,Primary_Physical__c=true);
        //insert accadd1;
        addtoInsert.add(accadd1);
        
        Account_Addresses__c accadd2 = new Account_Addresses__c(Name='Test2',Account__c=acc.id,Address_1__c='8416 7 Street SW3',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Shipping__c=true,PrimaryShipping__c=true,Physical__c=true,Primary_Physical__c=true);
        addtoInsert.add(accadd2);
        
        Account_Addresses__c accadd3 = new Account_Addresses__c(Name='Test3',Account__c=acc.id,Address_1__c='8416 7 Street SW4',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Physical__c=true,Primary_Physical__c=true);
        addtoInsert.add(accadd3);
        
        Account_Addresses__c accadd4 = new Account_Addresses__c(Name='Test4',Account__c=acc.id,Address_1__c='Test address15',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Shipping__c=true,PrimaryShipping__c=true,Billing__c=true,Primary__c=true);
        addtoInsert.add(accadd4);
        
        insert addtoInsert;
                
        accadd.Primary__c = false;
        accadd.PrimaryShipping__c = false;
        accadd.Primary_Physical__c = false;
        update accadd;
        
        AccountAddressesUpdate.beforeInsertOrUpdateAccountAddresses(addtoInsert);
        //AccountAddressesUpdateonAccount.afterInsertOrUpdateAccountAddresses(accids);
        
    }
    
    // End - Vinoth
    
    public static testmethod void unitTest2(){
       // Set<id> accids = new Set<id>();
        List<Account> lstacctoinsert = new List<Account>();
        account acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'IN', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
        lstacctoinsert.add(acc);
        account acc1 = new Account(Name='Test Account1', Primary_Selling_Account__c=acc.id,IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'IN', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingState = 'Karnataka', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
        lstacctoinsert.add(acc1);
        insert lstacctoinsert;
        //insert acc;
        //accids.add(acc.Id);
        
        List<Account_Addresses__c> addtoInsert = new List<Account_Addresses__c>();
        Account_Addresses__c accadd = new Account_Addresses__c(Name='Test',Account__c=acc.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Billing__c=true,Primary__c =true,PrimaryShipping__c=false);
        addtoInsert.add(accadd);
        //insert accadd;
        Account_Addresses__c accadd1 = new Account_Addresses__c(Name='Test1',Account__c=acc.id,Address_1__c='Test address1',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Shipping__c=true,Primary__c =false,PrimaryShipping__c=true);
        addtoInsert.add(accadd1);
        insert addtoInsert;
        
        List<Account_Addresses__c> addtoUpdate = new List<Account_Addresses__c>();
        Account_Addresses__c accadd3 = new Account_Addresses__c(Name='Test3',Account__c=acc1.id,Address_1__c='Test--address3',Source__c='One CRM',Country__c = 'Canada', State__c = 'Test1', City__c = 'Test1', Zip_Code__c = '674839',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='8765432',ERP_Billing_Site_Use_ID__c='8765432',Shipping__c=true,Primary__c =false,PrimaryShipping__c=true);
        addtoUpdate.add(accadd3);
        insert addtoUpdate;
        
        // Added By Navaneeth - Start
        
        List<Account_Addresses__c> addtoInsert1 = new List<Account_Addresses__c>();
        Account_Addresses__c accadd4 = new Account_Addresses__c(Name='Test4',Account__c=acc.id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'United States', State__c = 'Kansas', City__c = 'Florida', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437665',ERP_Billing_Site_Use_ID__c='1437662',Billing__c=true,Primary__c =true,PrimaryShipping__c=false);
        addtoInsert1.add(accadd4);
        //insert accadd;
        Account_Addresses__c accadd5 = new Account_Addresses__c(Name='Test5',Account__c=acc1.id,Address_1__c='Test address1',Source__c='One CRM',Country__c = 'United States', State__c = 'Kansas', City__c = 'Florida', Zip_Code__c = 'T2V 1G7',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='14376656',ERP_Billing_Site_Use_ID__c='1437698',Shipping__c=true,Primary__c =false,PrimaryShipping__c=true);
        addtoInsert1.add(accadd5);
        insert addtoInsert1;
        
        AccountAddressesUpdate.beforeInsertOrUpdateAccountAddresses(addtoInsert1);
        
        // Added By Navaneeth - End
        
        //AccountAddressesUpdateonAccount.afterInsertOrUpdateAccountAddresses(accids);

    }
}