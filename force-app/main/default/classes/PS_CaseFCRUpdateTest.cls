/************************************************************************************************************
* Apex Interface Name : PS_CaseFCRUpdateTest
* Version             : 1.1 
* Created Date        : 29/1/2016 2:44 
* Modification Log    : 
* Test Description    : Test class created for class PS_CaseFCRUpdate
* Expected Inputs     : Case Fcr field should get checked when status of case is closed
* Expected Outputs    : case fcr field is set to true
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Hasi Chakravarty         29/1/2016
* Ajay Chakradhar          03/3/2016             
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_CaseFCRUpdateTest{   //To Check FCR field in Case Object whether FCR is checked when case is closed without having change in ownership. 
    /*
     * Method to cover scenario where case fcr is checked when case gets closed.
     */
    static testMethod void validatePS_CaseFCRUpdate(){
         //user creation
         List<User> listWithUser = new List<User>();
         listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,2);
         insert listWithUser ;
         
         //contact creation
        List<Contact> conlist=TestDataFactory.createContact(1);
        insert conlist;
         
         //case creation
         List<case> cases = TestDataFactory.createCase(1,'School Assessment');
        cases[0].contactid=conlist[0].id;
        cases[0].accountid=conlist[0].accountid;
        //cases[0].ownerid=listWithUser[0].id;
        //system.runas(listWithUser[0]){
        Test.startTest();
         insert cases;
         //cases[0].ownerid=listWithUser[1].id;
         
         List<Case_Owner_Tracking__c> oCOT = new List<Case_Owner_Tracking__c>([select id from Case_Owner_Tracking__c
            where case__c =: cases[0].id]);
        
        cases[0].status=Label.PS_CaseClosureStatus;
        update cases[0];
        Case ca=[select id,fcr__c from case where id=:cases[0].id];
        Test.StopTest();
        //system.assert(ca.fcr__c==true,'FCR is not set');
    }
    /*
     * Method to cover negative scenario 
     */
    static testMethod void Negative_test_validatePS_CaseFCRUpdate(){
        //user creation
         List<User> listWithUser = new List<User>();
         listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,1);
         insert listWithUser ;
         
         //contact creation
        List<Contact> conlist=TestDataFactory.createContact(1);
        insert conlist;
         
         //case creation
         List<case> cases = TestDataFactory.createCase(1,'School Assessment');
        cases[0].contactid=conlist[0].id;
        cases[0].accountid=conlist[0].accountid;
        cases[0].ownerid=listWithUser[0].id;
        //system.runas(listWithUser[0]){
        Test.startTest();
         insert cases;
        Case ca=[select id,ownerid,status,fcr__c from case where id=:cases[0].id];
         update ca;
        Test.StopTest();
        //system.assert(ca.fcr__c!=true,'FCR is set');
        }
}