/************************************************************************************************************
* Apex Interface Name : emailHelper
* Version             : 1.0 
* Created Date        : 4/3/2017
* Modification Log    : 
* Developer              CP                  
* -----------------------------------------------------------------------------------------------------------
* Cristina Perez        4/3/2017      Class to send email without to user, specialy to support catching errors     
*   
************************************************************************************************************/
global class emailHelper {
  public static void sendEmailToUser(String emailBody, ID contactID, ID OrgWideEmail) {
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setUseSignature(false);
        message.setBccSender(false);
        //message.setOrgWideEmailAddressId(OrgWideEmail);
        message.setSaveAsActivity(false);
        message.setPlainTextBody(emailBody);
      
        ID UserID = UserInfo.getUserID();
        message.setTargetObjectId(UserID);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { message });
            
 
        
    }

}