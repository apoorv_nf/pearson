/************************************************************************************************************
* Apex Interface Name : AssetTriggerHandlerTest  
* Version             : 1.0     
* Created Date        : 9/9/2015 4:44 
* Modification Log    : Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 27,51) 
* Developer                   Date                  Summary Of Changes
* -----------------------------------------------------------------------------------------------------------
* Abhinav Srivastav         9/9/2015              R2.1 version
* Madhu  Rao                   29/10/2015            Added another method of AssetTriggerHandler class for code coverage.
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

@isTest
private Class AssetTriggerHandlerTest { // Test Class to cover AssetTriggerHandler Class
    static testMethod void myTestmthd2() { // Test Method to cover AssetTestHandler Class
         List<User> listWithUser = new List<User>();
         listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
         Map<Id,Asset> newMapAssets;
         Map<Id,Asset> oldMapAssets;
         Id astId;
         List<Asset> astlist = new List<Asset>();
         List<Asset> upastlist = new List<Asset>();
         if(listWithUser.size() > 0)
         {
            for(User newUser : listWithUser)
            {
                newUser.Product_Business_Unit__c = 'CTIPIHE';
            }
         } 
         Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Triggers__c=true);
         insert byp;
         //Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Triggers__c=true);
         //insert byp;
         System.runAs(listWithUser[0]){
        
        Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School',Market2__c='US',Phone = '9989887687');
        acc.ShippingStreet = 'TestStreet';
        acc.ShippingCity = 'Vns';
        acc.ShippingState = 'Delhi';
        acc.ShippingPostalCode = '234543';
        acc.ShippingCountry = 'India';
        acc.IsCreatedFromLead__c = True;
        insert acc;
        Contact con = new Contact(FirstName='FirstN', LastName='LastN',AccountId=acc.Id ,Salutation='MR.', Email='con@gmail.com',Phone='111222333',
                                  Preferred_Address__c = 'Mailing Address', MailingCountry = 'United Kingdom', MailingStreet = '1 Street', MailingPostalCode = 'NE27 0QQ', MailingCity  = 'City');
        insert con;
        Opportunity ro = new Opportunity(Name= 'OpTest11', AccountId = acc.id, StageName = 'Solutioning', Type = 'New Business', Academic_Vetting_Status__c = 'Un-Vetted', Academic_Start_Date__c = System.Today(),CloseDate = System.Today(),International_Student__c = true);
        insert ro;            
        OpportunityContactRole oc = new OpportunityContactRole();
        oc.opportunityId =  ro.Id;            
        oc.contactId = con.Id;
        oc.Role =  'Business User';
        oc.IsPrimary = True;             
        insert oc; 
        ID standardPB = Test.getStandardPricebookId(); //[select id from Pricebook2 where isStandard=true limit 1];
        Product2 pr1 = new Product2(Name = 'Bundle Product', IsActive = true, Qualification_Name__c = 'Test Bundle',Campus__c='Durbanville',Qualification_Level_Name__c= 2,Business_Unit__c = 'CTIPIHE',Market__c = listWithUser[0].Market__c,Line_of_Business__c='Higher Ed');
        insert pr1;
        UniversityCourse__c course=new UniversityCourse__c(Name='T0001-abhi',Catalog_Code__c='T0001',Course_Name__c='abhi',Account__c= acc.Id);
        insert course;
        Asset asst2= new Asset(Product2id=pr1.id,status__c='Active',ContactId=con.Id,Primary__c = true,Name='Test Assest2',AccountId= acc.Id, Opportunity__c=ro.id,Course__c=course.id);
        insert asst2;
        astlist.add(asst2);
        Asset asst= new Asset(Product2id=pr1.id,status__c='Active',Primary__c = true,Name='Bundle Product',AccountId= acc.Id, Opportunity__c=ro.id,Course__c=course.id);
        insert asst;
        System.debug('asset chk'+astId);
        astlist.add(asst);

         if(!astlist.isEmpty()){
        System.debug('check list4' +astlist);
           try{
            insert astlist;
            }catch(System.DMLException de){
            System.debug('check list5' +astlist);
        }
        }
        
        Test.StartTest();

        AssetTriggerHandler asThandler = new AssetTriggerHandler();
         Map<Id,Asset> oldasMap = new Map<Id,Asset>();
             oldasMap.put(asst.Id,asst);
        asThandler.primaryContactv2in(asst2);
         Map<Id,Asset> asMap = new Map<Id,Asset>();
             asMap.put(asst.Id,asst);     
         asThandler.primaryContactv2(astlist,asMap);  
            AssetTriggerHandler.BeforeEventValidation(asMap,oldasMap,'Update');  
        
        Test.StopTest();
        }
        }
}