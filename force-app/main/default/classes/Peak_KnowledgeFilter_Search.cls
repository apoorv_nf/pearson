/**
 * Created by 7Summits on 8/3/17.
 */

public with sharing class Peak_KnowledgeFilter_Search {

    public static Peak_KnowledgeFilter_SearchResults doSearch(String searchTerm, List<String> topicFilterOne, List<String> topicFilterTwo, List<String> topicFilterThree, List<String> topicFilterFour,  String orderByField, String orderByDirection, String UkBVName){
        String netwrkId = System.Network.getNetworkId();

        // prepare query data
        String objectType = 'Customer_Support__kav';
        String fields = 'KnowledgeArticleId, Title, ArticleTotalViewCount, LastModifiedDate, Summary';
        String withClause = '';
        String limitClause = 'LIMIT 1000';
        String offsetClause = '';
        
        // create where clause
        String whereClause = 'WHERE PublishStatus=\'online\' AND Language = \'en_US\' AND IsLatestVersion=true';
        
        if(!topicFilterOne.isEmpty()){
            List<Id> topicFilterOneIds = getTopicAssignments(topicFilterOne);
            whereClause += ' AND Id IN :topicFilterOneIds';

            if(!topicFilterTwo.isEmpty()){
                List<Id> topicFilterTwoIds = getTopicAssignments(topicFilterTwo);
                whereClause += ' AND Id IN :topicFilterTwoIds';
            }

            if(!topicFilterThree.isEmpty()){
                List<Id> topicFilterThreeIds = getTopicAssignments(topicFilterThree);
                whereClause += ' AND Id IN :topicFilterThreeIds';
            }

            if(!topicFilterFour.isEmpty()){
                List<Id> topicFilterFourIds = getTopicAssignments(topicFilterFour);
                whereClause += ' AND Id IN :topicFilterFourIds';
            }

        }
        //UK Community-Start
        if (UkBVName != '') {
            List<Id> topicFilterNameIds = getTopicAssignmentsFromName(UkBVName);
            whereClause += ' AND Id IN :topicFilterNameIds ';
            system.debug('topicFilterNameIds::::'+topicFilterNameIds);
        }
        //UK Community-End

        // create order by field
        String orderBy = (String.isNotEmpty(orderByField)) ? ('ORDER BY ' + orderByField) : 'ORDER BY ArticleTotalViewCount';
        orderBy += (String.isNotEmpty(orderByDirection)) ? (' ' + orderByDirection) : ' DESC';

        // create results object that will be returned
        Peak_KnowledgeFilter_SearchResults results;

        // Check if there is a searchTerm, if there is we need to use SOSL instead of SOQL
        if (String.isNotEmpty(searchTerm.trim())) {
            String searchQuery = String.format('FIND \'\'{0}\'\' RETURNING {1}({2} {3} {7} {5} {6})  {4}', new String[] {searchTerm, objectType, fields, whereClause, withClause, limitClause, offsetClause, orderBy});
            System.debug('Peak_KnowledgeFilterSearch SOSL Query: ' + searchQuery);

            // execute search
            Search.SearchResults searchResults = Search.find(searchQuery);

            // get the results
            List<Search.SearchResult> articlelist = searchResults.get(objectType);
            System.debug('Peak_KnowledgeFilterSearch SOSL Query: -------------------> RESULTS:' + articlelist);
            results = new Peak_KnowledgeFilter_SearchResults(articlelist);
        }

        // Execute SOQL query
        else {
            String searchQuery = String.format('SELECT {0} FROM {1} {2} {3} {6} {4} {5}', new String[] {fields, objectType, whereClause, withClause, limitClause, offsetClause, orderBy});
            System.debug('Peak_KnowledgeFilterSearch SOQL Query: ' + searchQuery);

            // execute search
            List<Customer_Support__kav> dbResults = Database.query(searchQuery);
            System.debug('Peak_KnowledgeFilterSearch SOQL Query: -------------------> RESULTS:' + dbResults);

            results = new Peak_KnowledgeFilter_SearchResults(dbResults);
        }

        return results;

    }

    public static List<Id> getTopicAssignments(List<String> topicIds) {
        List<TopicAssignment> topicAssignments = [SELECT EntityId FROM TopicAssignment WHERE TopicId IN :topicIds LIMIT 1000];

        List<Id> results = new List<Id>();

        for (TopicAssignment t : topicAssignments) {
            results.add(t.EntityId);
        }

        return results;
    }
    
    //UK Community- Start
    public static List<Id> getTopicAssignmentsFromName (String topicName) {
        List<id> iDList = new List<Id>();
        for (topic temp: [select id from topic where name = :topicName limit 10]) {
            iDList.add(temp.Id);
        }
        List<TopicAssignment> topicAssignments = [SELECT EntityId FROM TopicAssignment WHERE TopicId IN :iDList limit 1000];

        List<Id> results = new List<Id>();

        for (TopicAssignment t : topicAssignments) {
            results.add(t.EntityId);
        }

        return results;
    }
    //UK Community- End


}