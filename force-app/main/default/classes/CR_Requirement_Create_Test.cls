/************************************************************************************************************
* Apex Interface Name : CR_Requirement_Create 
* Version             : 1.0 
* Created Date        : 10/13/2017
* Modification Log    : 
* Developer                   Date                  Summary Of Changes
* -----------------------------------------------------------------------------------------------------------
* Cristina Perez                10/13/2017          Test class for CR_Requirement create button for requirements
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

@isTest
public class CR_Requirement_Create_Test
{
    public static testMethod void test_Data()
    {
        test.startTest();
        
        System.Debug('Debugging...');
        
        OneCRM_Change_Request__c testCR = new OneCRM_Change_Request__c();
        //testCR.Status__c='Approved for Development';
        testCR.Status__c='New';
        testCR.Description__c='Creating dummy CR';
        testCR.Request_Phase__c='';

        insert testCR;
        
        CR_Status_New_Approved__c crStatus = new CR_Status_New_Approved__c();
        crStatus.Name='Approved for Development';
        crStatus.Picklist_Values__c='Approved for Development';
        insert crStatus;    
         
        CR_Requirements__c requirementnew = new CR_Requirements__c();
        requirementnew.Jira_Integration__c ='';
        requirementnew.OneCRM_Change_Request__c =testCR.id;
         insert requirementnew;
        
        PageReference crPage = Page.CR_Requirement_Create;
        //crPage.getParameters().put('Id', testCR.Id);
        //Test.setCurrentPage(crPage);
        Test.setCurrentPage(crPage);
         ApexPages.currentPage().getParameters().put('Id',testCR.id);
        CR_Requirement_Create crController = new CR_Requirement_Create();
        crController.createRequirement();
        testCR.Status__c='Approved for Development';
        update testCR;
        crController.createRequirement();       
        test.stopTest();
    }
}