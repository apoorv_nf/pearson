@isTest
public class POC_findDuplicateLicenseTracker_Test {
    @istest static void InsertDuplicateLicenseTracker(){
        User u1 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = UserInfo.getProfileId(), 
                               Sample_Approval__c= true,Order_Approver__c= UserInfo.getUserId(),
                               TimeZoneSidKey='America/Los_Angeles', UserName='test1234'+Math.random()+'@pearson.com.testbau', Geography__c = 'Growth',
                               Market__c = 'US',Line_of_Business__c = 'Higher Ed', License_Pools__c = 'ANZ Sales');
            insert u1;
               		        
        Test.starttest();
                               
                License_Management__c lmg1 = new License_Management__c();
                lmg1.Name = 'Pearson Enterprise ESB';               
                lmg1.Market__c = 'UK';
                lmg1.License_Pool__c = 'Enterprise - ESB';
                lmg1.CurrencyIsoCode = 'GBP';
                insert lmg1;
                
                User_License_Tracker__c ULT = new User_License_Tracker__c();                    
                ULT.Market__c = 'UK';
                ULT.License_Pool__c = 'Enterprise - ESB';
                ULT.Allocated_License__c = 100;
        		ULT.Available_License__c = 100;
                ULT.License_Manage__c = lmg1.id;
                ULT.CurrencyIsoCode = 'GBP';
                ULT.License_Type__c = 'Salesforce';
                insert ULT;
        
        		User_License_Tracker__c ULT1 = new User_License_Tracker__c();                    
                ULT1.Market__c = 'UK';
                ULT1.License_Pool__c = 'Enterprise - ESB';
                ULT1.Allocated_License__c = 100;
        		ULT1.Available_License__c = 100;
                ULT1.License_Manage__c = lmg1.id;
                ULT1.CurrencyIsoCode = 'GBP';
                ULT1.License_Type__c = 'Salesforce';
            	try{
                insert ULT1;
                }catch(exception e){}
        
        		User_License_Tracker__c ULT2 = new User_License_Tracker__c();                    
                ULT2.Market__c = 'UK';
                ULT2.License_Pool__c = 'Enterprise';
                ULT2.Allocated_License__c = 100;
        		ULT2.Available_License__c = 100;
                ULT2.License_Manage__c = lmg1.id;
                ULT2.CurrencyIsoCode = 'GBP';
                ULT2.License_Type__c = 'Salesforce';
                insert ULT2;
       
        		ULT2.License_Pool__c = 'Enterprise - ESB';
      			try{
                update ULT2;
                }catch(exception e){}
        		
       		        
        Test.stopTest();
    }
             
}