/************************************************************************************************************
* Apex Interface Name : PS_KnowledgeCreateArticleFromCaseTest 
* Version             : 1.1 
* Created Date        : 5/2/2016  
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Sneha Sinha               5/2/2016       
*  Rashmi Prasad             3/4/2016                Added Negative scenario      
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_KnowledgeCreateArticleFromCaseTest 
{
 public static testMethod void validateArticleFromCase(){
    List<User> listWithUser = new List<User>();
    listWithUser  = TestDataFactory.createUser([select Id from Profile where Name ='System Administrator'].Id,1);
    insert listWithUser;  
    
  
    
    System.runAs(listWithUser[0]){
    
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        insert lstAccount ;
        
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].MailingState='England';
        lstContact[0].accountid=lstAccount[0].id;
        insert lstContact;
        
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=  listWithUser[0].id,Disable_Triggers__c=true);
        insert byp; 
        
        List<case> caseslist = TestDataFactory.createCase(1,'School Assessment');
        caseslist[0].Account=lstAccount[0];
        caseslist[0].Contact=lstContact[0]; 
        caseslist[0].Platform__c = '3PL BB';
        caseslist[0].PS_Product__c = 'Virtual ChemLab';
        caseslist[0].Category__c = 'Access Code';
        caseslist[0].Subcategory__c = '2nd Request for Return Label';
        caseslist[0].Resolution_Steps__c = 'test';
        caseslist[0].Geography__c = 'Core';
        Test.StartTest();
        insert caseslist;
        Test.StopTest();
        String caseId = caseslist[0].id;
        insert new PS_DataCategory__c(Name = 'LOB_K-12',Value__c = 'K_12');
        ApexPages.currentPage().getParameters().put('sourceId', caseId);
        ApexPages.currentPage().getParameters().put('sfdc.override', '1');
        ApexPages.KnowledgeArticleVersionStandardController ctl =new ApexPages.KnowledgeArticleVersionStandardController(new Customer_Support__kav());
        PS_KnowledgeCreateArticleFromCase sample= new PS_KnowledgeCreateArticleFromCase(ctl);
        System.assertEquals(caseId, ctl.getSourceId());
        
    }
 }

//Negative Scenario   
 public static testMethod void NegativetestArticleFromCase(){
    List<User> listWithUser = new List<User>();
    listWithUser  = TestDataFactory.createUser([select Id from Profile where Name ='System Administrator'].Id,1);
    insert listWithUser;
    
    
  
    System.runAs(listWithUser[0]){
        
        
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
    insert lstAccount ;
    
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].MailingState='England';
        lstContact[0].accountid=lstAccount[0].id;
        insert lstContact;   
        
        List<case> caseslist = TestDataFactory.createCase(1,'School Assessment');
        caseslist[0].Account=lstAccount[0];
        caseslist[0].Contact=lstContact[0]; 
        caseslist[0].Platform__c = '3PL BB';
        caseslist[0].PS_Product__c = 'Virtual ChemLab';
        caseslist[0].Category__c = 'Access Code';
        caseslist[0].Subcategory__c = '2nd Request for Return Label';
        caseslist[0].Resolution_Steps__c = 'test';
        caseslist[0].Geography__c = 'Core';
        Test.StartTest(); 
        insert caseslist;
        Test.StopTest();
        Customer_Support__kav cs= new Customer_Support__kav();
        cs.Issue__c='Description';
        cs.Environment__c='Text' ;
        try{        
            insert cs; 
        }
        catch(DmlException e){
        Boolean expectedExceptionThrown =  e.getMessage().contains('REQUIRED_FIELD_MISSING, Required fields are missing: [Title]:') ? true : false;
        System.AssertEquals(expectedExceptionThrown, true);
        System.assertEquals('REQUIRED_FIELD_MISSING' ,  e.getDmlStatusCode(0));
        }
        String caseId = caseslist[0].id; 
        insert new PS_DataCategory__c(Name = 'LOB_K-12',Value__c = '');
        ApexPages.currentPage().getParameters().put('sourceId', caseId);
        ApexPages.currentPage().getParameters().put('sfdc.override', '1');
        ApexPages.KnowledgeArticleVersionStandardController ctl =new ApexPages.KnowledgeArticleVersionStandardController(new Customer_Support__kav());
        PS_KnowledgeCreateArticleFromCase sample= new PS_KnowledgeCreateArticleFromCase(ctl);
        System.assertEquals(caseId, ctl.getSourceId());
        
    }
 }
}