/*******************************************************************************************************************
* Apex Class Name  : PS_CaseUpdateParentCaseCtrl.cls
* Version          : 1.0
* Created Date     : 18 Nov 2015
* Function         : RD-1546 - Update Parent Case if child case is created or updated or reopened
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
* Payal Popat               18/11/2015              Initial documentation of the class. 
* KP                        12/15/2015              Updated code to execute the logic if the cases belong to expected business bucket, removed boolen field from query criteria  
* KP                         2/3/2015               Amended class code to split validation and parent case update logic 
* RG                        09/06/2015              RG: Query Modified for D-5522  
*******************************************************************************************************************/
public without sharing class PS_CaseUpdateParentCaseCtrl{
    /**
* Description : Method to validate Child case creation
* @param NA 
* @return NULL
* @throws NA
**/
    public static PageReference mParentCaseValidation(List<case> triggeredcases,Map<Id,Case> newMap,boolean isInsert,boolean isUpdate,Map<id,Case> oldMap){
        //List<case> triggeredcases=newMap.values();
        List<Case> newcases = CommonUtils.getCaseServiceRecordType(System.Label.PS_ServiceRecordTypes, triggeredcases);
        Set<Id> caseIds = new Set<Id> ();
        Map<Id,Id> omapChildPar = new Map<Id,Id> ();
        if (newcases.size() > 0) {
            if (isInsert){ //validation while creating new child cases
                for (Case oNewCase: newcases) {
                    if (oNewCase.ParentId != NULL)
                        omapChildPar.put(oNewCase.Id,oNewCase.ParentId);
                    //caseIds.add(oNewCase.ParentId);
                }
            }//end insert
            if (isUpdate){ //validation while flipping existing cases as child cases
                for (Case oNewCase: newcases) {
                    if (oNewCase.ParentId != NULL && oNewCase.ParentId != oldmap.get(oNewCase.id).ParentId){
                        System.debug('Update parentcase');
                        omapChildPar.put(oNewCase.Id,oNewCase.ParentId);
                    }
                    //caseIds.add(oNewCase.ParentId);
                }                
            }
            if(!omapChildPar.isEmpty()){
                Map<Id,Case> mapParentCase = new Map<id,Case>([SELECT Id,FCR__c,closedDate,status,recordtypeid fROM CASE where Id IN: omapChildPar.values()]);                
                //validation on case types                       
                for (Case oNewCase: newcases) {
                    System.debug(oNewCase.recordtypeid+';'+mapParentCase.get(omapChildPar.get(oNewCase.Id)).recordtypeid);
                    if(oNewCase.recordtypeid != mapParentCase.get(omapChildPar.get(oNewCase.Id)).recordtypeid){
                        system.debug('record type does not match');
                        triggeredcases[0].addError('Child Cases must be the same type as the Parent Case.');
                        return null;
                    }
                }    
                //validation on no of closure days                                        
                for(Case objCase : mapParentCase.values()){
                    if(objCase.status==Label.PS_CaseClosureStatus){
                        DateTime dT = objCase.ClosedDate;
                        Date closeDate = date.newinstance(dT.year(), dT.month(), dT.day());
                        Integer noOfDays = closeDate.daysBetween(date.today());
                        //system.debug('Test closure'+noOfDays);
                        if (noOfDays > 7){
                            triggeredcases[0].addError(Label.PS_ChildCaseCreationAlert);  
                            return null;   
                        }   
                    }                
                }                                       
            }//end Parent case validation
        }//end service case        
        return null;
    }//end method mParentCaseValidation
    
    /**
* Description : Method to fetch Parent Case
* @param NA 
* @return NA
* @throws NA
**/
    public static void mProcessParentCase(List<Case> triggeredcases,boolean isInsert,boolean isUpdate,Map<id,Case> oldMap){  
        List<Case> newcases = CommonUtils.getCaseServiceRecordType(System.Label.PS_ServiceRecordTypes, triggeredcases);  
        Set<Id> oparcaseIdtoReopen = new Set<Id> ();//to capture parent case id of new child cases
        Set<Id> oparcaseIdtoClose = new Set<Id> ();//to capture parent case id of new child cases
        Set<Id> parentCaseIds = new Set<Id> ();//to capture case id of parent cases - status updates    
        
        
        if (newcases.size()>0){
            if(isInsert){    
                for (Case oNewCase: newcases) {
                    if (oNewCase.ParentId != NULL)
                        oparcaseIdtoReopen.add(oNewCase.ParentId);
                }           
            }//end insert
            if (isUpdate){ //validation while flipping existing cases as child cases
                for (Case oNewCase: newcases) {
                    //System.debug('@@oNewCase.ParentId: '+oNewCase.ParentId);
                    //System.debug('@@oldmap.get(oNewCase.id).ParentId '+oldmap.get(oNewCase.id).ParentId);
                    System.debug('@@oNewCase.Status '+oNewCase.Status);
                    System.debug('@@oldmap.get(oNewCase.id).Status '+oldmap.get(oNewCase.id).Status);
                    if (oNewCase.ParentId != NULL && oNewCase.ParentId != oldmap.get(oNewCase.id).ParentId)
                        oparcaseIdtoReopen.add(oNewCase.ParentId);
                    else if(oNewCase.ParentId != NULL && oNewCase.Status!=oldmap.get(oNewCase.id).Status){
                        if (oNewCase.Status!=Label.PS_CaseClosureStatus)            
                            oparcaseIdtoReopen.add(oNewCase.ParentId);
                    }
                    else if(oNewCase.ParentId == NULL && oNewCase.Status!=oldmap.get(oNewCase.id).Status && 
                            oNewCase.Status==Label.PS_CaseClosureStatus){ 
                                parentCaseIds.add(oNewCase.Id);
                            }
                }  //loop through case              
            }//end update           
        }
        if (oparcaseIdtoReopen.size()>0)
            mUpdateParentCase(oparcaseIdtoReopen,true);
        if (parentCaseIds.size()>0)             
            mUpdateChildCase(parentCaseIds,newcases,oldMap); 
        
    }//end method mProcessParentCase
    
    /**
* Description : update closed parent cases to inprogress whenever there is change/addition of child cases
* @param NA 
* @return NA
* @throws NA
**/
    public static void mUpdateParentCase(Set<Id> oCaseIdSet,Boolean bReopenFlg){ 
        String sValErrMsg='';
        List<Case> olstParCaseUpdate = new List<case>();
        if(bReopenFlg){
            for(Case oCase :[SELECT Id,FCR__c,closedDate,status fROM CASE where Id IN: oCaseIdSet AND 
                             STATUS =: Label.PS_CaseClosureStatus]){ 
                                 oCase.FCR__c = false;
                                 oCase.Status = Label.PS_CaseInProgressStatus;
                                 olstParCaseUpdate.add(oCase);
                             }//end for
        }//end parent case reopen scenario
        /*else{
for(Case oCase :[SELECT Id,FCR__c,closedDate,status,(Select isClosed,Status,Id from Cases where isClosed = false) FROM 
Case where Id IN: oCaseIdSet]){
if(oCase.Cases.size()== 0 ){  
oCase.Status = Label.PS_CaseClosureStatus;
olstParCaseUpdate.add(oCase);
}
}//end for        
}end parent case closure scenario*/
        if (olstParCaseUpdate.size()>0){
            try{update olstParCaseUpdate;}
            Catch(Exception e){
                if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                    sValErrMsg=e.getMessage().substring(e.getMessage().IndexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
                    
                }
                system.debug('Validation Error Message: Parent Case Update Failed: ' + sValErrMsg);
                throw(e);
                //return sValErrMsg;
            }
        }     
        //return sValErrMsg;
    }//end method mUpdateParentCase
    /**
* Description : Method to update Child Case
* @param NA 
* @return NA
* @throws NA
* @Modified By : Mayank for CR-01159
**/
    public static void mUpdateChildCase(Set<Id> Id,List<Case> newcases,map<Id,Case> OldMapCase){
        List<case> updateChildCases = new List<case>();
        List<Case> updateParentlst = new List<Case>();
        PS_Batch_Size_for_Child_Cases_Closure__c btchSize = PS_Batch_Size_for_Child_Cases_Closure__c.getValues('ChildBatchSize');
        system.debug('@@btchSize :'+btchSize.Batch_Size__c.intValue());
        System.debug('@@threshold size: '+btchSize.Threshold_No_Of_Childs__c.intValue());
        
        Map<Id, Case> pCaseMap = new Map<Id, Case>(newcases);
        List<Case> childCasesbeforeUpdate = [SELECT Id,Status,parentid,Case_Resolution_Category__c,Case_Resolution_Sub_Category__c,CaseNumber  FROM CASE where ParentId IN: Id and Status !=:Label.PS_CaseClosureStatus];   //RG: Query Modified for D-5522
        
        //Call batch class to update cases if there more than 5 child for a parent case
        if(!childCasesbeforeUpdate.isEmpty()) {
            if(childCasesbeforeUpdate.size() > btchSize.Threshold_No_Of_Childs__c.intValue()) {
                System.debug('@@more than 10 childs');
                if(!pCaseMap.isEmpty()){
                    System.debug('@@about to call batch');
                    System.debug('@@OldMapCase '+OldMapCase);
                    Database.executeBatch(new PS_CloseParentCaseWithChildCases(pCaseMap,OldMapCase), btchSize.Batch_Size__c.intValue()); 
                }
            } else {
                System.debug('@@less than 10 childs');
                // Call normal execution if there are less than 5 childs for parent case
                for(Case objCase:childCasesbeforeUpdate) {
                    objCase.Status = Label.PS_CaseClosureStatus;
                    objCase.Case_Resolution_Category__c=pCaseMap.get(objCase.parentid).Case_Resolution_Category__c;
                    objCase.Case_Resolution_Sub_Category__c=pCaseMap.get(objCase.parentid).Case_Resolution_Sub_Category__c;
                    updateChildCases.add(objCase);
                }//end for 
                
                if(!updateChildCases.isEmpty()){
                    try{
                        Update updateChildCases;  //Commented by Mayank for CR-1159 
                    }
                    catch(Exception e){
                      for(Case cs : (List<Case>)Trigger.New){
                            cs.addError('One of the child cases couldnot be closed due to some issue. Please try to fix the child case and then close the parent case.');
                        }
                    }
                }
            }   
        }
    
    }//end of Method   mUpdateChildCase
}