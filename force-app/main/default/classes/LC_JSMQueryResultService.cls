public class LC_JSMQueryResultService {

@AuraEnabled
    public static sObject executeQueryOneObject(String theQuery){
        try{
            //We do not escape  parameters here since the query must not be modified. 
            //Parameters must be escaped by consumers of this method
            return Database.query(theQuery);
        }catch(Exception e){
            throw new AuraHandledException('Error doing the query: '+theQuery+' Error: '+e.getMessage());
        }
    }
    
    @AuraEnabled
    public static sObject loadObjectInfoById(Id recordId) {
        DescribeSObjectResult objectType = recordId.getSobjectType().getDescribe();
        List<String> objectFields = new List<String>(objectType.fields.getMap().keySet());
        String query = 'SELECT ' + String.join(objectFields, ',') + ' FROM ' + objectType.getName() + ' WHERE Id = \'' + String.escapeSingleQuotes(recordId) + '\' LIMIT 1';

        return executeQueryOneObject(query);
    }  
}