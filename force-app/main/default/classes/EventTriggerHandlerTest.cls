/************************************************************************************************************
* Apex Interface Name : EventTriggerHandlerTest
* Version             : 1.0 
* Created Date        : 9/9/2015  
* Modification Log    : 
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Abhinav Srivastav         9/9/2015              R2.1 version
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

@isTest
private class EventTriggerHandlerTest
{
 
      Static testMethod void unittest()
        {
         Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
        sAccount.BillingCountry         = 'Australia';
        sAccount.BillingState           = 'Victoria';
        sAccount.BillingCountryCode     = 'AU';
        sAccount.BillingStateCode       = 'VIC';
        sAccount.ShippingCountry        = 'Australia';
        sAccount.ShippingState          = 'Victoria';
        sAccount.ShippingCountryCode    = 'AU';
        sAccount.ShippingStateCode      = 'VIC';
        sAccount.IsCreatedFromLead__c   = True;
        sAccount.Market2__c				='AU';
        sAccount.Line_of_Business__c    = 'Clinical';
        sAccount.Geography__c           = 'Core';
        insert sAccount;
        
        List<string>StrLst = new list<string>{'a','b','c'};
        Map<String,List<String>> exceptions = new Map<String,List<String>>();
        exceptions.put('a',StrLst);
        
        
        map<id,DateTime> csmap=new map<id,DateTime>();
        map<id,String> cobmap=new map<id,string>();
        map<Id, DateTime> cemap=new map<Id, DateTime>();
        map<id,String> cotomap=new map<id,String>();
         /* call__c c = new call__c ();
          c.Account__c = sAccount.Id ;
          c.Call_Start_Time__c=datetime.newInstance(2014, 9, 14, 12, 30, 0);
          c.Call_End_Time__c=datetime.newInstance(2014, 9, 16, 13, 30, 0);
          c.Call_Objective__c='Introduction';
          c.Other_Call_Objective__c='other';
          insert c;
          cobmap.put(c.id,c.Call_Objective__c);
          
          csmap.put(c.id,c.Call_Start_Time__c);
          system.debug('-------my id' +c.id);*/
                   
            map<Id,Event> mapev =new map<Id,Event>();
            list<Event> oldevent=new list<Event>();
            list<Event> updatedEvent=new list<Event>();
            Event sEvent1= new Event();         
            Event sEvent2= new Event();          
            sEvent1.EndDateTime = system.now().addHours(3);
            sEvent1.StartDateTime = system.now().addHours(2);
            sEvent1.DurationInMinutes = null;
            sEvent1.Status__c = 'Completed' ;
            //sEvent1.whatId = c.id;
            sEvent1.OwnerId = userinfo.getUserId();
            sEvent1.Location = 'USA';
            insert sEvent1;
            //system.debug('-------my event1 id' +c.id);
            mapev.put(sEvent1.Id,sEvent1);
            oldevent.add(sEvent1);
          
           /* csmap.put(c.id,system.now().addHours(2));
            system.debug('-------my event id' +c.id);
            cemap.put(c.id,system.now().addHours(3));*/
            
            sEvent2.EndDateTime = system.now().addHours(3);
            sEvent2.StartDateTime = system.now().addHours(2);
            sEvent2.DurationInMinutes = null;
            sEvent2.Status__c = 'Completed' ;
            //sEvent2.whatId = c.id;
            sEvent2.OwnerId = userinfo.getUserId();
            sEvent2.Location = 'USA';
            insert sEvent2;
            mapev.put(sEvent2.Id,sEvent2);
            oldevent.add(sEvent2);
            System.debug('map value check' +mapev);

            upsert oldevent;

            sEvent1.Location = 'test';
            sEvent1.EndDateTime = system.now().addHours(5);
            sEvent1.StartDateTime = system.now().addHours(3);
            //sEvent1.whatId = c.id;
            update sEvent1;
             updatedevent.add(sEvent1);
             update(updatedevent);
           
             System.debug('fetchingdata2' +sEvent1);
             System.debug('fetchingdata3' +oldevent);
             
             map<id,event> utilmap=new map<id,event>();
             PS_Util.addErrors_1(utilmap,exceptions );
            System.debug('fetchingdata' +sEvent1);
            PS_Util.addErrors_1(mapev,exceptions );
           
          EventTriggerHandler tests = new EventTriggerHandler(true,1);
          system.assertequals(tests.bIsVisualforcePageContext,false);
          system.assertequals(tests.bIsWebServiceContext,false);
          system.assertequals(tests.bIsExecuteAnonymousContext,false);
          system.assertequals(tests.bIsTriggerContext,true);
          Test.startTest();
          EventTriggerHandler e =new EventTriggerHandler(true,1);
          e.OnUndelete(oldevent); 
          e.OnAfterUpdate(oldevent,updatedEvent,mapev);
          e.OnBeforeDelete(updatedEvent,mapev);
          
          Test.stopTest();
        delete sEvent1; 
         undelete sEvent1;  
        }
        }