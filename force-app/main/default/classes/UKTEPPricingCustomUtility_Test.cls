/**************************************************************************
 * Class Name   : UKTEPPricingCustomUtility_Test
 * Author       : Aman Sawhney
 * Date Created : 16 / Nov / 2018
 * Description  : A test class for methods of UKTEPPricingCustomUtility
 * ************************************************************************/
@isTest
public class UKTEPPricingCustomUtility_Test {

    @isTest
    private static void getDoubleTest(){
        
        Double result = UKTEPPricingCustomUtility.getDouble('TEST-String');
        System.assert(result==0);
        
        result = UKTEPPricingCustomUtility.getDouble(null);
        System.assert(result==0);
        
        result = UKTEPPricingCustomUtility.getDouble('1');
        System.assert(result==1);
        
        result = UKTEPPricingCustomUtility.getDouble('-1');
        System.assert(result==-1);
        
    }
    
    @isTest
    private static void getIntegerTest(){
        
        Double result = UKTEPPricingCustomUtility.getInteger('TEST-String');
        System.assert(result==0);
        
        result = UKTEPPricingCustomUtility.getInteger(null);
        System.assert(result==0);
        
        result = UKTEPPricingCustomUtility.getInteger('1');
        System.assert(result==1);
        
        result = UKTEPPricingCustomUtility.getInteger('-1');
        System.assert(result==-1);
        
    }

}