/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 08/07/2018
 */
global without sharing class EinsteinBotUpdateContact {
    @InvocableMethod(label='Einstein Bot - Update Contact')
    global static void updateContact(List<UpdateContactRequest> requests) 
    {
        List<Einstein_Bot_Event__e> updateContactEvents = new List<Einstein_Bot_Event__e>();

		for(UpdateContactRequest request : requests) {
            Einstein_Bot_Event__e updateContactEvent = new Einstein_Bot_Event__e();
            updateContactEvent.Type__c = EinsteinBotEventHandler.UPDATE_CONTACT;
            updateContactEvent.Contact_Id__c = request.contact.Id;
            updateContactEvent.Role__c = request.role;
			updateContactEvent.Live_Agent_Session_Id__c = request.chatSessionKey;

            updateContactEvents.add(updateContactEvent);
        }

        List<Database.SaveResult> saveResults = EventBus.publish(updateContactEvents);
	}

    global class UpdateContactRequest
    {
        @InvocableVariable
        global Contact contact;

		@InvocableVariable
		global String role;

		@InvocableVariable
		global String chatSessionKey;
    }
}