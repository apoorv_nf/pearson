/************************************************************************************************************
* Apex Interface Name : PS_CaseChatTranscriptTest
* Description         : Test class created for class PS_CaseChatTranscript
* Version             : 1.1 
* Created Date        : 9/2/2016 
* Modification Log    : 
* Expected Inputs     : Pass parameters like Status,record type,caseid
* Expected Outputs    : Get list of ive chat transcript
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Dalia Jeo                 9/2/2016         
* Rohit             04/3/2016    
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
private class PS_CaseChatTranscriptTest{
    public static testMethod void testCaseChatTranscript(){  
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name ='System Administrator'].Id,1);
        System.runAs(listWithUser[0])
        {
            //Case Record Type - Technical Support
            RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND Name =: Label.Higher_Ed_Technical_Support];
            List<Case> cases = TestDataFactory.createCase(1,'School Assessment');
            cases[0].RecordTypeId= rt.Id;
            cases[0].Status = Label.PS_CaseClosureStatus;
            Test.StartTest();
            insert cases;
            PS_CaseChatTranscript ps = new PS_CaseChatTranscript();
            List<LiveChatTranscript> lstLiveChatTrans =  ps.getChatTranscript();
            system.assertEquals(cases[0].Status,Label.PS_CaseClosureStatus);
            Test.StopTest();
        }
    }
    
    public static testMethod void Negative_testCaseChatTranscript(){  
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name ='System Administrator'].Id,1);
        System.runAs(listWithUser[0])
        {
            //Case Record Type - Technical Support
            RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND Name =: Label.Higher_Ed_Technical_Support];
            List<Case> cases = TestDataFactory.createCase(1,'School Assessment');
            cases[0].RecordTypeId= rt.Id;  
            //Passing Wrong Data
            cases[0].Status = '';
            Test.StartTest();
            insert cases;
            //checking with null id 
            System.Assert(true,'Invalid Status'); 
            PS_CaseChatTranscript ps = new PS_CaseChatTranscript();
            List<LiveChatTranscript> lstLiveChatTrans =  ps.getChatTranscript();
            system.assertEquals(cases[0].Status,'');
            Test.StopTest();
        }
    }
}