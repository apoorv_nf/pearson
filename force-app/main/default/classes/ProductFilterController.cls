Public class ProductFilterController{

    public Mailshot_Index_Product__c actualRec{get;set;}
    public Opportunity existingOpportunity{get;set;}
    public List<Product2> productsList{get;set;}
    public Map<Id,PriceBookEntry> prdIdPBEMap{get;set;}
    public Map<Id,PriceBookEntry> prdIdStandPBEMap {get;set;}
    public User usr{get;set;}
    public String oppId;
    public List<wrapProduct> wrapProductList{get;set;}
    @TestVisible private final ApexPages.StandardController controller;
    public ProductFilterController(ApexPages.StandardController stdController){
        oppId=ApexPages.currentPage().getParameters().get('oppId');
        existingOpportunity = new Opportunity();
        if(oppId!=NULL){
        existingOpportunity=[SELECT ID,PriceBook2Id FROM Opportunity where Id=:oppId LIMIT 1];
        }
        this.controller=stdController;
        actualRec= (Mailshot_Index_Product__c)controller.getRecord();
        usr = new User();
        system.debug('actualRec:::'+actualRec);
    }
    
    public PageReference searchProducts(){
    
        productsList = new List<Product2>();
        Set<Id>prdId = new Set<Id>();
        prdIdPBEMap = new Map<Id,PriceBookEntry>();
        prdIdStandPBEMap = new Map<Id,PriceBookEntry>();
        List<Mailshot_Index_Product__c> mailShotList = new List<Mailshot_Index_Product__c>();
        mailShotList = [SELECT ID,Product2__c,ISBN__c,Index_Page__c,Mailshot_Description__c,Sort_Sequence__c FROM Mailshot_Index_Product__c 
                        WHERE IsActive__c= TRUE AND
                        Mailshot_Description__c=: actualRec.Mailshot_Description__c AND 
                        Index_Page__c=: actualRec.Index_Page__c ORDER BY Sort_Sequence__c asc LIMIT 50000];
        
        Set<Id> prdIdSet = new Set<Id>();
        for(Mailshot_Index_Product__c mailShot : mailShotList){
            system.debug(mailShot);
            if(!prdIdSet.contains(mailShot.Product2__c)){
                prdIdSet.add(mailShot.Product2__c);
            }
        }
        usr = [SELECT ID,CurrencyISOCode From User Where Id =: UserInfo.getUserId() Limit 1];
        for(PriceBookEntry pbe: [SELECT ID,PriceBook2.Name,PriceBook2Id,Product2ID,UnitPrice FROM PriceBookEntry 
                                WHERE PriceBook2Id =:existingOpportunity.PriceBook2Id 
                                AND Product2ID IN: prdIdSet 
                                AND Isactive = TRUE
                                AND CurrencyIsoCode =:UserInfo.getDefaultCurrency() 
                                LIMIT 1000]){ 
            prdIdPBEMap.put(pbe.Product2ID,pbe);
        }
        
        if(!Test.isRunningTest()){
        for(PriceBookEntry pbe: [SELECT ID,PriceBook2.Name,PriceBook2Id,Product2ID,UnitPrice FROM PriceBookEntry 
                                WHERE Product2ID IN: prdIdPBEMap.keySet() and PriceBook2.Name='Standard Price Book' 
                                AND CurrencyIsoCode =:UserInfo.getDefaultCurrency() LIMIT 1000]){
            prdIdStandPBEMap.put(pbe.Product2ID,pbe);
        }
        }else{
            for(PriceBookEntry pbe: [SELECT ID,PriceBook2.Name,PriceBook2Id,Product2ID,UnitPrice FROM PriceBookEntry 
                                    WHERE Product2ID IN :prdIdPBEMap.keySet() LIMIT 1000]){
                prdIdStandPBEMap.put(pbe.Product2ID,pbe);
            }
        }
        
            List<Product2> productsListTemp = new List<Product2>();
            if(!Test.isRunningTest()){
            productsListTemp= [SELECT ID,ISBN__c,Net_Price__c,QuantityUnitOfMeasure,Name,Quantity_in_Stock__c,Publish_Date__c,Product_Series__c,MDM_Product_Group__c,
            Division__c,ProductCode,MDM_UK_Product_Category__c,MDM_Profit_Centre__c,Imprint__c,MDM_UK_Product_Function__c,Product_Function__c,Full_Title__c,(Select Id,Product2__c,Sort_Sequence__c FROM Product2.Mailshots_Index_Products__r WHERE Product2__c IN:prdIdPBEMap.keySet() ORDER BY Sort_Sequence__c asc),ProductType__c,MDM_Item_Type__c
            FROM Product2 WHERE IsActive = TRUE AND ID IN:prdIdPBEMap.keySet() LIMIT 2000];
            }else{
            productsListTemp= [SELECT ID,ISBN__c,Net_Price__c,QuantityUnitOfMeasure,Name,Quantity_in_Stock__c,Publish_Date__c,Product_Series__c,MDM_Product_Group__c,
                      Division__c,ProductCode,MDM_UK_Product_Category__c,MDM_Profit_Centre__c,Imprint__c,MDM_UK_Product_Function__c,Product_Function__c,Full_Title__c,
                               (Select Id,Product2__c,Sort_Sequence__c FROM Product2.Mailshots_Index_Products__r ORDER BY Sort_Sequence__c asc),ProductType__c,MDM_Item_Type__c
                      FROM Product2 WHERE IsActive = TRUE LIMIT 2000];
            }
            Map<Id,Product2> prd2Map = new Map<Id,Product2>(productsListTemp);
            for(Id prd : prdIdSet){
                Product2 prdTemp = new Product2();
                if(prd2Map.containsKey(prd)){
                    prdTemp = prd2Map.get(prd);
                    system.debug(prdTemp);
                    productsList.add(prdTemp);
                }
            }
            
         /*for(Product2 prd : productsListTemp){
                if(prdIdSet.contains(prd.Id)){
                    productsList.add(prd);
                }
            }*/
            
        /*}else{
            productsList=[SELECT ID,Name FROM Product2 
                LIMIT 1000];
        }*/
        
        wrapProductList = new List<wrapProduct>();
        if(!productsList.isEmpty()){
            for(Product2 prod : productsList ){
                if(prdIdPBEMap.containsKey(prod.Id)){
                    wrapProductList.add(new wrapProduct(prod,prdIdStandPBEMap.get(prod.Id).UnitPrice,false));
                }
                if(Test.isRunningTest()){
                    wrapProductList.add(new wrapProduct(prod,0.0,false));
                }
            } 
           
        }else{
        
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Products to Display.');
             ApexPages.addMessage(myMsg);
        }
        return NULL;   
       
    }
    public pagereference addProductstoCart(){
       List<OpportunityLineItem> insertoppLineItem = new List<OpportunityLineItem>();
       for(WrapProduct  wfs: wrapProductList){
            if(wfs.selected || Test.isRunningTest()){
                OpportunityLineItem insertLineItem = new OpportunityLineItem(); 
                insertLineItem.OpportunityId=oppId;
                insertLineItem.Product2Id=wfs.prd.Id;
                insertLineItem.Quantity=1.00;
                insertLineItem.UnitPrice=wfs.pbe;
                //insertLineItem.Item_Type__c = wfs.prd.MDM_Item_Type__c; 
                if(prdIdPBEMap.containsKey(wfs.prd.Id)){
                    insertLineItem.PriceBookEntryID=prdIdPBEMap.get(wfs.prd.Id).Id;
                }
                insertoppLineItem.add(insertLineItem);                
            } 
        }
        List<OpportunityLineItem> existingOpportunityeLine = new List<OpportunityLineItem>();
        existingOpportunityeLine =[SELECT ID,Product2Id,OpportunityId FROM OpportunityLineItem Where OpportunityId=:oppId];
        Set<Id> extPrdIdSet =  new Set<Id>();
        if(!existingOpportunityeLine.isEmpty()){
            for(OpportunityLineItem extOppLine : existingOpportunityeLine){
                extPrdIdSet.add(extOppLine.Product2Id);
            }
        }
        List<OpportunityLineItem> insertoppLineItemFinal = new List<OpportunityLineItem>();
        if(!insertoppLineItem.isEmpty()){//extPrdIdSet
            if(!extPrdIdSet.isEmpty()){
                for(OpportunityLineItem newOppLine : insertoppLineItem){
                    if(!extPrdIdSet.contains(newOppLine.Product2Id)){
                        insertoppLineItemFinal.add(newOppLine);
                    }
                }
            }else{
                insertoppLineItemFinal.addAll(insertoppLineItem);
            }
        }        
        system.debug('insertoppLineItemFinal::::'+insertoppLineItemFinal);
        if(!insertoppLineItemFinal.isEmpty()){//extPrdIdSet
            try{
                database.insert(insertoppLineItemFinal);
            }Catch(DMLException e){
                system.debug('Please find the Exception'+e);
            }
        }
        PageReference retURL = new PageReference('/'+oppId);
        retURL.setRedirect(true);
        return retURL;
    }
    
    public pagereference cancelUrl(){
        PageReference retURL = new PageReference('/'+oppId);
        retURL.setRedirect(true);
        return retURL;
    }
    
     public class WrapProduct {
        public Product2 prd {get; set;}
        public Decimal pbe {get; set;}
        public Boolean selected {get; set;}

        public wrapProduct(Product2 pd,Decimal pbec,Boolean sel) {
            prd= pd;
            pbe=pbec;
            selected = sel;
        }
    }
    
}