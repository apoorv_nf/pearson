@isTest
public class TestUpdateTeamMember{
    static testMethod void updateAccountOwnerTest(){
        Profile pfile = [Select Id,name from profile where name = 'One CRM Integration'];
        User u = new User();
        u.LastName = 'territoryuser';
        u.alias = 'terrusr'; 
        u.Email = 'laxman.sity@pearson.com';  
        u.Username='laxmansity@gmail.com';
        u.LanguageLocaleKey='en_US'; 
        u.TimeZoneSidKey='America/New_York';
        u.Price_List__c='US HE All';
        u.LocaleSidKey='en_US';
        u.EmailEncodingKey='ISO-8859-1';
        u.ProfileId = pfile.id;                
        u.Geography__c = 'Growth';
        u.CurrencyIsoCode='USD';
        u.Line_of_Business__c = 'Higher Ed';
        u.Market__c = 'US';
        u.Business_Unit__c = 'US Field Sales';
        insert u;
        System.runAs(u){
            Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Learner').getRecordTypeId();       
            Account acc = new Account();
                acc.Name= 'Test Account Creation Request';
                acc.Types__c = 'Account Creation';
                acc.RecordTypeId = recordTypeId;
                acc.OwnerId = userinfo.getUserId();
            insert acc;
        }
    }
}