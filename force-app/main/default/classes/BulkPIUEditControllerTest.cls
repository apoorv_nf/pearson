/************************************************************************************************************
* Apex Interface Name : BulkPIUEditControllerTest 
* Version             : 1.0 
* Created Date        : 7/1/2015 4:44 
* Modification Log    : 
* Developer                   Date                  Summary Of Changes
* -----------------------------------------------------------------------------------------------------------
* Madhu                     29/10/2015            Adding code coverage for BulkPIUEditController class.
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/



@isTest
public class BulkPIUEditControllerTest
{
   
    static testMethod void testDataCreation()
    {
        test.startTest();
    
    
    List<User> listWithUser = new List<User>();
    listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
    if(listWithUser != null && !listWithUser.isEmpty())
    {
        listWithUser[0].Market__c = 'US';
        listWithUser[0].Price_List__c = 'Math & Science';
        listWithUser[0].Business_Unit__c = 'US Field Sales';
        listWithUser[0].Product_Business_Unit__c = 'US Field Sales';
        insert listWithUser;
    }
    
    //Boolean selected=true;
    
    List<Account> accountList =new List<Account>();
        
    List<AssetWrapper> asw= new List<AssetWrapper>();
    
    accountList=TestDataFactory.createAccount(1,'Direct SPH Inter-Division');
    insert accountList ;
    List<Contact> contactList=TestDataFactory.createContact(1);
    insert contactList;
    UniversityCourse__c course=new UniversityCourse__c(Name='T0001-kyama',Catalog_Code__c='T0001',Course_Name__c='kyama',Account__c= accountList[0].Id);
    insert course;
    system.runAs(listWithUser[0]){
    Product2 prod=new Product2 (Name='testProd',Market__c='US',Business_Unit__c='US Field Sales',Line_of_Business__c='Higher Ed');
    insert prod;
    
    Product2 prod1=new Product2 (Name='testProd1',Market__c='US',Business_Unit__c='US Field Sales',Line_of_Business__c='Higher Ed');
    insert prod1;
    
    Asset ass = new Asset(Product2id=prod.id,status__c='Active',Name='kyama',Product_Author__c='Kyama',AccountId= accountList[0].Id,ContactId=contactList[0].id,Course__c=course.id);
    insert ass; 
    
    Asset ass1 = new Asset(Product2id=prod.id,primary__c = true,status__c='Active',Name='kyama',Product_Author__c='Kyama',AccountId= accountList[0].Id,ContactId=contactList[0].id,Course__c=course.id);
    insert ass1;
     Asset ass2 = new Asset(Product2id=prod.id,status__c='Inactive',Name='kyama',Product_Author__c='Kyama',AccountId= accountList[0].Id,ContactId=contactList[0].id,Course__c=course.id);
    insert ass2;
    AssetWrapper aw = new AssetWrapper(ass, 'abcde');
    aw.getAsset();
    aw.getIsAssetSelected();
//    aw.getOliID();
    aw.setIsAssetSelected(true);
        
    BulkPIUEditController.AssetsWrapper wp = new BulkPIUEditController.AssetsWrapper(ass,true);
     
    BulkPIUEditController.SortPiu sortPIU = new BulkPIUEditController.SortPiu(ass); 
   
    system.assert(ass.Product_Author__c!=null);
    
   } 
    test.stopTest();
           PageReference pageRef = Page.BulkContactPIUEditPage;
    Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('courseId', course.Id);
    ApexPages.currentPage().getParameters().put('Type', 'Active');
    BulkPIUEditController testBulk = new BulkPIUEditController();
    testBulk.schangedcId = contactList[0].Id;
    testBulk.schangedflg = false;
    testBulk.updateContactList();
    List<BulkPIUEditController.AssetsWrapper> s = new  List<BulkPIUEditController.AssetsWrapper>();
    testBulk.assetList = s; 
    testBulk.getAsset();
    testBulk.getProductIdWithName();
    testBulk.getProdList();
    testBulk.getAssetSelected();
    testBulk.updateContactList();
    testBulk.courseCancelDetailPage();
    testBulk.massUpdate();
    testBulk.butType = false;
    testBulk.changeButton();
    testBulk.butType = true;
    testBulk.changeButton();
    testBulk.OK();
    testBulk.selectedAssets.clear();
    }
 }