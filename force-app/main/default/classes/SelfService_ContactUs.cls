/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            SelfService_ContactUs.cls
   Description:     SelfService_ContactUs class to get values and perform DML operations on Case object
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
   15/07/2016         1.0            Supriyam               Initial Release 
   22/8/2016          1.1            Payal                   Added method declarations, removed system debugs and unused code.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class SelfService_ContactUs {
    public static String QualificationSubject{get;set;}
    public static String sAANumber{get;set;}    
    public static String Subject{get;set;}
    public static String sAAOrganisation{get;set;}
    public static String CenterNumber{get;set;}
    public static String Product{get;set;}
    public static String Topic{get;set;}
    public static String sSubCat{get;set;}
    public String Fname{get;set;}
    public String Lname{get;set;}
    public String uemail{get;set;}
    public String uphone{get;set;}
    public String sRecType{get;set;}   
    public String sRecTypeId{get;set;}  
    public String sAccountName{get;set;} 
    public String selectedProduct{get;set;}
    public User userEmail = getCurrentUser();
    public Contact conT = new Contact();
    public Boolean isAuthUser;
    /**
    * Description : Method to Identify Autheticated User
    * @param NA
    * @return boolean 
    * @throws NA
    **/
    public boolean getisAuthUser(){
        if(UserInfo.getUserType() !='Guest')
        {
            return true; 
        }
        else{return false;}
    }
    /**
    * Description : Method to get Category Values from Product object for Pre-Chat page
    * @param NA
    * @return List<Product__c>
    * @throws NA
    **/
    @RemoteAction
    public static  List<Product__c> getCategoryValuesFromRequestType(String sCustType,String sSelectedReqType){
        String sCategoryType='Category';
        List<Product__c> objCatList=[select id,name from Product__c where Market__c=:System.label.PS_UKMarket and Type__c=:sCategoryType and Business_Vertical__C =: sSelectedReqType and Role__c =: sCustType];
        objCatList.sort();
        return objCatList;
    }
        /**
    * Description : Method to get Contry Values for chat
    * @param NA
    * @return List<CS_country__c>
    * @throws NA
    **/ 
  @RemoteAction
    public static List<CS_country__c> getAACountry() 
    {        
        List<CS_country__c> countryValues = CS_country__c.getall().values();
        countryValues.sort();
        return countryValues;
    }
    /**
    * Description : Method to get Topic Values from Product object for Pre-chat form
    * @param NA
    * @return List<Product__c>
    * @throws NA
    **/
    @RemoteAction
    public static  List<Product__c> getTopicValues(String sCustType){
        String sTopicType='Topic';
        List<Product__c> objTopicList=[select id,role__c ,Business_Vertical__c,name from Product__c where Market__c=:System.label.PS_UKMarket and Type__c=:sTopicType and Role__c =:sCustType];
        objTopicList.sort();
       
        return objTopicList;
    }
    /**
    * Description : Method to get Category Values from Product object
    * @param String SBusVertical
    * @return List<Product__c>
    * @throws NA
    **/    
    @RemoteAction
    public static  List<Product__c> getCategoryValues(String SBusVertical){
        List<Product__c> objCatList=[select id,name from Product__c where Market__c=:System.label.PS_UKMarket and Business_Vertical__c=:SBusVertical and Type__c='Category'];
        objCatList.sort();        
        return objCatList;
    }
    /**
    * Description : Method to get Products Values from Product object
    * @param String SBusVertical
    * @return List<Product__c>
    * @throws NA
    **/        
    @RemoteAction
    public static  List<Product__c> getProductValues(String SBusVertical){
        SBusVertical = '%'+SBusVertical +'%';
        List<Product__c> objProdList=[select id,name from Product__c where Market__c=:System.label.PS_UKMarket and Business_Vertical__c Like: SBusVertical and (Type__c='Product' or type__c='Platform')];
        objProdList.sort();
        return objProdList;
    }
    /**
    * Description : Method to get SubCategory Values from Product object
    * @param String SCategory
    * @return List<String>
    * @throws NA
    **/
    @RemoteAction
    public static List<String> getCategorySubcategoryValue(String SCategory,String sCustType) 
    {        
        //PP: Setting this value as null because Platform value is getting fetch as null
        if(sCustType == ''){
            sCustType = null;
        }
        List<CategorySubcategoryMapping__c> CategorySubcategoryMappingList = CategorySubcategoryMapping__c.getall().values();
        List<String> sSubCategoryList = new List<String>();
        for(CategorySubcategoryMapping__c objCategorySubcategoryMapping:CategorySubcategoryMappingList){
            if(objCategorySubcategoryMapping.AACategory__c == SCategory && objCategorySubcategoryMapping.Platform__c == sCustType){
                String objSubCat = objCategorySubcategoryMapping.AASubcategory__c; 
                sSubCategoryList.addAll(objSubCat.split(','));
            }
        }
        
        
        return sSubCategoryList;
    }    
    /**
    * Description : Method to get Product Values from Product object for lighting component
    * @param NA
    * @return List<Product__c>
    * @throws NA
    **/
    @AuraEnabled
    public static  List<Product__c> getProductValuesforComponent(){
        String productType ='Product';
        String platformType ='Platform';
        List<Product__c> objProdList=[select id,role__c ,Business_Vertical__c,DependentField__c,name from Product__c where Market__c=: System.label.PS_UKMarket and (Type__c=:productType OR type__c=:platformType) and business_vertical__c != ''];
        objProdList.sort();
       
        return objProdList;
    }
    /**
    * Description : Method to get Category Values from Product object for lighting component
    * @param NA
    * @return List<Product__c>
    * @throws NA
    **/
    @AuraEnabled
    public static  List<Product__c> getCategoryValuesForRequestType(){
        String sCategoryType='Category';
        List<Product__c> objCatList=[select id,role__c ,Business_Vertical__c,name from Product__c where Market__c=:System.label.PS_UKMarket and Type__c=:sCategoryType ];
        objCatList.sort();
       
        return objCatList;
    }
    /**
    * Description : Method to get Topic Values from Product object for lighting component
    * @param NA
    * @return List<Product__c>
    * @throws NA
    **/
    @AuraEnabled
    public static  List<Product__c> getTopicValuesforComponent(){
        String sTopicType='Topic';
        List<Product__c> objTopicList=[select id,role__c ,Business_Vertical__c,name from Product__c where Market__c=:System.label.PS_UKMarket and Type__c=:sTopicType];
        objTopicList.sort();
       
        return objTopicList;
    }
    /**
    * Description : Method to get Qualification Subejct Values from Product object for lighting component
    * @param NA
    * @return List<Product__c>
    * @throws NA
    **/
    @AuraEnabled
    public static  List<Product__c> getQualificationSubjectValuesforComponent(){
        String sQualSubType ='Qualification Subject';
        List<Product__c> objQualSubList=[select id ,Role__c,Business_Vertical__c,name from Product__c where Market__c=: System.label.PS_UKMarket and Type__c=:sQualSubType];
        objQualSubList.sort();
       
        return objQualSubList;
    }
    /**
    * Description : Method to get Category Values from Product object for lighting component
    * @param String SBusVertical
    * @return List<Product__c>
    * @throws NA
    **/
    @AuraEnabled
    public static  List<Product__c> getCategoryValuesforComponent(String SBusVertical){
        String sCategoryType='Category';
        List<Product__c> objCatList=[select id,name from Product__c where Market__c=:System.label.PS_UKMarket and Business_Vertical__c=:SBusVertical and Type__c=:sCategoryType];
        objCatList.sort();
        return objCatList;
    }
    /**
    * Description : Method to get Roles Values from Product object for lighting component
    * @param String SBusVertical,String SCusType
    * @return List<Product__c>
    * @throws NA
    **/
    @AuraEnabled
    public static  List<Product__c> getRoleValuesforComponent(String SBusVertical,String SCusType){
        String sRoleType='Role';
        List<Product__c> objRoleList;
       objRoleList=[select id,name,Role__c,Business_Vertical__c from Product__c where Market__c=: System.label.PS_UKMarket and Type__c=:sRoleType];
        objRoleList.sort();
        return objRoleList;
    }
    /**
    * Description : Method to fetch current user's details for Lightning Components
    * @param NA
    * @return User
    * @throws NA
    **/
    @AuraEnabled
    public static User getCurrentUser() 
    {
        String message;
        User toReturn;
        String sProfile = 'Pearson UK Support Profile';
        try{
           toReturn = [SELECT Id, Name, FirstName, MobilePhone, LastName, Email, City, Country, State , UserName , Profile.Name ,
           Phone,accountid,contact.AA_Number__c  FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
           //Phone,accountid,account.Pearson_Customer_Number__c,contact.AA_Number__c  FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1]; For Apttus changes
         
            
          if( toReturn.Profile.Name != sProfile)
           {     
               return toReturn;  
           }
          else{
             return null;
          }
        }
        Catch(System.Exception e)
        {
            message=e.getMessage();
            return null;
        }
    }
    /**
    * Description : Method to get Product values from product__c Custom Object
    * @param String 
    * @return String 
    * @throws NA
    **/
     @AuraEnabled
    public static String getProductIdValue(String sProdName){
        String sProdId='';
        for (product__c prod :[select id from product__c where name=:sProdName limit 1])
        {
            sProdId=prod.Id;
        }
        return sProdId;    
    }
   
    /**
    * Description : Method to get Category SubCategory Mapping from Custom setting
    * @param NA
    * @return List<CategorySubcategoryMapping__c>
    * @throws NA
    **/
    @AuraEnabled
    public static List<CategorySubcategoryMapping__c> getCategorySubcategoryMapping() 
    {        
        List<CategorySubcategoryMapping__c> CategorySubcategoryMappingList = CategorySubcategoryMapping__c.getall().values();
        CategorySubcategoryMappingList.sort();
        return CategorySubcategoryMappingList;
    }
    /**
    * Description : Method to get Phone Mapping from Custom setting
    * @param NA
    * @return List<SelfServicePhoneMapping__c>
    * @throws NA
    **/
    @AuraEnabled
    public static List<SelfServicePhoneMapping__c> mgetphoneMapping() 
    {        
        List<SelfServicePhoneMapping__c> phoneMapping = SelfServicePhoneMapping__c.getall().values();
        phoneMapping.sort();
        return phoneMapping;
    }

    /**
    * Description : Method to get Contry Values
    * @param NA
    * @return List<CS_country__c>
    * @throws NA
    **/
    @AuraEnabled
    public static List<CS_country__c> mgetCountryValues() 
    {        
        List<CS_country__c> countryValues = CS_country__c.getall().values();
        countryValues.sort();
        return countryValues;
    }
    /**
    * Description : Method to create case and contact
    * @param Strings to stamp values to case object
    * @return String to with case number
    * @throws NA
    **/
    @AuraEnabled
    public static String mGetCaseDetails(String fileId,Id parentID,String fileName, String base64Data, String contentType,String vCountry,String vRecordType,String vcustomerType,String vBusiness,String vFirstName,String vLastName,String vEmail,String vPhone,String vcenterNumber,String vProduct,String vQualificationSubject,String vTopic,String vSubject,String vDescription,String vOrganisation,String vCategory,String vSubCategory,String vAAnumber) 
    {
         
         Integer accFlag = 0;
        List<Id> accIdList = new List<Id>();        
        String accountName = vOrganisation;
        Contact con=new Contact();
        String Success_Message;
        String errorMsg;
        String recordType_case = vRecordType;
        String recordType_contact = 'Global_Contact';
        List<RecordType> RecordTypeID_contact = [Select Id FROM RecordType WHERE SobjectType = 'Contact' and DeveloperName =: recordType_contact LIMIT 1] ;
        List<RecordType> RecordTypeID_case = [Select Id FROM RecordType WHERE SobjectType = 'Case' and DeveloperName =: recordType_case LIMIT 1] ;
        String query = 'SELECT Id,AA_Number__c FROM Contact WHERE Email =' + '\'' + vEmail + '\'' + 'Limit 1';
        LIST <Contact> conn = Database.query(query);
        Case Obj=new Case();
        try{
            if(conn.isEmpty())
            {
                con.LastName=vLastName;
                con.FirstName=vFirstName;
                con.Email=vEmail;
                con.Phone=vPhone;
                con.AA_Number__c=vAAnumber;
                con.Market__C = System.label.PS_UKMarket;
                RecordType RecordTypeID_contactobj=RecordTypeID_contact.get(0);
                con.RecordTypeId=RecordTypeID_contactobj.Id;
                Database.DMLOptions dml = new Database.DMLOptions(); 
                            dml.DuplicateRuleHeader.AllowSave = true;
                Database.insert(con, dml);
                Obj.ContactId=con.Id;
               if(vcenterNumber == '')
                     {
                    Obj.Contact_Organization_School__c = accountName; 
                     }
                    else
                    {
                       Obj.Contact_Organization_School__c = vcenterNumber; 
                    }
            }
            else{
                Contact existingcontactobj=conn.get(0);
                Obj.ContactId=existingcontactobj.Id;
                String AccountIdquery = 'SELECT AccountId FROM Contact WHERE id =' + '\'' + existingcontactobj.Id + '\'' + '';
                  LIST <Contact> accountID = Database.query(AccountIdquery);
                Contact existingcontactAccount=accountID.get(0);
                List<Account> accountList = [Select id,Name,Pearson_Customer_Number__c from Account where (name =: accountName OR Pearson_Customer_Number__c =: vcenterNumber) AND id =: existingcontactAccount.AccountId];
                for(Account acc:accountList){
                 accFlag++;          
                 accIdList.add(acc.id);
                }
                if(accFlag == 1){
                    Obj.Accountid = accIdList[0];
                }
                else
                {
                     if(vcenterNumber == '')
                     {
                    Obj.Contact_Organization_School__c = accountName; 
                     }
                    else
                    {
                       Obj.Contact_Organization_School__c = vcenterNumber; 
                    }
                }
            }
            Obj.Account_Primary_Contact_Email__c = vEmail;
            
            RecordType RecordTypeID_caseobj=RecordTypeID_case.get(0);
            Obj.RecordTypeId = RecordTypeID_caseobj.Id;
            obj.Subject=vSubject;
            if(vCountry != ''){
                obj.Global_Country__c=vCountry;
            }else{
                obj.Global_Country__c = System.Label.UkCaseCountry;
                obj.Global_Language__c = System.Label.UKCaseLanguage;
            }
            obj.PS_Business_Vertical__c=vBusiness;
            obj.Customer_Type__c=vcustomerType;
            obj.Status=System.label.PS_New;
           /* String sBusinessVertical='Qualifications';
            String ResBV='Resources';
            String AssosiateBV='PQS';
            if(vBusiness == sBusinessVertical || vBusiness == ResBV ||vBusiness == AssosiateBV)
            {*/
                obj.Request_Type__c=vTopic;
           // }
             
            obj.Category__c=vCategory;
            obj.Subcategory__c=vSubCategory;
            obj.Market__c=System.label.PS_UKMarket;
            
                if(vBusiness == 'Digital Support')
                {
                  obj.Platform__c=vProduct; 
                }
                else
                {
                    if(vProduct != '')
                    {
                       
                        product__c pro =[select id from product__c where name =: vProduct and type__c='Product' and market__c ='UK'];
                        String productID = pro.Id;
                        obj.Products__c=productID;
                       
                    }
                }
            obj.Origin=System.Label.PS_Web;
            obj.Qualification_Subject__c=vQualificationSubject;  
            obj.Contact_Email__c=vEmail;
            obj.SuppliedEmail=vEmail;
            obj.Priority='Medium';
            obj.SuppliedPhone=vPhone;
          //  obj.ContactPhone=vPhone;
            obj.Description=vDescription;
            insert obj;
            Case Casenum = [select CaseNumber,Market__c from Case where id =:Obj.Id Limit 1];
            String CaseNumber = Casenum.CaseNumber;
            parentId = Obj.Id;
            if(fileName != '' && fileName != null)
            {
             // ComplaintFeedbackSupport complainFileAtatch= new ComplaintFeedbackSupport(); 
            ComplaintFeedbackSupport.saveTheChunk(parentId, fileName,base64Data, contentType, fileId);    
               // saveTheFile(parentID,fileName, base64Data, contentType);
            }
            Success_Message= System.label.PS_SelfService_SuccessMessage+' '+CaseNumber+'.';
        }
        catch(dmlexception e) {
            errorMsg = e.getMessage();
            //if(errorMsg != null || errorMsg != ''){
            if(String.ISBLANK(errorMsg)){
                Success_Message =   System.label.PS_SelfService_ErrorMessage;
            }
        }
        system.debug('Success_Message'+Success_Message);
         return Success_Message;
        
    }
    /**
    * Description : Method to get Qualification Subject picklist values
    * @param NA
    * @return List<String>
    * @throws NA
    **/
    @AuraEnabled
    public Static List<String> mGetQualificationSubject() 
    {        
        List<String> options = new List<String>();  
        Schema.DescribeFieldResult fieldResultQualificationSubject = Case.Qualification_Subject__c.getDescribe(); 
        List<Schema.PicklistEntry> pleQualificationSubject = fieldResultQualificationSubject.getPicklistValues();           
        for (Schema.PicklistEntry f: pleQualificationSubject) {
            options.add(f.getLabel());
        }   
        return options; 
    }
    
    /**
    * Description : Method to get Product values
    * @param NA
    * @return List<product__c>
    * @throws NA
    **/
    @AuraEnabled
    public Static List<product__c> mGetProduct() 
    {        
        List<product__c> vProductList = [select name from product__c];
        vProductList.sort();
        return vProductList;
    }
  
    /**
    * Description : Method to fetch values button id for a unique set of parameters
    * @param String sQualSubject,String sBusVertical
    * @return List<String>
    * @throws NA
    **/
    @AuraEnabled
    public static List<String> getButtonId(String sQualSubject,String sBusVertical) 
    {        
               
        List<String> sChatBtnlst=new List<String>();
        try{
            List<UK_FetchChatButton__mdt> lstChatButton = new List<UK_FetchChatButton__mdt>();
            //String query = 'SELECT Button_id__c,Deployment_Id__c,Recordtype__c from UK_FetchChatButton__mdt  WHERE Qualification_Subject__c=' + '\'' + sQualSubject + '\' LIMIT 1' ;
            String query = 'SELECT Button_id__c,Deployment_Id__c,Recordtype__c from UK_FetchChatButton__mdt WHERE BusinessVertical__c=' + 
                           '\'' + sBusVertical + '\' AND Qualification_Subject__c=' + 
                            '\'' + sQualSubject + '\' LIMIT 1' ;            
           
            lstChatButton= Database.query(query);
            if (lstChatButton.size()>0)
                sChatBtnlst=getChatButtonId(lstChatButton[0].Button_id__c,lstChatButton[0].Deployment_Id__c,lstChatButton[0].Recordtype__c);
            return sChatBtnlst;
        }
        catch(System.Exception e)
        {
            return null;
        }
    }
    /**
    * Description : Method to fetch values button details
    * @param String sButtonName,String sDepName,String sRTName
    * @return List<String>
    * @throws NA
    **/
    @AuraEnabled
    public static List<String> getChatButtonId(String sButtonName,String sDepName,String sRTName){
       
        List<String> lstChatButDets=new List<String>();
        List<LiveChatButton> listlcb = [select id,developername,masterlabel from LiveChatButton where developername=:sButtonName limit 1];
        if (listlcb.size()>0){
            lstChatButDets.add(listlcb[0].id);
        }
        else
            lstChatButDets.add(' ');
        List<LiveChatDeployment> listlcd = [select id,developername,masterlabel from LiveChatDeployment where developername=:sDepName limit 1];
        if (listlcd.size()>0){
            lstChatButDets.add(listlcd[0].id);
        }
        else
            lstChatButDets.add(' ');   
        List<RecordType> lstRecType = [select id,developername,name from RecordType where developername=:sRTName limit 1];
        if (lstRecType.size()>0){
            lstChatButDets.add(lstRecType[0].id);
        }
        else
            lstChatButDets.add(' ');               
        return lstChatButDets;
    }   
    /**
    * Description : Method to get Organization value Dynamically
    * @param String sButtonName,String sDepName,String sRTName
    * @return List<String>
    * @throws NA
    **/ 
    @AuraEnabled
    public static String getOrganization()
    {  
       return UserInfo.getOrganizationId();    
    } 
    /**
    * Description : Method to get Agent messages Dynamically
    * @param String sButtonName,String sDepName,String sRTName
    * @return List<String>
    * @throws NA
    **/ 
    @AuraEnabled
    public static List<String> getAgentMessages()
    {  
       List<String> agentMessages=new List<String>();
       String online = System.label.PS_AgentOnline; 
       String offline=  System.label.PS_AgentOffline;
       agentMessages.add(online);
       agentMessages.add(offline);
       return agentMessages;    
    }
     /**
    * Description : Method to get Knowledge Articles
    * @param String
    * @return List<Customer_Support__kav>
    * @throws NA
    **/
    @AuraEnabled
    public static List<Customer_Support__kav> mGetValues(List<String> vSearchTitle) 
    {        
        //String vSearchInputText = '%'+ vSearchTitle + '%';
        
        List<Customer_Support__kav> limitedarticle;
        try{
            if(vSearchTitle.size()>0)
            {
                Map<Id,Customer_Support__kav> lstMap = new Map<Id,Customer_Support__kav>(); 
                List<Id> articleIds = new List<Id>();
                /*for(TopicAssignment topicAs: [Select EntityId from TopicAssignment where topic.Name IN: vSearchTitle ])
                {
                    articleIds.add(topicAs.EntityId);
                } */
                AggregateResult[] groupedResults = [Select Count(Id),EntityId  from TopicAssignment where topic.Name IN: vSearchTitle Group BY EntityId order by Count(Id) DESC Limit 5];
                for (AggregateResult ar : groupedResults){
                    articleIds.add(String.valueOf(ar.get('EntityId')));
                }              
                LIST<Customer_Support__ViewStat> Normalizedarticle = [SELECT parentId FROM Customer_Support__ViewStat 
                    WHERE parentId in (SELECT KnowledgeArticleId FROM Customer_Support__kav
                    WHERE publishStatus = 'Online' AND language = 'en_US'
                    AND  Id in: articleIds) and channel = 'All Channels' Order BY ViewCount DESC ]; 
                
                List<Id> lstKnwArticleIds = new List<id>();
                for(Customer_Support__ViewStat objNorstat: Normalizedarticle){
                    lstKnwArticleIds.add(objNorstat.parentId);
                }
                 limitedarticle = [SELECT Id,KnowledgeArticleId,Title, UrlName, Summary FROM Customer_Support__kav WHERE PublishStatus ='Online' AND Language ='en_US' AND KnowledgeArticleId IN: lstKnwArticleIds];
            }
                return limitedarticle;
            
        }
        catch(System.Exception e)
        {
            String Message=e.getMessage();
            return null;
        }
    }
    /**
    * Description : Method to get FeedItems Questions
    * @param String sButtonName,String sDepName,String sRTName
    * @return List<String>
    * @throws NA
    **/ 
    @AuraEnabled
    public Static List<FeedItem> mGetFeedItem(List<String> vSearchTitle)
    {
       // String vSearchInputText = '%'+ vSearchTitle + '%';
        List<FeedItem> ofeedItem;
        try{
            if(vSearchTitle.size()>0)
            {
                Map<Id,FeedItem> lstMap = new Map<Id,FeedItem>(); 
                Set<Id> lstFeedItemId = new Set<Id>();
                 AggregateResult[] groupedResults = [Select Count(Id),EntityId  from TopicAssignment where topic.Name IN: vSearchTitle and EntityType = 'FeedItem' Group BY EntityId order by Count(Id) DESC Limit 5];
                for (AggregateResult ar : groupedResults){
                    lstFeedItemId.add(String.valueOf(ar.get('EntityId')));
                } 
              /*  for(TopicAssignment topicAs: [Select EntityId,TopicId,Topic.Name from TopicAssignment where topic.Name LIKE :vSearchTitle and EntityType = 'FeedItem'])
                {
                    lstFeedItemId.add(topicAs.EntityId);
                }*/
                
                ofeedItem = [Select Id,Title,Body,LikeCount  from FeedItem where type='QuestionPost' and Id In: lstFeedItemId Order by LikeCount DESC LIMIT 5];
            system.debug('ofeedItem'+ofeedItem);
            }
            return ofeedItem; 
            }
            catch(System.Exception e)
            {
                String Message=e.getMessage();
                return null;
            }
    }
    //Method definition to send articles through emails to customers
    @AuraEnabled
    public static void articlesendEmail(String yourEmail, string url, string articleName, string recEmail, string firstName, 
                                          string recName) {       
        Outbound_Notification__c outnot = new Outbound_Notification__c();
        outnot.Event__c = 'Article';
        outnot.Method__c = 'Email';
        outnot.Type__c = 'Article Outbound';
        outnot.PS_ArticleRecipient__c=recEmail;
        outnot.PS_ArticleCCRecipient__c=yourEmail;
        outnot.PS_ArticleName__c=articleName;
        outnot.PS_ArticleURL__c=url;
        outnot.PS_RecipientName__c=recName;
        outnot.PS_Sender_Name__c=firstName;                                      
        try{
            insert outnot;}
        catch(Exception e){
            throw(e);                                          
        }                                     
    }
    
  
}