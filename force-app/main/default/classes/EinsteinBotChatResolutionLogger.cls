/**
 * Apex Class Name : EinsteinBotChatResolutionLogger
 * Version         : 1.0
 * Created On      : 12-07-2018
 * Function        : A test class for EinsteinBotReportingBatch and EinsteinBotReportingBatchHelper
 * Test Class      : EinsteinBotChatResolutionLoggerTest
 */

public with sharing class EinsteinBotChatResolutionLogger {
        @InvocableMethod(label='Einstein Bot - Chat Resolution Logger')
    Public static void logResolutionSuccess(List<ChatLoggerRequest> chatLogRequests) {
        List<Einstein_Bot_Event__e> chatLogEvents = new List<Einstein_Bot_Event__e>();
        for(ChatLoggerRequest chatLogRequest : chatLogRequests) {
            Einstein_Bot_Event__e chatLogEvent = new Einstein_Bot_Event__e();
            chatLogEvent.Type__c = EinsteinBotEventHandler.LOG_CHAT_RESOLUTION;
            if(chatLogRequest.resolvedTrue == 'Yes') {
                chatLogEvent.Successful_Resolution__c = 'Yes';
            } else if (chatLogRequest.resolvedTrue == 'No')  {
                chatLogEvent.Successful_Resolution__c = 'No';
            } else {
                chatLogEvent.Successful_Resolution__c = 'N/A';
            }
            chatLogEvent.Live_Agent_Session_Id__c = chatLogRequest.liveAgentSessionId;
            chatLogEvents.add(chatLogEvent);

        }
        List<Database.SaveResult> saveResults = EventBus.publish(chatLogEvents);
    }

    Public class ChatLoggerRequest {
        @InvocableVariable(required=true)
        public String liveAgentSessionId;
        @InvocableVariable
        public String resolvedTrue;
    }

}