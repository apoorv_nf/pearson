/************************************************************************************************************
* Apex Class Name   : UpdateTeamMember.cls
* Version           : 1.1
* Function          : Class used for setting account team memeber ansd providing access to team members
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Leonard Victor              26/08/2015              Updated code as part of R3 code stream lining
* Srikanth Reddy              26/03/2019              CR-02663 - Adjust Communities Created Account Ownership
************************************************************************************************************/

Public without sharing Class UpdateTeamMember
{
    public static Map<Id,Boolean> needUpdateAccountShare = new Map<Id,Boolean>();
    
    public static void updateAccountOwner(list<account> triggeredAccountList){
        Id learnerRecordType;
        Id orgRecordType;
        Id corporateRecordType;
        Id higherEdRecordType;
        Id schoolRecordType;
        Id professionalrecordType;
        Id accountCreateRequest; //Added by srikanth for CR-02663
        //Using Util class to fetch record type instead of SOQL
       if(PS_Util.recordTypeMap.containsKey(PS_Constants.ACCOUNT_OBJECT) && 
         (PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).containsKey(PS_Constants.ACCOUNT_LEARNER_RECCORD) && 
          PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).containsKey(PS_Constants.ACCOUNT_ORGANISATION_RECCORD) &&
          PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).containsKey(PS_Constants.ACCOUNT_CORPORATE_RECCORD) &&
          PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).containsKey(PS_Constants.ACCOUNT_HIGHED_RECCORD) &&
          PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).containsKey(PS_Constants.ACCOUNT_SCHOOL_RECCORD) &&
          PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).containsKey(PS_Constants.ACCOUNT_PROFESSIONAL_RECCORD)&&
          PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).containsKey(PS_Constants.ACCOUNT_CREATION_REQUEST)//Added by srikanth for CR-02663
          ))
       {                          
           learnerRecordType = PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).get(PS_Constants.ACCOUNT_LEARNER_RECCORD); 
           orgRecordType = PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).get(PS_Constants.ACCOUNT_ORGANISATION_RECCORD);
           corporateRecordType = PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).get(PS_Constants.ACCOUNT_CORPORATE_RECCORD);
           higherEdRecordType = PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).get(PS_Constants.ACCOUNT_HIGHED_RECCORD);
           schoolRecordType = PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).get(PS_Constants.ACCOUNT_SCHOOL_RECCORD);
           professionalrecordType  = PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).get(PS_Constants.ACCOUNT_PROFESSIONAL_RECCORD);
           accountCreateRequest = PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).get(PS_Constants.ACCOUNT_CREATION_REQUEST);//Added by srikanth for CR-02663
       }
       else
       {
          learnerRecordType = PS_Util.fetchRecordTypeByName(Account.SobjectType,Label.PS_LearnerRecordType);
          orgRecordType = PS_Util.fetchRecordTypeByName(Account.SobjectType,Label.PS_OrganisationRecordType);
          corporateRecordType = PS_Util.fetchRecordTypeByName(Account.SobjectType,Label.Corporate);
          higherEdRecordType = PS_Util.fetchRecordTypeByName(Account.SobjectType,PS_Constants.ACCOUNT_HIGHED_RECCORD);
          schoolRecordType = PS_Util.fetchRecordTypeByName(Account.SobjectType,PS_Constants.ACCOUNT_SCHOOL_RECCORD);
          professionalrecordType  = PS_Util.fetchRecordTypeByName(Account.SobjectType,Label.PS_LeadOrgTypeProfessional);
          accountCreateRequest  = PS_Util.fetchRecordTypeByName(Account.SobjectType,PS_Constants.ACCOUNT_CREATION_REQUEST);//Added by srikanth for CR-02663
        }       
        
        //End of Record type fix  
        Boolean isException = false;
        List<ID> accountIds = new List<ID>();
        List<Account> accountList = new List<Account>();
        //Added as part of 101 SOQL error after SP,Lex and CI changes deploy
        //Profile currentProfile;
        User u = new User();
        try{
             u = [SELECT id,name FROM User WHERE name =: System.Label.OwnerForAccount and UserType='Standard'];
        }catch(Exception ex){
             isException = true;
        }        
        for(Account acc : triggeredAccountList){
           accountIds.add(acc.id);
            if(isException){
                acc.addError(System.Label.ErrorMsgForAccountReAssign);
            } 
        } 

        accountList = [SELECT RecordType.name,ownerId FROM Account 
                       WHERE id IN : accountIds];
        //Added as part of 101 SOQL error after SP,Lex and CI changes deploy
        //currentProfile = [SELECT name FROM Profile WHERE id =: UserInfo.getProfileId()];
                               
        for(Account acc : accountList){
            if( UserInfo.getName() != System.Label.OwnerForAccount  &&  (acc.RecordTypeid==learnerRecordType   || acc.RecordTypeid == orgRecordType ||
                                                                         acc.RecordTypeid==corporateRecordType || acc.RecordTypeid == higherEdRecordType ||
                                                                         acc.RecordTypeid==schoolRecordType    || acc.RecordTypeid == professionalrecordType ||
                                                                         acc.RecordTypeid == accountCreateRequest)){ //Added by srikanth for CR-02663
                    acc.ownerId = u.id;
                }
        
        }
        Database.update(accountList);
    }
    
    public static void addAccountTeamMember(list<Account> triggeredNewAccountList){
        Id learnerRecordType;
        Id orgRecordType;       
        
       //Using Util class to fetch record type instead of SOQL
       if(PS_Util.recordTypeMap.containsKey(PS_Constants.ACCOUNT_OBJECT) && (PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).containsKey(PS_Constants.ACCOUNT_LEARNER_RECCORD) && PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).containsKey(PS_Constants.ACCOUNT_ORGANISATION_RECCORD))){                          
           learnerRecordType = PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).get(PS_Constants.ACCOUNT_LEARNER_RECCORD); 
           orgRecordType = PS_Util.recordTypeMap.get(PS_Constants.ACCOUNT_OBJECT).get(PS_Constants.ACCOUNT_ORGANISATION_RECCORD);
       }
       else{
          learnerRecordType = PS_Util.fetchRecordTypeByName(Account.SobjectType,Label.PS_LearnerRecordType);
          orgRecordType = PS_Util.fetchRecordTypeByName(Account.SobjectType,Label.PS_OrganisationRecordType);
       }
                
        //End of Record type fix  

        List<id> AcountIdLst = new List<id>();
        String Recordtype; 
        User accountOwner = new User();
        List<Account> accountList = new List<Account>(); 
        //Added as part of 101 SOQL error after SP,Lex and CI changes deploy
        //List<ObjectPermissions> objPerm = new List<ObjectPermissions>();
        Profile currentProfile;
        //Commented SOQL on user and profile and taking value from PS_Util Class R3 code streamline
         //accountOwner = [Select ProfileId,id FROM User WHERE Id =: UserInfo.getUserId()];
        // currentProfile = [SELECT Name FROM profile WHERE Id =: UserInfo.getProfileId()];
        if(PS_Util.loggedInUser!=null){
            accountOwner = PS_Util.loggedInUser;
        }
        if(PS_Util.loggedInUserProfile!=null){
            currentProfile = PS_Util.loggedInUserProfile;
        }
        //End of Util logic for User and profile
        //Added as part of 101 SOQL error after SP,Lex and CI changes deploy
        //Object Permissions 
        /*objPerm = [SELECT Id, SObjectType, PermissionsRead, PermissionsCreate,PermissionsEdit
                   FROM ObjectPermissions 
                   WHERE parentid in (SELECT id 
                                      FROM permissionset 
                                      WHERE PermissionSet.Profile.Name =: currentProfile.Name )] ;
            
         */
        List<AccountTeamMember> accountTeamMembers = new List<AccountTeamMember>();
        
        for(Account acc : triggeredNewAccountList){
            Boolean b2bLeadAcc = acc.Lead_Conversion_Record_Type__c != null && acc.Lead_Conversion_Record_Type__c.equals(System.Label.Lead_B2B_RecordType);
            
            if(currentProfile.Name != PS_ProfileNames.PEARSON_LOCAL_ADMINISTRATOR  &&   currentProfile.Name != PS_ProfileNames.SYSTEM_ADMINISTRATOR ){
                   if(acc.recordtypeid == learnerRecordType || acc.recordtypeid == orgRecordType){
                     if(!b2bLeadAcc)
                     {
                         AccountTeamMember accTeamMember = new AccountTeamMember();
                         accTeamMember.AccountId = acc.Id;
                         accTeamMember.TeamMemberRole = 'Team Member';
                         accTeamMember.userid = accountOwner.id;
                         accountTeamMembers.add(accTeamMember);
                         needUpdateAccountShare.put(acc.id,true) ;
                     }
                    }
              }
         }       
         
         if(!accountTeamMembers.isEmpty())
            Database.insert(accountTeamMembers);
        }
    
    public static Void UpdateAccountShare(List<Account> triggeredAccountList){
        
        List<AccountShare> newShare = new List<AccountShare>();
        List<Accountshare> updatedAccountShares = new List<Accountshare>();
        
        List<AccountShare> shares = [SELECT Id,CaseAccessLevel,accountId,AccountAccessLevel,OpportunityAccessLevel,ContactAccessLevel,UserOrGroupId,RowCause  
                                      FROM AccountShare 
                                      WHERE accountId IN : triggeredAccountList
                                      AND UserOrGroupId =: UserInfo.getUserId()];
         
         List<ObjectPermissions> AccEditPermission=[SELECT Id, SObjectType, PermissionsEdit, 
                                                          Parent.label, Parent.IsOwnedByProfile 
                                                   FROM ObjectPermissions 
                                                   WHERE (ParentId IN (SELECT PermissionSetId 
                                                                       FROM PermissionSetAssignment 
                                                                       WHERE Assignee.Id =: UserInfo.getUserId() )) 
                                                                       AND (PermissionsEdit = true) 
                                                                       AND (SobjectType =:PS_Constants.ACCOUNT_OBJECT)];
         for (AccountShare sha : shares) { 
             if(AccEditPermission.size()>0 && !needUpdateAccountShare.isEmpty() && needUpdateAccountShare.get(sha.AccountId)){
                   AccountShare tempAccShare = new AccountShare();  
                   tempAccShare.Id = sha.id; 
                   
                   if(sha.OpportunityAccessLevel =='read' || sha.CaseAccessLevel == 'None'){
                       sha.OpportunityAccessLevel = 'Edit';
                       tempAccShare.OpportunityAccessLevel = 'Edit';
                   }
                   if(sha.CaseAccessLevel =='read' || sha.CaseAccessLevel == 'None'){
                       sha.CaseAccessLevel = 'Edit';
                       tempAccShare.CaseAccessLevel = 'Edit';
                   }
                   if(sha.ContactAccessLevel =='read' || sha.ContactAccessLevel == 'None'){
                       sha.ContactAccessLevel = 'Edit';
                       tempAccShare.ContactAccessLevel = 'Edit';
                   }
                     if(sha.AccountAccessLevel =='read' || sha.AccountAccessLevel == 'None'){
                       sha.AccountAccessLevel = 'Edit';
                       tempAccShare.AccountAccessLevel = 'Edit';
                    }
                   
                   updatedAccountShares.add(tempAccShare);
             }
         }
         
         if(updatedAccountShares.size()>0){
             update updatedAccountShares;
         }
         
       
     }
 }