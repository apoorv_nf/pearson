/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:         PS_leadidvalidationtest.cls 
   Description:  Test Class For  To check valid identification number on lead
                
   Date             Version      Author                  Tag     Summary of Changes 
   -----------      -------     -----------------       ---     ------------------------------------------------------------------------
   Karthik.A.S        1.0         21/01/2016              To check valid identification number on lead
  
  -------------------------------------------------------------------------------------------------------------------------------------------------------- */
@isTest
private class PS_leadidvalidationtest
{ 
     static testMethod void leadidvalidationtest() {
        Test.startTest();
        PS_leadidvalidation.dummy();
        Test.stopTest();
    }
   
/** Commented by Raja gopalarao B as part of CR-02923 
 
  static testMethod void leadidvalidationtest() 
  {
    List<lead> testlead = TestDataFactory.createlead(1,'D2L'); 
         
    lead testlead1 = testlead[0];
    
   // testlead1.Identification_Number__c='112124124';
    try{
    insert testlead1;
    
    }
    catch(exception e){}
        
    
    
    }
    static testMethod void leadidvalidationtest1() 
   {
    List<lead> testlead = TestDataFactory.createlead(1,'D2L'); 
         
    lead testlead1 = testlead[0];
    
    testlead1.Identification_Number__c='00000';
   try{
    insert testlead1;
    }
     catch(exception e){}
    
    }**/
    }