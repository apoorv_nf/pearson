/* --------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_ChatterPostUtilities
Description:     A utility class that is used for chatter posting
Date             Version           Author                                          Summary of Changes 
-----------      ----------   -----------------     ---------------------------------------------------------------------------------------
30th Sep 2015      1.0           Accenture NDC                                         Created

---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class PS_ChatterPostUtilities
{
    public static void createPostOnAccount(Map<Id,Id> mapObjUserID){
        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
        
        for(Id objId : mapObjUserID.keySet()){
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            feedItemInput.subjectId = objId;
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            mentionSegmentInput.id = mapObjUserID.get(objId);
            messageBodyInput.messageSegments.add(mentionSegmentInput);
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            textSegmentInput.text = ' '+Label.PS_AccountChatterPostMessage;
            messageBodyInput.messageSegments.add(textSegmentInput);
            feedItemInput.body = messageBodyInput;
            ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
            batchInputs.add(batchInput);
        }
        ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
    }
    
    // Added by Shiva - Start    
    // This method created for the GRL Opportunity ChatterPost  
    public static void createPostOnOpportunity(Map<Id, Map<String, String>> details){
        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
        
        for(Id recordId : details.keySet()){
            Map<String, String> detail = details.get(recordId);  
            
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            
            mentionSegmentInput.id = detail.get('userId');
            messageBodyInput.messageSegments.add(mentionSegmentInput);                        
            textSegmentInput.text = detail.get('status');
            messageBodyInput.messageSegments.add(textSegmentInput);
            
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = detail.get('recordId');
            ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
            batchInputs.add(batchInput);
        }
        ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
    }
    // Added by Shiva - End
}