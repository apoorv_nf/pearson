/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_AccountContactCreation.cls 
   Description:     To create AccountContact records based oncase creation and updation
   Date              Version            Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
    17/4/2016         1.0            Sudhakar Navuluri                     Initial Release 
    21/4/2016         2.0            Sudhakar Navuluri                     Amended code 
    21/4/2016         3.0            Sudhakar Navuluri                     Added Logic for Role,Role Detail
-------------------------------------------------------------------------------------------------------------------------------------------------------------*/  
public without sharing class PS_AccountContactCreation
{
 /**
    * Description : Method to create AccountContact records based oncase creation and updation
    * @param List<Case> newCases
    * @return NA
    * @throws NA
    **/
    public static void mAccountContactCreation(List<Case> triggeredcases,boolean isInsert,boolean isUpdate,Map<Id,Case> oldMap)
    { 
        List<Case> newcases =  CommonUtils.getCaseServiceRecordType(System.Label.PS_ServiceRecordTypes, triggeredcases);
        List<AccountContact__c> accConReclist=new List<AccountContact__c>();
        List<AccountContact__c > aclist= new List<AccountContact__c>();
        if (newcases != null)
        {   
            if(isInsert)
            {
                for(Case c: newcases)
                {
                    if(c.AccountId != null &&  c.ContactId != null)
                    {
                        accConReclist= [Select Id,Account__c,Contact__c from AccountContact__c where Account__c=:c.AccountId and Contact__c=:c.ContactId];
                        if(accConReclist.size() ==0)
                        {    
                             AccountContact__c acRec=new AccountContact__c();
                             Map <String, PS_ContactRole__c > csRole = PS_ContactRole__c.getAll();    
                             if (csRole.containsKey(c.Contact_Type__c)) 
                             {      
                                 acRec.AccountRole__c = csRole.get(c.Contact_Type__c).RoleName__c;
                                 acRec.Role_Detail__c = csRole.get(c.Contact_Type__c).RoleDetail__c;
                             }                                                                                        
                            acRec.Account__c =c.AccountId;
                            acRec.Contact__c =c.ContactId;             
                            aclist.add(acRec);                          
                        }                       
                    }
                }
                if (aclist.size() > 0) 
                {
                    try {
                            insert aclist;   
                        }
                    catch(Exception e){
                        system.debug('** Account Contact Creation Error ** '+e);
                        }                                           
                }  
            }
            
            if(isUpdate)
            {            
                for(Case c: newcases)
                { 
                   if(c.AccountId != null &&  c.ContactId != null && (c.AccountId != oldMap.get(c.Id).AccountId || c.ContactId != oldMap.get(c.Id).ContactId))
                   {
                       accConReclist= [Select Id,Account__c,Contact__c from AccountContact__c where Account__c=:c.AccountId and Contact__c=:c.ContactId];
                       if(accConReclist.size() ==0)
                        {
                            AccountContact__c acRec=new AccountContact__c();
                            Map <String, PS_ContactRole__c > csRole = PS_ContactRole__c.getAll();    
                             if (csRole.containsKey(c.Contact_Type__c)) 
                             {      
                                 acRec.AccountRole__c = csRole.get(c.Contact_Type__c).RoleName__c;
                                 acRec.Role_Detail__c = csRole.get(c.Contact_Type__c).RoleDetail__c;
                             }  
                            acRec.Account__c =c.AccountId;
                            acRec.Contact__c =c.ContactId;             
                            aclist.add(acRec);  
                        }                  
                   }                   
                }
                if (aclist.size() > 0) 
                {
                    try {
                            insert aclist;   
                        }
                    catch(Exception e){
                        system.debug('** Account Contact Creation Error ** '+e);
                        }                                           
                }  
            }
        } 
    }
}