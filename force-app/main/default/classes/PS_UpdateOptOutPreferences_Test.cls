/*----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_UpdateOptOutPreferences_Test.cls 
   Description:     Test class for PS_UpdateOptOutPreferences.cls
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  15/01/2016         1.0            Rahul Boinepally                       Initial Release 
  ---------------------------------------------------------------------------------------------------------------------------------------------------------- */


@isTest
private class PS_UpdateOptOutPreferences_Test {

    
    static testmethod void testESB_OptOut_Success() 
    {   
        
        List<Profile> esbID = [Select ID from Profile where Name = 'One CRM Integration'];
        system.debug(esbID[0].Id);
        List<User> usrLst = TestDataFactory.createUser(esbID[0].Id);

        for(User u : usrLst)
        {
            u.LastName = 'ESB'; 
        }

        insert usrLst;
        System.runas(usrLst[0])
        {       

        List<Case> cases = TestDataFactory.createCase(1, 'General');
        for(Case cc: cases)
        {           
            cc.Type = 'Opt Out';
        }
        insert cases;
        List<Case> caseUpdates = new List<Case>();
        List<Case> caseUpdates1 = new List<Case>();

        Test.startTest();  
        for(Case cc: cases)
        {
            cc.Status = 'Submitted for Approval';
            caseUpdates.add(cc);
        }

        update caseUpdates;
        

        for(Case cc:cases)
        {
            cc.Status = 'Approved';
            cc.HasOptedOutOfEmail__c = True;
            cc.Global_Marketing_Unsubscribe__c = True;
            caseUpdates1.add(cc);
        }       
        update caseUpdates1;    

        Test.stopTest();        

        Set<Id> con = new Set<Id>();

        for(Case cc: cases){
            con.add(cc.contactId);
        }
        
        List<Contact> conList = [Select c.HasOptedOutOfEmail,Global_Marketing_Unsubscribe__c from Contact c where c.ID IN :con];

        for(Contact cc : conList)
        {           
            //system.assertEquals(True, cc.HasOptedOutOfEmail);   
            //system.assertEquals(True,cc.Global_Marketing_Unsubscribe__c);
            
        }
        
        }
    }

    static testmethod void testESB_ReOptIn_Success() 
    {   
        
        List<Profile> esbID = [Select ID from Profile where Name = 'One CRM Integration'];
        system.debug(esbID[0].Id);
        List<User> usrLst = TestDataFactory.createUser(esbID[0].Id);

        for(User u : usrLst)
        {
            u.LastName = 'ESB';
        }

        insert usrLst;
        System.runas(usrLst[0])
        {
        

        List<Case> cases = TestDataFactory.createCase(1, 'General');
        for(Case cc: cases)
        {           
            cc.Type = 'Re-Opt In';
        }
        insert cases;
        List<Case> caseUpdates = new List<Case>();
        List<Case> caseUpdates1 = new List<Case>();

        Test.startTest();  

        for(Case cc: cases)
        {
            cc.Status = 'Submitted for Approval';
            caseUpdates.add(cc);
        }

        update caseUpdates;
        

        for(Case cc:cases)
        {
            cc.Status = 'Approved';
            cc.HasOptedOutOfEmail__c = false;
            cc.Global_Marketing_Unsubscribe__c = false;
            caseUpdates1.add(cc);
        }       
        update caseUpdates1;    

        Test.stopTest();        

        Set<Id> con = new Set<Id>();

        for(Case cc: cases){
            con.add(cc.contactId);
        }
        
        List<Contact> conList = [Select c.HasOptedOutOfEmail,Global_Marketing_Unsubscribe__c from Contact c where c.ID IN :con];

        for(Contact cc : conList)
        {           
            system.assertEquals(false, cc.HasOptedOutOfEmail);          
            system.assertEquals(false,cc.Global_Marketing_Unsubscribe__c);
            
        }
        
        }
    }

    static testmethod void testESB_OptOut_Failure() 
    {   
        PS_ExceptionLogger Logger = new PS_ExceptionLogger();

        List<Profile> esbID = [Select ID from Profile where Name = 'One CRM Integration'];
        system.debug(esbID[0].Id);
        List<User> usrLst = TestDataFactory.createUser(esbID[0].Id);        

        insert usrLst;
        System.runas(usrLst[0])
        {

        List<Case> cases = TestDataFactory.createCase(1, 'General');
        for(Case cc: cases)
        {           
            cc.Type = 'Opt Out';
        }
        insert cases;
        List<Case> caseUpdates = new List<Case>();
        List<Case> caseUpdates1 = new List<Case>();

        Test.startTest();  

        for(Case cc: cases)
        {
            cc.Status = 'Submitted for Approval';
            caseUpdates.add(cc);
        }

        update caseUpdates;

        for(Case cc:cases)
        {
            cc.Status = 'Approved';
            cc.HasOptedOutOfEmail__c = True;
            cc.Global_Marketing_Unsubscribe__c = True;
            caseUpdates1.add(cc);
        }       
        try{
            update caseUpdates1;    
        }catch(exception e){
            
            system.assertNotEquals(' ',e.getMessage(),'error - only ESB user can change case status');
            
        }

        Test.stopTest();        

        Set<Id> con = new Set<Id>();

        for(Case cc: cases){
            con.add(cc.contactId);
        }
        
        List<Contact> conList = [Select c.HasOptedOutOfEmail,c.Global_Marketing_Unsubscribe__c from Contact c where c.ID IN :con];

        for(Contact cc : conList)
        {           
            system.assertNotEquals(True,cc.HasOptedOutOfEmail);         
            system.assertNotEquals(True,cc.Global_Marketing_Unsubscribe__c);
            
        }
        }
    }   

    static testmethod void testESB_ReOptIn_Failure() 
    {   
        PS_ExceptionLogger Logger = new PS_ExceptionLogger();

        List<Profile> esbID = [Select ID from Profile where Name = 'One CRM Integration'];
        system.debug(esbID[0].Id);
        List<User> usrLst = TestDataFactory.createUser(esbID[0].Id);        

        insert usrLst;
        System.runas(usrLst[0])
        {

        List<Case> cases = TestDataFactory.createCase(1, 'General');
        for(Case cc: cases)
        {           
            cc.Type = 'Re-Opt In';
        }
        insert cases;
        List<Case> caseUpdates = new List<Case>();
        List<Case> caseUpdates1 = new List<Case>();

        Test.startTest();  

        for(Case cc: cases)
        {
            cc.Status = 'Submitted for Approval';
            caseUpdates.add(cc);
        }

        update caseUpdates;

        for(Case cc:cases)
        {
            cc.Status = 'Approved';
            cc.HasOptedOutOfEmail__c = True;
            cc.Global_Marketing_Unsubscribe__c = True;
            caseUpdates1.add(cc);
        }       
        try{
            update caseUpdates1;    
        }catch(exception e){
            
            system.assertNotEquals(' ',e.getMessage(),'error - only ESB user can change case status');

        }

        Test.stopTest();        

        Set<Id> con = new Set<Id>();

        for(Case cc: cases){
            con.add(cc.contactId);
        }
        
        List<Contact> conList = [Select c.HasOptedOutOfEmail,c.Global_Marketing_Unsubscribe__c from Contact c where c.ID IN :con];

        for(Contact cc : conList)
        {           
            system.assertNotEquals(True,cc.HasOptedOutOfEmail);         
            system.assertNotEquals(True,cc.Global_Marketing_Unsubscribe__c);
            
        }
        }
    }   

}