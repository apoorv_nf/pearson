@isTest
public class PS_AccountOperationsTest{
    static testMethod void  myUnitTest() { 
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Local Administrator OneCRM'].Id,1);
        if(listWithUser != null && !listWithUser.isEmpty()){
            insert listWithUser;
        }        
        System.assert(listWithUser[0].id != null, 'User creation failed'); 
        system.runas(listWithUser[0]){
            list<Account> accountdatalist = TestDataFactory.createAccount(2,PS_Constants.ACCOUNT_LEARNER_RECCORD); 
            accountdatalist[0].Pearson_Campus__c = false;
            accountdatalist[1].Pearson_Campus__c = false;
            Insert accountdatalist;  
                
            System.assert(accountdatalist[0].id != null, 'Account creation failed'); 
            
        }
    }
    static testMethod void  myUnitTest1() { 
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'One CRM Integration'].Id,1);
        listWithUser[0].Market__c = 'UK';
        if(listWithUser != null && !listWithUser.isEmpty()){
            insert listWithUser;
        }        
        System.assert(listWithUser[0].id != null, 'User creation failed'); 
        
            list<Account> accountdatalist = TestDataFactory.createAccount(2,PS_Constants.ACCOUNT_LEARNER_RECCORD); 
            accountdatalist[0].Pearson_Campus__c = false;
            accountdatalist[0].Territory_Code_s__c = '216';
            accountdatalist[1].Pearson_Campus__c = false;
            accountdatalist[1].Territory_Code_s__c = '217';
        system.runas(listWithUser[0]){
            Insert accountdatalist;  
                
            System.assert(accountdatalist[0].id != null, 'Account creation failed');
            
        }
        
        List<User> listWithUserSysAdm  = TestDataFactory.createUser([select Id from Profile where Name =: PS_ProfileNames.SYSTEM_ADMINISTRATOR].Id,1);
        listWithUserSysAdm[0].Market__c = 'UK';
        listWithUserSysAdm[0].IsActive = true;
        system.runas(listWithUserSysAdm[0]){
            
            Territory2Model trrmodel = [select id,State from Territory2Model where state= 'Active'];
            Territory2Type trrType = TestDataFactory.createTerritory2Type();
            
            Territory2 terrt1 = TestDataFactory.createTerritory('TestParent',trrmodel.Id,trrType.Id);
            terrt1.Territory_Code__c = '217';
            insert terrt1;
            
            Territory2 terrt = TestDataFactory.createTerritory('TestChild',trrmodel.Id,trrType.Id);
            terrt.Territory_Code__c = '217';
            terrt.ParentTerritory2Id = terrt1.Id;
            insert terrt;
            
            Territory2 terrt2 = TestDataFactory.createTerritory('TestChild1',trrmodel.Id,trrType.Id);
            terrt2.Territory_Code__c = '216';
            terrt2.ParentTerritory2Id = terrt1.Id;
            insert terrt2;
            
            Territory2 queterritory2 = [select id,Territory2Model.State,Territory_Code__c from Territory2 where id=: terrt.id limit 1 ];
            
            UserTerritory2Association userassgnment = TestDataFactory.createUserTerritory2Association(listWithUserSysAdm[0].Id,terrt.id);
            userassgnment.RoleInTerritory2 = 'Learning Technology Specialist';
            insert userassgnment;
            
            UserTerritory2Association userassgnment1 = TestDataFactory.createUserTerritory2Association(listWithUserSysAdm[0].Id,terrt2.id);
            userassgnment1.RoleInTerritory2 = 'Learning Technology Specialist';
            insert userassgnment1;
            accountdatalist[0].Territory_Code_s__c = '217';
            checkRecurssionBefore.run = true; 
            update accountdatalist;
            
        }
        
    }
}