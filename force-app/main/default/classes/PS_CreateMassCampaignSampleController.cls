global without sharing class PS_CreateMassCampaignSampleController{
    
    Id CampaignId{get;set;}
    private List<Campaign> lstCampaign{get;set;}
    public String CampaignName{get;set;}
    private List<Contact> CampaignContactList{get;set;}
    public Boolean errorOccured{get;set;} 
    Set<Id> CampaignContactIds;
    List<Campaignmember> campmemb;
    private List<Campaign_Product__c> campprod{get;set;}
    List<OrderItem> lstOrderLineItemsToInsert;
    List<Order> OrdersToInsert;
    
    public PS_CreateMassCampaignSampleController()
    {
        lstCampaign = new List<Campaign>();
        CampaignName = null;
        CampaignContactList = new List<Contact>();
        CampaignContactIds = new Set<Id>();
        campmemb = new List<Campaignmember>();
        campprod = new List<Campaign_Product__c>();
        errorOccured = false;
        
        CampaignId = ApexPages.currentPage().getParameters().get('Id');
        
        //Oct 11 : Modified for the CR-02115
        lstCampaign = [select id,status,Name,Shipping_Method__c,Pricebook__c,(select id,Campaignid,Order_Status__c,Status__c,Contactid,Type 
                    from Campaignmembers 
                    where campaignid =: CampaignId and Type = 'Contact'),(select id,Campaign__c,Product__c 
                    from Campaign_Products__r where Campaign__c=:CampaignId) from campaign where id =: CampaignId];
       /* CampaignName = lstCampaign[0].Name;
        
        campmemb = [select id,Campaignid,Order_Status__c,Status__c,Contactid,Type 
                    from Campaignmember 
                    where campaignid =: CampaignId and Type = 'Contact'];
      */
        for(Campaign cam : lstCampaign){
            CampaignName = cam.Name;
            campmemb = cam.Campaignmembers;
            campprod = cam.campaign_products__r;
        }
       
        for(campaignmember cm : campmemb){
            CampaignContactIds.add(cm.contactid);
        }
        CampaignContactList = [select id FROM Contact 
                               where id IN : CampaignContactIds AND Do_Not_Send_Samples__c=FALSE AND 
                               Account.Shipping_Address_Lookup__c !=null AND Account.Billing_Address_Lookup__c !=null AND
                               Global_Marketing_Unsubscribe__c =FALSE AND Account.ShippingStreet!=null AND 
                               Account.ShippingCity!=null AND Account.ShippingState!=null AND Account.ShippingPostalCode!=null 
                               AND Account.ShippingCountry!=null limit 5000];
       /* CampaignContactList = [select id,Name,Do_Not_Send_Samples__c,Global_Marketing_Unsubscribe__c,AccountId,
                               Account.ShippingStreet,Account.ShippingCity,Account.ShippingState,Account.ShippingPostalCode,
                               Account.ShippingCountry,Account.Shipping_Address_Lookup__c,Account.Billing_Address_Lookup__c FROM Contact 
                               where id IN : CampaignContactIds AND Do_Not_Send_Samples__c=FALSE AND 
                               Account.Shipping_Address_Lookup__c !=null AND Account.Billing_Address_Lookup__c !=null AND
                               Global_Marketing_Unsubscribe__c =FALSE AND Account.ShippingStreet!=null AND 
                               Account.ShippingCity!=null AND Account.ShippingState!=null AND Account.ShippingPostalCode!=null 
                               AND Account.ShippingCountry!=null];*/
        //campprod=[select id,Campaign__c,Product__c from Campaign_Product__c where Campaign__c=:CampaignId];
        
        if(lstCampaign[0].status =='Sent'){
            errorOccured = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Orders have already been created for this Campaign'));
        }
        
        if(campmemb.size()==0 || campprod.size()==0){
            errorOccured = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.PS_MassCampaignSampling));
        }
        if(campmemb.size()>0){
        if(CampaignContactList.size()==0){
            errorOccured = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'There is no contact with corresponding account address is not null'));  
          }
        }
        
        //Oct 11 : Added for the CR-02115
        User Usr =[select id,Create_Mass_Sample_Campaigns__c from user where id=:userinfo.getUserId()];
        if((campmemb.size()>50) && !(Usr.Create_Mass_Sample_Campaigns__c)){
            errorOccured = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'You cannot create more than 50 orders'));
        }
    }
        
    
    public pageReference CreateOrders(){
               
    if(!errorOccured){
        
        MassCampaignSampleOrderBatch mc= new MassCampaignSampleOrderBatch(lstCampaign,CampaignContactIds);
        Database.executeBatch(mc, 10);
        
    }
        PageReference backtoCampaignfrommethod = new PageReference('/'+CampaignId);
        backtoCampaignfrommethod.setRedirect(true);
        return backtoCampaignfrommethod;
        
 }
    
    
    public pageReference Cancel(){
        //PageReference backtoCampaign = new PageReference('/'+CampaignId+'?status='+'true');
        PageReference backtoCampaign = new PageReference('/'+CampaignId);
        backtoCampaign.setRedirect(true);
        return backtoCampaign;
    }
}