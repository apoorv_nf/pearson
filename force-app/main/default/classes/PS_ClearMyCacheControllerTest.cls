/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_ClearMyCacheControllerTest
   Description:     Test class for PS_ClearMyCacheController
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   04 Dec 2015      1.0           Accenture NDC                                         Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

@isTest
public class PS_ClearMyCacheControllerTest
{
    static testMethod void testProductFilterWithFamily()
    {
        PS_ClearMyCacheController controllerObj = new PS_ClearMyCacheController();
        Cache.Session.put(UserInfo.getUserId()+'selectedFamilyGuidedSelling','12345');
        controllerObj.clearMyCache();
    }
}