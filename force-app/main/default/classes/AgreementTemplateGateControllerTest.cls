@isTest class AgreementTemplateGateControllerTest {
    static final String DEAL_CLASSIFICATION = Opportunity.Channel_Detail__c.getDescribe().getPicklistValues()[0].getValue();

    @isTest static void redirectIfApproved_redirected() {
        final Set<String> APPROVED_CLASSIFICATIONS = new Set<String> {DEAL_CLASSIFICATION};
        
        Bypass_Settings__c bypass=new Bypass_Settings__c(Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true,Disable_Triggers__c=true);
        insert bypass;
        //Create Test data
        Account acc1 = new Account(Name='Test Account1',Abbreviated_Name__c ='Test Acc1',IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'US', 
                                  Line_of_Business__c= 'Schools', Geography__c= 'Core', 
                                  ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', 
                                  ShippingPostalCode = '5600371',Pearson_Customer_Number__c='1234');
        insert acc1;
        Contact con1 = new Contact(FirstName='TestContactFirstname3', LastName='TestContactLastname',AccountId=acc1.id ,Salutation='MRS.', Email='xyz3@email.com',MobilePhone='11122233344' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
        insert con1;
        
        Default_B2B_Service_Assignee__c rd= new Default_B2B_Service_Assignee__c(); 
        rd.Assignee_User_ID__c='005b0000002hB9C';
        insert rd;            
    
        List<Opportunity> testOpportunity = TestDataFactory.createOpportunity(1, 'Enterprise');
        testOpportunity[0].Name = 'TestMasterOppProbName3';
        testOpportunity[0].accountid = acc1.id;
        testOpportunity[0].StageName = 'Needs Analysis';
        testOpportunity[0].Approval_status__c = 'Approved';
        testOpportunity[0].Channel_Detail__c = 'All Inclusive';
        testOpportunity[0].isConvertedFromLead__c = true;
       
        testOpportunity[0].CloseDate = System.TODAY() + 10;
        testOpportunity[0].Include_in_Probablity_Calculations__c = true;
        testOpportunity[0].Probability = 10; 
        testOpportunity[0].Assigned_Regional_Director__c=rd.Assignee_User_ID__c;
        insert testOpportunity;

        final Map<String, String> PARAMETERS = new Map<String, String> {
            AgreementTemplateGateController.MASTER_ID => testOpportunity[0].Id,
            'otherParam' => 'someValue'
        };
        final PageReference EXPECTED_PAGE = Page.echosign_dev1__AgreementTemplateProcess;
        EXPECTED_PAGE.getParameters().putAll(PARAMETERS);

        ApexPages.currentPage().getParameters().putAll(PARAMETERS);
        //AgreementTemplateGateController.approvedClassifications = APPROVED_CLASSIFICATIONS;
        AgreementTemplateGateController controller = new AgreementTemplateGateController();
        Test.startTest();
        PageReference returnedPage = controller.redirectIfApproved();
        Test.stopTest();

        System.assertEquals(
            EXPECTED_PAGE.getUrl(),
            returnedPage.getUrl(),
            'If the Opportunity has final approval, the page should redirect to the Agreement Template Processor and pass through all parameters'
        );
    }

    @isTest static void redirectIfApproved_noFinalApproval() {
        final Set<String> APPROVED_CLASSIFICATIONS = new Set<String> {DEAL_CLASSIFICATION};
        Bypass_Settings__c bypass=new Bypass_Settings__c(Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true,Disable_Triggers__c=true);
        Insert bypass;
        //Create Test data
        Account acc1 = new Account(Name='Test Account1',Abbreviated_Name__c ='Test Acc1',IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'US', 
                                  Line_of_Business__c= 'Schools', Geography__c= 'Core', 
                                  ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', 
                                  ShippingPostalCode = '5600371',Pearson_Customer_Number__c='1234');
        insert acc1;
        Contact con1 = new Contact(FirstName='TestContactFirstname3', LastName='TestContactLastname',AccountId=acc1.id ,Salutation='MRS.', Email='xyz3@email.com',MobilePhone='11122233344' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
        insert con1;
        
        Default_B2B_Service_Assignee__c rd= new Default_B2B_Service_Assignee__c(); 
        rd.Assignee_User_ID__c='005b0000002hB9C';
        insert rd;            
    
        List<Opportunity> testOpportunity = TestDataFactory.createOpportunity(1, 'Enterprise');
        testOpportunity[0].Name = 'TestMasterOppProbName3';
        testOpportunity[0].accountid = acc1.id;
        testOpportunity[0].StageName = 'Needs Analysis';
        testOpportunity[0].CloseDate = System.TODAY() + 10;
        testOpportunity[0].Include_in_Probablity_Calculations__c = true;
        testOpportunity[0].isConvertedFromLead__c = true;
        testOpportunity[0].Probability = 10; 
        testOpportunity[0].Assigned_Regional_Director__c=rd.Assignee_User_ID__c;
        insert testOpportunity;
        
        final Map<String, String> PARAMETERS = new Map<String, String> {
            AgreementTemplateGateController.MASTER_ID => testOpportunity[0].Id,
            'otherParam' => 'someValue'
        };

        ApexPages.currentPage().getParameters().putAll(PARAMETERS);
        //AgreementTemplateGateController.approvedClassifications = APPROVED_CLASSIFICATIONS;
        AgreementTemplateGateController controller = new AgreementTemplateGateController();
        Test.startTest();
        PageReference returnedPage = controller.redirectIfApproved();
        Test.stopTest();

        System.assertEquals(
            null,
            returnedPage,
            'If the Opportunity does not have final approval, the page should not redirect'
        );
        System.assertEquals(
            1,
            ApexPages.getMessages().size(),
            'If the Opportunity does not have final approval, a message should be added to the page'
        );
        System.assertEquals(
            ApexPages.Severity.INFO,
            ApexPages.getMessages()[0].getSeverity(),
            'If the Opportunity does not have final approval, a message should be added to the page with severity set to INFO'
        );
    }

    @isTest static void redirectIfApproved_notApprovedClassification() {
        final Set<String> APPROVED_CLASSIFICATIONS = new Set<String>();
        Bypass_Settings__c bypass=new Bypass_Settings__c(Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true,Disable_Triggers__c=true);
        Insert bypass;
        //Create Test data
        Account acc1 = new Account(Name='Test Account1',Abbreviated_Name__c ='Test Acc1',IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'US', 
                                  Line_of_Business__c= 'Schools', Geography__c= 'Core', 
                                  ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', 
                                  ShippingPostalCode = '5600371',Pearson_Customer_Number__c='1234');
        insert acc1;
        Contact con1 = new Contact(FirstName='TestContactFirstname3', LastName='TestContactLastname',AccountId=acc1.id ,Salutation='MRS.', Email='xyz3@email.com',MobilePhone='11122233344' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
        insert con1;
        
        Default_B2B_Service_Assignee__c rd= new Default_B2B_Service_Assignee__c(); 
        rd.Assignee_User_ID__c='005b0000002hB9C';
        insert rd;            
    
        List<Opportunity> testOpportunity = TestDataFactory.createOpportunity(1, 'Enterprise');
        testOpportunity[0].Name = 'TestMasterOppProbName3';
        testOpportunity[0].accountid = acc1.id;
        testOpportunity[0].StageName = 'Needs Analysis';
        testOpportunity[0].CloseDate = System.TODAY() + 10;
        testOpportunity[0].isConvertedFromLead__c =True;
        testOpportunity[0].Include_in_Probablity_Calculations__c = true;
        testOpportunity[0].Probability = 10; 
        testOpportunity[0].Assigned_Regional_Director__c=rd.Assignee_User_ID__c;
        insert testOpportunity;
                
        final Map<String, String> PARAMETERS = new Map<String, String> {
            AgreementTemplateGateController.MASTER_ID => testOpportunity[0].Id,
            'otherParam' => 'someValue'
        };

        ApexPages.currentPage().getParameters().putAll(PARAMETERS);
        //AgreementTemplateGateController.approvedClassifications = APPROVED_CLASSIFICATIONS;
        AgreementTemplateGateController controller = new AgreementTemplateGateController();
        Test.startTest();
        PageReference returnedPage = controller.redirectIfApproved();
        Test.stopTest();

        System.assertEquals(
            null,
            returnedPage,
            'If the Opportunity does not have an approved Deal Classification, the page should not redirect'
        );
        System.assertEquals(
            1,
            ApexPages.getMessages().size(),
            'If the Opportunity does not have an approved Deal Classification, a message should be added to the page'
        );
        System.assertEquals(
            ApexPages.Severity.INFO,
            ApexPages.getMessages()[0].getSeverity(),
            'If the Opportunity does not have an approved Deal Classification, a message should be added to the page with severity set to INFO'
        );
    }

    @isTest static void redirectIfApproved_masterIdNotSet() {
     Bypass_Settings__c bypass=new Bypass_Settings__c(Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true,Disable_Triggers__c=true);
     Insert bypass;
        //Create Test data
        Account acc1 = new Account(Name='Test Account1',Abbreviated_Name__c ='Test Acc1',IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'US', 
                                  Line_of_Business__c= 'Schools', Geography__c= 'Core', 
                                  ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', 
                                  ShippingPostalCode = '5600371',Pearson_Customer_Number__c='1234');
        insert acc1;
        Contact con1 = new Contact(FirstName='TestContactFirstname3', LastName='TestContactLastname',AccountId=acc1.id ,Salutation='MRS.', Email='xyz3@email.com',MobilePhone='11122233344' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
        insert con1;
        
        Default_B2B_Service_Assignee__c rd= new Default_B2B_Service_Assignee__c(); 
        rd.Assignee_User_ID__c='005b0000002hB9C';
        insert rd;            
    
        List<Opportunity> testOpportunity = TestDataFactory.createOpportunity(1, 'Enterprise');
        testOpportunity[0].Name = 'TestMasterOppProbName3';
        testOpportunity[0].accountid = acc1.id;
       // testOpportunity[0].StageName = 'Prospecting';
      testOpportunity[0].StageName = 'Needs Analysis';
        testOpportunity[0].CloseDate = System.TODAY() + 10;
        testOpportunity[0].Include_in_Probablity_Calculations__c = true;
        testOpportunity[0].isConvertedFromLead__c =True;
        testOpportunity[0].Probability = 10; 
        testOpportunity[0].Assigned_Regional_Director__c=rd.Assignee_User_ID__c;
        insert testOpportunity;

        final Map<String, String> PARAMETERS = new Map<String, String> {
            'otherParam' => 'someValue'
        };
        final PageReference EXPECTED_PAGE = Page.echosign_dev1__AgreementTemplateProcess;
        EXPECTED_PAGE.getParameters().putAll(PARAMETERS);

        ApexPages.currentPage().getParameters().putAll(PARAMETERS);

        AgreementTemplateGateController controller = new AgreementTemplateGateController();
        Test.startTest();
        PageReference returnedPage = controller.redirectIfApproved();
        Test.stopTest();

        System.assertEquals(
            null,
            returnedPage,
            'If the Opportunity does not have the \'masterId\' parameter set, the page should not redirect'
        );
        System.assertEquals(
            1,
            ApexPages.getMessages().size(),
            'If the Opportunity does not have the \'masterId\' parameter set, a message should be added to the page'
        );
        System.assertEquals(
            ApexPages.Severity.ERROR,
            ApexPages.getMessages()[0].getSeverity(),
            'If the Opportunity does not have the \'masterId\' parameter set, a message should be added to the page with severity set to ERROR'
        );
    }
}