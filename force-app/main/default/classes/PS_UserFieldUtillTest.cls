/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 7/27/2018
 */
@isTest
private class PS_UserFieldUtillTest {

    @isTest
    static void it_should_set_field_values() {
        Account account = new Account();
        account.OwnerId = UserInfo.getUserId();
        PS_UserFieldUtill.setUserFieldValues(new List<Account>{account}, 'OwnerId');
    }

    @isTest
    static void it_should_set_fetch_user() {
        Set<Id> userSet = new Set<Id>();
        userSet.add(UserInfo.getUserId());
        PS_UserFieldUtill.fetchUser(userSet);
    }

    @isTest
    static void it_should_set_user_values() {
        List<User> users = [SELECT Id, Name, Market__c, Line_of_Business__c, Business_Unit__c, Geography__c, Group__c FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        PS_UserFieldUtill.setUserFieldValues(users);
    }

    @isTest
    static void it_should_have_object_field(){
        User userObject = [SELECT Id, Name FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        PS_UserFieldUtill.hasSObjectField('Name', userObject);
    }
}