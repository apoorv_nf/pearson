/************************************************************************************************************
* Apex Interface Name : PS_Contact_TriggerHandlerTest
* Version             : 1.1 
* Created Date        : 1/2/2016 2:44 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Rashmi Prasad         30/1/2016     
* Rashmi Prasad         3/4/2016                Added Negative scenario     
* Rashmi                3/18/2016               Updateded lstContact[0].sSelfServiceCreated__c = 'true' from false  
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_Contact_TriggerHandlerTest{
    static testMethod void validatePS_Contact_TriggerHandler(){
        List<User> listWithUser = new List<User>();
        String sLicenseName='';
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name = 'System Administrator'].Id,1);    
        
        insert listWithUser; 
        
        Bypass_Settings__c bys = new Bypass_Settings__c();
        bys.SetupOwnerId = listWithUser[0].id;
        bys.Disable_Validation_Rules__c = true;
        bys.Disable_Process_Builder__c = true;
        bys.Disable_Triggers__c = true;
        insert bys;
        
        Account testacc = New Account();
        testacc.Name = 'Testacc';
        insert testacc;
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].Role__c = 'Educator';
        lstContact[0].Role_detail__c = 'Instructor';
        lstContact[0].MailingState = 'England';
        lstContact[0].sSelfServiceCreated__c = 'true';
        lstContact[0].ACR_Check_For_GetSupport__c=FALSE;
        lstContact[0].Market__c='US';
        lstContact[0].AccountId = null;
        
        Test.StartTest();
        System.runAs(listWithUser[0]){
            insert new PS_ContactRole__c(RoleName__c = 'Educator',Name = 'Educator',RoleDetail__c = 'Instructor');        
            insert lstContact;
            System.assertEquals(lstContact[0].MailingState,'England'); 
            PS_Contact_TriggerHandler PSTest1=New PS_Contact_TriggerHandler();
            PSTest1.onBeforeInsert(lstContact);        
           
        }
        Test.StopTest();
    }
    
    //Negative Scenario
    static testMethod void negativetestPS_Contact_TriggerHandler(){
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name = 'System Administrator'].Id,1);       
        insert listWithUser;
         
        Bypass_Settings__c bys = new Bypass_Settings__c();
        bys.SetupOwnerId = listWithUser[0].id;
        bys.Disable_Validation_Rules__c = true;
        bys.Disable_Process_Builder__c = true;
        bys.Disable_Triggers__c = true;
        insert bys;
        
        Account testacc = New Account();
        testacc.Name = 'Testacc';
        insert testacc;
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].Role__c = 'Learner';        
        lstContact[0].MailingState = 'England';
        lstContact[0].sSelfServiceCreated__c = 'true';
        lstContact[0].ACR_Check_For_GetSupport__c=FALSE;
        lstContact[0].Market__c='US';
        lstContact[0].AccountId =null;
        Test.StartTest();
        System.runAs(listWithUser[0]){
            insert new PS_ContactRole__c(RoleName__c = 'Learner',Name = 'Learner',RoleDetail__c = '');        
            insert lstContact;
            System.assertEquals(lstContact[0].MailingState,'England');     
            PS_Contact_TriggerHandler PSTest2=New PS_Contact_TriggerHandler();
            PSTest2.onBeforeInsert(lstContact);       
        }
        Test.StopTest();
    }
    
}