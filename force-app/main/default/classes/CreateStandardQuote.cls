// CreateStandardQuote Class .this takes Opportunity Id as input.
/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            CreateStandardQuote.cls
Description:     For Creation of Quote - Overriding Standard New Button - After IR UK We have moved to Custom Button from Standard New Quote Button Overriding
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------       -----------------------------------------------------------------------------------------------
22-Sep-2018        1.0          Mani Harika              Created for ZA Market - Project Moving out of Apttus - To Use Quote.
20-Dec-2018        1.1          Srikanth                 Modified for UK Market - As part of IR UK Project.
20-Jan-2019        1.1          Navaneeth                Modified for UK Market - As part of IR UK Project.
26-Aug-2019       1.1          Asha                     Modified for ANZ Apttus to Standard Quote
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class CreateStandardQuote{
    public Quote newQuote{set;get;}
    public Id opportunityId;
    public Id quoRecTypeId;
    public Quote existingQuote;
    public list<User> currentUserId =[SELECT Id,market__c FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1];
    
    // Added by Shiva - Start
    // public List<String> quotemarkets = Label.Quote_market.split(',');
    // Added by Shiva - End
    
    // Commented the above to replace Custom label with Custom setting for CR-02916_Asha        
    Map<String,PS_Market__c> quotemarkets = PS_Market__c.getAll();      
    PS_Market__c optyMarketAU = PS_Market__c.getValues('AU');       
    PS_Market__c optyMarketNZ = PS_Market__c.getValues('NZ');       
    PS_Market__c optyMarketUK = PS_Market__c.getValues('UK');       
    //Added above for CR-02884_Asha
    
    // Added by Swapna / Navaneeth - Start
    public CreateStandardQuote(ApexPages.StandardSetController controller) {
        opportunityId =  ApexPages.currentPage().getParameters().get('id');    
        quoRecTypeId =  ApexPages.currentPage().getParameters().get('RecordType');
        existingQuote = (Quote)controller.getRecord();
    }
    // Added by Swapna / Navaneeth - End
    
    public CreateStandardQuote(ApexPages.StandardController stdController){
        opportunityId =  ApexPages.currentPage().getParameters().get('oppid');
        quoRecTypeId =  ApexPages.currentPage().getParameters().get('RecordType');
        existingQuote = (Quote)stdController.getRecord();
        
        
    }
    //Method  QuoteAlert
    public void QuoteAlert(){
        
        Quote quo=[SELECT ID,Name,OpportunityId,Opportunity.market__c,Opportunity.Earlybird_Payment_Reference__c,Opportunity.CreatedDate From Quote Where Id=:existingQuote.Id Limit 1];
        //Opportunity Oppty = [SELECT Id,AccountId, Conferrer__c, Name,Pricebook2Id, Primary_Contact__c,CreatedDate,
        //Qualification_Picklist__c,Academic_Start_Date__c,Type,Earlybird_Payment_Reference__c,Level__c from Opportunity where Id=:opportunityId];
        Map<String,Quote_Settings__c> quoteSettings = new Map<String,Quote_Settings__c>();
        quoteSettings = Quote_Settings__c.getAll();
        
        if(quo.Opportunity.Earlybird_Payment_Reference__c && quo.Opportunity.CreatedDate > quoteSettings.values()[0].Early_Bird__c && (!quotemarkets.containskey(quo.Opportunity.market__c))){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Early bird deadline has passed. Ensure you select correct Pricebook in the New Opportunity.');
            ApexPages.addMessage(myMsg);
            //return null;
        }
    }
    //Method  Quote_create
    public PageReference Quote_create(){
        //Opportunity Oppty = [SELECT Id,AccountId, Conferrer__c, Name,Pricebook2Id, Primary_Contact__c,CreatedDate,
        //Qualification_Picklist__c,Academic_Start_Date__c,Type,Earlybird_Payment_Reference__c,Level__c from Opportunity where Id=:opportunityId];
        Opportunity Oppty = [SELECT Id,AccountId,market__c,Name,Pricebook2Id, Primary_Contact__c,CreatedDate,
                             Academic_Start_Date__c,Type,Earlybird_Payment_Reference__c,SyncedQuote_Qualification_Level__c,Account.Primary_Selling_Account__c,
                             Primary_Contact__r.Email,Business_unit__c,Line_of_Business__c,Geography__c,Account.Shipping_Address_Lookup__c,Account.Primary_Selling_Account__r.Shipping_Address_Lookup__c,OwnerId,Amount,Pricebook2.Id,CurrencyIsoCode,StageName from Opportunity where Id=:opportunityId];
        //Asha Added Stagename field in above query for CR-02884 
        // Commented by Rob 19 August 2019 for Patricia to test OBL failure modes when using same process as UK
        // Added for OBL - By Navaneeth /  Swapna
        //if(Oppty.Business_unit__c=='Online and Blended Learning')
        // {
        //     PageReference pg = new PageReference('/0Q0/e?retURL=%2F'+opportunityId+'&oppid='+opportunityId);
        //     return pg;
        // }   
        // End of Addition for OBL By Navaneeth / Swapna
        // End of Rob's comment 
        
        // Commented by Navaneeth - Start
        // Shipping Address Population - Upon Quote - No Need to Queries - We should take from the Account  Shipping_Address_Lookup__c \ Accounts -> Primary Selling Account Shipping_Address_Lookup__c
        /*
List<Account_Addresses__c> accAddrs = new List<Account_Addresses__c>();
if(Oppty.AccountId != NULL){
accAddrs = [Select Id,Account__c,Primary_Address__c,Primary__c,PrimaryShipping__c,Shipping__c 
from Account_Addresses__c where Account__c =: Oppty.AccountID and PrimaryShipping__c = True];
}*/
        // Commented by Navaneeth - End
        Map<String,Quote_Settings__c> quoteSettings = new Map<String,Quote_Settings__c>();
        quoteSettings = Quote_Settings__c.getAll();
        List<OpportunityContactRole> oppContactRole = new List<OpportunityContactRole>();
        oppContactRole = [SELECT ContactId,Id,OpportunityId,Role FROM OpportunityContactRole 
                          where Role IN ('Primary sales contact','Business User') AND OpportunityId =:Oppty.Id LIMIT 50000];
        Quote quote_new = new Quote();
        
        // Added by Navaneeth for IRUK - Opportunity Primary Contact Population Logic - Start
        List<OpportunityContactRole> oppPrimContactRole = new List<OpportunityContactRole>();
        oppPrimContactRole = [SELECT ContactId,Contact.Email,Id,OpportunityId,Role FROM OpportunityContactRole where OpportunityId =:Oppty.Id AND IsPrimary = TRUE LIMIT 1];
        // Added by Navaneeth for IRUK - Opportunity Primary Contact Population Logic - End 
        //Id recordtypeid =  Schema.SObjectType.Quote.getRecordTypeInfosByName().get('HE Quote').getRecordTypeId();
        //Commented and Modified by Shiva - Start 
        if(quoRecTypeId!= null){
            quote_new.RecordTypeId = quoRecTypeId; // Quote record type 
        }else if(quotemarkets.containskey(Oppty.Market__c)){ // Added this Condition for UK Market on date 11/21/2018 
            quote_new.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Global Quote').getRecordTypeId();
        }else{
            quote_new.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('HE Quote').getRecordTypeId();
        }
        //Commented and Modified by Shiva - End  
        
        quote_new.Account__c = Oppty.AccountId;
        quote_new.Pricebook2Id=Oppty.Pricebook2Id;
        //quote_new.Conferrer__c = Oppty.Conferrer__c;
        //quote_new.Qualification_Level_Name__c=Oppty.Level__c;
        quote_new.ContactId = Oppty.Primary_Contact__c;
        quote_new.Name = Oppty.Name;
        quote_new.OpportunityId = Oppty.Id;
        quote_new.Sales_Consultant__c = currentUserId[0].Id;
        if(!quotemarkets.containskey(Oppty.Market__c)){  // Added this Condition for UK & US Market on date 25/01/2019 
            if(!oppContactRole.isEmpty()){
                for(OpportunityContactRole oppCon : oppContactRole){
                    if(oppCon.Role=='Business User'){
                        quote_new.Primary_Contact__c = oppCon.ContactId;
                    }
                    if(oppCon.Role=='Primary sales contact'){
                        quote_new.Sponsor__c=oppCon.ContactId;
                    }
                }
            }
            
            //quote_new.Qualification_Picklist__c = Oppty.Qualification_Picklist__c;
            
            quote_new.Degree_Type__c='Full-Time';
            quote_new.Status='Draft';
            quote_new.Payment_Method__c='Direct Deposit';
            quote_new.Payment_Type__c='Monthly Payment';        
            quote_new.Payment_Period_In_Month__c='10';
            quote_new.First_Payment_Date__c=Oppty.Academic_Start_Date__c;
            quote_new.Market__c = oppty.Market__c;
            //quote_new.Qualification_Level_Name__c=Oppty.Level__c;
            
            
            if(Oppty.Type=='New Business' && Oppty.Earlybird_Payment_Reference__c){
                quote_new.Registration_Fee__c=quoteSettings.values()[0].Default_Registration_Fee_New_Business__c;
                quote_new.Total_Early_Bird_Securing_Fee_Payments__c=quoteSettings.values()[0].Earlybird_Securing_Fee_New_Business__c;
            }
            else if(Oppty.Type=='Returning Business'&& Oppty.Earlybird_Payment_Reference__c){
                quote_new.Registration_Fee__c=quoteSettings.values()[0].Default_Registration_Fee_ReturnBusiness__c;
                quote_new.Total_Early_Bird_Securing_Fee_Payments__c=quoteSettings.values()[0].Earlybird_Securing_Fee_Return_Business__c;
            }
            else{
                quote_new.Registration_Fee__c=0;
                quote_new.Total_Early_Bird_Securing_Fee_Payments__c=0;
            }
        }
        else if(quotemarkets.containskey(Oppty.Market__c)){
            /** CR-2332 wireframe- Start **/
            if(Oppty.Market__c==optyMarketUK.Market__c) //Asha Added the condition to populate only for UK Market CR-02965
                quote_new.ERP_Operating_Unit__c = 'GB Pearson Education OU';
            //Asha Added the below for ANZ Market CR-02965      
            if(Oppty.Market__c==optyMarketAU.Market__c || Oppty.Market__c==optyMarketNZ.Market__c)      
                quote_new.ERP_Operating_Unit__c = 'AU Pearson Education OU';  
            quote_new.Primary_Selling_Account__c = Oppty.Account.Primary_Selling_Account__c;
            if(oppPrimContactRole.size()>0)
            {
                quote_new.Primary_Contact__c = oppPrimContactRole[0].ContactId;
                quote_new.ContactId = oppPrimContactRole[0].ContactId;
                quote_new.Email = oppPrimContactRole[0].Contact.Email;
                quote_new.Ship_To_Contact__c = oppPrimContactRole[0].ContactId; 
                quote_new.Subscription_Contact__c = oppPrimContactRole[0].ContactId;
                quote_new.Subscription_Contact_Email__c = oppPrimContactRole[0].Contact.Email;
            }   
            quote_new.Market__c = oppty.Market__c;
            quote_new.Name= oppty.Name;
            quote_new.Business_unit__c = oppty.Business_unit__c;
            quote_new.Line_of_Business__c = oppty.Line_of_Business__c;
            quote_new.Geography__c = oppty.Geography__c;
            // Commented and Modified by Navaneeth - Start
            /*
if(accAddrs.size()>0 && accAddrs[0].PrimaryShipping__c == True){
quote_new.Ship_to_Address__c = accAddrs[0].Id;
}*/
            if(oppty.Account.Shipping_Address_Lookup__c != null)
                quote_new.Ship_to_Address__c = oppty.Account.Shipping_Address_Lookup__c;
            else if ( oppty.Account.Primary_Selling_Account__r.Shipping_Address_Lookup__c != null )
                quote_new.Ship_to_Address__c =  oppty.Account.Primary_Selling_Account__r.Shipping_Address_Lookup__c;
            // Commented and Modified by Navaneeth - End
            quote_new.OwnerId = oppty.OwnerId;
            quote_new.ExpirationDate = System.today() + 90;
            //Asha Added the below to set the Expiration Date for ANZ for CR-02965      
            if(Oppty.Market__c==optyMarketAU.Market__c || Oppty.Market__c==optyMarketNZ.Market__c)      
                quote_new.ExpirationDate = System.today() + 30;    //Asha CR-02965 Changes end
            quote_new.Quote_Currency__c = oppty.CurrencyIsoCode;
            
            
        }         
        /** CR-2332 wireframe- END **/
        if((quotemarkets.containskey(Oppty.Market__c)) && (oppPrimContactRole.size()==0 || oppty.PriceBook2 == NULL)){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Pricebook and add a Primary Contact before creating the quote.');
            ApexPages.addMessage(myMsg);
        }
        //CR-2533 Commented by Laxman for CI-Start 
        else  if(Oppty.Academic_Start_Date__c == null && Oppty.Market__c =='ZA')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please Enter the Academic start date for ZA users '));
            return null;
        } 
        //CR-2533 Commented by Laxman for CI-End 
        //Asha changes for CR-02884 Begins      
        else if(Oppty.StageName != 'Proposal/Quote' && (Oppty.Market__c ==optyMarketAU.Market__c || Oppty.Market__c==optyMarketNZ.Market__c || Oppty.Market__c ==optyMarketUK.Market__c)){      
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please make sure that the Opportunity Stage is in Proposal/Quote before creating a quote.');      
            ApexPages.addMessage(myMsg);        
        } //CR-02884 Ends
        else{
            if(quote_new != null){
                try{
                    insert quote_new;
                    // Added By Navaneeth - Start
                    if(quotemarkets.containskey(Oppty.Market__c))
                        update Oppty; 
                    // Added By Navaneeth - End
                }catch(Exception ex){
                    throw ex;
                }
            }
            if(quote_new.id != null){
                PageReference pg = new PageReference('/'+quote_new.id);
                return pg;
            }else{
                PageReference pg = new PageReference('/'+opportunityId);
                return pg;
            }
        }
        return null;
    }
    //}
    
}