/*****************************************************************************************************************************/
/*Description:This class is getting called from Process builder for deleting an existing record in Existing Users List object

Date             Version              Author                          Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
20-Nov-2019         0.1             TAMIZHARASAN        CR-03158 (Mapping of extra fields from Mirror user to New User)      
/*****************************************************************************************************************************/
public class POC_JunctionObjectDeletion {
    @InvocableMethod
    public static void JunctionObjectDelete(List<id> userid)
    {
        List<User_and_License_Tracking__c> JunctionObject =[select id from User_and_License_Tracking__c where User__c in :userid];
        delete JunctionObject;
   }

}