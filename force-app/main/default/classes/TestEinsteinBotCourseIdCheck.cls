/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 11/07/2018
 */
@isTest
private class TestEinsteinBotCourseIdCheck
{
	@testSetup
	static void setup() {
		SMS_API_Settings__c settings = new SMS_API_Settings__c();
		settings.API_Key__c = 'abcdef';
		settings.Username__c = 'username123';
		settings.Password__c = 'password123';
		settings.SSO_Token__c = 'token1234';

		insert settings;
	}

	@isTest
	static void it_should_return_back_a_valid_course_id_response()
	{
		Test.setMock(HttpCalloutMock.class, new SmsApiMock());

		Test.startTest();
		List<EinsteinBotCourseIdCheck.CourseIdResponse> responses = EinsteinBotCourseIdCheck.checkCourseId(new List<String>{'courseId'});
		Test.stopTest();

		System.assert(!responses.isEmpty(), 'Responses should not be empty');

		System.assert(responses[0].success);
		System.assert(responses[0].valid);
	}

	@isTest
	static void it_should_verify_access_code_when_expired() {
		Test.setMock(HttpCalloutMock.class, new SmsApiMock());

		Test.startTest();
		List<EinsteinBotCourseIdCheck.CourseIdResponse> responses = EinsteinBotCourseIdCheck.checkCourseId(new List<String>{'token_expired'}); 
		Test.stopTest();

		System.assert(!responses.isEmpty(), 'Responses should not be empty');
		System.assertEquals(true, responses[0].sessionExpired);
	}
}