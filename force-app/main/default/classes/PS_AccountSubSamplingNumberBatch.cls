/*===========================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER        DESCRIPTION                              
 |  ====            =========        =========== 
 |  27/10/2015        IDC:RG         This Class is used AccountSubSamplingNumberIncrement
    29/07/2016        IDC:RJ         Logic Updated to query for more than one market.
 +===========================================================================*/
global class PS_AccountSubSamplingNumberBatch implements Database.Batchable < sobject > {
    Id RecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Organisation').getRecordTypeId();
    public String accQuery;
    Public  list<String> mkt ;

    public PS_AccountSubSamplingNumberBatch(String market) {
        mkt = String.valueof(market).split(',',0);
        //Query for fetching all the child accounts of Primary selling account 
        accQuery = 'select id,name,Primary_Selling_Account__c,Sampling_Account_Number__c from account where RecordTypeId =:RecTypeId and market__c  in :mkt and Primary_Selling_Account__c != null and (Sampling_Sub_Account_Number__c = null OR Sampling_Sub_Account_Number__c = \' \') order by Primary_Selling_Account__c limit 50000';
    }
    
    //Start method for initializing the batch query 
    global Database.Querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(accQuery);
    }

    //Execute method - main logic for updating the sampling and sub sampling account numbers
    global void execute(Database.BatchableContext BC, List < sobject > scope) {
        Set < Id > PrimarySellingAccountId = new Set < Id > (); //Id to hold updated PrimarySellingAccountid
        Map < Id, String > samaccnummap = new Map < Id, String > (); //getting samplingaccountnumber of primary selling account
        Map < Id, String > subsamaccnummap = new Map < Id, String > (); //getting the maxsubsampaccnumber of the child account
        try{
            //Iterating over the scope to get the Primary selling account id of the child records
            for (sObject objScope: scope) {
                Account newObjScope = (Account) objScope; //type casting from generic sOject to Account
                PrimarySellingAccountId.add(newObjScope.Primary_Selling_Account__c);  //adding the id's of PSA to a set
    
            }
            //checking if the PSA set is not empty
            if (!PrimarySellingAccountId.isEmpty() && PrimarySellingAccountId.size() > 0) {
                List < Account > subaccnumber = new List < Account > (); //to get max value of PSA -> child account
                List < Account > updatesubacc = new List < Account > (); //to update the account
                subaccnumber = [SELECT Id, Sampling_Account_Number__c, (SELECT id, Sampling_Sub_Account_Number__c FROM Accounts1__r where (Sampling_Sub_Account_Number__c != null OR Sampling_Sub_Account_Number__c != '') order by Sampling_Sub_Account_Number__c desc limit 1) FROM Account where id = : PrimarySellingAccountId and(Sampling_Account_Number__c != ''
                OR Sampling_Account_Number__c != null) limit 50000];
                //for map inintialization
                if (!subaccnumber.isEmpty()) {  //if the PSA'id -> Sampling account number is not empty
                //iterate over the subaccnumber query
                    for (Account accObj: subaccnumber) {
                        //if the PSA -> Sampling_Account_Number__c is not null then put the value of SampAcountnumber in a map
                        if (accObj.Sampling_Account_Number__c != null && accObj.Sampling_Account_Number__c != '') {
                            samaccnummap.put(accObj.Id, accObj.Sampling_Account_Number__c);
                        }
                        if (accObj.Accounts1__r != null) {  //if chlid query is not null put max subsampaccnumber in a map
                            if (accObj.Accounts1__r.size() > 0 && (accObj.Accounts1__r[0].Sampling_Sub_Account_Number__c != '' || accObj.Accounts1__r[0].Sampling_Sub_Account_Number__c != null)) {
                                subsamaccnummap.put(accObj.Id, accObj.Accounts1__r[0].Sampling_Sub_Account_Number__c);
                            }
                        }
                    } //end for
                }
                ID sPrimaryacc = null;
                Integer subcountor = 0;  //to increment the sub_samp_Account number of the Child account
                //Iterate over the Account scope 
                for (sObject objScope: scope) {
                    Account newObjScope = (Account) objScope; //type casting from generic sOject to Account
                    //intiializing primary account id and countor
                    if (sPrimaryacc != newObjScope.Primary_Selling_Account__c) { //check if previous PSA is not equalto Current
                        sPrimaryacc = newObjScope.Primary_Selling_Account__c; //make the PSA as sPrimaryacc if not same
                        if (subsamaccnummap.containsKey(newObjScope.Primary_Selling_Account__c)) {
                            subcountor = Integer.ValueOf(subsamaccnummap.get(sPrimaryacc)); //add the PSA->SamplingAccnum to Subcountor
                            
                        } else {
                            subcountor = 0;  //once again initialize the primary selling again to 0's
                        }
                    }
    
                    //to update sampling and sub sampling acc number of child acc
                    if (samaccnummap.containsKey(sPrimaryacc)) {
                        newObjScope.Sampling_Account_Number__c = samaccnummap.get(sPrimaryacc); //get the value from the 1st map
                        subcountor++;  //Increment the countor 
                        if (subcountor == 0) {
                            newObjScope.Sampling_Sub_Account_Number__c = '001';  
                        } else {
                            Integer lendelta = Integer.ValueOf(Label.PS_Sub_Sampling_Number_Length) - String.ValueOf(subcountor).length();
                            String stemp = String.valueOf(subcountor);
                            If(lendelta > 0) {
                                for (Integer i = 0; i < lendelta; i++) {
                                    stemp = '0' + stemp;
                                }
                            }
                            newObjScope.Sampling_Sub_Account_Number__c = stemp;
                        }
                    
                        //updating the accounts with Sampling_Account_number and Sampling_Sub_Account_Number            
                        updatesubacc.add(newObjScope);
                    } // if close
                }
                if (!updatesubacc.isEmpty() && updatesubacc.size() > 0) {
                    Database.SaveResult[] lstResult = Database.update(updatesubacc, false);
                    if (lstResult != null) {
                        List < PS_ExceptionLogger__c > errloggerlist = new List < PS_ExceptionLogger__c > ();
                        for (Database.SaveResult sr: lstResult) {
                            String ErrMsg = '';
                            if (!sr.isSuccess() || Test.isRunningTest()) {
                                PS_ExceptionLogger__c errlogger = new PS_ExceptionLogger__c();
                                errlogger.InterfaceName__c = 'AccountSubSamplingNumberBatch';
                                errlogger.ApexClassName__c = 'PS_AccountSubSamplingNumberBatch';
                                errlogger.CallingMethod__c = 'Execute';
                                errlogger.UserLogin__c = UserInfo.getUserName();
                                errlogger.recordid__c = sr.getId();
                                for (Database.Error err: sr.getErrors())
                                ErrMsg = ErrMsg + err.getStatusCode() + ': ' + err.getMessage();
                                errlogger.ExceptionMessage__c = ErrMsg;
                                errloggerlist.add(errlogger);
                            }
                        }
                        if (errloggerlist.size() > 0) {
                            insert errloggerlist;
                        }
                    }
                }
            } //if close
         }//end try
         catch(Exception e){
            ExceptionFramework.LogException('AccountSubSamplingNumberBatch','PS_AccountSubSamplingNumberBatch','Execute',e.getMessage(),UserInfo.getUserName(),'');
         } // catch end
   } //method close

    //Finish Method
    global void finish(Database.BatchableContext BC) {
        //Below code will fetch the job Id
        AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors, a.JobType, a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById, a.CompletedDate From AsyncApexJob a WHERE id = : BC.getJobId()]; //get the job Id
    }
}