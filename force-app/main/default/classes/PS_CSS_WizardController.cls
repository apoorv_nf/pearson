/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_CSS_WizardController.cls 
Description:     Lightning controller class for PS_CSS_Wizard component 
Test class:      NA 
Date             Version              Author                          Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
08-JUN-2017         0.1          Manikanta Nagubilli           Wizard Controller page - CR-01385
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class PS_CSS_WizardController {
	
    //Method to fetch all the Data from the Custom Settings PS_WizardData
    @AuraEnabled
    public static List<PS_WizardData__c> getValueWizardData() 
    {        
        List<PS_WizardData__c> WizardList = [Select URL__c,name from PS_WizardData__c Order By name asc nulls last];
        return WizardList;
    }
}