@isTest
public class OpptyProductSalesPriceUpdateTest {
    @isTest
    private static void testOpptyProductUpdation(){                
        Id acrRecordTypeId = Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Learner').getRecordTypeId();
        List<Account> accountsList = new List<Account>();
        Account acc= new Account();
        acc.RecordTypeId = acrRecordTypeId;
        acc.Name = 'TestAccount';
        accountsList.add(acc);
        insert accountsList;        
        System.debug('accountsList' + accountsList);
        Id ContRecordTypeId = Contact.sObjectType.getDescribe().getRecordTypeInfosByName().get('Global Contact').getRecordTypeId();
        List<Contact> contactList= new List<Contact>();
        Contact Con = new Contact();
        con.RecordTypeId = ContRecordTypeId;
        con.AccountId = accountsList[0].Id;
        con.FirstName = 'ForTestAccount';
        con.LastName = 'TestContact1';
        con.Email='test@email.com';
        con.MobilePhone = '8884888472';
        con.Phone='1112223331'; 
        con.OtherPhone ='1112223331';
        con.Secondary_Email__c='testsec@email.com';
        con.Home_Phone__c ='34435465';        
        con.Preferred_Address__c='Mailing Address';
        con.MailingCity = 'Newcastle';        
        con.MailingState='Northumberland';
        con.MailingCountry='United Kingdom';
        con.MailingStreet='1st Street';
        con.MailingPostalCode='NE28 7BJ';
        con.IndividualId=null;
        contactList.add(con);
        insert contactList;        
        System.debug('contactList' + contactList);        
        List<Opportunity> oppList = new List<Opportunity>();
        Id opptyRecordTypeId = Opportunity.sObjectType.getDescribe().getRecordTypeInfosByName().get('B2B').getRecordTypeId();
        Opportunity oppObj = new Opportunity();
        oppObj.Name = 'TestOpportunityName';
        oppObj.CloseDate = date.today();
        oppObj.StageName = 'Needs Analysis';
        oppObj.RecordTypeId = opptyRecordTypeId;
        oppList.add(oppObj);
        insert oppList;
        // This is how we get the Standard PriceBook Id.  Prior to Summer '14, we needed
        // to use SeeAllData=true, so this is a big improvement
        Id pricebookId = Test.getStandardPricebookId();
        
        //Create your product
        Product2 prod = new Product2(
            Name = 'Product X',
            ProductCode = 'Pro-X',
            isActive = true
        );
        insert prod;
        
        //Create your pricebook entry
        PricebookEntry pbEntry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prod.Id,
            UnitPrice = 100.00,
            IsActive = true
        );
        insert pbEntry;
        
        //create your opportunity line item.  This assumes you already have an opportunity created, called opp
        OpportunityLineItem oli = new OpportunityLineItem(
            OpportunityId = oppList[0].Id,
            Quantity = 5,
            PricebookEntryId = pbEntry.Id,
            TotalPrice = 5 * pbEntry.UnitPrice
        );
        insert oli;
        Test.startTest(); 
        Database.executeBatch(new OpptyProductSalesPriceUpdate());
        Test.stopTest();
        
    }
}