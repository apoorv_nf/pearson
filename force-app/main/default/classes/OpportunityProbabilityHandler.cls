/************************************************************************************************************
 DATE            DEVELOPER        DESCRIPTION                               
 |  ====            =========        =========== 
 |  06/28/2016        IDC            This Batch Class is used for Opportunity Probability Calculations.
    17/10/2019     Rajesh Paleti     Modified for CR-02940(Replacing Apttus with Non-Apttus)
 /----------------------------------------------------------------------------------------------
*************************************************************************************************************/
public class OpportunityProbabilityHandler{
    public static Map<Id,Opportunity> oppStageAwarded; //child closed won
    public static Map<Id,Opportunity> oppLostmasternego; //child closed lost
    public static Map<Id,Opportunity> oppLostmasterneedanalysis; //child closed lost
    public static Map<Id,List<id>>    oppChildIds; //map to hold child oppty for quotes
    public static Map<Id,List<id>>    oppCampaignIds; //map to hold campaign ids
    //public static Map<Id,Apttus_Proposal__Proposal__c> listOfPrimaryQuote; //map to hold primary quotes
    public static Map<String,CampaignMember> mapOfCampaignMem50; //map to hold oppty50
    public static Map<String,CampaignMember> mapOfCampaignMem30; //map to hold oppty 30
    public static List<id> campaignIds;
    public static Set<Id> childIds;
    //Method to Categorise Opportunities.
    public static void mCategorizeOpportunities(Map <Id, Opportunity> opportunities){
        oppStageAwarded = new Map<Id,Opportunity>();
        oppLostmasternego = new Map<Id,Opportunity>();
        oppLostmasterneedanalysis = new Map<Id,Opportunity>();
        oppChildIds     = new Map<Id,List<id>>();
        oppCampaignIds     = new Map<Id,List<id>>();
        system.debug('oppty to process...'+opportunities.size());
        //iterate Master Opportunities.
        if(opportunities!=null){
            //loop thorugh child opportunities            
            for(Opportunity masterOpp : opportunities.values()){
                system.debug('oppty Id:'+masterOpp.Id);
                if(masterOpp.Opportunities__r != null && masterOpp.Opportunities__r.size()>0){
                system.debug('child oppty:'+masterOpp.Opportunities__r);
                for(Opportunity childOpps: masterOpp.Opportunities__r){
                    if(childOpps.StageName =='Closed Won' && childOpps.RecordType.Name != 'B2B' && !oppStageAwarded.containsKey(masterOpp.id)){ //Later on replace with label
                        oppStageAwarded.put(masterOpp.id,childOpps);
                        if(oppChildIds.containsKey(masterOpp.id))
                            oppChildIds.remove(masterOpp.id);
                        if(oppLostmasternego.containsKey(masterOpp.id))
                            oppLostmasternego.remove(masterOpp.id);
                        if(oppLostmasterneedanalysis.containsKey(masterOpp.id))
                            oppLostmasterneedanalysis.remove(masterOpp.id);                                                       
                        break;
                    }
                    if(childOpps.StageName =='Closed Lost' && childOpps.RecordType.Name != 'B2B'){
                        if (masterOpp.Probability > 50 && !oppLostmasternego.containsKey(masterOpp.id))
                            oppLostmasternego.put(masterOpp.id,childOpps);
                        else
                            oppLostmasterneedanalysis.put(masterOpp.id,childOpps);
                    }
                    else{
                        if(oppChildIds.containsKey(masterOpp.id))
                            oppChildIds.get(masterOpp.id).add(childOpps.id); 
                        else
                            oppChildIds.put(masterOpp.id,new List<id>{childOpps.id}); 
                    }
                }//end child opp loop
                }
                system.debug('kp1...'+oppStageAwarded);
                system.debug('kp2...'+oppLostmasternego);
                system.debug('kp3...'+oppLostmasterneedanalysis);
        system.debug('kp4...'+oppChildIds);                
                //loop through CampaignOpportunity
                if(masterOpp.Campaign_Opportunities__r != null && masterOpp.Campaign_Opportunities__r.size()>0){
                system.debug('campaign:'+masterOpp.Campaign_Opportunities__r);    
                for(Campaign_Opportunity__c camOpp:masterOpp.Campaign_Opportunities__r){
                    system.debug('inside campaign loop...'+opportunities);
                    if(oppStageAwarded.containsKey(camOpp.opportunity__c))
                        break;
                    else{
                        if(oppCampaignIds.containsKey(masterOpp.id))
                            oppCampaignIds.get(masterOpp.id).add(camOpp.campaign__c); 
                        else
                            oppCampaignIds.put(masterOpp.id,new List<id>{camOpp.campaign__c}); 
                    }                  
                }//End of For campaign
                }
                              
             }//end for master opportunity loop
             system.debug('print campaign list:'+oppCampaignIds);
        }//If close  
        
        mProcessChildOpptyQuotes();
        mProcessCampaignMembers();
        mCalculateOpptyProbablity(opportunities);    
     }//Method End        
       
       //Fetch Child Opportunities Primary Quote
    public static void mProcessChildOpptyQuotes(){
        childIds = new Set<Id>();
        //listOfPrimaryQuote = new Map<id,Apttus_Proposal__Proposal__c>();
        //Iterate over list of childoppids
        if(oppChildIds.size()>0){
        for(integer i=0;i<oppChildIds.values().size();i++){  // This can be initialised globally along with oppChildIds map.
                childIds.addAll(oppChildIds.values()[i]); 
        }//End of for List Ids
        // Commented for CR-2949 Apttus(Changes) by Rajesh Paleti
       /* for(Apttus_Proposal__Proposal__c quote :[Select id,Apttus_Proposal__Primary__c,Name,Apttus_Proposal__Opportunity__c,
                                        Apttus_Proposal__Opportunity__r.related_opportunity__c from Apttus_Proposal__Proposal__c 
                                        where Apttus_Proposal__Opportunity__c in:childIds and Apttus_Proposal__Primary__c=true])
        {
            listOfPrimaryQuote.put(quote.Apttus_Proposal__Opportunity__r.related_opportunity__c,quote);    
        }//End of For Quote */
        }
    }//Method End  
        
        //Fetch Campaign members
    public static void mProcessCampaignMembers(){
        mapOfCampaignMem50 = new Map<String,CampaignMember>();
        mapOfCampaignMem30 = new Map<String,CampaignMember>();        
        campaignIds = new List<Id>();
        //Iterate Over CampaignIds
        if(oppCampaignIds.size()>0){
        for(integer i=0;i<oppCampaignIds.values().size();i++){  
            campaignIds.addAll(oppCampaignIds.values()[i]); 
        }//End of for List Ids
        //system.debug('Processing campaign members:');
        for(CampaignMember cm :[SELECT Id,CampaignId,Centre_Number__c,Considers_Offering__c,CSC_Appointment_Booking__c,Status__c,Submitted_Event_Survey__c  FROM 
                                CampaignMember where CampaignId in:campaignIds and type=:'Contact' and Centre_Number__c!=null]){
            system.debug('inside cm:'+cm.Status__c+';'+cm.Submitted_Event_Survey__c+';'+cm.Considers_Offering__c);
            if((cm.Status__c==Label.Campaing_Status_CQI && cm.Submitted_Event_Survey__c==true && cm.Considers_Offering__c==true)||
               (cm.Status__c==Label.Campaign_Status_CSI && cm.Submitted_Event_Survey__c==true && cm.Considers_Offering__c==true && cm.CSC_Appointment_Booking__c==true)){
                   //system.debug('inside cm id'+ cm.id);
                    mapOfCampaignMem50.put(cm.CampaignId+cm.Centre_Number__c,cm); 
                    //if(mapOfCampaignMem30.containsKey(cm.CampaignId))
                    //  mapOfCampaignMem30.remove(cm.CampaignId);                   
                    //break;
            }//if close
            else{
                 if((cm.Status__c==Label.Campaign_Status_CR && cm.Submitted_Event_Survey__c==false)||
                     (cm.Status__c==Label.Campaign_Status_CR && cm.Submitted_Event_Survey__c==true && cm.Considers_Offering__c==false)){
                     mapOfCampaignMem30.put(cm.CampaignId+cm.Centre_Number__c,cm); 
                }        
            }//else close
        }//End of for CampaignMem
        }
        //system.debug('50 camp:'+mapOfCampaignMem50);
       // system.debug('30 camp:'+mapOfCampaignMem30);
    }//Method End
    
       //Calculate Opportunity Probability
    public static void mCalculateOpptyProbablity(Map <Id, Opportunity> opportunities){
        Map<Id,Opportunity> mapUpdateOpp = new Map<Id,Opportunity>();
        List<Opportunity> finalUpdatelList = new List<Opportunity>();
        for(Opportunity masterOppUpdate : opportunities.values()){
            //added a string which retrieves opportunity center number
            String oppPearsonCustomerNumber = masterOppUpdate.Account.Pearson_Customer_Number__c;
            Opportunity updateOpp;
            if(oppStageAwarded.containsKey(masterOppUpdate.id)){
               updateOpp = new Opportunity(id = masterOppUpdate.id); 
               updateOpp.StageName = Label.StageAwarded;
               updateOpp.Probability= Integer.valueof(Label.Probability_90);
               mapUpdateOpp.put(updateOpp.id,updateOpp);
            }//if Close   
            else if(oppLostmasternego.containsKey(masterOppUpdate.id)){
               updateOpp = new Opportunity(id = masterOppUpdate.id);                
               updateOpp.StageName = Label.StageNegotiation;
               updateOpp.Probability= Integer.valueof(Label.Probability_50); 
               mapUpdateOpp.put(updateOpp.id,updateOpp);
            }  //else if close            
            
           /* else if(listOfPrimaryQuote.containsKey(masterOppUpdate.id)){
               updateOpp = new Opportunity(id = masterOppUpdate.id);                
               updateOpp.StageName = Label.StageNegotiation;
               updateOpp.Probability= Integer.valueof(Label.Probability_50); 
               mapUpdateOpp.put(updateOpp.id,updateOpp);
            }  //else if close */
            
            else if (oppCampaignIds.containsKey(masterOppUpdate.id)){                
                List<Id> objCampId=oppCampaignIds.get(masterOppUpdate.id);
                for(integer i=0;i<objCampId.size();i++){
                    //campaingn member center number = oppty center number
                    //if(mapOfCampaignMem50.get(objCampId[i]).Centre_Number__c==oppPearsonCustomerNumber){
                        if(mapOfCampaignMem50.containsKey(objCampId[i]+oppPearsonCustomerNumber)){
                           updateOpp = new Opportunity(id = masterOppUpdate.id);                 
                           updateOpp.StageName = Label.StageNegotiation;
                           updateOpp.Probability= Integer.valueof(Label.Probability_50); 
                           mapUpdateOpp.put(updateOpp.id,updateOpp);
                           Break; 
                        }
                   // }//if close
                    else{
                    //campaingn member center number = oppty center number
                   // if(mapOfCampaignMem30.get(objCampId[i]).Centre_Number__c==oppPearsonCustomerNumber){
                        if(mapOfCampaignMem30.containsKey(objCampId[i]+oppPearsonCustomerNumber)){
                                updateOpp = new Opportunity(id = masterOppUpdate.id);                 
                                updateOpp.StageName = Label.StatgeSolutioning;
                                updateOpp.Probability= Integer.valueof(Label.Probability_30); 
                                mapUpdateOpp.put(updateOpp.id,updateOpp);
                            }
                      //  }  //if close
                    }//else close
                }//for close
            }//else close
            if (updateOpp == null && oppLostmasterneedanalysis.containsKey(masterOppUpdate.id)){
                updateOpp = new Opportunity(id = masterOppUpdate.id);                 
                updateOpp.StageName = Label.StageNeeds_Analysis;
                updateOpp.Probability= Integer.valueof(Label.Probability_10); 
                mapUpdateOpp.put(updateOpp.id,updateOpp);   
            }
        }//End of for Opp 
        finalUpdatelList.addAll(mapUpdateOpp.values());
        if (!finalUpdatelList.isEmpty()) {
            /* NOTE: Note:The Implementation of Exception Logger was intentionally skipped  as 
              functionally there is no need of it and if any exception occurs it will be captured in Apex Jobs and have a quick fix done.*/ 
            Database.update(finalUpdatelList,true);
        }//if Close..

    }//Method End 
    
    
}