/*******************************************************************************************************************
* Apex Class Name   : PS_AccountTrigger_Test 
* Created Date      : 11th Sep 2015
* Description       : Test class for PS_AccountTrigger
*******************************************************************************************************************/

@isTest
private class PS_AccountTrigger_Test{  
     static testMethod void myCreateAccountTest() {
        list<Account> AccountList = TestDataFactory.createAccount(2,'Learner');
                Insert AccountList; 
 
        test.startTest();
       List<Account> insertedAccounts = [SELECT Name, Phone FROM Account WHERE Id IN :AccountList]; 
       System.debug('test--->'+ insertedAccounts);
       
       for(Integer i=0 ; i< insertedAccounts.size(); i++){
          insertedAccounts[i].Phone = '000005';
    }
    checkRecurssionAfter.run = true;
    checkRecurssionBefore.run = true;
    update insertedAccounts;
            test.stopTest(); 

     }
 }