/**
 * Name : PS_PopulateFieldsOnOrderTest
 * Author : Accenture_Karan
 * Description : Test class used for testing PS_PopulateFieldsOnOrder
 * Date : 08/01/16 
 * Version : <intial Draft> 
 */

@isTest
public class PS_PopulateFieldsOnOrderTest
{
    static testMethod void testAssignPricebook1()
    {
    
        // Test data preparation                            
        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        usList[0].Market__c = 'UK';
        usList[0].Line_of_Business__c = 'Higher Ed';
        usList[0].Business_Unit__c = 'Higher Ed';
        usList[0].CurrencyIsoCode = 'GBP';
        usList[0].DefaultCurrencyIsoCode = 'GBP';
        insert usList;
        Bypass_Settings__c settings = new Bypass_Settings__c();
        settings.Disable_Triggers__c = true;
        settings.SetupOwnerId = usList[0].id;
        insert settings;  
        System.runAs(usList[0])
        {           
            Order ord1 = TestDataFactory.returnorder();
            Pricebook2 pb = new Pricebook2(Name = 'UK HE All Products Price Book', isActive = true);
            insert pb;
            Test.startTest();
            Order ord2 = TestDataFactory.CreateMoreOrders(ord1);
            ord2.Pricebook2Id = null;
            ord2.Market__c = 'UK';
            ord2.Line_of_Business__c = 'Higher Ed';
            ord2.Business_Unit__c = 'Higher Ed';
            ord2.CurrencyIsoCode = 'GBP';
            insert ord2;
            List<Order> ordList = new List<Order>();
            ordList.add(ord1);
            ordList.add(ord2);
            PS_PopulateFieldsOnOrder obj = new PS_PopulateFieldsOnOrder(ordList);
            obj.PopulatePriceBook();
            Test.stopTest();

        }           
    }
    
    static testMethod void testAssignPricebook2()
    {
    
        // Test data preparation                            
        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        usList[0].Market__c = 'UK';
        usList[0].Line_of_Business__c = 'Higher Ed';
        usList[0].Business_Unit__c = 'Higher Ed';
        usList[0].CurrencyIsoCode = 'GBP';
        usList[0].DefaultCurrencyIsoCode = 'GBP';
        insert usList;
        Bypass_Settings__c settings = new Bypass_Settings__c();
        settings.Disable_Triggers__c = true;
        settings.SetupOwnerId = usList[0].id;
        insert settings;  
        System.runAs(usList[0])
        {                       
            Order ord1 = TestDataFactory.returnorder();
            Order ord2 = TestDataFactory.CreateMoreOrders(ord1);
            //ord2.Pricebook2Id = null;
            Test.startTest();  
            insert ord2;            
            List<Order> ordList = new List<Order>();
            ordList.add(ord1);
            ordList.add(ord2);
            PS_PopulateFieldsOnOrder obj = new PS_PopulateFieldsOnOrder(ordList);
            obj.PopulatePriceBook();
            
            List<Pricebook2> pblist = [SELECT Id, Name, IsActive FROM Pricebook2 WHERE IsActive = true AND Name LIKE '%Standard%'];
            System.AssertEquals(pblist[0].Id, ord2.pricebook2Id);
            Test.stopTest();
        }           
    }    
}