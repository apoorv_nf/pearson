/*
 * Author: Apoorv (Neuraflash)
 * Date: 29/07/2019
 * Description: Test Class for EinsteinBotGetCaseDetails.
*/
@isTest
private class EinsteinBotGetCaseDetailsTest {
    
    @testSetup 
    static void setup() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {

            String firstName = 'John';
            String lastName = 'Doe';

            Account acc = new Account();
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_Creation_Request').getRecordTypeId();
            acc.Name = firstName + ' ' + lastName;

            insert acc;
            
            Contact con = new Contact();
            con.FirstName = firstName;
            con.LastName = lastName;
            con.Email = 'john@salesforce.com';
            con.AccountId = acc.Id;
            insert con;

            Case newCase = new Case();
            newCase.Subject = 'Test Case';
            newCase.origin = 'SMS';
            insert newCase;

            // Create a queue
            Group testGroup = new Group(Name='NAUS HETS SMS ACR', Type='Queue');
            insert testGroup;

            // Assign user to queue
            GroupMember testGrpMember = new GroupMember(GroupId = testGroup.Id, UserOrGroupId = UserInfo.getUserId());
            insert testGrpMember;

            MessagingChannel channnel = new MessagingChannel(MasterLabel = '+11234567890', IsActive = TRUE, 
                                                            MessageType = 'Text', TargetQueueId = testGroup.Id,
                                                            MessagingPlatformKey = '+11234567890', DeveloperName = 'Text_11234567890');
            insert channnel;
            
            // Create a test MessagingEndUser
            MessagingEndUser msgUser =  new MessagingEndUser(MessagingPlatformKey = '+12345678901', Name = '+12345678901',
                                                            IsOptedOut = FALSE, MessagingChannelId = channnel.Id,
                                                            MessageType = 'Text', ContactId = con.Id);
            insert msgUser;

            MessagingSession msgSessionWithCase = new MessagingSession(MessagingEndUserId = msgUser.Id, MessagingChannelId = channnel.Id, 
                                                               CaseId = newCase.Id, Status = 'Active');
            insert msgSessionWithCase;

            MessagingEndUser theMsgUser =  new MessagingEndUser(MessagingPlatformKey = '+10987654321', Name = '+10987654321',
                                                            IsOptedOut = FALSE, MessagingChannelId = channnel.Id,
                                                            MessageType = 'Text', ContactId = con.Id);
            insert theMsgUser;

            MessagingSession msgSessionWithoutCase = new MessagingSession(MessagingEndUserId = theMsgUser.Id, MessagingChannelId = channnel.Id, 
                                                               Status = 'Active');
            insert msgSessionWithoutCase;

            insert new EBotResponseSettings__c(Delay__c = 1000);
        }
    }

    @isTest
    private static void testGetCaseDetails(){

        MessagingSession msgSession = [SELECT Id, CaseId FROM MessagingSession WHERE CaseId != null LIMIT 1];

        Test.startTest();
            List<Case> caseList = EinsteinBotGetCaseDetails.getCaseDetails(new List<String>{msgSession.Id});
        Test.stopTest();
        system.assertEquals(!caseList.isEmpty(), TRUE);
    }

    @isTest
    private static void testGetCaseDetailsTimeDelay(){

        MessagingSession msgSession = [SELECT Id, CaseId FROM MessagingSession WHERE CaseId = null LIMIT 1];

        Test.startTest();
            List<Case> caseList = EinsteinBotGetCaseDetails.getCaseDetails(new List<String>{msgSession.Id});
        Test.stopTest();
        system.assertEquals(caseList == null, TRUE);
    }
}