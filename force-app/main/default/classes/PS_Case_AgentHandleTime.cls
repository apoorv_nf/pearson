/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_Case_AgentHandleTime.cls 
   Description:     On afterUpdate of Case record 
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  13/12/2015         1.0            Chaitra                           Method to calculate Agent Handle Time  
  07/01/2016         1.1            Ajay                              Updated method to calculate Agent Handle Time wrt Businesshours
  09/03/2016         1.2            Ajay                              Updated code according to SME reviews   
   20/07/2016         1.3           RG                     Updated code for DR-0774 (Added a new method mCalculateAgentHandleTimeOwnerChange)
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
Public class PS_Case_AgentHandleTime {
 /**
  * Description : To calculate agent handling time of a case with respect to business hours
  * @param NA
  * @return NA
  * @throws NA
  **/
 Public static void mCalculateAgentHandleTime(List <
   case >oNewCaseList, Map < Id, Case > oOldmap, Map < Id, Case > oNewCaseMap) {
   try {
    system.debug('Entered into PS_Case_AgentHandleTime');
    Set < Id > sCaseidList = new set < ID > ();
    List < Case_Owner_Tracking__c > sCOTList = new List < Case_Owner_Tracking__c > ();
    Case_Owner_Tracking__c oCOT = new Case_Owner_Tracking__c();
    List <
     case >sClist = new list <
     case >();
    long businessMillisecondsDiff = 0;
    decimal businessHoursDiff = 0;
    //PP:12/14/2015: Added to check Case Service Record types
    List < Case > oCaseList = new List < Case > ();
    system.debug('New List Item===='+oNewCaseList);
    oCaseList = CommonUtils.getCaseServiceRecordType(System.Label.PS_ServiceRecordTypes, oNewCaseList);
       system.debug('New List Item=oCaseList==='+oCaseList);
    For(
     case c:
      oCaseList) {
     //Retrieve all the case when the email is sent to users
     //if(c.EmailRepliedTime__c !=null){
     //  if(oOldmap.get(c.id).EmailRepliedTime__c!= oNewCaseMap.get(c.id).EmailRepliedTime__c){
     sCaseidList.add(c.id);
     //  } 
     //  }
    }


    if (sCaseidList.size() > 0) {
     sClist = [select id, EmailReadTime__c, Status, OwnerId, LastModifiedDate, EmailRepliedTime__c, BusinessHoursId, (select id, NewTime__c, CaseModifiedDateTime__c, Agent_Handle_Time__c, CaseClosed__c, CreatedDate from Case_Owner_Trackings__r order by CreatedDate desc limit 1) from Case where id IN: sCaseidList];
     if (sClist.size() > 0) {
      for (Case c: sClist) {
       if (!c.Case_Owner_Trackings__r.isempty()) {
        oCOT = c.Case_Owner_Trackings__r;
        //if(oCOT.CaseClosed__c == false){ //to check if the case associated to caseOwnerTracking is not closed
       /*  if(oOldmap.get(c.id).OwnerId != c.OwnerId){
            System.debug('owner change');
            oCOT.CaseModifiedDateTime__c = System.now();
            businessMillisecondsDiff = BusinessHours.diff(c.BusinessHoursId, oCOT.CreatedDate, c.LastModifiedDate);
            businessHoursDiff = businessMillisecondsDiff / 1000;
            oCOT.Agent_Handle_Time__c += businessHoursDiff;
        } */
         if ((oOldmap.get(c.id).Status == 'New' || oOldmap.get(c.id).Status == 'In Progress') && c.status == 'Closed') {
             //System.debug('oOldmap.get(c.id).Status -->' + oOldmap.get(c.id).Status + 'c.status -->' + c.status);
             businessMillisecondsDiff = BusinessHours.diff(c.BusinessHoursId, oCOT.CreatedDate, c.LastModifiedDate);
             businessHoursDiff = businessMillisecondsDiff / 1000;
             if (oOldmap.get(c.id).Status == 'In Progress'){ 
                 oCOT.Agent_Handle_Time__c = oCOT.NewTime__c + businessHoursDiff;
                 oCOT.NewTime__c = 0;
              }
             else 
                 oCOT.Agent_Handle_Time__c += businessHoursDiff;
             
         } 
         else if ((oOldmap.get(c.id).Status == 'New' || oOldmap.get(c.id).Status == 'Waiting on Customer' || oOldmap.get(c.id).Status == 'Assigned Outside Support') && c.status == 'In Progress') {
             //System.debug('oOldmap.get(c.id).Status -->' + oOldmap.get(c.id).Status + 'c.status -->' + c.status);
             businessMillisecondsDiff = BusinessHours.diff(c.BusinessHoursId, oCOT.CreatedDate, c.LastModifiedDate);
             businessHoursDiff = businessMillisecondsDiff / 1000;
             if (oOldmap.get(c.id).Status == 'New'){
                  oCOT.NewTime__c = businessHoursDiff;
                  oCOT.CaseModifiedDateTime__c = System.now();
                  oCOT.Agent_Handle_Time__c += businessHoursDiff;
             }
             else 
                  oCOT.NewTime__c = 0;
              //oCOT.Agent_Handle_Time__c += businessHoursDiff;
        }
        else if ((oOldmap.get(c.id).Status == 'New' || oOldmap.get(c.id).Status == 'In Progress') && (c.status == 'Waiting on Customer' || c.status == 'Assigned Outside Support')) {
             //System.debug('oOldmap.get(c.id).Status -->' + oOldmap.get(c.id).Status + 'c.status -->' + c.status);
             if(oOldmap.get(c.id).Status == 'In Progress'){
                businessMillisecondsDiff = BusinessHours.diff(c.BusinessHoursId, oCOT.CaseModifiedDateTime__c, c.LastModifiedDate);
             }else{  
                businessMillisecondsDiff = BusinessHours.diff(c.BusinessHoursId, oCOT.CreatedDate, c.LastModifiedDate);
             }
             businessHoursDiff = businessMillisecondsDiff / 1000;
             oCOT.NewTime__c = businessHoursDiff;
             oCOT.CaseModifiedDateTime__c = System.now();
             oCOT.Agent_Handle_Time__c += businessHoursDiff;
        }
            sCOTList.add(oCOT);
            
         /*else if ((oOldmap.get(c.id).Status == 'Waiting on Customer' || oOldmap.get(c.id).Status == 'Assigned Outside Support') && (c.status == 'Closed' || c.status == 'In Progress')) {
             System.debug('oOldmap.get(c.id).Status -->' + oOldmap.get(c.id).Status + 'c.status -->' + c.status);
             businessMillisecondsDiff = BusinessHours.diff(c.BusinessHoursId, oCOT.CaseModifiedDateTime__c, c.LastModifiedDate);
             businessHoursDiff = businessMillisecondsDiff / 1000;
             //if (c.status == 'In Progress') 
             // oCOT.NewTime__c = businessHoursDiff;
             //else
              //oCOT.NewTime__c = 0;
             //oCOT.Agent_Handle_Time__c = 0;
        }       
        
        
        /* if( oCOT.Agent_Handle_Time__c == 0){
           businessMillisecondsDiff = BusinessHours.diff(c.BusinessHoursId,oCOT.CreatedDate,c.EmailRepliedTime__c); 
        }
        else{
            businessMillisecondsDiff = BusinessHours.diff(c.BusinessHoursId,c.EmailReadTime__c,c.EmailRepliedTime__c); 
        }*/
        //businessHoursDiff = businessMillisecondsDiff / (1000.0*60.0);
        //businessHoursDiff = businessMillisecondsDiff / 1000;
        //oCOT.Agent_Handle_Time__c += businessHoursDiff;
    
        //} 
       }
      }
      if (!sCOTList.isEmpty()) {
       Database.SaveResult[] sCOTListResult = Database.update(sCOTList, false);
       if (sCOTListResult != null) {
        List < PS_ExceptionLogger__c > errloggerlist = new List < PS_ExceptionLogger__c > ();
        for (Database.SaveResult sr: sCOTListResult) {
         String ErrMsg = '';
         if (!sr.isSuccess() || Test.isRunningTest()) {
          PS_ExceptionLogger__c errlogger = new PS_ExceptionLogger__c();
          errlogger.InterfaceName__c = 'Case fcr updation on closure Status';
          errlogger.ApexClassName__c = 'PS_Case_AgentHandleTime';
          errlogger.CallingMethod__c = 'Execute';
          errlogger.UserLogin__c = UserInfo.getUserName();
          errlogger.recordid__c = sr.getId();
          for (Database.Error err: sr.getErrors())
           ErrMsg = ErrMsg + err.getStatusCode() + ': ' + err.getMessage();
          errlogger.ExceptionMessage__c = ErrMsg;
          errloggerlist.add(errlogger);
         }
        }
        if (errloggerlist.size() > 0) {
         update errloggerlist;
        }
       }
      } //end of IF
     } // end of if
    }
   } //end of try
   catch (Exception e) {
    ExceptionFramework.LogException('Case_AgentHandleTime', 'PS_Case_AgentHandleTime', 'mCalculateAgentHandleTime', e.getMessage(), UserInfo.getUserName(), '');
   }
  } //end of method
  Public static void mCalculateAgentHandleTimeOwnerChange(List <case> oNewCaseList, Map <Id,Case> oOldmap, Map <Id,Case>  oNewCaseMap) {
   try {
    system.debug('Entered into PS_Case_AgentHandleTime');
    Set < Id > sCaseidList = new set < ID > ();
    List < Case_Owner_Tracking__c > sCOTList = new List < Case_Owner_Tracking__c > ();
    Case_Owner_Tracking__c oCOT = new Case_Owner_Tracking__c();
    List <case >sClist = new list <case>();
    long businessMillisecondsDiff = 0;
    decimal businessHoursDiff = 0;
    //PP:12/14/2015: Added to check Case Service Record types
    List < Case > oCaseList = new List < Case > ();
    oCaseList = CommonUtils.getCaseServiceRecordType(System.Label.PS_ServiceRecordTypes, oNewCaseList);
    For(case c:oCaseList) {
     sCaseidList.add(c.id);
    }
    if (sCaseidList.size() > 0) {
     sClist = [select id, EmailReadTime__c, Status, OwnerId, LastModifiedDate, EmailRepliedTime__c, BusinessHoursId, (select id, NewTime__c, CaseModifiedDateTime__c, Agent_Handle_Time__c, CaseClosed__c, CreatedDate from Case_Owner_Trackings__r order by CreatedDate desc limit 1) from Case where id IN: sCaseidList];
     if (sClist.size() > 0) {
      for (Case c: sClist) {
       if (!c.Case_Owner_Trackings__r.isempty()){
        oCOT = c.Case_Owner_Trackings__r;
        //if(oCOT.CaseClosed__c == false){ //to check if the case associated to caseOwnerTracking is not closed
        if(oOldmap.get(c.id).OwnerId != c.OwnerId){
            System.debug('owner change');
            oCOT.CaseModifiedDateTime__c = System.now();
            businessMillisecondsDiff = BusinessHours.diff(c.BusinessHoursId, oCOT.CreatedDate, c.LastModifiedDate);
            businessHoursDiff = businessMillisecondsDiff / 1000;
            oCOT.Agent_Handle_Time__c += businessHoursDiff;
        }
        sCOTList.add(oCOT);
       }
      }
      if (!sCOTList.isEmpty()) {
       Database.SaveResult[] sCOTListResult = Database.update(sCOTList, false);
       if (sCOTListResult != null) {
        List < PS_ExceptionLogger__c > errloggerlist = new List < PS_ExceptionLogger__c > ();
        for (Database.SaveResult sr: sCOTListResult) {
         String ErrMsg = '';
         if (!sr.isSuccess() || Test.isRunningTest()) {
          PS_ExceptionLogger__c errlogger = new PS_ExceptionLogger__c();
          errlogger.InterfaceName__c = 'Case fcr updation on closure Status';
          errlogger.ApexClassName__c = 'PS_Case_AgentHandleTime';
          errlogger.CallingMethod__c = 'mCalculateAgentHandleTimeOwnerChange';
          errlogger.UserLogin__c = UserInfo.getUserName();
          errlogger.recordid__c = sr.getId();
          for (Database.Error err: sr.getErrors())
           ErrMsg = ErrMsg + err.getStatusCode() + ': ' + err.getMessage();
          errlogger.ExceptionMessage__c = ErrMsg;
          errloggerlist.add(errlogger);
         }
        }
        if (errloggerlist.size() > 0) {
         update errloggerlist;
        }
       }
      } //end of IF
     } // end of if
    }
   } //end of try
   catch (Exception e) {
    ExceptionFramework.LogException('Case_AgentHandleTime', 'PS_Case_AgentHandleTime', 'mCalculateAgentHandleTimeOwnerChange', e.getMessage(), UserInfo.getUserName(), '');
   }
  } //end of method
}