/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            DeleteRecordsBatchSchedule.cls 
   Description:     Scheduler class for scheduling DeleteRecordsBatch
   Test Class:      DeleteRecordsBatchScheduleTest
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
  08/13/2019        0.1        Mani Kagithoju                Created
                                
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

global class DeleteRecordsBatchSchedule implements schedulable
{
    global void execute(SchedulableContext sc)
    {
        General_One_CRM_Settings__c crmCS = General_One_CRM_Settings__c.getValues('Delete Records Batch Size Limit');
        String oncrmCSVal = crmCS.Value__c;
        DeleteRecordsBatch drb = new DeleteRecordsBatch ();
        database.executebatch(drb,Integer.valueOf(oncrmCSVal));
    }
}