public class EmailHasherConntroller {


    public void doBatch() {

        BatchEmailHasher batchHasher = new BatchEmailHasher();

        Id batchId = Database.executeBatch(batchHasher);

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Batch Successfully Launched'));


    }

}