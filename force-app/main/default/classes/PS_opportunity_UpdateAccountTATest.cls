/***
* Apex Class : PS_opportunity_UpdateAccountTeamAdmin
***/

@isTest
public without sharing class PS_opportunity_UpdateAccountTATest 
{
  static testMethod void verifyupdateadmin()
  {  
    //List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
    //insert usrLst;    

    Profile profileId = [SELECT Id FROM Profile WHERE Name =: 'System Administrator' limit 1];
        
    List<User> usrLst = new List<User>();
    User usr = new user();
        usr.LastName = 'LIVESTON';
        usr.FirstName='JASON';
        usr.Alias = 'jliv';
        usr.Email = 'jason.liveston@pearson.com';
        usr.Username = 'jason.liveston@asdfsdf.com';                          
        usr.ProfileId = profileId.id;                           
        usr.TimeZoneSidKey = 'GMT';
        usr.LanguageLocaleKey = 'en_US';
        usr.EmailEncodingKey = 'UTF-8';
        usr.LocaleSidKey = 'en_US';
    
    usrLst.add(usr);

      // Added by Navaneeth for Code Coverage Improvement
      
        User usrAccTM = new user();
        usrAccTM.LastName = 'ACCOUNT';
        usrAccTM.FirstName='TeamMember';
        usrAccTM.Alias = 'acctm';
        usrAccTM.Email = 'ACCOUNT.TeamMember@pearson.com';
        usrAccTM.Username = 'TeamMember.TeamMember@asdfsdf.com';                          
        usrAccTM.ProfileId = profileId.id;                           
        usrAccTM.TimeZoneSidKey = 'GMT';
        usrAccTM.LanguageLocaleKey = 'en_US';
        usrAccTM.EmailEncodingKey = 'UTF-8';
        usrAccTM.LocaleSidKey = 'en_US';
        usrAccTM.IsActive = true;
        usrLst.add(usrAccTM);
      
      // End of Additon by Navaneeth for Code Coverage Improvement
      
      Insert usrLst;
    usr.IsActive = true;
    
    Update usrLst;
    
    System.runas(usrLst[0])    
    {
      Account acc = (Account)TestClassAutomation.createSObject('Account');
      acc.Name = 'Test';
      acc.IsCreatedFromLead__c = true;
      acc.ShippingCity = 'Bangalore';
      acc.ShippingCountry = 'India';
      acc.ShippingStreet = 'Ecospace';
      acc.ShippingPostalCode = '560103';
      insert acc; 
     
      // Start of Addition by Navaneeth for Code Coverage Improvement
      AccountTeamMember accountTeamMember = new AccountTeamMember();
      accountTeamMember.TeamMemberRole='Account Administrator';
      accountTeamMember.AccountId= acc.Id;
      accountTeamMember.userId = usrLst[0].Id;
      // End of Addition by Navaneeth for Code Coverage Improvement
     
      Account acc2 = (Account)TestClassAutomation.createSObject('Account');
      acc2.Name = 'Campus Test';
      acc2.Pearson_Campus__c = true;
      acc2.IsCreatedFromLead__c = true;
      acc2.ShippingCity = 'Bangalore';
      acc2.ShippingCountry = 'India';
      acc2.ShippingStreet = 'Ecospace';
      acc2.ShippingPostalCode = '560103';
      insert acc2; 
    
      // Start of Addition by Navaneeth for Code Coverage Improvement
      AccountTeamMember accountTeamMemberCA = new AccountTeamMember();
      accountTeamMemberCA.TeamMemberRole='Account Administrator';
      accountTeamMemberCA.AccountId= acc2.Id;
      accountTeamMemberCA.userId = usrLst[1].Id;    
      // End of Addition by Navaneeth for Code Coverage Improvement
        
      Account_Correlation__C ac = new Account_Correlation__C(Account__C = acc.Id, External_ID_Name__c = 'eVision Learner Number', External_ID__c = 'External ID');
      insert ac; 
     
      contact con = (Contact)TestClassAutomation.createSObject('Contact');
      con.Lastname= 'testcon';
      con.Firstname= 'testcon1';
      //added by Pooja for validation rule for Salutation,MobilePhone,First_Language__c
      con.Salutation='Mr.'; 
      con.MobilePhone='+914566897600'; 
      con.First_Language__c='English'; 
      con.Preferred_Address__c = 'Other Address';
      con.OtherCountry  = 'India';
      con.OtherStreet = 'Test';
      con.OtherCity  = 'Test';
      con.OtherPostalCode  = '123456';
      con.accountId = acc.Id;
      insert con;     
    
      Id oppRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('D2L').getRecordTypeId();
          
      List<Opportunity> oppty = new List<Opportunity>();    
      Opportunity opp = (Opportunity)TestClassAutomation.createSObject('Opportunity');
      opp.AccountId = acc.Id;
      opp.CurrencyIsoCode = 'GBP';
      opp.Lost_Reason_Other__c = 'XXX';
      opp.Academic_Vetting_Status__c = 'XXXXX';
      opp.Type = 'New Business';
      opp.StageName = 'Negotiation';
      opp.RecordTypeId = oppRecTypeId;
      opp.QuoteMigration_Campus__c = 'Campus Test';
      //CP 07/25/2018 opp.Qualification_Campus__c = acc2.id;
      //opp.SyncedQuoteId = NULL;
      insert opp;
      oppty.add(opp);                               
                 
      //accountTeamMember.User.IsActive = true;    
           
      insert accountTeamMember;
      insert accountTeamMemberCA;
      
        
      // Added by Navaneeth for Code Coverage Improvement

      Quote quoteSynced = (Quote)TestClassAutomation.createSObject('Quote');
      quoteSynced.OpportunityId = opp.Id; 
      quoteSynced.Conferrer__c = 'PIHE';
      quoteSynced.Qualification__c = 'Bachelor of Commerce';
      quoteSynced.Qualification_Level__c = '1';  
      quoteSynced.Qualification_Campus__c = acc2.id;
        
      insert quoteSynced;
          
      oppty[0].SyncedQuoteId = quoteSynced.Id;
      //oppty[0].SyncedQuote.Qualification_Campus__c = acc2.id;
      update oppty;
      
      // End of Addition by Navaneeth for Code Coverage Improvement 

      test.startTest();
      Set<Id> campusAccountId = new Set<Id>();
      campusAccountId.add(acc2.Id);
      List<AccountTeamMember> lstAccTM = [SELECT UserId, User.Name, TeamMemberRole, Id, AccountId, Account.Name FROM AccountTeamMember 
                                                WHERE  TeamMemberRole IN ('Account Administrator') AND AccountId IN:campusAccountId and User.IsActive = true];
      PS_opportunity_UpdateAccountTeamAdmin.updateadmin(oppty);
        
      test.stopTest();
    
    }  
  }
}