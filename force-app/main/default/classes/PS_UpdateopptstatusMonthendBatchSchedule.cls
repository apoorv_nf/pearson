global class PS_UpdateopptstatusMonthendBatchSchedule implements Schedulable {

    global void execute(SchedulableContext ctx) {
       PS_UpdateopptstatusMonthendBatch psupdate = new PS_UpdateopptstatusMonthendBatch();
       database.executeBatch(psupdate);
        
    }
    
}