/*************************************************************************************
Author: Tushar Jadav
Description: Replace System Admin's Email to actual email address after refresh.
Date: 10/17/2016

Davi Borges 02/15/2017 - Adding Email like '%@example.com%' to avoid locks in production
Darshan MS  05/9/2019  - Replaced "@example.com" with ".invalid" as the value in label "replaceinvalidemail" as part of CR-02759

**************************************************************************************/
global class PS_UpdateUserEmailPostRefresh implements SandboxPostCopy {
    global void runApexClass(SandboxContext context) {

        //List < User > userEmails = [Select Id, Email from User where User.Profile.Name = 'System Administrator' and Email like '%@example.com%'];
        /** Darshan <START> **/
        string query= 'Select Id, Email from User where User.Profile.Name=\'System Administrator\' and Email like \'%'+label.replaceinvalidemail+'\'';
        List < User > userEmails = Database.Query(query);
                
        /** Darshan <END> **/
        
        List < User > userEmailsUpd = new List < User > ();
        for (User u: userEmails) {
            String EmailUpd = u.Email;
            //EmailUpd = EmailUpd.replace('@example.com', '');
            
            /** Darshan <START> **/
            
              EmailUpd = EmailUpd.replace(label.replaceinvalidemail,'');
              
             /** Darshan <END> **/  
             
            //EmailUpd = EmailUpd.replace('=', '@');
            u.Email = EmailUpd;
            userEmailsUpd.add(u);
        }

        system.debug('Email List' + userEmailsUpd);
        update userEmailsUpd;


    }
}