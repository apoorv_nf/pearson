global class PS_MarketingBlackoutScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		PS_BatchMarketingBlackoutAccountUpdate b = new PS_BatchMarketingBlackoutAccountUpdate();
		database.executebatch(b);
	}
}