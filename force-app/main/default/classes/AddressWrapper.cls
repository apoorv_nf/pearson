/********************************************************************
KP    v1.0    6/24/2016    R6 Address validation response wrapper class to display in OneCRM
********************************************************************/

public class AddressWrapper{
    public Boolean bSelectedAddress{get; set;}
    public String sAddr1{get; set;}
    public String sAddr2{get; set;}
    public String sAddr3{get; set;}
    public String sAddr4{get; set;}
    public String sCity{get; set;}
    public String sState{get; set;}
    public String sCounty{get; set;} // Manimala :IRUK - Added for CR-2483 
    public String sZipCode{get; set;}
    public String sCountry{get; set;}    
    public String sOrgName{get; set;}
    public String sMailablityScore{get; set;}
    public String sMatchCode{get; set;}
    
    // Manimala :IRUK - passed the county attribute in addresswrapper for CR-2483 
    public AddressWrapper(String sAddr1,String sAddr2,String sAddr3,String sAddr4,String sCity,String sState,String sCounty,String sCountry,
    String sZip,String sOrg,String sMScore, String sMatchCode){
        this.sAddr1=sAddr1;
        this.sAddr2=sAddr2;
        this.sAddr3=sAddr3;
        this.sAddr4=sAddr4;
        this.sCity=sCity;
        this.sState=sState;
        this.sCounty=sCounty; // Manimala :IRUK - Added for CR-2483 
        this.sCountry=sCountry;        
        this.sZipCode=sZip;
        this.bSelectedAddress=false;
        this.sOrgName = sOrg;
        this.sMailablityScore = sMScore;
        this.sMatchCode = sMatchCode;
    }
    public AddressWrapper(){
    }
}