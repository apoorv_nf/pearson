@isTest
public class PS_UpdateUserEmailPostRefreshTest{
    static testMethod void testUpdatesysAdmEmail() {
         Profile pname = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
   List<User> u1 = TestDataFactory.createUser(pname.Id,1);
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu1';
            u1[0].Email ='Opptyproposaltest11@pearson.com.invalid';
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='Opptyproposaltest12';
            u1[0].LanguageLocaleKey ='en_US';
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = pname.Id;
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName ='standarduserQuoteCreation1@testorg.com';
            u1[0].Market__c ='US';
            u1[0].Business_Unit__c = 'Higher Ed';
            u1[0].line_of_Business__c = 'Higher Ed';
            u1[0].Product_Business_Unit__c = 'Higher Ed';
            insert u1;
        }
        //Id usrID=UserInfo.getUserId();
        Test.startTest();
        //PostRefresh script needs 2 IDs 
        Test.testSandboxPostCopyScript(new PS_UpdateUserEmailPostRefresh(),u1[0].id,u1[0].id,'MySandboxName');
        Test.stopTest();
    }
}