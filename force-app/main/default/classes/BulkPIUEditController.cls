/*Controller For BulkContactPIUEditPage

oct-13-2015  Madhusudhan  : code added to implement mass update requirement RD-01488, RD-01491 
oct-22-2015 KP: D-2241: fix to mass update fields only on product selected, Enable mass update after mass update selection, indent
oct-27-2015: KP: D-2241: Enable mass update fields on individual selection of contact fields.
Oct-27-2015: Madhu: Realigned PIU title and Edition for 2.2 fix
oct-28-2015 KP : Primary check box logic 
Apr-29-2016 RJ: Updated error message when no contact is choosen
May-26-2016: YK : DR-378: Autoselection of 'Tech Required' value on PIU conversion logic.
Mar-28-2016 RG : for null pointer check on the update if nothing is checked and clicked on update
May-30-2016 SA : For Sorting PIU - DR-639
June-16-2016 Sneha: Modified query to sort Competitor Products-DR-0389
June-22-2016 Pooja : Modified query to make the contacts of all piu's visible when the status is active/inactive. 
*/


public with sharing class BulkPIUEditController{

    Public Id courseID;
    Public integer techRequired;
    public List<AssetsWrapper> assetList {get;set;}
    public Map<String,List<AssetsWrapper>> assetDetails{get;set;}
    Public List<String> ProductAuthor {get;set;} //For Sorting PIU - DR-639
    public List<String> li{get;set;}
    public Map<Id,String> productIdWithName{get;set;}
    public List<Competitor_Product__c> CompProd{get;set;}
        
    public List<Asset> selectedAssets = new List<Asset>();
    public String primcntmap {get;set;}
    public String schangedcId {get;set;}
    public boolean schangedflg {get;set;}
    public String sproduct{get;set;}
    public String usageChange{get;set;}
    public String prdNamechnge{get;set;}
    public String statuschange{get;set;}
    public String lmschange{get;set;}
    public Date valDate{get;set;}
    public boolean smassindicator {get;set;}
    public boolean primchange{get;set;}
    public String typePIU{get;set;}
    public boolean butType{get;set;}
    public boolean hasChanged{get;set;}
    public String URL{get;set;}
// It returns Asset details
    public Map<String,List<AssetsWrapper>>  getAsset()
    {
        return assetDetails;
    }
        
// It returns productIdWithName
    public Map<Id,String> getProductIdWithName()
    {        
       return productIdWithName;       
    }
   //getProdList method added For Sorting PIU - DR-639
    public List<SortPiu> getProdList()
    { 
      set<ID> productIdSet1 = new set<ID>();
      set<ID> productIdSet2 = new set<ID>();
      List<SortPiu> ProductList2 = new list<SortPiu>();
    
    productIdSet1=productIdWithName.keySet();
    Id usHERecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('US HE Record Type').getRecordTypeId();
        //Query modified by Pooja for DR-381
    for(asset ProductList1 : [Select id,name,Product_Author__c,Product2Id,Product_ISBN__c,Product_edition__c,Contact.name,Usage__c, Status__c, Third_Party_LMS__c,Primary__c,InstallDate
    from Asset where course__c=:courseID and recordtypeid =: usHERecordTypeId and Product2Id IN: productIdSet1 order by Is_Competitor_Product__c desc, installdate desc,OpportunityAssetCreateDate__c desc Nulls last ]) { 
    if(!productIdSet2.contains(ProductList1.Product2Id)){    
     ProductList2.add(new SortPiu(ProductList1));
     
     productIdSet2.add(ProductList1.Product2Id);
     } 
      }
      return ProductList2; 
    }
    
    public void changedValue()
    {
        hasChanged = true;
    }
         
   
// to navigate to the courseid page

    public pagereference courseCancelDetailPage(){       
      return new pagereference('/'+courseID);
    }
    
    public void OK()
    {
        if(selectedAssets != null && !selectedAssets.isEmpty())
        selectedAssets.clear();
        for (String key : assetDetails.keySet()) {
        
        List<AssetsWrapper> assetList = assetDetails.get(key);
        
            for(AssetsWrapper asswrapper: assetList){        
            if(asswrapper.selected == true || Test.isRunningTest())    
                selectedAssets.add(asswrapper.ass);   
            }
        }
        boolean flag = editAll();  
        if(flag)
        {
             changeButton();
              
        }
         else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No record(s) selected to update')); //Added by RG (28/03/2016)
           // return null;
        }    
    }
    public void updateContactList(){
        if(schangedflg == false)
        {
        for (String key : assetDetails.keySet()) {  
             List<AssetsWrapper> assetListOnChange = assetDetails.get(key);    
                  for(AssetsWrapper asswrapper: assetListOnChange){ 
                  if (schangedcId != '' && schangedcId != null){
                  System.debug('asswrapper.ass.Contact.id...'+asswrapper.ass.Contact.id+'asswrapper.ass.Primary__c....'+asswrapper.ass.Primary__c);       
                             if(asswrapper.ass.Contact.id==schangedcId && asswrapper.ass.Primary__c == true && asswrapper.ass.Product2id!=sproduct)    
                                 
                                asswrapper.ass.Primary__c = false; 
                                }             

            }        
         }
     }
        schangedflg = null;
        schangedcId ='';
        sproduct='';
    }
    public void changeButton(){
               
        if(butType)
        {
            typePIU = 'Inactive';
            butType = false;
        }else
        {
             typePIU = 'Active';
             butType = true;
        }      
        hasChanged = false;
        productIdWithName= new Map<Id,String>();
        getPIUContacts();
        getUsages();
        getStatus(); 
        getLMS();
        
    }
// to display contacts of PIU on VF page

    public BulkPIUEditController (){    
        primcntmap = ''; 
        
        productIdWithName= new Map<Id,String>();
        hasChanged = false;
        courseID= ApexPages.currentPage().getParameters().get('courseId');
        typePIU = ApexPages.currentPage().getParameters().get('Type');
        if(typePIU == 'Active')
        {
            butType = true;
        }else
        {
            butType = false;
        }
        // YK : DR-378 
        URL = ApexPages.currentPage().getURL();
        if(URL.contains('techRequired'))
        {
        techRequired=integer.valueof(ApexPages.currentPage().getParameters().get('techRequired'));
        }    
        List<aggregateResult> results = [select contactid,count(id)Qty from asset where course__c=:courseID and primary__c=true and contactid != '' group by contactid];
        if(results.size()>0)
        {
            for(AggregateResult ar : results)
            {
            String conId=(String)ar.get('contactid');
            primcntmap=primcntmap+conId;
            }    
        } 
        
        getPIUContacts();
        getUsages();
        getStatus(); 
        getLMS();
         
     }

// to display contacts of PIU on VF page
    Public void getPIUContacts()
    {
    assetDetails= new Map<String,List<AssetsWrapper>>();   
    //Query modified by Pooja for DR-381
    for(Asset a:[Select id,name,Product2id,Product_Author__c,Product_edition__c,Product_ISBN__c,InstallDate  from Asset where Course__r.id =:courseID and Contact.id!='' and Status__c=:typePIU order by Name])
    {
        String productName_Author = '';
        if(a.Product_edition__c == null){ a.Product_edition__c = ''; }
        if(!a.Product_edition__c.contains('/e') && null != a.Product_edition__c && a.Product_edition__c != '' ){
            a.Product_edition__c= ','+' '+a.Product_edition__c+'/e';
        }
        if(a.Product_ISBN__c  == null)  { a.Product_ISBN__c ='';}
        If(a.Product_Author__c!=null)
         productName_Author=a.Product_Author__c+':'+' '+a.name+a.Product_edition__c+ ' <br> '+ a.Product_ISBN__c  ;
        Else
         productName_Author=a.name;

        If(a.Product2Id!=null){
        productIdWithName.put(a.Product2Id,productName_Author);
        }
    }
    set<ID> productIdSet = new set<ID>();
    productIdSet=productIdWithName.keySet();
    //Query modified by Pooja for DR-381
    for(Asset assetList1:[Select id,Product_Author__c,Product2Id,name,Contact.name,Usage__c, Status__c, Third_Party_LMS__c,Primary__c,InstallDate 
                          from Asset where course__c=:courseID and Product2Id IN: productIdSet and Contact.id!=null and Status__c=:typePIU order by Usage__c,Contact.name])
    {
     // YK : DR-378, Auto selection of 'Tech Required' value Logic. 
        if (techRequired == 1)
        assetList1.Usage__c = System.Label.Tech_Required;
        if(assetDetails.containsKey(assetList1.Product2Id))
        assetDetails.get(assetList1.Product2id).add(new AssetsWrapper(assetList1,false));
        
        else{
        List<AssetsWrapper> tempLst1 = new List<AssetsWrapper>();
        tempLst1.add(new AssetsWrapper(assetList1,false));
        assetDetails.put(assetList1.Product2id, tempLst1);
        }
     }
}

//Mass update Logic.
    public PageReference getAssetSelected(){
        if(selectedAssets != null && !selectedAssets.isEmpty())
        selectedAssets.clear();
        for (String key : assetDetails.keySet()) {
        
        List<AssetsWrapper> assetList = assetDetails.get(key);
        
            for(AssetsWrapper asswrapper: assetList){        
            if(asswrapper.selected == true || Test.isRunningTest())    
                selectedAssets.add(asswrapper.ass);   
            }
        }
        boolean flag = editAll();    
        if (flag){
            return new pagereference('/'+courseID);
        }    
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No record(s) selected to update')); //Added by RG (28/03/2016)
            return null;
        }    
    }

    //To get the SelectedAssets
    public List<Asset> getSelectedAssets(){
    //if(selectedAssets.size()>0) //commented by RG (28/03/2016)
    return selectedAssets;
    //else
    //return null;
    }

// Logic to Edit the assets
    public boolean editAll(){
        try{
            List<Asset> updateAsset = new List<Asset>();
            List<Asset> selAssets=getSelectedAssets();
            if(selAssets.size()>0 ){            
            for(Asset a:selAssets){    
               updateAsset.add(a);    
            }
            update updateAsset;
            }   
            return true;        
        }    
        Catch(Exception e){       
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info, 'Error: Please specify a Product in Use record to update by selecting a contact, or click Cancel to create Products in Use without updating any details.'));        
           return false;   
        }
    }
    
//fetches Asset usage picklist values
public List < SelectOption > getUsages() {

    Schema.DescribeFieldResult fieldResult = Asset.Usage__c.getDescribe();
    List <SelectOption> options = new List <SelectOption>();
    List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
    for (Schema.PicklistEntry f: ple) {
       // if((newAsset.Usage__c).equalsIgnoreCase(f.getValue()))
        options.add(new SelectOption(f.getLabel(), f.getValue()));
    }                    
    return options; 
} 

//fetches Asset Status picklist values
public List < SelectOption > getStatus() {
    List <SelectOption> options = new List <SelectOption>();
    Schema.DescribeFieldResult fieldResult = Asset.Status__c.getDescribe();
    List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
    for (Schema.PicklistEntry f: ple) {
        options.add(new SelectOption(f.getLabel(), f.getValue()));
    }                     
    return options;
}
        
 //fetches Third Party LMS picklist values       
public List < SelectOption > getLMS() {
    List <SelectOption> options = new List <SelectOption>();
    Schema.DescribeFieldResult fieldResult = Asset.Third_Party_LMS__c.getDescribe();
    List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
    for (Schema.PicklistEntry f: ple) {
        options.add(new SelectOption(f.getLabel(), f.getValue()));
    }
    return options;
}         
  
public void setmassupdateindicator() {
    
    if (prdNamechnge != null ){
        List<AssetsWrapper> assetOnChange = assetDetails.get(prdNamechnge);
        for(AssetsWrapper asswrapper: assetOnChange){
            asswrapper.massselected = smassindicator;
        }
        
    }
    
   //return null;  
}
  
public PageReference massUpdate()
{
    //hasChanged = false;
      if (prdNamechnge!= '' && prdNamechnge!= null){
        for (String key : assetDetails.keySet()) { 
            if( key == prdNamechnge){
                List<AssetsWrapper> assetListOnChange = assetDetails.get(key);
                    
                for(AssetsWrapper asswrapper: assetListOnChange){
                    
                    //if (asswrapper.massselected == true){
                    if(null!=usageChange && asswrapper.selected==true ){
                            asswrapper.ass.Usage__c=usageChange;
                           hasChanged = true;}
                    if(null!=statuschange && asswrapper.selected==true){
                            asswrapper.ass.Status__c= statuschange; 
                           hasChanged = true;}
                    if(null!=lmschange && asswrapper.selected==true){
                            asswrapper.ass.Third_Party_LMS__c= lmschange;
                           hasChanged = true;}
                    if(null!=primchange && asswrapper.selected==true){
                            asswrapper.ass.Primary__c = primchange;
                            hasChanged = true;}
                    if(null!=ValDate && asswrapper.selected==true){
                            asswrapper.ass.InstallDate = ValDate; 
                           hasChanged = true; }
                    
                }//end for loop
            }//end if productnamechange    
        }
      }
      usageChange = null;
      statuschange =null;
      lmschange=null;  
      valDate=null;
      //prdNamechnge=null;
      return null;
} 
   
  
public void massupdateContactList(){
        List<AssetsWrapper> massPrimchange = assetDetails.get(sproduct);
        Set<Id> sContactId=new Set<Id>();
        for(AssetsWrapper asswrapper: massPrimchange){
            sContactId.add(asswrapper.ass.Contact.id);
        }

        for (String key : assetDetails.keySet()) {  
             List<AssetsWrapper> assetListOnChange = assetDetails.get(key);    
                  for(AssetsWrapper asswrapper: assetListOnChange){ 
                  if(sContactId.contains(asswrapper.ass.Contact.id) && asswrapper.ass.Primary__c == true && asswrapper.ass.Product2id!=sproduct)   
                      asswrapper.ass.Primary__c = false; 
                  }
       }    
       sproduct='';
}  
   
// Wrapper class
public class AssetsWrapper{    
    public Asset ass{get; set;}    
    public Boolean selected {get; set;} 
    public Boolean massselected {get; set;} 
    
    
    public AssetsWrapper(Asset a,boolean primary)
    {
        ass = a;   
        massselected = false;
        if(primary!=null)
            selected =primary;
        else
            selected = false;
    }
    
}//end of Wrapper class definition
//SortPiu added For Sorting PIU - DR-639
Public class SortPiu{
Public asset a1 {get;set;}
Public String productName_Author{get;set;}


public SortPiu(asset a) {
a1=a;
 productName_Author = '';
        if(a.Product_edition__c == null){ a.Product_edition__c = ''; }
        if(!a.Product_edition__c.contains('/e') && null != a.Product_edition__c && a.Product_edition__c != '' ){
            a.Product_edition__c= ','+' '+a.Product_edition__c+'/e';
        }
        if(a.Product_ISBN__c  == null)  { a.Product_ISBN__c ='';}
        If(a.Product_Author__c!=null)
         productName_Author=a.Product_Author__c+':'+' '+a.name+a.Product_edition__c+ ' <br> '+ a.Product_ISBN__c  ;
        Else
         productName_Author=a.name;

        }
}
}