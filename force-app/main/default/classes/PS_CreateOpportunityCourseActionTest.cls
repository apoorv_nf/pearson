@istest(seealldata =true)
private class PS_CreateOpportunityCourseActionTest{
    
    static testmethod void constructorTest() {

            Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
            insert acc;
                
            Contact con = new Contact(FirstName='karthiik',LastName ='A.S',Phone='9999888898',Email='test2gh@gmail.com', AccountId = acc.Id);
            insert con;
            
            UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true,Fall_Enrollment__c=10);
            insert UnivCourse;
            
           Test.startTest();
            opportunity ld = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=system.today(),Business_Unit__c='ERPI');         
            insert ld;

            UniversityCourseContact__c uni = new UniversityCourseContact__c(UniversityCourse__c=UnivCourse.id,Contact__c = con.Id);
            insert uni;
                
            PageReference createOppty = new pagereference('apex/PS_CreateOpportunityCourseActionPage');
     
            id courseid=  UnivCourse.id;
            id ldid= ld.id;

            Test.setCurrentPage(createOppty);
            createOppty.getParameters().put('id', courseid);
            createOppty.getParameters().put('command', 'Opportunitycreate');
        
             ApexPages.StandardController sc = new ApexPages.standardController(UnivCourse);
             PS_CreateOpportunityCourseAction testnew1 = new PS_CreateOpportunityCourseAction();
                        
             testnew1.onLoad_CourseOpptyEdit();
  
             Test.stopTest();

    } 
    
    static testmethod void constructorTest2() {
        
        
        Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
        insert acc;
        
        Contact con = new Contact(FirstName='kartthik',LastName ='A.S',Phone='9999888898',Email='testl12@gmail.com', AccountId = acc.Id);
        insert con;
        
        UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true,Fall_Enrollment__c=10);
        insert UnivCourse;
        
        Test.startTest();
        
        opportunity ld = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=system.today(),Business_Unit__c='ERPI');         
        insert ld;

        UniversityCourseContact__c uni = new UniversityCourseContact__c(UniversityCourse__c=UnivCourse.id,Contact__c = con.Id);
        insert uni;
        
        PageReference createOppty = new pagereference('apex/PS_CreateOpportunityCourseActionPage');
        
        id courseid=  UnivCourse.id;
        id ldid= ld.id;

        Test.setCurrentPage(createOppty);
        createOppty.getParameters().put('id', courseid);
        createOppty.getParameters().put('command', 'CreateChildRecords');
        
        ApexPages.StandardController sc = new ApexPages.standardController(UnivCourse);
        PS_CreateOpportunityCourseAction testnew1 = new PS_CreateOpportunityCourseAction();
        
        testnew1.onLoad_CourseOpptyEdit();
        
        Test.stopTest();
        
    } 
    static testmethod void constructorTest3() {
        
        Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
        insert acc;
        
        Contact con = new Contact(FirstName='Priyaa',LastName ='T',Phone='9999888898',Email='test12l34@gmail.com', AccountId = acc.Id);
        insert con;
        
        UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true,Fall_Enrollment__c=10);
        insert UnivCourse;
        
        Test.startTest();
        
        opportunity ld = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=system.today(),Business_Unit__c='ERPI');         
        insert ld;
        
        UniversityCourseContact__c uni = new UniversityCourseContact__c(UniversityCourse__c=UnivCourse.id,Contact__c = con.Id);
        insert uni;
        
        PageReference createOppty = new pagereference('apex/PS_CreateOpportunityCourseActionPage');
        
        id courseid=  UnivCourse.id;
        id ldid= ld.id;

        Test.setCurrentPage(createOppty);
        createOppty.getParameters().put('opptyid', ldid);
        createOppty.getParameters().put('id', courseid);
        
        createOppty.getParameters().put('command', 'DeleteOpportunity');
        
        ApexPages.StandardController sc = new ApexPages.standardController(UnivCourse);
        PS_CreateOpportunityCourseAction testnew1 = new PS_CreateOpportunityCourseAction();
        
        testnew1.onLoad_CourseOpptyEdit();
        
        Test.stopTest();
        
    } 
    static testmethod void constructorTest4() {
      
      Test.startTest();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Pearson Sales User OneCRM'];
        User testUser = [SELECT id FROM User WHERE ProfileId = :p.id and Market__c = 'UK' and isActive = True Limit 1];
        
      System.runAs(testUser){
        PS_CreateOpportunityCourseAction testnew1 = new PS_CreateOpportunityCourseAction();
        testnew1.getUserDetails();
      }
      Test.stopTest();
  }   
    static testmethod void constructorTest5() {    
    
        Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
        insert acc;
        
        Contact con = new Contact(FirstName='karthiik',LastName ='A.S',Phone='9999888898',Email='test1l2@gmail.com', AccountId = acc.Id);
        insert con;
        
        UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true,Fall_Enrollment__c=10);
        insert UnivCourse;
        
        Test.startTest();           
        
        opportunity ld = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=system.today(),Business_Unit__c='ERPI');         
        insert ld;  
        
        UniversityCourseContact__c uni = new UniversityCourseContact__c(UniversityCourse__c=UnivCourse.id,Contact__c = con.Id);
        insert uni;
        
        id opptyid=  ld.id;
        
        PS_CreateOpportunityCourseAction testnew2 = new PS_CreateOpportunityCourseAction();
        testnew2.createOpportunityContact(opptyid);         
        Test.stopTest();
    }     
    
    static testmethod void constructorTest6() {
        Test.startTest();        
        List<User> listWithUser = new List<User>();    
        listWithUser = TestDataFactory.createUser([select Id from Profile where Name=:'System Administrator'].Id,1);
        if(listWithUser != null && !listWithUser.isEmpty()){
            listWithUser[0].market__c='UK';
            listWithUser[0].Business_Unit__c='ERPI';
            listWithUser[0].Line_of_Business__c='Schools';
            listWithUser[0].Group__c='Primary';
            insert listWithUser;
            System.runAs(listWithUser[0])
            {
                PS_CreateOpportunityCourseAction testnew1 = new PS_CreateOpportunityCourseAction();
                testnew1.getUserDetails();
            }
            Test.stopTest();
        }
    }
    static testmethod void constructorTest7() {
        Test.startTest();        
        List<User> listWithUser = new List<User>();    
        listWithUser = TestDataFactory.createUser([select Id from Profile where Name=:'System Administrator'].Id,1);
        if(listWithUser != null && !listWithUser.isEmpty()){
            listWithUser[0].market__c='AU';
            listWithUser[0].Business_Unit__c='ERPI';
            listWithUser[0].Line_of_Business__c='Schools';
            listWithUser[0].Group__c='Primary';
            insert listWithUser;
            System.runAs(listWithUser[0])
            {
                PS_CreateOpportunityCourseAction testnew1 = new PS_CreateOpportunityCourseAction();
                testnew1.getUserDetails();
            }
            Test.stopTest();
        }
    }
    static testmethod void constructorTestvalidate() {          
            
        
        List<User> listWithUser = new List<User>();    
        listWithUser = TestDataFactory.createUser([select Id from Profile where Name=:'System Administrator'].Id,1);
        if(listWithUser != null && !listWithUser.isEmpty()){
            listWithUser[0].market__c='UK';
            listWithUser[0].Business_Unit__c='ERPI';
            listWithUser[0].Line_of_Business__c='Schools';
            listWithUser[0].Group__c='Primary';
            insert listWithUser;
            System.runAs(listWithUser[0])
            {
         
             Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School');
            insert acc;
                
            Contact con = new Contact(FirstName='kartthik',LastName ='A.S',Phone='9999888898',Email='test2cv@gmail.com', AccountId = acc.Id);
            insert con;
        
         Test.startTest(); 
            UniversityCourse__c UnivCourse =new UniversityCourse__c(Name='testcourse',Account__c= acc.id,Catalog_Code__c='testcode',Course_Name__c='testcoursename',CurrencyIsoCode='GBP',Active__c=true,Fall_Enrollment__c=10);
            insert UnivCourse;                      
            opportunity ld = new opportunity(Name='Test', StageName='Needs Analysis', CloseDate=system.today(),Business_Unit__c='ERPI');         
            insert ld;

            UniversityCourseContact__c uni = new UniversityCourseContact__c(UniversityCourse__c=UnivCourse.id,Contact__c = con.Id);
            insert uni;  
            PageReference createOppty = new pagereference('apex/PS_CreateOpportunityCourseActionPage');
     
            id courseid=  UnivCourse.id;
            id ldid= ld.id;

            Test.setCurrentPage(createOppty);
            createOppty.getParameters().put('id', courseid);
            createOppty.getParameters().put('oppId', ld.id);
            createOppty.getParameters().put('command', 'Opportunitycreate');
        
           //  ApexPages.StandardController sc = new ApexPages.standardController(UnivCourse);
             PS_CreateOpportunityCourseAction testnew1 = new PS_CreateOpportunityCourseAction();
                        
             testnew1.onLoad_CourseOpptyEdit();
             
            }
            Test.stopTest();
        }
    }
}