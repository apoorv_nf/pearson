@isTest
public class CourseActiveInactiveAssetExtnTest {
    public static List<User> listWithUser;
    public static Asset asst;
    public static void userCreation()
    {
       listWithUser = new List<User>();
         listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
         Map<Id,Asset> newMapAssets;
         Map<Id,Asset> oldMapAssets;
         Id astId;
         List<Asset> astlist = new List<Asset>();
         List<Asset> upastlist = new List<Asset>();
         if(listWithUser.size() > 0)
         {
            for(User newUser : listWithUser)
            {
                newUser.Product_Business_Unit__c = 'CTIPIHE';
            }
         } 
         Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=listWithUser[0].id,Disable_Triggers__c=true);
         insert byp;

    }

     static testMethod void testSortData()
    {
         userCreation();
         System.runAs(listWithUser[0]){
        
       UniversityCourse__c tcou = dataCreation();
        //testing sort funtionality
        PageReference testPage = Page.CourseActiveInactiveAsset;
        testPage.getParameters().put('CourseId',tcou.id);
        testPage.getParameters().put('Type','Active');
        Test.setCurrentPageReference(testPage);
        Apexpages.StandardController stdController = new Apexpages.StandardController(tcou);
        CourseActiveInactiveAssetExtn coursetest = new CourseActiveInactiveAssetExtn(stdController);
        coursetest.ViewData();
        //CourseActiveInactiveAssetExtn.dynamicAddFilterSearch testfil = new CourseActiveInactiveAssetExtn.dynamicAddFilterSearch(null,null,'Name','ASC');
        coursetest.addNewFilter();
        coursetest.addNewFilter();
        List<CourseActiveInactiveAssetExtn.dynamicAddFilterSearch> srtList = coursetest.listWithSelectOptions;
        CourseActiveInactiveAssetExtn.dynamicAddFilterSearch sfilter = srtList.get(0);
        sfilter.selField = 'name';
        sfilter.serField = 'ASC';
             
        CourseActiveInactiveAssetExtn.dynamicAddFilterSearch sfilter1 = srtList.get(1);
        sfilter1.selField = 'product_author__c';
        sfilter1.serField = 'ASC';
             
        CourseActiveInactiveAssetExtn.dynamicAddFilterSearch sfilter2 = srtList.get(2);
        sfilter2.selField = 'installdate';
        sfilter2.serField = 'DESC';
        //coursetest.listWithSelectOptions.add(testfil);
        List<CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1> tempList = coursetest.listWithSelectOptions1;
        
        
        CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1 filter2 = tempList.get(0);
        filter2.searchText = '';
        filter2.selectedProductFilterValue = '--None--';
        //filter2.selectedCondition = '';
             
        coursetest.sortData();
        testPage.getParameters().put('Type','Inactive');
        coursetest.sortData();
        coursetest.votId = asst.Id;
        coursetest.delVote();
        
        coursetest.rowToRemove = 1;
        coursetest.listWithSelectOptions = srtList;
        coursetest.removeFilter();
         }
    }
    public static UniversityCourse__c dataCreation()
    {
        
        UniversityCourse__c tcou;
        System.runAs(listWithUser[0]){
         Account tact = new Account();
        tact.Name = 'Test Account';
        tact.Geography__c = 'Core';
        tact.Line_of_Business__c = 'Schools';
        insert tact;
        tcou = new UniversityCourse__c();
        tcou.Name = 'Test Course';
        tcou.Catalog_Code__c = 'Test Catalog';
        tcou.Account__c = tact.Id;
        tcou.Course_Name__c = 'Test Course';
        insert tcou;
        Product2 pr1 = new Product2(Name = 'Bundle Product', IsActive = true, Configuration_Type__c = 'Bundle',Qualification_Name__c = 'Test Bundle',Campus__c='Durbanville',Qualification_Level_Name__c= 2,Business_Unit__c = 'CTIPIHE',Market__c = listWithUser[0].Market__c,Line_of_Business__c='Higher Ed');
        insert pr1;
        asst = new Asset();
        asst.Name = 'Test1';
        asst.Status__c = 'Active';
        asst.Product2Id = pr1.Id;
        asst.AccountId = tact.Id;
        asst.Course__c = tcou.Id; 
        insert asst;
        Asset asst1 = new Asset();
        asst1.Name = 'Test2';
        asst1.Status__c = 'Active';
        asst1.Product2Id = pr1.Id;
        asst1.AccountId = tact.Id;
        asst1.Course__c = tcou.Id;
        insert asst1;
        Asset asst2 = new Asset();
        asst2.Name = 'Test3';
        asst2.Status__c = 'Inactive';
        asst2.Product2Id = pr1.Id;
        asst2.AccountId = tact.Id;
        asst2.Course__c = tcou.Id;
        insert asst2;
        Asset asst3 = new Asset();
        asst3.Name = 'Test4';
        asst3.Status__c = 'Inactive';
        asst3.Product2Id = pr1.Id;
        asst3.AccountId = tact.Id;
        asst3.Course__c = tcou.Id;
        insert asst3;
       
        }
         return tcou;
    }
        static testMethod void testSearchData()
    {
        userCreation();
        UniversityCourse__c tcou = dataCreation();
         System.runAs(listWithUser[0]){

        PageReference testPage = Page.CourseActiveInactiveAsset;
        Test.setCurrentPageReference(testPage);
        testPage.getParameters().put('CourseId',tcou.id);
        testPage.getParameters().put('Type','Active');
        Apexpages.StandardController stdController = new Apexpages.StandardController(tcou);
        CourseActiveInactiveAssetExtn coursetest = new CourseActiveInactiveAssetExtn(stdController);
        coursetest.addNewFilter1();
        
       
        List<CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1> tempList = coursetest.listWithSelectOptions1;
        CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1 filter = tempList.get(0);
        filter.searchText = 'True';
        filter.selectedProductFilterValue = 'Rollover2__c';
        filter.selectedCondition = '=';
                
        CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1 filter1 = tempList.get(1);
        filter1.searchText = 'Active';
        filter1.selectedProductFilterValue = 'status__c';
        filter1.selectedCondition = '=';
        
        
        coursetest.searchResults();
        coursetest.rowToRemove1 = 1;
        coursetest.listWithSelectOptions1 = tempList;
        coursetest.removeFilter1();
         }}
    
     static testMethod void testSearchData1()
    {
        userCreation();
        UniversityCourse__c tcou = dataCreation();
         System.runAs(listWithUser[0]){

        PageReference testPage = Page.CourseActiveInactiveAsset;
        Test.setCurrentPageReference(testPage);
        testPage.getParameters().put('CourseId',tcou.id);
        testPage.getParameters().put('Type','Active');
        Apexpages.StandardController stdController = new Apexpages.StandardController(tcou);
        CourseActiveInactiveAssetExtn coursetest = new CourseActiveInactiveAssetExtn(stdController);
        coursetest.addNewFilter1();
        //coursetest.addNewFilter1();
       
        List<CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1> tempList = coursetest.listWithSelectOptions1;
        
        
        CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1 filter2 = tempList.get(0);
        filter2.searchText = '23-04-2017';
        filter2.selectedProductFilterValue = 'installdate';
        filter2.selectedCondition = '!=';
        
        
        CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1 filter3 = tempList.get(1);
        filter3.searchText = 'Test';
        filter3.selectedProductFilterValue = 'Name';
        filter3.selectedCondition = 'like %';
        
     
        
        coursetest.searchResults();
        coursetest.rowToRemove1 = 1;
        coursetest.listWithSelectOptions1 = tempList;
        coursetest.removeFilter1();
         }}
    static testMethod void testSearchData2()
    {
        userCreation();
        UniversityCourse__c tcou = dataCreation();
         System.runAs(listWithUser[0]){

        PageReference testPage = Page.CourseActiveInactiveAsset;
        Test.setCurrentPageReference(testPage);
        testPage.getParameters().put('CourseId',tcou.id);
        testPage.getParameters().put('Type','Active');
        Apexpages.StandardController stdController = new Apexpages.StandardController(tcou);
        CourseActiveInactiveAssetExtn coursetest = new CourseActiveInactiveAssetExtn(stdController);
        coursetest.addNewFilter1();
        //coursetest.addNewFilter1();
       
        List<CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1> tempList = coursetest.listWithSelectOptions1;
        
        
       
        
        CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1 filter4 = tempList.get(0);
        filter4.searchText = 'Test';
        filter4.selectedProductFilterValue = 'Name';
        filter4.selectedCondition = 'like %%';
        
        CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1 filter5 = tempList.get(1);
        filter5.searchText = 'Test';
        filter5.selectedProductFilterValue = 'Name';
        filter5.selectedCondition = 'like';
        
        coursetest.searchResults();
     
         }}
     static testMethod void testSearchData3()
    {
        userCreation();
        UniversityCourse__c tcou = dataCreation();
         System.runAs(listWithUser[0]){

        PageReference testPage = Page.CourseActiveInactiveAsset;
        Test.setCurrentPageReference(testPage);
        testPage.getParameters().put('CourseId',tcou.id);
        testPage.getParameters().put('Type','Active');
        Apexpages.StandardController stdController = new Apexpages.StandardController(tcou);
        CourseActiveInactiveAssetExtn coursetest = new CourseActiveInactiveAssetExtn(stdController);
        coursetest.addNewFilter1();
       // coursetest.addNewFilter1();
       
        List<CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1> tempList = coursetest.listWithSelectOptions1;
        
        
       
        
        CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1 filter4 = tempList.get(0);
        filter4.searchText = '23-04-2017';
        filter4.selectedProductFilterValue = 'installdate';
        filter4.selectedCondition = '>';
        
        CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1 filter5 = tempList.get(1);
        filter5.searchText = '23-04-2017';
        filter5.selectedProductFilterValue = 'installdate'; 
        filter5.selectedCondition = '<';
        
        coursetest.searchResults();
       
         }}
     static testmethod void testchangeSerachTextType(){
    PageReference testPage = Page.CourseActiveInactiveAsset; 
    userCreation();
    UniversityCourse__c tcou = dataCreation();
    testPage.getParameters().put('selectedRow', '0');
    testPage.getParameters().put('CourseId', tcou.id);     
    testPage.getParameters().put('type', 'Active'); //Page requires an object id (Opportunity or University Course)
    Test.setCurrentPageReference(testPage);
    
    Apexpages.StandardController stdController = new Apexpages.StandardController(tcou);
    CourseActiveInactiveAssetExtn lookupCtrl = new CourseActiveInactiveAssetExtn(stdController);
    lookupCtrl.addNewFilter1();
    lookupCtrl.addNewFilter1();
    List<CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1> tempList = lookupCtrl.listWithSelectOptions1;
    CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1 filter = tempList.get(0);
    filter.searchText = 'Test';
    filter.selectedProductFilterValue = 'name';
    //filter.selectedCondition = 'like %';
    
    CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1 filter1 = tempList.get(1);
    filter1.searchText = '23-04-2017';
    filter1.selectedProductFilterValue = 'installdate';
    //filter1.selectedCondition = '<';
    
    CourseActiveInactiveAssetExtn.dynamicAddFilterSearch1 filter2 = tempList.get(2);
    filter2.searchText = 'true';
    filter2.selectedProductFilterValue = 'status__c';
    //filter2.selectedCondition = '=';
    lookupCtrl.listWithSelectOptions1 = tempList;
    lookupCtrl.changeSerachTextType();
    
    testPage.getParameters().put('selectedRow', '1'); //Page requires an object id (Opportunity or University Course)
    Test.setCurrentPageReference(testPage);
    lookupCtrl.changeSerachTextType();
    
    testPage.getParameters().put('selectedRow', '2'); //Page requires an object id (Opportunity or University Course)
    Test.setCurrentPageReference(testPage);
    lookupCtrl.changeSerachTextType();
     
        
    }
     static testmethod void testgetStatuses(){
    PageReference testPage = Page.CourseActiveInactiveAsset; 
    userCreation();
    UniversityCourse__c tcou = dataCreation();
    testPage.getParameters().put('fieldName', 'installdate'); //Page requires an object id (Opportunity or University Course)
    testPage.getParameters().put('CourseId', tcou.id);     
    testPage.getParameters().put('type', 'Active'); 
    Test.setCurrentPageReference(testPage);
    Apexpages.StandardController stdController = new Apexpages.StandardController(tcou);
    CourseActiveInactiveAssetExtn lookupCtrl = new CourseActiveInactiveAssetExtn(stdController);
    Test.startTest();
    lookupCtrl.getStatuses();
    Test.stopTest();
    testPage.getParameters().put('fieldName', 'status__c'); //Page requires an object id (Opportunity or University Course)
    Test.setCurrentPageReference(testPage);
    lookupCtrl.getStatuses();   
        
        
    }
}