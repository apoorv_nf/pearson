/************************************************************************************************************
* Apex Interface Name : PS_OrderItemValidationB2BSampling
* Version             : 1.0 
* Created Date        : 13 Jul 2015
* Function            : interfaces that hold the logic for validate basic order Item operation
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Leonard Victor             13/Jul/2015            Initial version
* Rony Joseph                03/Dec/2015            Updated Error Handling 
* Rony Joseph                29/dec/2015            Fix for permission issues
* Rony Joseph                18/Jan/2016            Added validation for shipping method field.
* Rony Joseph                11/Mar/2016            Defect fix for D-4460
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

public class PS_OrderItemValidationB2BSampling implements PS_OrderItemValidationInterface{
    
    public static boolean comingfromui;
    private Map<String,OrderItem> inOrderItems;
    private Map<Id, OrderItem> oldOrderItems;
    private User contextUser;
    
    public static Map<Id, Order> orderMap;
    
    /*************************************************************************************************************
* @Name        : initialize
* @Description : used to initiliaze the class
* 
* @Todo        : 
* @Input       : inOrderItems: list of order to be validated
inOldOrderItem: old version of the order 
userContext: user as context for the validation process    
* @Output      : N/A
*************************************************************************************************************/
    public void initialize(Map<String,OrderItem> inOrderItem, Map<Id, OrderItem> oldOrderItem, User contextUser)
    {
        this.inOrderItems = inOrderItem;
        this.oldOrderItems = oldOrderItem;
        this.contextUser = contextUser;
        
        getOrderInfo(this.inOrderItems.values());
    }
    
    /*************************************************************************************************************
* @Name        : validateInsert
* @Description : determines if the order can be inserted
* 
* @Todo        : 
* @Input       : exceptions: instance of map that will receive the exeption. The error should not be added 
to the order item to allow this interface to be used outside the tigger context.
* @Output      : true if there is no error, false if there is error.
*************************************************************************************************************/
    public Boolean validateInsert(Map<String,List<String>> exceptions)
    {
        
        
        Map<String,OrderItem> inOrderItemsToProces = new Map<String,OrderItem>();
        
        
        
        for(String ordItemKey : inOrderItems.keySet()){
            
            OrderItem ord = inOrderItems.get(ordItemKey);
            inOrderItemsToProces.put(ordItemKey,ord);
        }
        
        if(inOrderItemsToProces.isEmpty()){
            
            return true;
            
        }
        
        Boolean userHasPermission = (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Sample Order Approver') || 
                                     PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Backend Order Creation'));
        
        System.debug('User Bypass:' + userHasPermission);
        
        //validation for other user that don't have te permission
        for(String ordKey : inOrderItemsToProces.keySet()){
            
            Order itemOrder = orderMap.get(inOrderItemsToProces.get(ordKey).OrderId);
            if( itemOrder!= null)  
            { 
                if(!userHasPermission && itemOrder.Status != 'New' && ! itemOrder.isTemporary__c){
                    inOrderItemsToProces.get(ordKey).unitPrice.adderror(Label.PS_Order_Item_Create);
                }
            }  
        }
        
        return exceptions.isEmpty();
    }
    
    
    /*************************************************************************************************************
* @Name        : validateUpdate
* @Description : determines if the order can be updated
* 
* @Todo        : 
* @Input       : exceptions: instance of map that will receive the exeption. The error should not be added 
to the order item to allow this interface to be used outside the tigger context.
* @Output      : true if there is no error, false if there is error.
*************************************************************************************************************/
    
    public Boolean validateUpdate(Map<String,List<String>> exceptions)
    {
        try{
            //filter order with status 
            
            comingfromui = true; 
            
            Map<String,OrderItem> inOrderItemsToProces = new Map<String,OrderItem>();
            
            for(String ordItemKey : inOrderItems.keySet())
            {
                OrderItem ord = inOrderItems.get(ordItemKey);
                
                
                inOrderItemsToProces.put(ordItemKey,ord);
            }
            
            if(inOrderItemsToProces.isEmpty()){
                
                return true;
                
            }
            
            Boolean userHasPermission = (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Sample Order Approver') || 
                                         PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Backend Order Creation')|| 
                                         PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Manage Orders'));
            Boolean userHasPermissionforStatuschange = (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Front-End Sampling')); 
            Boolean userHasPermissionforBackendOrderCreation = (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Backend Order Creation'));
            Boolean userHasPermissionforApproval = (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Sample Order Approver')); 
            Boolean userhasStatusBypassPermisssion   =  (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Manage Orders'));              
            
            
            System.debug('User Bypass:' + userHasPermission);
            
            if(!Test.isRunningTest()){
                for(String ordKey : inOrderItemsToProces.keySet()){ 
                    Order itemOrder = orderMap.get(inOrderItemsToProces.get(ordKey).OrderId);
                    
                    /* GH - If the old value of Status is either Open or Cancelled,
revoke access for all except 'Pearson Manage Orders'

String oldItemStatus = oldOrderItems.get(inOrderItems.get(ordKey)).Status__c;
if(oldItemStatus == 'Open' || oldItemStatus == 'Cancelled') {
userHasPermission = FALSE;
userHasPermissionforApproval = FALSE;
userHasPermissionforBackendOrderCreation = FALSE;
userHasPermissionforStatuschange = FALSE;
}

*/ 
                    
                    List<String> errors = validateUpdateSingleOrder(inOrderItems.get(ordKey), oldOrderItems.get(inOrderItems.get(ordKey).Id), itemOrder, userHasPermission);
                    String sProductStatusErr = validateOrderProductStatus(inOrderItems.get(ordKey), oldOrderItems.get(inOrderItems.get(ordKey).Id),userHasPermissionforStatuschange,userHasPermissionforApproval,userhasStatusBypassPermisssion,userHasPermissionforBackendOrderCreation );
                    system.debug('$$$$$$4Error'+errors) ; 
                    if(sProductStatusErr!='')errors.add(sProductStatusErr);
                    if(! errors.isEmpty()){
                        
                        exceptions.put(ordKey,errors);
                    }
                }
            }
            
            return exceptions.isEmpty();
        }catch(exception e){
            string errormessage=e.getMessage()+e.getStackTraceString();
            ExceptionFramework.LogException('OrderItem Update','PS_OrderItemValidationB2BSampling','validateUpdate',errormessage,UserInfo.getUserName(),'');           
            return null;
        }     
    }
    
    private String validateOrderProductStatus(OrderItem updatedOrderItem, OrderItem oldOrderItem,Boolean userHasPermissionforStatuschange, Boolean userHasPermissionforApproval, Boolean userhasStatusBypassPermisssion,Boolean userHasPermissionforBackendOrderCreation ){
        try{
            String statuserrmsg=''; 
            Boolean userHasNetworkSampler = (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Network Sampler')); 

            system.debug('userHasPermissionforStatuschange:'+userHasPermissionforStatuschange+' userHasPermissionforApproval:'+userHasPermissionforApproval+' userhasStatusBypassPermisssion:'+userhasStatusBypassPermisssion);  
            system.debug('Entered'+oldOrderItem.Status__c+'2'+updatedOrderItem.Status__c);                                            
            if(userhasStatusBypassPermisssion)
                return statuserrmsg;
            else if (userHasPermissionforApproval){
                system.debug('EnteredAP'+oldOrderItem.Status__c+'2'+updatedOrderItem.Status__c)  ; 
                if((updatedOrderItem.Status__c != oldOrderItem.Status__c) &&((oldOrderItem.Status__c != 'Requires Approval') ||((updatedOrderItem.Status__c != 'Approved') && (updatedOrderItem.Status__c != 'Cancelled'))))
                    if(!userHasPermissionforStatuschange){statuserrmsg=Label.PS_OrderItem_ApprovalError_Message;}
            } 
            else if (userHasPermissionforBackendOrderCreation){
                system.debug('EnteredFE'+oldOrderItem.Status__c+'2'+updatedOrderItem.Status__c)  ; 
                if ((updatedOrderItem.Status__c != oldOrderItem.Status__c) &&(((oldOrderItem.Status__c != 'Backordered')||(updatedOrderItem.Status__c != 'Cancelled'))&&(((oldOrderItem.Status__c != 'On Hold')||  
                                                                                                                                                                          (updatedOrderItem.Status__c != 'Resend Request' && updatedOrderItem.Status__c != 'Cancelled')))&&((oldOrderItem.Status__c != null) &&(updatedOrderItem.Status__c != 'Entered'))))                
                    statuserrmsg=Label.PS_OrderItem_StatusChangeError_Message; 
            }
            else if (userHasPermissionforStatuschange){
                system.debug('EnteredFE'+oldOrderItem.Status__c+'2'+updatedOrderItem.Status__c)  ; 
                if ((updatedOrderItem.Status__c != oldOrderItem.Status__c) &&(((oldOrderItem.Status__c != 'Backordered')||(updatedOrderItem.Status__c != 'Cancelled'))&&(((oldOrderItem.Status__c != 'On Hold')||  
                                                                                                                                                                          (updatedOrderItem.Status__c != 'Resend Request' && updatedOrderItem.Status__c != 'Cancelled')))&&((oldOrderItem.Status__c != null) &&(updatedOrderItem.Status__c != 'Entered'))))                
                    statuserrmsg=Label.PS_OrderItem_StatusChangeError_Message; 
            }   
            else
                if(!userHasNetworkSampler){statuserrmsg=Label.PS_OrderItem_Status;}
            return statuserrmsg;    
        }catch(exception e){
            string errormessage=e.getMessage()+e.getStackTraceString();
            ExceptionFramework.LogException('OrderItem Update','PS_OrderItemValidationB2BSampling','validateOrderProductStatus',errormessage,UserInfo.getUserName(),'');           
            return null;
        } 
    }
    
    private List<String> validateUpdateSingleOrder(OrderItem updatedOrderItem, OrderItem oldOrderItem, Order itemOrder, Boolean userBypass)
    {
        try{
            List<String> errors =  new List<String>();
            
            
            if(userBypass || itemOrder.Status == 'New' || itemOrder.isTemporary__c)
            {
                return errors;
            }
            
            
            /*if(updatedOrderItem.Target_System__c == 'UOPS'&& updatedOrderItem.ERP_Order_Number__c == null && updatedOrderItem.Status__c != oldOrderItem.Status__c )
{
errors.add('You can only change the status of an UOPS items after the integration is completed and ERP order number is returned!');

}*/
            
            
            /* if(updatedOrderItem.Status__c != oldOrderItem.Status__c) 
{errors.add(Label.PS_OrderItem_Status);}*/
            
            
            if(updatedOrderItem.Quantity != oldOrderItem.Quantity)
            {
                errors.add(Label.PS_OrderItem_Quantity);
                
            }
            
            if(updatedOrderItem.UnitPrice != oldOrderItem.UnitPrice)
            {
                errors.add(Label.PS_OrderItem_UnitPrice);
                
            }
            
            if(updatedOrderItem.StatusReason__c != oldOrderItem.StatusReason__c)
            {
                errors.add(Label.PS_OrderItem_StatusReason);
            }
            
            if(updatedOrderItem.Shipping_Method__c == 'Overnight')
            {
                errors.add('You do not have the permission to select that value for Shipping Method');
            }
            
            
            
            if(!userBypass && errors.isEmpty() && updatedOrderItem.id==null){
                errors.add(Label.PS_Order_Item_Create);
            }   
            
            
            system.debug('Errror###'+errors);
            return errors;
        }catch(exception e){
            string errormessage=e.getMessage()+e.getStackTraceString();
            ExceptionFramework.LogException('OrderItem Update','PS_OrderItemValidationB2BSampling','validateUpdateSingleOrder',errormessage,UserInfo.getUserName(),'');           
            return null;
        } 
    }
    
    
    /*************************************************************************************************************
* @Name        : validateDelete
* @Description : determines if the order can be deleted
* 
* @Todo        : 
* @Input       : exceptions: instance of map that will receive the exeption. The error should not be added 
to the order item to allow this interface to be used outside the tigger context.
* @Output      : true if there is no error, false if there is error.
*************************************************************************************************************/
    
    public Boolean validateDelete(Map<String,List<String>> exceptions)
    {
        Map<String,OrderItem> inOrderItemsToProces = new Map<String,OrderItem>();
        
        for(String ordItemKey : oldOrderItems.keySet())
        {
            OrderItem ord = oldOrderItems.get(ordItemKey);
            
            
            inOrderItemsToProces.put(ordItemKey,ord);
        }
        
        if(inOrderItemsToProces.isEmpty()){
            
            return true;
            
        }
        
        Boolean userHasPermission = (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Sample Order Approver') || 
                                     PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Backend Order Creation'));
        Boolean userHasSampleordpermission = (PS_Util.hasUserPermissionSet(contextUser.Id, 'Pearson Sample Order Approver'));                             
        
        System.debug('User Bypass:' + userHasPermission);
        
        //validation for other user that don't have te permission
        for(String ordKey : oldOrderItems.keySet()){
            
            List<String> errors = new List<String>();
            
            Order itemOrder = orderMap.get(inOrderItemsToProces.get(ordKey).OrderId);  
            
            if(! userHasPermission && ! itemOrder.isTemporary__c)
            {
                errors.add(Label.PS_OrderItem_Delete);
            }
            
            if(userHasSampleordpermission && itemOrder.status=='Open')//Defect fix for D-4460
            {
                errors.add('You cannot delete order products once associated order is Approved.');
            }
            
            if(! errors.isEmpty()){
                
                exceptions.put(ordKey,errors);
            }
        }
        
        
        return exceptions.isEmpty();
    }
    
    private void getOrderInfo(List<OrderItem> items)
    {
        if(orderMap == null)
        {
            orderMap = new Map<Id, Order>();
        }
        
        Set<Id> orderIdsToQuery = new Set<Id>();
        
        for(OrderItem item : items)
        {
            if( ! orderMap.containsKey(Item.OrderId))
            {
                orderIdsToQuery.add(Item.OrderId);
            }
        }
        
        List<Order> orders = new  List<Order>();
        
        if( ! orderIdsToQuery.isEmpty())
        {
            orders = [SELECT Id, Status, isTemporary__c FROM Order WHERE Id in :orderIdsToQuery];
        }
        
        if( ! orders.isEmpty())
        {
            
            orderMap.putAll(new Map<Id, Order>(orders));
        }
        
    }
    
    
    
}