global class ProcessIntegrationRequestFailedZABatch implements Database.Batchable<SObject>,Database.Stateful,Schedulable
{

    global Database.QueryLocator start(Database.BatchableContext BC){
        DateTime dt = System.now()-1;
        String query = 'select id, Status__c, Error_Code__c, Error_Description__c, Object_Id__c, Market__c from Integration_Request__c WHERE LastModifiedDate >=:dt and Object_Id__c Like \'006%\' and Market__c =\'ZA\' AND Status__c IN (\'Functional Error\', \'Technical Error\')';
        System.debug('ProcessIntegrationRequestFailedZABatch : start : Query '+query);
        return Database.getQueryLocator(query);
     
    }
    
     global void execute(Database.BatchableContext BC, List<SObject> scope){
         
         Set<ID> intReqsID = new Set<ID>();
         List<Integration_Request__c> intReqsList = new List<Integration_Request__c>();
         try{ 
         for(Sobject s : Scope){
             
             Integration_Request__c intReq = (Integration_Request__c)s;
             System.debug('Status'+intReq.status__c);
             intReqsID.add(intReq.id);
             intReq.Status__c = 'Ready for Submission';
             intReq.Error_Code__c = '';
             intReq.Error_Description__c = '';
             intReqsList.add(intReq);
         }
         System.debug('ProcessIntegrationRequestFailedZABatch : execute : inteReqsList : '+intReqsList.size());
         System.debug('ProcessIntegrationRequestFailedZABatch : execute : intReqsID : '+intReqsID.size());
             
        // List<System_Response__c> sysResponseList = [Select id from System_Response__c where Integration_Request__c IN: intReqsID];
             
        //System.debug('ProcessIntegrationRequestFailedZABatch : execute : sysResponseList : '+sysResponseList.size());
        //if(!sysResponseList.isEmpty())
         //    Delete sysResponseList;
        if(!intReqsList.isEmpty())     
             update intReqsList;
         }
         catch(Exception e){
             System.debug('Exception occured while updating Integration Request Records'+e.getMessage());
         }
     }
    global void finish(Database.BatchableContext BC)
    {
        
    }//end batch finish
    
    global void execute(SchedulableContext sc) {
       database.executebatch(new ProcessIntegrationRequestFailedZABatch());
    }
    
}