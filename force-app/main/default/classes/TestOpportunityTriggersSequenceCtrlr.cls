@isTest
private class TestOpportunityTriggersSequenceCtrlr{
    static testMethod void myUnitTest()
    {

        TestClassAutomation.FillAllFields = true;
        
        Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
        sAccount.BillingCountry         = 'United Kingdom';
        sAccount.BillingCountryCode     = 'GB';
        sAccount.BillingState           = 'England';
       // sAccount.BillingCountryCode     = 'GB';
        sAccount.BillingStateCode       = 'GBE';
        sAccount.ShippingCountry        = 'United Kingdom';
        sAccount.ShippingState          = 'England';
        sAccount.ShippingStateCode      = 'GBE';
        sAccount.ShippingCountryCode    = 'GB';
        sAccount.PS_AccountSegment1__c  = null;
        sAccount.Payment__c             = null;
        sAccount.Qualification_Level__c = null;
        sAccount.Lead_Sponsor_Type__c   = null;
        
        System.debug('sAccount----------->'+sAccount);
        
        insert sAccount;
        
        ActivityTemplate__c sAT1        = new ActivityTemplate__c();
        sAT1.Related_To__c              = 'Opportunity';
        sAT1.Status__c                  = 'On Schedule';
        sAT1.Stage__c                   = 'Prospecting';
        sAT1.Subject__c                 = 'Test opportunity activity';
        sAT1.Description__c             = 'Test opportunity activity';
        sAT1.Active__c                  = true;
        sAT1.NPS_Rating_Required__c     = true;
        sAT1.Days_Until_Due__c          = 10;
        sAT1.Account_Location__c        = 'Australia';
        insert sAT1;
        
        ActivityTemplate__c sAT2        = new ActivityTemplate__c();
        sAT2.Related_To__c              = 'Opportunity';
        sAT2.Status__c                  = 'On Schedule';
        sAT2.Stage__c                   = 'Invoiced';
        sAT2.Subject__c                 = 'Test opportunity activity 2';
        sAT2.Description__c             = 'Test opportunity activity 2';
        sAT2.Active__c                  = true;
        sAT2.NPS_Rating_Required__c     = true;
        sAT2.Days_Until_Due__c          = 10;
        sAT2.Account_Location__c        = 'Australia';
        insert sAT2;
        
        Test.startTest();
    
        Opportunity sOpportunity        = (Opportunity)TestClassAutomation.createSObject('Opportunity');
        sOpportunity.AccountId          = sAccount.Id;
        sOpportunity.StageName          = sAT1.Stage__c;
        sOpportunity.RecordTypeId       = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Global Opportunity').getRecordTypeId();
        sOpportunity.Cadence__c         = '';
        sOpportunity.priority__c         = '';
        sOpportunity.Status__c          = sAT1.Status__c;
        insert sOpportunity;
        checkRecurssionAfter.run = true; 
        
        checkRecurssionBefore.run = true;
        sOpportunity.StageName                  = 'Invoiced';
        sOpportunity.Renewal_Date__c            = system.today().addmonths(8);
        sOpportunity.Re_engagement_Date__c      = system.today().addmonths(1);
        update sOpportunity;
        
        
       
    
        Test.stopTest();
    }   
}