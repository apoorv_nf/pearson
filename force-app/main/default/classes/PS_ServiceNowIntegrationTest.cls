/************************************************************************************************************
* Apex Interface Name : PS_ServiceNowIntegrationTest
* Version             : 1.1 
* Created Date        : 1/2/2016 2:44 
* Modification Log    :   
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Rashmi Prasad         30/1/2016             
* Sakshi Agarwal        3/4/16 (Negative Scenario)  
* Rashmi Prasad         3/8/2016 Updated Method testPS_ServiceNowIntegration to add TS case
* Raushan Kumar         6/28/2016 updatd for test class failure 
* Christwin Durai       5/26/2017 Updated from State & National to School Assessment 
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class PS_ServiceNowIntegrationTest{
    static testMethod void testPS_ServiceNowIntegration(){

        List<Account> lstAccount = TestDataFactory.createAccount(3,'Organisation');
        insert lstAccount ;

        List<Contact> lstContact = TestDataFactory.createContacts(3);
        for(Integer i=0;i<lstContact.size();i++){
            lstContact[i].Accountid=lstAccount[0].id;
            lstContact[i].MailingState='England';
            lstContact[i].MailingCity = 'Bangalore';
            lstContact[i].MailingPostalCode= '560037';
            lstContact[i].MailingStreet= 'BNG';
        }
        insert lstContact;
        // 26th May 2017-Christwin-SA Project , Changed the State National to School Assessment
        Id SNRecordType = Schema.SObjectType.case.getRecordTypeInfosByName().get('School Assessment').getRecordTypeId();
        // 26th May 2017-Christwin-SA Project , Changed the State National to School Assessment
        Id TSRecordType = Schema.SObjectType.case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
        
        string caseStatus = Label.PS_Case_onholdstatus; 
        
        //Test.StartTest();
        /**For 'State & National' Cases**/
        List<case> casesOld = TestDataFactory.createCase(2,'School Assessment');
        casesOld[0].Account=lstAccount[0];
        casesOld[0].Contact=lstContact[0]; 
        casesOld[0].Escalation_External_Status__c='New';
        casesOld[0].Status = caseStatus;
        casesOld[0].PS_Business_Vertical__c = 'Higher Education';
        casesOld[1].RecordTypeid = TSRecordType;
        casesOld[1].Account=lstAccount[0];
        casesOld[1].Contact=lstContact[0]; 
        casesOld[1].Escalation_External_Status__c='New';
        casesOld[1].Status = caseStatus;
        casesOld[1].PS_Business_Vertical__c = 'Higher Education';
        Test.StartTest();
        insert casesOld;
        
        Map<Id,case> caseOldMap = new Map<Id,case>();
        caseOldMap.put(casesOld[0].Id, casesOld[0]);
        caseOldMap.put(casesOld[1].Id, casesOld[1]); 
       
        List<case> casesNew = new List<case>();
        casesNew.add(casesOld[0]);
        casesNew[0].Escalation_External_Status__c='Closed';
        casesNew.add(casesOld[1]);
        casesNew[1].Escalation_External_Status__c='Closed';
        Update casesNew;
        
        Map<Id,case> caseNewMap = new Map<Id,case>();
        caseNewMap.put(casesNew[0].Id,casesNew[0]);
        caseNewMap.put(casesNew[1].Id,casesNew[1]);
        
        PS_ServiceNowIntegration.mUpdateCaseStatuswithEscalationStatus(casesNew,caseOldMap,caseNewMap);       
        
        Test.StopTest();   
     }
     
    static testMethod void testPS_ServiceNowIntegration2(){
      
        //User
        List<User> user1 = new List<User>();
        user1  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,1);
        insert user1;
       List<Account> lstAccount = TestDataFactory.createAccount(3,'Organisation');
        insert lstAccount ;
            List<Contact> lstContact = TestDataFactory.createContacts(1);
             lstContact[0].Accountid=lstAccount[0].id;
            lstContact[0].MailingState='England';
            insert lstContact; 
        Test.StartTest();
         System.runAs(user1[0]){
               
            
            string caseStatus = Label.PS_Case_onholdstatus; 
        
            Id TSRecordType = Schema.SObjectType.case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
            
            //insert testGroup;
            Group testGroup = new Group(Name='test group', type='Queue');
            
            //insert testQueue;
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SobjectType = 'Case');
            
            System.assertEquals(lstContact[0].MailingState,'England');               
          }
         Test.StopTest(); 
     }
     
     /*Negative Test Method*/
     static testMethod void testPS_ServiceNowIntegrationNegative(){

        List<Account> lstAccount = TestDataFactory.createAccount(3,'Organisation');
        insert lstAccount ;

        List<Contact> lstContact = TestDataFactory.createContacts(3);
        for(Integer i=0;i<lstContact.size();i++){
            lstContact[i].Accountid=lstAccount[0].id;
            lstContact[i].MailingState='England';
        }
        insert lstContact;
        
        // 26th May 2017-Christwin-SA Project , Changed the State National to School Assessment
        Id SNRecordType = Schema.SObjectType.case.getRecordTypeInfosByName().get('School Assessment').getRecordTypeId();
        // 26th May 2017-Christwin-SA Project , Changed the State National to School Assessment
        
        string caseStatus = 'test'; 
        
        Test.StartTest();
        /**For 'State & National' Cases**/
        List<case> casesOld = TestDataFactory.createCase(1,'School Assessment');
        casesOld[0].Account=lstAccount[0];
        casesOld[0].Contact=lstContact[0]; 
        casesOld[0].Escalation_External_Status__c='New';
        casesOld[0].Status = caseStatus;
        casesOld[0].PS_Business_Vertical__c = 'Higher Education';
        insert casesOld;
        
        Map<Id,case> caseOldMap = new Map<Id,case>();
        caseOldMap.put(casesOld[0].Id, casesOld[0]);
        
        List<case> casesNew = new List<case>();
        casesNew.add(casesOld[0]);
        casesNew[0].Escalation_External_Status__c='Closed';
        Update casesNew;
        
        Map<Id,case> caseNewMap = new Map<Id,case>();
        caseNewMap.put(casesNew[0].Id, casesNew[0]);
        
        
        PS_ServiceNowIntegration.mUpdateCaseStatuswithEscalationStatus(casesNew,caseOldMap,caseNewMap);
        System.assertNotEquals(casesNew[0].Status,Label.PS_CaseClosureStatus);  
        
        Test.StopTest(); 
     }   
    
    static testMethod void testPS_ServiceNowIntegration3(){

        List<Account> lstAccount = TestDataFactory.createAccount(3,'Organisation');
        insert lstAccount ;

        List<Contact> lstContact = TestDataFactory.createContacts(3);
        for(Integer i=0;i<lstContact.size();i++){
            lstContact[i].Accountid=lstAccount[0].id;
            lstContact[i].MailingState='England';
            lstContact[i].MailingCity = 'Bangalore';
            lstContact[i].MailingPostalCode= '560037';
            lstContact[i].MailingStreet= 'BNG';
        }
        insert lstContact;
        
        // 26th May 2017-Christwin-SA Project , Changed the State National to School Assessment
        Id SNRecordType = Schema.SObjectType.case.getRecordTypeInfosByName().get('School Assessment').getRecordTypeId();
        // 26th May 2017-Christwin-SA Project , Changed the State National to School Assessment
        Id TSRecordType = Schema.SObjectType.case.getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
        
        string caseStatus = Label.PS_Case_onholdstatus; 
        
        //Test.StartTest();
        /**For 'State & National' Cases**/
        // Modified for Code Coverage 30 May 2017, Christwin
        List<case> casesOld = TestDataFactory.createCase(3,'School Assessment');
        casesOld[0].Account=lstAccount[0];
        casesOld[0].Contact=lstContact[0]; 
        casesOld[0].Escalation_External_Status__c='New';
        casesOld[0].Status = caseStatus;
        casesOld[0].PS_Business_Vertical__c = 'Higher Education';
        
        casesOld[1].RecordTypeid = TSRecordType;
        casesOld[1].Account=lstAccount[0];
        casesOld[1].Contact=lstContact[0]; 
        casesOld[1].Escalation_External_Status__c='New';
        casesOld[1].Status = caseStatus;
        casesOld[1].PS_Business_Vertical__c = 'Higher Education';
        
        casesOld[2].RecordTypeid = TSRecordType;
        casesOld[2].Account=lstAccount[0];
        casesOld[2].Contact=lstContact[0]; 
        casesOld[2].Escalation_External_Status__c='New';
        casesOld[2].Status = caseStatus;
        casesOld[2].PS_Business_Vertical__c = 'Higher Education';
        
        Test.StartTest();
        insert casesOld;
        
        Map<Id,case> caseOldMap = new Map<Id,case>();
        
        case avoidPassByRef1 = new case();
        avoidPassByRef1.id=casesOld[0].Id;
        avoidPassByRef1.Escalation_External_Status__c='New';
        avoidPassByRef1.Status = caseStatus;
        avoidPassByRef1.PS_Business_Vertical__c = 'Higher Education';
        avoidPassByRef1.RecordTypeid = TSRecordType;
        avoidPassByRef1.Account=lstAccount[0];
        avoidPassByRef1.Contact=lstContact[0];
        caseOldMap.put(casesOld[0].Id, avoidPassByRef1);
        
        case avoidPassByRef2 = new case();
        avoidPassByRef2.id=casesOld[2].Id;
        avoidPassByRef2.Escalation_External_Status__c='New';
        avoidPassByRef2.Status = caseStatus;
        avoidPassByRef2.PS_Business_Vertical__c = 'Higher Education';
        avoidPassByRef2.RecordTypeid = TSRecordType;
        avoidPassByRef2.Account=lstAccount[0];
        avoidPassByRef2.Contact=lstContact[0]; 
        caseOldMap.put(casesOld[2].Id, avoidPassByRef2); 
        
        case avoidPassByRef = new case();
        avoidPassByRef.id=casesOld[1].Id;
        avoidPassByRef.Escalation_External_Status__c='New';
        avoidPassByRef.Status = caseStatus;
        avoidPassByRef.PS_Business_Vertical__c = 'Higher Education';
        avoidPassByRef.RecordTypeid = TSRecordType;
        avoidPassByRef.Account=lstAccount[0];
        avoidPassByRef.Contact=lstContact[0]; 
        caseOldMap.put(casesOld[1].Id, avoidPassByRef);
       
        List<case> casesNew = new List<case>();
        casesNew.add(casesOld[0]);
        casesNew[0].Escalation_External_Status__c='Pending';
        casesNew.add(casesOld[1]);
        casesNew[1].Escalation_External_Status__c='Resolved';
        casesNew[1].Status = 'Assigned Outside Support';
        casesNew.add(casesOld[2]);
        casesNew[2].Escalation_External_Status__c='Pending';        
        Update casesNew;
        
        Map<Id,case> caseNewMap = new Map<Id,case>();
        caseNewMap.put(casesNew[0].Id,casesNew[0]);
        caseNewMap.put(casesNew[1].Id,casesNew[1]);
        // Modified for Code Coverage 30 May 2017,Christwin End
        PS_ServiceNowIntegration.mUpdateCaseStatuswithEscalationStatus(casesNew,caseOldMap,caseNewMap);       
        
        Test.StopTest();   
     }
}