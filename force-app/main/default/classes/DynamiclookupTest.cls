/************************************************************************************************************
* Apex Interface Name : DynamiclookupTest
* Version             : 1.1 
* Created Date        : 10/04/2017 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Manikanta Nagubilli       10/04/2017      
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class DynamiclookupTest{
    
    static testMethod void test_getproducts(){
        Product__c pr = new Product__c(name='prod1', Active_in_Case__c=true,Active_in_Self_Service__c = true, Type__c='Product',Country__c = 'United States');
        insert pr;
        Test.StartTest();
            Dynamiclookup.getdataList('United States','prod','Name','Product__c','id,name,Active_in_Case__c,Type__c,Country__c,Active_in_Self_Service__c',false,'Clinical');
            Dynamiclookup.getdataList('United States','prod','Name','Account','id,name',true,'test');
            Dynamiclookup.getdataList('United States','prod','Name','Account','id,name',true,'test');
            Dynamiclookup.getdataList('India','prod','Name','Account','id,name',true,'test');
            Dynamiclookup.getObjInfo('Product__c','id,name,Active_in_Case__c,Type__c,Country__c,Active_in_Self_Service__c',pr.id+'id_'+pr.id);
        Test.StopTest();
    }
}