/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_caseEntitlement.CLS
   Description:     Method used for Population entitlement name during case creation - RD-01565
    TestClass:      PS_caseEntitlementTest.cls
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  10/12/2015         1.0             Tejaswini Janda                          Initial Release 
  14/7/2015          1.1             Payal Popat                             Updated class to fire for Customer Service Case
16/01/2019          1.2             Saritha Singamsetty                     Modified for CR-02308 to remove dependency of business vertical for case entitlements
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public with sharing class PS_caseEntitlement{
    /**
    * Description : Method to stamp entitlement on case creation based on case account and LOB
    * @param NA
    * @return String
    * @throws NA
    **/ 
    public static void UpdateCaseEntitlement(List<Case> triggeredcase){
        List<Case> newCases =new List<Case>();
        Set<ID> accID=new Set<ID>();
        Set<ID> caseID=new Set<ID>();
        List<Entitlement> listEnt=new List<Entitlement>();
        Map<Id,Entitlement> mapEnt=new Map<Id,Entitlement>();
      //Map<Id,String> mapOfIdToLOB = new Map<Id,String>(); 
        
        //Filter Service Record type casesfor HE Tech and SN
        newCases.addAll(CommonUtils.getCaseServiceRecordType(System.Label.CaseHeTechSupportRecordType,triggeredcase) );
        newCases.addAll(CommonUtils.getCaseServiceRecordType(System.Label.State_National,triggeredcase) );
        //R6 - SUS
        newCases.addAll(CommonUtils.getCaseServiceRecordType(System.Label.CaseCSRecordType,triggeredcase) );
        if (newCases.size()>0){    
            //Init Case Account and LOB                    
            for(Case c:newCases){
               accID.add(c.AccountID);
               /*if(c.PS_Business_Vertical__c != null){
                   mapOfIdToLOB.put(c.id,c.PS_Business_Vertical__c);
               } */   
            }//end for loop
            //Fetch Entitlements
            listEnt = [select id,accountid from entitlement where accountid in : accID order by lastmodifieddate desc];
            
            /*listEnt = [select id,accountid,Business_Vertical__c from entitlement where accountid in : accID and 
                       Business_Vertical__c in : mapOfIdToLOB.values() order by lastmodifieddate desc];*/
            //Use recently created a=entitlement based on Account Id and case LOB
            for(Case caseObj:newCases){
                for(Entitlement ent:listEnt){                 
                    /* if(!mapEnt.ContainsKey(ent.AccountID+ent.Business_Vertical__c ) && ent.Business_Vertical__c == mapOfIdToLOB.get(caseObj.id)){
                         mapEnt.put(ent.AccountID+ent.Business_Vertical__c,ent );
                     }*/
                     if(!mapEnt.ContainsKey(ent.AccountID)){
                         mapEnt.put(ent.AccountID,ent );
                     }
                }
            }
            //default entitlement to case
            for(Case c:newCases){
              if(c.EntitlementID == null &&  c.AccountID != null){
                  // Baracket added after if to fix INC4840811 - Christwin 3/14/2019
                  if (mapEnt.containsKey(c.AccountID)){
                      if(!mapEnt.isEmpty())
                      {
                      c.EntitlementID=mapEnt.get(c.AccountID).id; 
                      }
                  }    
              }
            }
                /*
              if(c.EntitlementID == null &&  c.AccountID != null){
                //  if (mapEnt.containsKey(c.AccountID))
                      c.EntitlementID= System.Label.DefaultCaseEntitlementID;
              }
              
            }//end case loop*/
        }//end service cases condition
    }//end method definition
}