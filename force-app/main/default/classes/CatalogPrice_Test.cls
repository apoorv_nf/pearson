@isTest
public class CatalogPrice_Test {
    
    static testMethod void DeleteCatPrice() {
        
        User newUser =  [select Id, username from User where market__c = 'US' and Profile.name = 'System Administrator' and isActive = true Limit 1];
        
        Bypass_Settings__c settings = new Bypass_Settings__c();
        settings.Disable_Triggers__c = true;
        //settings.SetupOwnerId = lstWithTestUser[0].id;
        settings.SetupOwnerId =newUser.id;
        insert settings;
        
        //GK
        newUser.Product_Business_Unit__c = 'CTIPIHE';
        newUser.Market__c = 'US';
        newUser.Line_of_Business__c = 'Higher Ed';
        newUser.Business_Unit__c = 'CTIPIHE';
        update newUser;
        
        system.RunAs(newUser){           
             
            
            Recordtype recordtypeInst = [select Id from recordtype where developername = 'HE_Record_Type' and sobjecttype = 'Product2'];
    		
            List<Product2> sProduct = TestDataFactory.createProduct(1);
            sProduct[0].Market__c = 'US';
            sProduct[0].Line_of_Business__c= 'Higher Ed';
            sProduct[0].Business_Unit__c = 'CTIPIHE';
            sProduct[0].RecordtypeId = recordtypeInst.Id;
            //GK
            sProduct[0].Author__c = 'Talent'; 
            sProduct[0].Copyright_Year__c = '2005';
            sProduct[0].Competitor_Product__c = true;
            sProduct[0].isActive = true;
            sProduct[0].net_price__c = 4.5;
            insert sProduct;
            system.debug('product details---'+sProduct);
           
            Catalog__c cat = new Catalog__c();        
            cat.Name = 'US HE ALL';
            insert cat;
            
            Test.startTest();
            CatalogPrice__c catPrice = new CatalogPrice__c();
            catPrice.catalog__c = cat.Id ;      
            catPrice.Product__c = sProduct[0].Id;
            catPrice.isActive__c = true;
            catPrice.NetPrice__c = 10.1;
            Insert catPrice;           
            
            catPrice.NetPrice__c = 20.1;
            Update catPrice;
            
            Delete catPrice;
            Test.stopTest();
        }
    } 
    
}