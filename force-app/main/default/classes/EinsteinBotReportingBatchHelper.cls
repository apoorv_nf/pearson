/**
 * Apex Class Name : EinsteinBotReportingBatchHelper
 * Version         : 1.0
 * Created On      : 12-07-2018
 * Function        : A helper class to process the LiveChatTranscript to populate all the Reporting metrics, used in Batch class
 *                      > It calculates Self_Served_Dialog_Reached__c , Business_Dialog_Reached__c, Einstein_Bot_Escalation__c, Agent_Wait_Time__c, System_Wait_Time__c on LiveChatTranscript by looking at the related Einstein_Bot_Chat_Log__c records
 *                      > It updates the IsFirstTopicName__c on Einstein_Bot_Chat_Log__c to summerize the Topic names
 * Test Class      : EinsteinBotReportingBatchTest
 */

public class EinsteinBotReportingBatchHelper {
    List<LiveChatTranscriptEvent> transcriptEvtList;
    public EinsteinBotReportingBatchHelper(Set<Id> tanscriptIds){
        this.transcriptEvtList = [SELECT  Id, LiveChatTranscriptId, Name, Time, Type, CreatedDate, IsDeleted FROM LiveChatTranscriptEvent WHERE LiveChatTranscriptId IN :tanscriptIds];
        update this.updateTranscriptResolution_Escalation(this.updateTranscriptsBusiness(this.updateTranscriptsSelfServed(this.calculateWaitTime())));
        this.updateFirstTopicEinsteinChatLog(tanscriptIds);
    }

    public void updateFirstTopicEinsteinChatLog(Set<Id> tanscriptIds) {
        Map<String,Einstein_Bot_Chat_Log__c> sessionTopicNameChatLogMap = new Map<String,Einstein_Bot_Chat_Log__c>();
        List<Einstein_Bot_Chat_Log__c> einsteinChatLogList = [SELECT Id,IsFirstTopicName__c,Topic_Name__c,Live_Agent_Session_Id__c FROM Einstein_Bot_Chat_Log__c WHERE Live_Chat_Transcript__c IN :tanscriptIds ORDER BY CreatedDate];
        for(Einstein_Bot_Chat_Log__c chatLog : einsteinChatLogList) {
            String tempString = chatLog.Topic_Name__c+chatLog.Live_Agent_Session_Id__c;
            if(!sessionTopicNameChatLogMap.ContainsKey(tempString)) {
                chatLog.IsFirstTopicName__c = true;
                sessionTopicNameChatLogMap.put(tempString,chatLog);
            }
        }
        if(!sessionTopicNameChatLogMap.isEmpty()) {
            update sessionTopicNameChatLogMap.values();
        }
    }

    public List<LiveChatTranscript> updateTranscriptsSelfServed(List<LiveChatTranscript> trancripts) {
        Map<String,LiveChatTranscript> sessionTranscriptMap = new Map<String,LiveChatTranscript>();
        for (LiveChatTranscript transcript : trancripts) {
            sessionTranscriptMap.put(transcript.ChatKey,transcript);
        }
        Einstein_Bot_Chat_Log__c[] chatLogIntentCycles = [SELECT Live_Agent_Session_Id__c,Business_Metric_Type__c FROM Einstein_Bot_Chat_Log__c WHERE Live_Agent_Session_Id__c IN : sessionTranscriptMap.keySet()];
        for (Einstein_Bot_Chat_Log__c chatLogIntentCycle : chatLogIntentCycles) {
            if(!sessionTranscriptMap.get(chatLogIntentCycle.Live_Agent_Session_Id__c).Self_Served_Dialog_Reached__c && (chatLogIntentCycle.Business_Metric_Type__c == 'Direct'  || chatLogIntentCycle.Business_Metric_Type__c == 'End' )  ) {
                sessionTranscriptMap.get(chatLogIntentCycle.Live_Agent_Session_Id__c).Self_Served_Dialog_Reached__c = true;
            }
        }
        return sessionTranscriptMap.values();
    }

    public List<LiveChatTranscript> updateTranscriptResolution_Escalation(List<LiveChatTranscript> trancripts) {
        Map<String,LiveChatTranscript> sessionTranscriptMap = new Map<String,LiveChatTranscript>();
        for (LiveChatTranscript transcript : trancripts) {
            sessionTranscriptMap.put(transcript.ChatKey,transcript);
        }
        Einstein_Bot_Chat_Log__c[] chatLogIntentCycles = [SELECT Live_Agent_Session_Id__c,Successful_Resolution__c,Current_Dialog_Name__c FROM Einstein_Bot_Chat_Log__c WHERE Live_Agent_Session_Id__c IN : sessionTranscriptMap.keySet()];
        for (Einstein_Bot_Chat_Log__c chatLogIntentCycle : chatLogIntentCycles) {
            if(chatLogIntentCycle.Successful_Resolution__c != null) {
                sessionTranscriptMap.get(chatLogIntentCycle.Live_Agent_Session_Id__c).Successful_Resolution__c = chatLogIntentCycle.Successful_Resolution__c;
            }
            if(chatLogIntentCycle.Current_Dialog_Name__c == 'No_Agent_Available') {
                sessionTranscriptMap.get(chatLogIntentCycle.Live_Agent_Session_Id__c).Einstein_Bot_Escalation__c = 'No Agents Available';
            }
        }
        return sessionTranscriptMap.values();
    }

    public List<LiveChatTranscript> updateTranscriptsBusiness(List<LiveChatTranscript> trancripts) {
        Map<String,LiveChatTranscript> sessionTranscriptMap = new Map<String,LiveChatTranscript>();
        for (LiveChatTranscript transcript : trancripts) {
            sessionTranscriptMap.put(transcript.ChatKey,transcript);
        }
        Einstein_Bot_Chat_Log__c[] chatLogIntentCycles = [SELECT Live_Agent_Session_Id__c,Business_Metric_Type__c FROM Einstein_Bot_Chat_Log__c WHERE Live_Agent_Session_Id__c IN : sessionTranscriptMap.keySet()];
        for (Einstein_Bot_Chat_Log__c chatLogIntentCycle : chatLogIntentCycles) {
            if(!sessionTranscriptMap.get(chatLogIntentCycle.Live_Agent_Session_Id__c).Business_Dialog_Reached__c && chatLogIntentCycle.Business_Metric_Type__c == 'Start') {
                sessionTranscriptMap.get(chatLogIntentCycle.Live_Agent_Session_Id__c).Business_Dialog_Reached__c = true;
            }
        }
        return sessionTranscriptMap.values();
    }

    public List<LiveChatTranscript> calculateWaitTime() {
        MAp<Id, Map<String, LiveChatTranscriptEvent>> transcriptToEventTypeMap = new  MAp<Id, Map<String, LiveChatTranscriptEvent>>();
        LiveChatTranscript transcript;
        for(LiveChatTranscriptEvent event : transcriptEvtList) {
            Map<String, LiveChatTranscriptEvent> temp = new Map<String, LiveChatTranscriptEvent>();
            if (transcriptToEventTypeMap.containsKey(event.LiveChatTranscriptId)) {
                temp = transcriptToEventTypeMap.get(event.LiveChatTranscriptId);
            }
            temp.put(event.Type, event);
            transcriptToEventTypeMap.put(event.LiveChatTranscriptId, temp);
        }
        Map<Id,LiveChatTranscript> transcriptMap = new Map<Id,LiveChatTranscript>([SELECT Id,Einstein_Analytics_Reporting_Process__c,ChatKey,
                System_Wait_Time__c,Agent_Wait_Time__c,LiveChatButton.DeveloperName  FROM LiveChatTranscript WHERE Id IN :transcriptToEventTypeMap.keyset()]);
        for(String transcriptId : transcriptToEventTypeMap.keyset()) {
            transcriptMap.get(transcriptId).Einstein_Analytics_Reporting_Process__c=true;
            Map<String, LiveChatTranscriptEvent> evtTypeToEvtObjMap = transcriptToEventTypeMap.get(transcriptId);
            if(evtTypeToEvtObjMap.get('LeaveVisitor') != null) {
                if(evtTypeToEvtObjMap.ContainsKey('PushAssignment') && evtTypeToEvtObjMap.ContainsKey('Transfer')) {
                    transcriptMap.get(transcriptId).Agent_Wait_Time__c = this.subtractTime(evtTypeToEvtObjMap.get('PushAssignment').Time,evtTypeToEvtObjMap.get('Transfer').Time);
                }
                if(evtTypeToEvtObjMap.ContainsKey('PushAssignment') && evtTypeToEvtObjMap.ContainsKey('TransferredToButton')) {
                    transcriptMap.get(transcriptId).System_Wait_Time__c = this.subtractTime(evtTypeToEvtObjMap.get('TransferredToButton').Time,evtTypeToEvtObjMap.get('PushAssignment').Time);
                }
                if(evtTypeToEvtObjMap.ContainsKey('ChatRequest') && (evtTypeToEvtObjMap.containsKey('TransferredToButton') || evtTypeToEvtObjMap.containsKey('LeaveVisitor'))) {
                    if(evtTypeToEvtObjMap.containsKey('TransferredToButton')){
                        transcriptMap.get(transcriptId).Einstein_Bot_Duration__c = this.subtractTime(evtTypeToEvtObjMap.get('ChatRequest').Time,evtTypeToEvtObjMap.get('TransferredToButton').Time);
                    } else {
                        transcriptMap.get(transcriptId).Einstein_Bot_Duration__c = this.subtractTime(evtTypeToEvtObjMap.get('ChatRequest').Time,evtTypeToEvtObjMap.get('LeaveVisitor').Time);
                    }
                }
                if(evtTypeToEvtObjMap.containsKey('TransferredToButton')) {
                    transcriptMap.get(transcriptId).Einstein_Bot_Escalation__c = 'Requested';
                    if(evtTypeToEvtObjMap.containsKey('Transfer')) {
                        transcriptMap.get(transcriptId).Einstein_Bot_Escalation__c = 'Successful';
                    } else {
                        transcriptMap.get(transcriptId).Einstein_Bot_Escalation__c = 'Miss Chat';
                    }
                }
            }
        }
        return transcriptMap.values();
    }


    public Integer subtractTime(DateTime startTime,DateTime endTime) {
        Integer subtractedTime;
        Long startTimeMS = startTime.getTime();
        Long endTimeMS = endTime.getTime();
        subtractedTime = (Integer)(endTimeMS - startTimeMS);
        return subtractedTime/1000;
    }
}