/**
 * Name : PS_OpportunityTerritoryAssignment_Test
 * Author : Accenture_Karan
 * Description : Test class used for testing PS_OpportunityTerritoryAssignment
 * Date : 20/10/15 
 * Version : <intial Draft>
 */


@isTest
public class PS_OpportunityTerritoryAssignment_Test
{
    static testMethod void testOpportunityTerritoryAssignmentCase1(){
    
        // Test data preparation        
         List<PS_TerritoryMovedAddeddeleted__c> lstWithTerritoryCustomSetting = new List<PS_TerritoryMovedAddeddeleted__c>();
      lstWithTerritoryCustomSetting = TestDataFactory.createTerritoryCustomSettingRecord();  
      
        List<Account> accountList = TestDataFactory.createAccount(1,'Organisation');    
        insert accountList; 
        
        List<Opportunity> optyList = TestDataFactory.createOpportunity(1, PS_Constants.OPPORTUNITY_GLOBAL_RECCORD);
        optyList[0].AccountId = accountList[0].Id;
        insert optyList;
        
        List<Id> optyIdList = new List<Id>();
        optyIdList.add(optyList[0].Id);         
        
        test.startTest();           
            insert lstWithTerritoryCustomSetting;    
            PS_OpportunityTerritoryAssignment obj = new PS_OpportunityTerritoryAssignment();
            Map<Id,Id> oppTerMap = obj.getOpportunityTerritory2Assignments(optyIdList);
            
            Id dummyId;
            PS_OpportunityTerritoryAssignment.Territory2Priority obj2 = new PS_OpportunityTerritoryAssignment.Territory2Priority(dummyId, 1, true);
            
            Id toBeTerId;
            if(oppTerMap.get(optyIdList[0]) !=null){
                toBeTerId = oppTerMap.get(optyIdList[0]);
            }
            System.AssertEquals(toBeTerId, null);           
            
        test.stopTest();    
    }
    
    static testMethod void testOpportunityTerritoryAssignmentCase2(){
    
        // Test data preparation        
        
        List<Account> accountList = TestDataFactory.createAccount(1,'Organisation');    
        insert accountList; 
         List<PS_TerritoryMovedAddeddeleted__c> lstWithTerritoryCustomSetting = new List<PS_TerritoryMovedAddeddeleted__c>();
      lstWithTerritoryCustomSetting = TestDataFactory.createTerritoryCustomSettingRecord();  
      
        List<Opportunity> optyList = TestDataFactory.createOpportunity(1, PS_Constants.OPPORTUNITY_GLOBAL_RECCORD);
        optyList[0].AccountId = accountList[0].Id;
        insert optyList;
        
        List<Id> optyIdList = new List<Id>();
        optyIdList.add(optyList[0].Id); 
                
        List<User> usadminList = TestDataFactory.createUser([select Id FROM Profile WHERE Name ='System Administrator'].Id, 1);     
        insert usadminList;
        
        test.startTest();           
            insert lstWithTerritoryCustomSetting;
            // creating Territory as different user because of MIXED DML Operation Error
            system.runAs(usadminList[0]){                                
                Territory2Model t2m = TestDataFactory.createTerritory2Model('TestClassTerritory2Model');
                insert t2m;
                Territory2Type tType = TestDataFactory.createTerritory2Type();
                List<Territory2Model> t2mList = new List<Territory2Model>([SELECT Id, State FROM Territory2Model WHERE State = 'Active']);
                Territory2 tr0 = TestDataFactory.createTerritory('acTestTerritory1', t2mList[0].Id, tType.Id);
                insert tr0;
            }
            
            List<Territory2> tr1 = new List<Territory2>([SELECT Id FROM Territory2 WHERE Territory2Model.State = 'Active' limit 1]);
            ObjectTerritory2Association ota = new ObjectTerritory2Association();
            ota.ObjectId = accountList[0].Id;
            ota.Territory2Id = tr1[0].Id;
            ota.AssociationCause = 'Territory2Manual';
            insert ota;     
                
            PS_OpportunityTerritoryAssignment obj = new PS_OpportunityTerritoryAssignment();
            Map<Id,Id> oppTerMap = obj.getOpportunityTerritory2Assignments(optyIdList);
            
            Id dummyId;
            PS_OpportunityTerritoryAssignment.Territory2Priority obj2 = new PS_OpportunityTerritoryAssignment.Territory2Priority(dummyId, 1, true);
                        
            Id toBeTerId = oppTerMap.get(optyIdList[0]);
            System.AssertEquals(toBeTerId, tr1[0].Id);           
            
        test.stopTest();    
    }
    
    static testMethod void testOpportunityTerritoryAssignmentCase3(){
    
        List<User> usadminList = TestDataFactory.createUser([select Id FROM Profile WHERE Name ='System Administrator'].Id, 2);     
        insert usadminList;
         List<PS_TerritoryMovedAddeddeleted__c> lstWithTerritoryCustomSetting = new List<PS_TerritoryMovedAddeddeleted__c>();
      lstWithTerritoryCustomSetting = TestDataFactory.createTerritoryCustomSettingRecord();  
      
        test.startTest();           
            insert lstWithTerritoryCustomSetting;
            // creating Territory as different user because of MIXED DML Operation Error
            system.runAs(usadminList[0]){                                
                Territory2Model t2m = TestDataFactory.createTerritory2Model('TestClassTerritory2Model');
                insert t2m;
                Territory2Type tType = TestDataFactory.createTerritory2Type();
                List<Territory2Model> t2mList = new List<Territory2Model>([SELECT Id, State FROM Territory2Model WHERE State = 'Active']);
                List<Territory2> trList = new List<Territory2>();
                Territory2 tr0 = TestDataFactory.createTerritory('accTestTerritory1', t2mList[0].Id, tType.Id);
                Territory2 tr1 = TestDataFactory.createTerritory('accTestTerritory2', t2mList[0].Id, tType.Id);
                Territory2 tr2 = TestDataFactory.createTerritory('cTestTerritory2', t2mList[0].Id, tType.Id);
                trList.add(tr0);
                trList.add(tr1);
                trList.add(tr2);
                insert trList;
            }
                        
            List<Account> accountList = new List<Account>();
            List<Opportunity> optyList = new List<Opportunity>();
            List<Territory2> trList = new List<Territory2>([SELECT Id, Territory2ModelId FROM Territory2 WHERE Territory2Model.State = 'Active' limit 3]); 
            
            system.runAs(usadminList[1]){                                
                
                accountList = TestDataFactory.createAccount(1,'Organisation');    
                insert accountList; 
                
                optyList = TestDataFactory.createOpportunity(1, PS_Constants.OPPORTUNITY_GLOBAL_RECCORD);
                optyList[0].AccountId = accountList[0].Id;
                insert optyList;                
                
                List<ObjectTerritory2Association> otaList = new List<ObjectTerritory2Association>();
                ObjectTerritory2Association ota1 = TestDataFactory.createObjectTerritory2Association(accountList[0].Id, trList[0].Id, 'Territory2Manual');         
                otaList.add(ota1);
                ObjectTerritory2Association ota2 = TestDataFactory.createObjectTerritory2Association(accountList[0].Id, trList[1].Id, 'Territory2Manual');         
                otaList.add(ota2);
                insert otaList; 
                UserTerritory2Association uta = TestDataFactory.createUserTerritory2Association(UserInfo.getUserId(), trList[2].Id);
                insert uta;                
            }
                        
            List<Id> optyIdList = new List<Id>();
            optyIdList.add(optyList[0].Id);        
            PS_OpportunityTerritoryAssignment obj = new PS_OpportunityTerritoryAssignment();
            Map<Id,Id> oppTerMap = obj.getOpportunityTerritory2Assignments(optyIdList);
            
            Id toBeTerId = oppTerMap.get(optyIdList[0]);
            System.AssertEquals(toBeTerId, trList[2].Id);           
            
        test.stopTest();    
    }
    
    static testMethod void testOpportunityTerritoryAssignmentCase4(){
    
        List<User> usadminList = TestDataFactory.createUser([select Id FROM Profile WHERE Name ='System Administrator'].Id, 2);     
        insert usadminList;
         List<PS_TerritoryMovedAddeddeleted__c> lstWithTerritoryCustomSetting = new List<PS_TerritoryMovedAddeddeleted__c>();
      lstWithTerritoryCustomSetting = TestDataFactory.createTerritoryCustomSettingRecord();  
      
        test.startTest();           
            insert lstWithTerritoryCustomSetting;
            // creating Territory as different user because of MIXED DML Operation Error
            system.runAs(usadminList[0]){                                
                Territory2Model t2m = TestDataFactory.createTerritory2Model('TestClassTerritory2Model');
                insert t2m;
                Territory2Type tType = TestDataFactory.createTerritory2Type();
                List<Territory2Model> t2mList = new List<Territory2Model>([SELECT Id, State FROM Territory2Model WHERE State = 'Active']);
                List<Territory2> trList = new List<Territory2>();
                Territory2 tr0 = TestDataFactory.createTerritory('accTestTerritory1', t2mList[0].Id, tType.Id);
                Territory2 tr1 = TestDataFactory.createTerritory('accTestTerritory2', t2mList[0].Id, tType.Id);
                Territory2 tr2 = TestDataFactory.createTerritory('cTestTerritory2', t2mList[0].Id, tType.Id);
                trList.add(tr0);
                trList.add(tr1);
                trList.add(tr2);
                insert trList;
            }
                        
            List<Account> accountList = new List<Account>();
            List<Opportunity> optyList = new List<Opportunity>();
            List<Territory2> trList = new List<Territory2>([SELECT Id, Territory2ModelId FROM Territory2 WHERE Territory2Model.State = 'Active' limit 4]); 
            
            system.runAs(usadminList[1]){                                
                
                accountList = TestDataFactory.createAccount(1,'Organisation');    
                insert accountList; 
                
                optyList = TestDataFactory.createOpportunity(1, PS_Constants.OPPORTUNITY_GLOBAL_RECCORD);
                optyList[0].AccountId = accountList[0].Id;
                insert optyList;                
                
                List<ObjectTerritory2Association> otaList = new List<ObjectTerritory2Association>();
              
                ObjectTerritory2Association ota1 = TestDataFactory.createObjectTerritory2Association(accountList[0].Id, trList[0].Id, 'Territory2Manual');         
                otaList.add(ota1);
                ObjectTerritory2Association ota2 = TestDataFactory.createObjectTerritory2Association(accountList[0].Id, trList[1].Id, 'Territory2Manual');         
                otaList.add(ota2);
                insert otaList; 
                List<UserTerritory2Association> utaList = new List<UserTerritory2Association>();
                UserTerritory2Association uta1 = TestDataFactory.createUserTerritory2Association(UserInfo.getUserId(), trList[2].Id);
                UserTerritory2Association uta2 = TestDataFactory.createUserTerritory2Association(UserInfo.getUserId(), trList[3].Id);
                utaList.add(uta1);
                utaList.add(uta2);
                insert utaList;                
            }
                        
            List<Id> optyIdList = new List<Id>();
            optyIdList.add(optyList[0].Id);        
            PS_OpportunityTerritoryAssignment obj = new PS_OpportunityTerritoryAssignment();
            Map<Id,Id> oppTerMap = obj.getOpportunityTerritory2Assignments(optyIdList);
            
            Id toBeTerId;
            if(oppTerMap.get(optyIdList[0]) !=null){
                toBeTerId = oppTerMap.get(optyIdList[0]);
            }            
            System.AssertEquals(toBeTerId, null);           
            
        test.stopTest();    
    }
}