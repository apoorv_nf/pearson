/*******************************************************************************************************************
* Apex Class Name   : PS_Account_TriggerSequenceCtrlr_Test
* Created Date      : 16th Sep 2015
* Description       : Test class for Account_TriggerSequenceCtrlr class
*******************************************************************************************************************/
@isTest
public class PS_Account_TriggerSequenceCtrlr_Test {

    static testMethod void myTest1() {
    
        Account pAccount2=new Account();
        pAccount2.Name='Test Sample Account2';
        pAccount2.Phone = '00012544';
        pAccount2.Primary_Selling_Account_check__c =true;
        pAccount2.ShippingCountry ='India';
        pAccount2.ShippingCity ='Chennai';
        pAccount2.ShippingStreet ='Dubai';
        pAccount2.ShippingPostalCode ='600119';
        insert pAccount2;
        
        
        Account pAccount1=new Account();
        pAccount1.ParentId= pAccount2.Id;
        pAccount1.Name='Test Sample Account';
        pAccount1.Phone = '0001254';
        pAccount1.Primary_Selling_Account_check__c =true;
        insert pAccount1;
        
        
        Account pAccount=new Account();
    //    Id RecordTypeIdContact = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Organisation').getRecordTypeId();
        pAccount.ParentId= pAccount1.Id;
        pAccount.Name='Test Sample Account1';
        pAccount.Phone = '00012522';
        pAccount.Primary_Selling_Account_check__c =true;
     //   pAccount.recordtypeid=RecordTypeIdContact;
     //   pAccount.ShippingCountry='India';
    //    pAccount.ShippingCity ='Chennai';
    //    pAccount.ShippingStreet ='Dubai';
    //    pAccount.ShippingPostalCode ='600119';
   //     pAccount.RecordTypeId = Label.Record_type_of_Account_Organisation;
        insert pAccount;
        
        
        List<Account> lstCreateAccount=new List<Account>();
        //lstCreateAccount.add(pAccount);
        
        List<Account> accountList=TestDataFactory.createAccount(1,'Learner');
        //insert accountList;
        
        checkRecurssionAfter.run =true;
        test.startTest();
            List<Account> lstAccount=new List<Account>();
            for(Account acc:accountList){
                acc.parentId=pAccount.Id;
                lstAccount.add(acc);
            }
            lstCreateAccount.addAll(lstAccount);
            insert lstCreateAccount;
            
            
            checkRecurssionAfter.run =true;
            Account ac=new Account(Id= lstAccount[0].Id);
            ac.ParentId=pAccount.Id;
            ac.Primary_Selling_Account_check__c =true;
            update ac;
            
            checkRecurssionAfter.run =true;
            Account ac1=new Account(Id= ac.Id);
            //ac.ParentId=pAccount.Id;
            ac1.Primary_Selling_Account_check__c =false;
            update ac1;
            System.assert(true,ac);
            System.assert(true,pAccount1);
            System.assert(true,pAccount);
        test.stopTest();
    }
}