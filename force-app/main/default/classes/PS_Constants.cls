/*******************************************************************************************************************************************************
* Apex Class Name   : PS_Constants.cls
* Version           : 1.0  
* Created Date      : 26 Aug 2015
* Function          : Contant Class               
* Modification Log  :
* Developer                   Date                    Description
* -------------------------------------------------------------------------------------------------------------------------------------------------------
* Leonard Victor           26/08/2015                Constant Class used to store  static values
* Gousia begum             1/10/2015                 B2B2L Record type added
* Rahul Boinepally         13/10/2015                Added two new Lead trigger variables. 
* Leonard Victor           3/11/2015                 Added Genric Constants and Removed B2B2L
* Tom Carman               14/Dec/2015               Add various constants to do with Order / Order Item
* Navaneeth                26/Nov/2018               Added a new Constant for "Global Sample Order" in Order Object as per CR # 02339-Req-1
*********************************************************************************************************************************************************/

public class PS_Constants {
    //Genric Contants
    public static String TRIGGER_INSERT = 'Insert';
    public static String TRIGGER_UPDATE = 'Update';
    public static String GENRIC_NAMING = 'PS_';
    public static String GENRIC_NAMING1 = 'Operations';
    public static String COUNTRY_UK = 'UK';
    public static String COUNTRY_AU = 'AU';
    public static String COUNTRY_NZ = 'NZ';
    public static String COUNTRY_CA = 'CA';//Added for CR-1459--Req-3
    public static String SELLING_SEMESTER1 = 'Semester 1';
    public static String SELLING_SEMESTER2 = 'Semester 2';
    public static String TRIGGER_DELETE = 'Delete';
    //Account Constants
    public static String ACCOUNT_OBJECT = 'Account';
    public static String ACCOUNT_LEARNER_RECCORD = 'Learner';
    public static String ACCOUNT_ORGANISATION_RECCORD = 'Organisation';
    public static String ACCOUNT_BOOKSTORE_RECCORD = 'Bookstore';
    public static String ACCOUNT_CORPORATE_RECCORD = 'Corporate';
    public static String ACCOUNT_HIGHED_RECCORD = 'Higher Education';
    public static String ACCOUNT_NONINT_RECCORD = 'Non_Integrated';
    public static String ACCOUNT_SCHOOL_RECCORD = 'School';
    public static String ACCOUNT_PROFESSIONAL_RECCORD = 'Professional';
    public static String ACCOUNT_CREATION_REQUEST = 'Account Creation Request';//added by Srikanth for CR-02663
    public static String Account_GENERATE_PSID1 = '123456789BCDFGHJKLMNPQRSTVWXYZ';
    public static String Account_GENERATE_PSID2 = '123456789';
    public static String Account_TRIGGER = 'PS_AccountTrigger';
    public static String ACCOUNT_PREFIX = 'Account.%';
    
    //Opportunity Constants
    public static String OPPORTUNITY_OBJECT = 'Opportunity';
    public static String OPPORTUNITY_B2B2L_RECCORD = 'B2B';
    public static String OPPORTUNITY_B2B_RECCORD = 'B2B';
    public static String OPPORTUNITY_GLOBAL_RECCORD = 'Global Opportunity';
    public static String OPPORTUNITY_D2L_RECCORD = 'D2L';
    public static String OPPORTUNITY_OPPORTUNITY_RECCORD = 'Opportunity';
    public static String OPPORTUNITY_TRIGGER = 'PS_OpportunityTrigger';
    public static String OPPTYCOURSE_TRIGGER = 'OpportunityUniversityCourse';
    
    //Course Constants

    public static String COURSE_TRIGGER = 'UniversityCourse';
    
    //UniversityCourse contacts
    public static String COURSECONTACT_TRIGGER = 'UniversityCourseContact';
    
    //Asset
    public static String ASSET_TRIGGER ='Asset';
    
    // Lead
    public static String LEAD_AFTER_TRIGGER ='PS_LeadAfterTrigger';
    public static String LEAD_BEFORE_TRIGGER ='PS_LeadBeforeTrigger';
    
    // Order 
    public static String ORDER_SAMPLE_ORDER ='Sample Order';
    public static String ORDER_REVENUE_ORDER ='Revenue Order'; 
    public static String UK_ORDER_GLOBAL_SAMPLE_ORDER ='Global Sample Order'; // Added by Navaneeth for CR # 02339-Req-1
    
    //GRL - CR-2141-Req-28(IR)  
    public static String ORDER_GLOBAL_RIGHTS_SALES_CONTRACT ='Global Rights Sales Contract';

    
    public static String Global_REVENUE_ORDER ='Global Revenue Order';  //R6

    public static final Set<String> ORDER_SUBMITTED_STATUSES = new Set<String>{'Open'};

    
    //Line Of Business 
    public static String LINEOFBIZ_HIGHER_ED ='Higher Ed'; 
    //Business Unit
     public static String BIZUNIT_ELT ='ELT'; 


    // Permission Set Names
    public static final String PERMISSIONSET_BACKEND_REVENUE = 'Pearson Global Backend Revenue';
    public static final String PERMISSIONSET_BACKEND_SAMPLE = 'Pearson Global Backend Sample';
  
    // Trigger Names
    public static String LEAD = 'Lead';
    public static String CONTACT ='Contact';
    public static String ACCOUNTCONTACT = 'AccountContact';
    public static String OPPORTUNITY ='Opportunity';
    public static String ACCOUNTADDRESSES_TRIGGER='AccountAddressesTrigger'; // Added by Navaneeth
    
     //Contact Constants
    public static String CONTACT_TRIGGER = 'Contact';
        
}