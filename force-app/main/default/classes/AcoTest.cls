/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            AcoTest
   Description:     Test class for   AcoTest
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   31 Jan 2017      1.0           Manikanta Nagubilli                                         Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
@isTest
public class AcoTest
{
    static testmethod void testSampleoperations() 
    {
         Account acc = new Account();
                acc.Name = 'Test Parent Account';
                acc.RecordTypeId = '012b0000000DpIMAA0';
                acc.Type = 'Account Creation';
        insert acc;
        Account acc1 = new Account();
                acc1.Name = 'Test Parent Account';
                acc1.RecordTypeId = '012b0000000DpIMAA0';
                acc1.Type = 'Account Creation';
        insert acc1;
        Contact contactRecord = new Contact(FirstName='TestClassAccount', LastName='TestMyClassAccount',
                                                AccountId=acc1.Id ,Salutation='MR.', Email= 'testyy7@email.com',MobilePhone = '8884888472',
                                                Phone='1112223334', Preferred_Address__c='Mailing Address', MailingCity = 'Newcastle', MailingState='Northumberland', MailingCountry='United Kingdom', MailingStreet='1st Street' , MailingPostalCode='NE28 7BJ',National_Identity_Number__c = '9412055708083' , Passport_Number__c = 'ABCDE12345',Home_Phone__c = '9999',Birthdate__c = Date.newInstance(1900 , 10,10),Ethnic_Origin__c = 'Asian' , Marital_Status__c = 'Single',First_Language__c = 'English');     
        insert contactRecord;
        Id pricebookId = Test.getStandardPricebookId();
        Order ord= new Order();
        ord.AccountId=acc.id;
        ord.Status = 'New';
        ord.Type = 'Sample';
        ord.EffectiveDate = Date.newInstance(2017,01,06);
        ord.ShippingCountry = 'India';
        ord.ShippingCity = 'Bangalore';
        ord.ShippingStreet = 'BNG1';
        ord.ShippingPostalCode = '5600371';
        ord.ShippingState = 'Karnataka';
        ord.Market__c='CA';
        ord.Pricebook2Id=pricebookId;
        ord.Salesperson__c=userinfo.getUserId();
        ord.Status = 'New';
        insert ord;
        
         product2  prod = new product2();
        prod.name='test';
        prod.ISBN__c = '9781292099545';
        prod.Market__c ='US';
        prod.Line_of_Business__c = 'Higher Ed';
        prod.Business_Unit__c = 'US Field Sales';
        insert prod;
        PricebookEntry standardPrice = new PricebookEntry();
        standardPrice.Pricebook2Id = pricebookId;
        standardPrice.Product2Id = prod.Id;
        standardPrice.UnitPrice = 10000;
        standardPrice.IsActive = true;
        insert standardPrice;
        orderitem orderitem = new orderitem();
        orderitem.orderid=ord.id;
        orderitem.Shipped_Product__c = prod.id;
        orderitem.Quantity = 1;
        orderitem.pricebookentryid= standardPrice.id;
        orderitem.unitprice=0;
        orderitem.Shipping_Method__c= 'Ground' ;
        insert orderitem; 
        
        orderitem orderitem1 = new orderitem();
        orderitem1.orderid=ord.id;
        orderitem1.Shipped_Product__c = prod.id;
        orderitem1.Quantity = 1;
        orderitem1.pricebookentryid= standardPrice.id;
        orderitem1.unitprice=0;
        orderitem1.Shipping_Method__c= 'Ground' ;
        insert orderitem1; 
        
        orderitem.Quantity = 2;
        update orderitem; 
        
        delete orderitem;
        
            /*
            AccountContact__c  ac = new AccountContact__c();
            ac.Account__c  = acc.id;
            ac.Contact__c  = contactRecord.id;
            ac.AccountRole__c = 'Educator';
            ac.Role_Detail__c = 'Administrator';
            insert ac;
            AccountContact__c  ac1 = new AccountContact__c();
            ac1.Account__c  = acc.id;
            ac1.Contact__c  = contactRecord.id;
            ac1.AccountRole__c = 'Educator';
            ac1.Role_Detail__c = 'Administrator';
            insert ac1;
        delete ac1;
        undelete ac1;
        
        AccountContact__c  ac2 = new AccountContact__c();
        ac2.Account__c  = acc.id;
        ac2.Contact__c  = contactRecord.id;
        ac2.AccountRole__c = 'Educator';
        ac2.Role_Detail__c = 'Administrator'; 
        insert ac2;
        ac.Role_Detail__c = 'Admissions'; 
        update ac;
        
        List<AccountContact__c> accContList = TestDataFactory.createAccountContact(3, acc.Id, contactRecord.id);    
        insert accContList;
        
        accContList[0].Role_Detail__c = 'Admissions';
        accContList[1].Role_Detail__c = 'Admissions';
        accContList[2].Role_Detail__c = 'Admissions';
        update accContList;
        
        delete accContList;
        
        undelete accContList; */
    }
}