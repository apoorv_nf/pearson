public class AssignLeadsUsingAssignmentRules
{
    @InvocableMethod
    public static void LeadAssign(List<Id> LeadIds)
    {
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule= true;          
        List<Lead> leadlst=[select id from lead where lead.id in :LeadIds];
        for(Lead l:leadlst){
            l.setOptions(dmo);
            l.Run_assignment_rules__c = False;
        }
        
        Database.update(leadlst);
    }
}