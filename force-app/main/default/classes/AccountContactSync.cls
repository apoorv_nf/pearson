/*******************************************************************************************************************
* Apex Class Name  : AccountContactSync
* Version          : 1.0 
* Created Date     : 19 March 2015
* Function         : Class for sync AccountContact custom object data with the standard AccountContactRelation object
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*                      19/03/2015              Created Initial Version of AccountContactSync Class
*  Cristina             5/30/2018               Modified class to work for AccountContactRelation instead of AccountContactRole for the first Lightning Release
                                                Deleting AccounrContacts__c records updates the remaining, and does not delete te relationship.
                                                Commenting out GetAccountContactRelationRecords method because it is not used
*******************************************************************************************************************/
public without sharing class AccountContactSync  
{

    /*************************************************************************************************************
    * Name        : GetAccountContactRelationRecords  
    * Description : Get the corresponding AccountContactRelation standard object records based on AccountContact custom object records
    * Input       : TargetRecords - AccountContact custom object records
    * Output      : AccountContactRelation standard object records
    *************************************************************************************************************/
    /*public Map<ID,AccountContactRelation> GetAccountContactRelationRecords(List<AccountContact__c> TargetRecords)
    {
        // Get the Ids of the synchorinzed AccountContactRelation records
        List<ID> SyncIds = new List<ID>();
        
        for(AccountContact__c TargetRecord :TargetRecords){
            //SyncIds.add(TargetRecord.Synchronized_Account_Contact_Role__c);
            SyncIds.add(TargetRecord.Synchronized_AccountContactRelation__c);
        } 
        
        // Get the synchorinzed AccountContactRelation records
        List<AccountContactRelation> listSyncAccountContactRelationRecords = [SELECT Id, AccountId, ContactId, Roles, IsDirect, IsDeleted FROM AccountContactRelation WHERE Id IN:SyncIds ALL ROWS];
        
        Map<ID,AccountContactRelation> mapSyncAccountContactRelationRecords = new Map<ID,AccountContactRelation>(listSyncAccountContactRelationRecords );
            
        return mapSyncAccountContactRelationRecords;
    }*/
   
    /*************************************************************************************************************
    * Name        : SetAsNotPrimary
    * Description : Set the AccountContact custom records as not primary
    * Input       : TargetRecords - AccountContact custom object records
    * Output      : 06/01/2018 CP Removed because not called from anywhere
    *************************************************************************************************************/
    public void SetAsNotPrimary(List<AccountContact__c> TargetRecords)
    {
       List<AccountContact__c> RecordsToUpdate = new List<AccountContact__c>();
       
       for(AccountContact__c accCon :TargetRecords)
       {
           AccountContact__c accConNotPrimary = accCon;
           accConNotPrimary.Primary__c = false;
           RecordsToUpdate.add(accConNotPrimary);
       }
       
       if(RecordsToUpdate.size()>0)
       {
           update RecordsToUpdate;
       }
    }   
    /*************************************************************************************************************
    * Name        : InsertAccountContactRelationRecords (AccountContactRelation)
    * Description : Insert a record on the AccountContactRelation standard object per each AccountContact custom object record with sync information
    * Input       : NewRecords - AccountContact custom object records
    * Output      : 
    *************************************************************************************************************/
    public void InsertAccountContactRelationRecords(List<AccountContact__c> NewRecords)
    {
        //List<AccountContact__c> OriginalRecords = [SELECT Id, Contact__c, Account__c, AccountRole__c, Primary__c FROM AccountContact__c WHERE Id IN:NewRecords];
        List <AccountContactRelation> newAccountContactRelations = new List<AccountContactRelation>();
        //List <AccountContactRelation> failedAccountContactRelations = new List<AccountContactRelation>();
        List <AccountContact__c> failedACs = new List <AccountCOntact__c>();
        List<ID> NewPrimaryRecordsAccountIds = new List<ID>();
        
        //Loop the AccountContact new records to see which are primary in this insert
        for(AccountContact__c NewRecord :NewRecords)
        {
            //Add the primary AccountContact Account information to the collection of Account Ids
            if(NewRecord.Primary__c)
            {    
                NewPrimaryRecordsAccountIds.add(NewRecord.Account__c);
            }
        }    
        //Check if we have primary ones existing on the , if we do update them to not Primary 
        List<AccountContact__c> ExistingPrimaryRecords = [SELECT Id, Account__c, Contact__c, AccountRole__c, Primary__c FROM AccountContact__c WHERE Id NOT IN:NewRecords AND Account__c IN:NewPrimaryRecordsAccountIds AND Primary__c = true];
        
        if(ExistingPrimaryRecords.size()>0)
        {
            SetAsNotPrimary(ExistingPrimaryRecords);
        }
            
        //WE COULD SUBSTITUTE THE NEXT LINES OF CODE WITH THE  UpdateAccountContactRelationRecords BECAUSE WHEN A NEW CONTACT IS CREATED A RELATION RECORD GETS CREATED AUTOMATICALLY
        for(AccountContact__c OriginalRecord :NewRecords)
        {
            AccountContactRelation newACR = new AccountContactRelation();
            newACR.AccountId = OriginalRecord.Account__c;
            newACR.ContactId = OriginalRecord.Contact__c;
            newACR.Roles = OriginalRecord.AccountRole__c;
            
            
            newAccountContactRelations.add(newACR);
        }
        //We try inserting if fail then we need to look for existing record and update the role
        Database.SaveResult[] SR = Database.insert(newAccountContactRelations,false);
        
        
        for (Integer i=0;i<SR.size();i++){
            if(!SR[i].isSuccess()){
                AccountContact__c existingAC = new AccountContact__c();
                existingAC.Account__c = newAccountContactRelations[i].AccountId;
                existingAC.Contact__c = newAccountContactRelations[i].ContactId;
                existingAC.AccountRole__c = newAccountContactRelations[i].Roles;
                failedACs.add(existingAC);
                //failedAccountContactRelations.add(newAccountContactRelations[i]);
            }
        }
        //Now I update all the current failedrecords
        if (failedACs.size() >0){
             UpdateAccountContactRelationRecords(failedACs);
        }
    }
       
       
    /*************************************************************************************************************
    * Name        : UpdateAccountContactRelationRecords
    * Description : Update the records on the AccountContactRelation standard object to be sync with the AccountContact custom object records
    * Input       : UpdatedRecords - AccountContact custom object records
    * Output      : 
    *************************************************************************************************************/
    public void UpdateAccountContactRelationRecords(List<AccountContact__c> UpdatedRecords)
    {
        List <AccountContactRelation> lstACRToUpdate = new List <AccountContactRelation>();
        List <AccountContactRelation> allACRs = new List <AccountContactRelation>();
        //Map <ID, Set<AccountContact__c>> allACsMap = new  Map <ID, Set<AccountContact__c>>();
        //Map <ID, AccountContactRelation> allACR = new Map<ID,AccountContactRelation>();
        
        Set<Id> setAccountIds = new Set<Id>();
        Set<Id> setContactIds = new Set<Id>();
       
        for(AccountContact__c ACRToUpdate :UpdatedRecords)
        {
            setAccountIds.add(ACRToUpdate.Account__c); 
            setContactIds.add(ACRToUpdate.Contact__c); 
        }
            ID NewAct;
            ID LastAct;
            ID NewCtc;
            ID LastCtC;
            String lastRoles = '';
            Integer i = 0;
        // get all the existing AccountContacts records so we can concatenate the roles and create possible relations to update
        for(AccountContact__c  allACs : [select id,Account__c,Contact__c, accountrole__c from AccountContact__c where Account__c IN: setAccountIds And Contact__c IN: setContactIds order by Account__c,Contact__c limit 1000])
        {
            NewAct = allACs.Account__c;
            NewCtc = allACs.Contact__c;
            AccountContactRelation tempACR;
            //first row
            if  (NewAct != LastAct && LastAct == null){
                    
                    tempACR = new AccountContactRelation();
                    tempACR.AccountId= AllACs.Account__c;
                    tempACR.ContactId = AllACs.Contact__c;
                    tempACR.Roles = AllACs.AccountRole__c;
                    AllACRs.add(tempACR);
                
            }
            else if (newAct != LastAct && LastAct != null){
                
                 
                    tempACR = new AccountContactRelation();
                    tempACR.AccountId= AllACs.Account__c;
                    tempACR.ContactId = AllACs.Contact__c;
                    tempACR.Roles = AllACs.AccountRole__c;
                    AllACRs.add(tempACR);
            }
            else if (NewAct == LastAct){
                if (newCtc == LastCtc){
                     //Add role to existing AC
                    tempACR = AllACRs.remove(i-1);
                    tempACR.Roles = tempACR.Roles +';'+ AllACs.AccountRole__c;
                    AllACRs.add(tempACR);
                    i=i-1;
                } else{
                    
                    tempACR = new AccountContactRelation();
                    tempACR.AccountId= AllACs.Account__c;
                    tempACR.ContactId = AllACs.Contact__c;
                    tempACR.Roles = AllACs.AccountRole__c;
                    AllACRs.add(tempACR);
                }
                
            }
            //AllACRs.add(tempACR);
            LastAct=NewAct;
            LastCtc=NewCtc;
            i++;
        }
        
        //Get List of Account Contacts Relations from Account=UpdateRecords.account and Contact=UpdatedRecords.contact
        for(AccountContactRelation  uniqueACR : [select id,AccountId,ContactId, Roles from AccountContactRelation where AccountId IN: setAccountIds And ContactId IN: setContactIds order by Accountid,Contactid limit 1000])
        {
            //loop thru the ACR with Roles appended
            for (AccountContactRelation ACR : AllACRs){
                //if the relation matches update the roles to the new value
                if(uniqueACR.AccountId == ACR.AccountId && uniqueACR.ContactId == ACR.ContactId){
                    uniqueACR.Roles = ACR.Roles;
                    lstACRToUpdate.add(uniqueACR);
                    
                }
            }
        }
        if (lstACRToUpdate.size()>0){
            update lstACRToUpdate;
        }
        
    }
    
    /*************************************************************************************************************
    * Name        : DeleteAccountContactRelationRecords
    * Description : Delete the records on the AccountContactRelation standard object to be sync with the AccountContact custom object records
    * Input       : DeletedRecords - AccountContact custom object records
    * Output      : 
    *************************************************************************************************************/
   public void DeleteAccountContactRelationRecords(List<AccountContact__c> DeletedRecords)
    {
        List<AccountContact__c> relToDelete = new List<AccountContact__c>();
        List<AccountContactRelation> lstRelToDelete = new List<AccountContactRelation>();
        Set<Id> setAccountIds = new Set<Id>();
        Set<Id> setContactIds = new Set<Id>();
        for(AccountContact__c ACRToDelete :DeletedRecords)
        {
            setAccountIds.add(ACRToDelete.Account__c); 
            setContactIds.add(ACRToDelete.Contact__c); 
        }
        //we create a list of AccountContact that still exist in AccountContacts__c so we update them instead of delete
        List<AccountContact__c> existingAccounts = new List<AccountContact__c>();
        for(AccountContact__c existingAC:[select id,Account__c,Contact__c, accountrole__c from AccountContact__c where Account__c IN: setAccountIds And Contact__c IN: setContactIds order by Account__c,Contact__c limit 1000]){
            existingAccounts.add(existingAC);
        }
        //update the ones that exist
        if (existingAccounts.size() >0){
            //we had remaining records to maintain so we update
            UpdateAccountContactRelationRecords(existingAccounts);
        }
        // NO NEED TO DELETE, USERS SHOULD DEACTIVATE THE RECORD, BUT NOT DELETE
     }     
    /*************************************************************************************************************
    * Name        : DeleteCourseContactRecords
    * Description : Check for the duplicate accountContacts exists or not. If exists do the below actions
                    Delete the records on the CourseContact object to be sync with the AccountContact custom object records
                    Deactivate the PIU's related to the contact
    * Input       : DeletedRecords - AccountContact custom object records
    * Output      : 
    *************************************************************************************************************/
    public void DeleteCourseContactRecords(List<AccountContact__c> DeletedRecords)
    {
       set<Id> accountIds = new Set<Id>();
        set<Id> contactIds = new Set<Id>();
        List<AccountContact__c> accConsList = new List<AccountContact__c>();
        Boolean isDuplicate = false;
        List<UniversityCourseContact__c> toBeDeleted = new List<UniversityCourseContact__c>();
               
        for(AccountContact__c accCon : DeletedRecords){
            accountIds.add(accCon.account__c);  
            contactIds.add(accCon.contact__c);  
        }
        
    //fetch all the account Contacts under the account
      accConsList = [select id, name, account__c, contact__c from accountContact__c where account__c In : accountIds and isdeleted=false];
    //check for duplicate accountContact
  if(!accConsList.isEmpty()){
    for(AccountContact__c accConDel : DeletedRecords){
    for(AccountContact__c accCon : accConsList){
        if(accConDel.account__c == accCon.account__c && accConDel.contact__c == accCon.contact__c){
            isduplicate = true;
             }
                       
        }
       }
    }
    
    //if there is no duplicate accountContact then only decativate the PIU and delete the course Contact
    if(!isduplicate){    
     List<Asset> assetList = new List<Asset>();
     assetList = [Select id, name, contactId, accountId from Asset where ContactId In : contactIds and status__c = 'Active'];
    if(!assetList.isEmpty()){
    List<Asset> toBeInactivePIUs = new List<Asset>();    
    for(AccountContact__c accCon : DeletedRecords){
        for(Asset ast : assetList){         
            if(accCon.contact__c == ast.ContactId && accCon.account__c == ast.accountId){
                ast.status__c = 'Inactive';
                toBeInactivePIUs.add(ast);
            }
        
        }
    }
   if(!toBeInactivePIUs.isEmpty())
    update toBeInactivePIUs;
    }
    
    List<UniversityCourse__C> univCourseList = new List<UniversityCourse__C>();
        univCourseList = [Select id,name,account__c,(select id,Contact__c from University_Course_Contacts__r where isdeleted = false) from UniversityCourse__c where account__c in : accountIds];
        for (UniversityCourse__c temp : univCourseList) {
        for(AccountContact__c accCon : DeletedRecords){
            for (UniversityCourseContact__c tempucc : temp.University_Course_Contacts__r) {
                if(accCon.contact__c == tempucc.contact__c)
                 toBeDeleted.add(tempucc);
                
                }
            } 
         }
        if(!toBeDeleted.isEmpty())
        Delete toBeDeleted;

   }        
                 
    }

    
    /*************************************************************************************************************
    * Name        : UndeleteAccountContactRelationRecords
    * Description : Recreates the records on the AccountContactRelation standard object to be sync with the AccountContact custom object records
    * Input       : UndeletedRecords- AccountContact custom object records
    * Output      : 
    *************************************************************************************************************/
    public void UndeleteAccountContactRelationRecords(List<AccountContact__c> UndeletedRecords)
    {
        //Recreate the AccountContactRelation records and updates the AccountContact undeleted records with the new reference
        InsertAccountContactRelationRecords(UndeletedRecords);
    }
     /*************************************************************************************************************
    * Name        : CheckMultipleAccountContactPrimaries
    * Description : Check if the list contains more that one record for the same account is set as primary
    * Input       : TargetRecords - AccountContact custom object records
    * Output      : 
    *************************************************************************************************************/
    /*public void CheckMultipleAccountContactPrimaries(List<AccountContact__c> TargetRecords)
    {
        
        //Group the AccountContact records set as primary by Account
        Map<ID, List<AccountContact__c>> mapTargetRecordsPrimaryMarkedByAccount = new Map<ID, List<AccountContact__c>>();
        
        for(AccountContact__c accCon :TargetRecords)
        {
            if(accCon.Primary__c)
            {
                if(mapTargetRecordsPrimaryMarkedByAccount.containsKey(accCon.Account__c))
                {
                    List<AccountContact__c> tempListAccCon = new List<AccountContact__c>(mapTargetRecordsPrimaryMarkedByAccount.get(accCon.Account__c));
                    tempListAccCon.add(accCon);
                    mapTargetRecordsPrimaryMarkedByAccount.put(accCon.Account__c,tempListAccCon);
                }
                else{
                
                    List<AccountContact__c> newListAccCon = new List<AccountContact__c>();
                    newListAccCon.add(accCon);
                    mapTargetRecordsPrimaryMarkedByAccount.put(accCon.Account__c,newListAccCon);
                }
            }
        }
        
        //Check if there are multiple AccountContact records set as primary for the same account
        List<ID> ListAccountIDs = new List<ID>(mapTargetRecordsPrimaryMarkedByAccount.keySet());
        
        for(ID accId :ListAccountIDs)
        {
            if(mapTargetRecordsPrimaryMarkedByAccount.get(accId).size()>1)
            {
                for(AccountContact__c accConWithError :mapTargetRecordsPrimaryMarkedByAccount.get(accId))
                {
                    accConWithError.addError('More than one contact is set as primary');
                }
            }
        }
        
    }*/
    
}