@isTest
private class MDR_Contact_Trigger_Test {
    
    @isTest static void TestDeleteMDR_Contact() {
        
        MDR_Contact__c mdrCtc = new MDR_Contact__c(DATAHUB_KEY__c='MDR__CUSTOMER|$|01561813|$|23999',DEPT_PID__c='10046450',JOB1__c='AG0015',
                                                   FIRST__c='Sammy',email__c='sammy@example.com',NID__c='23999',PID__c='1561813');
        insert mdrCtc;
        
             
        // Perform test
        Test.startTest();
            
        Database.DeleteResult result = Database.delete(mdrCtc, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have published a event,
        // so verify 
        System.assert(result.isSuccess());
        System.assert(result.getErrors().size()<=0);
    }
}