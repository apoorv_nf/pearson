/**********************************************************************************************************

* Apex Class Name : SampleOrderContacts
* Version             : 1.0 
* Created Date        : 22/07/2016 
* Function            :  wrapper class to display contact while creating sample orders
* Modification Log    :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------

       Rony Joseph                   22/07/2016             wrapper class to display contact while creating sample orders
	   Gayatri Keshkamat		     13/12/2016				Added comments to the class and methods	
***********************************************************************************************************/

public class SampleOrderContacts{
        
        public Boolean ischecked{get;set;}
        public Date futureShipDate{get;set;}                                
        public String shippingInst{get;set;}                                
        public String address{get;set;}
        public String shippingMethod{get;set;}          
        public OpportunityContactRole optyCont{get;set;}
        public Contact cont{get;set;}
        public String addressOption{get;set;}
        public Boolean isContMailingAdd{get;set;}
        public Boolean isContOtherAdd{get;set;}
        public Boolean isAccPhysAdd{get;set;}
        public Boolean isCustomAdd{get;set;}
        public String addLine1{get;set;}
        public String addLine2{get;set;}
        public String addLine3{get;set;}
        public String addLine4{get;set;}
        public String addCity{get;set;}
        public String addPostalCode{get;set;}
        public String purchaseOrderNum{get;set;}
        public Order tempOrder {get; set;} 
        private string oppmarket;
        private string oppbu;
        private string opplob; 
        
    	/*** Description : method used to get shipping method options for particular market, LOB and BU.
		* @param NA
		* @return List<SelectOption> containing shipping methods.
		* @throws NA
		**/ 
        public List<SelectOption> getmyshippingOptions( ){
            List<SelectOption> options = new List<SelectOption>();
            try{
                list<Sample_Order_Page_Behaviour__mdt> shippingmethodlist = [select Available_Shipping_Methods__c,oppty_market__c,Oppty_BU__c,Oppty_LOB__c from Sample_Order_Page_Behaviour__mdt where  oppty_market__c = :oppmarket and Oppty_LOB__c = :opplob and Oppty_BU__c = :oppbu limit 1 ];
                system.debug('value'+shippingmethodlist);
                if(!shippingmethodlist.isempty() &&( shippingmethodlist[0].Available_Shipping_Methods__c != ''&&shippingmethodlist[0].Available_Shipping_Methods__c!=null))
                {
                    list<string> shippingmethods = String.valueof(shippingmethodlist[0].Available_Shipping_Methods__c).split(',',0);
                    if(shippingmethods.size()>0)
                    {
                        for (string method:shippingmethods)
                        {options.add(new SelectOption(string.valueof(method),string.valueof(method)));}
                    }
                }
                return options;
            }catch(exception e){system.debug('Error'+e);options.add(new SelectOption('',''));return options;} 
        }  
        /*** Description : Constructor of the class
			* @param OpportunityContactRole optyCont, Contact cont, String prefAdd,string market,string lob,string bu
			* @return NA
			* @throws NA
			* 
			**/   
        public SampleOrdercontacts(OpportunityContactRole optyCont, Contact cont, String prefAdd,string market,string lob,string bu){
            ischecked = false;
            address = prefAdd;
            this.optyCont = optyCont;
            this.cont = cont;
            shippingMethod = Label.PS_CreateSampleOrderShippingMethod;
            addLine1 = '';
            addLine2 = '';
            addLine3 = '';
            addLine4 = '';
            addCity = '';
            addPostalCode = '';
            purchaseOrderNum = '';
            tempOrder = new Order();
            oppmarket = market;
            opplob=lob;
            oppbu=bu;
            addressOption=Label.PS_CreateSampleOrderAccountAddress;
          }
    }