/*
KP:2/19/2016: Added criteria for R1 class to bypass for R4
*/
public with sharing class CloseCase {
    
    public static void cannotCloseCase(List<Case> caseList){
        
        Map<id,Case> case_IdMap = new Map<Id,Case>();
        List<Action__c> actions = new List<Action__c> ();
        for(Case c : caseList){
            if(c.Status == 'Closed' && c.id != null)//KP Added condition on id not null
            	case_IdMap.put(c.id,c);
        }
        //system.debug('####################cases :'+case_IdMap);
        if (case_IdMap.size()>0){ //KP Added condition on collection size > 0
        actions = [SELECT Case__r.id,status__c,case__r.recordtype.name FROM Action__c
                                    WHERE case__r.id IN :case_IdMap.keySet()];
        //system.debug('####################Actions :'+actions);
        if (actions.size()> 0){
        for(Action__c act : actions){
        	//KP:Added recordtype
            if(!(act.Status__c == 'Completed') && !act.case__r.recordtype.name.contains(Label.PS_ServiceRecordTypes)){
                //system.debug('#####################cant close case, NonCompleted action');
                case_IdMap.get(act.Case__r.id).addError(System.Label.ErrorMsgCloseCase);
            }
        }
        }
        }    
    }

}