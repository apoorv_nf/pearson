/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_ClearMyCacheController
   Description:     Controller class for PS_ClearMyCache VF Page - Basically, clears user cache which is used for product filter
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   01 Dec 2015      1.0           Accenture NDC                                         Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class PS_ClearMyCacheController
{
    public String configRequestId;
    public String tempObjectId;
    public String flow;
    public pageReference clearMyCache()
    {
        configRequestId = apexpages.currentpage().getparameters().get('configRequestId');
        tempObjectId = apexpages.currentpage().getparameters().get('Id');
        flow = apexpages.currentpage().getparameters().get('flow');
        pageReference redirectToCart = new pageReference('/apex/apttus-config2__SelectConfigProductsFilterView?configRequestId='+configRequestId+'&id='+tempObjectId+'&flow='+flow);
        system.debug(redirectToCart);
        if(Cache.Session.contains(UserInfo.getUserId()+'selectedFamilyGuidedSelling'))
        {
            Cache.Session.remove(UserInfo.getUserId()+'selectedFamilyGuidedSelling');
        }
        return redirectToCart;
    }
}