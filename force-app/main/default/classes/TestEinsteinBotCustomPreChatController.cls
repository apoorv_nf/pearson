/*
 * Author: Matt Mitchener (Neuraflash)
 * Date: 7/16/2018
 */
@isTest
private class TestEinsteinBotCustomPreChatController {

	@isTest
	static void it_should_get_logged_in_user() {
		EinsteinBotCustomPreChatController.LoggedInUserDetails loggedInUser = EinsteinBotCustomPreChatController.getLoggedInUser();

		System.assertEquals(UserInfo.getUserId(), loggedInUser.userId);
	}

	@isTest
	static void it_should_get_contact() {
		Contact contact = new Contact(
			FirstName = 'Rick',
			LastName = 'Sanchez',
			Email = 'rick.sanchez@email.com'
		);
		insert contact;

		Contact returnedContact = EinsteinBotCustomPreChatController.getContact('rick.sanchez@email.com');

		System.assertEquals(contact.LastName, returnedContact.LastName);
	}

	@isTest
	static void it_should_return_null_if_no_contact_exists() {
		Contact returnedContact = EinsteinBotCustomPreChatController.getContact('rick.sanchez@email.com');

		System.assertEquals(null, returnedContact.LastName);
	}
}