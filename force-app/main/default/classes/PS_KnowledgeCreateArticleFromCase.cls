/***********************************************************************
Author: Chad Lieberman & Kameswari Padmanaban
Created Date: 10/26/2015
CL    1.0    Created to map case fields to customer support articles                             

************************************************************************/

public class PS_KnowledgeCreateArticleFromCase{
      
  /**
     * Description : Constructor to initialize article type and case 
     * @param NA
     * @return NA
     * @throws NA
    **/

   public PS_KnowledgeCreateArticleFromCase(ApexPages.KnowledgeArticleVersionStandardController ctl) {
        //Customer_Support__kav article = (Customer_Support__kav) ctl.getRecord();       
        SObject article = ctl.getRecord();                                 
        String sourceId = ctl.getSourceId(); 
        Case c = [select Id,Environment__c,Description, Cause__c, Resolution_Steps__c, Geography__c,PS_Business_Vertical__c, 
                  Subject, Request_Type__c,Contact_Type__c from Case where id=:sourceId];
        if (article instanceof Customer_Support__kav)
            mapCasewithArticle(c, (Customer_Support__kav) article,ctl);        
    }
       
    /**
     * Description : Method definition to map case fields to Customer Support Article fields 
     * @param NA
     * @return NA
     * @throws NA
    **/
    
    public void mapCasewithArticle(Case c, Customer_Support__kav cs,ApexPages.KnowledgeArticleVersionStandardController ctl) {
        SObject article1 = ctl.getRecord();  
        System.debug('Mapping Article with case');
        cs.put('Issue__c',c.Description);  
        cs.put('Environment__c',c.Environment__c);  
        cs.put('Cause__c',c.Cause__c);
        cs.put('Resolution__c',c.Resolution_Steps__c);
        cs.put('Geography__c',c.Geography__c);
        cs.put('Business_Vertical__c',c.PS_Business_Vertical__c);
        cs.put('Requesttype__c',c.Request_Type__c);
        Map<String, PS_DataCategory__c> mapdata = PS_DataCategory__c.getAll();        
        
    /**
     * Description : Autoassignment of data category on create. 
     * @param NA
     * @return NA
     * @throws NA
    **/ 
       
        if (c.Geography__c != '' && c.Geography__c != null){           
                if (mapdata.containsKey('Geo_'+c.Geography__c))                         
                ctl.selectDataCategory('Geography',mapdata.get('Geo_'+c.Geography__c).Value__c);            
        }
        
        if (c.Contact_Type__c != '' && c.Contact_Type__c != null){       
                if (mapdata.containsKey('R_'+c.Contact_Type__c))                        
                ctl.selectDataCategory('Role',mapdata.get('R_'+c.Contact_Type__c).Value__c);            
        }
        
        if (c.PS_Business_Vertical__c != '' && c.PS_Business_Vertical__c != null){          
           if (mapdata.containsKey('LOB_'+c.PS_Business_Vertical__c))                         
                ctl.selectDataCategory('Business_Vertical',mapdata.get('LOB_'+c.PS_Business_Vertical__c).Value__c);           
        } 
        
        if (c.Request_Type__c != '' && c.Request_Type__c != null){          
           if (mapdata.containsKey('RT_'+c.Request_Type__c))                         
                ctl.selectDataCategory('Request_Type',mapdata.get('RT_'+c.Request_Type__c).Value__c);           
        }                 
    }    
}