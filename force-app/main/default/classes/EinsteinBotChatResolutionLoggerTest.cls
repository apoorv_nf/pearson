@isTest
public with sharing class EinsteinBotChatResolutionLoggerTest {
    @IsTest
    static void testBehavior() {

        EinsteinBotChatResolutionLogger.ChatLoggerRequest clr = new EinsteinBotChatResolutionLogger.ChatLoggerRequest();
        clr.liveAgentSessionId='test123';
        clr.resolvedTrue= 'Yes';

        Test.startTest();
        EinsteinBotChatResolutionLogger.logResolutionSuccess(new List<EinsteinBotChatResolutionLogger.ChatLoggerRequest>{
                clr
        });
        clr.resolvedTrue= 'No';
        EinsteinBotChatResolutionLogger.logResolutionSuccess(new List<EinsteinBotChatResolutionLogger.ChatLoggerRequest>{
                clr
        });
        EinsteinBotChatResolutionLogger.logResolutionSuccess(new List<EinsteinBotChatResolutionLogger.ChatLoggerRequest>{
                clr
        });
        Test.stopTest(); 
    }

}