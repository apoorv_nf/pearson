@isTest
private class PS_OpptyPrimaryPearsonCourseTest
{
     
    static testMethod void myUnitTest()
    {
      //code for creating an User
     //query to get the profile of sales user OneCRM
     Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
     
      //code for creating an User
      User u = new User();
      u.LastName = 'territoryuser';
      u.alias = 'terrusr'; 
      u.Email = 'territoryuser@gmail.com';  
      u.Username='territoryuser@gmail.com';
      u.LanguageLocaleKey='en_US'; 
      u.TimeZoneSidKey='America/New_York';
      u.Price_List__c='Humanities & Social Science';
      u.LocaleSidKey='en_US';
      u.EmailEncodingKey='ISO-8859-1';
      u.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
      u.Geography__c = 'Growth';
      u.Market__c = 'US';
      u.Business_Unit__c = 'US Field Sales';
      u.Line_of_Business__c = 'Higher Ed';
      u.isactive=true;
      u.CurrencyIsoCode='USD';
      u.License_Pools__c='Test User';
      insert u;       
      
      System.runAs(u){
          List<Account> Acc = TestDataFactory.createAccount(1, 'Organisation');
          insert Acc;  
          
          List<Opportunity> Opp = TestDataFactory.createOpportunity(1, 'B2B');
          Opp[0].AccountId = Acc[0].id;
          insert Opp;  
          
          //code for creating the Hirarchy
          /*Apttus_Config2__ClassificationName__c cn = new Apttus_Config2__ClassificationName__c();
          cn.Apttus_Config2__HierarchyLabel__c = 'TestcategoryName';
          cn.Apttus_Config2__Type__c = 'Offering';
          cn.Apttus_Config2__Active__c = true;
          cn.CurrencyIsoCode = 'USD';
          cn.Market__c = 'US';
          cn.Name = 'TestcategoryName';*/
          ProductCategory__c cn= new ProductCategory__c(Name='Professional & Career Services',HierarchyLabel__c='Professional & Career Services');
          insert cn;
          
          
          //code for inserting Category Hierarchy 
          /*Apttus_Config2__ClassificationHierarchy__c categoryhierarchy = new Apttus_Config2__ClassificationHierarchy__c();
          categoryhierarchy.Apttus_Config2__HierarchyId__c = cn.id; 
          categoryhierarchy.Apttus_Config2__Label__c = 'TestCategoryHierarchy';
          categoryhierarchy.Name = 'TestCategoryHierarchy';
          categoryhierarchy.Apttus_Config2__AncestorId__c = categoryhierarchy.id;
          categoryhierarchy.Apttus_Config2__PrimordialId__c = categoryhierarchy.id;
          categoryhierarchy.Region__c='US';
          categoryhierarchy.CurrencyISOCode = 'USD';*/
          Hierarchy__c categoryhierarchy= new Hierarchy__c(Name ='testcategory',ProductCategory__c=cn.id,Label__c='testlabel');
          insert categoryhierarchy; 
          
          //code for getting the records in non-pttus tables
         /* List <ProductCategory__c> mappedCat = [SELECT id FROM ProductCategory__c WHERE Category_ExternalID__c = :cn.id];
          
          Hierarchy__c mappedhier = new Hierarchy__c();
          mappedhier.CategoryHierarchy_ExternalID__c = categoryhierarchy.id;
          mappedhier.ProductCategory__c = mappedCat.get(0).id;
          mappedhier.Label__c = 'Test_Hierarchy';
          insert mappedhier;
          */
          //code for mapping Pearson Course with the category Hirarchy
          Opportunity_Pearson_Course_Code__c  pce = new  Opportunity_Pearson_Course_Code__c();
          pce.Active__c = true;   
          pce.Opportunity__c = Opp[0].id;
          pce.Primary__c = True;
          pce.Pearson_Course_Code_Hierarchy__c= categoryhierarchy.id;
          Insert pce;   
          
          List <Hierarchy__c> hier =  [SELECT id FROM Hierarchy__c WHERE CategoryHierarchy_ExternalID__c = :categoryhierarchy.id];
          System.debug('list size : ' + hier.size());
          
          //code for mapping Pearson Course with the category Hirarchy
          Opportunity_Pearson_Course_Code__c  pce1 = new  Opportunity_Pearson_Course_Code__c();
          pce1.Active__c = false;   
          pce1.Opportunity__c = Opp[0].id;
          pce1.Primary__c = false;
          pce1.Pearson_Course_Code_Hierarchy__c= categoryhierarchy.id;
          //pce1.Pearson_Course_Code_Hierarchy__c = hier.get(0).id;
          Insert pce1;
          
          //code for Updating Pearson Course with the category Hirarchy
          Opportunity_Pearson_Course_Code__c  upce = new  Opportunity_Pearson_Course_Code__c();
          upce.id = pce1.id;  
          upce.Primary__c = True;
          update upce; 
      }        
        
    }
}