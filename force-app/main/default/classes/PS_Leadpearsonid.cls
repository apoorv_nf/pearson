/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:           PS_Leadpearsonid 
   Description:    used to generate PearsonID On Lead record from Pearson Lead Id Generator Tab
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  25/05/2016         1.0            Karthik.A.S                       Initial Release 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */

Public class PS_Leadpearsonid{

public integer count;

List<lead> leadLst = new List<lead>();

    public PS_Leadpearsonid(){
        leadLst = [select id,Pearson_ID_Number__c from lead where createdById =: UserInfo.getUserId() and Pearson_ID_Number__c = '' limit 200];
    }
    
    public Integer getCount(){
        count = leadLst.size();
        return count;
    }
    
    
    public void updateSiteId(){
     if(leadlst.size()>0){
    PS_Lead_UpdateStudentId.beforelead1(leadLst);
        }
        
        
    }
}