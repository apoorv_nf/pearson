@isTest
private class TestOpportunityCourseTrigger {

    public static testMethod void myUnitTest() {
        Test.startTest();
        // TO DO: implement unit test
        //create the account
        String AcctRecordTypeId;
        String DiscRecordTypeId;
        String ClusterRecordTypeId;
        String OptyRecordTypeId;                        
        
        List<RecordType> RecordTypeList = new List<RecordType>([select Id,Name,SobjectType from RecordType]);
        
        for (RecordType i : RecordTypeList) {
          if (i.Name == 'Bookstore') {
            AcctRecordTypeId = i.Id;
          }
          if (i.Name == '3) Discipline') {
            DiscRecordTypeId = i.Id;
          }
          if (i.Name == '2) Cluster') {
            ClusterRecordTypeId = i.Id;
          }
          if (i.SobjectType== 'Opportunity') {
            OptyRecordTypeId = i.Id;
          }                    
        }
        
        Account acct = new Account (name = 'Test1', BillingStreet = 'Street1',
                   BillingCity = 'London', BillingPostalCode = 'ABC DEF', BillingCountry = 'Australia',
                  //Vista_Account_Number__c = 'xyz',
                  RecordTypeId= AcctRecordTypeId); //'012E0000000Ll83');
        insert acct;
        
        //link account to universtiy course
        UniversityCourse__c univcrs = new UniversityCourse__c (Account__c = acct.Id, name = 'acct mgmt1');
        insert univcrs;
           
        //create pearson course structures
        PearsonCourseStructure__c disc = new PearsonCourseStructure__c (Name = 'Accounting & Taxation',
          Active_Indicator__c = True,Type__c = 'Discipline'); //,RecordTypeId = DiscRecordTypeId);
        insert disc;
        
        
        PearsonCourseStructure__c cluster = new PearsonCourseStructure__c (Name = 'Financial Accounting',
          Active_Indicator__c = True,Type__c = 'Cluster',Parent_Pearson_Course_Structure__c = disc.Id);
            //RecordTypeId = ClusterRecordTypeId );
        insert cluster;

        PearsonCourseStructure__c cluster2 = new PearsonCourseStructure__c (Name = 'Business Accounting',
          Active_Indicator__c = True,Type__c = 'Cluster',Parent_Pearson_Course_Structure__c = disc.Id);
          //RecordTypeId = ClusterRecordTypeId );
        insert cluster2;

        //associate university course to pearson course structures
        UniversityCoursePearsonCourseStructure__c univcrspearsoncrs = 
          new UniversityCoursePearsonCourseStructure__c (PearsonCourseStructure__c = cluster.Id,
            UniversityCourse__c = univcrs.Id);
        insert univcrspearsoncrs;

        UniversityCoursePearsonCourseStructure__c univcrspearsoncrs2 = 
          new UniversityCoursePearsonCourseStructure__c (PearsonCourseStructure__c = cluster2.Id,
            UniversityCourse__c = univcrs.Id);
           insert univcrspearsoncrs2;
              
        //create opportunity for account
        Opportunity opty_1 = new Opportunity (name = 'Test Opty1',Account = acct, StageName = 'Identification',
          CloseDate = System.Today(),RecordTypeId = OptyRecordTypeId );
        insert opty_1;
        
        Opportunity opty_2 = new Opportunity (name = 'Test Opty2',Account = acct, StageName = 'Identification',
          CloseDate = System.Today(),RecordTypeId = OptyRecordTypeId );
        insert opty_2;
           
        List<ProductCategory__c> listWithCategory = new List<ProductCategory__c>();
        ProductCategory__c newCategory = new ProductCategory__c();
        newCategory.HierarchyLabel__c= 'Professional & Career Services';
        newCategory.Name = 'Professional & Career Services';
        listWithCategory.add(newCategory);
        insert listWithCategory;
        
        List<Hierarchy__c> listWithHierarchy = new List<Hierarchy__c>();
        Hierarchy__c newHierarchy = new Hierarchy__c();
        newHierarchy.ProductCategory__c=  listWithCategory[0].Id;
        newHierarchy.Label__c= '(BIM) Building Information Modeling';
        newHierarchy.Name = '(BIM) Building Information Modeling';
        newHierarchy.Type__c = 'Course';
        listWithHierarchy.add(newHierarchy );
        insert listWithHierarchy ;
        
        List<Opportunity_Pearson_Course_Code__c> listWithOpportunityPearsonCourseCode2 = new List<Opportunity_Pearson_Course_Code__c>();
        Opportunity_Pearson_Course_Code__c newOpportunityPearsonCourseCode2 = new Opportunity_Pearson_Course_Code__c();
        newOpportunityPearsonCourseCode2.Opportunity__c = opty_2.Id;
        newOpportunityPearsonCourseCode2.Pearson_Course_Code_Hierarchy__c= listWithHierarchy[0].Id;
        newOpportunityPearsonCourseCode2.Primary__c = true;
        listWithOpportunityPearsonCourseCode2.add(newOpportunityPearsonCourseCode2);
        insert listWithOpportunityPearsonCourseCode2;
        
                
        List<Pearson_Course_Equivalent__c> courseEqv_create = new List<Pearson_Course_Equivalent__c>();
        Pearson_Course_Equivalent__c pce = new Pearson_Course_Equivalent__c();
        pce.Active__c = true;  
        pce.Primary__c = true;
        pce.Pearson_Course_Code_Hierarchy__c= listWithHierarchy[0].Id;
        pce.Course__c = univcrs.Id;
        courseEqv_create.add(pce); 
        insert courseEqv_create;
        
        //associate opportunity to university course
        List<OpportunityUniversityCourse__c> optyunivcrs = new List<OpportunityUniversityCourse__c>();
        OpportunityUniversityCourse__c opptyUniv = new OpportunityUniversityCourse__c();
        
        opptyUniv.Account__c=acct.id;
        opptyUniv.Close_Date__c = system.today()+7;
        opptyUniv.Opportunity_Name__c='Test';
        opptyUniv.Opportunity_University_Course_Amount__c=100.00;
        opptyUniv.Opportunity__c = opty_1.Id;
        opptyUniv.UniversityCourse__c = univcrs.Id;
        optyunivcrs.add(opptyUniv);
        
        insert optyunivcrs;
        
        //Validate that opportunity is linked to the pearson course structure associated to the university course
        OpportunityPearsonCourseStructure__c optypearsoncrs = [select Opportunity__c,Pearson_Course_Structure__c
          from OpportunityPearsonCourseStructure__c where Opportunity__c = :opty_1.Id
          and Pearson_Course_Structure__c = :cluster.Id];
        
        System.assertEquals (optypearsoncrs.Pearson_Course_Structure__c, cluster.Id);
        
        OpportunityPearsonCourseStructure__c optypearsoncrs2 = [select Opportunity__c,Pearson_Course_Structure__c
          from OpportunityPearsonCourseStructure__c where Opportunity__c = :opty_1.Id
          and Pearson_Course_Structure__c = :cluster2.Id];
        
        System.assertEquals (optypearsoncrs2.Pearson_Course_Structure__c, cluster2.Id);

        //associate opportunity to university course again
        //OpportunityUniversityCourse__c optyunivcrs_1 = new OpportunityUniversityCourse__c (
       //  Opportunity__c = opty_1.Id,UniversityCourse__c = univcrs.Id);
        //insert optyunivcrs_1;
        
        //Validate that opportunity is linked to the pearson course structure associated to the university course
        OpportunityPearsonCourseStructure__c optypearsoncrs3 = [select Opportunity__c,Pearson_Course_Structure__c
          from OpportunityPearsonCourseStructure__c where Opportunity__c = :opty_1.Id
          and Pearson_Course_Structure__c = :cluster.Id];
        
        System.assertEquals (optypearsoncrs3.Pearson_Course_Structure__c, cluster.Id);
        
        OpportunityPearsonCourseStructure__c optypearsoncrs4 = [select Opportunity__c,Pearson_Course_Structure__c
          from OpportunityPearsonCourseStructure__c where Opportunity__c = :opty_1.Id
          and Pearson_Course_Structure__c = :cluster2.Id];
        
        System.assertEquals (optypearsoncrs4.Pearson_Course_Structure__c, cluster2.Id);        
                
        Test.stopTest();
    }
    
    public static testMethod void myUnitTest2() {
        Test.startTest();
        // TO DO: implement unit test
        //create the account
        String AcctRecordTypeId;
        String DiscRecordTypeId;
        String ClusterRecordTypeId;
        String OptyRecordTypeId;                        
        
        List<RecordType> RecordTypeList = new List<RecordType>([select Id,SobjectType,Name from RecordType]);
        
        for (RecordType i : RecordTypeList) {
          if (i.Name == 'Bookstore') {
            AcctRecordTypeId = i.Id;
          }
          if (i.Name == '3) Discipline') {
            DiscRecordTypeId = i.Id;
          }
          if (i.Name == '2) Cluster') {
            ClusterRecordTypeId = i.Id;
          }
          if (i.SobjectType == 'Opportunity') {
            OptyRecordTypeId = i.Id;
          }                    
        }
        
        Account acct = new Account (name = 'Test1', BillingStreet = 'Street1',
                   BillingCity = 'London', BillingPostalCode = 'ABC DEF', BillingCountry = 'Australia',
                  //Vista_Account_Number__c = 'xyz',
                  RecordTypeId= AcctRecordTypeId); //'012E0000000Ll83');
        insert acct;
        
        //link account to universtiy course
        UniversityCourse__c univcrs = new UniversityCourse__c (Account__c = acct.Id, name = 'acct mgmt1');
        insert univcrs;
        
        Opportunity opty_1 = new Opportunity (name = 'Test Opty1',Account = acct, StageName = 'Identification',
          CloseDate = System.Today(),RecordTypeId = OptyRecordTypeId );
        insert opty_1;
        
        List<ProductCategory__c> listWithCategory = new List<ProductCategory__c>();
        ProductCategory__c newCategory = new ProductCategory__c();
        newCategory.HierarchyLabel__c= 'Professional & Career Services';
        newCategory.Name = 'Professional & Career Services';
        listWithCategory.add(newCategory);
        insert listWithCategory;
        
        List<Hierarchy__c> listWithClassificationHierarchy = new List<Hierarchy__c>();
        Hierarchy__c newClassificationHierarchy = new Hierarchy__c();
        newClassificationHierarchy.ProductCategory__c=  listWithCategory[0].Id;
        newClassificationHierarchy.Label__c= '(BIM) Building Information Modeling';
        newClassificationHierarchy.Name = '(BIM) Building Information Modeling';
        newClassificationHierarchy.Type__c = 'Course';
        listWithClassificationHierarchy.add(newClassificationHierarchy);
        insert listWithClassificationHierarchy;
        
        List<Opportunity_Pearson_Course_Code__c> listWithOpportunityPearsonCourseCode2 = new List<Opportunity_Pearson_Course_Code__c>();
        Opportunity_Pearson_Course_Code__c newOpportunityPearsonCourseCode2 = new Opportunity_Pearson_Course_Code__c();
        newOpportunityPearsonCourseCode2.Opportunity__c = opty_1.Id;
        newOpportunityPearsonCourseCode2.Pearson_Course_Code_Hierarchy__c= listWithClassificationHierarchy[0].Id;
        newOpportunityPearsonCourseCode2.Primary__c = true;
        listWithOpportunityPearsonCourseCode2.add(newOpportunityPearsonCourseCode2);
        insert listWithOpportunityPearsonCourseCode2;
                
        List<Pearson_Course_Equivalent__c> courseEqv_create = new List<Pearson_Course_Equivalent__c>();
        Pearson_Course_Equivalent__c pce = new Pearson_Course_Equivalent__c();
        pce.Active__c = true;  
        pce.Primary__c = true;
        pce.Pearson_Course_Code_Hierarchy__c= listWithClassificationHierarchy[0].Id;
        pce.Course__c = univcrs.Id;
        courseEqv_create.add(pce); 
        insert courseEqv_create;
        
        //associate opportunity to university course
        List<OpportunityUniversityCourse__c> optyunivcrs = new List<OpportunityUniversityCourse__c>();
        OpportunityUniversityCourse__c opptyUniv = new OpportunityUniversityCourse__c();
        
        opptyUniv.Account__c=acct.id;
        opptyUniv.Close_Date__c = system.today()+7;
        opptyUniv.Opportunity_Name__c='Test';
        opptyUniv.Opportunity_University_Course_Amount__c=100.00;
        opptyUniv.Opportunity__c = opty_1.Id;
        opptyUniv.UniversityCourse__c = univcrs.Id;
        optyunivcrs.add(opptyUniv);
        
        insert optyunivcrs;
        
        opptyUniv.Opportunity_University_Course_Amount__c=100.00;
        update optyunivcrs;
        
        delete optyunivcrs;
        
        undelete optyunivcrs;
        
        PS_OpportunityUniversityCourseOperations controllerObj = new PS_OpportunityUniversityCourseOperations();
        controllerObj.beforeUpdate(new List<sObject>(),new Map<Id, sObject>(),new List<sObject>(),new Map<Id, sObject>());
        PS_OpportunityUniversityCourseOperations.afterUpdate(new List<sObject>(),new Map<Id, sObject>(),new List<sObject>(),new Map<Id, sObject>());
        controllerObj.beforeDelete(new List<sObject>(),new Map<Id, sObject>(),new List<sObject>(),new Map<Id, sObject>());
        controllerObj.afterDelete(new List<sObject>(),new Map<Id, sObject>() ,new List<sObject>(),new Map<Id, sObject>()); 
        controllerObj.afterUndelete(new List<sObject>(),new Map<Id, sObject>());
        Test.stopTest();
        }
}