/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_Lead_RecordTaggingCtrlr.cls 
Description:     On insert/update/ of Lead record 
Date             Version         Author                             Summary of Changes 
-----------      ----------      -----------------     ---------------------------------------------------------------------------------------------------
03/2015            1.0           Srinadh Reddy                       Initial Release 
------------------------------------------------------------------------------------------------------------------
03/11/2015         1.1           Rahul Boinepally              Modified the class to populate Market, Line of Business,
Geography and Business unit fields from user object only
if they are not populated against Lead object.
04/25/2016         1.2           Kyama Rajeshwari              Updated Class To map Group field from User Group Value for RD-01729.                                                      
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class PS_Lead_RecordTagging_Ctrlr
{  
    public void leadRecordTagging(List<Lead> leadUpdateList)
    {
        
        List<User> leadLst = new List<User>();  
        //Asha_06/24/2019 Added the below for CR-02295 AIV   
        Id WebRequestLeadRecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Web Request').getRecordTypeId();     
        Map<String,Lead> mapWebRequestLead = new Map<String,Lead>(); 
        
        String user_id = UserInfo.getUserId();
        //Added Group in the query for RD-01729 R5.
        
        List <User> userdetails = [select Market__c , Line_of_Business__c , Geography__c, Business_Unit__c,Group__c from User where id =:user_id limit 1];
        
        if(userdetails[0].Market__c != null && userdetails[0].Line_of_Business__c != null && userdetails[0].Geography__c != null && userdetails[0].Business_Unit__c != null  && !userdetails.isEmpty())
        { 
            for(Lead newlead: leadUpdateList)
            {
                
                //lead l = new lead();
                if (string.isEmpty(newLead.Market__c)) 
                {
                    newlead.Market__c = userdetails[0].Market__c;
                }
                
                if(string.isEmpty(newlead.Line_of_Business__c))
                {
                    newlead.Line_of_Business__c = userdetails[0].Line_of_Business__c;   
                }
                
                if(string.isEmpty(newLead.Geography__c))
                {
                    newlead.Geography__c = userdetails[0].Geography__c;         
                }
                
                if(string.isEmpty(newLead.Business_Unit__c))
                {
                    newlead.Business_Unit__c = userdetails[0].Business_Unit__c;         
                }
                
                if(string.isEmpty(newLead.Group__c))
                {
                    newlead.Group__c = userdetails[0].Group__c;         
                }
                
            }
            //leadLst.add(newlead);
                        
        }
        else if(leadUpdateList[0].Market__c != null && leadUpdateList[0].Line_of_Business__c != null && leadUpdateList[0].Geography__c != null && leadUpdateList[0].Business_Unit__c != null )
        {
        }
        else
        {
            leadUpdateList[0].addError(System.label.Lead_Record_Tagging_Error);
        }
        
        //if(!leadLst.isEmpty())
        //{
        //update leadLst;
                // }  
        //Asha_06/24/2019: Changes For CR-02295 AIV begins        
        for(Lead lead:leadUpdateList){               
            if(lead.RecordTypeId == WebRequestLeadRecordTypeId && lead.Email !=null && lead.Contact_Lookup__c == null){             
                mapWebRequestLead.put(lead.Email,lead);     
            }       
        }       
        if(!mapWebRequestLead.isEmpty()){       
            List<Contact> matchedContactEmailList = [select Id,Email from contact where Email in:mapWebRequestLead.keyset()];       
            for(Contact con : matchedContactEmailList){     
                if(mapWebRequestLead.containsKey(con.Email)){       
                    Lead l=mapWebRequestLead.get(con.Email);        
                    l.Contact_Lookup__c = con.Id;           
                }       
            }       
        } //CR-02295 AIV changes ends
    }
    
    public void updateDateforQualifiedLead(List<Lead> lstNewLead , Map<Id,Lead> oldMap){
        
        
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Lead.getRecordTypeInfosById();
        Id leadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('D2L').getRecordTypeId();
        
        for(Lead leadObj : lstNewLead){
            if(leadObj.RecordTypeId == leadRecordTypeId){
                if(leadObj.status=='Qualified' && leadObj.status!=oldMap.get(leadObj.id).status)
                    leadObj.Lead_changed_Qualified__c = System.Today();
            }
        }
    }
}