/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_CaseBatchDeleteTest .cls 
   Description:     This class contains logic to delete all cases with Record Type as Customer Support
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  9/2/2016         1.0            Sakshi Agarwal                        Initial Release 
  12/2/2016        1.1            Supriyam Srivastava                   code review changes
   24/6/2016        1.2           Hasi Chakravarty                  code review changes
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
@isTest(SeeAllData = False)

public class PS_CaseBatchDeleteTest{
        // test method for ps_casebatchdelete
    static testMethod void PS_CaseBatchDeleteTest(){  
         //creating account
        List<Account> acc = TestDataFactory.createAccount(1,'Organisation');
        insert acc;
         //creating contact
        List<Contact> contactRecord = TestDataFactory.createContact(1);
         contactRecord[0].MailingState='England';
        Insert contactRecord;
         //creating case
        List<Case> caseList = TestDataFactory.createCase(1,'CustomerSupport');
        caseList[0].AccountId=acc[0].id;
        caseList[0].Contact=contactRecord[0];
        caseList[0].Origin='Post';
        caseList[0].Status ='New';
        caseList[0].Priority='Low';
        caseList[0].Reason = 'financial';
        caseList[0].Returning_Tablet__c = 'No';
        caseList[0].Sponsor_name__c = contactRecord[0].id;
        caseList[0].Tablet_Unique_Number__c = '123';
        insert caseList;
        List<Case> caseToDelete = [Select id from Case where id =: caseList[0].id];            
        
        PS_CaseBatchDelete obj = new PS_CaseBatchDelete();
        //running test as current user
        System.runAs(new User(Id=UserInfo.getUserId())){
        //start test
        Test.StartTest();
        Database.executebatch(obj,1);
         //end test
        Test.StopTest();
        System.assertEquals(caseList[0].Origin,'Post');
        
        }
    }
    public static testMethod void PS_scheduledCaseBatchableTest() {
  
    System.runAs(new User(Id=UserInfo.getUserId())){
         //start test
        Test.StartTest();
        PS_scheduledCaseBatchable obj = new PS_scheduledCaseBatchable();
        String sch = '0 0 23 * * ?'; 
        //passing variable
        system.schedule('Test Territory Check', sch, obj);
        //end test
        Test.stopTest(); 
        }
        
    }
}