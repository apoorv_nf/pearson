/*************************************************************************************************
*Name:   PS_PrimaryPearsonCourse
*Description : This class supports the trigger on object Pearson_Course_Equivalent__c
* Created:  Sachi   
* Modified: Cristina  - Modified trigger so it also updates field to non-Apttus tables
* Modified: Anurutheran- CR-02940[Replacing Apttus with Non-Apttus]
Pearson_Course_Code__r.PearsonCourseStructureCode__c => Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c
Pearson_Course_Code__c => Pearson_Course_Code_Hierarchy__c
*
*****************************************************************************************************/

public class PS_PrimaryPearsonCourse{

public void updatePrimaryFlag(List<Pearson_Course_Equivalent__c> newlist,Map<Id,Pearson_Course_Equivalent__c> oldmap,
Boolean isinsert,Boolean isUpdate){
    //List<Pearson_Course_Equivalent__c> newlist=newmap.values();
   //Temporary variable
    Boolean FalseToTrueFlag = False;
    Set<Id> courseId=new Set<Id>();
    
    //create the inistance for the customobject
    List<Pearson_Course_Equivalent__c> listToupdate= new List<Pearson_Course_Equivalent__c>();
    //create the inistance to the customsettings object
    List<ByPass_PS_Course_Equi_Primary_Field_Vali__c> customData = new List<ByPass_PS_Course_Equi_Primary_Field_Vali__c>();
    //Get the data from customsettings objects
    customData = [select id,ByPass__c from ByPass_PS_Course_Equi_Primary_Field_Vali__c LIMIT 1];
    for(Pearson_Course_Equivalent__c pce: newlist){
    if(isinsert){
        if(pce.Primary__c){
            courseId.add(pce.course__c);
        }
    }//insert condition
    if(isUpdate){
        //It will execute value is changing from false to true
        if (pce.Primary__c && (!oldmap.get(pce.id).Primary__c) && !FalseToTrueFlag) {//true---------Not(false)
            FalseToTrueFlag = True;
        }      
        system.debug('Oldvalue='+oldmap.get(pce.id).Primary__c);
        if(pce.Primary__c && (!oldmap.get(pce.id).Primary__c)){
            courseId.add(pce.course__c);
        }
    }//update cn
    }//for loop
    if (FalseToTrueFlag) {
        //Updating the customsetting value to ByPass the validation rule
        for(ByPass_PS_Course_Equi_Primary_Field_Vali__c temp : customData) {
            temp.ByPass__c = false;
        }
        if (customData.size() > 0) {
            update customData;
        }
    }
    
    if (courseId.size()>0){
        List<Pearson_Course_Equivalent__c> preprimlist=[select id,primary__c from Pearson_Course_Equivalent__c where course__c in :courseId and primary__c=true];
        if (preprimlist.size()>0){
            listToupdate.clear();
            for(Pearson_Course_Equivalent__c pce: preprimlist){
                pce.primary__c=false;
                listToupdate.add(pce);
            }
         }
     }
     if (listToupdate.size()>0){
         try{
             update listToupdate;
         }
         catch(DMLException e){
             throw(e);
         }              
      }
      if (FalseToTrueFlag) {
          for(ByPass_PS_Course_Equi_Primary_Field_Vali__c temp : customData) {
              temp.ByPass__c = true;//Updating the Custom setting value
          }
          if (customData.size() > 0) {
              update customData;
          } 
      }
}

public void upsertHierarchy(List<Pearson_Course_Equivalent__c>  newlist){

      //CP 12/04/2017 To add the non Apttus lookup data
      Map < id ,Hierarchy__c > allCatHierMap = new Map<id, Hierarchy__c >();
      Set<Id> newApttusCatHier = new Set<Id>();
      //we create a list of the apttus Categoryhierarchy Id's we need to map
       for (Pearson_Course_Equivalent__c newPce : newlist){
           //12-07 newApttusCatHier.add(newPce.Pearson_course_Code__c);
           newApttusCatHier.add(newPce.Pearson_course_Code_Hierarchy__c);
       }
        //we create a list of new CategoryHierarchies from the list just created of Ids
       //List<Hierarchy__c> hierLst= [Select id, CategoryHierarchy_ExternalID__c FROM Hierarchy__c WHERE CategoryHierarchy_ExternalID__c IN :newApttusCatHier];
       List<Hierarchy__c> hierLst= [Select id, CategoryHierarchy_ExternalID__c FROM Hierarchy__c WHERE ID IN :newApttusCatHier];
        //System.debug('in trigger upsert - hierLst.size() = ' + hierLst.size());
       //we add them to a map so we can get the external id as the id
       for (Hierarchy__c hier : hierLst){
           //allCatHierMap.put(hier.CategoryHierarchy_ExternalID__c, hier);
           allCatHierMap.put(hier.id, hier);
       }
       // we loop thru the list of new course equivalents and should check that we have this Hierarchy in our list
       for (Pearson_Course_Equivalent__c upsertPce : newList){
          //if (allCatHierMap.containsKey(upsertPce.Pearson_Course_Code__c)){
          if (allCatHierMap.containsKey(upsertPce.Pearson_Course_Code_Hierarchy__c)){
               //Hierarchy__c hierMap = (Hierarchy__c)allCatHierMap.get(upsertPce.Pearson_Course_Code__c);
                Hierarchy__c hierMap = (Hierarchy__c)allCatHierMap.get(upsertPce.Pearson_Course_Code_Hierarchy__c);
                //upsertPce.Pearson_Course_Code_Hierarchy__c = hierMap.id;
                //upsertPce.Pearson_Course_Code_Hierarchy__c = hierMap.CategoryHierarchy_ExternalID__c ; // Commented for CR-02940[Replacing Apttus with Non-Apttus]
            }
       }
      
}
//After Delete - INC2660466 Pearson Course Code primary indicator not setting when you delete course codes to FIX -

public void OnBeforeAfterDelete(Map<Id,Pearson_Course_Equivalent__c> oldmap) {
    set<String> courseIds = new set<String>(); // To Collect Course IDs if the Primary flag is true in PrimaryCourse Equivalent
    Map<String,Pearson_Course_Equivalent__c> courseCodeMap = new Map<String,Pearson_Course_Equivalent__c>(); // To collect existing Primary Equivalent to the related Courses
    //Iteration on Pearson_Course_Equivalent__c object
    //Jaydip changed for INC2660466
    LIST<string> strCode=new LIST<string>();
    LIST<ID> strIDs=new LIST<string>();
    for(Pearson_Course_Equivalent__c codes: oldMap.values()) {
        if(codes.Primary__c == true)
            courseIds.add(codes.Course__c);
            strIDs.add(codes.Id);
            strCode.add(codes.Course_Code__c.substring(0,2)+'%');
    }
    
   
    //system.debug('strCode:'+strCode);   
    system.debug('%%%%%%%%% courseIds '+courseIds );
    
    
    
    
    if(courseIds.size() > 0) {
    
        //for(Pearson_Course_Equivalent__c codeval:[Select Pearson_Course_Code__r.PearsonCourseStructureCode__c from Pearson_Course_Equivalent__c where id in :strIDs]) {
         //   strCode.add(codeval.Pearson_Course_Code__r.PearsonCourseStructureCode__c);
            
        
        //}
        //Query on Pearson_Course_Equivalent__c object to collect the remaining records related to the Course IDs
        
        // CR-02940[Replacing Apttus with Non-Apttus]
        for(Pearson_Course_Equivalent__c code: [Select Id, Course__c, Primary__c,Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c from Pearson_Course_Equivalent__c where Course__c IN:courseIds AND ID Not IN :oldmap.keySet() and Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c like :strCode order by Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c ASC]) {
            if(!courseCodeMap.containsKey(code.Course__c)) {
               code.Primary__c = true;
               courseCodeMap.put(code.Course__c, code);
            }
        }
        
        system.debug('courseCodeMap:'+courseCodeMap);
        if(courseCodeMap.size()==0) {
        // CR-02940[Replacing Apttus with Non-Apttus]
        for(Pearson_Course_Equivalent__c code: [Select Id, Course__c, Primary__c from Pearson_Course_Equivalent__c where Course__c IN:courseIds AND ID Not IN :oldmap.keySet() order by Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c ASC]) {
            if(!courseCodeMap.containsKey(code.Course__c)) {
               code.Primary__c = true;
               courseCodeMap.put(code.Course__c, code);
            }
        }
        
        }
        //Data validation and updating primary flag if more than one course equivalent records for the same Course
       /* for(Pearson_Course_Equivalent__c codes: oldMap.values()) {
            //If only one Equilant recordd related to Course Id, add error to stop the delete
            if(codes.Primary__c == true && !courseCodeMap.containsKey(codes.Course__c)){}
               //codes.addError('You can\'t UnCheck the Primary flag.');
        }*/
        
        system.debug('%%%%%%%%% courseCodeMap'+courseCodeMap);
        if (courseCodeMap.size()>0){
            try{
                update courseCodeMap.values();
            }
         catch(DMLException e){
             system.debug('%%%%%%%%% Exception '+e);
             throw(e);
         }              
       }
        
    }
    
}
    
    
    
    
    
    
    
// Created By Abraham For CR-1375 for Active checkbox functionality in Opportunity object
    
public void OnBeforeAfterDeleteCourse(Map<Id,Opportunity_Pearson_Course_Code__c> oldmap) {
    set<String> courseIds = new set<String>(); // To Collect Course IDs if the Primary flag is true in PrimaryCourse Equivalents
    Map<String,Opportunity_Pearson_Course_Code__c> courseCodeMap = new Map<String,Opportunity_Pearson_Course_Code__c>(); // To collect existing Primary Equivalents to the related Courses
    //Iteration on Opportunity_Pearson_Course_Code__c object
    LIST<string> strCode=new LIST<string>();
    LIST<ID> strIDs=new LIST<string>();
    for(Opportunity_Pearson_Course_Code__c codes1: oldMap.values()) {
        if(codes1.Primary__c == true)
            courseIds.add(codes1.Opportunity__c);
            strIDs.add(codes1.Id);
            strCode.add(codes1.Pearson_Course_Code_Name_f__c.substring(0,2)+'%');//CR-02940
    }
    
   
    //system.debug('strCode:'+strCode);   
    system.debug('%%%%%%%%% courseIds '+courseIds );
    
       
    
    if(courseIds.size() > 0) {
                
        for(Opportunity_Pearson_Course_Code__c code: [Select Id, Opportunity__c, Primary__c,Pearson_Course_Code_Hierarchy__r.Name,Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c from Opportunity_Pearson_Course_Code__c where Opportunity__c IN:courseIds AND ID Not IN :oldmap.keySet() and Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c like :strCode order by Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c ASC]) {
            if(!courseCodeMap.containsKey(code.Opportunity__c)) {
               code.Primary__c = true;
               courseCodeMap.put(code.Opportunity__c, code);
            }
        }
        
        system.debug('courseCodeMap:'+courseCodeMap);
        if(courseCodeMap.size()==0) {
        
        for(Opportunity_Pearson_Course_Code__c code: [Select Id, Opportunity__c, Primary__c from Opportunity_Pearson_Course_Code__c where Opportunity__c IN:courseIds AND ID Not IN :oldmap.keySet() order by Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c ASC]) {
            if(!courseCodeMap.containsKey(code.Opportunity__c)) {
               code.Primary__c = true;
               courseCodeMap.put(code.Opportunity__c, code);
            }
        }
        
        }
        //Data validation and updating primary flag if more than one course equivalent records for the same Course
       /* for(Pearson_Course_Equivalent__c codes: oldMap.values()) {
            //If only one Equilant recordd related to Course Id, add error to stop the delete
            if(codes.Primary__c == true && !courseCodeMap.containsKey(codes.Course__c)){}
               //codes.addError('You can\'t UnCheck the Primary flag.');
        }*/
        
        system.debug('%%%%%%%%% courseCodeMap'+courseCodeMap);
        if (courseCodeMap.size()>0){
            try{
                update courseCodeMap.values();
            }
         catch(DMLException e){
             system.debug('%%%%%%%%% Exception '+e);
             throw(e);
         }              
       }
        
    }
    
}
    
    
}