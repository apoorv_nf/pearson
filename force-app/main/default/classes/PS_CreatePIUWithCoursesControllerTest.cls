/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_CreatePIUWithCoursesControllerTest
   Description:     Test class for PS_CreatePIUWithCoursesController
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   18 Nov 2015      1.0           Accenture NDC                                         Created
   10 May 2016      1.1           Abhinav                                               Modified class for code coverage.
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
 
@isTest
public class PS_CreatePIUWithCoursesControllerTest
{
    static testmethod void testWithCoursesForIndirectChannel()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        List<UniversityCourse__c> univCourse = new List<UniversityCourse__c>();
        List<Product2> lstWithProduct = new List<Product2>();
        List<OpportunityUniversityCourse__c> lstWithOpportunityUniversityCourse = new List<OpportunityUniversityCourse__c>();
        List<OpportunityLineItem> lstWithOpptyProd = new List<OpportunityLineItem>();
        List<Contact> lstWithContact = new List<Contact>();
        List<OpportunityContactRole> lstOpptyContactRole = new List<OpportunityContactRole>();
        lstWithContact = TestDataFactory.createContact(1);
        lstWithProduct = TestDataFactory.createProduct(2);
        univCourse = TestDataFactory.insertCourse();
        lstWithOpportunityUniversityCourse = TestDataFactory.createOpportunityUniversityCourse();
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');  
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Indirect';
            lstWithOppty[0].Selling_Period__c = 'Fall';
            lstWithOppty[0].Selling_Period_Year__c = '2020';
            lstWithOppty[0].StageName = 'Awarded';
        }
        test.startTest();
        
    insert lstWithAccount;
        insert lstWithContact;
        insert lstWithProduct;
        for(UniversityCourse__c newUnivCourse : univCourse)
        {
            newUnivCourse.Account__c = lstWithAccount[0].Id;
        }
        insert univCourse;    
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].AccountId = lstWithAccount[0].Id;
            insert lstWithOppty;
            lstWithOpportunityUniversityCourse[0].Opportunity__c = lstWithOppty[0].Id;
            lstWithOpportunityUniversityCourse[0].Account__c = lstWithAccount[0].Id;
            lstWithOpportunityUniversityCourse[0].UniversityCourse__c = univCourse[0].Id;
        }
        if(lstWithProduct.size() > 0 && lstWithOppty.size() > 0)
        {
            List<PriceBookEntry> listWithPriceBookEntry = new List<PriceBookEntry>();
            for(Product2 insertedProd : lstWithProduct)
            {
                PriceBookEntry newPriceBookEntry = new PriceBookEntry();
                newPriceBookEntry.Pricebook2Id = Test.getStandardPricebookId();
                newPriceBookEntry.UnitPrice = 10.0;
                newPriceBookEntry.Product2Id = insertedProd.Id; 
                newPriceBookEntry.IsActive = true;
                listWithPriceBookEntry.add(newPriceBookEntry);
            }
            insert listWithPriceBookEntry;
            if(listWithPriceBookEntry.size() > 0)
            {
                for(PriceBookEntry pbe : listWithPriceBookEntry)
                {
                    opportunityLineItem newLineItem = new opportunityLineItem();
                    newLineItem.PricebookEntryId = pbe.Id;
                    newLineItem.OpportunityId = lstWithOppty[0].Id;
                    newLineItem.Quantity = 12;
                    newLineItem.UnitPrice = 10.0;
                    lstWithOpptyProd.add(newLineItem);
                }
            }
        }
        insert lstWithOpptyProd;
        insert lstWithTestUser;
        insert lstWithOpportunityUniversityCourse;
        opportunitycontactrole newOpptyCon = new opportunitycontactrole();
        newOpptyCon.contactId = lstWithContact[0].Id;
        newOpptyCon.opportunityId = lstWithOppty[0].Id;
        newOpptyCon.role = 'Team Member';
        lstOpptyContactRole.add(newOpptyCon);
        if(lstOpptyContactRole.size() > 0)
        {
            insert  lstOpptyContactRole;
        }
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
          //Test the class in 'PS_CreatePIUWithCourses' page context
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_CreatePIUWithCourses?accountId='+lstWithOppty[0].AccountId+'&channel='+lstWithOppty[0].Channel__c+'&opportunityId='+lstWithOppty[0].Id+'&sellingPeriod='+lstWithOppty[0].Selling_Period__c);
              Test.setCurrentPage(pageRef);
              ApexPages.StandardController sc = new ApexPages.StandardController(lstWithOppty[0]);
              PS_CreatePIUWithCoursesController controllerObj = new PS_CreatePIUWithCoursesController(sc);
              controllerObj.getCourse();
              controllerObj.getUsage();
              controllerObj.getDigitalUsage();
              controllerObj.getStatus();
              controllerObj.getModeOfDelivery();
              controllerObj.getThirdPartyLMS();
              controllerObj.getChannel();
              controllerObj.getChannelDetail();
              controllerObj.Beginning();
              controllerObj.Previous();
              controllerObj.Next();
              controllerObj.End();
              controllerObj.getDisablePrevious();
              controllerObj.getDisableNext();
              controllerObj.getTotal_size();
              controllerObj.getPageNumber();
              controllerObj.getTotalPages();
              controllerObj.lstWithProdWrapper[0].selectProduct = true;
              controllerObj.createAndModifyTempList();
              controllerObj.getCourse();
              controllerObj.insertPIU();
              controllerObj.cancel();
          }
        }  
        
    }
    static testmethod void testWithCoursesForDirectChannel()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');  
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Direct';
            lstWithOppty[0].Selling_Period__c = 'Spring';
            lstWithOppty[0].Selling_Period_Year__c = '2020';
            lstWithOppty[0].StageName = 'Awarded';
            lstWithOppty[0].Business_unit__c = 'Higher Ed';
            lstWithOppty[0].Line_of_Business__c = 'Higher Ed';
            lstWithOppty[0].Market__c = 'UK';
        }
        test.startTest();
        
    insert lstWithAccount;
        insert lstWithTestUser;
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].AccountId = lstWithAccount[0].Id;
            insert lstWithOppty;
        }   
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
          //Test the class in 'PS_CreatePIUWithCourses' page context
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_CreatePIUWithCourses?accountId='+lstWithOppty[0].AccountId+'&channel='+lstWithOppty[0].Channel__c+'&opportunityId='+lstWithOppty[0].Id+'&sellingPeriod='+lstWithOppty[0].Selling_Period__c);
              Test.setCurrentPage(pageRef);
              ApexPages.StandardController sc = new ApexPages.StandardController(lstWithOppty[0]);
              
              PS_CreatePIUWithCoursesController controllerObj = new PS_CreatePIUWithCoursesController(sc);
              
              controllerObj.insertPIU();
          }
        }      
    }
    static testmethod void testWithCoursesForSellingPeriodSemester1()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');  
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Direct';
            lstWithOppty[0].Selling_Period__c = 'Semester 1';
            lstWithOppty[0].Selling_Period_Year__c = '2020';
            lstWithOppty[0].StageName = 'Awarded';
        }
        test.startTest();
        
    insert lstWithAccount;
        insert lstWithTestUser;
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].AccountId = lstWithAccount[0].Id;
            insert lstWithOppty;
        }   
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
          //Test the class in 'PS_CreatePIUWithCourses' page context
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_CreatePIUWithCourses?accountId='+lstWithOppty[0].AccountId+'&channel='+lstWithOppty[0].Channel__c+'&opportunityId='+lstWithOppty[0].Id+'&sellingPeriod='+lstWithOppty[0].Selling_Period__c);
              Test.setCurrentPage(pageRef);
              ApexPages.StandardController sc = new ApexPages.StandardController(lstWithOppty[0]);
              PS_CreatePIUWithCoursesController controllerObj = new PS_CreatePIUWithCoursesController(sc);
          }
        }      
    }
    static testmethod void testWithCoursesForSellingPeriodSemester2()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');  
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Direct';
            lstWithOppty[0].Selling_Period__c = 'Semester 2';
            lstWithOppty[0].Selling_Period_Year__c = '2020';
            lstWithOppty[0].StageName = 'Awarded';
        }
        test.startTest();
        
    insert lstWithAccount;
        insert lstWithTestUser;
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].AccountId = lstWithAccount[0].Id;
            insert lstWithOppty;
        }   
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
          //Test the class in 'PS_CreatePIUWithCourses' page context
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_CreatePIUWithCourses?channel='+lstWithOppty[0].Channel__c+'&opportunityId='+lstWithOppty[0].Id+'&sellingPeriod='+lstWithOppty[0].Selling_Period__c);
              Test.setCurrentPage(pageRef);
              ApexPages.StandardController sc = new ApexPages.StandardController(lstWithOppty[0]);
              PS_CreatePIUWithCoursesController controllerObj = new PS_CreatePIUWithCoursesController(sc);
              controllerObj.insertPIU();
              controllerObj.expiryDate = system.today();
              controllerObj.selectProductUI = true;
              controllerObj.selectedProductId = '';
              controllerObj.selectedCourseId = '';
              controllerObj.elementId = '';
          }
        }      
    }
    static testmethod void testWithCoursesForSellingPeriodempty()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        List<Account> lstWithAccount  = new List<Account>();
        List<Opportunity> lstWithOppty = new List<Opportunity>();
        
        
        lstWithOppty = TestDataFactory.createOpportunity(1,'Global Opportunity');
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');  
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].Channel__c = 'Direct';
            lstWithOppty[0].Selling_Period__c = '';
            lstWithOppty[0].Selling_Period_Year__c = '2020';
            lstWithOppty[0].StageName = 'Awarded';
        }
        test.startTest();
        
    insert lstWithAccount;
        insert lstWithTestUser;
        if(lstWithOppty.size() > 0)
        {
            lstWithOppty[0].AccountId = lstWithAccount[0].Id;
            insert lstWithOppty;
        }   
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
          //Test the class in 'PS_CreatePIUWithCourses' page context
          if(lstWithOppty.size() > 0)
          {
              PageReference pageRef = new PageReference('/apex/PS_CreatePIUWithCourses?accountId='+lstWithOppty[0].AccountId+'&channel='+lstWithOppty[0].Channel__c+'&opportunityId='+lstWithOppty[0].Id+'&sellingPeriod='+lstWithOppty[0].Selling_Period__c);
              Test.setCurrentPage(pageRef);
              ApexPages.StandardController sc = new ApexPages.StandardController(lstWithOppty[0]);
              PS_CreatePIUWithCoursesController controllerObj = new PS_CreatePIUWithCoursesController(sc);
          }
        }      
    }
}