/* --------------------------------------------------------------------------------------------------------------------------------------

  Description:  Class to assign Internal requests to the Queue's defined in custom metadata object 'Internal_Request_Assignment_Rule__mdt'
  Test class:   PS_InternalRequestAssignmentTest.cls 

   Date             Version           Author                Summary of Changes 
   -----------      -------        -----------------   ----------------------------------------------------------------------
  16-Nov-2015         1.0          Tom Carman               Intial implementation
  15-Jun-2016         1.1            Kyama                  SUS512- Contact de-duplication for R6
  -------------------------------------------------------------------------------------------------------------------------------------- */

// Class to assign Internal requests to the Queue's defined in custom metadata object 'Internal_Request_Assignment_Rule__mdt'
public class PS_InternalRequestAssignment {    

    private SObjectType objectType;
    private Map<Id, String> recordTypeNameMap;
    private final String ANY_MATCH = '*';

    // only records with the following RecordType.Name will be processed:
    private final Set<String> recordTypesToProcess = new Set<String>{'Account Request','Contact Request'};//15-Jun-2016:R6:Kyama added record type to processfor SUS512- Contact de-duplication


    //Constructor method definition - to decide Object Type
    public PS_InternalRequestAssignment(SObjectType objectType) {
        this.objectType = objectType;
    } 


    // Main -Method to fetch Assigmnment rule from Custom Meta data.
    public void processAssignmentRules(List<Internal_Request__c> internalRequests) {

        getRecordTypeNames(internalRequests);

        internalRequests = filterRecordsToProcess(internalRequests);

        List<Internal_Request_Assignment_Rule__mdt> unsortedRules = getCustomAssignmentRecords();

        List<PS_InternalRequestRuleWrapper> sortedRules = sortAssignmentRules(unsortedRules);

        Map<Id, String> requestToAssignedQueueName = processRules(internalRequests, sortedRules);

        assignRequestToQueue(requestToAssignedQueueName);

    }


    // Assignment Logic
    private List<PS_InternalRequestRuleWrapper> sortAssignmentRules(List<Internal_Request_Assignment_Rule__mdt> unsortedRules) {

        List<PS_InternalRequestRuleWrapper> sortedRules = new List<PS_InternalRequestRuleWrapper>();

        for(Internal_Request_Assignment_Rule__mdt rule : unsortedRules) {
            sortedRules.add(new PS_InternalRequestRuleWrapper(rule));
        }

        sortedRules.sort(); // uses custom compareTo implemented in the wrapper class

        return sortedRules;


    }

 // Assignment Logic to assign Queues.
    private Map<Id, String> processRules(List<Internal_Request__c> internalRequests, List<PS_InternalRequestRuleWrapper> sortedRules) {

        Map<Id, String> requestToAssignedQueueName = new Map<Id, String>();

        for(Internal_Request__c request : internalRequests) {

            for(PS_InternalRequestRuleWrapper rule : sortedRules) {

                if(rule.market == ANY_MATCH || rule.market == request.Market__c) {
                    if(rule.lineOfBusiness == ANY_MATCH || rule.lineOfBusiness == request.Line_of_Business__c) {
                        if(rule.recordTypeName == ANY_MATCH || (recordTypeNameMap.containsKey(request.RecordTypeId) && (rule.recordTypeName == recordTypeNameMap.get(request.RecordTypeId)))) {
                            if(rule.type == ANY_MATCH || rule.type == request.Internal_Request_Type__c) {
                                System.debug('Matched to this rule: ' + rule.label);
                                requestToAssignedQueueName.put(request.Id, rule.queueName);
                                break;
                            }
                        }
                    }
                }
            }
        }

        return requestToAssignedQueueName;

    }

//  Logic to assign IR requests to Queues.
    private void assignRequestToQueue(Map<Id, String> requestToAssignedQueueName) {

        List<Internal_Request__c> requestsToUpdate = new List<Internal_Request__c>();

        Map<String, Id> queueNameToId = getQueueIds(requestToAssignedQueueName.values());

        for(Id requestId : requestToAssignedQueueName.keySet()) {
            Internal_Request__c request = new Internal_Request__c();
            request.Id = requestId;

            if(requestToAssignedQueueName.containsKey(requestId) && queueNameToId.containsKey(requestToAssignedQueueName.get(requestId))) {
                request.OwnerId = queueNameToId.get(requestToAssignedQueueName.get(requestId));
                requestsToUpdate.add(request);
                
            }
        }
        update requestsToUpdate;

    }




    // Helpers Classes to process which record type records.
     private void getRecordTypeNames(List<Internal_Request__c> internalRequests) {

        recordTypeNameMap = new Map<Id, String>();

        Map<ID,Schema.RecordTypeInfo> recordTypeSchemaMap = Internal_Request__c.SObjectType.getDescribe().getRecordTypeInfosById();

        for(Internal_Request__c request : internalRequests) {
            if(recordTypeSchemaMap.containsKey(request.RecordTypeId)) {
                recordTypeNameMap.put(request.RecordTypeId, recordTypeSchemaMap.get(request.RecordTypeId).getName());
            }
        }
    }

// Helpers Classes to process which record type records.
    private List<Internal_Request__c> filterRecordsToProcess(List<Internal_Request__c> incomingRequests) {

        List<Internal_Request__c> requestsToProcess = new List<Internal_Request__c>();

        for(Internal_Request__c request : incomingRequests) {
            if(recordTypeNameMap.containsKey(request.RecordTypeId)) {
                if(recordTypesToProcess.contains(recordTypeNameMap.get(request.RecordTypeId))) {
                    requestsToProcess.add(request);
                }
            }
        }
        return requestsToProcess;

    }

    //Method to Fetch Data from Custom metata data table.
    private List<Internal_Request_Assignment_Rule__mdt> getCustomAssignmentRecords() {

        String objectName = String.valueOf(objectType);

        List<Internal_Request_Assignment_Rule__mdt> rules = [SELECT Id,
                                                                    Label, 
                                                                    Object__c, 
                                                                    Market__c,
                                                                    Line_of_Business__c,
                                                                    Record_Type__c,
                                                                    Type__c,
                                                                    Queue__c
                                                            FROM Internal_Request_Assignment_Rule__mdt
                                                            WHERE Object__c = :objectName limit 50000];

        return rules;

    }

    //Method to Fetch Data from Custom metata data table.
    private Map<String, Id> getQueueIds(List<String> queueNames) {

       // List<Group> queues = [SELECT Id, DeveloperName FROM Group WHERE DeveloperName IN :queueNames];

        Map<String, Id> queueNameToId = new Map<String, Id>();

        for(Group queue : [SELECT Id, DeveloperName FROM Group WHERE DeveloperName IN :queueNames limit 50000]) {
            queueNameToId.put(queue.DeveloperName, queue.Id);
        }

        return queueNameToId;

    }




}