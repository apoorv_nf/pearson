public with sharing class BatchContactPIUEditController {
 	public final String opportunityId {get; private set;}
 	public Opportunity currentOpportunity {get; private set;}
 	public UniversityCourseWrapper currentCourse {get; private set;}
 	public Integer currentCourseIndex {get; private set;}
 	public List<UniversityCourseWrapper> coursesList {get; private set;}
 	
 	
 	public BatchContactPIUEditController(){
 		opportunityId = ApexPages.currentPage().getParameters().get('opportunityId');
 		currentCourseIndex = 0;
 		coursesList = new List<UniversityCourseWrapper>();
 	}
 	
 	public PageReference initializeOpportunityData(){
 		currentOpportunity = [SELECT Id, AccountId FROM Opportunity WHERE Id = :opportunityId];
 		List<OpportunityUniversityCourse__c> opportunityCoursesList = getCourseList();
 		List<OpportunityLineItem> opportunityProductList = getOpportunityProductList();
 		List<OpportunityContactRole> opportunityContactList = getOpportunityContactList();
 		Map<String, Asset> assetMap = getOpportunityAssetsMap();
 		//System.assertEquals(assetMap + '', '');
 		
 		for (OpportunityUniversityCourse__c opportunityCourse : opportunityCoursesList){
 			coursesList.add(new UniversityCourseWrapper(
 				opportunityCourse, 
 				opportunityProductList, 
 				opportunityContactList,
 				assetMap
 			));
 		}
 		
 		currentCourse = coursesList.get(currentCourseIndex);
 		
 		return null;
 	} 	
          
	 public void getPreviousCourse(){
	 	if (currentCourseIndex > 0){
	 		currentCourseIndex -= 1;
	 		currentCourse = coursesList.get(currentCourseIndex);
	 	}
	 }
     
	 public void getNextCourse(){
	 	if (currentCourseIndex < coursesList.size() - 1){
	 		currentCourseIndex += 1;
	 		currentCourse = coursesList.get(currentCourseIndex);
	 	}
	 }
	
	public PageReference getAssetSelected(){
		List<Asset> assetsToUpsert = new List<Asset>(), assetsToDelete = new List<Asset>();
		for (UniversityCourseWrapper courseWrapper : coursesList){
			for (ProductWrapper oppProductWrapper : courseWrapper.opportunityProductList){
				for (ContactAssetWrapper assetWrapper : oppProductWrapper.contactAssetList){
					if (assetWrapper.selected == true){
						assetsToUpsert.add(assetWrapper.resultAsset);
					} else if (assetWrapper.isExisting && !assetWrapper.selected){
						assetsToDelete.add(assetWrapper.resultAsset);
					}
				}
			}
		}
		
		if (assetsToUpsert.size() > 0) { Database.upsert(assetsToUpsert); }
		if (assetsToDelete.size() > 0) { Database.delete(assetsToDelete); }
		update new Opportunity(Id = opportunityId, Product_Converted__c = true);
		return new PageReference('/'+this.opportunityId);
	}
	
	public PageReference selectDefaultValue(){
		Integer productIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('productIndex'));
		String fieldName = String.valueOf(ApexPages.currentPage().getParameters().get('fieldName'));
		currentCourse.opportunityProductList.get(productIndex).specifyDefaulAssetValuesForField(fieldName);
		return null;
	}
 	
 	private List<OpportunityUniversityCourse__c> getCourseList(){
 		return	[SELECT Id, UniversityCourse__c, Name, University_Course_Name__c
		        		     FROM OpportunityUniversityCourse__c 
		         		    WHERE Opportunity__c = :opportunityId];
 	}
 	
 	private List<OpportunityLineItem>  getOpportunityProductList(){
 		return [SELECT Id, ListPrice, OpportunityId, Opportunity.CloseDate, Opportunity.Name,
                       Opportunity.AccountId, PricebookEntry.Product2Id, PricebookEntryId, 
                       PricebookEntry.Name, Quantity, 
                       ServiceDate, TotalPrice, 
                       UnitPrice, Sales_Channel__c,
                       Bookshop__c,  Expected_Sales_Quantity__c,
                       Required_by_Date__c, Suggested_Order_Quantity__c,
                       Discount, Description,
                       Mode_of_Delivery__c
 				FROM OpportunityLineItem
 				WHERE OpportunityId = :this.opportunityId
 		];
 	}
 	
 	private List<OpportunityContactRole> getOpportunityContactList(){
 		return [SELECT Id, ContactId, Contact.Name
 				FROM OpportunityContactRole
 				WHERE OpportunityId = :this.opportunityId
 		];
 	}
 	
 	private Map<String, Asset> getOpportunityAssetsMap(){
 		List<Asset> assetList = [SELECT Id, Name, Product2Id, AccountId, Purchase_Opportunity__c,
 				   ContactId, InstallDate, Converted_Asset__c, 
 				   Bypass_Lookup_Filtering__c, Discount__c,
 				   Course__c, Status__c, Usage__c, Third_Party_LMS__c, Primary__c
 			FROM Asset
 			WHERE Purchase_Opportunity__c = :this.opportunityId
 		];
 		
 		Map<String, Asset> assetMap = new Map<String, Asset>();
 		for (Asset oppAsset : assetList){
 			String key = oppAsset.Course__c + '|' + oppAsset.Product2Id + '|' + oppAsset.ContactId;
 			assetMap.put(key, oppAsset);
 		}
 		return assetMap;
 	}
 	
 	public PageReference courseCancelDetailPage(){
 		return new PageReference('/' + opportunityId);
 	}
 	
 	public class ContactAssetWrapper{
 		public Asset resultAsset {get; set;}
 		public Boolean selected {get; set;}
 		public String ContactName {get; private set;}
 		public Boolean isExisting {get; private set;}
 		
 		public ContactAssetWrapper(Id courseId, 
 								   OpportunityLineItem oppProduct, 
 								   OpportunityContactRole oppContact,
 								   Map<String, Asset> assetMap){
 			selected = false;
 			String key = courseId + '|' + oppProduct.PricebookEntry.Product2Id + '|' + oppContact.ContactId;
 			isExisting = assetMap.containsKey(key);
 			selected = assetMap.containsKey(key);
 			
 			if (isExisting) {
 				resultAsset = assetMap.get(key);
 			} else {
	 			resultAsset = new Asset(
	 				Name = oppProduct.PriceBookEntry.Name,
	 				Product2Id = oppProduct.PriceBookEntry.Product2Id,
	 				AccountId = oppProduct.Opportunity.AccountId,
	 				Purchase_Opportunity__c = oppProduct.OpportunityId,
	 				ContactId = oppContact.ContactId,
	 				InstallDate = Date.today(),
	 				Converted_Asset__c = true,
	 				Bypass_Lookup_Filtering__c = true,
	 				Discount__c = oppProduct.Discount,
	 				Course__c = courseId,
	 				Status__c = 'Active'
	 			);      
 			}        
 			
 			ContactName = oppContact.Contact.Name;       
 		}
 	}
 	
 	public class ProductWrapper{
 		public Id productId {get; set;}
 		public OpportunityLineItem opportunityProduct {get; private set;}
 		public String productName {get; set;}
 		public Asset defaultAssetValues {get; set;}
 		
 		public List<ContactAssetWrapper> contactAssetList {get; set;}
 		
 		public ProductWrapper(Id courseId, OpportunityLineItem oppProduct, 
 							  List<OpportunityContactRole> contactRoleList,
 							  Map<String, Asset> assetMap){
 			productId = oppProduct.PriceBookEntry.Product2Id;
 			productName = oppProduct.PriceBookEntry.Name;
 			opportunityProduct = oppProduct;
 			defaultAssetValues = new Asset();
 			
 			contactAssetList = new List<ContactAssetWrapper>();
 			for (OpportunityContactRole contactRole : contactRoleList){
 				contactAssetList.add(new ContactAssetWrapper(courseId, oppProduct, contactRole, assetMap));
 			}
 		}
 		
 		public void specifyDefaulAssetValuesForField(String fieldName){
 			for (ContactAssetWrapper assetWrapper : contactAssetList){
 				assetWrapper.resultAsset.put(fieldName, defaultAssetValues.get(fieldName));
 			}
 		}
 	}
 	
 	public class UniversityCourseWrapper{
 		public String courseNumber {get; private set;}
 		public String courseName {get; private set;}
 		public String universityCourseId {get; private set;}
 		public List<ProductWrapper> opportunityProductList {get; set;}
 		
 		public UniversityCourseWrapper(OpportunityUniversityCourse__c course, 
 									   List<OpportunityLineItem> productList, 
 									   List<OpportunityContactRole> contactRoleList,
 									   Map<String, Asset> assetMap){
 			
 			courseNumber = course.Name;
 			courseName = course.University_Course_Name__c;
 			universityCourseId = course.UniversityCourse__c;
 			
 			opportunityProductList = new List<ProductWrapper>();
 			for (OpportunityLineItem opportunityProduct : productList){
 				opportunityProductList.add(new ProductWrapper(universityCourseId, opportunityProduct, contactRoleList, assetMap));
 			}
 		}
 	}   
}