@isTest
private class TestPS_PreventChangingOwner  //Unit Test class
{   
     static testMethod void preventChangingAccountOwner(){
        Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
        User usr = new User();
        usr.LastName = 'territoryuser';
        usr.alias = 'terrusr'; 
        usr.Email = 'territoryuser11@pearson.com';  
        usr.Username='territoryuser11@pearson.com';
        usr.LanguageLocaleKey='en_US'; 
        usr.TimeZoneSidKey='America/New_York';
        usr.Price_List__c='US HE All';
        usr.LocaleSidKey='en_US';
        usr.EmailEncodingKey='ISO-8859-1';
        usr.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
        usr.Geography__c = 'Growth';
        usr.CurrencyIsoCode='USD';
        usr.Line_of_Business__c = 'Higher Ed';
        usr.Market__c = 'US';
        usr.Business_Unit__c = 'US Field Sales';
        insert usr;
        System.runAs(usr){
            user u= [select id from user where profile.name= 'System Administrator' and id!=:userinfo.getUserId() limit 1];
            Account acc = new Account();
            acc.Name='Test Learner Account';
            acc.Line_of_Business__c = 'Higher Ed';
            acc.Geography__c = 'Core';
            acc.OwnerId = u.id;        
            insert acc;
            try{
                acc.OwnerId=userinfo.getUserId();
                Update acc;
            }
            catch(Exception e){
                System.assert(e.getMessage().contains('Cannot change the owner if the current owner is Pearson'));
            }
        }
    }   
    
    static testMethod void preventChangingContactOwner(){
        Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
        User usr = new User();
        usr.LastName = 'territoryuser';
        usr.alias = 'terrusr'; 
        usr.Email = 'territoryuser22@pearson.com';  
        usr.Username='territoryuser22@pearson2.com';
        usr.LanguageLocaleKey='en_US'; 
        usr.TimeZoneSidKey='America/New_York';
        usr.Price_List__c='US HE All';
        usr.LocaleSidKey='en_US';
        usr.EmailEncodingKey='ISO-8859-1';
        usr.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
        usr.Geography__c = 'Growth';
        usr.CurrencyIsoCode='USD';
        usr.Line_of_Business__c = 'Higher Ed';
        usr.Market__c = 'US';
        usr.Business_Unit__c = 'US Field Sales';
        insert usr;
        System.runAs(usr){
            user u= [select id from user where profile.name= 'System Administrator' and id!=:userinfo.getUserId() limit 1];
            Account acc = new Account();
            acc.Name='Test Learner Account';
            acc.Line_of_Business__c = 'Higher Ed';
            acc.Geography__c = 'Core';
            acc.OwnerId=u.id;        
            insert acc;
            Contact con = new Contact();
            con.FirstName= 'First';
            con.LastName='Last';
            con.Email= 'testmail@gmail.com';
            con.OwnerId = u.id;
            con.AccountId = acc.id;
            insert con;
            try{
                con.OwnerId=userinfo.getUserId();
                Update con;
            }
            catch(Exception e){
                System.assert(e.getMessage().contains('Cannot change the owner if the current owner is Pearson'));
            }
        }
    }
    static testMethod void preventChangingLeadOwner(){
        Profile pfile1 = [Select Id,name from profile where name = 'System Administrator'];
        User usr = new User();
        usr.LastName = 'territoryuser';
        usr.alias = 'terrusr'; 
        usr.Email = 'territoryuser33@pearson.com';  
        usr.Username='territoryuser33@pearson3.com';
        usr.LanguageLocaleKey='en_US'; 
        usr.TimeZoneSidKey='America/New_York';
        usr.Price_List__c='US HE All';
        usr.LocaleSidKey='en_US';
        usr.EmailEncodingKey='ISO-8859-1';
        usr.ProfileId = pfile1.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
        usr.Geography__c = 'Growth';
        usr.CurrencyIsoCode='USD';
        usr.Line_of_Business__c = 'Higher Ed';
        usr.Market__c = 'US';
        usr.Business_Unit__c = 'US Field Sales';
        insert usr;
        Bypass_Settings__c byp = new Bypass_Settings__c(Disable_Validation_Rules__c=true);
        insert byp;
        System.runAs(usr){
            Id recordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Account Request').getRecordTypeId();
            user u= [select id from user where profile.name= 'System Administrator' and id!=:userinfo.getUserId() limit 1];
            Profile pfile = [Select Id,name from profile where name = 'System Administrator' limit 1];
            User us =[Select Id from User where profileId=: pfile.Id limit 1];
            General_One_CRM_Settings__c gocs = new General_One_CRM_Settings__c();
            gocs.Name = 'Lead Automation Batch Limit';
            gocs.Category__c ='Batch Processing';
            gocs.Description__c ='Used in PS_LeadAfterTrigger to skip execution of et4ae5.triggerUtility.automate when batch size of Leads in this trigger instance exceed this value. Starting with 10 on April 15, 2016 on advice from Dennis Thong, our Salesforce Program Architect.';
            gocs.Value__c = '10';
            insert gocs;
            List<Lead> leads = new List<Lead>();
            Map<id,Lead> oldMapLeads= new Map<id,Lead>();
            Lead lea = new Lead();
            lea.Company = 'Test Company';
            lea.Organisation_Type1__c = 'School';
            lea.Type__c = 'Other';
            lea.Sub_Type__c = 'Other';
            lea.FirstName = 'Rakesh';
            lea.LastName = 'Byri';          
            lea.RecordTypeId = recordTypeId;
            //lea.OwnerId = u.Id;
            insert lea;
            oldMapLeads.put(lea.id,lea);
            lea.OwnerId =us.Id;
            leads.add(lea);
            PS_PreventChangingOwner.PreventChangingLeadOwner(leads,oldMapLeads);
        }
    }                    
}