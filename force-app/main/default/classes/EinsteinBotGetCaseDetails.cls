/*
 * Author: Apoorv (Neuraflash)
 * Date: 26/07/2019
 * Description: Used for getting Case Details - Developed for SMS Bot.
 * Update: Added a hack to add delay before returning control back to the Bot to overcome 
 *		   50 Apex limits reached & as a workaround until Pearson dev team optimizes their code - Apoorv (13/11/2019)
 */
public with sharing class EinsteinBotGetCaseDetails {

    @InvocableMethod(label='Einstein SMS Bot - Get Case Details')
    public static List<Case> getCaseDetails(List<String> sessionIds){
		try {
			for(Case caseRec : [SELECT Id, CaseNumber FROM Case 
									WHERE Id 
										IN (
											SELECT CaseId 
												FROM MessagingSession
													WHERE Id IN :sessionIds
										)
								]){
				return new List<Case>{caseRec};
			}
			EBotResponseSettings__c respSetting = EBotResponseSettings__c.getOrgDefaults(); 
			if(respSetting != null && Integer.valueOf(respSetting.Delay__c)>0){
				Integer delay = Integer.valueOf(respSetting.Delay__c);
				Long start = DateTime.now().getTime();
				while(DateTime.now().getTime()-start<delay);
			}
		} catch(Exception ex) {}
		return null;
	}
}