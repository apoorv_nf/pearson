/*
KP:09/2015: closed Opportunity validation
KP:10/2015: Method signature update to handle insert validation
Rajesh Paleti:10/2019:Take out Apttus related objects&Fields and replaced with standard objects&Fields 
//////
*/
public class PS_OpportunityValidationFactory {
    public static List<PS_OpportunityValidationInterface> CreateOpptyValidations(List<opportunity> newoppylist,Map<Id,opportunity> inopportunity, Map<Id, opportunity> inOldopportunity){
          List<PS_OpportunityValidationInterface> validations = new List<PS_OpportunityValidationInterface>();  
          PS_opportunityD2L validation=new PS_opportunityD2L();
          validation.opptyinitialize(newoppylist,inopportunity,inOldopportunity);
          validations.add(validation);
          return validations; 
    }
    //public static List<PS_OpportunityValidationInterface> ProposalOpptyValidations(Map<Id,Apttus_Proposal__Proposal__c> inproposal, 
                                                                                   //Map<Id, Apttus_Proposal__Proposal__c> inOldproposal){
    // Commented for CR-2949 Apttus(Take out Apttus related objects&Fields and replaced with standard objects&Fields) by Rajesh Paleti
    /*    Commentment the below method for Apttus Changes
    public static List<PS_OpportunityValidationInterface> ProposalOpptyValidations(Map<Id,Quote> inproposal, 
                                                                                   Map<Id, Quote> inOldproposal){                                                                               
          List<PS_OpportunityValidationInterface> validations = new List<PS_OpportunityValidationInterface>();  
          PS_opportunityD2L validation=new PS_opportunityD2L();
          validation.proposalopptyinitialize(inproposal,inOldproposal);
          validations.add(validation);
          return validations; 
    }
    */  
    public static List<PS_OpportunityValidationInterface> OpptyLineItemValidations(Map<Id,OpportunityLineItem> inOpptyLineItem, 
                                                             Map<Id, OpportunityLineItem> inOldOpptyLineItem,String operation){
          List<PS_OpportunityValidationInterface> validations = new List<PS_OpportunityValidationInterface>();  
          PS_opportunityD2L validation=new PS_opportunityD2L();
          validation.opptylineiteminitialize(inOpptyLineItem,inOldOpptyLineItem,operation);
          validations.add(validation);
          return validations; 
    } 
    
    public static List<PS_OpportunityValidationInterface> OpptyAssetValidations(Map<Id,Asset> inAsset, 
                                                             Map<Id, Asset> inOldAsset,String operation){
          List<PS_OpportunityValidationInterface> validations = new List<PS_OpportunityValidationInterface>();  
          PS_opportunityD2L validation=new PS_opportunityD2L();
          validation.opptyPIUinitialize(inAsset,inOldAsset,operation);
          validations.add(validation);
          return validations; 
    }
    
    public static List<PS_OpportunityValidationInterface> OpptyEventValidations(List<Event> evlist,Map<Id,Event> inNewEvent,
                                                             Map<Id, Event> inOldEvent,String operation){
          List<PS_OpportunityValidationInterface> validations = new List<PS_OpportunityValidationInterface>();  
          PS_opportunityD2L validation=new PS_opportunityD2L();
          validation.opptyeventinitialize(evlist,inNewEvent,inOldEvent,operation);
          validations.add(validation);
          return validations; 
    }
    
    public static List<PS_OpportunityValidationInterface> OpptyTaskValidations(List<Task> tlist,Map<Id,Task> inNewTask, 
                                                             Map<Id, Task> inOldTask,String operation){
          List<PS_OpportunityValidationInterface> validations = new List<PS_OpportunityValidationInterface>();  
          PS_opportunityD2L validation=new PS_opportunityD2L();
          validation.opptytaskinitialize(tlist,inNewTask,inOldTask,operation);
          validations.add(validation);
          return validations; 
    }             
}