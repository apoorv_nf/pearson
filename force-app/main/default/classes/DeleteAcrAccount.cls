/***********************************************************************************************************************************
##CR-01018
##Description: Delete the ACR account during Case account update from ACR to Non ACR account if ACR has one case and one Contact.
##
##
************************************************************************************************************************************/
Public class DeleteAcrAccount{

    Public static void DeleteAcr(List<Case> caseList,Map<Id,Case> caseOldmap){
         Set<Id> OldAccID = new Set<Id>();
         Set<Id> newAccid = new Set<Id>();
         Set<Id> acctId = new Set<Id>();
         Set<Id> contId = new Set<Id>();
         System.debug('caseList'+caseList); 
         System.debug('caseOldmap'+caseOldmap);   
        for(Case cs: caseList){
            OldAccId.add(caseOldmap.get(cs.id).AccountId);
            newAccid.add(cs.AccountId);
         
        }
        Set<Id> newSet = new Set<Id>(OldAccId);
        newSet.addall(newAccid);
         System.debug('newSet'+newSet);
        Map<Id, Account> accrecType= New Map<Id, Account>([Select Id,recordtypeId from Account where Id in:newSet]);
        System.debug('accrecType'+accrecType);
        if(accrecType.size()!=0)
        {
            for(Case cs: caseList)
            {
                if(caseoldmap.get(cs.id).Accountid!=null && accrecType.get(caseoldmap.get(cs.id).Accountid).recordtypeId ==Label.PS_ACR_RecordType && accrecType.get(cs.AccountId).recordtypeId !=Label.PS_ACR_RecordType)
                {
                    acctId.add(caseoldmap.get(cs.id).Accountid);
                    contId.add(cs.contactId);
                }
            }
        }
        
        if(!acctId.isEmpty()){
            Map<Id,Contact> contMap = new Map<Id,Contact>([Select Id,account.recordtypeid from contact where id in:contId]);
            AggregateResult[] contactsCount =[Select Accountid,count(id) from contact where accountid in : acctId group by Accountid];
            AggregateResult[] casesCount =[Select Accountid,count(id) from case where accountid in : acctId group by Accountid];
            Map<String,Integer> accContMap = new Map<String,Integer>();
            Map<String,Integer> accCaseMap = new Map<String,Integer>();
            for(AggregateResult ar: contactsCount){
                accContMap.put(String.ValueOf(ar.get('accountId')),Integer.ValueOf(ar.get('expr0')));
            }
            for(AggregateResult arr: casesCount){
                accCaseMap.put(String.ValueOf(arr.get('accountId')),Integer.ValueOf(arr.get('expr0')));
            }
            Map<String,List<String>> masterChild = new Map<String,List<String>>();
            List<contact> updateConList = new List<Contact>();
            List<Account> deleteAccountList = new List<Account>();
            for(Case caseObj: caseList){
              if(Integer.ValueOf(accContMap.get(caseoldmap.get(caseObj.id).accountId))==1 && accCaseMap.isEmpty() && contMap.get(caseObj.contactId).account.recordtypeId==Label.PS_ACR_RecordType)
                {
                    // Old Account with one Contact and one case
                    System.debug('Inside MergeCall');
                    Contact con = new Contact();
                    con.accountId = caseObj.accountId;
                    con.Id = caseObj.contactId;
                    updateConList.add(con);
                    Account childacc = new Account(id=caseOldmap.get(caseObj.id).AccountId);
                    deleteAccountList.add(childacc);
                  }
              }
              Update updateConList;
              Delete deleteAccountList;
         }
  }
}