/*----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_EmailPublisherLoader.cls 
Description:     Email defaulting on Case Feed
Test class:      NA 
Date             Version              Author                          Summary of Changes 
-----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
22-Jan-2016        1.0               Sakshi Agarwal                Email Publisher to default the from Address on Case for RD-1530
16-Feb-2016        2.0                KP                            Updated field to "To_Email_Origin_Address__c"
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
global class PS_EmailPublisherLoader implements QuickAction.QuickActionDefaultsHandler {
    // Empty constructor 
    global PS_EmailPublisherLoader() {}
    
    // Method to set default from Address
    global void onInitDefaults(QuickAction.QuickActionDefaults[] defaults) {
        QuickAction.SendEmailQuickActionDefaults sendEmailDefaults = null;
        // Check if the quick action is the standard Case Feed send email action
        for (Integer j = 0; j < defaults.size(); j++) {
            if (defaults.get(j) instanceof QuickAction.SendEmailQuickActionDefaults && defaults.get(j).getTargetSObject().getSObjectType() == EmailMessage.sObjectType 
                && (defaults.get(j).getActionName().equals('Case.Email') || defaults.get(j).getActionName().equals('Case.SendEmail') 
                    || defaults.get(j).getActionName().equals('EmailMessage._Reply') || defaults.get(j).getActionName().equals('EmailMessage._ReplyAll'))
                && (defaults.get(j).getActionType().equals('Email') || defaults.get(j).getActionType().equals('SendEmail')))
            {
                sendEmailDefaults = (QuickAction.SendEmailQuickActionDefaults)defaults.get(j);
                break;
            }
        }
		System.debug('sendEmailDefaults: '+sendEmailDefaults);
        if (sendEmailDefaults != null){ 
            Case c = [SELECT To_Email_Origin_Address__c,id,origin,recordtype.name,program__c,Contact.Email,SuppliedEmail,Market__c,subject,CaseNumber FROM Case 
                      WHERE Id=:sendEmailDefaults.getContextId()];
            EmailMessage emailMessage = (EmailMessage)sendEmailDefaults.getTargetSObject();
            emailMessage.Fromaddress = getFromAddress(c.To_Email_Origin_Address__c,c.Origin);
            if(sendEmailDefaults.getActionName().equals('Case.SendEmail')){
                emailMessage.ValidatedFromAddress = emailMessage.FromAddress;
                emailMessage.CcAddress = '';
            }
            //Manimala : CRMSFDC-5448 - case number added to the subject line
            emailMessage.subject=('Case Number '+c.casenumber+' : '+c.subject);
            emailMessage.BccAddress='';
            //Rakesh : CRMSFDC-5165 - Added WebEmail to ToAddress in Email quickaction only UK market
            if(c.Market__c=='UK' && c.Contact.Email!=null && c.Contact.Email!='' && c.SuppliedEmail!='' && c.SuppliedEmail!=null && c.Contact.Email!=c.SuppliedEmail){
                emailMessage.toAddress=(c.Contact.Email+' '+c.SuppliedEmail);
            }
            else if(c.Market__c=='UK' && c.Contact.Email!=null && c.Contact.Email!='' && c.SuppliedEmail!='' && c.SuppliedEmail!=null && c.Contact.Email==c.SuppliedEmail){
                emailMessage.toAddress=(c.Contact.Email);
            }
            //Filtering the Email address specific to the program for the School Assessment Record Type
            if(System.label.School_Assessment == c.recordtype.name){
                List<String> emailAddresses = sendEmailDefaults.getFromAddressList();
                emailAddresses.clear();
                emailAddresses.add(getSAFromAddress(c.program__c, c.origin));
            }
        }       
    }    
    
    //Method to fetch From Address from Case field 'To_Email_Origin_Address__c'   
    private String getFromAddress(String fromAddress,String origin) {
        String customerSupportEmail = System.label.CustomerSupportEmail;
        if(fromAddress != null && origin == 'Email'){
            customerSupportEmail = fromAddress;
        }
        
        return customerSupportEmail;       
    }
    
    //Method Added for School Assessment from Prod - By Sunil
    private String getSAFromAddress(String programName, String caseOrigin) {
        String emailAddress = '';
        if (programName != null) {
            if('PARCC'.equals(programName) && !'Email'.equals(caseOrigin)){
                emailAddress = Label.SA_PARCC_Support_Address;
            } else if ('PARCC'.equals(programName) && 'Email'.equals(caseOrigin)) {
                emailAddress = Label.SA_PARCC_Email_Support_Address;
            } else if ('ACCUPLACER'.equals(programName)) {
                emailAddress = Label.SA_ACCUPLACER_Support_Address;
            } else if ('NBPTS'.equals(programName)) {
                emailAddress = Label.SA_NBPTS_Support_Address;
            } else {
                emailAddress = Label.SA_Support_Address_For_Other_Programs;
            }
        }
        return emailAddress; 
    }
}