@isTest
public class CaseDeletionWithACRTest {
    @isTest
    private static void testCaseDeletion(){                
        Id acrRecordTypeId = Account.sObjectType.getDescribe().getRecordTypeInfosByName().get('Account Creation Request').getRecordTypeId();
        List<Account> accountsList = new List<Account>();
        Account acc= new Account();
        acc.RecordTypeId = acrRecordTypeId;
        acc.Name = 'TestAccount';
        accountsList.add(acc);
        insert accountsList;        
        Id ContRecordTypeId = Contact.sObjectType.getDescribe().getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
        List<Contact> contactList= new List<Contact>();
        Contact Con = new Contact();
        con.RecordTypeId = ContRecordTypeId;
        con.AccountId = accountsList[0].Id;
        con.FirstName = 'ForTestAccount';
        con.LastName = 'TestContact1';
        con.Email='test@email.com';
        con.MobilePhone = '8884888472';
        con.Phone='1112223331'; 
        con.OtherPhone ='1112223331';
        con.Secondary_Email__c='testsec@email.com';
        con.Home_Phone__c ='34435465';        
        con.Preferred_Address__c='Mailing Address';
        con.MailingCity = 'Newcastle';        
        con.MailingState='Northumberland';
        con.MailingCountry='United Kingdom';
        con.MailingStreet='1st Street';
        con.MailingPostalCode='NE28 7BJ';
     	con.IndividualId=null;
        contactList.add(con);
        insert contactList;        
        Id caseRecordTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('Technical Support').getRecordTypeId();
        List<Case> caseList= new List<Case>();
        List<Case> caseList1= new List<Case>();
        Case cs = new Case();
        cs.AccountId = accountsList[0].Id;
        cs.ContactId= contactList[0].Id; 
        cs.Status='New';
        cs.SuppliedName ='TestContact1 ForTestAccount';
        cs.RecordTypeId = caseRecordTypeId;
        cs.CreatedDate =System.now();
        caseList.add(cs);        
        Test.startTest(); 
        insert caseList;
     /*   cs.Status='Closed';
        cs.Case_Resolution_Category__c = 'Customer - No Response';
        Id ownerId = [Select Id from Group where Name = 'Pre-Call Form Queue' and Type = 'Queue'].Id;
        cs.OwnerId=ownerId;
        caseList1.add(cs);        
        update caseList1;*/
        List<Case> css1 =[SELECT Id, ContactId, AccountId FROM Case where Status='Closed' and CreatedDate = Today];
        Database.executeBatch(new CaseDeletionWithACR());
        Test.stopTest();         
    }
}