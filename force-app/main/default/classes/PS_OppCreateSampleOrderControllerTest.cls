/**
* Name : PS_OppCreateSampleOrderControllerTest
* Author : Accenture_Karan
* Description : Test class used for testing PS_OppCreateSampleOrderController
* Date : 17/11/15 
* Version : <intial Draft> 
* Replaced Qualification__c field by QuoteMigration_Qualification__c as part of ZA Apttus Decomm by Mani - 07/25/2018 
*/
@isTest(SeeAllData=true)
public class PS_OppCreateSampleOrderControllerTest
{    
    static testMethod void testOppCreateSampleOrder(){
        Profile ProfileNameadmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        List<User> u1 = TestDataFactory.createUser(ProfileNameadmin.Id,1);
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu1'; 
            u1[0].Email ='Opptyproposaltest11@pearson.com'; 
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='Opptyproposaltest12'; 
            u1[0].LanguageLocaleKey ='en_US'; 
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = ProfileNameadmin.Id; 
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName ='standarduserQuoteCreation1@testorg.com';
            u1[0].Market__c ='UK';
            u1[0].Business_Unit__c = 'Higher Ed';
            u1[0].line_of_Business__c = 'Higher Ed';
            u1[0].Product_Business_Unit__c = 'Higher Ed';
            insert u1;
        }
        
        Bypass_Settings__c bys = [Select id,SetupOwnerId,Disable_Validation_Rules__c from Bypass_Settings__c limit 1];
        bys.SetupOwnerId = u1[0].id;
        bys.Disable_Validation_Rules__c = true;
        bys.Disable_Process_Builder__c = true;
        bys.Disable_Triggers__c = true;
        upsert bys;
        
        System.runAs(u1[0]){
        // Test data preparation               
        // setting LeadConversionChecker.inLeadConversion to true reduces the number of SOQL statements being executed as it prevents
        // the primary selling flag functionality from executing
        LeadConversionChecker.inLeadConversion = true;
        account acc = new Account();
        acc.Name='Test Account1';
        acc.IsCreatedFromLead__c = True;
        acc.Phone='+91000001';
        acc.Market2__c= 'AU';
        acc.Line_of_Business__c= 'Higher Ed';
        acc.Geography__c= 'Core';
        acc.ShippingCountry = 'India';
        acc.ShippingCity = 'Bangalore';
        acc.ShippingStreet = 'BNG1'; 
        acc.ShippingPostalCode = '5600371';
        //acc.Primary_Selling_Account__r.name='test';    
        insert acc;
        List<RecordType> rt1 = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name = 'Global Opportunity' LIMIT 1];  
        Opportunity opp = new opportunity ( Name = 'OpporName1',RecordTypeId= rt1[0].Id,AccountId = acc.Id,CloseDate = System.TODAY() + 30,StageName = 'OpporStageName',Channel__c='Student Driven', QuoteMigration_Qualification__c  = 'Graphic Design', Market__c ='AU', Line_of_Business__c = 'Higher Ed',Business_unit__c ='Higher Ed');
        insert opp;
        Contact con = new Contact(FirstName='TestContactFirstname12',skipdedupe__c=True, LastName='TestContactLastname12',AccountId=opp.accountid ,Salutation='MR.',MailingStreet='test',Mailling_Home_Address__c=True, Email='test12@gmail.com',MobilePhone='111222333' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English',Preferred_Address__c = 'Other Address');  
        insert con;
        OpportunityContactRole oc = new OpportunityContactRole( opportunityId =  opp.Id,contactId = con.id ,Role =  'Business User');
        insert oc;
        
        Product2 prod = new Product2();
        prod.Configuration_Type__c='Option'; 
        prod.name = 'NA Test Product5';
        prod.Duration__c = 'D20';
        prod.Market__c ='AU';
        prod.Business_Unit__c='Higher Ed';
        prod.Line_of_Business__c = 'Higher Ed';           
        prod.Author__c = 'Test Author'; 
        prod.Edition__c = '10';
        prod.Medium2__c = 'Print';
        prod.Relevance_Value__c = 10;
        prod.Brand__c = null;    
        prod.Binding__c = 'Cloth';
        //prod.PIHE_Repeater__c = 'True';
        insert Prod;
        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true); 
        insert customPB;
        PricebookEntry pbe2 =new PricebookEntry(Product2Id=Prod.Id,Pricebook2Id=pricebookId,UnitPrice = 100.00,isActive=true,UseStandardPrice = false);
        insert pbe2;
        List<RecordType> rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Order' AND Name = 'Sample Order' LIMIT 1];
        order sampleorder1 = new order(Accountid=opp.accountid,RecordTypeId=rt[0].Id,ShipToContactid=con.id,Opportunityid=opp.id,EffectiveDate=system.today(),Status='New',Pricebook2Id=pricebookId,CurrencyIsoCode = userinfo.getDefaultCurrency(),type=System.Label.Order_Type, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371', ShippingState = 'Karnataka',Geography__c = 'Core',Line_of_Business__c='Higher Ed',Market__c='AU',ERP_Order_Type__c='HE-US-SAMPLE',Salesperson__c = UserInfo.getUserId());
        insert sampleorder1;
        order sampleorder2 = new order(Accountid=opp.accountid,RecordTypeId=rt[0].Id,ShipToContactid=con.id,Opportunityid=opp.id,EffectiveDate=system.today(),Status='New',Pricebook2Id=pricebookId,CurrencyIsoCode = userinfo.getDefaultCurrency(),type=System.Label.Order_Type, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371', ShippingState = 'Karnataka',Geography__c = 'Core',Line_of_Business__c='Higher Ed',Market__c='AU', ERP_Order_Type__c='HE-US-SAMPLE',Salesperson__c = UserInfo.getUserId());
        insert sampleorder2;
        List<orderitem> olist = new List<orderitem>();
        orderitem item1 = new orderitem(orderid=sampleorder1.id,Shipped_Product__c = prod.id,Quantity = 1,pricebookentryid= pbe2.id, unitprice=0,Shipping_Method__c= 'Ground' );
        orderitem item2 = new orderitem(orderid=sampleorder1.id,Shipped_Product__c = prod.id,Quantity = 1,pricebookentryid= pbe2.id, unitprice=0,Shipping_Method__c= 'Ground' );
        orderitem item3 = new orderitem(orderid=sampleorder1.id,Shipped_Product__c = prod.id,Quantity = 1,pricebookentryid= pbe2.id, unitprice=0,Shipping_Method__c= 'Ground' );
        orderitem item4 = new orderitem(orderid=sampleorder2.id,Shipped_Product__c = prod.id,Quantity = 1,pricebookentryid= pbe2.id, unitprice=0,Shipping_Method__c= 'Ground', status__c ='Approved' );
        olist.add(item1);
        olist.add(item2);
        olist.add(item3);
        olist.add(item4);
        test.startTest();
        insert olist;
        
 
        //delete item3;
        OpportunityLineItem Oli = new OpportunityLineItem();
        Oli.OpportunityId = opp.id;
        Oli.Product2Id = Prod.Id;
        Oli.PricebookEntryId = pbe2.Id;
        Oli.Quantity = 1;
        oli.sample__c = true;
        Oli.TotalPrice = pbe2.UnitPrice;
        Insert Oli;
        
        
        PageReference tpageRef = Page.PS_OppCreateSampleOrder;
        Test.setCurrentPage(tpageRef);          
        ApexPages.currentPage().getParameters().put('Id', opp.Id);
        PS_OppCreateSampleOrderController obj = new PS_OppCreateSampleOrderController();
        //obj.erpBillingLocationErrorPost();        
        obj.AddressOverride();
        obj.BackFromAddressOverride();
        obj.selectTheAddress();
        obj.orderContactsList[0].ischecked = true;
        obj.orderContactsList[0].addressOption = '';
        obj.selectTheAddress();
        obj.orderContactsList[0].addressOption = 'Mailing Address';
        obj.orderContactsList[0].address = 'Mailing Address';
        obj.selectTheAddress();
        /** Rajesh
        obj.orderContactsList[0].addressOption = 'Other Address';
        obj.orderContactsList[0].address = 'Other Address';
        obj.selectTheAddress();
        obj.orderContactsList[0].addressOption = 'Account Address';
        obj.orderContactsList[0].address = 'Account Address';
        obj.selectTheAddress(); **/
        obj.orderContactsList[0].cont.MailingCountry = 'United States';
        obj.orderContactsList[0].addressOption = 'Mailing Address';
        obj.selectTheAddress();
        obj.orderContactsList[0].addressOption = 'Custom Address';
        obj.orderContactsList[0].tempOrder.ShippingCountryCode = 'US';
        obj.selectTheAddress();
        obj.orderContactsList[0].addressOption = 'Mailing Address';
        obj.orderContactsList[0].cont.MailingStreet = '';
        obj.orderContactsList[0].cont.MailingCity = '';
        obj.orderContactsList[0].cont.MailingState = '';
        obj.orderContactsList[0].cont.MailingCountry = '';
        obj.orderContactsList[0].cont.MailingPostalCode = '';
        obj.selectTheAddress();
        
       
        
        obj.i=0;
        obj.j=0;
        obj.orderContactsList[0].ischecked = true;
        obj.orderContactsList[0].addressOption = '';
        obj.selectTheAddress();
        obj.VerfyProd();
        obj.VerifyQuantity();
        obj.refreshverifybutton();
        obj.erpBillingLocationErrorPost();
        obj.Cancel();
        obj.orderContactsList[0].quantity = 1;
        OpportunityContactRole oct = new OpportunityContactRole();
        Contact cont = new Contact();
        PS_OppCreateSampleOrderController.SampleOrderContacts innerSOC = new PS_OppCreateSampleOrderController.SampleOrderContacts(oct, cont, 'Add');         
        innerSOC.getmyshippingOptions();
        obj.BackFromVerifyProd();
        obj.SaveVerifyProd();
        OpportunityLineItem pli2 = new OpportunityLineItem();
        PS_OppCreateSampleOrderController.SampleOrderLineItems innerSOLI = new PS_OppCreateSampleOrderController.SampleOrderLineItems(pli2, 'stype');         
        innerSOLI.getSampleTypeOptions();
        Id dummyId;
        PS_OppCreateSampleOrderController.duplicateProductContact innerDPC = new PS_OppCreateSampleOrderController.duplicateProductContact('cname',dummyId,dummyId); 
        innerDPC.getProdDuplistOptions();
        obj.SubmitSampleOrder();
        //obj.orderLineItemList[0].sampleType = 'Sample Copy';
        //obj.orderLineItemList[1].sampleType = 'Inspection Copy';
        obj.SubmitSampleOrder();
        
        obj.orderContactsList[0].addressOption = 'Other Address';
        obj.orderContactsList[0].address = 'Other Address';
        obj.selectTheAddress();
        obj.SubmitSampleOrder();
        
        obj.orderContactsList[0].addressOption = 'Custom Address';
        obj.orderContactsList[0].address = 'Custom Address';
        obj.selectTheAddress();
        obj.SubmitSampleOrder();
        
        test.stopTest();    
       }
    }
    static testMethod void testOppCreateSampleOrder2(){
        Profile ProfileNameadmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        List<User> u1 = TestDataFactory.createUser(ProfileNameadmin.Id,1);
        if(u1 != null && !u1.isEmpty()){
            u1[0].Alias = 'Opprtu1'; 
            u1[0].Email ='Opptyproposaltest11@pearson.com'; 
            u1[0].EmailEncodingKey ='UTF-8';
            u1[0].LastName ='Opptyproposaltest12'; 
            u1[0].LanguageLocaleKey ='en_US'; 
            u1[0].LocaleSidKey ='en_US';
            u1[0].ProfileId = ProfileNameadmin.Id; 
            u1[0].TimeZoneSidKey ='America/Los_Angeles';
            u1[0].UserName ='standarduserQuoteCreation1@testorg.com';
            u1[0].Market__c ='UK';
            u1[0].Business_Unit__c = 'Higher Ed';
            u1[0].line_of_Business__c = 'Higher Ed';
            u1[0].Product_Business_Unit__c = 'Higher Ed';
            insert u1;
        }
        
        Bypass_Settings__c bys = [Select id,SetupOwnerId,Disable_Validation_Rules__c from Bypass_Settings__c limit 1];
        bys.SetupOwnerId = u1[0].id;
        bys.Disable_Validation_Rules__c = true;
        bys.Disable_Process_Builder__c = true;
        bys.Disable_Triggers__c = true;
        upsert bys;
        
        System.runAs(u1[0]){
        // Test data preparation               
        // setting LeadConversionChecker.inLeadConversion to true reduces the number of SOQL statements being executed as it prevents
        // the primary selling flag functionality from executing
        LeadConversionChecker.inLeadConversion = true;
        account acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'AU', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
        insert acc;
        List<RecordType> rt1 = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name = 'Global Opportunity' LIMIT 1];  
        Opportunity opp = new opportunity ( Name = 'OpporName1',RecordTypeId= rt1[0].Id,AccountId = acc.Id,CloseDate = System.TODAY() + 30,StageName = 'OpporStageName',Channel__c='Student Driven', QuoteMigration_Qualification__c  = 'Graphic Design', Market__c ='AU', Line_of_Business__c = 'Higher Ed',Business_unit__c ='Higher Ed');
        insert opp;
        Contact con = new Contact(FirstName='TestContactFirstname1', LastName='TestContactLastname321',AccountId=opp.accountid ,Salutation='MR.', Email='test321@gmail.com',MobilePhone='111222333' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English',Preferred_Address__c = 'Other Address');  
        insert con;
        OpportunityContactRole oc = new OpportunityContactRole( opportunityId =  opp.Id,contactId = con.id ,Role =  'Business User');
        insert oc;
        
        Product2 prod = new Product2();
        prod.Configuration_Type__c='Option'; 
        prod.name = 'NA Test Product5';
        prod.Duration__c = 'D20';
        prod.Market__c ='AU';
        prod.Business_Unit__c='Higher Ed';
        prod.Line_of_Business__c = 'Higher Ed';           
        prod.Author__c = 'Test Author'; 
        prod.Edition__c = '10';
        prod.Medium2__c = 'Print';
        prod.Relevance_Value__c = 10;
        prod.Brand__c = null;    
        prod.Binding__c = 'Cloth';
        //prod.PIHE_Repeater__c = 'True' ;
        insert Prod;
        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true); 
        insert customPB;
        PricebookEntry pbe2 =new PricebookEntry(Product2Id=Prod.Id,Pricebook2Id=pricebookId,UnitPrice = 100.00,isActive=true,UseStandardPrice = false);
        insert pbe2;
        List<RecordType> rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Order' AND Name = 'Sample Order' LIMIT 1];
        order sampleorder1 = new order(Accountid=opp.accountid,RecordTypeId=rt[0].Id,ShipToContactid=con.id,Opportunityid=opp.id,EffectiveDate=system.today(),Status='New',Pricebook2Id=pricebookId,CurrencyIsoCode = userinfo.getDefaultCurrency(),type=System.Label.Order_Type, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371', ShippingState = 'Karnataka',Geography__c = 'Core',Line_of_Business__c='Higher Ed',Market__c='AU', ERP_Order_Type__c='HE-US-SAMPLE', Salesperson__c = UserInfo.getUserId());
        insert sampleorder1;
        order sampleorder2 = new order(Accountid=opp.accountid,RecordTypeId=rt[0].Id,ShipToContactid=con.id,Opportunityid=opp.id,EffectiveDate=system.today(),Status='New',Pricebook2Id=pricebookId,CurrencyIsoCode = userinfo.getDefaultCurrency(),type=System.Label.Order_Type, ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371', ShippingState = 'Karnataka',Geography__c = 'Core',Line_of_Business__c='Higher Ed',Market__c='AU', ERP_Order_Type__c='HE-US-SAMPLE', Salesperson__c = UserInfo.getUserId());
        insert sampleorder2;
        
        List<orderitem> olist = new List<orderitem>();
        orderitem item1 = new orderitem(orderid=sampleorder1.id,Shipped_Product__c = prod.id,Quantity = 1,pricebookentryid= pbe2.id, unitprice=0,Shipping_Method__c= 'Ground' );
        orderitem item2 = new orderitem(orderid=sampleorder1.id,Shipped_Product__c = prod.id,Quantity = 1,pricebookentryid= pbe2.id, unitprice=0,Shipping_Method__c= 'Ground' );
        orderitem item3 = new orderitem(orderid=sampleorder2.id,Shipped_Product__c = prod.id,Quantity = 1,pricebookentryid= pbe2.id, unitprice=0,Shipping_Method__c= 'Ground', status__c ='Approved');
        orderitem item4 = new orderitem(orderid=sampleorder1.id,Shipped_Product__c = prod.id,Quantity = 1,pricebookentryid= pbe2.id, unitprice=0,Shipping_Method__c= 'Ground' );
        
        olist.add(item1);
        olist.add(item2);
        olist.add(item3);
        olist.add(item4);
        test.startTest();           
        insert olist;
        //delete item4;
        
        sampleorder1.Status = 'open';
        try{
            update sampleorder1;
        }
        Catch(Exception ex ){
            System.Debug('### Exception has occurred  ' + ex);
            //system.Assert
        }      
        OpportunityLineItem Oli = new OpportunityLineItem();
        Oli.OpportunityId = opp.id;
        Oli.Product2Id = Prod.Id;
        Oli.PricebookEntryId = pbe2.Id;
        Oli.Quantity = 1;
        oli.sample__c = true;
        Oli.TotalPrice = pbe2.UnitPrice;
        Insert Oli;
        
        PageReference tpageRef = Page.PS_OppCreateSampleOrder;
        Test.setCurrentPage(tpageRef);          
        ApexPages.currentPage().getParameters().put('Id', opp.Id);
        PS_OppCreateSampleOrderController obj = new PS_OppCreateSampleOrderController();
        obj.AddressOverride();
        obj.BackFromAddressOverride();
        obj.selectTheAddress();
        obj.orderContactsList[0].ischecked = true;
        obj.orderContactsList[0].addressOption = '';
        obj.selectTheAddress();
        obj.orderContactsList[0].addressOption = 'Mailing Address';
        obj.orderContactsList[0].address = 'Mailing Address';
        obj.selectTheAddress();
        obj.orderContactsList[0].addressOption = 'Other Address';
        obj.orderContactsList[0].address = 'Other Address';
        obj.selectTheAddress();
        obj.orderContactsList[0].addressOption = 'Account Address';
        obj.orderContactsList[0].address = 'Account Address';
        obj.selectTheAddress();
        obj.orderContactsList[0].cont.MailingCountry = 'United States';
        obj.orderContactsList[0].addressOption = 'Mailing Address';
        obj.selectTheAddress();
        obj.orderContactsList[0].addressOption = 'Custom Address';
        obj.orderContactsList[0].tempOrder.ShippingCountryCode = 'US';
        obj.selectTheAddress();
        obj.orderContactsList[0].addressOption = 'Mailing Address';
        obj.orderContactsList[0].cont.MailingStreet = '';
        obj.orderContactsList[0].cont.MailingCity = '';
        obj.orderContactsList[0].cont.MailingState = '';
        obj.orderContactsList[0].cont.MailingCountry = '';
        obj.orderContactsList[0].cont.MailingPostalCode = '';
        obj.selectTheAddress();
        obj.i=0;
        obj.j=0;
        obj.orderContactsList[0].ischecked = true;
        obj.orderContactsList[0].addressOption = '';
        obj.selectTheAddress();
        obj.VerfyProd();
        obj.VerifyQuantity();
        obj.refreshverifybutton();
        obj.orderContactsList[0].quantity = 1;
        OpportunityContactRole oct = new OpportunityContactRole();
        Contact cont = new Contact();
        PS_OppCreateSampleOrderController.SampleOrderContacts innerSOC = new PS_OppCreateSampleOrderController.SampleOrderContacts(oct, cont, 'Add');         
        innerSOC.isContOtherAdd = false;
        innerSOC.isAccPhysAdd = false;
        innerSOC.isCustomAdd = false;
        innerSOC.isContMailingAdd = false;
        innerSOC.getmyshippingOptions();
        obj.BackFromVerifyProd();
        obj.SaveVerifyProd();
        OpportunityLineItem pli2 = new OpportunityLineItem();
        PS_OppCreateSampleOrderController.SampleOrderLineItems innerSOLI = new PS_OppCreateSampleOrderController.SampleOrderLineItems(pli2, 'stype');         
        innerSOLI.getSampleTypeOptions();
        Id dummyId;
        PS_OppCreateSampleOrderController.duplicateProductContact innerDPC = new PS_OppCreateSampleOrderController.duplicateProductContact('cname',dummyId,dummyId); 
        innerDPC.getProdDuplistOptions();
        obj.SubmitSampleOrder();
        //obj.orderLineItemList[0].sampleType = 'Sample Copy';
        //obj.orderLineItemList[1].sampleType = 'Inspection Copy';
        obj.SubmitSampleOrder();
        test.stopTest();  
      }  
    } 
    static testMethod void testOppLitemsFieldsPrepopulate(){
        
        List<User> listWithUser = new List<User>();
        List<userRole> userRole = [select id  from userrole where name = 'HED Team B Manager' LIMIT 1];
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Sales User OneCRM'].Id,1);
        listWithUser[0].Market__c='AU';
        listWithUser[0].Line_of_Business__c='Higher Ed';
        listWithUser[0].UserRoleId = userRole[0].Id;
        listWithUser[0].Price_List__c = 'GHEPM AU HED ALL'; 
        System.runAs(listWithUser[0]){
            account acc = new Account(Name='Test Account1',IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'AU', Line_of_Business__c= 'Higher Ed', Geography__c= 'Core', ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371',SelfServiceCreated__c =True);
            if(acc.RecordType.Name != 'Learner'&& acc.RecordType.Name !='Account Creation Request'){
            insert acc;}
            List<RecordType> rt1 = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Opportunity' AND Name = 'Global Opportunity' LIMIT 1];  
            Opportunity opp = new opportunity ( Name = 'OpporName1',RecordTypeId= rt1[0].Id,AccountId = acc.Id,CloseDate = System.TODAY() + 30,StageName = 'OpporStageName', QuoteMigration_Qualification__c  = 'Graphic Design', Market__c ='AU', Line_of_Business__c = 'Higher Ed',Business_unit__c ='Higher Ed',Enrolments__c=2);
            insert opp;
            Contact con = new Contact(FirstName='TestContactFirstname1', LastName='TestContactLastname',AccountId=opp.accountid ,Salutation='MR.', Email='test@gmail.com',MobilePhone='111222333' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English',Preferred_Address__c = 'Other Address');  
                  //insert con;
            OpportunityContactRole oc = new OpportunityContactRole( opportunityId =  opp.Id,contactId = con.id ,Role =  'Business User');
                  //insert oc;
            
            Product2 prod = new Product2();
            prod.Configuration_Type__c='Option'; 
            prod.name = 'NA Test Product5';
            prod.Duration__c = 'D20';
            prod.Market__c ='AU';
            prod.Business_Unit__c='Higher Ed';
            prod.Line_of_Business__c = 'Higher Ed';           
            prod.Author__c = 'Test Author'; 
            prod.Edition__c = '10';
            prod.Medium2__c = 'Print';
            prod.Relevance_Value__c = 10;
            prod.Brand__c = null;    
            prod.Binding__c = 'Cloth';
            prod.IsActive = True;
            //prod.PIHE_Repeater__c = 'True' ;
            insert Prod;
            Id pricebookId = Test.getStandardPricebookId();
            //  Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true); 
            //  insert customPB;
            PricebookEntry pbe2 =new PricebookEntry(Product2Id=Prod.Id,Pricebook2Id=pricebookId,UnitPrice = 100.00,isActive=true,UseStandardPrice = false);
            insert pbe2;  
            /* 
            Apttus_Config2__PriceList__c  psList = new Apttus_Config2__PriceList__c ();
            psList.CurrencyISOCode = 'USD';
            psList.Name='Apttus_Config2__PriceList__c';
            insert psList;
            Apttus_Config2__PriceListItem__c psListItem= new Apttus_Config2__PriceListItem__c();
            psListItem.Apttus_Config2__PriceListId__c=psList.id;
            psListItem.Apttus_Config2__ProductId__c = Prod.Id;
            psListItem.Net_Price__c = 10.0;
            insert psListItem;
            */
            Catalog__c Catlg = new Catalog__c();
            Catlg.Name = 'GHEPM AU HED ALL';
            Insert Catlg;
            
            CatalogPrice__c CatlgPrice = new CatalogPrice__c();
            //CatlgPrice.Name = 'TestCP';
            CatlgPrice.Catalog__c = Catlg.Id;
            CatlgPrice.NetPrice__c = 143;
            CatlgPrice.Product__c = prod.Id;
            CatlgPrice.isActive__c = True;
            Insert CatlgPrice;
            
            OpportunityLineItem Oli = new OpportunityLineItem();
            Oli.OpportunityId = opp.id;
            Oli.Product2Id = Prod.Id;
            Oli.PricebookEntryId = pbe2.Id;
            Oli.Quantity = 11;
            Oli.sample__c = true;
            //oli.TotalPrice = 200.00;
            //System.debug('@@pbe2.UnitPrice '+pbe2.UnitPrice);
            Oli.TotalPrice = pbe2.UnitPrice;            
            Insert Oli;
            
            // oli.TotalPrice = 10;
            //update oli;
        }                
    }          
}