/**

  * Class : MergeAccountsAutomation
  * Description: Class called from Process Builderwhen an Account is updated and the field
  * 

  **/  
/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            MergeAccountsAutomation.Cls
   Description:     Class called from Process Builderwhen an Account is updated and the field   Account_Duplicate__c (winner) is not null. 
   Date             Version           Author                                Summary of Changes 
   -----------      ----------   -----------------      ---------------------------------------------------------------------------------------
   3/18/2019         0.1          C.Perez                CR-02606 Class to Merge Accounts. Receiving Loser with dupe field  has Winner
   09/04/2019        0.2            Manimekalai          CR-03029  unchecking primary addresses on loser account addresses
---------------------------------------------------------------------------------------------------------------------------------------------------*/
public class MergeAccountsAutomation {
    //Method to merge accounts
  @InvocableMethod
     public static List<Account> MergeAccountsAutomation(List<Account> accLst) {
       Account masterAccount = new Account();
        Account accountToMerge = new Account();
        Boolean sendemail = False;
        Integer recordsToMerge = 0;
        Integer lstSize = accLst.size();
        //Added for CR-03029 by Manimekalai -Start
        set<Id>accIdSet = new set<Id>();
        List<Account_Addresses__c>accAddList = new List<Account_Addresses__c>();
        List<Account_Addresses__c>listToUpdateAccAdd = new List<Account_Addresses__c>();
        //CR-03029 -End
         if (lstSize < 51){
             recordsToMerge = lstSize;
         }else{
             recordsToMerge = 51;
             sendemail = true;
         }
         //Added for CR-03029 by Manimekalai -Start
         for(Integer i=0; i<recordsToMerge; i++) {
             accIdSet.add(accLst.get(i).Id);//get the Loser account ID
         }
         //fetch account address records in which Primary addresses set to TRUE for the loser account 
         accAddList= [Select Id,Account__c FROM Account_Addresses__c where Account__c IN :accIdSet AND (Primary__c =true or Primary_Physical__c = true OR PrimaryShipping__c = true) ];
            //Set Primary addresses to false for the account addresses records for the loser account
            if(accAddList.size() > 0){
                for(Account_Addresses__c accAdd:accAddList) {
                    accAdd.Primary__c =false;
                    accAdd.Primary_Physical__c =false;
                    accAdd.PrimaryShipping__c =false;
                    listToUpdateAccAdd.add(accAdd);
                }
            }
          //CR-03029 - End
         for(Integer i=0; i<recordsToMerge; i++) {
             
             accountToMerge = accLst.get(i);
             //System.debug('accountToMerge: '+ accountToMerge);
             // Set the Ids of the master account 
             masterAccount.Id = accountToMerge.Account_Duplicate__c;
                            
             try{ 
                //Added  by Manimekalai for CR-03029 - Start
                //Update the Accountaddressess before merging loser accounts
                if(listToUpdateAccAdd.size() > 0){
                     update listToUpdateAccAdd;
                }
                //CR-03029 - End
                 merge masterAccount accountToMerge;
             }catch(DmlException e){
                 System.debug('An unexpected error has occurred: ' + e.getMessage());
             }  //try
         }
         if (sendemail){
             
             //System.debug('account list bigger than 50');
             //get the list of emails
                          
             Group OneCRMGroup = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name = 'CRM Operations'];
             List<id> idList = new List<id>();
             for (GroupMember gm : OneCRMGroup.groupMembers) {
                 idList.add(gm.userOrGroupId);
             } 
             
             //send email to CRM Operations Group when reached 51
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
             //Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
             
             mail.setSenderDisplayName('One CRM Support');
             //Getting template
             EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE DeveloperName ='Account_Merge_Issue'];
             mail.setTemplateId(et.id);
             mail.setSaveAsActivity(false);
             // Assign the addresses for the To and CC lists to the mail object.
            
             for (ID usrId : idList){
                 mail.setTargetObjectId(usrId);
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
             }  
             
         }//lstSize >50
        return accLst;
  }//constructor
     
}