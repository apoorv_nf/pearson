/************************************************************************************************************
* Apex Class Name   : PS_GenricTriggerHandler.cls
* Version           : 1.0 
* Created Date      : 23 Sept 2015
* Function          : Genric Handler to invoke different objects Handler
* Additional Info   : All the Event methods shall remain empty , when required invoking of business logic can be done in respective event method
					  
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Leonard Victor           23/09/2015				Handler Class for AccountLandScape
************************************************************************************************************/

public class PS_GenricTriggerHandler {
    private PS_GenricTriggerInterface triggerHandler;


    public PS_GenricTriggerHandler(String className) {
        try {
                triggerHandler = (PS_GenricTriggerInterface) Type.forName(PS_Constants.GENRIC_NAMING + className + PS_Constants.GENRIC_NAMING1).newInstance();
        } catch (Exception caughtException) {
            // TODO: ADD PROPER EXCEPTION HANDLING
            SYSTEM.DEBUG('CAUGHT EXCEPTION : ' + caughtException);
        }
    }
    
    public void beforeInsert(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew) {
        triggerHandler.beforeInsert(lTriggerNew, mTriggerNew);
    }
    
    public void afterInsert(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew) {
        triggerHandler.afterInsert(lTriggerNew, mTriggerNew);
    }
    
    public void beforeUpdate(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) {
        triggerHandler.beforeUpdate(lTriggerNew, mTriggerNew, lTriggerOld, mTriggerOld);
    }
    
    public void afterUpdate(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) {
        triggerHandler.afterUpdate(lTriggerNew, mTriggerNew, lTriggerOld, mTriggerOld);
    }
    
    public void beforeDelete(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) {
        triggerHandler.beforeDelete(lTriggerNew, mTriggerNew, lTriggerOld, mTriggerOld);
    }
    
    public void afterDelete(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) {
        triggerHandler.afterDelete(lTriggerNew, mTriggerNew, lTriggerOld, mTriggerOld);
    }
    
    public void afterUndelete(List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) {
        triggerHandler.afterUndelete(lTriggerOld, mTriggerOld);
    }
}