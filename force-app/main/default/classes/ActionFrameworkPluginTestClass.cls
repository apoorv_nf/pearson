/*******************************************************************************************************************
* Apex Class Name  : ActionFrameworkPluginTestClass
* Version          : 1.0  
* Created Date     : 19 March 2015
* Function         : Test Class of the ActionFrameworkPlugin Class
* Modification Log :
*
* Developer                   Date                    Description
* ------------------------------------------------------------------------------------------------------------------
*                             19/03/2015              Created Initial Version
--------------------------------------------------------------------------------------------------------------------
Date            Version             Developer                   Descripton 
--------    ----------------    ---------------------       ----------------------------------
21/12/2015      1.1              Rahul Boinepally           Modified LeadActionsInsertionTest method to store Market of Lead from Market of Account. 

*******************************************************************************************************************/
@isTest (seeAllData=true)                  
private class ActionFrameworkPluginTestClass
{
    /*************************************************************************************************************
    * Name        : AccountActionsInsertionTest
    * Description : Verify the insertion of actions related to an account using the Action Framework Plugin    
    * Input       : 
    * Output      : 
    *************************************************************************************************************/
    static testMethod void AccountActionsInsertionTest()
    {
        //Generate an account
        Account acc = new Account(Name = 'Account 1', ShippingCity ='Shipping City', ShippingCountry = 'United Kingdom', ShippingStreet = 'Shipping Street', ShippingPostalCode = 'NE27 0QQ');
        insert acc;
    
        //Generate Action Template Items
        List<ActionTemplateItem__c> ActionTemplateItemsInserted1 = GenerateActionTemplateItems(10, 'Account', 'User', 30,'Template 1');
        List<ActionTemplateItem__c> ActionTemplateItemsInserted2 = GenerateActionTemplateItems(10, 'Account', 'Queue', 30,'Template 1');
        List<ActionTemplateItem__c> ActionTemplateItemsInserted3 = GenerateActionTemplateItems(10, 'Account', '', 30,'Template 1');
        
        List<ActionTemplateItem__c> ActionTemplateItemsInserted = new List<ActionTemplateItem__c>();
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted1);
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted2);
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted3);
        
        // Define Plugin input and output parameters
        ActionFrameworkPlugin actionPlugin = new ActionFrameworkPlugin();
        Map<String,Object> inputParams = new Map<String,Object>();
        Map<String,Object> outputParams = new Map<String,Object>();

        inputParams.put('TemplateName','Template 1');
        inputParams.put('ParentObjectType', 'Account');
        inputParams.put('ParentObjectID', acc.Id);

        // Execute the plugin
        test.startTest();
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        Process.PluginResult result;
        result = actionPlugin.invoke(request);
        test.stopTest();

        System.debug('ERROR MESSAGE '+result.OutputParameters.get('ResultMessage'));
        
        // Check that the plugin execution is successfull
        System.Assert(Boolean.valueOf(result.OutputParameters.get('ActionsInserted')),true);
        System.assertEquals(String.valueOf(result.OutputParameters.get('ResultMessage')),'Success');
    }  
    
    /*************************************************************************************************************
    * Name        : ContactActionsInsertionTest
    * Description : Verify the insertion of actions related to a contact using the Action Framework Plugin    
    * Input       : 
    * Output      : 
    *************************************************************************************************************/
    static testMethod void ContactActionsInsertionTest()
    {
        //Generate an account
        Account acc = new Account(Name = 'Account 1', ShippingCity ='Shipping City', ShippingCountry = 'United Kingdom', ShippingStreet = 'Shipping Street', ShippingPostalCode = 'NE27 0QQ');
        insert acc;
        
        //Generate contact
        RecordType rtcontact = [SELECT Id,Name FROM RecordType WHERE SobjectType='Contact' and DeveloperName = 'Global_Contact'];
        Contact con = new Contact(FirstName ='Contact1' ,LastName = 'Contact 1', Email = 'test@test.com.demo', AccountId = acc.Id, RecordTypeId= rtcontact.Id,
                                  Preferred_Address__c = 'Mailing Address', MailingCountry = 'United Kingdom', MailingStreet = '1 Street', MailingPostalCode = 'NE27 0QQ', MailingCity  = 'City');
        insert con;
    
        //Generate Action Template Items
        List<ActionTemplateItem__c> ActionTemplateItemsInserted1 = GenerateActionTemplateItems(10, 'Contact', 'User', 30,'Template 1');
        List<ActionTemplateItem__c> ActionTemplateItemsInserted2 = GenerateActionTemplateItems(10, 'Contact', 'Queue', 30,'Template 1');
        
        List<ActionTemplateItem__c> ActionTemplateItemsInserted = new List<ActionTemplateItem__c>();
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted1);
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted2);
        
        // Define Plugin input and output parameters
        ActionFrameworkPlugin actionPlugin = new ActionFrameworkPlugin();
        Map<String,Object> inputParams = new Map<String,Object>();
        Map<String,Object> outputParams = new Map<String,Object>();

        inputParams.put('TemplateName','Template 1');
        inputParams.put('ParentObjectType', 'Contact');
        inputParams.put('ParentObjectID', con.Id);

        // Execute the plugin
        test.startTest();
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        Process.PluginResult result;
        result = actionPlugin.invoke(request);
        test.stopTest();

        System.debug('ERROR MESSAGE '+result.OutputParameters.get('ResultMessage'));
        
        // Check that the plugin execution is successfull
        System.assertEquals(Boolean.valueOf(result.OutputParameters.get('ActionsInserted')),true);
        System.assertEquals(String.valueOf(result.OutputParameters.get('ResultMessage')),'Success');
    }  
    
    /*************************************************************************************************************
    * Name        : CaseActionsInsertionTest
    * Description : Verify the insertion of actions related to a case using the Action Framework Plugin    
    * Input       : 
    * Output      : 
    *************************************************************************************************************/
    static testMethod void CaseActionsInsertionTest()
    {
        //Generate an account
        Account acc = new Account(Name = 'Account 1', ShippingCity ='Shipping City', ShippingCountry = 'United Kingdom', ShippingStreet = 'Shipping Street', ShippingPostalCode = 'NE27 0QQ');
        insert acc;
        
        //Generate contact
        RecordType rtcontact = [SELECT Id,Name FROM RecordType WHERE SobjectType='Contact' and DeveloperName = 'Global_Contact'];
        Contact con = new Contact(FirstName ='Contact1' ,LastName = 'Contact 1', Email = 'test@test.com.demo', AccountId = acc.Id, RecordTypeId= rtcontact.Id, 
                                  Preferred_Address__c = 'Mailing Address', MailingCountry = 'United Kingdom', MailingStreet = '1 Street', MailingPostalCode = 'NE27 0QQ', MailingCity  = 'City');
        insert con;
        
        //Generate case
        RecordType rtcase = [SELECT Id,Name FROM RecordType WHERE SobjectType='Case' and DeveloperName = 'Loan_Bursary_Request'];
        Case c = new Case( AccountId = acc.Id, ContactId = con.Id, RecordTypeId = rtcase.Id);
        insert c;
    
        //Generate Action Template Items
        List<ActionTemplateItem__c> ActionTemplateItemsInserted1 = GenerateActionTemplateItems(10, 'Case', 'User', 30,'Template 1');
        List<ActionTemplateItem__c> ActionTemplateItemsInserted2 = GenerateActionTemplateItems(10, 'Case', 'Queue', 30,'Template 1');
        
        List<ActionTemplateItem__c> ActionTemplateItemsInserted = new List<ActionTemplateItem__c>();
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted1);
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted2);
        
        // Define Plugin input and output parameters
        ActionFrameworkPlugin actionPlugin = new ActionFrameworkPlugin();
        Map<String,Object> inputParams = new Map<String,Object>();
        Map<String,Object> outputParams = new Map<String,Object>();

        inputParams.put('TemplateName','Template 1');
        inputParams.put('ParentObjectType', 'Case');
        inputParams.put('ParentObjectID', c.Id);

        // Execute the plugin
        test.startTest();
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        Process.PluginResult result;
        result = actionPlugin.invoke(request);
        test.stopTest();

        System.debug('ERROR MESSAGE '+result.OutputParameters.get('ResultMessage'));
        
        // Check that the plugin execution is successfull
        System.assertEquals(Boolean.valueOf(result.OutputParameters.get('ActionsInserted')),true);
        System.assertEquals(String.valueOf(result.OutputParameters.get('ResultMessage')),'Success');
    }  
    
    /*************************************************************************************************************
    * Name        : LeadActionsInsertionTest
    * Description : Verify the insertion of actions related to a lead using the Action Framework Plugin    
    * Input       : 
    * Output      : 
    *************************************************************************************************************/
    static testMethod void LeadActionsInsertionTest()
    {

        //Generate Lead
        
        //Account school = [SELECT Id, Market2__c FROM Account WHERE Name ='Unknown School'limit 1];
        User usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Price_List__c= 'Math & Science');

        usr.market__c='UK';
        usr.Business_Unit__c='Schools';
        usr.Line_of_Business__c='Schools';
        usr.Group__c='Primary';
        usr.License_Pools__c='Test User';
        insert usr;
       
        System.assert(usr.id != null, 'User creation failed');        


        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true);
        insert byp;
        System.runas(usr){
        RecordType accrt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account' AND Name ='School'];    
        Account school = new Account(Name='Test B2B R6 School',Market2__c='UK',Line_of_Business__c='Schools',Business_Unit__c='Schools',Group__c='Primary',Organisation_Type__c ='School',Type__c='Secondary',Sub_Type__c='Academy Second',
                        Phone='+91000001', ShippingCountry = 'United kingdom',ShippingState = 'greater london', ShippingCity = 'london', ShippingStreet = 'BNG', ShippingPostalCode = 'SE27 0AA',billingstreet='BNG', billingcity='London', billingstate='Greater london',billingcountry='United Kingdom',billingpostalcode='SE27 0AA');  

        school.recordtypeid=accrt.Id;
        school.ShippingCountry='United kingdom';
        system.runas(usr){
        insert school;
        
        
        RecordType rtlead = [SELECT Id,Name FROM RecordType WHERE SobjectType='Lead' and DeveloperName = 'D2L'];
        Lead lead = new Lead( Company = 'Account 1', LastName = 'Contact 1', RecordTypeId= rtlead.Id, Country_of_Origin__c ='South Africa', Market__c = school.Market2__c, Line_of_Business__c = school.Line_of_Business__c, Business_Unit__c = school.Business_Unit__c, Geography__c ='Core' , Institution_Organisation__c = school.Id, Preferred_Campus__c ='Durban', Preferred_Contact_Method__c ='Email', Email='xxx@xxx.com');                
        
        insert lead;
        }
        system.debug(lead.market__c);
    
        //Generate Action Template Items
        List<ActionTemplateItem__c> ActionTemplateItemsInserted1 = GenerateActionTemplateItems(10, 'Lead', 'User', 30,'Template 1');
        List<ActionTemplateItem__c> ActionTemplateItemsInserted2 = GenerateActionTemplateItems(10, 'Lead', 'Queue', 30,'Template 1');
        
        List<ActionTemplateItem__c> ActionTemplateItemsInserted = new List<ActionTemplateItem__c>();
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted1);
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted2);
        
        // Define Plugin input and output parameters
        ActionFrameworkPlugin actionPlugin = new ActionFrameworkPlugin();
        Map<String,Object> inputParams = new Map<String,Object>();
        Map<String,Object> outputParams = new Map<String,Object>();
        String sLeadId=''+ lead.Id;
        inputParams.put('TemplateName','Template 1');
        inputParams.put('ParentObjectType', 'Lead');
        inputParams.put('ParentObjectID', sLeadId);

        // Execute the plugin
        test.startTest();
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        Process.PluginResult result;
        result = actionPlugin.invoke(request);
        test.stopTest();

        System.debug('ERROR MESSAGE '+result.OutputParameters.get('ResultMessage'));
        
        // Check that the plugin execution is successfull
        //System.assertEquals(Boolean.valueOf(result.OutputParameters.get('ActionsInserted')),true);
        //System.assertEquals(String.valueOf(result.OutputParameters.get('ResultMessage')),'Success');
    }
    }
    
    /*************************************************************************************************************
    * Name        : OpportunityActionsInsertionTest
    * Description : Verify the insertion of actions related to an opportunity using the Action Framework Plugin    
    * Input       : 
    * Output      : 
    *************************************************************************************************************/
    static testMethod void OpporutniyActionsInsertionTest()
    {
        //Generate an account
        Account acc = new Account(Name = 'Account 1', ShippingCity ='Shipping City', ShippingCountry = 'United Kingdom', ShippingStreet = 'Shipping Street', ShippingPostalCode = 'NE27 0QQ');
        insert acc;
        
        //Generate opportunity
        RecordType rtopp = [SELECT Id,Name FROM RecordType WHERE SobjectType='Opportunity' and DeveloperName = 'D2L'];
        Opportunity opp = new Opportunity( Name = 'Opportunity', StageName = 'Prospecting', AccountId = acc.Id, CloseDate = System.Today(),RecordTypeId = rtopp.Id);
        insert opp;
    
        //Generate Action Template Items
        List<ActionTemplateItem__c> ActionTemplateItemsInserted1 = GenerateActionTemplateItems(10, 'Opportunity', 'User', 30,'Template 1');
        List<ActionTemplateItem__c> ActionTemplateItemsInserted2 = GenerateActionTemplateItems(10, 'Opportunity', 'Queue', 30,'Template 1');
        
        List<ActionTemplateItem__c> ActionTemplateItemsInserted = new List<ActionTemplateItem__c>();
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted1);
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted2);
        
        // Define Plugin input and output parameters
        ActionFrameworkPlugin actionPlugin = new ActionFrameworkPlugin();
        Map<String,Object> inputParams = new Map<String,Object>();
        Map<String,Object> outputParams = new Map<String,Object>();

        inputParams.put('TemplateName','Template 1');
        inputParams.put('ParentObjectType', 'Opportunity');
        inputParams.put('ParentObjectID', opp.Id);

        // Execute the plugin
        test.startTest();
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        Process.PluginResult result;
        result = actionPlugin.invoke(request);
        test.stopTest();

        System.debug('ERROR MESSAGE '+result.OutputParameters.get('ResultMessage'));
        
        // Check that the plugin execution is successfull
        System.assertEquals(Boolean.valueOf(result.OutputParameters.get('ActionsInserted')),true);
        System.assertEquals(String.valueOf(result.OutputParameters.get('ResultMessage')),'Success');
    } 
    
    /*************************************************************************************************************
    * Name        : ErrorActionsInsertionTest
    * Description : Verify the error handle of an insertion of actions related to an account using the Action Framework Plugin    
    * Input       : 
    * Output      : 
    *************************************************************************************************************/
    static testMethod void ErrorActionsInsertionTest()
    {
        //Generate Action Template Items
        List<ActionTemplateItem__c> ActionTemplateItemsInserted1 = GenerateActionTemplateItems(10, 'Opportunity', 'User', 30,'Template 1');
        List<ActionTemplateItem__c> ActionTemplateItemsInserted2 = GenerateActionTemplateItems(10, 'Opportunity', 'Queue', 30,'Template 1');
        
        List<ActionTemplateItem__c> ActionTemplateItemsInserted = new List<ActionTemplateItem__c>();
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted1);
        ActionTemplateItemsInserted.addAll(ActionTemplateItemsInserted2);
        
        // Define Plugin input and output parameters
        ActionFrameworkPlugin actionPlugin = new ActionFrameworkPlugin();
        Map<String,Object> inputParams = new Map<String,Object>();
        Map<String,Object> outputParams = new Map<String,Object>();

        inputParams.put('TemplateName','Template 1');
        inputParams.put('TemplateName2','Template 2');
        inputParams.put('ParentObjectType', 'Opportunity');
        inputParams.put('ParentObjectID', '');

        // Execute the plugin
        test.startTest();
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        Process.PluginResult result;
        result = actionPlugin.invoke(request);
        test.stopTest();

        System.debug('ERROR MESSAGE '+result.OutputParameters.get('ResultMessage'));
        
        // Check that the plugin execution is successfull
        System.assertEquals(Boolean.valueOf(result.OutputParameters.get('ActionsInserted')), false);
    } 
    
    /*************************************************************************************************************
    * Name        : describeTest
    * Description : Verify the definition of the Action Framework Plugin interface
    * Input       : 
    * Output      : 
    *************************************************************************************************************/
    static testMethod void describeTest() {

        ActionFrameworkPlugin actionPlugin = new ActionFrameworkPlugin();
        
        Process.PluginDescribeResult result = actionPlugin.describe();
        
        System.AssertEquals(result.inputParameters.size(), 4);
        System.AssertEquals(result.OutputParameters.size(), 2);
        System.AssertEquals(result.description ,  'The Action Framework plugin generate action records related with the parent object provided based on the specified action template definition');
        System.AssertEquals(result.tag , 'Action Framework');
        
     }
    
    public static List<ActionTemplateItem__c> GenerateActionTemplateItems(Integer NumberOfActionTemplateItems, String ParentObjectType, String OwnerType, Integer TimeToComplete, String TemplateName)
    {
        List<ActionTemplateItem__c> actTempItemsList = new List<ActionTemplateItem__c>();
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        List<User> ActiveUsers = [SELECT Id FROM User WHERE IsActive = true limit 10];
        
        System.runAs (thisUser){
            // Create a Queue
            GenerateQueue('Action__c', 'Queue 1');
            
            for(Integer i=0; i<NumberOfActionTemplateItems; i++)
            {
                ActionTemplateItem__c tempItem = new ActionTemplateItem__c();
                
                tempItem.ParentObjectType__c = ParentObjectType;
                tempItem.TemplateName__c = TemplateName;
                tempItem.Sequence__c = (i+1);
                tempItem.Title__c = 'Title ' + (i+1);
                tempItem.Description__c = 'Description ' + (i+1);
                tempItem.TimeToComplete__c = TimeToComplete;
                
                if(OwnerType == 'User')
                {
                    tempItem.AssignToType__c = OwnerType;
                    tempItem.User__c = ActiveUsers.get(0).Id;
                }
                else if(OwnerType == 'Queue')
                {
                    tempItem.AssignToType__c = OwnerType;
                    tempItem.QueueName__c = 'Queue 1';
                }
                
                actTempItemsList.add(tempItem);
            }
            
            if(actTempItemsList.size()>0){
                insert actTempItemsList;
            }
        }
                
        return actTempItemsList;
    }
    
    public static void GenerateQueue(String ObjectType, String QueueName)
    {
        Group g = new Group(Name=QueueName, Type='Queue');
        insert g;
            
        QueuesObject q = new QueueSObject(QueueID = g.Id, SobjectType = ObjectType);
        insert q;
    }
}