@isTest
public class LC_JSMQueryResultServiceTest {
    @isTest
    static public void executeQueryOneObjectTest(){
        //do the query
        String theQuery = 'SELECT Id FROM User LIMIT 1';
        User user = (User)LC_JSMQueryResultService.executeQueryOneObject(theQuery);
        
        //compare the result
        System.assertNotEquals(null, user);
    }

    @isTest
    static public void loadObjectInfoByIdTest(){
        try{
            Task testTask = new Task();
            testTask.Subject = 'Subject Test';
            testTask.Priority = 'Normal';
            testTask.Status = 'Completed';

            Test.startTest();
                insert testTask;
            Test.stopTest();

            Task tsk = (Task)LC_JSMQueryResultService.loadObjectInfoById(testTask.Id);

            System.assertEquals(testTask.Id, tsk.Id);
            System.assertEquals(testTask.Subject, tsk.Subject);
            System.assertEquals(testTask.Priority, tsk.Priority);
            System.assertEquals(testTask.Status, tsk.Status);

        } catch(AuraHandledException ex) {
            System.assert(false);
        }
    }

}