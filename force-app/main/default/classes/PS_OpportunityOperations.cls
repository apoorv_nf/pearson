/************************************************************************************************************
* Apex Class Name   : PS_OpportunityOperations.cls
* Version           : 1.0
* Function          : Business logic realted to opportunity
* Modification Log  :
* Developer                   Date                   Description
* -----------------------------------------------------------------------------------------------------------
* Leonard Victor              24/08/2015            RD-01257  Moved business logic from handler class to separate business class
* Manimala                    10/Apr/2018           CR-01459-Req-14: Set up Pricebooks [auto populate the pricebook based on User Price List
* Manimala                    21/Jun/2018           CR-01459-Req-14: added condition to restrict for only CA ERPI & Schools business unit
************************************************************************************************************/

public class PS_OpportunityOperations {
   public static User loggedUser;
    
    // Rd: 153 | Pooja 
     // If Contract exist on an account and Opp is getting created on same account then Copy fields on Opportunity.
     public static void copyFieldsOnOpportuity(List<Opportunity> opptyList){
        //Moving record type check from trigger to class
        List<Opportunity> oppList = new List<Opportunity>();
        
        //Using Util class to fetch record type instead of SOQL or Mutiple describe calls
            Id d2lRecordType;
        
            if(PS_Util.recordTypeMap.containsKey(PS_Constants.OPPORTUNITY_OBJECT) && PS_Util.recordTypeMap.get(PS_Constants.OPPORTUNITY_OBJECT).containsKey(PS_Constants.OPPORTUNITY_D2L_RECCORD)){                          
                    d2lRecordType = PS_Util.recordTypeMap.get(PS_Constants.OPPORTUNITY_OBJECT).get(PS_Constants.OPPORTUNITY_D2L_RECCORD); 
            }
            else{
                    d2lRecordType = PS_Util.fetchRecordTypeByName(Opportunity.SobjectType,PS_Constants.OPPORTUNITY_D2L_RECCORD);
            }  
           //End of Record type fix    
           
                
        for(Opportunity oppObj : opptyList){
            if(oppObj.RecordTypeId==d2lRecordType)
                oppList.add(oppObj);
        }
        
            if(!oppList.isEmpty()){
             Map<Id,Id> oppMap = new Map<Id,Id>();
             Set<Id> accIdSet = new Set<Id>();
             List<Opportunity> OpportunityList = new List<Opportunity>();
             try{
                 for(Opportunity op : oppList){
                     oppMap.put(op.Id,op.AccountId);
                     accIdSet.add(op.AccountId);
                 }
                 //List<Contract> conList = [select Id,AccountId, Opportunity__c,Opportunity__r.Student_Registered__c,Opportunity__r.Registration_Payment_Reference__c,Opportunity__r.Received_Signed_Registration_Contract__c from Contract where AccountId in: accIdSet];
                List<Contract> conList = [select Id,AccountId, Opportunity__c,Opportunity__r.Student_Registered__c,Opportunity__r.Registration_Payment_Reference__c,Opportunity__r.Received_Signed_Registration_Contract__c from Contract where AccountId in: accIdSet];
                Map<Id,Contract> conMap = new Map<Id,Contract>();
                For(Contract con : conList){
                    conMap.put(con.AccountId,con);
                }
                For(Id i : oppMap.keySet()){
                    Id accountId = oppMap.get(i);
                    Contract crec = conMap.get(accountId);
                    Opportunity op = new Opportunity(Id=i);
                    if(crec != null)
                    {                
                    op.Student_Registered__c=crec.Opportunity__r.Student_Registered__c;
                    op.Registration_Payment_Reference__c = crec.Opportunity__r.Registration_Payment_Reference__c;
                    //op.Registration_Payment_Reference1__c = crec.Opportunity__r.Registration_Payment_Reference1__c;
                    op.Received_Signed_Registration_Contract__c = crec.Opportunity__r.Received_Signed_Registration_Contract__c;
                    }
                    OpportunityList.add(op);
                }
                if(!OpportunityList.isEmpty())
                    update OpportunityList;
           }
          Catch(Exception e){
          }
        }
     }
     
     
      // Rd: 153 | Pooja
    // If Opp is getting updated and Student Registration is checked then create a contract on this opportunity.
    // //CP - CR-02085 Do not create Contracts fo ZA, but simply update a field on the Account record
    public static void createContract(List<Opportunity> opptyList){
        //Moving record type check from trigger to class
        List<Opportunity> oppList = new List<Opportunity>();
        
        //Using Util class to fetch record type instead of SOQL or Mutiple describe calls
        Id d2lRecordType;
        
        if(PS_Util.recordTypeMap.containsKey(PS_Constants.OPPORTUNITY_OBJECT) && PS_Util.recordTypeMap.get(PS_Constants.OPPORTUNITY_OBJECT).containsKey(PS_Constants.OPPORTUNITY_D2L_RECCORD)){                          
            d2lRecordType = PS_Util.recordTypeMap.get(PS_Constants.OPPORTUNITY_OBJECT).get(PS_Constants.OPPORTUNITY_D2L_RECCORD); 
        }
        else{
            d2lRecordType = PS_Util.fetchRecordTypeByName(Opportunity.SobjectType,PS_Constants.OPPORTUNITY_D2L_RECCORD);
        }  
        //End of Record type fix    
        for(Opportunity oppObj : opptyList){
            if(oppObj.RecordTypeId==d2lRecordType)
                oppList.add(oppObj);
        }
        if(!oppList.isEmpty()){
            try{
                List<Id> acctRegisteredLst = new List<Id>(); 
                For(Opportunity op : oppList){
                    if(op.Student_Registered__c == True){
                        acctRegisteredLst.add(op.AccountId);
                    }
                }
                
                if (acctRegisteredLst.size() > 0){
                
                    List <Account> acctContrList = [select id, CTIPIHE_Contract_Signed_Date__c from Account where  CTIPIHE_Contract_Signed_Date__c= null and id in :acctRegisteredLst];
                        
                    if (acctContrList.size() >0){
                        for (Account newSignedCntrct : acctContrList){ newSignedCntrct.CTIPIHE_Contract_Signed_Date__c = system.today();
                        }
                        update acctContrList;
                        
                    }
                }
            }Catch(Exception e){
            }
        }
    }
    
    
    public static void beforeUpdatevalidation(List<Opportunity> newopp,Map<Id,Opportunity> mapNewOppty, Map<Id,opportunity> mapOldoppty)
    {
        List<PS_OpportunityValidationInterface> validations =PS_OpportunityValidationFactory.CreateOpptyValidations(newopp,mapNewOppty,mapOldoppty);
        Map<String,List<String>> exceptions = new Map<String,List<String>>();

        for(PS_OpportunityValidationInterface validation: validations)
        {
            validation.opptyvalidateUpdate(exceptions);
        }

        if( ! exceptions.isEmpty())
        {
            if (mapNewOppty != null) PS_Util.addErrors_1(mapNewOppty,exceptions);
            else{
                PS_Util.addErrorsusinglistindex(newopp,exceptions); } return;
        }        
    }

     
     public static void setPricebookforglobaloppty(List<Opportunity> newOppLst){
     
         //Using Util class to fetch logged in user record
         
        // CR-01459-Req-14 code changes starts
        
            if(!PS_Util.isFetchedUserRecord){
                PS_Util.getUserDetail(Userinfo.getUserid());
                
            } 
            loggedUser = PS_Util.loggedInUser;
            
         // CR-01459-Req-14 code changes ends
            
         Map<string,Id> Pricebookmap = new Map<string,Id>();
         for(Pricebook2 eachpricebook: [Select Id ,Name from Pricebook2 where IsActive = true]){
             Pricebookmap.put(eachpricebook.Name, eachpricebook.Id);
         }
                
         for(Opportunity eachopptyObj : newOppLst){
             if(eachopptyObj.RecordTypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Global_Opportunity).getRecordTypeId() && !eachopptyObj.Opportunity_With_Product__c && loggedUser != null ){
                  List<Proposal_Pricelist_Mapping__mdt> ProposalPriceListMapping = new List<Proposal_Pricelist_Mapping__mdt>();
                   //Jun- 21st:- CR-01459-Req-14 code changes starts
                   if(loggedUser.Market__c =='CA' && (loggedUser.Business_Unit__c=='ERPI' || (loggedUser.Business_Unit__c=='Schools' && loggedUser.Price_List__c=='CA Schools ALL')))
                   {
                        ProposalPriceListMapping = [SELECT Id,Oppty_Currency__c,Oppty_Record_Type__c,Price_List_Name__c,QualifiedApiName,Rep_Business_Unit__c,Rep_Market__c,Rep_Line_of_Business__c, Price_Book__c 
                                                     FROM Proposal_Pricelist_Mapping__mdt 
                                                     where Oppty_Record_Type__c = 'Global Opportunity' AND
Rep_Line_of_Business__c =: eachopptyObj.Line_of_Business__c AND Rep_Business_Unit__c =: eachopptyObj.Business_unit__c AND Price_List_Name__c =:loggedUser.Price_List__c AND  // added for CR-01459-Req-14
                                                     Rep_Market__c =: eachopptyObj.Market__c LIMIT 1];
                     
                    }                                                                                      
                    else
                    {
                        ProposalPriceListMapping = [SELECT Id,Oppty_Currency__c,Oppty_Record_Type__c,Price_List_Name__c,QualifiedApiName,Rep_Business_Unit__c,Rep_Market__c,Rep_Line_of_Business__c, Price_Book__c 
                                                     FROM Proposal_Pricelist_Mapping__mdt 
                                                     where Oppty_Record_Type__c = 'Global Opportunity' AND
                                                     Rep_Line_of_Business__c =: eachopptyObj.Line_of_Business__c AND
                                                     Rep_Business_Unit__c =: eachopptyObj.Business_unit__c AND
                                                     Rep_Market__c =: eachopptyObj.Market__c LIMIT 1];
                    }
                     //Jun- 21st:- CR-01459-Req-14 code changes ends
                   if(ProposalPriceListMapping != null && !ProposalPriceListMapping.isEmpty() && ProposalPriceListMapping[0].Price_Book__c != null && Pricebookmap.get(ProposalPriceListMapping[0].Price_Book__c) != null ){
                       eachopptyObj.Pricebook2Id = Pricebookmap.get(ProposalPriceListMapping[0].Price_Book__c);
                   }
                    
             }
         }
     }

    
}