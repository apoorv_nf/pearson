/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_UpdateAccountChildTeamsController_Test
   Description:     Test class for PS_UpdateAccountChildTeamsController
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   21 oct 2015      1.0           Accenture NDC                                         Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */

@isTest
public class PS_UpdateAccountChildTeamsControllerTest
{
    static testmethod void testChildAccountTeam()
    {
        List<Account> lstWithAccount = new List<Account>();
        List<Account> lstWithChildAccount = new List<Account>(); 
        List<AccountTeamMember> lstWithAcctTeamMember = new List<AccountTeamMember>(); 
        List<AccountTeamMember> lstWithParentAcctTeamMember = new List<AccountTeamMember>();
        List<AccountTeamMember> lstWithAcctTeamMemberSec = new List<AccountTeamMember>();
        List<AccountShare> updatAccShare = new List<AccountShare>();
        Profile profileId = [select id from profile where Name = 'System Administrator'];
		
		lstWithAccount = TestDataFactory.createAccount(1,'Organisation');
        lstWithChildAccount = TestDataFactory.createAccount(1,'Organisation');
        
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=UserInfo.getProfileId(),Disable_Triggers__c=true,Disable_Process_Builder__c=true);
        insert byp;
		
        Bypass_Settings__c byp1 = [select id,Disable_Triggers__c,Disable_Process_Builder__c from Bypass_Settings__c where SetupOwnerId=:UserInfo.getProfileId()];
        byp1.Disable_Triggers__c = false;
        byp1.Disable_Validation_Rules__c = true;
        byp1.Disable_Process_Builder__c=true;
        upsert byp1;
        //create the account using TestDataFactory class
      
        lstWithAcctTeamMember = TestDataFactory.getAccountTeamMembers();
        lstWithParentAcctTeamMember = TestDataFactory.getAccountTeamMembers();
        lstWithAcctTeamMemberSec = TestDataFactory.getAccountTeamMembers();
        //Update the parent account as primary selling account
        
        
        test.startTest(); 
        //All insertion takes place inside start and stop test
        insert lstWithAccount;
        lstWithChildAccount[0].ParentId = lstWithAccount[0].Id;
        lstWithParentAcctTeamMember[0].AccountId = lstWithAccount[0].Id;
        lstWithAccount[0].Primary_Selling_Account_check__c = true;
        insert lstWithChildAccount;
        for(AccountTeamMember acctTeam : lstWithAcctTeamMember)
        {
        	acctTeam.AccountId = lstWithChildAccount[0].Id;
        }
        insert lstWithAcctTeamMember;
        insert lstWithTestUser;
        insert lstWithParentAcctTeamMember;
        insert lstWithAcctTeamMemberSec;
        
        updatAccShare = [select Accountaccesslevel,caseaccesslevel,opportunityaccesslevel,contactaccesslevel from AccountShare where AccountId =: lstWithAcctTeamMember[0].AccountId And UserOrGroupId =: lstWithAcctTeamMember[0].UserId];
        if(updatAccShare.size()>0)
        {
	        updatAccShare[0].Accountaccesslevel = 'Edit';
	        updatAccShare[0].caseaccesslevel = 'Edit';
	        updatAccShare[0].opportunityaccesslevel = 'Edit';
	        updatAccShare[0].contactaccesslevel = 'Edit';
        }
        
        update updatAccShare;
       
        test.stopTest();
        System.runAs(lstWithTestUser[0]) 
        {
        	//Test the class in 'PS_UpdateAccountChildTeams' page context
        	PageReference pageRef = new PageReference('/apex/PS_UpdateAccountChildTeams?accountId='+lstWithAccount[0].Id);
        	Test.setCurrentPage(pageRef);
        	ApexPages.StandardController sc = new ApexPages.StandardController(lstWithAccount[0]);
        	PS_UpdateAccountChildTeamsController controllerObj = new PS_UpdateAccountChildTeamsController(sc);
        	
        	//make sure that the parentaccountid is same as the accountid passed in url
        	system.assertequals(controllerObj.parentAccountId,lstWithAccount[0].Id);
        	//invoke the main method to fetch the results and make sure that the child account has 2 team members created 
        	controllerObj.getAcctTeamMember();
        	Set<Id> setWithAccId = new Set<Id>();
        	setWithAccId.add(lstWithChildAccount[0].Id);
        	system.assertequals(controllerObj.mapWithAccountTeamMember.keyset(),setWithAccId);
        	system.assertequals(((controllerObj.mapWithAccountTeamMemberList).size()),1);
        	//invoke the pagination methods and make sure that the counter increases as per the logic. The assertion is checked for 20 since the page size is set to 20 in main class
        	controllerObj.Beginning();
        	controllerObj.Next();
        	system.assertequals(controllerObj.counter,20);
        	controllerObj.getDisablePrevious();
        	controllerObj.Previous();  
        	system.assertequals(controllerObj.counter,0);
        	controllerObj.End();
        	controllerObj.getDisableNext();
        	controllerObj.getTotal_size();
        	controllerObj.getPageNumber();
        	controllerObj.getTotalPages();
        	//assign accountid,teammemberid and checkFlag is set to true to add the selected record in wrapper class and ensure it is added
        	controllerObj.checkFlag = true;
        	controllerObj.selectProductUI = true;
        	controllerObj.selectedAcctId = lstWithChildAccount[0].Id;
        	controllerObj.selectedAcctTeamMemId = lstWithAcctTeamMember[0].Id;
        	controllerObj.selectedUserId = lstWithAcctTeamMember[0].UserId;
        	controllerObj.saveBulkSelectedRecordsInTemp();
        	system.assertequals(((controllerObj.lstWithChildMemberWrapper).size()),2);
        	controllerObj.saveSelectedRecordsInTemp();
        	//checkFlag is set to false to remove the selected record in wrapper class  	
        	controllerObj.checkFlag = false;
        	controllerObj.saveSelectedRecordsInTemp();
	        controllerObj.saveBulkSelectedRecordsInTemp();
	        controllerObj.confirmChanges();
        }
        
    }
    
    static testMethod void testUncheckScenario()
    {
    	//Assign variables and create records with the help of TestDataFactory
    	List<Account> lstWithAccount = new List<Account>();
        List<Account> lstWithChildAccount = new List<Account>(); 
        List<AccountTeamMember> lstWithAcctTeamMemberSec = new List<AccountTeamMember>();
        List<AccountTeamMember> lstWithAcctTeamMember = new List<AccountTeamMember>(); 
        List<AccountTeamMember> lstWithParentAcctTeamMember = new List<AccountTeamMember>();
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        
        lstWithAccount = TestDataFactory.createAccount(1,'Organisation');
        lstWithChildAccount = TestDataFactory.createAccount(1,'Organisation');
         
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1); 
         Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=UserInfo.getProfileId(),Disable_Triggers__c=true,Disable_Process_Builder__c=true);
        insert byp;
		
        Bypass_Settings__c byp1 = [select id,Disable_Triggers__c,Disable_Process_Builder__c from Bypass_Settings__c where SetupOwnerId=:UserInfo.getProfileId()];
        byp1.Disable_Triggers__c = false;
        byp1.Disable_Validation_Rules__c = true;
        byp1.Disable_Process_Builder__c=true;
        upsert byp1;
        
        lstWithAcctTeamMember = TestDataFactory.getAccountTeamMembers();
        lstWithParentAcctTeamMember = TestDataFactory.getAccountTeamMembers();
        lstWithAcctTeamMemberSec = TestDataFactory.getAccountTeamMembers();
        
        test.startTest(); 
        //All insert statements is handled inside start and stop test
        lstWithAccount[0].Primary_Selling_Account_check__c = true;
        insert lstWithAccount;
        lstWithChildAccount[0].ParentId = lstWithAccount[0].Id;
        lstWithParentAcctTeamMember[0].AccountId = lstWithAccount[0].Id;
        lstWithAcctTeamMemberSec[0].AccountId = lstWithAccount[0].Id;
        insert lstWithChildAccount;
        for(AccountTeamMember acctTeam : lstWithAcctTeamMember)
        {
        	acctTeam.AccountId = lstWithChildAccount[0].Id;
        }
        insert lstWithTestUser;
        insert lstWithAcctTeamMemberSec;
        insert lstWithAcctTeamMember;
        insert lstWithParentAcctTeamMember;
        test.stopTest();
        
        System.runAs(lstWithTestUser[0]) 
        {
	    	//setting the pageReference to PS_UpdateAccountChildTeams
	    	PageReference pageRef = new PageReference('/apex/PS_UpdateAccountChildTeams?accountId='+lstWithAccount[0].Id);
        	Test.setCurrentPage(pageRef);
        	ApexPages.StandardController sc = new ApexPages.StandardController(lstWithAccount[0]);
        	PS_UpdateAccountChildTeamsController controllerObj = new PS_UpdateAccountChildTeamsController(sc);
        	//setting the check flag to true to delete the added record from database
	    	controllerObj.checkFlag = true;
        	controllerObj.selectProductUI = true;
        	controllerObj.selectedAcctId = lstWithChildAccount[0].Id;
        	controllerObj.selectedAcctTeamMemId = lstWithAcctTeamMember[0].Id;
        	controllerObj.selectedUserId = lstWithAcctTeamMember[0].UserId;
	        controllerObj.saveSelectedRecordsInTemp();
	        controllerObj.saveBulkSelectedRecordsInTemp();
	        controllerObj.confirmChanges();
	        List<AccountTeamMember> lstaccteam = new List<AccountTeamMember>();
	        //ensure that selected records are deleted
	        lstaccteam = [select id from AccountTeamMember where id =: lstWithAcctTeamMember[0].Id];
	        system.assertequals(0,lstaccteam.size(),'Expected 0,but list size is'+lstaccteam.size());
	        //ensure that account team records from parent are created in child account 
	        List<AccountTeamMember> lstWithAcctTeam = [select id from AccountTeamMember where AccountId =: lstWithChildAccount[0].Id];
	    	system.assertequals(3, lstWithAcctTeam.size(),'Expected 3,but list size is'+lstWithAcctTeam.size());
	    }
    }
    
    static testMethod void testNoRecords()
    {
    	List<Account> lstWithAccount = new List<Account>();
    	lstWithAccount = TestDataFactory.createAccount(1,'Organisation');
    	
    	test.startTest();
    	insert lstWithAccount;
    	test.stopTest();
    	//Check if 'No records' message is dispalyed if no records are found
    	PageReference pageRef = new PageReference('/apex/PS_UpdateAccountChildTeams?accountId='+lstWithAccount[0].Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(lstWithAccount[0]);
        PS_UpdateAccountChildTeamsController controllerObj = new PS_UpdateAccountChildTeamsController(sc);
        //to cover try,catch exceptions
        controllerObj.lstWithChildMemberWrapper = null;
        controllerObj.confirmChanges();
    }

}