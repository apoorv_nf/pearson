global class PS_AccountSamplingNumberScheduler implements Schedulable{
//Replaced paramater of Batch Class Call with Custom Label-RJ.
    /* public static String schedtime = '0 00 09 * * ?';  //Every Day at 5:00 PM Monday, Eastern Time (ET)
    public static String jobName = 'Batch Job To Stamp Sampling and Sub Sampling Account Number';

    global static String scheduleMe() {
        PS_AccountSamplingNumberScheduler SC = New PS_AccountSamplingNumberScheduler(); 
        return System.schedule(jobName, schedtime, SC);
    } */

    global void execute(SchedulableContext sc) {
        PS_AccountSamplingNumberBatch exBatch = new PS_AccountSamplingNumberBatch(Label.PS_NA_Market);
        ID batchprocessid = Database.executeBatch(exBatch,50);           
    }
}