/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            AccountAddressesUpdate.cls
   Description:     Remove Primary Billing and Shipping Flags for Old Account Addresses Records
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------       -----------------------------------------------------------------------------------------------
    1-Mar-2018        1.0          Navaneeth                   Remove Primary Billing and Shipping Flags for Old Account Addresses Records
    25-Oct-2018       2.0          Darshan                     Replace Address_type__c with Billing__c,Shipping__c as part of CR-02281-Req-3
    30-Nov-2018       3.0          Vinoth                      Add Physical Address(Primary_Physical__c,Physical__c ) as part of CR-02397
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public without sharing class AccountAddressesUpdate{

    public static void beforeInsertOrUpdateAccountAddresses(List<Account_Addresses__c> AccAddList)
    {
        Set<Id> UniqueAccountIdSet = new Set<Id>();
        Set<Id> UniquePrimaryBillingAccAddAccountIdSet = new Set<Id>();
        Set<Id> UniquePrimaryShippingAccAddAccountIdSet = new Set<Id>();
        
        List<Account_Addresses__c> UpdateExistingAccountAddressList = new List<Account_Addresses__c>();
        
        Map<Id,Account_Addresses__c> FinalMapToUpdate = new Map<Id,Account_Addresses__c>();
        
        Map<Id,List<Account_Addresses__c>> AccountAddressExistingMap = new Map<Id,List<Account_Addresses__c>>();
        List<Account_Addresses__c> AccountAddressNewList = new List<Account_Addresses__c>();
        
       // try
       // {
            for ( Account_Addresses__c AccAdd : AccAddList) 
            {
                if(AccAdd.Account__c!=null)
                {
                    if(AccAdd.Primary__c == true || AccAdd.PrimaryShipping__c == true || AccAdd.Primary_Physical__c == true )
                    {   
                        UniqueAccountIdSet.add(AccAdd.Account__c);
                        AccountAddressNewList.add(AccAdd);
                    }   
                    /*if(Primary__c == true)
                        ;
                    if(PrimaryShipping__c == true)  
                        ;*/
                }
            }
                
            //List<Account_Addresses__c> AccountAddressQueryList = [Select Id,Account__c,Primary__c,PrimaryShipping__c,Address_Type__c from Account_Addresses__c where Account__c IN: UniqueAccountIdSet];
            List<Account_Addresses__c> AccountAddressQueryList = [Select Id,Account__c,Primary__c,PrimaryShipping__c,Primary_Physical__c,Physical__c,Billing__c,Shipping__c from Account_Addresses__c where Account__c IN: UniqueAccountIdSet];

            // Forming a Map of QueryRecords
            
            for(Id AccId : UniqueAccountIdSet)
            {
                List<Account_Addresses__c> TempAccListAddress = new List<Account_Addresses__c>();
                for(Account_Addresses__c AccAdd : AccountAddressQueryList)
                {
                    Account_Addresses__c TempAccAddress = new Account_Addresses__c();
                    if(AccId == AccAdd.Account__c )
                    {
                        TempAccAddress = AccAdd;
                        TempAccListAddress.add(TempAccAddress);
                    }
                }
                AccountAddressExistingMap.put(AccId,TempAccListAddress);
            }
            
            // Logic to find out the list to Records to update [Removing Primary Billing and Shipping Flags]
            
            for( Account_Addresses__c NewAccAdd : AccountAddressNewList)
            {
                for( Account_Addresses__c ExistingAccAdd : AccountAddressExistingMap.get(NewAccAdd.Account__c))
                {
                    if( NewAccAdd.Id != ExistingAccAdd.Id && ( ExistingAccAdd.Primary__c == true || ExistingAccAdd.PrimaryShipping__c == true || ExistingAccAdd.Primary_Physical__c == true))
                    {   
                        if(NewAccAdd.Primary__c == true && ExistingAccAdd.Primary__c == true &&  NewAccAdd.PrimaryShipping__c == true && ExistingAccAdd.PrimaryShipping__c == true &&  NewAccAdd.Primary_Physical__c== true && ExistingAccAdd.Primary_Physical__c == true )
                        {
                            ExistingAccAdd.Primary__c = false;
                            ExistingAccAdd.PrimaryShipping__c = false;
                            ExistingAccAdd.Primary_Physical__c = false;
                            //UpdateExistingAccountAddressList.add(ExistingAccAdd);
                            FinalMapToUpdate.put(ExistingAccAdd.Id,ExistingAccAdd);
                        }
                        //CR-02397 - Start
                        else if((NewAccAdd.Primary__c == true && ExistingAccAdd.Primary__c == true) && ( NewAccAdd.PrimaryShipping__c == true && ExistingAccAdd.PrimaryShipping__c == true) )
                        {
                            ExistingAccAdd.Primary__c = false;
                            ExistingAccAdd.PrimaryShipping__c = false;
                            //UpdateExistingAccountAddressList.add(ExistingAccAdd);
                            FinalMapToUpdate.put(ExistingAccAdd.Id,ExistingAccAdd);
                        }
                        else if((NewAccAdd.Primary__c == true && ExistingAccAdd.Primary__c == true) && ( NewAccAdd.Primary_Physical__c== true && ExistingAccAdd.Primary_Physical__c == true) )
                        {
                            ExistingAccAdd.Primary__c = false;
                            ExistingAccAdd.Primary_Physical__c = false;
                            //UpdateExistingAccountAddressList.add(ExistingAccAdd);
                            FinalMapToUpdate.put(ExistingAccAdd.Id,ExistingAccAdd);
                        }
                        else if((NewAccAdd.PrimaryShipping__c == true && ExistingAccAdd.PrimaryShipping__c == true) && ( NewAccAdd.Primary_Physical__c== true && ExistingAccAdd.Primary_Physical__c == true) )
                        {
                            ExistingAccAdd.Primary_Physical__c = false;
                            ExistingAccAdd.PrimaryShipping__c = false;
                            //UpdateExistingAccountAddressList.add(ExistingAccAdd);
                            FinalMapToUpdate.put(ExistingAccAdd.Id,ExistingAccAdd);
                        }                                                                                                           
                        //CR-02397 - End
                        
                        else if(NewAccAdd.Primary__c == true && ExistingAccAdd.Primary__c == true)
                        {
                            ExistingAccAdd.Primary__c = false;
                            //UpdateExistingAccountAddressList.add(ExistingAccAdd);
                            FinalMapToUpdate.put(ExistingAccAdd.Id,ExistingAccAdd);
                        }
                        else if ( NewAccAdd.PrimaryShipping__c == true && ExistingAccAdd.PrimaryShipping__c == true)    
                        {
                            ExistingAccAdd.PrimaryShipping__c = false;
                            //UpdateExistingAccountAddressList.add(ExistingAccAdd);
                            FinalMapToUpdate.put(ExistingAccAdd.Id,ExistingAccAdd);
                        }
                        else if ( NewAccAdd.Primary_Physical__c== true && ExistingAccAdd.Primary_Physical__c== true)  
                        {
                            ExistingAccAdd.Primary_Physical__c = false;
                            //UpdateExistingAccountAddressList.add(ExistingAccAdd);
                            FinalMapToUpdate.put(ExistingAccAdd.Id,ExistingAccAdd);
                        }
                         else
                        {
                            // do nothing
                        }
                    }
                }
            }
            
            UpdateExistingAccountAddressList = FinalMapToUpdate.values();
            
            if(UpdateExistingAccountAddressList.size()>0){
                //update UpdateExistingAccountAddressList;
                List<Database.SaveResult> results = Database.update(UpdateExistingAccountAddressList, false);
                for (Database.SaveResult result : results) {
                if (!result.isSuccess()){
                for (Database.Error err : result.getErrors()){
                    System.debug('Error: '+ err.getStatusCode() + ' ' + err.getMessage());
                    }
                  }
                }
            }
      /***  } 
        catch(Exception e)
        {
            System.debug('Exception Occurred Inside <<<<<ApexClass:AccountAddressesUpdate>><<Method:beforeInsertOrUpdateAccountAddresses>>>>>>'+ e.getMessage());
            throw e;
        }   ***/
    } 
     //Commented by Priya for CR-03043
    /*public static void PopulateAddTypebeforeInsertOrUpdateAccountAddresses(List<Account_Addresses__c> AccAddList, Map<Id,Account_Addresses__c> mapoldAccAdds){
       List<Account_Addresses__c> lstAccAddrs = new List<Account_Addresses__c>();
       
       for(Account_Addresses__c accAdd : AccAddList){
          
          if(accAdd.Billing__c == True && accAdd.Shipping__c == False && accAdd.Physical__c == False){
             accAdd.Address_Type__c = 'Billing';
          }
          else if(accAdd.Billing__c == False && accAdd.Shipping__c == True && accAdd.Physical__c == False){
             accAdd.Address_Type__c = 'Shipping';
          }
          else if(accAdd.Billing__c == False && accAdd.Shipping__c == False && accAdd.Physical__c == True){
             accAdd.Address_Type__c = 'Physical';
          }
          else if(accAdd.Billing__c == True && accAdd.Shipping__c == True && accAdd.Physical__c == False){
             accAdd.Address_Type__c = 'Billing and Shipping';
          }
          else if(accAdd.Billing__c == True && accAdd.Shipping__c == False && accAdd.Physical__c == True){
             accAdd.Address_Type__c = 'Billing and Physical';
          }
          else if(accAdd.Billing__c == False && accAdd.Shipping__c == True && accAdd.Physical__c == True){
             accAdd.Address_Type__c = 'Shipping and Physical';
          }
          else if(accAdd.Billing__c == True && accAdd.Shipping__c == True && accAdd.Physical__c == True){
             accAdd.Address_Type__c = 'Billing and Shipping and Physical';
          }
          else if(accAdd.Billing__c == False && accAdd.Shipping__c == False && accAdd.Physical__c == False){
             accAdd.Address_Type__c = '';
          }
       }
       
    } */
}