/*************************************************************
@Name : PS_BatchUpdateCoursesFrontlists 
@Author     : Accenture IDC 
@Description: Batch Class to update Course FrontLists
@Date       : 7/01/15
@Version    : 1.0
**************************************************************/ 
/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_BatchUpdateCoursesFrontlists
Description:     Batch Class for Pearson Choice Generation.
Date             Version                                   Author                             Summary of Changes
-----------      ----------                                -----------------    ---------------------------------------------------------------------------------------------------
27/12/2016        1.1 [CR-00280 Jan 2017 Release]          MAYANK LAL                Updated code to update Course Frontlists for Canada as well (Modified Lines- 20,28-32,35,67,78,168)
10/10/2019        1.2 [Apttus Decomm Project]              Mani Kagithoju            Removed Apttus Object/field references            
16/10/2019        1.3 [Apttus Decomm Project]              Vinoth                    CR-02949 - Replacement the Apttus to Non-Apttus changes            
----------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class PS_BatchUpdateCoursesFrontlists implements Database.Batchable<sObject>,Database.Stateful,Schedulable
{
    
    global set<id> setOfCourseIds = new set<id>();
    global String finalstr = 'Course ID, Course Name, Failure Reason \n';
    List<String> sMarket = new List<String>(); // Changed String to List<String> CR-00280
    String sId;
    boolean initialld;
    
    List<Database.SaveResult> dbupres=null;
    list<UniversityCourse__c> listOfcourseObj=null;
    
    public PS_BatchUpdateCoursesFrontlists(String sCountry,String sCourseId,boolean initialload){
        sMarket = sCountry.split(','); // Storing regions in list CR-00280
        sId=sCourseId;
        initialld=initialload;
    }//end constructor
    global Database.QueryLocator start(Database.BatchableContext BC){
        
            if (initialld==true){ 
                return Database.getQueryLocator([SELECT id,name,Spring_Front_List__c ,Fall_Front_List__c
                                                 from UniversityCourse__c where Status__c='Active']);
             }
             else{
                return Database.getQueryLocator([SELECT id,name,Spring_Front_List__c ,Fall_Front_List__c
                                         from UniversityCourse__c where Status__c='Active' and id in 
                                        (select course__c from Pearson_Course_Equivalent__c where Pearson_Course_Code_Hierarchy__r.lastmodifieddate >= YESTERDAY)]);
             }        
        //}

    }//end start
    global void execute(Database.BatchableContext BC, List<UniversityCourse__c> lstCourse){
        listOfcourseObj= new List<UniversityCourse__c>();
        List<UniversityCourse__c> courseLst = new List<UniversityCourse__c>();
        //Debugging<--Start-->
		List<Pearson_Course_Equivalent__c> pEqu=[SELECT Name,Pearson_Course_Code_Hierarchy__r.Market__c,Pearson_Course_Code_Hierarchy__r.CurrentPeriod__c,Pearson_Course_Code_Hierarchy__r.PriorPeriod__c FROM Pearson_Course_Equivalent__c];
		system.debug('@@##1=>'+pEqu[0].Pearson_Course_Code_Hierarchy__r.Market__c); 
        system.debug('@@##2=>'+sMarket); 
        //Debugging<--End-->
        if(initialld)
            //Added In Clause in where condition CR-00280
            /*  CR-02949 - Commented the Apttus to Non-Apttus changes by Vinoth - Start
            /*courseLst = [SELECT id,name,Spring_Front_List__c ,Fall_Front_List__c,
                                        (SELECT Name,Pearson_Course_Code__c,Course__c,Course__r.Spring_Front_List__c,
                                        Course__r.Fall_Front_List__c,Pearson_Course_Code_Hierarchy__r.Current_Period__c,
                                        Pearson_Course_Code_Hierarchy__r.Prior_Period__c,Pearson_Course_Code_Hierarchy__r.Region__c 
                                        FROM Pearson_Course_Equivalent__r where 
                                        (Pearson_Course_Code_Hierarchy__r.Current_Period__c = true OR Pearson_Course_Code_Hierarchy__r.Prior_Period__c=true) AND 
                                        Pearson_Course_Code_Hierarchy__r.Region__c IN :sMarket order by 
                                        Pearson_Course_Code_Hierarchy__r.Current_Period__c desc,Pearson_Course_Code_Hierarchy__r.Prior_Period__c desc) 
                                        from UniversityCourse__c where Status__c='Active' and Id in :lstCourse];
            Commented for CR-02949 Apttus to Non-Apttus changes by Vinoth - End     */ 
              
             
        	//system.debug('###courseLst=>'+courseLst);
            // CR-02949 - Replacement the Apttus to Non-Apttus changes by Vinoth
            courseLst = [SELECT id,name,Spring_Front_List__c ,Fall_Front_List__c,
                                        (SELECT Name,Pearson_Course_Code_Hierarchy__c,Course__c,Course__r.Spring_Front_List__c,
                                        Course__r.Fall_Front_List__c,Pearson_Course_Code_Hierarchy__r.CurrentPeriod__c,
                                        Pearson_Course_Code_Hierarchy__r.PriorPeriod__c,Pearson_Course_Code_Hierarchy__r.Market__c
                                        FROM Pearson_Course_Equivalent__r where 
                                        (Pearson_Course_Code_Hierarchy__r.CurrentPeriod__c = true OR Pearson_Course_Code_Hierarchy__r.PriorPeriod__c=true) AND 
                                        Pearson_Course_Code_Hierarchy__r.Market__c IN :sMarket order by 
                                        Pearson_Course_Code_Hierarchy__r.CurrentPeriod__c desc,Pearson_Course_Code_Hierarchy__r.PriorPeriod__c desc) 
                                        from UniversityCourse__c where Status__c='Active' and Id in :lstCourse];
        else
            //Added In Clause in where condition CR-00280
            // CR-02949 - Replacement the Apttus to Non-Apttus changes by Vinoth
            courseLst =[SELECT id,name,Spring_Front_List__c ,Fall_Front_List__c,
                                        (SELECT Name,Pearson_Course_Code_Hierarchy__c,Course__c,Course__r.Spring_Front_List__c,
                                        Course__r.Fall_Front_List__c,Pearson_Course_Code_Hierarchy__r.CurrentPeriod__c,
                                        Pearson_Course_Code_Hierarchy__r.PriorPeriod__c,Pearson_Course_Code_Hierarchy__r.Market__c
                                        FROM Pearson_Course_Equivalent__r where 
                                        (Pearson_Course_Code_Hierarchy__r.CurrentPeriod__c = true OR Pearson_Course_Code_Hierarchy__r.PriorPeriod__c=true) AND 
                                        Pearson_Course_Code_Hierarchy__r.Market__c IN :sMarket and
                                        Pearson_Course_Code_Hierarchy__r.Type__c=:System.Label.PS_BatchFLCatType
                                        order by Pearson_Course_Code_Hierarchy__r.CurrentPeriod__c desc,Pearson_Course_Code_Hierarchy__r.PriorPeriod__c desc) 
                                        from UniversityCourse__c where Status__c='Active' and id in :lstCourse];
                                        
            /* Commented for CR-02949 Apttus to Non-Apttus changes by Vinoth - Start
            courseLst =[SELECT id,name,Spring_Front_List__c ,Fall_Front_List__c,
                                        (SELECT Name,Pearson_Course_Code__c,Course__c,Course__r.Spring_Front_List__c,
                                        Course__r.Fall_Front_List__c,Pearson_Course_Code_Hierarchy__r.Current_Period__c,
                                        Pearson_Course_Code_Hierarchy__r.Prior_Period__c,Pearson_Course_Code_Hierarchy__r.Region__c 
                                        FROM Pearson_Course_Equivalent__r where 
                                        (Pearson_Course_Code_Hierarchy__r.Current_Period__c = true OR Pearson_Course_Code_Hierarchy__r.Prior_Period__c=true) AND 
                                        Pearson_Course_Code_Hierarchy__r.Region__c IN :sMarket and
                                        Pearson_Course_Code_Hierarchy__r.Type__c=:System.Label.PS_BatchFLCatType
                                        order by Pearson_Course_Code_Hierarchy__r.Current_Period__c desc,Pearson_Course_Code_Hierarchy__r.Prior_Period__c desc) 
                                        from UniversityCourse__c where Status__c='Active' and id in :lstCourse];
            Commented for CR-02949 Apttus to Non-Apttus changes by Vinoth - End     */                                        

        if(courseLst.size()>0)
        {
        listOfcourseObj.addAll(courseLst);
        integer iMonth = system.today().month();
        try{
        for(UniversityCourse__c courseObj : listOfcourseObj){
            //System.debug(courseObj.name);
            boolean priorCheck = false;
            boolean currentCheck = false;
            courseObj.Fall_Front_List__c = false;   
            courseObj.Spring_Front_List__c = false;
            if(courseObj.Pearson_Course_Equivalent__r.size() > 0){
                for(Pearson_Course_Equivalent__c  pearson_equivalent : courseObj.Pearson_Course_Equivalent__r){
                    if (!(courseObj.Fall_Front_List__c && courseObj.Spring_Front_List__c)){
                    if (priorCheck == false){
                        if(pearson_equivalent.Pearson_Course_Code_Hierarchy__r.PriorPeriod__c == true){
                            if ((iMonth >=1 && iMonth <=4) || iMonth > 10){
                                courseObj.Fall_Front_List__c = true;
                            }
                            else{
                                courseObj.Spring_Front_List__c = true;
                            }
                            priorCheck=true;
                        }
                    }
                    if (currentCheck == false){
                        if(pearson_equivalent.Pearson_Course_Code_Hierarchy__r.CurrentPeriod__c == true){
                            if ((iMonth >=1 && iMonth <=4) || iMonth > 10){
                                courseObj.Spring_Front_List__c = true;
                            }
                            else{
                                courseObj.Fall_Front_List__c = true;
                            }
                            currentCheck=true;
                        }
                    }
                    }
                    else{break;}
                }//END LOOP THROUGH PEARSON COURSE
            }//end code to check for Pearson course
        }//end loop through course
        dbupres=Database.Update(listOfcourseObj,false);
        
        List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
      try{            
          //Database.SaveResult[] srList=result;
          for (integer i=0;i<listOfcourseObj.size();i++){
              UniversityCourse__c course=listOfcourseObj[i];
              Database.SaveResult res=dbupres[i];
              String ErrMsg='';
              if (!res.isSuccess() || Test.isRunningtest()){
                  PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                  errlogger.InterfaceName__c='BatchUpdateCourseFrontList';
                  errlogger.ApexClassName__c='PS_BatchUpdateCoursesFrontlists';
                  errlogger.CallingMethod__c='finish';
                  //errlogger.ExceptionMessage__c=err.getStatusCode() + ': ' + err.getMessage(); 
                  errlogger.UserLogin__c=UserInfo.getUserName(); 
                  errlogger.RecordId__c=course.id;   
                  for(Database.Error err : res.getErrors()) {
                      ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage();
                  }   
                  errlogger.ExceptionMessage__c=ErrMsg;
                  errloggerlist.add(errlogger);                     
              }
          }
          if(errloggerlist.size()>0){insert errloggerlist;}
           
       }
       catch(DMLException e){
           throw(e);
       }
        
        }
        
        catch(Exception e){
            ExceptionFramework.LogException('BatchUpdateCourseFrontList','PS_BatchUpdateCoursesFrontlists','execute',e.getMessage(),UserInfo.getUserName(),'');
        }
       } 
    }
    global void finish(Database.BatchableContext BC){
              
    }//end batch finish
    
    global void execute(SchedulableContext sc) {
       database.executebatch(new PS_BatchUpdateCoursesFrontlists(System.Label.Multiple_Market,'',false));
    }
    
}//end class