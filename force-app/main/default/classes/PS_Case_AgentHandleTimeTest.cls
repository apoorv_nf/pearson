/************************************************************************************************************
* Apex Interface Name : PS_Case_AgentHandleTimeTest
* Version             : 1.1 
* Created Date        : 1/2/2016 2:44 
* Modification Log    :
* Test Description    : Test class created for class PS_Case_AgentHandleTimeTest
* Expected Inputs     : For Cases Email Replied time has been provided as inputs
* Expected Outputs    : Agent Handle Time should get stamped  
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Hasi Chakravarty         1/2/2016
* Ajay Chakradhar          7/3/2016
* Ajay Chakradhar          13/4/2016                  
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest(SeeAllData =true)
public class PS_Case_AgentHandleTimeTest{ //To Calculate Agent handle time when case origin is email.
    /*
     * Method to cover scenario where Agent handle time is getting stamped.
     */
    static testMethod void validatePS_Case_AgentHandleTime(){
         //creating account
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
         Test.StartTest(); 
        List<Account> acc = TestDataFactory.createAccount(1,'Organisation');
        insert acc;
         //creating contact
        List<Contact> contactRecord = TestDataFactory.createContacts(1);
        contactRecord[0].accountId  = acc[0].id;
         contactRecord[0].MailingState='England';
        Insert contactRecord;            
        
        //case creation
       List<case> cases = TestDataFactory.createCase(1,'School Assessment');
        cases[0].AccountId=acc[0].id;
        cases[0].Contact=contactRecord[0];
        cases[0].EmailReadTime__c=System.now();
        cases[0].EmailRepliedTime__c =DateTime.Now().AddDays(1);
        
        
           
            insert cases;
            cases[0].EmailRepliedTime__c =DateTime.Now().AddDays(4);
            update cases;
            List<Case_Owner_Tracking__c> oCOT = [select id,Agent_Handle_Time__c from Case_Owner_Tracking__c 
                        where case__c=:cases[0].id];
            //system.assert(oCOT.size()>0,'Agent handle time is not stamped');
            System.assertEquals(cases[0].priority,'Medium');
            Test.StopTest();
        
        }
    }
    /*
     * Method to cover negative scenario.
     */
   static testMethod void Negative_test_validatePS_Case_AgentHandleTime(){
         //creating account
        List<Account> acc = TestDataFactory.createAccount(1,'Organisation');
        insert acc;
         //creating contact
        List<Contact> contactRecord = TestDataFactory.createContacts(1);
        contactRecord[0].accountId  = acc[0].id;
         contactRecord[0].MailingState='England';
        Insert contactRecord;
        
        //case creation
       List<case> cases = TestDataFactory.createCase(1,'General');
        cases[0].AccountId=acc[0].id;
        cases[0].Contact=contactRecord[0];
        cases[0].EmailReadTime__c=System.now();
        cases[0].EmailRepliedTime__c =DateTime.Now().AddDays(1);
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            Test.StartTest(); 
            insert cases;
            cases[0].EmailRepliedTime__c =DateTime.Now().AddDays(4);
            try {
                    update cases;
                    System.assert(true, 'Exception expected');
                } catch (DMLException e) {
                    System.assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'), 'message=' + e.getMessage());
                }
           Test.StopTest();
        }
    }
}