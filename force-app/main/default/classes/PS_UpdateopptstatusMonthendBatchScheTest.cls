@isTest
public class PS_UpdateopptstatusMonthendBatchScheTest{
    
    static testmethod void testScheduledJob() {
    
        String CRON_EXP = '0 0 6 ? * 6 *';
        Test.startTest();
        String jobId = System.schedule('PS_UpdateopptstatusMonthendBatchSchedule',
                                       CRON_EXP, 
                                       new PS_UpdateopptstatusMonthendBatchSchedule());
        
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                          NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same
        System.assertEquals(CRON_EXP, ct.CronExpression);
        
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        
        // Verify the next time the job will run
        //System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        
        Test.stopTest();
        
    }
    
    public static testmethod void maintest(){
        
            account acc = new Account();
            acc.Name='Test Account1';
            acc.IsCreatedFromLead__c =True;
            acc.Phone='+91000001';
            acc.Market2__c= 'UK';
            acc.Line_of_Business__c= 'Higher Ed';
            acc.Business_Unit__c='Rights & Royalties';
            acc.Geography__c='Core';
            acc.ShippingCountry = 'India';
            acc.ShippingStreet = '34 Downing Street'; 
            acc.ShippingCity = 'Bangalore';
            acc.ShippingState = 'Karnataka';            
            acc.ShippingPostalCode = '5600371';
            insert acc;
                        
            Opportunity opp = new opportunity ( Name = 'OpporName1',AccountId = acc.Id, CloseDate = System.TODAY() + 30,StageName = 'OpporStageName',Auto_Expire__c = True,Chased__c = True);
            insert opp;
            
            Test.startTest();
            PS_UpdateopptstatusMonthendBatch batch1 = new PS_UpdateopptstatusMonthendBatch();
            Database.executeBatch(batch1);
            Test.stopTest();
    }            
}