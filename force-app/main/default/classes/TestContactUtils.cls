/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 7/27/2018
 */
@isTest
private class TestContactUtils {

    @isTest
    static void it_should_create_account_contacts() {
        Bypass_Settings__c bypass = new Bypass_Settings__c(
            SetupOwnerId = UserInfo.getUserId(),
            Disable_Triggers__c = true,
            Disable_Validation_Rules__c = true);
        insert bypass;

        List<Lead> leads = TestDataFactory.createLead(1,'B2B');
        leads[0].IsBeingConverted__c = true;
        leads[0].Market__c = 'AU';
        leads[0].Line_of_Business__c = 'Schools';
        leads[0].Geography__c = 'Core';
        leads[0].Business_Unit__c =  'Pearson Assessment'; 
        leads[0].OwnerId = UserInfo.getUserId();
        leads[0].ConvertedContactId = Contact.getSobjectType().getDescribe().getKeyPrefix() + '000000000000';
        leads[0].ConvertedAccountId = Account.getSobjectType().getDescribe().getKeyPrefix() + '000000000000';
        leads[0].Id = Lead.getSobjectType().getDescribe().getKeyPrefix() + '000000000000';

        Map<Id, Lead> leadMap = new Map<Id, Lead>{leads[0].Id => leads[0]};

        new ContactUtils().createAccountContact(leads[0], leadMap);
    }
}