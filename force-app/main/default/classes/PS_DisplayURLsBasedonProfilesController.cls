/**
* Name : PS_DisplayURLsBasedonProfilesController
* Description : This class holds the Logic to get the List of Links Based on the logged in User’s Profile.                 
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Mayank Lal                  17/07/2017              CR-1432 reg categorization of guided flows
************************************************************************************************************/
public with sharing class PS_DisplayURLsBasedonProfilesController {
    public Boolean bRenderGuidedList {get;set;}
    public String sPageName {get;set;}
    public static string Category{get;set;}
    public String currentUrl {get; set;}
    AppCatalog__c CustSetting = AppCatalog__c.getValues('First');
    String APPLICATIONNAME = CustSetting.APPLICATIONNAME__c;
    String CLASSNAME = CustSetting.PS_DisplayURLsBasedONProfile__c;
    String METHODNAME = CustSetting.getLinksBasedOnProfile__c;
    String MYAPPS = CustSetting.MyApps__c;
    String PERMISSIONSETNAME='';
    String PERMISSIONSETNAME_Profile='';
    boolean VISIBLEONGF=FALSE;
    boolean VISIBLEONQL=FALSE;
    Integer urlLimit = 10;
    List<App_Profile__c> listAppProfiles = new List<App_Profile__c>();
    List<AggregateResult> listCategories = new List<AggregateResult>();
    List<Application__c> listOfApp= new List<Application__c>();
    public static List<App_Profile__c> listAppProfiles1{get;set;}
    Map<String, List<App_Profile__c>> mapCategoryAppProf = new Map<String, List<App_Profile__c>>();
    
    //Constructor
    public PS_DisplayURLsBasedonProfilesController(){
        bRenderGuidedList =false;
        sPageName='';
        Category = apexpages.currentpage().getparameters().get('category');
        if(Category != null)
        {
           getGuidedFlowForCategories(); 
        }
        
    } 
/**
       @Method Name  : getAppProfiles
       @description  : This Method gets the Links based on the current user's profile
       @param        : NA
       @return       : void
    */
    public List<App_Profile__c> GuidedFlow {
        get {
            bRenderGuidedList = false;
            try{ 
                   //Query the profile of Logged In User 
                Profile loggedInUserProfile = [Select Name from Profile where Id =: userinfo.getProfileid() LIMIT 1]; 
                urlLimit = Integer.valueOf(ACURLLimit__c.getValues(MYAPPS).Limit__c);
            //Query apps corresponding to Logged In User's Profile 
            listAppProfiles = [select Name, Application__r.URL__c, Application__r.App_Label__c,
                                Application__r.Name, Application__r.App_Type__c, Ranking__c,Permission_Set__c
                                FROM App_Profile__c WHERE Profile_Name__c =: loggedInUserProfile.Name
                                AND Application__r.Visible_On_MyApp__c = True
                                ORDER BY Application__r.App_Label__c LIMIT : urlLimit] ; 
               

            }
            catch(Exception gExcp){
                    ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,gEXCp.getMessage());
                    ApexPages.addMessage(errMsg);                         
            }
            return listAppProfiles;
        }
        private set;
    }
 /**
@Method Name  : getlistCategories
@description  : This Method gets Unique Categories for Guided Flow
@param        : NA
@return       : void
@Created By   : MAYANK LAL
*/
    public List<AggregateResult> GuidedCategories {
        get {
            bRenderGuidedList = false;
            try{ 
                //Query the profile of Logged In User 
                Profile loggedInUserProfile = [Select Name from Profile where Id =: userinfo.getProfileid() LIMIT 1]; 
                urlLimit = Integer.valueOf(ACURLLimit__c.getValues(MYAPPS).Limit__c);
                //Query apps corresponding to Logged In User's Profile 
                listCategories = [select Application__r.Category__c appcat
                                  FROM App_Profile__c WHERE Profile_Name__c =: loggedInUserProfile.Name
                                  AND Application__r.Visible_On_MyApp__c = True AND Application__r.Category__c != null
                                  GROUP BY Application__r.Category__c  LIMIT : urlLimit] ; 
              }
            catch(Exception gExcp){
                ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,gEXCp.getMessage());
                ApexPages.addMessage(errMsg);                         
            }
            return listCategories;
        }
        private set;
    }
    
    public List<App_Profile__c> QuickLinks {
        get {
            bRenderGuidedList = false;
            try{ 
                //Query the profile of Logged In User 
                Profile loggedInUserProfile = [Select Name from Profile where Id =: userinfo.getProfileid() LIMIT 1]; 
                urlLimit = Integer.valueOf(ACURLLimit__c.getValues(MYAPPS).Limit__c);
                //Query apps corresponding to Logged In User's Profile 
                listAppProfiles = [select Name, Application__r.URL__c, Application__r.App_Label__c,
                                   Application__r.Name, Application__r.App_Type__c,Application__r.Category__c,Ranking__c,Permission_Set__c
                                   FROM App_Profile__c WHERE Profile_Name__c =: loggedInUserProfile.Name
                                   AND Application__r.Visible_On_Quicklinks__c = True AND Application__r.Category__c != null
                                   ORDER BY Ranking__c LIMIT : urlLimit] ; 
                
                
                
            }
            catch(Exception gExcp){
                ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,gEXCp.getMessage());
                ApexPages.addMessage(errMsg);                         
            }
            return listAppProfiles;
        }
        private set;
    }
    
    public PageReference mRenderList(){
        if(bRenderGuidedList)
            bRenderGuidedList = false;
        else
            bRenderGuidedList = true;
        //return bRenderGuidedList ;
        return null;
    }
    
    public String PermissionSetCheck(){
        
        List<PermissionSetAssignment> lstcurrentUserPerSet =[SELECT Id, PermissionSet.Name,AssigneeId
                                                             FROM PermissionSetAssignment
                                                             WHERE AssigneeId = :Userinfo.getUserId()
                                                             ORDER BY PermissionSet.Name LIMIT : 1];
        
        for ( PermissionSetAssignment psa : lstcurrentUserPerSet ) {
            PERMISSIONSETNAME=psa.PermissionSet.Name;
        }
        return PERMISSIONSETNAME;
    }
    /**
@Method Name  : getGuidedFlowForCategories
@description  : This Method gets respective guided flow related to Category
@param        : Category 
@return       : void
@Created By   : MAYANK LAL
*/
    public  static void getGuidedFlowForCategories () {
        listAppProfiles1  = new List<App_Profile__c>();
        Profile loggedInUserProfile = [Select Name from Profile where Id =: userinfo.getProfileid() LIMIT 1]; 
        AppCatalog__c CustSetting = AppCatalog__c.getValues('First');
        Integer urlLimit1 = 10;
        String MYAPPS1 = CustSetting.MyApps__c;
        urlLimit1 = Integer.valueOf(ACURLLimit__c.getValues(MYAPPS1).Limit__c);
        listAppProfiles1 = [select Name, Application__r.URL__c, Application__r.App_Label__c,
                            Application__r.Name, Application__r.App_Type__c,Application__r.Category__c, Ranking__c,Permission_Set__c
                            FROM App_Profile__c WHERE Profile_Name__c =: loggedInUserProfile.Name
                            AND Application__r.Visible_On_MyApp__c = True AND Application__r.Category__c != null
                            AND Application__r.Category__c =: Category ORDER BY Application__r.App_Label__c LIMIT : urlLimit1];
           
    }
}