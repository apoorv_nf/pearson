@isTest
public class AccountActiveInactiveCourseExtnTest {
  
    public static Account tact;
    
    public static Account dataCreation()
    {
       tact = new Account();
        tact.Name = 'Test Account';
        tact.Geography__c = 'Core';
        tact.Line_of_Business__c = 'Schools';
        insert tact;
        UniversityCourse__c tcou = new UniversityCourse__c();
        tcou.Name = 'Test Course';
        tcou.Catalog_Code__c = 'Test Catalog';
        tcou.Account__c = tact.Id;
        tcou.Course_Name__c = 'Test Course';
        insert tcou;
        UniversityCourse__c tcou1 = new UniversityCourse__c();
        tcou1.Name = 'Test Course';
        tcou1.Catalog_Code__c = 'Test Catalog';
        tcou1.Account__c = tact.Id;
        tcou1.Course_Name__c = 'Test Course';
        insert tcou1;
        UniversityCourse__c tcou2 = new UniversityCourse__c();
        tcou2.Name = 'Test Course';
        tcou2.Catalog_Code__c = 'Test Catalog';
        tcou2.Account__c = tact.Id;
        tcou2.Course_Name__c = 'Test Course';
        insert tcou2;
        UniversityCourse__c tcou3 = new UniversityCourse__c();
        tcou3.Name = 'Test Course';
        tcou3.Catalog_Code__c = 'Test Catalog';
        tcou3.Account__c = tact.Id;
        tcou3.Course_Name__c = 'Test Course';
        insert tcou3;
         return tact;
    }
    static testMethod void testSortData()
    {
        Account tact1 = dataCreation();
        //testing sort funtionality
        PageReference testPage = Page.AccountActiveInactiveCourse;
        testPage.getParameters().put('AccountName',tact.Name);
        testPage.getParameters().put('AccountId',tact.id);
        testPage.getParameters().put('Type','Active');
    Test.setCurrentPageReference(testPage);
        Apexpages.StandardController stdController = new Apexpages.StandardController(tact);
        AccountActiveInactiveCourseExtn accountest = new AccountActiveInactiveCourseExtn(stdController);
        accountest.uName = '';
        accountest.aId = tact1.Id;
        accountest.arId = tact1.Id;
        accountest.sortOptions1 = '';
        accountest.selOptions2 = '';
        accountest.sortOptions2 = '';
        accountest.serCond1 = '';
        accountest.serOptions2 = '';
        accountest.serCond2 = '';
        accountest.selandor = '';
        accountest.ViewData();
        //CourseActiveInactiveAssetExtn.dynamicAddFilterSearch testfil = new CourseActiveInactiveAssetExtn.dynamicAddFilterSearch(null,null,'Name','ASC');
         accountest.addNewFilter();
        accountest.addNewFilter();
        List<AccountActiveInactiveCourseExtn.dynamicAddFilterSearch> srtList = accountest.listWithSelectOptions;
    AccountActiveInactiveCourseExtn.dynamicAddFilterSearch sfilter = srtList.get(0);
        sfilter.selField = 'name';
        sfilter.serField = 'ASC';
             
        AccountActiveInactiveCourseExtn.dynamicAddFilterSearch sfilter1 = srtList.get(1);
        sfilter1.selField = 'fall_enrollment__c';
        sfilter1.serField = 'ASC';
             
        AccountActiveInactiveCourseExtn.dynamicAddFilterSearch sfilter2 = srtList.get(2);
        sfilter2.selField = 'spring_enrollment__c';
        sfilter2.serField = 'DESC';
        
        accountest.sortData();
            
        accountest.accId = tact1.Id;
        accountest.delCourse();
    
        accountest.rowToRemove = 1;
        accountest.listWithSelectOptions = srtList;
        accountest.removeFilter();
         }
    static testMethod void testSortData1()
    {
        Account tact = dataCreation();
        //testing sort funtionality
        PageReference testPage = Page.AccountActiveInactiveCourse;
        testPage.getParameters().put('AccountName',tact.Name);
        testPage.getParameters().put('AccountId',tact.id);
        testPage.getParameters().put('Type','Inactive');
    Test.setCurrentPageReference(testPage);
        Apexpages.StandardController stdController = new Apexpages.StandardController(tact);
        AccountActiveInactiveCourseExtn accountest = new AccountActiveInactiveCourseExtn(stdController);
        accountest.ViewData();
        //CourseActiveInactiveAssetExtn.dynamicAddFilterSearch testfil = new CourseActiveInactiveAssetExtn.dynamicAddFilterSearch(null,null,'Name','ASC');
         accountest.addNewFilter();
        accountest.addNewFilter();
        List<AccountActiveInactiveCourseExtn.dynamicAddFilterSearch> srtList = accountest.listWithSelectOptions;
    AccountActiveInactiveCourseExtn.dynamicAddFilterSearch sfilter = srtList.get(0);
        sfilter.selField = 'name';
        sfilter.serField = 'ASC';
             
        AccountActiveInactiveCourseExtn.dynamicAddFilterSearch sfilter1 = srtList.get(1);
        sfilter1.selField = 'fall_enrollment__c';
        sfilter1.serField = 'ASC';
             
        AccountActiveInactiveCourseExtn.dynamicAddFilterSearch sfilter2 = srtList.get(2);
        sfilter2.selField = 'spring_enrollment__c';
        sfilter2.serField = 'DESC';
        
        accountest.sortData();
        accountest.accId = tact.Id;
        accountest.delCourse();
    
        accountest.rowToRemove = 1;
        accountest.listWithSelectOptions = srtList;
        accountest.removeFilter();
         }
    
    
static testMethod void testSearchData()
    {
        
        Account tact = dataCreation();
         

        PageReference testPage = Page.AccountActiveInactiveCourse;
        testPage.getParameters().put('AccountName',tact.Name);
        testPage.getParameters().put('AccountId',tact.id);
        testPage.getParameters().put('Type','Active');
    Test.setCurrentPageReference(testPage);
        Apexpages.StandardController stdController = new Apexpages.StandardController(tact);
        AccountActiveInactiveCourseExtn accountest = new AccountActiveInactiveCourseExtn(stdController);
        accountest.addNewFilter1();
        
       
        List<AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1> tempList = accountest.listWithSelectOptions1;
        AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1 filter = tempList.get(0);
        filter.searchText = 'Test';
        filter.selectedProductFilterValue = 'coursespecialty__c';
        filter.selectedCondition = '=';
                
        AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1 filter1 = tempList.get(1);
        filter1.searchText = 'Active';
        filter1.selectedProductFilterValue = 'status__c';
        filter1.selectedCondition = '!=';
        
        
        accountest.searchResults();
        accountest.rowToRemove1 = 1;
        accountest.listWithSelectOptions1 = tempList;
        accountest.removeFilter1();
         }
    
     static testMethod void testSearchData1()
    {
        
        Account tact = dataCreation();
         

        PageReference testPage = Page.AccountActiveInactiveCourse;
        testPage.getParameters().put('AccountName',tact.Name);
        testPage.getParameters().put('AccountId',tact.id);
        testPage.getParameters().put('Type','Active');
    Test.setCurrentPageReference(testPage);
        Apexpages.StandardController stdController = new Apexpages.StandardController(tact);
        AccountActiveInactiveCourseExtn accountest = new AccountActiveInactiveCourseExtn(stdController);
        accountest.addNewFilter1();
        accountest.addNewFilter1();
       
        List<AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1> tempList = accountest.listWithSelectOptions1;
        
        
        AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1 filter2 = tempList.get(0);
        filter2.searchText = '1';
        filter2.selectedProductFilterValue = 'fall_front_list__c';
        filter2.selectedCondition = '!=';
        
        
        AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1 filter3 = tempList.get(1);
        filter3.searchText = 'Test';
        filter3.selectedProductFilterValue = 'name';
        filter3.selectedCondition = 'like %';
        
     
        
        accountest.searchResults();
        accountest.rowToRemove1 = 1;
        accountest.listWithSelectOptions1 = tempList;
        accountest.removeFilter1();
         }
    static testMethod void testSearchData2()
    {
        
        Account tact = dataCreation();
         

        PageReference testPage = Page.AccountActiveInactiveCourse;
        testPage.getParameters().put('AccountName',tact.Name);
        testPage.getParameters().put('AccountId',tact.id);
        testPage.getParameters().put('Type','Active');
    Test.setCurrentPageReference(testPage);
        Apexpages.StandardController stdController = new Apexpages.StandardController(tact);
        AccountActiveInactiveCourseExtn accountest = new AccountActiveInactiveCourseExtn(stdController);
        accountest.addNewFilter1();
        accountest.addNewFilter1();
       
        List<AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1> tempList = accountest.listWithSelectOptions1;
 
        AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1 filter4 = tempList.get(0);
        filter4.searchText = 'Test';
        filter4.selectedProductFilterValue = 'Name';
        filter4.selectedCondition = 'like %%';
        
        AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1 filter5 = tempList.get(1);
        filter5.searchText = 'Test';
        filter5.selectedProductFilterValue = 'Name';
        filter5.selectedCondition = 'like';
        
        accountest.searchResults();
     
         }
     static testMethod void testSearchData3()
    {
        
        Account tact = dataCreation();
         

        PageReference testPage = Page.AccountActiveInactiveCourse;
        testPage.getParameters().put('AccountName',tact.Name);
        testPage.getParameters().put('AccountId',tact.id);
        testPage.getParameters().put('Type','Active');
    Test.setCurrentPageReference(testPage);
        Apexpages.StandardController stdController = new Apexpages.StandardController(tact);
        AccountActiveInactiveCourseExtn accountest = new AccountActiveInactiveCourseExtn(stdController);
        accountest.addNewFilter1();
        accountest.addNewFilter1();
       
        List<AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1> tempList = accountest.listWithSelectOptions1;

        AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1 filter4 = tempList.get(0);
        filter4.searchText = '1';
        filter4.selectedProductFilterValue = 'fall_front_list__c';
        filter4.selectedCondition = '>';
        
        AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1 filter5 = tempList.get(1);
     filter5.searchText = '2';
        filter5.selectedProductFilterValue = 'fall_front_list__c';  
        filter5.selectedCondition = '<';
        
        accountest.searchResults();
       
         }
    static testmethod void testchangeSerachTextType(){
        Account tact = dataCreation();
    PageReference testPage = Page.AccountActiveInactiveCourse;
        testPage.getParameters().put('selectedRow', '0');
        testPage.getParameters().put('AccountName',tact.Name);
        testPage.getParameters().put('AccountId',tact.id);
        testPage.getParameters().put('Type','Active');
    Test.setCurrentPageReference(testPage);
        Apexpages.StandardController stdController = new Apexpages.StandardController(tact);
        AccountActiveInactiveCourseExtn accountest = new AccountActiveInactiveCourseExtn(stdController);
    accountest.addNewFilter1();
    accountest.addNewFilter1();
    List<AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1> tempList = accountest.listWithSelectOptions1;
    AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1 filter = tempList.get(0);
    filter.searchText = 'Test';
    filter.selectedProductFilterValue = 'name';
    //filter.selectedCondition = 'like %';
    
    AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1 filter1 = tempList.get(1);
    filter1.searchText = 'Emer';
    filter1.selectedProductFilterValue = 'coursespecialty__c';
    //filter1.selectedCondition = '<'; 
    
    AccountActiveInactiveCourseExtn.dynamicAddFilterSearch1 filter2 = tempList.get(2);
    filter2.searchText = 'true';
    filter2.selectedProductFilterValue = 'status__c';
    //filter2.selectedCondition = '=';
    accountest.listWithSelectOptions1 = tempList;
    accountest.changeSerachTextType();
    
    testPage.getParameters().put('selectedRow', '1'); //Page requires an object id (Opportunity or University Course)
    Test.setCurrentPageReference(testPage);
    accountest.changeSerachTextType();
    
    testPage.getParameters().put('selectedRow', '2'); //Page requires an object id (Opportunity or University Course)
    Test.setCurrentPageReference(testPage);
    accountest.changeSerachTextType();
   
    }
    static testmethod void testgetStatuses(){

        PageReference testPage = Page.AccountActiveInactiveCourse;
        Account tact = dataCreation();
      testPage.getParameters().put('fieldName', 'status__c');
        testPage.getParameters().put('AccountName',tact.Name);
        testPage.getParameters().put('AccountId',tact.id);
        testPage.getParameters().put('Type','Active');
    Test.setCurrentPageReference(testPage);
        Apexpages.StandardController stdController = new Apexpages.StandardController(tact);
        AccountActiveInactiveCourseExtn accountest = new AccountActiveInactiveCourseExtn(stdController);
    Test.startTest();
    accountest.getStatuses();
    Test.stopTest();
    testPage.getParameters().put('fieldName', 'Fall_Front_List__c');
    Test.setCurrentPageReference(testPage);
    accountest.getStatuses();
    
    
    }
}