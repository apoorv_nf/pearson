/****************************************************************************************************************************
* Apex Class Name   : PS_InternalRequestControllerTest 
* Developer                    Date                    Description
* -------------------------------------------------------------------------------------------------------------------------------
* Madhusudhan                 24/11/2015                Test class for class PS_InternalRequestController
*********************************************************************************************************************************/
@isTest
public class PS_InternalRequestControllerTest {
  
   
     static testMethod void testInternalRequestController(){
    
        // Test data preparation        
        
        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =:Label.Pearson_Sales_User_OneCRM].Id, 3);
        List<UserRole> userRoleDMList = TestDataFactory.getUserRole('NA_District_Sales_Manager_2O');
        List<UserRole> userRoleELTList = TestDataFactory.getUserRole('ANZ_ELT');
        usList[0].Market__c = Label.PS_USMarket;
        usList[0].Business_Unit__c = Label.PS_BU; 
        usList[0].UserRoleId = userRoleDMList[0].Id;  
        usList[1].Market__c = Label.PS_USMarket;
        usList[1].Business_Unit__c = Label.PS_BU;  
        usList[1].UserRoleId = userRoleELTList[0].Id;
        usList[2].Market__c = Label.PS_UKMarket;
         
        usList[2].Manager = null;
        insert usList;  
        
        test.startTest();           
            
            system.runAs(usList[0]){
                
                List<Internal_Request__c> intReqList = TestDataFactory.createInternalRequest(1,'Territory_Realignment');                
                insert intReqList;
                List<Attachment> attList = TestDataFactory.createAttachment(intReqList[0].Id,1);
                insert attList;
                   
                system.debug('attList size in second method'+attList.size()); 
                PS_InternalRequestController psInternalObject = new PS_InternalRequestController();
                psInternalObject.approveProcess();                            
            } 
            system.runAs(usList[1]){
                
                PS_InternalRequestController psInternalObject = new PS_InternalRequestController();
                psInternalObject.approveProcess();               
            }
            system.runAs(usList[2]){
                
                PS_InternalRequestController psInternalObject = new PS_InternalRequestController();
                psInternalObject.approveProcess(); 
            } 
        
        test.stopTest();
    
    }

}