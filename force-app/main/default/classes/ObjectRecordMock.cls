@isTest
global class ObjectRecordMock implements HttpCalloutMock {
   
    global HTTPResponse respond(HTTPRequest request) {
         HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"count":"552","name":"Account","name":"Contact","name":"User","name":"UserRole","name":"Task","name":"Product2","name":"OrderItem","name":"Account_Correlation__c","name":"AccountContact__c","name":"Asset","name":"Campaign","name":"CampaignMember","name":"Case","name":"Competitor_Product__c","name":"Contact_Correlation__c","name":"Event","name":"EventRelation","name":"Lead","name":"Opportunity","name":"OpportunityContactRole","name":"OpportunityHistory","name":"OpportunityLineItem","name":"Order","name":"ObjectTerritory2AssignmentRule","name":"ObjectTerritory2AssignmentRuleItem","name":"ObjectTerritory2Association","name":"OpportunityUniversityCourse__c","name":"Pearson_Choice__c","name":"Pearson_Course_Equivalent__c","name":"Price_List_Mapping__c","name":"PricebookEntry","name":"Product__c","name":"RuleTerritory2Association","name":"Territory2","name":"Territory2Model","name":"Territory2Type","name":"UniversityCourse__c","name":"UniversityCourseContact__c","name":"UserTerritory2Association","name":"VisitPlan__c","name":"Apttus_Config2__ClassificationHierarchy__c","name":"Apttus_Config2__ClassificationName__c","name":"Apttus_Config2__PriceList__c","name":"Apttus_Config2__PriceListCategory__c","name":"Apttus_Config2__PriceListItem__c","name":"Apttus_Config2__ProductClassification__c","name":"Apttus_Config2__RelatedProduct__c"}');
        response.setStatusCode(200);
        return response;
    }
    
 
}