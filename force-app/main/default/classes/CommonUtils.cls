/* -----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            CommonUtils.cls 
   Description:      
   Date             Version         Author                            Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
   
  13/10/2015         1.2         Rahul Boinepally          Included getLeadBypassANZRecordType method to validate if the passed record type matches the record 
                                                           type of Lead record.     
                                                           Note - Couldnt rename to standard 'PS' as this class is being used in more than 3 other triggers.
  17/12/2015         1.3         Ajay                      Removed getLiveChatTranscriptRecord method 
  08-May-2019         1.4         Navaneeth \ Harika        For CR-02761 - Account Merge - Customer Portal Custom Field Population Functionality                                                      
------------------------------------------------------------------------------------------------------------------------------------------------------------ */



public with sharing class CommonUtils {
    
    
    public static List<Account> getAccountbypassRecordType(String recordType,List<Account> triggeredAccounts){
        
        List<Account> accountList = new List<Account>();
        Schema.DescribeSObjectResult describeSobjectResult = Schema.SObjectType.Account; 
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeSobjectResult.getRecordTypeInfosById();
        
        if(triggeredAccounts != null)
            for(Account a : triggeredAccounts){
                if(a.RecordTypeId!=null){
                    if(!recordType.contains(recordTypeMap.get(a.RecordTypeId).getName()))
                        accountList.add(a);
                }
            } 
        
        return accountList;
    }
    
    public static List<Contact> getContactbypassRecordType(String recordType,List<Contact> triggeredContacts){
        
        List<Contact> contactList = new List<Contact>();
        Schema.DescribeSObjectResult describeSobjectResult = Schema.SObjectType.Contact; 
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeSobjectResult.getRecordTypeInfosById();
        
        if(triggeredContacts != null)
            for(Contact c : triggeredContacts){
                if(c.RecordTypeId!=null){
                    if(!recordType.contains(recordTypeMap.get(c.RecordTypeId).getName()))
                        contactList.add(c);
                }
            } 
        
        return contactList;
    }
    
    public static List<Account> getAccountRecordType(String recordType,List<Account> triggeredAccounts){
        
        List<Account> accountList = new List<Account>();
        Schema.DescribeSObjectResult describeSobjectResult = Schema.SObjectType.Account; 
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeSobjectResult.getRecordTypeInfosById();
        
        if(triggeredAccounts != null)
            for(Account a : triggeredAccounts){
                if(a.RecordTypeId!=null){
                    if(recordType.contains(recordTypeMap.get(a.RecordTypeId).getName()))
                        accountList.add(a);
                }
            } 
        
        return accountList;
    }

    public static List<Case> getCaseBypassANZRecordType(String recordType,List<Case> triggeredCases){
        
        List<Case> caseList = new List<Case>();
        Schema.DescribeSObjectResult describeSobjectResult = Schema.SObjectType.Case; 
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeSobjectResult.getRecordTypeInfosById();
        
        if(triggeredCases != null) for(Case c : triggeredCases){ if(c.RecordTypeId!=null){
                     if(!recordType.contains(recordTypeMap.get(c.RecordTypeId).getName()))  //if(recordType.contains(recordTypeMap.get(c.RecordTypeId).getName()))   
                        caseList.add(c);
                }
            } 
        
        return caseList;
    }
    
    public static List<Lead> getLeadBypassANZRecordType(String recordType,List<Lead> triggeredLeads)
    {   
        List<Lead> leadList = new list<Lead>();
        Schema.DescribeSobjectResult leadSobjectResult = Schema.SobjectType.lead;
        Map<Id,schema.RecordTypeInfo> recordTypemap = leadSobjectResult.getRecordTypeInfosByID();
            
        if (triggeredLeads != null) for(Lead leadFor : triggeredLeads)
            { if(leadFor.RecordTypeID != null)
                {
                    if(!recordType.equals(recordTypeMap.get(leadFor.recordTypeId).getName())) leadList.add(leadFor);
                }
            } 
        
        return leadList;
    }
    
    /**
    * Description : Method to get Service Cases only
    * @param NA
    * @return List
    * @throws NA
    **/
     public static List<Case> getCaseServiceRecordType(String recordType,List<Case> triggeredCases){        
        List<Case> caseList = new List<Case>(); 
        Schema.DescribeSObjectResult describeSobjectResult = Schema.SObjectType.Case; 
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeSobjectResult.getRecordTypeInfosById();
        if(triggeredCases != null){
            for(Case c : triggeredCases){ if(c.RecordTypeId!=null){
                     if(recordType.contains(recordTypeMap.get(c.RecordTypeId).getName())){ caseList.add(c);
                     } } }//end of for 
        }//end of if     
      return caseList;
    }
    
    /*
    Created the method newly for Account Record Bypass - For Before Trigger alone.
    Created by Navaneeth \ Harika for CR-02761
    */
    public static List<Account> getAccountbypassRecordTypebefore(String recordType,List<Account> triggeredAccounts){
        
        List<Account> accountList = new List<Account>();
        Schema.DescribeSObjectResult describeSobjectResult = Schema.SObjectType.Account; 
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeSobjectResult.getRecordTypeInfosById();
        
        if(triggeredAccounts != null)
            for(Account a : triggeredAccounts){
                if(a.RecordTypeId!=null){
                    if(!recordType.contains(recordTypeMap.get(a.RecordTypeId).getName()))
                    {
                        if(a.isCustomerPortal == true )
                        {
                            a.is_Customer_Portal__c = true;
                        }
                        accountList.add(a);
                    }
                }
            }
        return accountList;
    }
}