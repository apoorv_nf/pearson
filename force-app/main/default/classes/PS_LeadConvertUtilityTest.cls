/* -------------------------------------------------------------------------------------------------------------------------------------------
Name:            PS_LeadConvertUtilityTest
Description:     Test class for PS_LeadConvertUtility and LeadConversionChecker
Date             Version           Author                                          Summary of Changes 
-----------      ----------   -----------------     ---------------------------------------------------------------------------------------
04 Dec 2015      1.0           Accenture NDC        Created
---------------------------------------------------------------------------------------------------------------------------------------------- */
@isTest(SeeAllData=true)
private class PS_LeadConvertUtilityTest 
{
    static testMethod void myUnitTest1() 
    {
        User ius = [select id,Geography__c,CurrencyIsoCode,Line_of_Business__c,Market__c,Business_Unit__c from user where id='005b0000002fw6N' limit 1];
        ius.Geography__c = 'Global';
        ius.CurrencyIsoCode='USD';
        ius.Line_of_Business__c = 'Higher Ed';
        ius.Market__c = 'US';
        ius.Business_Unit__c = 'US Field Sales';
        update ius;
        
         Bypass_Settings__c bs =  [select SetupOwnerId, Disable_Integration_Requests__c, Disable_Triggers__c, Disable_Validation_Rules__c, Disable_Workflow_Rules__c, Disable_Process_Builder__c  from Bypass_Settings__c  where SetupOwnerId = '005b0000002fw6N'];
        bs.SetupOwnerId = ius.id;
        bs.Disable_Integration_Requests__c = false;
        bs.Disable_Triggers__c = true;
        bs.Disable_Validation_Rules__c = true;
        bs.Disable_Workflow_Rules__c = false;
        bs.Disable_Process_Builder__c = true;
        update bs;
         
           system.runas(ius){
            List<Account> accs = TestDataFactory.createAccount(1,'Corporate');
            accs[0].isBeingConvertedTo__c = true;
            accs[0].isBeingConvertedToText__c = 'True';
            accs[0].Market2__c = 'US';
            accs[0].Business_Unit__c = 'Pearson Assessment';
            accs[0].Line_of_Business__c = 'Schools';
            accs[0].Geography__c = 'Core';
            accs[0].Organisation_Type__c = 'School';
            insert accs;
            List<Lead> leads = TestDataFactory.createLead(1,'B2B');
            leads[0].isBeingConverted__c = true;
            leads[0].Institution_Organisation__c = accs[0].id;
            leads[0].market__C = 'AU';
            leads[0].Line_of_Business__c = 'Schools';
            leads[0].Geography__c = 'Core';
            leads[0].Business_Unit__c =  'Pearson Assessment'; 
            leads[0].ownerid = UserInfo.getUserId();
            insert leads;
            List<Account> accounts = TestDataFactory.createAccount(2,'Corporate');
            accounts[0].isBeingConvertedTo__c = true;
            accounts[0].isBeingConvertedToText__c = 'True';
            accounts[0].Converted_Lead_Id__c = leads[0].Id;
            accounts[0].Market2__c = 'US';
            accounts[0].Business_Unit__c = 'Pearson Assessment';
            accounts[0].Organisation_Type__c = 'School';
            insert accounts;
            
            AccountTeamMember acountTeamMember= new AccountTeamMember (
               userId = UserInfo.getUserId(), 
                
                TeamMemberRole='Sales',AccountId =accounts[0].id );
           Insert acountTeamMember;
            Map<Id, Account> accountMap = new Map<Id, Account>(accounts);
            Test.startTest();
            LeadConversionChecker.setLeadConversionStatus(accounts);
            PS_LeadConvertUtility plu = new PS_LeadConvertUtility();
            plu.rollbackIncorrectAccountConversion(accountMap, accountMap);
            plu.updateAccountTeamShare();      
            Test.stopTest();
        } 
    }
    static testMethod void myUnitTest11() 
    {
        User ius = [select id,Geography__c,CurrencyIsoCode,Line_of_Business__c,Market__c,Business_Unit__c from user where id='005b0000002fw6N' limit 1];
        ius.Geography__c = 'Global';
        ius.CurrencyIsoCode='USD';
        ius.Line_of_Business__c = 'Higher Ed';
        ius.Market__c = 'US';
        ius.Business_Unit__c = 'US Field Sales';
        update ius;
        
        Bypass_Settings__c bs =  [select SetupOwnerId, Disable_Integration_Requests__c, Disable_Triggers__c, Disable_Validation_Rules__c, Disable_Workflow_Rules__c, Disable_Process_Builder__c  from Bypass_Settings__c  where SetupOwnerId = '005b0000002fw6N'];
        bs.SetupOwnerId = ius.id;
        bs.Disable_Integration_Requests__c = false;
        bs.Disable_Triggers__c = true;
        bs.Disable_Validation_Rules__c = false;
        bs.Disable_Workflow_Rules__c = false;
        bs.Disable_Process_Builder__c = true;
        update bs;
         
        
        system.runas(ius){
            List<Account> accs = TestDataFactory.createAccount(1,'Corporate');
            accs[0].isBeingConvertedTo__c = true;
            accs[0].isBeingConvertedToText__c = 'false';
            accs[0].Market2__c = 'AU';
            accs[0].Business_Unit__c = 'Pearson Assessment';
            accs[0].Line_of_Business__c = 'Schools';
            accs[0].Geography__c = 'Core';
            accs[0].Organisation_Type__c = 'School';
            insert accs;
            
            List<Lead> leads = TestDataFactory.createLead(1,'B2B');
            leads[0].isBeingConverted__c = true;
            leads[0].Institution_Organisation__c = null;
            leads[0].market__C = 'AU';
            leads[0].Line_of_Business__c = 'Schools';
            leads[0].Geography__c = 'Core';
            leads[0].company = 'companyTest';
            leads[0].Business_Unit__c =  'Pearson Assessment'; 
            leads[0].ownerid = UserInfo.getUserId(); 
            insert leads;
            
            List<Lead> leads1 = TestDataFactory.createLead(1,'B2B');
            leads1[0].isBeingConverted__c = true;
            leads1[0].Institution_Organisation__c = null;
            leads1[0].market__C = 'AU';
            leads1[0].Line_of_Business__c = 'Schools';
            leads1[0].Geography__c = 'Core';
            leads1[0].company = 'companyTest';
            leads1[0].Business_Unit__c =  'Pearson Assessment'; 
            // leads1[0].ownerid = UserInfo.getUserId(); 
            insert leads1;  
            
            List<Account> accounts = TestDataFactory.createAccount(1,'Corporate');
            accounts[0].isBeingConvertedTo__c = true;
            accounts[0].isBeingConvertedToText__c = 'false';
            accounts[0].Converted_Lead_Id__c = leads[0].Id;
            accounts[0].Market2__c = 'AU';
            accounts[0].Business_Unit__c = 'Pearson Assessment';
            accounts[0].Organisation_Type__c = 'School';
            insert accounts;
            
            List<Account> accounts1 = TestDataFactory.createAccount(1,'Corporate');
            accounts1[0].isBeingConvertedTo__c = true;
            accounts1[0].isBeingConvertedToText__c = 'false';
            accounts1[0].Converted_Lead_Id__c = leads[0].Id;
            accounts1[0].Market2__c = 'AU';
            accounts1[0].Business_Unit__c = 'Pearson Assessment';
            accounts1[0].Organisation_Type__c = 'School';
            insert accounts1;
            
            AccountTeamMember acountTeamMember= new AccountTeamMember (
                userId = UserInfo.getUserId(), 
                TeamMemberRole='Sales',AccountId =accounts[0].id );
            system.debug('***acountTeamMember'+acountTeamMember);
            
            Insert acountTeamMember;
            
            Opportunity ioppty= new Opportunity(name = 'test',
                                                isConvertedFromLead__c = true,stagename = 'Closed Won',CloseDate = system.today(), Account = accounts[0]);
            
            insert ioppty;
            Map<Id, Account> accountMap = new Map<Id, Account>(accounts);    
            
            Test.startTest();
            LeadConversionChecker.setLeadConversionStatus(accounts);
            PS_LeadConvertUtility plu = new PS_LeadConvertUtility();
            plu.rollbackIncorrectAccountConversion(accountMap, accountMap);      
            plu.updateAccountTeamShare(); 
            Test.stopTest(); 
        }  
    }
    
    static testMethod void myUnitTest2() 
    { 
        
        User ius = [select id,Geography__c,CurrencyIsoCode,Line_of_Business__c,Market__c,Business_Unit__c from user where id='005b0000002fw6N' limit 1];
        ius.Geography__c = 'Global';
        ius.CurrencyIsoCode='USD';
        ius.Line_of_Business__c = 'Higher Ed';
        ius.Market__c = 'US';
        ius.Business_Unit__c = 'US Field Sales';
        update ius;
        
        Bypass_Settings__c bs =  [select SetupOwnerId, Disable_Integration_Requests__c, Disable_Triggers__c, Disable_Validation_Rules__c, Disable_Workflow_Rules__c, Disable_Process_Builder__c  from Bypass_Settings__c  where SetupOwnerId = '005b0000002fw6N'];
        bs.SetupOwnerId = ius.id;
        bs.Disable_Integration_Requests__c = false;
        bs.Disable_Triggers__c = true;
        bs.Disable_Validation_Rules__c = false;
        bs.Disable_Workflow_Rules__c = false;
        bs.Disable_Process_Builder__c = true;
        update bs;
         
 
        system.runas(ius){
            List<Account> accounts = TestDataFactory.createAccount(1,'Corporate');
            accounts[0].isBeingConvertedTo__c = true;
            accounts[0].isBeingConvertedToText__c = 'True';
            accounts[0].Organisation_Type__c = 'School';
            accounts[0].Market2__c = 'US';
            accounts[0].Business_Unit__c = 'Pearson Assessment';
            insert accounts;   
            
            List<Lead> leads = TestDataFactory.createLead(1,'B2B');
            
            for(Lead l : leads)
            { 
                l.Channel__c ='Direct';
                l.Organisation_Type1__c = 'Large Corporate';
                l.Role__c = 'Employee';
                l.Enquiry_Type__c = 'Product';
                l.isBeingConverted__c = true;
                l.ownerid = ius.id;
                l.Company = 'New Company';
                l.Institution_Organisation__c = null;
                l.company = 'companyTest';
            }
            
            insert(leads);
            
            for(Lead l : leads)
            {
                Database.LeadConvert convertLead = new database.LeadConvert();
                convertLead.setLeadId(l.ID);
                convertLead.setDoNotCreateOpportunity(true);
                convertLead.setAccountId(accounts[0].Id);
                convertLead.setConvertedStatus('Qualified');
                Database.LeadConvertResult lcr = Database.convertLead(convertLead);
                System.assert(lcr.isSuccess());      
            } 
            
            List <Lead> convertedLeads = [SELECT Id, ConvertedAccountId, ConvertedOpportunityId, ConvertedContactId, Role__c, Role_Detail__c, Other_Role_Detail__c, Additional_Responsibilities__c,
                                          Discipline_Of_Interest_Multi_Select__c, Sub_Discipline_of_Interest__c, Decision_Making_Level__c, Sponsor_Type__c, recordtypeId, 
                                          Institution_Organisation__c, Company, Street, City, Country, postalCode, Abbreviated_Name__c, Lead_Owner_For_Account__c, Lead_Record_Type_For_Account__c, 
                                          Lead_Type__c, Line_of_Business__c, Organisation_Type1__c, Other_Lead_Source__c, Pearson_ID_Number__c, RecordID__c, Request_Account_Creation__c, 
                                          Sponsor_Address_City__c, Academic_Achievement__c, Sponsor_Address_Country__c, Sponsor_Address_State_Province__c, Sponsor_Address_Street__c, 
                                          Sponsor_Address_ZIP_Postal_Code__c, Students__c, Sub_Type__c, Type__c, isBeingConvertedText__c, Additional_Sub_Type__c, Business_Unit__c, Geography__c, 
                                          isBeingConverted__c, IsCreatedFromLead__c, Last_Name_Account__c, ownerid  FROM Lead where Id = :leads[0].Id];
            
            List<Account> accs = TestDataFactory.createAccount(1,'Corporate');
            accs[0].isBeingConvertedTo__c = true;
            accs[0].isBeingConvertedToText__c = 'True';
            accs[0].Organisation_Type__c = 'School';
            accs[0].Market2__c = 'AU';
            accs[0].Business_Unit__c = 'Pearson Assessment';
            insert accs;                                              
            //convertedLeads[0].Institution_Organisation__c = accs[0].Id;
            Map<Id, Lead> leadMap = new Map<Id, Lead>(convertedLeads);   
            
            List<Opportunity> Opps = TestDataFactory.createOpportunity(1,'B2B');
            Opps[0].name = 'test';
            Opps[0].isConvertedFromLead__c = true;
            Opps[0].stagename = 'Closed Won';
            Opps[0].accountid = accs[0].id;
            Opps[0].CloseDate = system.today(); 
            Insert opps;
            
            
            AccountTeamMember acountTeamMember= new AccountTeamMember (
                userId = UserInfo.getUserId(), 
                TeamMemberRole='Sales',AccountId =accs[0].id );
            
            
            Insert acountTeamMember;
            
            
            //Map<Id, opportunity> oppMap = new Map<Id, opportunity>(opps);  
            
            Test.startTest(); 
            lead l = new Lead();
            
            PS_LeadConvertUtility plu = new PS_LeadConvertUtility();
            //plu.createnewaccount(l);
            plu.fixIncorrectLeadConversion(leadMap, leadMap);
            plu.updateAccountTeamShare();
            // plu.reparentIncorrectlyParentedOpportunity(leadMap,convertedLeads);
            
            Test.stopTest(); 
        }
    } 
}