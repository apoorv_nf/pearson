/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_SubmitOrderControllerTest
   Description:     Test class for PS_SubmitOrderControllerTest
   Date             Version           Author                                          Summary of Changes 
   -----------      ----------   -----------------     ---------------------------------------------------------------------------------------
   18 Nov 2015      1.0           Accenture NDC                                         Created
---------------------------------------------------------------------------------------------------------------------------------------------------------- */
 
@isTest
public class PS_SubmitOrderControllerTest
{
    static testmethod void testWithOpportunityScenario()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        insert lstWithTestUser;
        /* Order newOrder = new Order();
        test.startTest();
        newOrder = TestDataFactory.returnorder();
        insert lstWithTestUser;
        test.stopTest(); */
        System.runAs(lstWithTestUser[0]) 
        {
             //GK
            Bypass_Settings__c settings = new Bypass_Settings__c();
          settings.Disable_Triggers__c = true;
          settings.SetupOwnerId = lstWithTestUser[0].id;
          insert settings;
            
            Order newOrder = new Order();
          newOrder = TestDataFactory.returnorder();
          test.startTest();
            //Test the class in 'PS_SubmitOrder' page context
            if(newOrder != null)
            {//GK
                PageReference pageRef = new PageReference('/apex/PS_SubmitOrder?orderId='+newOrder.Id+'&opportunityId='+newOrder.Opportunityid+'&accountId='+newOrder.AccountId+'&contactId='+newOrder.ShipToContactid+'&recordType='+'Revenue Order'+'&OrderSubmitted=0');
                Test.setCurrentPage(pageRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(newOrder);
                PS_SubmitOrderController controllerObj = new PS_SubmitOrderController(sc);
                controllerObj.redirect();
                //controllerObj.closePopup();
            }
            test.stopTest();
        }
    } 
    static testmethod void testWithoutOpportunityScenario()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        insert lstWithTestUser;
        
        System.runAs(lstWithTestUser[0]) 
        {
            //GK
            Bypass_Settings__c settings = new Bypass_Settings__c();
          settings.Disable_Triggers__c = true;
          settings.SetupOwnerId = lstWithTestUser[0].id;
          insert settings;
            
            Order newOrder = new Order();
          newOrder = TestDataFactory.returnorder();
       
            test.startTest();
       
            //Test the class in 'PS_SubmitOrder' page context
            if(newOrder != null)
            {//GK
                PageReference pageRef = new PageReference('/apex/PS_SubmitOrder?orderId='+newOrder.Id+'&accountId='+newOrder.AccountId+'&contactId='+newOrder.ShipToContactid+'&recordType='+'Revenue Order'+'&OrderSubmitted=0');
                Test.setCurrentPage(pageRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(newOrder);
                PS_SubmitOrderController controllerObj = new PS_SubmitOrderController(sc);
                controllerObj.redirect();
            }
             test.stopTest();
        }
    }  
    
    static testmethod void testSampleOrder()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
        insert lstWithTestUser;
        
    
        
       
        System.runAs(lstWithTestUser[0]) 
        {
             //GK
            Bypass_Settings__c settings = new Bypass_Settings__c();
          settings.Disable_Triggers__c = true;
          settings.SetupOwnerId = lstWithTestUser[0].id;
          insert settings;
            
            Order newOrder = new Order();
            newOrder = TestDataFactory.returnorder();
             test.startTest();
            //Test the class in 'PS_SubmitOrder' page context
            if(newOrder != null)
            {//GK
                PageReference pageRef = new PageReference('/apex/PS_SubmitOrder?orderId='+newOrder.Id+'&accountId='+newOrder.AccountId+'&contactId='+newOrder.ShipToContactid+'&recordType='+'Sample Order'+'&OrderSubmitted=0');
                Test.setCurrentPage(pageRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(newOrder);
                PS_SubmitOrderController controllerObj = new PS_SubmitOrderController(sc);
                controllerObj.redirect();
            }
            test.stopTest();
        }
    }
    
    static testmethod void testWithOpportunityAndCourseScenario()
    {
        Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,1);
         insert lstWithTestUser;
        
        System.runAs(lstWithTestUser[0]) 
        {
          //GK
            Bypass_Settings__c settings = new Bypass_Settings__c();
          settings.Disable_Triggers__c = true;
          settings.SetupOwnerId = lstWithTestUser[0].id;
          insert settings;
            
            
            List<OpportunityUniversityCourse__c> lstWithOpportunityUniversityCourse = new List<OpportunityUniversityCourse__c>();
            List<UniversityCourse__c> univCourse = new List<UniversityCourse__c>();
            univCourse = TestDataFactory.insertCourse();
            Order newOrder = new Order();
                
            test.startTest();
            newOrder = TestDataFactory.returnorder();
            lstWithOpportunityUniversityCourse = TestDataFactory.createOpportunityUniversityCourse();
            lstWithOpportunityUniversityCourse[0].Opportunity__c = newOrder.opportunityId;
            lstWithOpportunityUniversityCourse[0].Account__c = newOrder.AccountId;
            for(UniversityCourse__c newUnivCourse : univCourse)
            {
                newUnivCourse.Account__c = newOrder.AccountId;
            }
            insert univCourse;   
            lstWithOpportunityUniversityCourse[0].UniversityCourse__c = univCourse[0].Id;
            //insert lstWithOpportunityUniversityCourse;
          
                //Test the class in 'PS_SubmitOrder' page context
                if(newOrder != null)
                {//GK
                    PageReference pageRef = new PageReference('/apex/PS_SubmitOrder?orderId='+newOrder.Id+'&opportunityId='+newOrder.Opportunityid+'&accountId='+newOrder.AccountId+'&contactId='+newOrder.ShipToContactid+'&recordType='+'Revenue Order'+'&OrderSubmitted=0');
                    Test.setCurrentPage(pageRef);
                    ApexPages.StandardController sc = new ApexPages.StandardController(newOrder);
                    PS_SubmitOrderController controllerObj = new PS_SubmitOrderController(sc);
                    //controllerObj.redirect();
                    controllerObj.closePopup();
                }
            test.stopTest();
        }
    }     
}