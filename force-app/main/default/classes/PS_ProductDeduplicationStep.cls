/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_ProductDeduplicationStep.cls
   Description:     Class that represent the step to invoke batch job to remove duplicated products


   Test Class:      PS_ApttusHierarchyUpdateTest
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   23/02/2016         1.0           Davi Borges             RD-01702                         Created
                                         
-----------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------- */

public class PS_ProductDeduplicationStep {
	
	public class PS_ProductDeduplicationStepResponse {
        
    	@InvocableVariable (label='Error Message' description='Error message related to the request' )
        public String error;

        @InvocableVariable (label='Status Code' description='Status Code 0 = Success, -1 failure' required=true)
        public Integer status;

        @InvocableVariable (label='Job Id ' description='Job Id of product deduplication')
        public String job_id ;

    }

    public class PS_ProductDeduplicationStepRequest {
    	@InvocableVariable (label='Job Request Execution Id' description='Salesforce Id of the Job Execution Request' )
    	public String Id;

    }

    @InvocableMethod(
        label='Invoke Product Deduplication Job'
        description='Invoke Product Deduplication Job')
    public static List<PS_ProductDeduplicationStepResponse> invokeJob(List<PS_ProductDeduplicationStepRequest > requests) {
       
       List<PS_ProductDeduplicationStepResponse> responses = new List<PS_ProductDeduplicationStepResponse>();
       
       List<String> job_ids = new   List<String>();
       
       for(PS_ProductDeduplicationStepRequest request : requests)
       {
           job_ids.add(request.id);
       }

       //same response to all requests
       PS_ProductDeduplicationStepResponse response = invokeJob(job_ids);

       for(PS_ProductDeduplicationStepRequest request : requests)
       {
           responses.add(response);
       }
       
       return responses;
    }


    public static PS_ProductDeduplicationStepResponse invokeJob(List<String> job_ids) {
       
       	PS_ProductDeduplicationStepResponse response = new PS_ProductDeduplicationStepResponse();
       	response.status = 0 ;
        response.error = '';    
       
       	Id apexJobId = Database.executeBatch(new PS_DeactivateDuplicateProductsBatch(null,job_ids), 200); 
       
       	response.job_id = apexJobId;

       	//Design Decision: batch job will be aborted during test execution
    	if(Test.isRunningTest()){
    		System.abortJob(apexJobId);
    	}
       
       return response;

    }
}