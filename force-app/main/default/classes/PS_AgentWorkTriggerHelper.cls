/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_AgentWorkTriggerHelper.cls
   Description:     On update of Agentwork record 
   Date             Version              Author                          Summary of Changes
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  08-March-2016       0.1              Sudhakar Navuluri                 Initial Release 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
public class PS_AgentWorkTriggerHelper
{ 
public static void afterUpdate(List<AgentWork> agentworkList)
    {
        PS_AgentWorkTriggerHandler.onafterupdate(agentworkList);
    }
    
}