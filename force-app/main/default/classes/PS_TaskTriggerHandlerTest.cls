/************************************************************************************************************
* Apex Interface Name : PS_TaskTriggerHandlerTest 
* Version             : 1.0 
* Created Date        : 9/9/2015 4:44 AM
* Modification Log    : 
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Abhinav Srivastav         9/9/2015              R2.1 version
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

@isTest
private Class PS_TaskTriggerHandlerTest { // Test Class to cover TaskTriggerHandler Class
    static testMethod void myUnitTest() { // Test Method to cover TaskTestHandler Class
       //  List<User> listWithUser = new List<User>();
        // listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1);
        List<User> usrLst = TestDataFactory.createUser(Userinfo.getProfileId());
        insert usrLst;
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true);
         insert byp;
         Map<Id,Task> newTasks=new Map<Id,Task>();
         Map<Id,Task> oldTasks= new map<id,task>();
         Map<Id,Task> newTasksupdate=new Map<Id,Task>();
         Map<Id,Task> oldTasksupdate= new map<id,task>();
         Map<Id,Task> newTasksdelete=new Map<Id,Task>();
         Map<Id,Task> oldTasksdelete= new map<id,task>();
         List<Task> tasklist=new list<task>();
         List<Task> tasklistupdate=new list<task>();
         List<Task> tasklistdelete=new list<task>();
         Map<String,List<String>> exceptions = new Map<String,List<String>>();
         List<String> strlist = new List<String>{'a','b','c'};
        // System.runAs(listWithUser[0]){
      //  User u1= [SELECT Id,Name FROM User WHERE ID='005b0000000wuJP'];
        system.runas(usrLst[0]){
        Account acc = new Account(Name = 'AccTest1',Line_of_Business__c='Schools',CurrencyIsoCode='GBP',Geography__c = 'Growth',Organisation_Type__c = 'Higher Education',Type = 'School',Market2__c='US',Phone = '9989887687');
        acc.ShippingStreet = 'TestStreet';
        acc.ShippingCity = 'Vns';
        acc.ShippingState = 'Delhi';
        acc.ShippingPostalCode = '234543';
        acc.ShippingCountry = 'India';
        acc.IsCreatedFromLead__c = TRUE;
        insert acc;
        Contact con = new Contact(FirstName='AssetHandler',LastName ='Test',Phone='9999888898',Email='AssetHandler.Test@testclass.com', AccountId = acc.Id,
                                  Preferred_Address__c = 'Mailing Address', MailingCountry = 'United Kingdom', MailingStreet = '1 Street', MailingPostalCode = 'NE27 0QQ', MailingCity  = 'City');
        insert con;
        Opportunity ro = new Opportunity(Name= 'OpTest', AccountId = acc.id, StageName = 'Solutioning', Type = 'New Business', Academic_Vetting_Status__c = 'Un-Vetted', Academic_Start_Date__c = System.Today(),CloseDate = System.Today(),International_Student__c = true);
        insert ro;
        System.debug('opportunity1 inserted' +ro.Name);
        Task ts = new Task(Subject ='Email',Priority ='Normal', Status ='Not Started', WhatId = ro.Id,Ownerid = userinfo.getUserId());
        //insert ts;
        Task ts2 = new Task(Subject ='Call',Priority ='Normal', Status ='Not Started', WhatId = ro.Id,Ownerid = userinfo.getUserId());
        insert ts2;
        Task ts3 = new Task(Subject ='Email',Priority ='High', Status ='Not Started', WhatId = ro.Id,Ownerid = userinfo.getUserId());
        insert ts3;
        System.debug('subject' +ts.subject);
        //System.debug('id is :' +ts.id);
        System.debug('check list1' +tasklist);
        tasklist.add(ts);
        //tasklistupdate.add(ts2);
        tasklistdelete.add(ts3);
        System.debug('check list3' +tasklist);
       if(!tasklist.isEmpty() ){
        System.debug('check list4' +tasklist);
       //try{
            insert tasklist;
            //insert tasklistupdate;
            ts2.subject='Email';
            //update ts2;
            tasklistupdate.add(ts2);
            update tasklistupdate;
            tasklistdelete.add(ts3);
            delete ts3;
            //}catch(System.DMLException de){
            //System.debug('check list5' +tasklist);
        //}
        }
        system.debug('check list2' +tasklist);
        //insert ts;
        
        
        newTasks.put(ts.id,ts);
        oldTasks.put(ts.id,ts);
        newTasksupdate.put(ts2.id,ts2);
        oldTasksupdate.put(ts2.id,ts2);
        newTasksdelete.put(ts3.id,ts3);
        oldTasksdelete.put(ts3.id,ts3);
        exceptions.put('1',strlist);
        System.debug('inside debug' +tasklist +newtasks +oldtasks);
        User u = [Select id, Alias from User where Alias='duser' limit 1];
        
       // system.runas(u1){
       // system.runas(u){
        Test.StartTest();
                
        PS_TaskTriggerHandler.beforeTaskOpptyvalidation(tasklistupdate,newTasksupdate,oldTasksupdate,'Update');
        PS_TaskTriggerHandler.beforeTaskOpptyvalidation(null,null,oldTasksdelete,'Delete');
        PS_TaskTriggerHandler.beforecreateTaskOpptyvalidation(tasklist,newTasks,null,'Insert');
        Test.StopTest();
       // }
        
        }
    }    
}