global class BatchEmailHasher implements Database.Batchable<sObject> {
    
    
    global BatchEmailHasher() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id, Email__c, Hash__c FROM Email_Hasher__c WHERE Hash__c = null OR Hash__c = '']);
    }

    global void execute(Database.BatchableContext BC, List<Email_Hasher__c> scope) {

        List<Email_Hasher__c> emailHashesToUpdate = new List<Email_Hasher__c>();

        for(Email_Hasher__c emailHash : scope) {
            emailHashesToUpdate.add(generateHash(emailHash));
        }

        update emailHashesToUpdate;
    
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }


    private Email_Hasher__c generateHash(Email_Hasher__c emailHash) {

        Blob hash = Crypto.generateDigest('SHA-256', Blob.valueOf(emailHash.Email__c.toLowerCase()));
        emailHash.Hash__c = EncodingUtil.convertToHex(hash);
        return emailHash;
        

    }
    
}