/**
/**
 * @author         Comity Designs, Inc.
 * @version        1.0
 * @date           Aug 30 2017
 * @description    Util class for the Controller for Visualforce page and Lightning Component (ProductsToAssetsCtrl)
 */
 
public with sharing class ProductsToAssetsUtil {
    
    public static List<Asset> getConvertedAssets(Id OpportunityId){
          
          String accountId = ([SELECT Opportunity.Account.Id FROM Opportunity WHERE Id =:opportunityId LIMIT 1])[0].Account.Id;
          
          return new List < Asset > ([Select Name,
      Quantity,
      Price,
      Product2.Name,
      Status,
      Forecasted_Install_Date__c,
      PurchaseDate,
      UsageEndDate,
      SerialNumber,
      Account.Name,
      InstallDate,
      Purchase_Opportunity__r.Name,
      Oli__c,
      Sales_Channel__c,
      Other_Mode_of_Delivery__c,
      Mode_of_Delivery__c        
      from Asset
      where Converted_Asset__c = true and
      AccountId =: accountId and
      Purchase_Opportunity__c =: opportunityId
      order by CreatedDate desc
    ]);
    }
    
    public static List<OpportunityLineItem> getOlis(Id opportunityId){
            return new List<OpportunityLineItem>([Select o.Id,
        o.IsDeleted,
        o.ListPrice,
        o.OpportunityId,
        o.Opportunity.CloseDate,
        o.Opportunity.Name, //added Opportunity Name for concatenation in Asset Name
        o.PricebookEntry.Product2Id,
        o.PricebookEntryId,
        o.PricebookEntry.Name,
        o.Quantity,
        o.ServiceDate, //Relabeled to the Forecasted Install Date
        o.TotalPrice,
        o.UnitPrice,
        o.Forecasted_Install_Date__c, //Forecasted Install Date field; Displaying in Visualforce page
         o.Sales_Channel__c, //Sales Channel Picklist field; Displaying in Visualforce page
         o.Bookshop__c,  //Bookshop Lookup field
         o.Expected_Sales_Quantity__c,
         o.Required_by_Date__c, 
         o.Suggested_Order_Quantity__c,
         o.Discount,
         o.Description,
         o.Mode_of_Delivery__c  //Mode of Delivery Picklist field; Displaying in Visualforce page
        from OpportunityLineItem o
        where o.IsDeleted = false and
        o.OpportunityId =: opportunityId
      ]);
     
    }
    
    public static list<Competitor_Product__c> getComplis(Id opportunityId){
            return new list<Competitor_Product__c>([Select Id,
                            Name,
                            ISBN10__c,Opportunity__c,Product__c                                                                       
                    from    Competitor_Product__c 
                    where   IsDeleted=false and 
                            Opportunity__c=:opportunityId]);
     
    }
    public static Map < String, Integer > getConvertedAssetsMap(Id opportunityId){
        Map < String, Integer > convertedAssetsMap = new Map < String, Integer > ();
        for (Asset a: getConvertedAssets(opportunityId)) {
          if(convertedAssetsMap.containsKey(a.Oli__c)){
              if (convertedAssetsMap.get(a.Oli__c) == null) {
                  system.debug('a.Quantity:'+a.Quantity);
                  if(a.Quantity!=NULL){
                    convertedAssetsMap.put(a.Oli__c, a.Quantity.intValue());
                  }else{
                      convertedAssetsMap.put(a.Oli__c,0);
                  }
              } else {
                  if(a.Quantity!=NULL){
                    convertedAssetsMap.put(a.Oli__c, convertedAssetsMap.get(a.Oli__c) + a.Quantity.intValue());
                  }else{
                      convertedAssetsMap.put(a.Oli__c, convertedAssetsMap.get(a.Oli__c));
                  }
              }
          }
        }
        return convertedAssetsMap;      
    }
    
    public static Asset getAssetFromOli(Id opportunityId, OpportunityLineItem o, String accountId){
          Asset a1 = new Asset();
          Map < String, Integer > convertedAssetsMap = getConvertedAssetsMap(opportunityId);
          Integer convertedAssetsQuantity = convertedAssetsMap.get(o.Id) == null ? 0 : convertedAssetsMap.get(o.Id);
      
      a1.Name = o.PriceBookEntry.Name + ' + ' + o.Opportunity.Name;
      a1.Product2Id = o.PriceBookEntry.Product2Id;
      a1.AccountId = accountId;
      a1.Purchase_Opportunity__c = opportunityId;
      a1.Contact = null;
      if(o.Quantity > 1.00){
         a1.Quantity=1.00;
                    
         a1.Price=o.TotalPrice/(o.Quantity.intValue() == 0? 1 : o.Quantity.intValue());
      }else{
           a1.Quantity = o.Quantity;
      }
      
      a1.Price = o.TotalPrice;
      a1.InstallDate = o.ServiceDate;
      a1.PurchaseDate = o.Opportunity.CloseDate;
      a1.Converted_Asset__c = true;
      a1.Oli__c = o.Id;
      a1.Status = 'Purchased';
      a1.Sales_Channel__c = o.Sales_Channel__c;
      return a1;
    }
    
    public static Asset getAssetFromCompoli(Id opportunityId, Competitor_Product__c o, String accountId){
        Asset a1 = new Asset();
        a1.Name                     = o.Name;                                  
        a1.AccountId                = accountId;
        a1.Purchase_Opportunity__c  = opportunityId;
        a1.Product2Id               = o.Product__c;
        a1.Contact                  = null;
        a1.Converted_Asset__c       = true;
        a1.Status                   = 'Purchased';
    return a1;
    }
    
    public static Integer getTotalOfConvertedAssets(Id opportunityId){
        system.debug('Entered into ProductsToAssets:'+opportunityId);
        Integer sum = 0;
        List<Asset> convertAssetsList=new List<Asset>();
        convertAssetsList = ProductsToAssetsUtil.getConvertedAssets(opportunityId);
        if(!convertAssetsList.isEmpty()){
            sum=convertAssetsList.size();            
        }
        return sum;
        
    }
    
}