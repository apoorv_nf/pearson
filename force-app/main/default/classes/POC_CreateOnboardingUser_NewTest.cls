/*****************************************************************************************************************************/
/*Description:This class is used to cover code for POC_CreateOnboardingUser_New and POC_MirrorUserPermissionAssignment

/*****************************************************************************************************************************/
@isTest
public class POC_CreateOnboardingUser_NewTest {
			Public static testmethod void UserOnboarding(){
           User u1 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = UserInfo.getProfileId(), 
                               Sample_Approval__c= true,Order_Approver__c= UserInfo.getUserId(),
                               TimeZoneSidKey='America/Los_Angeles', UserName='test1234'+Math.random()+'@pearson.com.testbau', Geography__c = 'Growth',
                               Market__c = 'US',Line_of_Business__c = 'Higher Ed', License_Pools__c = 'ANZ Sales');
            insert u1;
                
             User u2 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', Profileid = [select id from profile where name = 'Pearson Service Cloud User OneCRM'].id, 
                               Sample_Approval__c= true,Order_Approver__c= UserInfo.getUserId(),
                               TimeZoneSidKey='America/Los_Angeles', UserName='test1234'+Math.random()+'@pearson.com.testbau', Geography__c = 'Growth',
                               Market__c = 'US',Line_of_Business__c = 'Higher Ed', License_Pools__c = 'ANZ Sales');   
                insert u2;
                
                POC_Permissionsetassign_Test.UserPermissionSet(u2.id);
                
                
                
            Test.starttest();
                List<id> usronbrdrequest = new list<id>();
                
                License_Management__c lmg = new License_Management__c();
                lmg.Name = 'Pearson Enterprise ESB';               
                lmg.Market__c = 'UK';
                lmg.License_Pool__c = 'ANZ Sales';
                lmg.CurrencyIsoCode = 'AUD';
                insert lmg;
                
                User_License_Tracker__c ULT = new User_License_Tracker__c();    
                ULT.Name = 'ANZ Sales';
                ULT.Market__c = 'ANZ';
                ULT.License_Pool__c = 'ANZ Sales';
                ULT.Allocated_License__c = 100;
                ULT.License_Manage__c = lmg.id;
                ULT.CurrencyIsoCode = 'AUD';
                ULT.License_Type__c = 'Salesforce';
                insert ULT;
                
                User_Onboarding_Request__c usr = new User_Onboarding_Request__c();
                usr.First_Name__c = 'Support';
                usr.Name = 'Agent1';
                usr.Email__c = 'supportagent123@test.com';
                usr.Mirror_User__c = u2.id;
                usr.User_License_Tracker__c = ULT.id;
                insert usr;
                
                //PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Pearson_Sales_Sampling_Request'];
				//insert new PermissionSetAssignment(AssigneeId = usr.Mirror_User__r.id, PermissionSetId = ps.Id);
				list<id> usrid = new list<id>();
				
				User u3 = new User(Alias = 'standt', Email='standarduser11@pearson.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', Profileid = [select id from profile where name = 'Pearson Service Cloud User OneCRM'].id, 
                               Sample_Approval__c= true,Order_Approver__c= UserInfo.getUserId(),
                               TimeZoneSidKey='America/Los_Angeles', UserName='test1234'+Math.random()+'@pearson.com.testbau', Geography__c = 'Growth',
                               Market__c = 'US',Line_of_Business__c = 'Higher Ed', License_Pools__c = 'ANZ Sales'); 
                insert u3;
                usrid.add(u3.id);
                
				User_and_License_Tracking__c usrlt = new User_and_License_Tracking__c();
                usrlt.check_user__c = TRUE;
                usrlt.Deactivate_User__c = false;
				usrlt.User__c = u3.id;
                usrlt.Name = 'Test';
                usrlt.User_License_Tracker__c = ULT.id;
                usrlt.License_Pool__c = 'ANZ Sales';
                insert usrlt;
                
                    
               usronbrdrequest.add(usr.Id);
                
                POC_CreateOnboardingUser_New.createUser(usronbrdrequest);
                POC_JunctionObjectDeletion.JunctionObjectDelete(usrid);
            Test.stopTest();
            }
}