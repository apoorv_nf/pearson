/*=============================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER        DESCRIPTION                               
 |  ====            =========        =========== 
 |  7/5/2015        Sahir Ali        This Batch Class is used to put integration requests 
 /---------------------------------------------------------------------------------------------- */
 

global class PS_IntegrationRequestSendAtDateBatch implements Database.Batchable<sObject> ,Database.Stateful {
    
    public string batchQuery;
    global Id jId; //jobId to query records
    
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(batchQuery);
	}    
    
    global PS_IntegrationRequestSendAtDateBatch(Id jobid)
    {
        jId = jobId;
        
        batchQuery = 'Select Id, status__c from Integration_Request__c where '+
            		 'Status__c =  \'Send At DateTime\' AND Batch_Job_Id__c =\''+ jobid + '\''; 
    }
    global void execute(Database.BatchableContext BC, List<Integration_Request__c> scope){
        
        for(Integration_Request__c intItm : scope){
	    	intItm.status__c = 'Ready For Submission';
	    }
        
        update scope;
    }
    
    global void finish(Database.BatchableContext BC){
    }

}