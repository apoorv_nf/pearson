/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 29/05/2018
 */
global with sharing class EinsteinBotAccessCode {

	@InvocableMethod(label='Einstein Bot - Check Access Code')
	global static List<AccessCodeResponse> accessCodeExpired(List<String> accessCodes) {
		List<AccessCodeResponse> accessCodeResponse = new List<AccessCodeResponse>();

		for (String accessCode : accessCodes) {
			AccessCodeResponse response = new AccessCodeResponse();
			response.success = false;
			response.timedOut = false;
			try {
				SmsApi.AccessCodeDetails details = new SmsApiService(8000, false).getAccessCode(accessCode);
				response.success = true;
				response.valid = details.status == 'Found' ? true : false;
				response.redeemed = details.cashedIn == 'Y' ? true : false;
				response.expired = details.expirationDate < Datetime.now() ? true : false;
				response.replaced = Date.valueOf(details.expirationDate) == Date.newInstance(2005, 1, 5) ? true : false;
				response.sessionExpired = details.valid == 'false' && details.reason.contains('SSO Session') ? true : false;

				if(response.sessionExpired && [SELECT Id FROM AsyncApexJob WHERE JobType = 'Future' AND MethodName = 'resetAccessToken' AND Status NOT IN ('Completed','Failed')].isEmpty()) {
					resetAccessToken(accessCode);
				}
			} catch(CalloutException ex) {
				//Fail gracefully if a timeout of 8000 is reached
				response.timedOut = ex.getMessage() == 'Read timed out' ? true : false;
			} catch (Exception ex) {}

			accessCodeResponse.add(response);
		}

		return accessCodeResponse;
	}

	@future(callout=true)
	private static void resetAccessToken(String accessCode) {
		new SmsApiService().getAccessCode(accessCode);
	}

    global class AccessCodeResponse
    {
        @InvocableVariable
        global Boolean success;

        @InvocableVariable
        global Boolean redeemed;

        @InvocableVariable
        global Boolean expired;

        @InvocableVariable
        global Boolean valid;

        @InvocableVariable
        global Boolean replaced;

		@InvocableVariable
		global Boolean timedOut;

		@InvocableVariable
		global Boolean sessionExpired;
	}
}