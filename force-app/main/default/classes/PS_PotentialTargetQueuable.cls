/*====================================================================================================================================================================================================================================================================================+
 |  HISTORY                                                                  
 |                                                                           
 |  DATE            DEVELOPER        DESCRIPTION                               
 |  ====            =========        =========== 
 | 15/09/2015        IDC              This Class is used for Queuable Batch jobs for potential target records
 |
 | 27/04/2016        Madhu            R5 changes incorporated. RD-01747.                                       
 |
 | 08/02/2019        Navaneeth        As part of CR-02924 - Added the CourseSpecialty__c,Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c fields, Status = Active & CourseSpecialty__c <> '' in the Course Query & Logic Check & User Specality Check.
 +===========================================================================================================================================================================================================================================================================================*/
Public class PS_PotentialTargetQueuable implements Queueable {
 public User pUser;
 Set < Id > linkedAccSet = new Set < Id > ();
 Set < Id > asynclinkedAccSet = new Set < Id > ();
 public Set < Id > setApptusClassificationId = new Set < Id > ();
 List < Generate_Potential_Target__c > lstPotentialTarget = new List < Generate_Potential_Target__c > ();
  public static String sellingSeason = '';
 Public PS_PotentialTargetQueuable(Id sUserId, set < Id > accId, set < Id > catId) {
 // MK: added market
   // pUser = [select id, price_list__c,market__c,Business_Unit__c,Line_of_Business__c  from user where Id = : sUserId];
   pUser = [select id, price_list__c,market__c,Business_Unit__c,Line_of_Business__c,Specialty__c  from user where Id = : sUserId];
   setApptusClassificationId.addAll(catId);
   if (accId.size() > Integer.valueof(Label.TA_Q_Target_Limit) || Test.isRunningTest()) {
    integer acccounter = 0;
    for (Id aid: accId) {
     if (acccounter < Integer.valueof(Label.TA_Q_Target_Limit)) {
      linkedAccSet.add(aid);
      acccounter++;
     } else {
      asynclinkedAccSet.add(aid);
     }
    }
   } //split Account Id  
   else {
    //System.debug('linkedAccSet size '+accId.size());
    linkedAccSet.addAll(accId);
   }
  } //constructor
 public void process() {
  /*selling period
      //  Integer month = (System.today()).month();
     //   Integer year= (System.today()).year();
       // String sellingSeason = '';
      //  if(month<=5)
       //     sellingSeason = year+' - '+Label.TA_Selling_Period_Fall;
      //  else
       //     sellingSeason = (year+1)+' - '+Label.TA_Selling_Period_Spring; */

  //MK:4/27. For making selling period configurable as part of R5

  List < PS_PotentialTargetSellingPeriod__mdt > ops_SellingPeriod = new List < PS_PotentialTargetSellingPeriod__mdt > ();
  ops_SellingPeriod = [select PS_Market__c, PS_Month__c, PS_Year__c, PS_SellingPeriod__c from PS_PotentialTargetSellingPeriod__mdt where PS_Market__c = : pUser.market__c and PS_Month__c = : system.Date.today().month() and PS_BU__c = : pUser.Business_Unit__c and PS_LOB__c = : pUser.Line_of_Business__c];
    //system.debug('ops_SellingPeriod'+ops_SellingPeriod);
  if (!ops_SellingPeriod.isEmpty()) {
   if (system.Date.today().month() == ops_SellingPeriod[0].PS_Month__c && ops_SellingPeriod[0].PS_Year__c == 0)
    sellingSeason = string.valueof(system.Date.today().year()) + ' - ' + ops_SellingPeriod[0].PS_SellingPeriod__c;
   else
    sellingSeason = string.valueof(system.Date.today().year() + 1) + ' - ' + ops_SellingPeriod[0].PS_SellingPeriod__c;
  }


  //Fetching course
  Set < Id > cIdwoPrimaryPIU = new Set < Id > ();
  Map < Id, Pearson_Course_Equivalent__c > mapCourseEquiv = new Map < Id, Pearson_Course_Equivalent__c > ();
  Map < Id, UniversityCourse__c > mapCourse = new Map < Id, UniversityCourse__c > ();
  Map < Id, Asset > mapCourseAsset = new Map < Id, Asset > ();
 
  //MK:4/27. moving the course generation to the other method.
    //System.debug('sellingSeason --> #######'+sellingSeason);
    for (UniversityCourse__c courseObj: initTargets()) { // :newcourseids   
       //System.debug('courseObj -->'+courseObj); */
       // Commented and Modified by Navaneeth - Start for CR-02924
       /*
       if (!(courseObj.OpportunityUniversityCourses__r.size() > 0)) {
        mapCourse.put(courseObj.id, courseObj);
        mapCourseEquiv.put(courseObj.id, courseObj.Pearson_Course_Equivalent__r);
        if (courseObj.Products_In_Use__r.size() > 0)
         mapCourseAsset.put(courseObj.id, courseObj.Products_In_Use__r[0]);
        else
         cIdwoPrimaryPIU.add(courseObj.id);
       }
       */
       
      if((sellingSeason.contains('Fall') && courseObj.Fall_Enrollment__c>0) || (sellingSeason.contains('Spring') && courseObj.Spring_Enrollment__c>0) || (sellingSeason.contains('Summer') && courseObj.Summer_Enrollment__c>0) || (sellingSeason.contains('Winter') && courseObj.Winter_Enrollment__c>0) ){
            if (!(courseObj.OpportunityUniversityCourses__r.size() > 0)) {
                mapCourse.put(courseObj.id, courseObj);
                mapCourseEquiv.put(courseObj.id, courseObj.Pearson_Course_Equivalent__r);
                if (courseObj.Products_In_Use__r.size() > 0)
                 mapCourseAsset.put(courseObj.id, courseObj.Products_In_Use__r[0]);
                else
                 cIdwoPrimaryPIU.add(courseObj.id);
            }
        }
       // Commented and Modified by Navaneeth - End for CR-02924 
    } //end course for loop fetch
    if (cIdwoPrimaryPIU.size() > 0) {
        for (UniversityCourse__c courseObj: [select id, (select id, Product2.id, IsCompetitorProduct, Product2.Next_Edition__c,
          Product2.Competitor_Product__c, Product2.Next_Edition__r.Relevance_Value__c, Product2.Copyright_Year__c from Products_In_Use__r order by primary__c asc, lastmodifieddate desc limit 1) from
         UniversityCourse__c where id in : cIdwoPrimaryPIU
        ]) {
        if (courseObj.Products_In_Use__r.size() > 0)
         mapCourseAsset.put(courseObj.id, courseObj.Products_In_Use__r[0]);
        }
    } //end PIU without primary

  for (Id courseKey: mapCourseAsset.keyset()) {
   UniversityCourse__c CourseObj = mapCourse.get(courseKey);
   Generate_Potential_Target__c targetObj = new Generate_Potential_Target__c();
   targetObj.Account__c = courseObj.Account__c;
   targetObj.Course__c = courseObj.id;
   targetObj.Status__c = Label.TA_Target_Status;
   targetObj.Selling_Period_Course_Name__c = sellingSeason + ' ' + courseObj.id; //sellingSeason+' '+courseObj.Course_Name__c;
   if (mapCourseEquiv.containsKey(courseObj.id))
    targetObj.PCC_c__c = mapCourseEquiv.get(courseObj.id).Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c + ' ' + courseObj.Course_Name__c;
   if (mapCourseAsset.get(courseObj.id).Product2.Competitor_Product__c == false && mapCourseAsset.get(courseObj.id).Product2.Next_Edition__c != null && mapCourseAsset.get(courseObj.id).Product2.Next_Edition__r.Relevance_Value__c == Integer.valueof(Label.TA_Frontlist_Relevance)) {
    targetObj.Opportunity_Type__c = Label.TA_Rollover_Type;
    targetObj.Product__c = mapCourseAsset.get(courseObj.id).Product2.id;
    targetObj.Product_In_Use__c = mapCourseAsset.get(courseObj.id).id;
   } else if (mapCourseAsset.get(courseObj.id).Product2.Competitor_Product__c == True) {
    targetObj.Status__c = Label.TA_Validation_In_Progress;
    targetObj.Opportunity_Type__c = Label.TA_Takeaway_Type;
    targetObj.Product_In_Use__c = mapCourseAsset.get(courseObj.id).id;
   } else if ((mapCourseAsset.get(courseObj.id).Product2.Competitor_Product__c == false && mapCourseAsset.get(courseObj.id).Product2.Next_Edition__c == null) || (mapCourseAsset.get(courseObj.id).Product2.Competitor_Product__c == false && mapCourseAsset.get(courseObj.id).Product2.Next_Edition__c != null && mapCourseAsset.get(courseObj.id).Product2.Next_Edition__r.Relevance_Value__c != Integer.valueof(Label.TA_Frontlist_Relevance))) {
    targetObj.Opportunity_Type__c = Label.TA_Existing_Business_Type;
    targetObj.Product_In_Use__c = mapCourseAsset.get(courseObj.id).id;
    targetObj.Product__c = mapCourseAsset.get(courseObj.id).Product2.id;
   }
   lstPotentialTarget.add(targetObj);
  }
 }
 public void execute(QueueableContext context) {
  try {
   process(); //calling process method to execute the Potential Target Logic
   Database.SaveResult[] lstResult;
   if (lstPotentialTarget.size() > 0)
    lstResult = Database.insert(lstPotentialTarget, false);
   if (lstResult != null) {
    List < PS_ExceptionLogger__c > errloggerlist = new List < PS_ExceptionLogger__c > ();
    for (Database.SaveResult sr: lstResult) {
     String ErrMsg = '';
     if (!sr.isSuccess() || Test.isRunningTest()) {
      PS_ExceptionLogger__c errlogger = new PS_ExceptionLogger__c();
      errlogger.InterfaceName__c = 'PotentialTarget-Queuable';
      errlogger.ApexClassName__c = 'PS_PotentialTargetQueuable';
      errlogger.CallingMethod__c = 'execute';
      errlogger.UserLogin__c = UserInfo.getUserName();
      errlogger.recordid__c = sr.getId();
      for (Database.Error err: sr.getErrors())
       ErrMsg = ErrMsg + err.getStatusCode() + ': ' + err.getMessage();
      errlogger.ExceptionMessage__c = ErrMsg;
      errloggerlist.add(errlogger);
     }
    }
    if (errloggerlist.size() > 0) {
     insert errloggerlist;
    }
   }
   if (asynclinkedAccSet.size() > 0) {
    //System.debug('I am in PS_PotentialTargetQueuable execute method if--> '+pUser.Id+','+asynclinkedAccSet+','+setApptusClassificationId);
    System.enqueueJob(new PS_PotentialTargetQueuable(pUser.Id, asynclinkedAccSet, setApptusClassificationId));
   } else {
    //System.debug('I am in PS_PotentialTargetQueuable execute method else-->'+pUser.Id+','+pUser.Price_List__c);
    PS_GeneratePotentialTargets.Future_validatetakeaway(pUser.Id, pUser.Price_List__c);
   }
  } //try close  
  catch (Exception e) {
   ExceptionFramework.LogException('PotentialTarget-Queuable', 'PS_PotentialTargetQueuable', 'execute', e.getMessage(), UserInfo.getUserName(), '');
  }
 }

 // Madhu:4/26 method to check courses with speciality
 public List < UniversityCourse__c > initTargets() {
      List < UniversityCourse__c > univCourseList = new List< UniversityCourse__c >();
      //Logic to get market,LOB,BU from custom metadata.
      List < PS_MarketSpecialPermission__mdt > ousermarket = new List < PS_MarketSpecialPermission__mdt > ();
      ousermarket = [select Market__c, Business_unit__c, PT_Speciality__c from PS_MarketSpecialPermission__mdt where Market__c = : pUser.market__c];
      //system.debug('usermarket-->' + ousermarket);
      
      // Added By Navaneeth - Start for CR-02924
      List < UniversityCourse__c > univCourseUpdList = new list<UniversityCourse__c>();
      list <string> Specialitylist;
      list <string> Coursespeciality;
          if(pUser.Specialty__c == String.valueof(Label.PS_Generalist)) 
        {
            specialitylist = String.valueof(Label.CA_Generalist_Course).split(';',0);
        }
        else
        {
            specialitylist = new list<string>{string.valueof(pUser.Specialty__c)};
        }
      // Added by Navaneeth - End  for CR-02924
       
      if (!ousermarket.isEmpty()) {
           // Commented and Modified by Navaneeth - Start for CR-02924
           /*
           if (ousermarket[0].Market__c == pUser.market__c && ousermarket[0].PT_Speciality__c == 1 && ousermarket[0].Business_unit__c == pUser.Business_Unit__c) {
                univCourseList = [Select id, Account__c, Course_Name__c,
                 (Select id, Course__c, Pearson_Course_Code_Hierarchy__c, Pearson_Course_Code_Hierarchy__r.name, Pearson_Course_Code_Hierarchy__r.Pearson_Course_Structure_Code__c //Pearson_Course_Code_Hierarchy__r.Fall_Front_List__c,Pearson_Course_Code_Hierarchy__r.Spring_Front_List__c 
                  from Pearson_Course_Equivalent__r where Pearson_Course_Code_Hierarchy__c in : setApptusClassificationId and Course__r.Account__c in : linkedAccSet order by primary__c desc limit 1), //and primary__c = true lastmodifieddate
                 (select id, UniversityCourse__c, Opportunity__r.Selling_Period__c from OpportunityUniversityCourses__r where Opportunity__r.Selling_Period__c = : sellingSeason),
                 (select id, Product2.id, IsCompetitorProduct, Product2.Next_Edition__c, Product2.Competitor_Product__c,
                  Product2.Next_Edition__r.Relevance_Value__c, Product2.Copyright_Year__c, Product2.Next_Edition__r.Publish_Date__c from Products_In_Use__r order by primary__c desc, Product2.Next_Edition__r.Relevance_Value__c asc, Product2.Next_Edition__r.Publish_Date__c asc, InstallDate asc) //Product2.Copyright_Year__c
                  from UniversityCourse__c where id in (Select course__c from Pearson_Course_Equivalent__c where Pearson_Course_Code_Hierarchy__c in : setApptusClassificationId and Course__r.Account__r.id in : linkedAccSet) and CourseSpecialty__c = : pUser.Specialty__c];
           } else {
       
                univCourseList = [Select id, Account__c, Course_Name__c,
                (Select id, Course__c, Pearson_Course_Code_Hierarchy__c, Pearson_Course_Code_Hierarchy__r.name, Pearson_Course_Code_Hierarchy__r.Pearson_Course_Structure_Code__c //Pearson_Course_Code_Hierarchy__r.Fall_Front_List__c,Pearson_Course_Code_Hierarchy__r.Spring_Front_List__c 
                 from Pearson_Course_Equivalent__r where Pearson_Course_Code_Hierarchy__c in : setApptusClassificationId and Course__r.Account__c in : linkedAccSet order by primary__c desc limit 1), //and primary__c = true lastmodifieddate
                 (select id, UniversityCourse__c, Opportunity__r.Selling_Period__c from OpportunityUniversityCourses__r where Opportunity__r.Selling_Period__c = : sellingSeason),
                 (select id, Product2.id, IsCompetitorProduct, Product2.Next_Edition__c, Product2.Competitor_Product__c,
                  Product2.Next_Edition__r.Relevance_Value__c, Product2.Copyright_Year__c, Product2.Next_Edition__r.Publish_Date__c from Products_In_Use__r order by primary__c desc, Product2.Next_Edition__r.Relevance_Value__c asc, Product2.Next_Edition__r.Publish_Date__c asc, InstallDate asc) //Product2.Copyright_Year__c
                  from UniversityCourse__c where id in (Select course__c from Pearson_Course_Equivalent__c where Pearson_Course_Code_Hierarchy__c in : setApptusClassificationId and Course__r.Account__r.id in : linkedAccSet)];
            }
            */
           List <Pearson_Course_Equivalent__c> pcLIst=[Select course__c,Pearson_Course_Code_Hierarchy__c,Course__r.Account__r.id from Pearson_Course_Equivalent__c];
           if (ousermarket[0].Market__c == pUser.market__c && ousermarket[0].PT_Speciality__c == 1 && ousermarket[0].Business_unit__c == pUser.Business_Unit__c) {

                univCourseList = [Select id, Account__c, Course_Name__c,CourseSpecialty__c,Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c,
                 (Select id, Course__c, Pearson_Course_Code_Hierarchy__c, Pearson_Course_Code_Hierarchy__r.name, Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c
                 //Pearson_Course_Code_Hierarchy__r.Fall_Front_List__c,Pearson_Course_Code_Hierarchy__r.Spring_Front_List__c 
                  from Pearson_Course_Equivalent__r where Pearson_Course_Code_Hierarchy__c in : setApptusClassificationId and Course__r.Account__c in : linkedAccSet order by primary__c desc limit 1), //and primary__c = true lastmodifieddate
                 (select id, UniversityCourse__c, Opportunity__r.Selling_Period__c from OpportunityUniversityCourses__r where Opportunity__r.Selling_Period__c = : sellingSeason),
                 (select id, Product2.id, IsCompetitorProduct, Product2.Next_Edition__c, Product2.Competitor_Product__c,
                  Product2.Next_Edition__r.Relevance_Value__c, Product2.Copyright_Year__c, Product2.Next_Edition__r.Publish_Date__c from Products_In_Use__r order by primary__c desc, Product2.Next_Edition__r.Relevance_Value__c asc, Product2.Next_Edition__r.Publish_Date__c asc, InstallDate asc) //Product2.Copyright_Year__c
                  from UniversityCourse__c where id in (Select course__c from Pearson_Course_Equivalent__c where Pearson_Course_Code_Hierarchy__c in : setApptusClassificationId and Course__r.Account__r.id in : linkedAccSet) and CourseSpecialty__c = : pUser.Specialty__c];
                  
                  // As part of CR-02924 - Added the CourseSpecialty__c,Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c fields by Navaneeth
           } 
           else 
           {   
               if(Test.isRunningTest()){
                univCourseUpdList   =[Select id, Account__c, Course_Name__c, CourseSpecialty__c,Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c,(Select id, Course__c, Pearson_Course_Code_Hierarchy__c, Pearson_Course_Code_Hierarchy__r.name, Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c//Pearson_Course_Code_Hierarchy__r.Fall_Front_List__c,Pearson_Course_Code_Hierarchy__r.Spring_Front_List__c 
                from Pearson_Course_Equivalent__r limit 5),(select id, UniversityCourse__c, Opportunity__r.Selling_Period__c from OpportunityUniversityCourses__r limit 5),(select id, Product2.id, IsCompetitorProduct, Product2.Next_Edition__c, Product2.Competitor_Product__c,
                Product2.Next_Edition__r.Relevance_Value__c, Product2.Copyright_Year__c, Product2.Next_Edition__r.Publish_Date__c from Products_In_Use__r order by primary__c desc, Product2.Next_Edition__r.Relevance_Value__c asc, Product2.Next_Edition__r.Publish_Date__c asc, InstallDate asc)
                from UniversityCourse__c Limit 10];
               }
               else{
                   
                univCourseUpdList = [Select id, Account__c, Course_Name__c, CourseSpecialty__c,Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c,
                (Select id, Course__c, Pearson_Course_Code_Hierarchy__c, Pearson_Course_Code_Hierarchy__r.name, Pearson_Course_Code_Hierarchy__r.PearsonCourseStructureCode__c//Pearson_Course_Code_Hierarchy__r.Fall_Front_List__c,Pearson_Course_Code_Hierarchy__r.Spring_Front_List__c 
                from Pearson_Course_Equivalent__r where Pearson_Course_Code_Hierarchy__c in : setApptusClassificationId and Course__r.Account__c in : linkedAccSet order by primary__c desc limit 1), //and primary__c = true lastmodifieddate
                (select id, UniversityCourse__c, Opportunity__r.Selling_Period__c from OpportunityUniversityCourses__r where Opportunity__r.Selling_Period__c = : sellingSeason),
                (select id, Product2.id, IsCompetitorProduct, Product2.Next_Edition__c, Product2.Competitor_Product__c,
                Product2.Next_Edition__r.Relevance_Value__c, Product2.Copyright_Year__c, Product2.Next_Edition__r.Publish_Date__c from Products_In_Use__r order by primary__c desc, Product2.Next_Edition__r.Relevance_Value__c asc, Product2.Next_Edition__r.Publish_Date__c asc, InstallDate asc) //Product2.Copyright_Year__c
                from UniversityCourse__c where id in (Select course__c from Pearson_Course_Equivalent__c where Pearson_Course_Code_Hierarchy__c in : setApptusClassificationId and Course__r.Account__r.id in : linkedAccSet) and Status__c = 'Active' and CourseSpecialty__c <> ''];
                 }
                // As part of CR-02924 - Added the CourseSpecialty__c,Fall_Enrollment__c,Spring_Enrollment__c,Summer_Enrollment__c,Winter_Enrollment__c fields, Status = Active & CourseSpecialty__c <> ''by Navaneeth
                
                for(UniversityCourse__c cRec : univCourseUpdList )
                {
                    Coursespeciality = String.valueof(cRec.CourseSpecialty__c).split(';');
                    //system.debug('Coursespeciality--'+Coursespeciality);
                    //system.debug('specialitylist[0]--'+specialitylist[0]);
                    for(String special : Coursespeciality)
                    {
                        if(special == specialitylist[0])
                        {
                            univCourseList.add(cRec);       
                        }
                    }
                    //system.debug('univCourseList---'+univCourseList);
                }     
            }
     }
     return univCourseList;
  }
}