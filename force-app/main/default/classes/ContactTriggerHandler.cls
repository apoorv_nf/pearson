/************************************************************************************************************
* Apex Class Name   : ContactTriggerHandler.cls
* Version           : 1.0 
* Created Date      : 10 MARCH 2014
* Function          : Handler class for Contact Object Trigger
* Modification Log  :
* Developer                   Date                   Description
* -----------------------------------------------------------------------------------------------------------
* Mitch Hunt                  10/03/2014              Created Default Handler Class Template
*                             23/03/2015              Update to include reference to the B2CAccountSync apex class
* cp                          22/10/2015              called class apttustemplates -VCO-0235,Removed the recursive handler controlling Ps_PreventChangingOwner class call .
* Karthik.A.S                 21/01/2016              called class PS_contactidvalidation To check valid identification number on contact
* rashmi Prasad                16/2/2016               R4 updated access modifier to without
* sasiraja            11/08/2017         To seperate Active and Inactive contacts CR-01247

************************************************************************************************************/
public without sharing class ContactTriggerHandler
{   
  // Init Contact Utils
  private ContactUtils utils = new ContactUtils();
  private B2CAccountSync b2cSync = new B2CAccountSync();    
    
  // EXECUTE AFTER INSERT LOGIC
  public void onAfterInsert(Contact[] lstNewContacts)
  {
    utils.handleAfterInsert(lstNewContacts);   
  }
  
  // EXECUTE BEFORE INSERT LOGIC
  public void onBeforeInsert(Contact[] lstNewContacts)
  {
    utils.handleBeforeInsert(lstNewContacts);  
    PS_HashingUtility.extractEmailHash(lstNewContacts,Label.PS_SObjectContact); 
    PS_contactidvalidation.contactidvalidation((list<contact>)Trigger.new);     
    
    for(Contact individualcontact : lstNewContacts){ //Added by Rob for CR-01785
         if(individualcontact.market__c=='ZA'){
              individualcontact.National_Identity_Number_Unencrypted__c=individualcontact.National_Identity_Number__c;
              individualcontact.Passport_Number_Unencrypted__c=individualcontact.Passport_Number__c;
         }
    }   //End CR-01785
  }
    
    // BEFORE UPDATE LOGIC
    //
    public void OnBeforeUpdate(Contact[] newContacts, map<ID, Contact> oldMap)
    {
    // AccountReadOnly.BeforeInsert(lstUpdatedContacts);
    PS_contactidvalidation.contactidvalidation((list<contact>)Trigger.new);
    for(Contact individualcontact : newContacts){ //Added by Rob for CR-01785
         if(individualcontact.market__c=='ZA'){
              individualcontact.National_Identity_Number_Unencrypted__c=individualcontact.National_Identity_Number__c;
              individualcontact.Passport_Number_Unencrypted__c=individualcontact.Passport_Number__c;
         }
    } //End CR-01785
    Ps_PreventChangingOwner.PreventChangingContactOwner(newContacts,oldMap);
    //Call the hashing functionality if email is changed 
    List<Contact> listWithEmailChangedContacts = new List<Contact>();
    for(Contact newContact : newContacts)
    {
        if(newContact.Email != null)
        {
            if(newContact.Email != oldMap.get(newContact.Id).Email)
            {
                listWithEmailChangedContacts.add(newContact);
            }
        }
    }
    if(listWithEmailChangedContacts.size() > 0)
    {
        PS_HashingUtility.extractEmailHash(listWithEmailChangedContacts,Label.PS_SObjectContact);  
    }     
    }
    // Logic to execute on Before insert and before update 
    public void OnBeforeContactInsertUpdate(Contact[] newContact){
        for(Contact con : newContact){
            if(con.Contact_Status__c != null){
         //       if(con.Contact_Status__c != oldMap.get(con.Id).Contact_Status__c){
                    if(con.Contact_Status__c == TRUE){
                        con.Active_Contact__c = con.AccountId;
                        con.Inactive_Contact__c = null;
                    }
                    else if(con.Contact_Status__c == FALSE){
                        con.Inactive_Contact__c = con.AccountId;
                        con.Active_Contact__c = null;
                    }
          //      }
            }
        }
    }
    
  // AFTER UPDATE LOGIC
  public void onAfterUpdate(Contact[] lstOldContacts, Contact[] lstUpdatedContacts, map<ID, 
                        Contact> NewMapIDContact, map<ID, Contact> oldMapIDContact)
  {   
        List<ID> contactIdList = new List<Id>();
        for(Contact contObj: lstUpdatedContacts)
            contactIdList.add(contObj.Id);
        //Added by Mani as part of ZA Non-Apttus Project
        /*List<Apttus_Proposal__Proposal__c> UpdateQuoteProposal = [select id,Apttus_Proposal__Primary_Contact__c,Opportunity_International_Student__c,Apptus_Id_Passport__c,Apttus_Proposal__Opportunity__r.IsClosed
         from Apttus_Proposal__Proposal__c where Apttus_Proposal__Primary_Contact__c IN : ContactIdList and Apttus_Proposal__Opportunity__c !=null];
        
        if(UpdateQuoteProposal.size() > 0)        
          PS_ApttusTemplates.ApttusTemplatesContacts(UpdateQuoteProposal,NewMapIDContact);*/
    if(checkRecurssion.runOnce())
    {
      b2cSync.B2CAccountSyncWhenContactChange(lstUpdatedContacts, NewMapIDContact, oldMapIDContact);
      PrimaryAccountUtilities.updateRoleInformationOnAccountContact(lstOldContacts, lstUpdatedContacts, 
                                                                NewMapIDContact, oldMapIDContact); 
       PrimaryAccountUtilities.checkAccountContactExists(lstUpdatedContacts);
    }
  }
    
  // BEFORE DELETE LOGIC
  public void onBeforeDelete(Contact[] lstContactsToDelete, map<ID, Contact> mapIDContact)
  {
    utils.preventDelete(lstContactsToDelete);
  }
}