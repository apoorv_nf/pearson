/************************************************************************************************************
* Apex Interface Name : CommunityProxyShellApexController
* Version             : 1 
* Created Date        : 7/8/2016 
* Modification Log    : Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 30) 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Christwin                  7/8/2016 
-------------------------------------------------------------------------------------------------------------
************************************************************************************************************/
@isTest
public class CommunityProxyShellApexControllerTest {
    
    static testMethod void getUserPermissions () {

        String communityUserProfileName = 'Pearson Self-Service User';
        boolean RolePresentLoc = true;
        boolean ProfilePresentLoc = true;
        string permissionSetsLoc = 'TestPermissionSet';
        List<User> userList = new List<User>();
        user commUser = new User();

        User usr;
        UserRole tempUserRole;
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        System.debug('userroleid' + tempUserRole.id);
        insert usr;
        
        System.runAs(usr){
            Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=  usr.id,Disable_Triggers__c=true);
        	insert byp; 
            Test.StartTest();
            userList  = TestDataFactory.createUserWithContact([select Id from Profile where Name =: communityUserProfileName].Id,1);
            insert userList;       
            if (userList.size() > 0) {
                commUser = userList[0];    
            }
            
            PermissionSet userPermSet = new PermissionSet ();
            userPermSet.name = 'TestPermissionSet';
            userPermSet.Label = 'TestPermissionSet';
            insert userPermSet;
            
            System.runAs(commUser){
                Bypass_Settings__c bypsettng = new Bypass_Settings__c(SetupOwnerId=  commUser.id,Disable_Triggers__c=true);
        		insert bypsettng;
                string returnVal = CommunityProxyShellApexController.getUserPermissions(RolePresentLoc,ProfilePresentLoc,permissionSetsLoc);
            }
            
            PermissionSetAssignment PermSetAssign = new PermissionSetAssignment();
            PermSetAssign.AssigneeId = commUser.Id;
            PermSetAssign.PermissionSetId = userPermSet.Id;
            insert PermSetAssign;
            
            System.runAs(commUser){
                string returnVal = CommunityProxyShellApexController.getUserPermissions(RolePresentLoc,ProfilePresentLoc,permissionSetsLoc);
            }
            
            Test.StopTest();
        }
    }    

}