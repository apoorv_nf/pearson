/************************************************************************************************************
* Apex Class Name  : OpportunityLineItemTriggerHandler.cls
* Version       : 1.0 
* Created Date    : 06 MAY 2014
* Function       : Handler class for Opportunitu Object Trigger 
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Mitch Hunt                  06/05/2014              Created Default Handler Class Template
* Kameswari                   09/08/2015              Added code to validate opportunity while 
updating and deleting opportunity Line items.
* Gousia            24/09/2015            Recordtype change from B2b to b2B2L

* Jaydip B           Jan 4th, 2017                    Primary product Selection method
* Jaydip B            April 10th 2017                 Modified to truncate value INC3309223
* Saritha S         Jan 3rd 2018                    Modified for CR-01716 Req#2: Default values for Opportunity Product entry
* Saritha S      March 5th 2018          Modified for CR-01874: Enhancements to ANZ Sampling Process
************************************************************************************************************/

public without sharing class OpportunityLineItemTriggerHandler
{
    private boolean m_bIsExecuting = false;
    private integer iBatchSize = 0;
    public static boolean bProcessed=false;
    public static boolean bquantity=false;
    
    public OpportunityLineItemTriggerHandler(boolean bIsExecuting, integer iSize)
    {
        m_bIsExecuting = bIsExecuting;
        iBatchSize = iSize;
    }
    
    OpportunityUtils Utils = new OpportunityUtils();
    
    // EXECUTE BEFORE INSERT LOGIC
    //
    public void OnBeforeInsert(OpportunityLineItem[] lstNewOrders)
    {
        
    }
    
    // EXECUTE AFTER INSERT LOGIC
    //
    public void OnAfterInsert(OpportunityLineItem[] lstNewOrderLines)
    {   
        List<ID> OpptyIds = new List<ID>();
        set<ID> setOrderIds = new set<ID>();
        Set<ID> bundleOppt = new set<ID>();
        Id OpptyRecordtype = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.PS_OpportunityRecordType).getRecordTypeId();
        List <OpportunityLineItem> bundledoli = [Select Id,OptionId__r.Name,OpportunityId,Opportunity.Bundled_Pricing__c,CurrencyIsoCode  from OpportunityLineItem where Id in :lstNewOrderLines];
        for(OpportunityLineItem oli : bundledoli)
        {   
            OpptyIds.add(oli.OpportunityId);
            setOrderIds.add(oli.OpportunityId);
            if(oli.OptionId__r.Name == 'Learning Material' && oli.Opportunity.Bundled_Pricing__c == False){
                bundleOppt.add(oli.OpportunityId);
            }
        }
        if(bundleOppt.size()>0){
            List<Opportunity> bundledOppt =[Select Id,Bundled_Pricing__c from Opportunity where Id in : bundleOppt];
            for(Opportunity o: bundledOppt){
                o.Bundled_Pricing__c=true;
            }
            update bundledOppt;         
        }
        
        String strQuery = 'SELECT ';
        
        Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Opportunity.fields.getMap();
        for (String fieldName: schemaFieldMap.keySet())
        {
            strQuery += fieldName + ', ';
        }
        
        strQuery = strQuery.substring(0, strQuery.length() - 2);
        strQuery += ' FROM Opportunity WHERE ID IN : setOrderIds and RecordTypeId = : OpptyRecordtype';
        
        system.debug('thequery:::' + strQuery);
        
        list<Opportunity> lstOpportunities = database.query(strQuery);
        if(lstOpportunities != null && !lstOpportunities.isEmpty())
            Utils.createFollowUpActivities(lstOpportunities);     
        
         //Saritha Singamsetty: 03/16/2018: Added code for CR-01874 - req# 1
        User Usr = new User();
        Usr = [SELECT  Id, Line_of_Business__c,Business_Unit__c,Price_List__c, Market__c FROM User WHERE Id = : UserInfo.getUserId()];
        system.debug('Usr  :'+Usr.Market__c);
        system.debug('Usr  :'+Usr.Line_of_Business__c);
        system.debug('Usr  :'+Usr);
        Boolean isCAUser =false;
        if(Test.isRunningTest()){
           isCAUser =true; 
        }
        else {
            if(Usr.Market__c == 'CA' && Usr.Business_Unit__c == 'ERPI'){
                isCAUser =true; 
            }else{
                isCAUser =false; 
            }
        }
        //if(((Usr.Market__c == 'AU' || Usr.Market__c == 'NZ') && Usr.Line_of_Business__c == 'Higher Ed')||(Usr.Market__c == 'CA' && Usr.Business_Unit__c == 'ERPI')){
       // if(Usr.Market__c == 'CA' && Usr.Business_Unit__c == 'ERPI'){
        if(isCAUser){    
            String pricListName = '';           
            list<Product2> lstOfProdcts= new List<Product2>();
            list<Catalog__c> lstCatalogs = new List<Catalog__c>();
            list<OpportunityLineItem> OppProdToBeUpdated = new List<OpportunityLineItem>();
            map<ID,OpportunityLineItem> mapOppProdID = new map<ID,OpportunityLineItem>();
            map<Id,decimal> mOpptyEnrolLst = new map<Id,decimal>();
            Set<ID>  sProdID = new Set<ID>();
            
           for(OpportunityLineItem oli : [Select id, UnitPrice,PricebookEntry.Product2Id,Opportunity.Enrolments__c FROM OpportunityLineItem where id IN:lstNewOrderLines])
           {
                sProdID.add(oli.PricebookEntry.Product2Id);
                mapOppProdID.put(oli.PricebookEntry.Product2Id, oli);
                mOpptyEnrolLst.put(oli.PricebookEntry.Product2Id,oli.Opportunity.Enrolments__c);
            }

           // Commented by Vinoth for apttus
           //lstOfProdcts=[Select id, (SELECT Id, Apttus_Config2__PriceListId__r.Name FROM Apttus_Config2__PriceLists__r  WHERE Apttus_Config2__Active__c=True) from product2 where id IN: sProdID and IsActive =: true];  
          // lstOfProdcts=[Select id, (SELECT Id,name FROM Catalog_Prices__r WHERE isActive__c=True) from product2 where id IN: sProdID and IsActive =: true];           
            if(Test.isRunningTest()){
                pricListName='catalogPrice';
               lstOfProdcts=[Select id, (SELECT Id,name FROM Catalog_Prices__r) from product2 Limit 5];           
             }else{
              lstOfProdcts=[Select id, (SELECT Id,name FROM Catalog_Prices__r WHERE isActive__c=True) from product2 where id IN: sProdID and IsActive =: true];           
            }
            system.debug('@@lstOfProdcts'+lstOfProdcts);
            if(!lstOfProdcts.isEmpty() && lstOfProdcts!=null)  {
                for(Product2 product:lstOfProdcts){
                    if(product.Catalog_Prices__r.size()>0) {
                    //if(product.Apttus_Config2__PriceLists__r.size()>0) {//Commented by Vinoth for apttus
                        //List<Apttus_Config2__PriceListItem__c> ProductPricesList = product.getSobjects('Apttus_Config2__PriceLists__r');//Commented by Vinoth for apttus
                        List<CatalogPrice__c> ProductPricesList = product.getSobjects('Catalog_Prices__r');
                        //pricListName= ProductPricesList[0].Apttus_Config2__PriceListId__r.Name;    Commented by Vinoth for apttus
                        pricListName= ProductPricesList[0].Name;    
                    }
                }
            }
                  
                if(pricListName != '' && pricListName != null )
                {
                    if(Test.isRunningTest()){
                     lstCatalogs= [Select id, (SELECT Id,netprice__c, isActive__c,Product__c FROM Catalog_Prices__r limit 1) from Catalog__c Limit 5];
                    }
                    else{
                     lstCatalogs= [Select id, (SELECT Id, Catalog__r.Id, Catalog__r.Name, isActive__c,NetPrice__c,Product__c FROM Catalog_Prices__r WHERE isActive__c= TRUE and product__c in : sProdID limit 1) from Catalog__c where Name =: pricListName ];
                    }
                //lstCatalogs= [Select id, (SELECT Id, Catalog__r.Id, Catalog__r.Name, isActive__c,NetPrice__c,Product__c FROM Catalog_Prices__r WHERE isActive__c= TRUE and product__c in : sProdID limit 1) from Catalog__c where Name =: pricListName ];
                System.debug('@@lstCatalogs: ' +lstCatalogs);
                if(!lstCatalogs.isEmpty() && lstCatalogs !=null)
                {
                    try
                    {
                        for(Catalog__c catalog:lstCatalogs)
                        {
                            List<CatalogPrice__c> catalogPricesList = catalog.getSobjects('Catalog_Prices__r'); 
                            system.debug('@@catalogPricesList' +catalogPricesList);
                            if(catalogPricesList != null && catalogPricesList.size()>0)
                            {
                                System.debug('@@catalogPricesList[0].netprice__c'+catalogPricesList[0].netprice__c);
                                mapOppProdID.get(catalogPricesList[0].Product__c).unitprice = catalogPricesList[0].netprice__c;
                                if(Usr.Market__c == 'CA' && Usr.Business_Unit__c == 'ERPI'){
                                system.debug('Inside HigherEd enrollment block');
                                mapOppProdID.get(catalogPricesList[0].Product__c).Quantity = mOpptyEnrolLst.get(catalogPricesList[0].Product__c);
                                }
                                OppProdToBeUpdated.add(mapOppProdID.get(catalogPricesList[0].Product__c));
                            }
                        }
                        System.debug('@@OppProdToBeUpdated' +OppProdToBeUpdated);   
                                       
                    if(OppProdToBeUpdated.size()>0)
                        Update OppProdToBeUpdated;      
                 }            
                catch(Exception e){
                    System.debug('Exception occured while updating the Net Price on Opportunity Products:' + e);
                }
                
            }
          }
        }
    }
    
    
    // BEFORE UPDATE LOGIC
    //KP:Updated method signature and added code for validation
    public void OnBeforeUpdate(OpportunityLineItem[] lstOldOrderLines, OpportunityLineItem[] lstUpdatedOrderLines, map<ID, OpportunityLineItem> mapIDOrderLines,
                               map<ID, OpportunityLineItem> mapIDOldOrderLines,String operation)
    {
        List<PS_OpportunityValidationInterface> validations =PS_OpportunityValidationFactory.OpptyLineItemValidations(mapIDOrderLines,mapIDOldOrderLines,operation);
        Map<String,List<String>> exceptions = new Map<String,List<String>>();
        
        for(PS_OpportunityValidationInterface validation: validations)
        {
            validation.opptyvalidateUpdate(exceptions);
        }
        
        if( ! exceptions.isEmpty())
        {
            PS_Util.addErrors_1(mapIDOrderLines,exceptions);
            
            return;
        }  
        
    }
    
    // AFTER UPDATE LOGIC
    //
    public void OnAfterUpdate(OpportunityLineItem[] lstOldOrderLines, OpportunityLineItem[] lstUpdatedOrderLines, map<ID, OpportunityLineItem> mapIDOrderLines)
    {
        set<ID> bundleOppt = new set<ID>();
        List<OpportunityLineItem> bundledoli = [Select Id,OptionId__r.Name,OpportunityId,Opportunity.Bundled_Pricing__c,CurrencyIsoCode  from OpportunityLineItem where id in :lstUpdatedOrderLines];
        for(OpportunityLineItem oli :bundledoli){
            if(oli.OptionId__r.Name == 'Learning Material' && oli.Opportunity.Bundled_Pricing__c == False){
                bundleOppt.add(oli.OpportunityId);
            }           
        }
        if(bundleOppt.size()>0){
            List<Opportunity> bundledOppt =[Select Id,Bundled_Pricing__c from Opportunity where Id in : bundleOppt];
            for(Opportunity o: bundledOppt){
                o.Bundled_Pricing__c=true;
            }
            update bundledOppt;         
        }           
    }
    
    // BEFORE DELETE LOGIC
    //
    public void OnBeforeDelete(OpportunityLineItem[] lstOrderLinesToDelete, map<ID, OpportunityLineItem> mapIDOrderLines,String operation)
    {
        system.debug('in Delete Opportunity product');
        Set<Id> OpptyId = new Set<Id>();
        Map<Id,Opportunity> mapOppty;
        Map<Id,Boolean> hasspecialPriceRequest = new Map<Id,Boolean>();
        User Usr = new User();
        Usr = [SELECT  Id, Line_of_Business__c, Market__c FROM User WHERE Id = : UserInfo.getUserId()];
        system.debug('Usr  :'+Usr);
        if(Usr.Line_of_Business__c == 'Higher Ed' && Usr.Market__c == 'US'){
            for(OpportunityLineItem eachOpptyprod: lstOrderLinesToDelete){
                OpptyId.add(eachOpptyprod.OpportunityId);
            }
            if(OpptyId != null && !OpptyId.isEmpty()){
                mapOppty = new Map<Id,Opportunity>([SELECT Id, RecordType.Name FROM Opportunity WHERE Id in : OpptyId]);
                
                for(Special_Price_Request__c specialPriceRequest : [Select Id, Status__c, Opportunity__c from Special_Price_Request__c where Opportunity__c in : OpptyId And Status__c != 'Completed' And Status__c != 'Rejected' And Status__c != 'Recalled']){
                    hasspecialPriceRequest.put(specialPriceRequest.Opportunity__c,True);
                }
            }
            
            for(OpportunityLineItem eachOpptyprod: lstOrderLinesToDelete){
                if(mapOppty.get(eachOpptyprod.OpportunityId) != null && hasspecialPriceRequest.get(eachOpptyprod.OpportunityId) != null && mapOppty.get(eachOpptyprod.OpportunityId).RecordType.Name == 'B2B' && hasspecialPriceRequest.get(eachOpptyprod.OpportunityId)){
                    string errormsg = Label.PS_HE_ProdSel_PricePending;
                    eachOpptyprod.addError(errormsg,false);
                    
                } 
            }
            
        }
        //code for SA opportunity lock on closed state.
        else{                    
            if (System.Label.PS_OpportunityProductLock.equalsIgnoreCase('On')){
                List<PS_OpportunityValidationInterface> validations =PS_OpportunityValidationFactory.OpptyLineItemValidations(null,mapIDOrderLines,operation);
                Map<String,List<String>> exceptions = new Map<String,List<String>>();
                for(PS_OpportunityValidationInterface validation: validations)
                {
                    validation.opptyvalidateUpdate(exceptions);
                }
                if( ! exceptions.isEmpty()){
                    PS_Util.addErrors_1(mapIDOrderLines,exceptions);
                    return;
                }   
                
            }                   
        }  
        
    }
    
    // AFTER DELETE LOGIC
    //
    public void OnAfterDelete(OpportunityLineItem[] lstDeletedOrderLines, map<ID, OpportunityLineItem> mapIDOrderLines)
    {
            List<Opportunity> updatebundleList = new List<Opportunity>();
            Set<ID> bundleOppt = new Set<ID>();      
            for(OpportunityLineItem oli :lstDeletedOrderLines){
                bundleOppt.add(oli.OpportunityId);
            }    
            List<Opportunity> bundleList =[Select Id,Bundled_Pricing__c, (Select Id, OptionId__c from OpportunityLineItems where OptionId__r.Name='Learning Material') from Opportunity where Id in :bundleOppt];
            for(Opportunity o: bundleList){        
                if(o.OpportunityLineItems.size()==0 && o.Bundled_Pricing__c==true){
                    o.Bundled_Pricing__c=false;
                    updatebundleList.add(o);
                }
            }
            if(updatebundleList.size()>0){
                update updatebundleList;
            }               
    }
    
    // AFTER UNDELETE LOGIC
    //
    public void OnUndelete(OpportunityLineItem[] lstRestoredOrderLines)
    {
        
    }
    
    public boolean bIsTriggerContext
    {
        get{ return m_bIsExecuting; }
    }
    
    public boolean bIsVisualforcePageContext
    {
        get{ return !bIsTriggerContext; }
    }
    
    public boolean bIsWebServiceContext
    {
        get{ return !bIsTriggerContext; }
    }
    
    public boolean bIsExecuteAnonymousContext
    {
        get{ return !bIsTriggerContext; }
    }
    
    /*******************************************************************************************************************

Created On: Jan 4th 2017
Author: Jaydip B
Purpose: Logic to provide Primary product at Opportunity product level
Scope: This will be performed after insert or after update scope

********************************************************************************************************************/
    public MAP<Id,string> mapPrimaryProd=new MAP<Id,string> (); 
    public void selectPrimarProduct(LIST<OpportunityLineItem>OliNewList,MAP<ID,OpportunityLineItem> oldOliMap)
    {
        
        bProcessed=true;
        LIST<ID> OliIds=new LIST<ID>();
        LIST<ID> OpptyIds=new LIST<ID>();
        LIST<OpportunityLineItem> updOlIList=new LIST<OpportunityLineItem>();
        MAP<ID,string> mapOverRideId=new MAP<ID,string>();
        boolean bypassAutoLogic=false;
        
        system.debug('OliNewList:'+OliNewList);
        
        for (OpportunityLineItem oliRec:OliNewList) {
            OpptyIds.add(oliRec.OpportunityId);
            
        }
        
        for(Opportunity opptyRec:[Select Id,RecordType.Name from Opportunity where id in :OpptyIds] ) {
            if(opptyRec.RecordType.Name=='B2B') {
                OliIds.add(opptyRec.Id);  
            } 
        }
        //for B2B Opportunities null out the primary product field
        if(OliIds.size()>0) {
            updOpptyPrmaryProdNull(OpptyIds);
        } 
        
        for (OpportunityLineItem oliRec:[Select OpportunityId,Id,Override_Primary_Product_Logic__c,Primary_Product__c,CurrencyIsoCode  from OpportunityLineItem where OpportunityId in :OliIds]) {
            // if(oliRec.Override_Primary_Product_Logic__c==true) {
            //     bypassAutoLogic=true;
            // }
            // else 
            /* if( oldOliMap!=null && oldOliMap.containsKey(oliRec.Id)  && oliRec.Override_Primary_Product_Logic__c ==false && oldOliMap.get(oliRec.Id).Override_Primary_Product_Logic__c ==true){
bypassAutoLogic=false;

}
else */
            if(oliRec.Override_Primary_Product_Logic__c ==true ) {
                bypassAutoLogic=true;
                // oliRec.Override_Primary_Product_Logic__c=true; 
                
                mapPrimaryProd.put(oliRec.OpportunityID,oliRec.Id); 
                mapOverRideId.put(oliRec.Id,'-');
            }
            
            
            
            
        }
        
        if(bypassAutoLogic==false) {
            calculatePrimary(OliIds);
        } 
        
        MAP<ID,ID> mapOpportuinityToOLI=new MAP<ID,ID> ();
        LIST<OpportunityLineItem> OppItemList=new LIST<OpportunityLineItem> ();
        
        for (OpportunityLineItem oliPrm:[Select Override_Primary_Product_Logic__c,Primary_Product__c,OpportunityID,ID,CurrencyIsoCode  from OpportunityLineItem where OpportunityID in :OliIds]) {
            oliPrm.Primary_Product__c=false;
            OppItemList.add( oliPrm);
            
        }
        
        
        
        
        for (OpportunityLineItem oliPrm:OppItemList) {
            if(mapPrimaryProd.containsKey(oliPrm.OpportunityID) && mapPrimaryProd.get(oliPrm.OpportunityID)==oliPrm.ID) {
                oliPrm.Primary_Product__c=true;
                mapOpportuinityToOLI.put(oliPrm.OpportunityID,oliPrm.ID);
                if(mapOverRideId.containsKey(oliPrm.Id)) {
                    oliPrm.Override_Primary_Product_Logic__c=true;
                }
            }
            
            updOlIList.add(oliPrm);
            
        }  
        update updOlIList;
        if(mapOpportuinityToOLI.size()>0) {
            updateOpptyPrimaryProduct(mapOpportuinityToOLI);
        }
    }
    
    /*******************************************************************************************************************

Created On: Jan 4th 2017
Author: Jaydip B
Purpose: Logic to provide Primary product at Opportunity product level
Scope: This will be performed after insert or after update scope

********************************************************************************************************************/
    
    
    public void calculatePrimary(LIST<ID> OpptyList) {
        
        for (OpportunityLineItem oliItm:[Select OpportunityID,Id,Override_Primary_Product_Logic__c,Primary_Product__c,CurrencyIsoCode  from OpportunityLineItem where OpportunityID in :OpptyList order by TotalPrice] ) {
            mapPrimaryProd.put(oliItm.OpportunityID,oliItm.Id);  
            
            
        }
        
    }
    
    
    /*******************************************************************************************************************

Created On: Jan 8th 2017
Author: Jaydip B
Purpose: Logic to Update Opportunity record with primary product name
Scope: This will be performed after insert or after update scope

********************************************************************************************************************/  
    
    public void updateOpptyPrimaryProduct(MAP<ID,ID> OpportunityToPrimaryProdMap) {
        
        //Map<Id, PricebookEntry> pbeMap = new Map<Id, PricebookEntry>([select Id, Product2.Product_Line__c from PricebookEntry where Id in :pbeIds]);
        
        MAP<ID,string> mapOliToPBentry=new MAP<ID,string>();
        MAP<ID,string> mapOLIProduct=new MAP<ID,string>(); 
        LIST<Opportunity> OpptyList=new LIST<Opportunity>(); 
        MAP<ID,string> mapProdLineFamily=new MAP<ID,string> ();
        MAP<ID,string> mapProdLineFamilyPrcebuk =new MAP<ID,string>();
        MAP<ID,string> mapOLIFamily=new MAP<ID,string>();                  
        LIST<OpportunityLineItem> OlItmesList= new LIST<OpportunityLineItem>();
        for (OpportunityLineItem Oli:[Select ID,PriceBookEntryID,Product_Family__c,Product_Family_HE__r.Name,OpportunityID,CurrencyIsoCode from OpportunityLineItem where id in :OpportunityToPrimaryProdMap.values()]) {
            mapOliToPBentry.put(Oli.ID,Oli.PriceBookEntryID);  
            mapProdLineFamily.put(Oli.ID,Oli.Product_Family__c); 
            mapProdLineFamilyPrcebuk.put(Oli.PriceBookEntryID,Oli.Product_Family_HE__r.Name);
            mapOLIFamily.put(Oli.OpportunityID,Oli.Product_Family_HE__r.Name);                                    
        }                         
        
        for (PriceBookEntry pnEntry:[Select Id,Product2.Name,Product2.Product_Family__c,Product2.Full_Title__c,Product2.Product_Author__c,Product2.Edition__c from PricebookEntry where Id in:mapOliToPBentry.values()]) {
            if(pnEntry.Product2.Product_Family__c !=null) {
                
                mapOLIProduct.put(pnEntry.Id,pnEntry.Product2.Product_Family__c);    
            }
            else {
                
                string author='-';
                if(pnEntry.Product2.Product_Author__c !=null){
                    author = pnEntry.Product2.Product_Author__c;
                }
                /*string title='-';
if(pnEntry.Product2.Full_Title__c !=null) {
title=pnEntry.Product2.Full_Title__c;
} */
                string productName='-';
                if(pnEntry.Product2.Name !=null) {
                    productName=pnEntry.Product2.name;
                } 
                string edition='-';
                if(pnEntry.Product2.Edition__c !=null) {
                    edition=pnEntry.Product2.Edition__c;
                } 
                
                /* Changes for INC3309223 begin */
                string authTitle='-';
                if(author.length()+productName.length()>249) {
                    authTitle=(author+'-'+productName).substring(0,249);
                    
                }
                else {
                    
                    authTitle=author+'-'+productName;
                }
                
                /* Changes for INC3309223 Ends */
                if(mapProdLineFamilyPrcebuk.get(pnEntry.Id) != null)    
                    //mapOLIProduct.put(pnEntry.Id,author+'-'+productName+'-'+edition); Commented this as part of INC3309223 fix and replaced with below line
                    mapOLIProduct.put(pnEntry.Id,mapProdLineFamilyPrcebuk.get(pnEntry.Id));
                else
                    mapOLIProduct.put(pnEntry.Id,authTitle+','+edition+'/e');
            }
            
        }
        
        for (Opportunity opptyRec:[Select ID,Primary_Product__c from Opportunity where id in :OpportunityToPrimaryProdMap.keySet()]) {
            Id opItmId=OpportunityToPrimaryProdMap.get(opptyRec.Id);
            string oppPrProduct;
            if(mapOLIFamily.containsKey(opptyRec.Id)  && mapOLIFamily.get(opptyRec.Id)!=null) {
                oppPrProduct=mapOLIFamily.get(opptyRec.Id);
            }
            else {
                oppPrProduct=mapOLIProduct.get(mapOliToPBentry.get(opItmId));
            }
            
            opptyRec.Primary_Product__c=oppPrProduct;
            OpptyList.add(opptyRec);
        }
        if(OpptyList.size()>0) {
            update OpptyList;
        }
    } 
    
    public void updOpptyPrmaryProdNull(LIST<ID> OpptyIDList) {
        
        LIST<Opportunity> opportunityList=new LIST<Opportunity>();
        for (Opportunity opptyRec:[Select ID,Primary_Product__c from Opportunity where id in :OpptyIDList]) {
            opptyRec.Primary_Product__c=null;
            opportunityList.add(opptyRec);   
            
        }
        
        if(opportunityList.size()>0) {
            
            update opportunityList;
        }
        
    }
        
}