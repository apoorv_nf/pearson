/************************************************************************************************************
* Apex Class Name   : PS_UniversityCourseOperations.cls
* Version           : 1.0 
* Created Date      : 12 Oct 2015 
* Function          : Operation class for Course Object Trigger
* Additional Info   : All the Event methods shall remain empty , Infuture required logic can be added to respective event methods
            
* Modification Log  :
* Developer                  Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Leonard Victor           22/10/2015               Operations Class for UniversityCourse
  Kyama                    17/02/2016               Update Opportunity Speciality field on course Update(RD-01686)
  Kyama                    12/04/2016               Added New Method to update Market on Course as per RD-01744.
  Karthik.A.S              04/05/2016               Update Opportunity while Speciality field not equal on course Speciality field(INC2524114 & INC2509045).
************************************************************************************************************/


public class PS_UniversityCourseOperations implements PS_GenricTriggerInterface {

    public void beforeInsert(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew) {
        //////////////////////////////////////
        //Setting Primary Acount from Account on Insert
        courseAccountMapping(lTriggerNew);
       // Added by Kyama Method to update Market on Course as per RD-01744.R5
        mapMarketOnCourse(lTriggerNew);
        ////////////////////////////////////
    }
    
    public void afterInsert(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew) {

  }
    
    public void beforeUpdate(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) {
    
        //////////////////////////////////////
        //Setting Primary Acount from Account on Update
        List<UniversityCourse__c> courseUpdateLst = new List<UniversityCourse__c>();
        for(sObject indvRecord : lTriggerNew){
           UniversityCourse__c courseObj = (UniversityCourse__c)indvRecord;
           UniversityCourse__c courseObjOld = (UniversityCourse__c)mTriggerOld.get(courseObj.id);
           if(courseObj.Account__c!=null && (courseObj.Account__c!=courseObjOld.Account__c)){
                courseUpdateLst.add(courseObj);
           }
        }
        if(!courseUpdateLst.isEmpty()){
            courseAccountMapping(courseUpdateLst);
        }
        ////////////////////////////////////
  
        
  
  }
    //Added by Kyama For for RD-01686.3.2 release
    public void afterUpdate(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) 
    {
         try{
         set < Id > course_id = new set < Id > ();
         List < Opportunity > updateoppty = new List < Opportunity > ();
         List < OpportunityUniversityCourse__c > oppCourses = new List < OpportunityUniversityCourse__c > ();
           for (sObject indvRecord : lTriggerNew)
          {
           UniversityCourse__c courseObj = (UniversityCourse__c)indvRecord;
           course_id.add(courseObj.id);
          }
          if (course_id.size() > 0) {
           oppCourses = [select id, Opportunity__r.id, Opportunity__r.name, Opportunity__r.Speciality__c ,UniversityCourse__r.CourseSpecialty__c from OpportunityUniversityCourse__c where UniversityCourse__c in : course_id];
          }
          if (oppCourses.size() > 0) {
           for (OpportunityUniversityCourse__c opp: oppCourses) {
           
           if(opp.Opportunity__r.Speciality__c!=opp.UniversityCourse__r.CourseSpecialty__c)
            {
            Opportunity oppnew = new Opportunity();
            oppnew.id = opp.Opportunity__r.id;
            oppnew.Speciality__c = opp.UniversityCourse__r.CourseSpecialty__c;
            updateoppty.add(oppnew);
             }
            }
           if (updateoppty.size() > 0 && !updateoppty.isEmpty()) {
            update updateoppty;
           }
          }
        }catch(Exception e){
        }
       }
    
    public void beforeDelete(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) {
        
    }
    
    public void afterDelete(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) {
        
    }
    
    public void afterUndelete(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew) {
        
    }

    public void courseAccountMapping(List<UniversityCourse__c> triggerNewLst){

        ////////////////////////////////////////////////////////////////////////////////
        //Setting Primary Selling Account
        try{
            //Iteration of each record from Trigger to fetch Account tagged to course
            Set<Id> courseAccSet = new Set<Id>();
             for(UniversityCourse__c courseObj : triggerNewLst){
               if(courseObj.Account__c!=null){
                    courseAccSet.add(courseObj.Account__c);
               }
              
             }
             //End of Iteration for Course Account Tagging
             //Fetching Account Detail based on above fetched account
             if(!courseAccSet.isEmpty()){
                Map<Id,Account> primaryAccountMap = new Map<Id,Account>([Select id,Name,Primary_Selling_Account__c,Primary_Selling_Account_check__c ,Primary_Selling_Account__r.name,Parent_Selling_Account__c from Account where id in :courseAccSet]); 
                if(!primaryAccountMap.isEmpty()){
                    //Iteration of each record from Trigger to set primary account from account
                     for(UniversityCourse__c coursePrimayObj : triggerNewLst){
                        if(primaryAccountMap.containsKey(coursePrimayObj.Account__c) && primaryAccountMap.get(coursePrimayObj.Account__c).Primary_Selling_Account_check__c){
                            coursePrimayObj.Primary_Selling_AccountText__c= primaryAccountMap.get(coursePrimayObj.Account__c).Name;
                        }
                        else if(primaryAccountMap.containsKey(coursePrimayObj.Account__c) && primaryAccountMap.get(coursePrimayObj.Account__c).Primary_Selling_Account__c!=null){
                            coursePrimayObj.Primary_Selling_AccountText__c= primaryAccountMap.get(coursePrimayObj.Account__c).Primary_Selling_Account__r.name;
                        }
                        else{
                            coursePrimayObj.Primary_Selling_AccountText__c = '';
                        }
                     }
                    
                }
             }
         }catch(Exception e){
                 ExceptionFramework.LogException('CRITICAL','PS_UniversityCourseOperations','Primary Account Mapping',e.getMessage(),UserInfo.getUserName(),'');
                    
          }
         //End of Setting Primary Selling Account
         ///////////////////////////////////////////////////////////////////////////////


    }
    
    //Added Method for release R5 RD-01744
    public void mapMarketOnCourse(List<UniversityCourse__c> triggerNewLst){
       try{
            //Iteration of each record from Trigger to fetch Account tagged to course
            Set<Id> courseAccountSet = new Set<Id>();
             for(UniversityCourse__c courseObject : triggerNewLst){
               if(courseObject.Account__c!=null){
                    courseAccountSet.add(courseObject.Account__c);
               }
             }
          if(!courseAccountSet.isEmpty()){
               Map<Id,Account> accountMap = new Map<Id,Account>([select id, Market2__c , Line_of_Business__c , Geography__c, Business_Unit__c from Account where id in :courseAccountSet]);
          
          if(!accountMap.isEmpty()){
          for(UniversityCourse__c courses:triggerNewLst){ 
                    
          if(accountMap.containsKey(courses.Account__c) && accountMap.get(courses.Account__c).Market2__c!=null && string.isEmpty(courses.Market__c)){
                            courses.Market__c= accountMap.get(courses.Account__c).Market2__c;
                        }
          if(accountMap.containsKey(courses.Account__c) && accountMap.get(courses.Account__c).Geography__c!=null && string.isEmpty(courses.Geography__c)){
                            courses.Geography__c= accountMap.get(courses.Account__c).Geography__c;
                        }
          if(accountMap.containsKey(courses.Account__c) && accountMap.get(courses.Account__c).Line_of_Business__c!=null && string.isEmpty(courses.Line_of_Business__c)){
                            courses.Line_of_Business__c= accountMap.get(courses.Account__c).Line_of_Business__c;
                        }
          }
         }
        }  
      } catch(Exception e){
                 ExceptionFramework.LogException('CRITICAL','PS_UniversityCourseOperations','Market/Geo/LOB Mapping',e.getMessage(),UserInfo.getUserName(),'');
                    
          }
    }
}