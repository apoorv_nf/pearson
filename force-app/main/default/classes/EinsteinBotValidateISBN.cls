/*
 * Author: Apoorv Saxena (Neuraflash)
 * Date: 28/08/2019
 * Description: Checks if the String being passed is a valid ISBN.
 * Test Class: EinsteinBotValidateISBNTest
 */
public without sharing class EinsteinBotValidateISBN{

    @InvocableMethod(label='Einstein SMS Bot - Validate ISBN Format')
    public static List<Boolean> verifyISBNFormat(List<String> isbns) {
    
        try{
            String isbn = isbns[0];
            if(!String.isBlank(isbn)){
                isbn = isbn.trim();
                String isbnRegex = '^(?:ISBN(?:-13)?:? )?(?=[0-9]{13}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)97[8][- ]?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9]$';
                Pattern myPattern = Pattern.compile(isbnRegex);
                Matcher myMatcher = myPattern.matcher(isbn);

                if (myMatcher.matches()){ 
                    return new List<Boolean>{true};
                }
            }
        } catch(Exception ex){}
        return new List<Boolean>{false};
	}
}