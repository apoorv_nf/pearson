/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PreProcessingBatchJob.Cls
   Description:     Abstract class containing common interface and 
   Test Class:      PreProcessingCleanHierarchyBatchTest
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   14/09/2016         0.1        Accenture - Davi Borges                 Created
   12/12/2016         1.1        Accenture - Iegor Nech	                 Added Comments
                                             
-----------------------------------------------------------------------------------------------------------------------------------------------------------
*/

global abstract class PreProcessingBatchJob implements Database.Batchable<SObject>,Database.Stateful{
	//Lookup field values for the records Job_Execution_Result__c Job_Execution_Request__c to keep the status
    public static final String STATUS_STARTED = 'Started'; 
    public static final String STATUS_CRITICAL_ERROR = 'Critical Error';
    public static final String STATUS_COMPLETED = 'Completed';
    public static final String STATUS_BYPASS = 'Bypass';    
    public static final String STATUS_COMPLETED_WITH_ERRORS = 'Completed With Errors';
    
    private static final Integer MAX_ERROR_MESSAGE_SIZE = 30000; //Max size of Error string which will be saved in Salesforce Text Area field
    
    public Date dataFrom {get; protected set;} //Upper bounds of LastModifiedDate for batch Query
    public String jobRequestId {get; protected set;} //Id of record Job_Execution_Request__c
    
    //Copy of Batch job class name into variable
    public String className {get; protected set;}
    //Field API Identifier which can set status, size, dataFrom and jobId API field names by adding corresponding prefixes and __c suffix
    protected String fieldApiIdentifier; //Common preffix of Salesforce fields related to batch
    public String statusFieldApiName {get; protected set;} //Api Name of Specific Job Status field
    public String batchSizeFieldApiName {get; protected set;} //Api Name of Specific Job Batch Size field
    public String dataFromFieldApiName {get; protected set;} //Api Name of Specific Job Date From field
    public String jobIdFieldApiName {get; protected set;} //Api Name of Specific Job Id field
    //If true then Status should not be updated when finish, as other Batch job should start after
    protected Boolean shouldSkipStatusUpdateOnFinish; 
    
    private BatchExecutionResult executionResult; //Variable to keep common batch execution result, which will be saved in database
    
    protected Boolean isJobFailed = false; //True if job gets crytical error
    protected Boolean containsErrors = false; //True if some of records in batch has been failed
    protected String errorMessage = null;
    
    //Unparametrised contstructor required for Type.newInstance call
    global PreProcessingBatchJob(){
        executionResult = new BatchExecutionResult(0);
        setFieldApiNames();
    }
    
    //Common initialize method to set values from Job_Execution_Request__c
    global void initialize(String JobRequestId, Date dataFrom){
        this.jobRequestId = jobRequestId;
        this.dataFrom = BatchUtils.getBatchRunFromDate(dataFrom,className);     
    }
    
    //Populate all Api Field Names by adding corresponding preffixes to fieldApiIdentifier
    protected virtual void setFieldApiNames(){
        if (fieldApiIdentifier != null){
            statusFieldApiName = 'Status_' + fieldApiIdentifier + '__c';
            batchSizeFieldApiName = 'Size_' + fieldApiIdentifier + '__c';
            dataFromFieldApiName = 'Start_Date_' + fieldApiIdentifier + '__c';
            jobIdFieldApiName = 'Job_Id_' + fieldApiIdentifier + '__c';
        }
    }
    
    //Database.Batchable interface start method
    global Database.QueryLocator start(Database.BatchableContext BC){
        Database.QueryLocator queryLocator = null;
        try {
            queryLocator = start();
        } catch (Exception e){
            //Batch job will not start, so method should be called directly
            queryLocator = null;
            setJobAsFailed(e.getMessage());
            markJobStatusOnCompletion();
        }
        
        return queryLocator;
    }
    
    //Protected function which will be overridden in child classes to get Database.QueryLocator
    global protected abstract Database.QueryLocator start();

	//Database.Batchable interface execute method
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        BatchExecutionResult bactchIterationResult = new BatchExecutionResult(scope.size());
        Savepoint sp = Database.setSavepoint(); 
        
        //Keep every block in try/catch to save all possible Exceptions in Job Execution result
        try {
            this.execute(bactchIterationResult, scope);
        } catch (Exception e){
            Database.rollback(sp); //Rollback in case of critical issue
            bactchIterationResult.setExceptionResult(e); //Set critical issue in case of whole batch fails
        } finally {
            executionResult.combineWithBatchIterationResult(bactchIterationResult); //Save batch execution result in memory
        }
    }
    
    //Protected execute method which could be overriden in child classes
    protected virtual void execute(BatchExecutionResult BE, List<sObject> scope){
        BE.setCompletedResult();
    }
    
    //Database.Batchable interface finish method
    global void finish(Database.BatchableContext BC){
        Savepoint sp = Database.setSavepoint();
        try {
            finish();
        } catch (Exception e){
            Database.rollback(sp);
            setJobAsFailed(e.getMessage());
        } finally {
            saveBatchJobExectionInfomration(BC);
            markJobStatusOnCompletion();
        }
    }
    
    //Protected methods which will be overriden in child classes
    protected virtual void finish(){}
    
    //Set Batch job as critically failed to avoid further execution
    protected void setJobAsFailed(String errorMessage){
        this.isJobFailed = true;
        this.errorMessage = errorMessage;
    }
    
    //Create Job_Execution_Result__c record containg overall batch execution results and save it into database
    private void saveBatchJobExectionInfomration(Database.BatchableContext BC){
        Job_Execution_Result__c jobExecutionResult = new Job_Execution_Result__c();
        jobExecutionResult.Status__c = isJobFailed ? STATUS_CRITICAL_ERROR : STATUS_COMPLETED;
        jobExecutionResult.Batch_Class_Name__c = this.className;
        jobExecutionResult.Batch_Job_Id__c = BC.getJobId();
        jobExecutionResult.Job_Execution_Request__c = this.jobRequestId;
        jobExecutionResult.Total_Job_Items__c = executionResult.totalObjectsCount;
        jobExecutionResult.Executed_Date__c = DateTime.now();
        jobExecutionResult.Number_Of_Errors__c = executionResult.errorCount;
        jobExecutionResult.Number_Of_Items_Skipped__c = executionResult.skippedCount;
        jobExecutionResult.Number_Of_Items_Processed__c = executionResult.executedCount;
        if (executionResult.errorCount > 0){ containsErrors = true; }
        
        if (isJobFailed){ //Critical exception has been thrown during batch execution
            jobExecutionResult.Error_Details__c = this.errorMessage;
        } else if (containsErrors){ //Batch finished but some records has failed
            jobExecutionResult.Status__c = STATUS_COMPLETED_WITH_ERRORS;
            if (executionResult.errorList != null && executionResult.errorList.size() > 0){ //Cut general error string to avoid errors
                jobExecutionResult.Error_Details__c = String.join(executionResult.errorList, '\n\n').abbreviate(MAX_ERROR_MESSAGE_SIZE);
            }
        }
        
        Database.insert(jobExecutionResult);
    }
       
    protected Database.SaveResult markJobStatusOnCompletion(){
        Job_Execution_Request__c executionRequest = new Job_Execution_Request__c(Id =  this.jobRequestId);
        if (isJobFailed){
            executionRequest.put(statusFieldApiName, STATUS_CRITICAL_ERROR);
            executionRequest.Error_Message__c = this.errorMessage;
        } else if (shouldSkipStatusUpdateOnFinish != true){
            executionRequest.put(statusFieldApiName, containsErrors ?  STATUS_COMPLETED_WITH_ERRORS : STATUS_COMPLETED);
        }
        
        return Database.update(executionRequest);
    }
    
    //Batch Execution class accumulates information taken from each batch iteration
    //To save final result in Job_Execution_Result__c  record
    public class BatchExecutionResult{
        public Integer totalObjectsCount = 0; //Total number of records in all batches (Including those which will be skipped)
        public Integer errorCount = 0; //How many errors has been found during batch execution
        public Integer executedCount = 0; //How many records has been successfully 
        public Integer skippedCount = 0; //How many records has been ignored by batch (No changes for them were required)
        public List<String> errorList = new List<String>();
        
        //Constructor with batch size
        public BatchExecutionResult(Integer batchSize){
            this.totalObjectsCount = batchSize;
        }
        
        //Add single batch execution result into accumulated batch job execution result
        public void combineWithBatchIterationResult(BatchExecutionResult batchIterationResult){
            totalObjectsCount += batchIterationResult.totalObjectsCount;
            errorCount += batchIterationResult.errorCount;
            executedCount += batchIterationResult.executedCount;
            skippedCount += batchIterationResult.skippedCount;
            errorList.addAll(batchIterationResult.errorList);
        }
        
        //Add executed count and errors in single job execution
        public void addPartialResult(Integer executedCount, Integer skippedCount, Integer errorCount, List<String> errorList){
            this.errorCount += errorCount;
            this.executedCount += executedCount;
            this.skippedCount += skippedCount;
            if (errorList != null){
                this.errorList.addAll(errorList);
            }
        }
        
        //Add job execution error
        public void addSingleError(String errorString){
            errorCount += 1;
            errorList.add(errorString);
        }
        
        //Save that all records has been successfully executed by batch
        public void setCompletedResult(){
            errorCount = 0;
            skippedCount = 0;
            executedCount = totalObjectsCount;
        }
        
        
        //Save single error into batch
        public void setExceptionResult(Exception e){
            errorCount = 1;
            executedCount = 0;
            skippedCount = 0;
            errorList = new List<String>{'Exception: ' + e.getMessage() + ' (Line ' + e.getLineNumber() + ') ' + e};
        }
        
        //Save DML errors into batch
        private void addDatabaseErrors(String operation, String objectId, Database.Error[] dmlErrorList){
            errorCount += 1;
            System.debug('Batch DML Errors for ' + objectId + ': ' + dmlErrorList);
            for (Database.Error errorInfo : dmlErrorList){
                errorList.add(operation + ' of ' + objectId + ': ' + errorInfo.getStatusCode() + ' ' + errorInfo.getMessage());
            }           
        }
        
        //Save DML deletion result 
        public void addDmlResultData(Database.DeleteResult[] dmlResultList){
            for (Database.DeleteResult dmlResult : dmlResultList){
                if (dmlResult.isSuccess()){
                    executedCount += 1;
                } else {
                    addDatabaseErrors('Delete', dmlResult.getId(), dmlResult.getErrors());
                }
            }
        }
                
        //Save DML insert/update result
        public void addDmlResultData(Database.SaveResult[] dmlResultList){
            for (Database.SaveResult dmlResult : dmlResultList){
                if (dmlResult.isSuccess()){
                    executedCount += 1;
                } else {
                    addDatabaseErrors('Save', dmlResult.getId(), dmlResult.getErrors());
                }
            }
        }
                        
        //Save DML upsert result
        public void addDmlResultData(Database.UpsertResult[] dmlResultList){
            for (Database.UpsertResult dmlResult : dmlResultList){
                if (dmlResult.isSuccess()){
                    executedCount += 1;
                } else {
                    addDatabaseErrors('Upsert', dmlResult.getId(), dmlResult.getErrors());
                }
            }
        }
    }
}