/*******************************************************************
* Class Name   : UKTEPPricingController
* Author       : Aman Sawhney
* Date Created : 7 / Nov / 2018 
* Description  : For calling the new prices of the quote 
*                line items from TEP
* ****************************************************************/

public with sharing class UKTEPPricingController {
    
    //general declarations and definitions
    public AlertBox alertBoxCmp {get;set;}
    public Quote targetQuote {get;set;}
    public String requestLabel {get;set;}
    public UKTEPPricingXMLController quoteXMlHandler{get;set;}
    public UKTEPPricingXMLController.QuoteRequestDetail quoteRequest{get;set;}
    public final Integer REQUEST_TIMEOUT = 120;
    
    public UKTEPPricingController(ApexPages.StandardController sc) {
        
        //get target quote
        targetQuote = (Quote)sc.getRecord();
        
        //xml request handler initialize
        quoteXMlHandler = UKTEPPricingXMLController.getInstance();
        quoteXMlHandler.quoteId = targetQuote.Id;
        
        //initialize the alert box 
        this.alertBoxCmp = new AlertBox();
        
        //if at least one quote line item exist for price fetch
        if (targetQuote.QuoteLineItems!=null && !targetQuote.QuoteLineItems.isEmpty()) {
            
            //if quote is on 'needs pricing' status
            if(targetQuote.Status == Label.Quote_Price_Call_Status){
                
                this.alertBoxCmp.rendered = true;
                this.alertBoxCmp.showSpinner = true;
                this.alertBoxCmp.styleClass = 'slds-theme_alt-inverse';
                this.alertBoxCmp.message = Label.Fetch_Price_Wait_Message;
                this.alertBoxCmp.heading = Label.Fetch_Price_Alert_Heading;
                
            }
            
            //if quote is not in 'needs pricing' status
            else {
            
                //set an alert for quote not in need pricing status
                this.alertBoxCmp.rendered = true;
                this.alertBoxCmp.showSpinner = false;
                this.alertBoxCmp.styleClass = 'slds-theme_error';
                this.alertBoxCmp.message = Label.Quote_Not_In_Pricing_Status;
                this.alertBoxCmp.heading = Label.Fetch_Price_Alert_Error_Heading;
                this.alertBoxCmp.primaryButtonLabel = 'Back To Quote';
            
            }
            
        } 
        
        //if no quote line item exist
        else{
            
            //set an alert for no line items for pricing
            this.alertBoxCmp.rendered = true;
            this.alertBoxCmp.showSpinner = false;
            this.alertBoxCmp.styleClass = 'slds-theme_warning';
            this.alertBoxCmp.message = Label.No_Line_Items_For_Pricing;
            this.alertBoxCmp.heading = Label.Fetch_Price_Alert_Heading;
            this.alertBoxCmp.primaryButtonLabel = 'Back To Quote';
                
        }
        
        
        
    }
    
    //method to do pricing callback    
    public System.Continuation getPrice() {
        
        Continuation callerProcess = null;
        
        //make price call only if quote needs pricing
        if (targetQuote.Status == Label.Quote_Price_Call_Status) {
            
            //HTTP CALL 
            HttpRequest request = new HttpRequest();
            
            //set request attributes
            request.setEndpoint(General_One_CRM_Settings__c.getInstance('TEPPricing').value__c);
            request.setClientCertificateName(General_One_CRM_Settings__c.getInstance('TEPPricing').Category__c);
            request.setHeader('Content-Type', 'application/xml; charset=UTF-8');
            request.setHeader('Accept', 'application/xml');
            request.setMethod('POST');
            
            //fetch the request body having details of quote
            this.quoteRequest = quoteXMlHandler.getQuoteRequestDetail();
            
            
            //if xml string is not null
            if(this.quoteRequest.tepXMLRequest!=null){
                
                //set the request body
                request.setBody(this.quoteRequest.tepXMLRequest);
                
                //Setup Continuation
                callerProcess = new Continuation(this.REQUEST_TIMEOUT);
                callerProcess.continuationMethod = 'repriceResponseHandler';
                
                //save the request call for retreiving the response in handler
                this.requestLabel = callerProcess.addHttpRequest(request);
                
            }
            
            else{
                
                //if sample quote line items are existing, update them 
                if(this.quoteRequest.sampleQuoteLineItemList!=null && !this.quoteRequest.sampleQuoteLineItemList.isEmpty()){
                    
                    //update sample quote line items with unit price
                    this.quoteRequest.sampleQuoteLineItemList = sampleQuoteLineItemAssignments(this.quoteRequest.sampleQuoteLineItemList);
                    
                    Database.update(this.quoteRequest.sampleQuoteLineItemList);
                    
                    //show the success message of line items updation
                    this.alertBoxCmp.rendered = true;
                    this.alertBoxCmp.showSpinner = false;
                    this.alertBoxCmp.styleClass = 'slds-theme_success';
                    this.alertBoxCmp.message = Label.Sample_Quotes_Updated;
                    this.alertBoxCmp.heading = Label.Fetch_Price_Alert_Heading;
                    this.alertBoxCmp.primaryButtonLabel = 'Back To Quote';
                    
                }
                
                else{
                    
                    //set an alert for no line items for pricing
                    this.alertBoxCmp.rendered = true;
                    this.alertBoxCmp.showSpinner = false;
                    this.alertBoxCmp.styleClass = 'slds-theme_warning';
                    this.alertBoxCmp.message = Label.No_Line_Items_For_Pricing;
                    this.alertBoxCmp.heading = Label.Fetch_Price_Alert_Heading;
                    this.alertBoxCmp.primaryButtonLabel = 'Back To Quote';    
                    
                    
                }
                
                
            }
            
            
        }
        return callerProcess;
    }
    
    //A response handler for reprice call
    public void repriceResponseHandler() {
        
        //get the response
        HttpResponse response = Continuation.getResponse(this.requestLabel);
        
        switch on response.getStatusCode(){
            
            //timeout
            when 2000{
                
                //show the error message of timeout
                this.alertBoxCmp.rendered = true;
                this.alertBoxCmp.showSpinner = false;
                this.alertBoxCmp.styleClass = 'slds-theme_error';
                this.alertBoxCmp.message = Label.Server_Timeout_Error;
                this.alertBoxCmp.heading = Label.Fetch_Price_Alert_Heading;
                this.alertBoxCmp.primaryButtonLabel = 'Back To Quote';
                
            }
            
            //connection failure
            when 2001{
                
                //show the error message of connection failure
                this.alertBoxCmp.rendered = true;
                this.alertBoxCmp.showSpinner = false;
                this.alertBoxCmp.styleClass = 'slds-theme_error';
                this.alertBoxCmp.message = Label.Connection_Failure;
                this.alertBoxCmp.heading = Label.Fetch_Price_Alert_Heading;
                this.alertBoxCmp.primaryButtonLabel = 'Back To Quote';
                
            }
            
            //response hasent arrived
            when 2003{
                
                
            }
            
            //response too large
            when 2004{
                
                
                //show the error message of connection failure
                this.alertBoxCmp.rendered = true;
                this.alertBoxCmp.showSpinner = false;
                this.alertBoxCmp.styleClass = 'slds-theme_error';
                this.alertBoxCmp.message = 'Some error occurred!';
                this.alertBoxCmp.heading = Label.Fetch_Price_Alert_Heading;
                this.alertBoxCmp.primaryButtonLabel = 'Back To Quote';
                
            } 
            
            //response received
            when else{
                
                //Debug response
                
                //get the line items from response
                // Commented and Modified by Navaneeth - Start
                /* List<QuoteLineItem> tepQuoteLineItemList = quoteXMlHandler.getLineItemsFromXMLResponse(response.getBody());*/
                UKTEPWrapperResponseClass wrapResponse = quoteXMlHandler.getLineItemsFromXMLResponseReturnWrapper(response.getBody());
                List<QuoteLineItem> tepQuoteLineItemList = wrapResponse.processResponseQuoteLineItemList;
                String finalStatus = wrapResponse.processResponseStatus;
                String finalMessage = wrapResponse.processResponseMessage;
                // Commented and Modified by Navaneeth - End
                
                
                //if the tep line items list is not null and not empty, update the quote line items
                if(tepQuoteLineItemList!=null && !tepQuoteLineItemList.isEmpty()){
                    
                    //prepare the list of sample and non-sample line items combine list
                    List<QuoteLineItem> updatableQuoteLineItemList = new List<QuoteLineItem>();
                    
                    //add non-sample quote line items to update
                    updatableQuoteLineItemList.addAll(tepQuoteLineItemList);
                    
                    //add sample quote line items to update
                    if(this.quoteRequest.sampleQuoteLineItemList!=null && !this.quoteRequest.sampleQuoteLineItemList.isEmpty()){
                        
                        //update sample quote line items with unit price
                        this.quoteRequest.sampleQuoteLineItemList = sampleQuoteLineItemAssignments(this.quoteRequest.sampleQuoteLineItemList);
                        
                        updatableQuoteLineItemList.addAll(this.quoteRequest.sampleQuoteLineItemList);    
                    }
                    
                    
                    //update the line items
                    try{
                        
                        update updatableQuoteLineItemList ;    
                        
                        //update quote to approved status
                        targetQuote.Status = 'Approved';
                        update targetQuote;
                    
                        //show the success message of line items updation
                        this.alertBoxCmp.rendered = true;
                        this.alertBoxCmp.showSpinner = false;
                        this.alertBoxCmp.styleClass = 'slds-theme_success';
                        this.alertBoxCmp.message = Label.TEP_Price_Updated_Alert_Message;
                        this.alertBoxCmp.heading = Label.Fetch_Price_Alert_Heading;
                        this.alertBoxCmp.primaryButtonLabel = 'Back To Quote';
                        
                        this.alertBoxCmp.message = this.alertBoxCmp.message + ' Actual TEP Success Message ::: ' + finalMessage;
                        
                    }
                    catch(Exception e){
                        
                        //show the error message of line items updation error
                        this.alertBoxCmp.rendered = true;
                        this.alertBoxCmp.showSpinner = false;
                        this.alertBoxCmp.styleClass = 'slds-theme_error';
                        this.alertBoxCmp.message = Label.Quote_Line_Update_Error;
                        this.alertBoxCmp.heading = Label.Fetch_Price_Alert_Heading;
                        this.alertBoxCmp.primaryButtonLabel = 'Back To Quote';
                        this.alertBoxCmp.message = this.alertBoxCmp.message + ' Actual OneCRM Exception Message ::: ' + e.getMessage();
                        
                        
                    }
                    
                }
                
                else{
                    
                    //show the error message of line items data is not fetched
                    this.alertBoxCmp.rendered = true;
                    this.alertBoxCmp.showSpinner = false;
                    this.alertBoxCmp.styleClass = 'slds-theme_error';
                    this.alertBoxCmp.message = Label.Quote_Line_Price_Fetch_Failed;
                    this.alertBoxCmp.heading = Label.Fetch_Price_Alert_Heading;
                    this.alertBoxCmp.primaryButtonLabel = 'Back To Quote';
                    this.alertBoxCmp.message = this.alertBoxCmp.message + ' Actual TEP Error Message ::: ' + finalMessage ;
                    
                }
                
            }
        }
        
        
    }
    
    //this method assigns values to sample quote line items
    public List<QuoteLineItem> sampleQuoteLineItemAssignments(List<QuoteLineItem> quoteLineItemList){
        
        if(quoteLineItemList!=null && !quoteLineItemList.isEmpty()){
            for(QuoteLineItem li : quoteLineItemList){
                li.UnitPrice = 0;
            }    
        }
        
        return quoteLineItemList;
        
    }
    
    //SubClass to handle alertbox related attributes
    public class AlertBox {
        
        public String heading {get;set;}
        public String message {get;set;}
        public Boolean showSpinner {get;set;}
        public Boolean rendered {get;set;}
        public String styleClass {get;set;}
        public String primaryButtonLabel {get;set;}
        
    }
    
}