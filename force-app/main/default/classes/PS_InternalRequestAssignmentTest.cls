/* --------------------------------------------------------------------------------------------------------------------------------------

  Description:  Unit Tests for PS_InternalRequestAssignment.cls
  Related class:   PS_InternalRequestAssignment.cls 

   Date             Version           Author                Summary of Changes 
   -----------      -------        -----------------   ----------------------------------------------------------------------
  16-Nov-2015         1.0          Tom Carman               Intial implementation
  -------------------------------------------------------------------------------------------------------------------------------------- */



@isTest
private class PS_InternalRequestAssignmentTest {
  
  @isTest static void testRuleMatch() {

    Id accountRequestRecordTypeId = Schema.SObjectType.Internal_Request__c.getRecordTypeInfosByName().get('Account Request').getRecordTypeId();

    List<Internal_Request__c> requestsToInsert = new List<Internal_Request__c>();

    //Internal_Request__c request1 = TestDataFactory.createInternalRequest(); // Removing due to unrelated compilation issues with TestDataFactory

    Internal_Request__c request1 = new Internal_Request__c();
    request1.RecordTypeId = accountRequestRecordTypeId;
    request1.Internal_Request_Type__c = 'Account Update';
    request1.Market__c = 'US';
    requestsToInsert.add(request1);

    //Internal_Request__c request2 = TestDataFactory.createInternalRequest();
    Internal_Request__c request2 = new Internal_Request__c();
    request2.RecordTypeId = accountRequestRecordTypeId;
    request2.Internal_Request_Type__c = 'Account Update';
    request2.Market__c = 'ZA';
    requestsToInsert.add(request2);

    Test.startTest();

      insert requestsToInsert;

    Test.stopTest();


    Map<Id, Group> queueMap = new Map<Id, Group>([SELECT Id, DeveloperName FROM Group WHERE Type = 'Queue']);


    //requery requests for updated data
    requestsToInsert = [SELECT Id, OwnerId, Market__c FROM Internal_Request__c WHERE Id IN :requestsToInsert];

    for(Internal_Request__c request : requestsToInsert) {
      if(request.Market__c == 'US') {
        System.assertEquals('US_Account_Admin_Requests', queueMap.get(request.OwnerId).DeveloperName, 'Expect to be owned by Queue matched in rules');
      } else if(request.Market__c == 'ZA') {
        System.assertEquals('ZA_Account_Admin_Requests', queueMap.get(request.OwnerId).DeveloperName, 'Expect to be owned by Queue matched in rules');
      }
    }

  }  


  @isTest static void testRuleNoMatch() {

    Id accountRequestRecordTypeId = Schema.SObjectType.Internal_Request__c.getRecordTypeInfosByName().get('Account Request').getRecordTypeId();

    List<Internal_Request__c> requestsToInsert = new List<Internal_Request__c>();

    //Internal_Request__c request1 = TestDataFactory.createInternalRequest();
    Internal_Request__c request1 = new Internal_Request__c();

    request1.RecordTypeId = accountRequestRecordTypeId;
    request1.Internal_Request_Type__c = 'Account Update';
    request1.Market__c = 'Wonderland'; // MARKET DOES NOT EXIST, should not find rule that matches
    requestsToInsert.add(request1);

    Test.startTest();

      insert requestsToInsert;

    Test.stopTest();

    //requery requests for updated data
    requestsToInsert = [SELECT Id, OwnerId, Market__c FROM Internal_Request__c WHERE Id IN :requestsToInsert];


    System.assertEquals(UserInfo.getUserId(), requestsToInsert[0].OwnerId, 'Expect record to still be owned by user, as no matching rule');


  }  


  @isTest static void testInvalidRecordType() {

    // use record type which should not be processed
    Id accountRequestRecordTypeId = Schema.SObjectType.Internal_Request__c.getRecordTypeInfosByName().get('Territory Realignment').getRecordTypeId();

    List<Internal_Request__c> requestsToInsert = new List<Internal_Request__c>();

    //Internal_Request__c request1 = TestDataFactory.createInternalRequest();
    Internal_Request__c request1 = new Internal_Request__c();

    request1.RecordTypeId = accountRequestRecordTypeId;
    request1.Internal_Request_Type__c = 'Account Update';
    request1.Market__c = 'US'; 


    requestsToInsert.add(request1);

    Test.startTest();

      insert requestsToInsert;

    Test.stopTest();

    //requery requests for updated data
    requestsToInsert = [SELECT Id, OwnerId, Market__c FROM Internal_Request__c WHERE Id IN :requestsToInsert];


    System.assertEquals(UserInfo.getUserId(), requestsToInsert[0].OwnerId, 'Expect record to still be owned by user, as no matching rule');


  }  

  
}