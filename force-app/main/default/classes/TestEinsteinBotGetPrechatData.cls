/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 08/07/2018
 */
@isTest
private class TestEinsteinBotGetPrechatData {

	@isTest
	static void it_should_get_prechat_data_from_live_agent_chat() {
		Case cas = new Case();
		insert cas;

		Test.startTest();
		Account acc = new Account(
			Name='testAccount',
			BillingStreet = 'Street1',
            BillingCity = 'Sydney',
			BillingPostalCode = 'ABC DEF',
			BillingCountry = 'Australia');
		insert acc;
		Test.stopTest();

		Contact contact = new Contact(
			FirstName = 'Rick',
			LastName = 'Sanchez',
			Email = 'rick.sanchez@email.com'
		);
		insert contact;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

		String chatKey = 'asdfasdf';

		LiveChatTranscript transcript = new LiveChatTranscript(
			CaseId = cas.Id,
			ContactId = contact.Id,
			AccountId = acc.Id,
			LiveChatVisitorId = vistor.Id,
			ChatKey = chatKey,
			Browser = 'Chrome 123123123');
		insert transcript;

		List<EinsteinBotGetPrechatData.PreChatDataResponse> prechatDataResults = EinsteinBotGetPrechatData.getPreChatData(new List<String>{chatKey});

		System.assert(!prechatDataResults.isEmpty(), 'Results should not be empty');

		System.assertEquals(contact.Id, prechatDataResults[0].contact.Id);
		System.assertEquals(true, prechatDataResults[0].matchFound);
		System.assertEquals(cas.Id, prechatDataResults[0].cas.Id);
		System.assertEquals('Chrome', prechatDataResults[0].browser);
		System.assertEquals(true, prechatDataResults[0].newAccount);
	}

	@isTest
	static void it_should_get_prechat_data_from_live_agent_chat_firefox() {
		Case cas = new Case();
		insert cas;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

		String chatKey = 'asdfasdf';

		LiveChatTranscript transcript = new LiveChatTranscript(
			CaseId = cas.Id,
			LiveChatVisitorId = vistor.Id,
			ChatKey = chatKey,
			Browser = 'Firefox 123123123');
		insert transcript;

		List<EinsteinBotGetPrechatData.PreChatDataResponse> prechatDataResults = EinsteinBotGetPrechatData.getPreChatData(new List<String>{chatKey});

		System.assert(!prechatDataResults.isEmpty(), 'Results should not be empty');

		System.assertEquals('Firefox', prechatDataResults[0].browser);
	}

	@isTest
	static void it_should_get_prechat_data_from_live_agent_chat_safari() {
		Case cas = new Case();
		insert cas;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

		String chatKey = 'asdfasdf';

		LiveChatTranscript transcript = new LiveChatTranscript(
			CaseId = cas.Id,
			LiveChatVisitorId = vistor.Id,
			ChatKey = chatKey,
			Browser = 'Safari 123123123');
		insert transcript;

		List<EinsteinBotGetPrechatData.PreChatDataResponse> prechatDataResults = EinsteinBotGetPrechatData.getPreChatData(new List<String>{chatKey});

		System.assert(!prechatDataResults.isEmpty(), 'Results should not be empty');

		System.assertEquals('Safari', prechatDataResults[0].browser);
	}

	@isTest
	static void it_should_get_prechat_data_from_live_agent_chat_explorer() {
		Case cas = new Case();
		insert cas;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

		String chatKey = 'asdfasdf';

		LiveChatTranscript transcript = new LiveChatTranscript(
			CaseId = cas.Id,
			LiveChatVisitorId = vistor.Id,
			ChatKey = chatKey,
			Browser = 'Internet Explorer');
		insert transcript;

		List<EinsteinBotGetPrechatData.PreChatDataResponse> prechatDataResults = EinsteinBotGetPrechatData.getPreChatData(new List<String>{chatKey});

		System.assert(!prechatDataResults.isEmpty(), 'Results should not be empty');

		System.assertEquals('Internet Explorer', prechatDataResults[0].browser);
	}

	@isTest
	static void it_should_get_prechat_data_from_live_agent_chat_edge() {
		Case cas = new Case();
		insert cas;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

		String chatKey = 'asdfasdf';

		LiveChatTranscript transcript = new LiveChatTranscript(
			CaseId = cas.Id,
			LiveChatVisitorId = vistor.Id,
			ChatKey = chatKey,
			Browser = 'Microsoft Edge');
		insert transcript;

		List<EinsteinBotGetPrechatData.PreChatDataResponse> prechatDataResults = EinsteinBotGetPrechatData.getPreChatData(new List<String>{chatKey});

		System.assert(!prechatDataResults.isEmpty(), 'Results should not be empty');

		System.assertEquals('Microsoft Edge', prechatDataResults[0].browser);
	}

	@isTest
	static void it_should_get_prechat_data_from_live_agent_chat_unknown() {
		Case cas = new Case();
		insert cas;

        LiveChatVisitor vistor = new LiveChatVisitor();
        insert vistor;

		String chatKey = 'asdfasdf';

		LiveChatTranscript transcript = new LiveChatTranscript(
			CaseId = cas.Id,
			LiveChatVisitorId = vistor.Id,
			ChatKey = chatKey,
			Browser = 'unknown');
		insert transcript;

		List<EinsteinBotGetPrechatData.PreChatDataResponse> prechatDataResults = EinsteinBotGetPrechatData.getPreChatData(new List<String>{chatKey});

		System.assert(!prechatDataResults.isEmpty(), 'Results should not be empty');

		System.assertEquals('Unknown', prechatDataResults[0].browser);
	}
}