/*******************************************************************************************************************
* Apex Class Name  : PS_mappingShippedProductISBN.cls
* Version       : 1.0 
* Created Date    : 28 APRIL 2016
* Function       : class for mapping shipped product based on OrderProduct ISBN10
* Modification Log  :
* Developer                   Date                   Description
* ------------------------------------------------------------------------------------------------------------------
* RG                          28/04/2016             Created for mapping shipped product based on OrderProduct ISBN10
*******************************************************************************************************************/
public without sharing class PS_MappingShippedProduct{

    public void mappShippedProduct(Map<Id,OrderItem> mapNewOrderProduct, Map<Id,OrderItem> mapOldOrderProduct){
        Map<id,String> oOLItoISBNmap = new Map<id,String>(); 
        for(OrderItem oli: mapNewOrderProduct.values()){
          if(oli.Shipped_Product_ISBN10__c != null){
              oOLItoISBNmap.put(oli.id,oli.Shipped_Product_ISBN10__c);
          }         
        }
        
        Map<String,Product2> oKeytoProductmap = new Map<String,Product2>(); 
            for(Product2 shippedProduct : [SELECT Id,Line_of_Business__c,SBN__c,Business_Unit__c,market__c FROM product2 where SBN__c IN :oOLItoISBNmap.values()]){ //query more option
                String ProdDetails = shippedProduct.SBN__c+'_'+shippedProduct.market__c+'_'+shippedProduct.Line_of_Business__c+'_'+shippedProduct.Business_Unit__c;  
                oKeytoProductmap.put(ProdDetails,shippedProduct);
            }
        
        for(OrderItem oli: mapNewOrderProduct.values()){
            if (oOLItoISBNmap.containsKey(oli.id))
            //if(oli.Shipped_Product_ISBN10__c != null && (mapOldOrderProduct.get(oli.Id).Shipped_Product_ISBN10__c != mapNewOrderProduct.get(oli.Id).Shipped_Product_ISBN10__c))
            {
              String oliProductDetails = mapNewOrderProduct.get(oli.Id).Shipped_Product_ISBN10__c+'_'+mapNewOrderProduct.get(oli.Id).market__c+'_'+mapNewOrderProduct.get(oli.Id).Line_of_Business__c+'_'+mapNewOrderProduct.get(oli.Id).Business_Unit__c; 
              if(oKeytoProductmap.containsKey(oliProductDetails)){
                  oli.Shipped_Product__c = oKeytoProductmap.get(oliProductDetails).Id;
              }else{
                  String errorMessage= 'Product doesnt exist for :[' + oli.Shipped_Product_ISBN10__c + ']';
                  oli.Shipped_Product__c = null;
              } 
                
            }
         }    
    }
}