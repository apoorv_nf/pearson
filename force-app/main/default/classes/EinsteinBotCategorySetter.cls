/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 02/06/2018
 */
global without sharing class EinsteinBotCategorySetter {

    @InvocableMethod(label='Einstein Bot - Set Category/Subcategory')
    global static List<CaseCreationResponse> setCategory(List<String> dialogs) 
    {
		List<CaseCreationResponse> responses = new List<CaseCreationResponse>();
		Map<String, Einstein_Bot_Slot_Map__mdt> slotMaps = new Map<String, Einstein_Bot_Slot_Map__mdt>();
		for(Einstein_Bot_Slot_Map__mdt slotMap : [
			SELECT DeveloperName, Category__c, Subcategory__c, Message__c
			FROM Einstein_Bot_Slot_Map__mdt
			WHERE DeveloperName IN :dialogs]) {
			slotMaps.put(slotMap.DeveloperName, slotMap);
		}

		for(String dialog : dialogs) {
			CaseCreationResponse response = new CaseCreationResponse();
			Einstein_Bot_Slot_Map__mdt slotMap = slotMaps.get(dialog);

			if(slotMap != null) {
				response.category = slotMap.Category__c;
				response.subCategory = slotMap.Subcategory__c;
				response.message = slotMap.Message__c;
			}

			responses.add(response);
		}

		return responses;
	}

    global class CaseCreationResponse
    {
        @InvocableVariable
        global String category;

        @InvocableVariable
        global String subCategory;
		
		@InvocableVariable
		global String message;
    }
}