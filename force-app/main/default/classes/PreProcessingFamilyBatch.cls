/* --------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PreProcessingFamilyBatch.Cls
   Description:     Batch class responsible for Preprocessing family information that was originated from MDM
   Test Class:      PreProcessingFamilyBatchTest
   Date             Version           Author                  Tag                                Summary of Changes 
   -----------      ----------   ------------------------   --------     ---------------------------------------------------------------------------------------
   14/09/2016         0.1        Accenture - Davi Borges                 Created
   25/10/2016         0.5        Accenture - Iegor Nechypor              Added inheritance from PreProcessingBatchJob
                                             
-----------------------------------------------------------------------------------------------------------------------------------------------------------
*/
global class PreProcessingFamilyBatch extends PreProcessingBatchJob implements Database.Batchable<SObject>,Database.Stateful {
    // standard constructor
    global PreProcessingFamilyBatch() {
        super();       
    }
    	
    //Set information about class and field preffix to store Batch Result in query	
	protected override void setFieldApiNames(){
	    this.className = 'PreProcessingFamilyBatch';
	    this.fieldApiIdentifier = 'Family_Member_Creation';
		super.setFieldApiNames();
	}

	//Extend PreProcessBatchJob start function to set logic for Database.Batchable interface method start
    global override Database.QueryLocator start() {
       return Database.getQueryLocator([SELECT 
                                            Id,
                                            Market__c,
                                            MDM_Integration_External_ID__c                                 
                                        FROM
                                            Product_Family__c
                                        WHERE                                  
                                            LastModifiedDate >= :this.dataFrom AND
                                            MDM_Integration_External_ID__c != null AND
                                            MDM_Active__c = true
                                        ]);
    }

	//Extend PreProcessingBatchJob execute function to set logic for Database.Batchable interface method execute
    protected virtual override void execute(BatchExecutionResult batchResult, List<SObject> scope){
      Map<String,Product_Family__c> families = new Map<String,Product_Family__c>();
      extractFamilyMap(families,scope);

      System.debug('Quantity of Families-->' + families.size());

      Map<String,FamilyRelationship> productMembers = new  Map<String,FamilyRelationship>();
      retrieveExistingMembers(productMembers,families);
      System.debug('Quantity of Members after  existing-->' + productMembers.size());
      retrieveStagingMembers(productMembers,families);
      System.debug('Quantity of Members after staging-->' + productMembers.size());

      flushFamilyMembers(batchResult, productMembers);
    }

     /* FAMILY METHODS */
	//Convert sObject List to map of Product families
    private void extractFamilyMap(Map<String,Product_Family__c> families,List<SObject> sObjFamiles)
    {
      for(SObject obj: sObjFamiles)
      {
        Product_Family__c family = (Product_Family__c) obj;
        families.put(family.MDM_Integration_External_ID__c, family);
      }
    }

	//Get list of Family members for specific product families and created wrapper instances for them
    public void retrieveExistingMembers(Map<String,FamilyRelationship> productMembers, Map<String,Product_Family__c> families){
    
        for(Program_Family_Member__c member :[SELECT 
                                            Id,
                                            MDM_Alternative_Version__c,
                                            MDM_Association_Category__c,
                                            MDM_eLearning__c,
                                            MDM_Instructor_Resource__c,
                                            MDM_Main__c,
                                            MDM_Package__c,
                                            MDM_Primary_Sampling__c,
                                            MDM_Primary_Selling__c,
                                            MDM_Student_Resource__c,
                                            Product__c,
                                            Product__r.External_Product_ID__c,
                                            Program_Family__r.Id,
                                            Program_Family__r.MDM_Integration_External_ID__c,
                                            MDM_Regional_Attributes__r.Id,
                                            MDM_Regional_Attributes__r.MDM_Integration_External_ID__c                        
                                        FROM
                                            Program_Family_Member__c
                                        WHERE                                  
                                            Program_Family__r.MDM_Integration_External_ID__c = :families.keySet()
                                        ]){

        FamilyMember memberItem = new FamilyMember();
      memberItem.salesforceId = member.Id;
      memberItem.productExternalID = member.Product__r.External_Product_ID__c;
      memberItem.familyExternalID = member.Program_Family__r.MDM_Integration_External_ID__c;
      memberItem.regionalAttrExternalID = member.MDM_Regional_Attributes__r.MDM_Integration_External_ID__c;
      memberItem.pSell = member.MDM_Primary_Selling__c;
      memberItem.pSam = member.MDM_Primary_Sampling__c;
      memberItem.alternativeVersion = member.MDM_Alternative_Version__c;
      memberItem.eLearning = member.MDM_eLearning__c;
      memberItem.instructorResource = member.MDM_Instructor_Resource__c;
      memberItem.mainTitle = member.MDM_Main__c;
      memberItem.isPackage = member.MDM_Package__c;
      memberItem.studentResource = member.MDM_Student_Resource__c;               
      memberItem.isInStage = false;

      addItemtoMap(productMembers,memberItem);
    }

  }

	//Get Staing Relationships present between two family members to populate parent/child relationships between family members 
    public void retrieveStagingMembers(Map<String,FamilyRelationship> productMembers, Map<String,Product_Family__c> families){

      FamilyMember memberItem = new FamilyMember();

      for(Item_Relationship_Staging__c member :[SELECT 
                                            MDM_Item_Id__c,
                                            MDM_Related_Item_Id__c,
                                            MDM_ATTR_CHAR_1__c,
                                            MDM_ATTR_CHAR_2__c,
                                            MDM_ATTR_CHAR_3__c                        
                                        FROM
                                            Item_Relationship_Staging__c
                                        WHERE
                                            MDM_Relationship_Type_Id__c = '1' AND 
                                            MDM_ATTR_CHAR_1__c ='Family' AND                                
                                            MDM_Item_Id__c = :families.keySet()
                                        ORDER BY
                                            MDM_Item_Id__c,
                                            MDM_Related_Item_Id__c
                                        ]){
        if((memberItem.familyExternalID !=null 
          && memberItem.familyExternalID != member.MDM_Item_Id__c) 
          || memberItem.productExternalID != member.MDM_Related_Item_Id__c )
        {
          addItemtoMap(productMembers,memberItem);

          memberItem = new FamilyMember();
        }


        memberItem.productExternalID = member.MDM_Related_Item_Id__c;
        memberItem.familyExternalID = member.MDM_Item_Id__c;
        memberItem.regionalAttrExternalID = member.MDM_Related_Item_Id__c + '|' + families.get(memberItem.familyExternalID).Market__c;
       
        memberItem.pSell = (member.MDM_ATTR_CHAR_3__c =='PSEL')? true :  memberItem.pSell;
        memberItem.pSam = (member.MDM_ATTR_CHAR_3__c =='PSAM')? true :  memberItem.pSam;
        memberItem.alternativeVersion = (member.MDM_ATTR_CHAR_3__c =='ALTV')? true :  memberItem.alternativeVersion;
        memberItem.eLearning = (member.MDM_ATTR_CHAR_3__c =='ELRA')? true :  memberItem.eLearning;
        memberItem.instructorResource = ( member.MDM_ATTR_CHAR_3__c =='INSR')? true :  memberItem.instructorResource;
        memberItem.mainTitle = false;
        memberItem.isPackage = (member.MDM_ATTR_CHAR_2__c =='Value Pack' )? true :  memberItem.isPackage;
        memberItem.studentResource = (member.MDM_ATTR_CHAR_3__c =='STUR ')? true :  memberItem.studentResource;              
        memberItem.isInStage = true;
      }

      if(memberItem.familyExternalID !=null )
      {
        addItemtoMap(productMembers,memberItem);
      }

    }

	//Add new Family Member to product members map
    private void addItemtoMap(Map<String,FamilyRelationship> productMembers, FamilyMember member)
    {
      if(! productMembers.containsKey(member.familyExternalID))
        {
          productMembers.put(member.familyExternalID , new FamilyRelationship());
        }

        productMembers.get(member.familyExternalID).addMember(member);
    }

   //Process batch DML operations to insert, delete and update family members records
    private void flushFamilyMembers(BatchExecutionResult batchResult, Map<String,FamilyRelationship> productMembers)
   {
     List<Program_Family_Member__c> targetInsertUpdate = new List<Program_Family_Member__c>();
     List<Program_Family_Member__c> targetDelete = new List<Program_Family_Member__c>();
    
     for(FamilyRelationship family: productMembers.values())
     {
      targetInsertUpdate.addAll(family.toInsert());   
      targetInsertUpdate.addAll(family.toUpdate());
      targetDelete.addAll(family.toDelete());
     }

     System.debug('Items to be Insert Updated:[' + targetInsertUpdate.size() +']' + targetInsertUpdate);
     System.debug('Items to be Deleted:[' + targetDelete.size() +']' + targetDelete);

     if(targetInsertUpdate.size() >0)
     {
      	Database.UpsertResult[] upsertResults = Database.upsert(targetInsertUpdate, false);
      	batchResult.addDmlResultData(upsertResults);
     }
     
     if(targetDelete.size() >0)
     {
        Database.DeleteResult[] deleteResults = Database.delete(targetDelete, false);
        batchResult.addDmlResultData(deleteResults);
     }
   }

     /* FAMILY BUSINESS MODEL */
   //Wrapper to contain data related to salesforce object Program_Family_Member__c
   public class FamilyMember{
    public String salesforceId;
    public String productExternalID;
    public String familyExternalID;
    public String regionalAttrExternalID;
    public Boolean isInStage = false;
    public Boolean isChanged = false;
    public Boolean pSell = false;
    public Boolean pSam= false;
    public Boolean alternativeVersion = false;
    public Boolean eLearning = false;
    public Boolean instructorResource = false;
    public Boolean mainTitle = false;
    public Boolean isPackage = false;
    public Boolean studentResource = false;

    public Boolean shouldDelete {get{ return (salesforceId !=null) && (!isInStage);}}

    public Boolean isNew {get{ return (salesforceId ==null) && isInStage;}}

   }


   public class FamilyRelationship
   {
      public Map<String, FamilyMember> parentToMember = new Map<String, FamilyMember>();

	  //Add Member wrapper into Family Wrapper populating it with specific family data
      public void addMember(FamilyMember member)
      {
        System.debug('addMember:' +member);
        //Parent of family member is already in list
        if(parentToMember.containsKey(member.productExternalID))
        {
            FamilyMember existingMember = parentToMember.get(member.productExternalID);
            existingMember.salesforceId = member.salesforceId != null ? member.salesforceId  : existingMember.salesforceId;
            existingMember.isInStage = member.isInStage || existingMember.isInStage;

			//Check if any data for member has been changed
            if(existingMember.pSell != member.pSell || existingMember.pSam != member.pSam || existingMember.alternativeVersion != member.alternativeVersion
            || existingMember.eLearning != member.eLearning || existingMember.instructorResource != member.instructorResource 
            || existingMember.mainTitle != member.mainTitle || existingMember.isPackage != member.isPackage || existingMember.studentResource != member.studentResource )
            {
                existingMember.pSell = member.pSell;
                existingMember.pSam = member.pSam;
            existingMember.alternativeVersion = member.alternativeVersion;
            existingMember.eLearning = member.eLearning;
            existingMember.instructorResource = member.instructorResource;
            existingMember.mainTitle = member.mainTitle;
            existingMember.isPackage = member.isPackage;
            existingMember.studentResource = member.studentResource;

                existingMember.isChanged = true;
            }

        }else
        {
            parentToMember.put(member.productExternalID,member);
        }
      }

	  //Generates list of Family Members which are deprected and should be removed
      public List<  Program_Family_Member__c> toDelete()
      {
        List<   Program_Family_Member__c> output = new List<    Program_Family_Member__c>();
        for(FamilyMember component: this.parentToMember.values())
        {
            if(component.shouldDelete)
            {
                output.add(new  Program_Family_Member__c(Id = component.salesforceId));
            }
        }
        return output;
      } 

	  //Generates list of family members which has been created during batch execution and should be inserted
      public List<Program_Family_Member__c> toInsert()
      {
        List<Program_Family_Member__c> output = new List<Program_Family_Member__c>();

        for(FamilyMember component: this.parentToMember.values())
        {
            if(component.isNew)
            {
                output.add(extractFamilyMember(component));
            }
        }

        return output;
      } 

	  //Generates list of family members which has been modified during batch execution and should be updated in database
      public List<Program_Family_Member__c> toUpdate()
      {
        List<Program_Family_Member__c> output = new List<Program_Family_Member__c>();

        for(FamilyMember component: this.parentToMember.values())
        {
            if(component.isChanged)
            {
                Program_Family_Member__c member = extractFamilyMember(component);
                member.Id = component.salesforceId;
                output.add(member);
                    
            }
        }

        return output;
      } 

	  //Create Salesforce Record Program_Family_Member__c from data saved in wrapper class
      private Program_Family_Member__c extractFamilyMember(FamilyMember component)
      {
        return new Program_Family_Member__c(
                    product__r = new Product2 (External_Product_ID__c = component.productExternalID),
                    MDM_Association_Category__c = 'N/A',
                    MDM_Alternative_Version__c = component.alternativeVersion,
                    MDM_eLearning__c = component.eLearning,
                    MDM_Instructor_Resource__c = component.instructorResource,
                    MDM_Main__c = component.mainTitle,
                    MDM_Package__c = component.isPackage,
                    MDM_Primary_Sampling__c = component.pSam,
                    MDM_Primary_Selling__c = component.pSell,
                    MDM_Student_Resource__c = component.studentResource,
                    Program_Family__r = new Product_Family__c(MDM_Integration_External_ID__c = component.familyExternalID),
                    MDM_Regional_Attributes__r = new Regional_Attributes__c(MDM_Integration_External_ID__c = component.regionalAttrExternalID )
                    );
      }
   }
}