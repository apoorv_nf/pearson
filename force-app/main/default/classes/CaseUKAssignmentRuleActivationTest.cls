/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            CaseUKAssignmentRuleActivationTest.cls 
   Description:     Test Class to cover code coverage PS_CaseServiceAssignRuleActivation
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
    12/9/2016         1.0            Payal Popat                       Initial Release 
------------------------------------------------------------------------------------------------------------------------------------------------------------- 
* Test Name                   :Call Assignment Rules
* Test Description            :Call Assignment Rules
* Expected Inputs             :Create case with Web,Emai.. and Escalation_Point__c is not tier 1
* Expected Outputs            :Depending on criteria tow different Assignments rules must call
------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class CaseUKAssignmentRuleActivationTest{
    
    static testMethod void mScenarioInsertCase(){
    
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,1);
        listWithUser[0].Market__c='UK';
        listWithUser[0].Line_of_Business__c='Core';
        
        
         

        //Account Creation start
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        lstAccount[0].Market2__c = 'UK';
        insert lstAccount ;
        //Account Creation 
        //Contact Creation start
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].MailingState='England';
        lstContact[0].Market__c = Label.PS_UKMarket;
        lstContact[0].Account = lstAccount[0];
        lstContact[0].AccountId = lstAccount[0].Id;
        
        insert lstContact;
        //Contact Creation 
        List<AccountContact__C> lstAcctCont = TestDataFactory.createAccountContact(1,lstAccount[0].Id,lstContact[0].Id);
        insert lstAcctCont;
        //Case Creation start
       // List<case> cases = TestDataFactory.createCase(1,'Technical Support');
      
         
     string recordType = 'Technical Support';
     //Case caseList = new Case();
     
   
     RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND Name =: recordType ];
     
     List<Contact> contactList = new List<Contact>();
     
     Contact contactRecord = new Contact(FirstName='Test', LastName='Test',
                                                Salutation='MR.', Email='Test' + '@email.com',Preferred_Address__c = 'Mailing Address',MailingCity = 'Newcastle', MailingState='Northumberland', MailingCountry='United Kingdom', MailingStreet='1st Street' , MailingPostalCode='NE28 7BJ', 
                                                MobilePhone  ='111222333',First_Language__c  ='English', accountid = lstAccount[0].id); 
    contactList.add(contactRecord);
     //Insert contactRecord;                                   
     Insert contactList;                                   
    
    
     
     
        Case cases = new Case(Type ='Technical Support',ContactId = contactList[0].id,Status ='In Progress',Priority='Low',Origin ='Post',Reason = 'financial',Returning_Tablet__c = 'No',
                                                 Sponsor_name__c = contactList[0].id,Tablet_Unique_Number__c = '123', RecordTypeId = rt.id, AccountId = lstAccount[0].Id );     
      
        cases.Account=lstAccount[0];
        cases.Contact=lstContact[0];
        cases.Escalation_Point__c = 'Tier 2';
        cases.Global_Country__c = 'United Kingdom';
        cases.Escalation_Reason__c='Test';
        cases.Market__c = Label.PS_UKMarket;
        Bypass_Settings__c settings = new Bypass_Settings__c();
         settings.Disable_Triggers__c = true;
         settings.SetupOwnerId = listWithUser[0].id;
         insert settings;  
        Test.StartTest();
        insert cases;
        System.assertEquals(cases.Escalation_Point__c,'Tier 2');
        Test.StopTest();
        //Case Creation Ends
    }
    static testMethod void mNegativeScenarioInsertCase(){
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,1);
        listWithUser[0].Market__c='UK';
        listWithUser[0].Line_of_Business__c='Core';
        //Account Creation start
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Organisation');
        lstAccount[0].Market2__c = 'UK';
        insert lstAccount ;
        //Account Creation 
        //Contact Creation start
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].MailingState='England';
        lstContact[0].Market__c = Label.PS_UKMarket;
        lstContact[0].Account = lstAccount[0];
        lstContact[0].AccountId = lstAccount[0].Id;
        insert lstContact;
        //Contact Creation 
        List<AccountContact__C> lstAcctCont = TestDataFactory.createAccountContact(1,lstAccount[0].Id,lstContact[0].Id);
        insert lstAcctCont;
        try{
            //Case Creation start
        string recordType = 'Technical Support';
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND Name =: recordType ];
        List<Contact> contactList = new List<Contact>();
        Contact contactRecord = new Contact(FirstName='Test', LastName='Test',
                                                Salutation='MR.', Email='Test' + '@email.com',Preferred_Address__c = 'Mailing Address',MailingCity = 'Newcastle', MailingState='Northumberland', MailingCountry='United Kingdom', MailingStreet='1st Street' , MailingPostalCode='NE28 7BJ', 
                                                MobilePhone  ='111222333',First_Language__c  ='English', accountid = lstAccount[0].id);
        contactList.add(contactRecord);
        Insert contactList;
        Case cases = new Case(Type ='Technical Support',ContactId = contactList[0].id,Status ='New',Priority='Low',Origin ='Post',Reason = 'financial',Returning_Tablet__c = 'No',
                                                 Sponsor_name__c = contactList[0].id,Tablet_Unique_Number__c = '123', RecordTypeId = rt.id, AccountId = lstAccount[0].Id );     
      
            cases.Contact=lstContact[0];
            cases.Escalation_Point__c = 'Tier 2';
            cases.Global_Country__c = 'United Kingdom';
            cases.Escalation_Reason__c='Test';
            cases.Market__c = Label.PS_UKMarket;
            Bypass_Settings__c settings = new Bypass_Settings__c();
            settings.Disable_Triggers__c = true;
            settings.SetupOwnerId = listWithUser[0].id;
            insert settings;
            Test.StartTest();
            insert cases;
            System.assertEquals(cases.Escalation_Point__c,'Tier 2');
            List <Case> caseList = new List <Case>();
            caseList.add(cases);
            system.runAs(listWithUser[0]){
                CaseUKAssignmentRuleActivation.mAssignEscalationCase(caseList,null);
            }
            Test.StopTest();
            //Case Creation Ends
        }catch (DmlException e) {
           //Assert Error Message            
           System.AssertEquals(e.getMessage(), e.getMessage());
         } 
    }
    static testMethod void mScenarioInsertEmailCase(){
        List<User> listWithUser = new List<User>();
        listWithUser  = TestDataFactory.createUser([select Id from Profile where Name =: 'Pearson Service Cloud User OneCRM'].Id,1);
        listWithUser[0].Market__c='UK';
        listWithUser[0].Line_of_Business__c='Core';
        //Account Creation start
        List<Account> lstAccount = TestDataFactory.createAccount(1,'Account Creation Request');
        lstAccount[0].Market2__c = System.Label.PS_UKMarket;
        insert lstAccount ;
        //Account Creation 
        //Contact Creation start
        List<Contact> lstContact = TestDataFactory.createContacts(1);
        lstContact[0].MailingState='England';
        lstContact[0].Market__c = Label.PS_UKMarket;
        lstContact[0].Account = lstAccount[0];
        lstContact[0].AccountId = lstAccount[0].Id;
        insert lstContact;
        //Contact Creation 
        List<AccountContact__C> lstAcctCont = TestDataFactory.createAccountContact(1,lstAccount[0].Id,lstContact[0].Id);
        insert lstAcctCont;
        //Case Creation start
      //  List<case> cases = TestDataFactory.createCase(1,'Technical Support');
      string recordType = 'Technical Support';
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND Name =: recordType ];
        List<Contact> contactList = new List<Contact>();
        Contact contactRecord = new Contact(FirstName='Test', LastName='Test',
                                                Salutation='MR.', Email='Test' + '@email.com',Preferred_Address__c = 'Mailing Address',MailingCity = 'Newcastle', MailingState='Northumberland', MailingCountry='United Kingdom', MailingStreet='1st Street' , MailingPostalCode='NE28 7BJ', 
                                                MobilePhone  ='111222333',First_Language__c  ='English', accountid = lstAccount[0].id);
        contactList.add(contactRecord);
        Insert contactList;
        Case cases = new Case(Type ='Technical Support',ContactId = contactList[0].id,Status ='New',Priority='Low',Origin ='Post',Reason = 'financial',Returning_Tablet__c = 'No',
                                                 Sponsor_name__c = contactList[0].id,Tablet_Unique_Number__c = '123', RecordTypeId = rt.id, AccountId = lstAccount[0].Id );     
      
        cases.Account=lstAccount[0];
        cases.Contact=lstContact[0];
        cases.Global_Country__c = 'United Kingdom';
        cases.Origin = 'Web';
        cases.Request_Type__c='Primary Digital';
        cases.Market__c = Label.PS_UKMarket;
        Bypass_Settings__c settings = new Bypass_Settings__c();
            settings.Disable_Triggers__c = true;
            settings.SetupOwnerId = listWithUser[0].id;
            insert settings;
        insert cases;
         List <Case> caseList = new List <Case>();
            caseList.add(cases);
        
        Test.StartTest();
        Map<Id,Case> oldMap = new Map<Id,Case>();
        
        //System.runAs(listWithUser[0]){
        for(case cs:caseList){
            cs.Escalation_Point__c = 'Tier 2';
            cs.Escalation_Reason__c='Test';
            cs.Request_Type__c='Clinical';
            oldMap.put(cs.Id,cs);
        }   
            CaseUKAssignmentRuleActivation.mAssignEscalationCase(caseList,oldMap);
            CaseUKAssignmentRuleActivation.mActivateUKAssignmentRule(caseList);
        //}
        System.assertEquals(cases.Escalation_Point__c,'Tier 2');
        Test.StopTest();
        //Case Creation Ends
    }
    
}