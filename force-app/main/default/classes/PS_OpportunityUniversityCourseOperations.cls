/************************************************************************************************************
* Apex Class Name   : PS_OpportunityUniversityCourseOperations.cls
* Version           : 1.0 
* Created Date      : 12 Oct 2015
* Function          : Operation class for OpportunityCourse Object Trigger
* Additional Info   : All the Event methods shall remain empty , Infuture required logic can be added to respective event methods
            
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Leonard Victor           12/10/2015               Operations Class for OpportunityCourse
************************************************************************************************************/


public without sharing class PS_OpportunityUniversityCourseOperations implements PS_GenricTriggerInterface {

    public void beforeInsert(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew) {
      
            
    }
    
    public void afterInsert(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew) {
            Set<Id> opptyIdSet = new Set<Id>();
            //PCC from course mapping to Opportunity - This logic fires on Afterinsert when new course is mapped to opportunity
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            //SOQL to Avoid logic execution for US record types
            ////////
            List<OpportunityUniversityCourse__c> lstOpptyCourse = [Select UniversityCourse__c,Opportunity__c , Opportunity__r.Recordtype.Name from OpportunityUniversityCourse__c where id in:lTriggerNew and Opportunity__r.Recordtype.Name!=:PS_Constants.OPPORTUNITY_B2B_RECCORD ];
            
            ///////
            Map<Id,List<Id>> courseOpptyMap = new Map<Id,List<Id>>();
            List<Opportunity_Pearson_Course_Code__c> lstCourseEqu = new List<Opportunity_Pearson_Course_Code__c>();
            for(OpportunityUniversityCourse__c opptCourseObj : lstOpptyCourse){
                   if(courseOpptyMap.containsKey(opptCourseObj.UniversityCourse__c)){
                        courseOpptyMap.get(opptCourseObj.UniversityCourse__c).add(opptCourseObj.Opportunity__c);
                   }else{
                        List<Id> tempLst = new List<Id>();
                        tempLst.add(opptCourseObj.Opportunity__c);
                        courseOpptyMap.put(opptCourseObj.UniversityCourse__c,tempLst);
                    }
                    opptyIdSet.add(opptCourseObj.Opportunity__c);
            }

            if(!courseOpptyMap.isEmpty()){
                List<Pearson_Course_Equivalent__c> lstPcc= [Select id,Course__c,DM_Identifier__c,Pearson_Course_Code_Hierarchy__c/*Pearson_Course_Code__c*/,Pearson_Course_Code_Name_f__c,Primary__c,Active__c from Pearson_Course_Equivalent__c where Course__c in:courseOpptyMap.keySet()];
                //Logic for Restricting Duplicate Pearson Course Code
                List<Opportunity_Pearson_Course_Code__c> lstOpptyPcc= [Select id,Pearson_Course_Code_Hierarchy__c /*Pearson_Course_Code_Name__c*/,Opportunity__c from Opportunity_Pearson_Course_Code__c where Opportunity__c in:opptyIdSet];
                Map<Id,Set<Id>> opptyCCMap = new Map<Id,Set<Id>>();
                for(Opportunity_Pearson_Course_Code__c opptyCCObj : lstOpptyPcc){
                    if(opptyCCMap.containsKey(opptyCCObj.Opportunity__c)){
                        //opptyCCMap.get(opptyCCObj.Opportunity__c).add(opptyCCObj.Pearson_Course_Code_Name__c);
                        opptyCCMap.get(opptyCCObj.Opportunity__c).add(opptyCCObj.Pearson_Course_Code_Hierarchy__c);
                    }
                    else{
                        Set<Id> tempCCSet = new Set<Id>();
                        //tempCCSet.add(opptyCCObj.Pearson_Course_Code_Name__c); Replaced the Apttus to Non-Apttus Component
                        tempCCSet.add(opptyCCObj.Pearson_Course_Code_Hierarchy__c);
                        opptyCCMap.put(opptyCCObj.Opportunity__c , tempCCSet);
                    }
                    
                    
                }
                //End of Logic for Duplicate Course Code
                if(!lstPcc.isEmpty()){
                    for(Pearson_Course_Equivalent__c equvObj: lstPcc){
                         if(courseOpptyMap.containsKey(equvObj.Course__c)){
                             for(Id oppId:courseOpptyMap.get(equvObj.Course__c)){
                                     //if(!opptyCCMap.containsKey(oppId) || (opptyCCMap.containsKey(oppId) && !opptyCCMap.get(oppId).contains(equvObj.Pearson_Course_Code__c))){
                                     if(!opptyCCMap.containsKey(oppId) || (opptyCCMap.containsKey(oppId) && !opptyCCMap.get(oppId).contains(equvObj.Pearson_Course_Code_Hierarchy__c))){
                                         Opportunity_Pearson_Course_Code__c temp_oppty_Pearson_Course_Codes = new Opportunity_Pearson_Course_Code__c();
                                         temp_oppty_Pearson_Course_Codes.Opportunity__c = oppId;
                                         //temp_oppty_Pearson_Course_Codes.Pearson_Course_Code_Name__c = equvObj.Pearson_Course_Code__c; Replaced the Apttus To Non-Apttus
                                         temp_oppty_Pearson_Course_Codes.Pearson_Course_Code_Hierarchy__c = equvObj.Pearson_Course_Code_Hierarchy__c;
                                         temp_oppty_Pearson_Course_Codes.Primary__c = equvObj.Primary__c;
                                         temp_oppty_Pearson_Course_Codes.Active__c =equvObj.Active__c;
                                         lstCourseEqu.add(temp_oppty_Pearson_Course_Codes);
                                     }
                             }
                         }

                    }
                    if(!lstCourseEqu.isEmpty()){
                        try{
                            Database.SaveResult[] lstResult= Database.insert(lstCourseEqu, false);
                             if (!lstResult.isEmpty()){
                                        List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
                                        for(Database.SaveResult resultObj:lstResult){
                                            String ErrMsg='';
                                            if (!resultObj.isSuccess()){
                                                    PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c(InterfaceName__c='Course Oppty Action',ApexClassName__c='PS_CreateOpportunityCourseAction',CallingMethod__c='saveOppty',UserLogin__c=UserInfo.getUserName(),recordid__c=resultObj.getId());
                                                    /*errlogger.InterfaceName__c='Course Oppty Action';
                                                    errlogger.ApexClassName__c='PS_CreateOpportunityCourseAction';
                                                    errlogger.CallingMethod__c='saveOppty';
                                                    errlogger.UserLogin__c=UserInfo.getUserName(); 
                                                    errlogger.recordid__c=resultObj.getId();*/
                                                    for(Database.Error err : resultObj.getErrors())
                                                          ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage(); 
                                                    errlogger.ExceptionMessage__c=  ErrMsg;  
                                                    errloggerlist.add(errlogger);    
                                            }
                                        }
                                        if(errloggerlist.size()>0){
                                                insert errloggerlist;
                                        }
                             }
                            
                        }catch(Exception e){
                              ExceptionFramework.LogException('CRITICAL','PS_OpportunityUniversityCourseOperations','afterInsert - Create Course Equvivalent',e.getMessage(),UserInfo.getUserName(),'');
                              System.debug('-->Error:'+ e);
                            
                        }
                    }


             }
          }
    
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //End of PCC mapping to opportunity
  }
    
    public void beforeUpdate(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) {
  
  }
    
    public static void afterUpdate(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) {
  
  }
    
    public void beforeDelete(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) {
        
    }
    
    public void afterDelete(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew, List<sObject> lTriggerOld, Map<Id, sObject> mTriggerOld) {
        
    }
    
    public void afterUndelete(List<sObject> lTriggerNew, Map<Id, sObject> mTriggerNew) {
        
    }
}