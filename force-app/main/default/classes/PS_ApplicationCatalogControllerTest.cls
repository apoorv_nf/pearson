/************************************************************************************************************
* Apex Interface Name : PS_ApplicationCatalogControllerTest
* Version             : 1.1 
* Created Date        : 11-Feb-2016 
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Hemangini                  11-Feb-2016             
* Rohit Kulkarni         04/03/2016
-------------------------------------------------------------------------------------------------------------
* Test Name                   :Display Links BY Application Catalog
* Test Description            :Display Links BY Application Catalog
* Expected Inputs             :Pass parameters like Application name,static parameters,profile
* Expected Outputs            :Call the AppLauncher to dsplay page in new tab depending profile

-------------------------------------------------------------------------------------------------------------

* Modified by Manikanta Nagubilli on 13/10/2016 for INC2926449 / CR-00965 / MGI -> PIHE(Modified Lines- 38,75,121) 
************************************************************************************************************/
@isTest
public class PS_ApplicationCatalogControllerTest{
    
    /*
     * Method to cover the scenario where Exception occurs for getApps() method
     */
    static testMethod void test_getApps_Exception(){
        User usr;
        List<User> userList = new List<User>();
        UserRole tempUserRole;
        string profileNameStr='Pearson Service Cloud User OneCRM';
        List<AppCatalog__c> appCatalogList = new List<AppCatalog__c>();
        
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error and User validation errors
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){
            userList  = TestDataFactory.createUser([select Id from Profile where Name =: profileNameStr].Id,1);
            
            //DATA CREATION: for getApps() method -START
            appCatalogList.add( new AppCatalog__c(name='First', Catalog__c='Catalog', APPLICATIONNAME__c='Application Catalog', PS_ApplicationCatalogController__c='PS_ApplicationCatalogController', fetchApplicationcatalogUrls__c='fetchApplicationcatalogUrls'));
            insert appCatalogList;
            //DATA CREATION: for getApps() method -ENDS
        }
        
        System.runAs(userList[0]){
            Test.startTest();
            PS_ApplicationCatalogController appCatalogObj = new PS_ApplicationCatalogController();
            appCatalogObj.getApps();
            System.assertEquals(userList[0].Email,'h@gmail.com');
            Test.stopTest();
        }
    }
    
    /*
     * Method to cover the getApps() method
     */
    static testMethod void test_getApps(){
        User usr;
        List<User> userList = new List<User>();
        UserRole tempUserRole;
        string profileNameStr='Pearson Service Cloud User OneCRM';
        List<AppCatalog__c> appCatalogList = new List<AppCatalog__c>();
        List<ACURLLimit__c> acurlLimitList = new List<ACURLLimit__c>();
        List<Application__c> appList = new List<Application__c>();
        
        //user role to avoid 'portal account owner must have a role' error
        tempUserRole = new UserRole(Name='CEO');
        insert tempUserRole;
        
        //user to avoid Mixed DML error and User validation errors
        usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
        
        System.runAs(usr){
            //'Pearson Service Cloud User OneCRM' profile user
            userList  = TestDataFactory.createUser([select Id from Profile where Name =: profileNameStr].Id,1);
            
            //DATA CREATION: for getApps() method -START
            appCatalogList.add( new AppCatalog__c(name='First', Catalog__c='Catalog', APPLICATIONNAME__c='Application Catalog', PS_ApplicationCatalogController__c='PS_ApplicationCatalogController', fetchApplicationcatalogUrls__c='fetchApplicationcatalogUrls'));
            insert appCatalogList;
            
            acurlLimitList.add( new ACURLLimit__c(name='Catalog', Limit__c=20));
            insert acurlLimitList;
            
            appList.add(new Application__c(URL__c='Test URL', App_Label__c='Test Label', App_Type__c='External', Visible_On_application_Catalog__c = true));
            appList.add(new Application__c(URL__c='Test URL1', App_Label__c='Test Label1', App_Type__c='External1', Visible_On_application_Catalog__c = true));
            insert appList;
            //DATA CREATION: for getApps() method -END
        }
        
        System.runAs(userList[0]){
            Test.startTest();
            PS_ApplicationCatalogController appCatalogObj = new PS_ApplicationCatalogController();
            appCatalogObj.getApps();
            System.assertEquals(userList[0].Email,'h@gmail.com');
            Test.stopTest();
        }
    }
    
    
        
    /*
     * Method to cover Negative scenario where Exception occurs for getApps() method
     */
    static testMethod void Negative_test_getApps_Exception(){
        try{
            User usr;
            List<User> userList = new List<User>();
            UserRole tempUserRole;
            string profileNameStr='Pearson Service Cloud User OneCRM';
            List<AppCatalog__c> appCatalogList = new List<AppCatalog__c>();
            
            //user role to avoid 'portal account owner must have a role' error
            tempUserRole = new UserRole(Name='CEO');
            insert tempUserRole;
            
            //user to avoid Mixed DML error and User validation errors
            usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Market__c = 'US', Line_of_Business__c = 'Higher Ed', Business_Unit__c = 'CTIPIHE', Price_List__c= 'Math & Science', UserRoleId=tempUserRole.Id);
            
            System.runAs(usr){
                userList  = TestDataFactory.createUser([select Id from Profile where Name =: profileNameStr].Id,1);
                
                //DATA CREATION: for getApps() method -START
                appCatalogList.add( new AppCatalog__c(Catalog__c='Catalog', APPLICATIONNAME__c='Application Catalog', PS_ApplicationCatalogController__c='PS_ApplicationCatalogController', fetchApplicationcatalogUrls__c='fetchApplicationcatalogUrls'));
                insert appCatalogList;
                //DATA CREATION: for getApps() method -ENDS
            }
            
            System.runAs(userList[0]){
                Test.startTest();
                PS_ApplicationCatalogController appCatalogObj = new PS_ApplicationCatalogController();
                appCatalogObj.getApps();
                System.assertEquals(appCatalogList[0].Catalog__c,'');
                Test.stopTest();
            }
        }
        catch (DmlException e) {
           //Assert Error Message            
            system.debug('case exception'+e.getMessage());
            Boolean expectedExceptionThrown =  e.getMessage().contains('Insert failed. First exception on row 0; first error: REQUIRED_FIELD_MISSING, Required fields are missing: [Name]: [Name]') ? true : false;
            System.AssertEquals(expectedExceptionThrown,true);
            System.assertEquals('REQUIRED_FIELD_MISSING' , e.getDmlStatusCode(0));
            
        }
        
    }
    
}