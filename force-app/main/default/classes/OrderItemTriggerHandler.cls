/************************************************************************************************************
* Apex Class Name   : OrderItemTriggerHandler.cls
* Version           : 1.1 
* Created Date      : 26 May 2015
* Function          : Handler class for StandarOrder Object Product(OrderItem)  Trigger 
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Davi Borges         13 Jul 2015                   Inclusion of validation framework
* Rahul Boinepally    24 Sep 2015                   Do not to call PS_INT_IntegrationRequestController if 
Supress Integration flag is True.   
* Rony Joseph         04 Nov 2015                   Defect Fix for New Status of Orders
* Rahul Boinepally    26th Jan 2016                 Defect D-3706. Populate start date of Order Product with 
*                                                   current date. 
* RG                  28/04/2016                    called a class PS_mappingShippedProductISBN10 in before update logic 
*Asha G               11/04/2018                    Created new function to update orderline item status to "open" if all of the order line items status changed to "Approved"  for CR-2023
************************************************************************************************************/

public without sharing class OrderItemTriggerHandler
{
    
    
    // EXECUTE BEFORE INSERT LOGIC
    //
    public void OnBeforeInsert(list<orderitem> orderitems)
    {
        
        map<id,orderitem> orderitemids =  new map<id,orderitem>();
        map<id,id> pricebooktoprodidmap= new map<id,id>();
        for(orderitem indorderitem:orderitems)
        {
            if(indorderitem.Shipped_Product__c == null)                    
            { 
                orderitemids.put(indorderitem.Pricebookentryid,indorderitem);
            }
        }
        if(orderitemids.size()>0)
        {        
            list<pricebookentry> pricebookentrylist = [select id,Product2Id from Pricebookentry where id in :orderitemids.keyset()];
            for(pricebookentry pbe:pricebookentrylist){  pricebooktoprodidmap.put(pbe.id,pbe.product2id);  }
            /* for(id orderitem:orderitemids.keyset())
{
orderitemids.get(orderitem).Shipped_Product__c = pricebooktoprodidmap.get(orderitem);
}    */
            for(orderitem indorderitem:orderitems){
                if(indorderitem.Shipped_Product__c == null){ 
                    //orderitemids.put(indorderitem.Pricebookentryid,indorderitem);
                    if (pricebooktoprodidmap.containsKey(indorderitem.Pricebookentryid))
                        indorderitem.Shipped_Product__c=pricebooktoprodidmap.get(indorderitem.Pricebookentryid);    
                }
            }           
        }
        
        Map<String, OrderItem> mapNewOrderItem = createListMap(orderitems);
        
        List<PS_OrderItemValidationInterface> validations = PS_OrderItemValidationFactory.CreateValidations(mapNewOrderItem , null, new User(Id=UserInfo.getUserId()));
        
        Map<String,List<String>> exceptions = new Map<String,List<String>>();
        
        for(PS_OrderItemValidationInterface validation: validations)
        {
            validation.validateInsert(exceptions);
        }
        
        for(OrderItem orderFor: orderitems )
        {            
            if(orderFor.ServiceDate != null)
                orderFor.ServiceDate = System.Today();
        }
        
        
    }
    
    // EXECUTE AFTER INSERT LOGIC
    //
    public void OnAfterInsert(list<orderitem> orderitems)
    {
        
    }
    
    // BEFORE UPDATE LOGIC
    //
    public void OnBeforeUpdate(Map<Id,OrderItem> mapNewOrderProduct, Map<Id,OrderItem> mapOldOrderProduct)
    {
        try
        {
            PS_MappingShippedProduct SpISBN10 = new PS_MappingShippedProduct();
            SpISBN10.mappShippedProduct(mapNewOrderProduct,mapOldOrderProduct);         
            Map<String, OrderItem> mapNewOrderItem = createListMap(mapNewOrderProduct);
            
            List<PS_OrderItemValidationInterface> validations = PS_OrderItemValidationFactory.CreateValidations(mapNewOrderItem , mapOldOrderProduct, new User(Id=UserInfo.getUserId()));
            
            
            Map<String,List<String>> exceptions = new Map<String,List<String>>();
            
            for(PS_OrderItemValidationInterface validation: validations)
            {
                validation.validateUpdate(exceptions);
            }
            
            if( ! exceptions.isEmpty())
            {
                PS_Util.addErrors(mapNewOrderItem , exceptions);
                
                return;
            }
        }catch(exception e){
            string errormessage=e.getMessage()+e.getStackTraceString();
            ExceptionFramework.LogException('OrderLineItem Update','OrderItemTriggerHandler','OnBeforeUpdate',errormessage,UserInfo.getUserName(),'');           
        } 
        
    }
    
    // AFTER UPDATE LOGIC
    //
    public void OnAfterUpdate(List<OrderItem> orderItems, Map<Id, OrderItem> orderItemMap,Map<id, OrderItem> OrderOldMap, List<orderitem> orderoldlist)
    { 
        orderItems = (List<OrderItem>)checkRecurssion.filterNotExecutedRecords(orderItems);
        set<id> orderids = new set<id>();
        orderItemMap = new Map<Id,OrderItem>(orderItems);
        if (orderItems.size() > 0) {
            PS_AssetUtilities.updateRelatedAssetStatus(orderItemMap);
            
            // Start change - Dont call PS_INT_IntegrationRequestController if supress integration flag is True (Tag - T1)
            List<Orderitem> intList = new List<Orderitem>();
            
            for(OrderItem checkFlag:orderitems)
            {
                if (checkFlag.Supress_Notification__c != True) 
                {
                    
                    intList.add(checkFlag);
                }     
              if((checkflag.Status__c != OrderOldMap.get(checkflag.id).status__C) && (checkflag.Status__c=='Approved' || checkflag.Status__c=='Cancelled') )
                {
                    orderids.add(checkflag.Orderid);                
                }            
                if(!orderids.isempty() && checkflag.market__c== Label.PS_USMarket)
                {
                    updateorders(orderids);
                } 
                
            }
            //PS_INT_IntegrationRequestController.createIntegrationRequestOrderLine(intList,Trigger.oldMap);
            // End of change.
            
            
           List<PS_OrderUpdateInterface> updates = PS_OrderUpdateFactory.createOrderUpdates(orderitems);
            for(PS_OrderUpdateInterface updateInterface: updates)
            {
                updateInterface.updateOrders();
            }
        }
    }
    
    
    // BEFORE DELETE LOGIC
    //
    public void OnBeforeDelete(Map<Id,OrderItem> mapNewOrderProduct, Map<Id,OrderItem> mapOldOrderProduct)
    {
        Map<String, OrderItem> mapNewOrderItem = createListMap(mapOldOrderProduct);
        
        List<PS_OrderItemValidationInterface> validations = PS_OrderItemValidationFactory.CreateValidations(mapNewOrderItem , mapOldOrderProduct, new User(Id=UserInfo.getUserId()));
        
        
        Map<String,List<String>> exceptions = new Map<String,List<String>>();
        
        for(PS_OrderItemValidationInterface validation: validations)
        {
            validation.validateDelete(exceptions);
        }
        
        if( ! exceptions.isEmpty())
        {
            PS_Util.addErrors(mapNewOrderItem , exceptions);
            
            return;
        }
    }
    
    // AFTER DELETE LOGIC
    //
    public void OnAfterDelete(list<orderitem> orderitems)
    {
        
    }
    
    // AFTER UNDELETE LOGIC
    //
    public void OnUndelete(list<orderitem> orderitems)
    {
        
    }
    
    public static Map<String,OrderItem> createListMap(Map<Id,OrderItem> objs)
    {
        Map<String,OrderItem> output = new  Map<String,OrderItem>();
        
        for(Id id: objs.keySet())
        {
            output.put(id,objs.get(id));
        }
        
        return output;
    }
    
    public static Map<String,OrderItem> createListMap(List<OrderItem> objs)
    {
        Map<String,OrderItem> output = new  Map<String,OrderItem>();
        
        for(Integer i =0 ; i< objs.size(); i++)
        {
            output.put(String.valueOf(i), objs[i]);
        }
        
        return output;
    }
    /**Created new function to update order status to "open" if all of the order line items status changed to "Approved"  starts for CR-2023**/
    Public static void updateorders(set<id> orders)
    {
        list<order> orderupdatefinal=new list<order>();
        for(order ord:[select id,status,(select id,status__c from OrderItems where status__c='Requires Approval' or  status__c ='Approved' or status__c ='Entered') from order where id in :orders])
        {
            integer approvedcount=0;
            integer requirescount=0;
            integer Enteredcount=0;
            for(orderitem ordit:ord.orderitems)
            {
                if(ordit.status__c=='Requires Approval')
                {
                    requirescount++;
                } 
                
                if(ordit.status__c=='Approved')
                {
                    approvedcount++;
                }
                
                if(ordit.status__c=='Entered')
                {
                    Enteredcount++;
                }     
            }
            
            if((requirescount==0 && approvedcount>0) || (requirescount==0 && Enteredcount>0) )
            {
               
                ord.status='Open';
                orderupdatefinal.add(ord);
                
            }
        }
        if(!orderupdatefinal.isempty())
        {
            update orderupdatefinal; 
        }
    }
    /**Created new function to update orderline item status to "open" if all of the order line items status changed to "Approved"  Ends for CR-2023***/
}