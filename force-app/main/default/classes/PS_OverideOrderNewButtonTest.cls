/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            PS_OverideOrderNewButtonTest
   
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
  09/11/2015         1.0            Accenture IDC                       Initial Release
  06/01/2015         1.1            Karan Khanna                        Created myUnitTest2() to increase the test coverage 
------------------------------------------------------------------------------------------------------------------------------------------------------------ */
@isTest
private Class PS_OverideOrderNewButtonTest {
           
    static testMethod void orderNewButtonTestMethod1() {
        LeadConversionChecker.inLeadConversion = true;
        Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
        User u = new User();
        u.LastName = 'territoryuser';
        u.alias = 'terrusr'; 
        u.Email = 'territoryuser@pearson.com';  
        u.Username='territoryuser@gmail1.com';
        u.LanguageLocaleKey='en_US'; 
        u.TimeZoneSidKey='America/New_York';
        u.Price_List__c='US HE All';
        u.LocaleSidKey='en_US';
        u.EmailEncodingKey='ISO-8859-1';
        u.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
        u.Geography__c = 'Growth';
        u.CurrencyIsoCode='USD';
        u.Line_of_Business__c = 'Higher Ed';
        u.Market__c = 'CA';
        u.Business_Unit__c = 'US Field Sales';
        u.License_Pools__c = 'Test User';
        insert u;
       // system.debug(u+'***MY USER ID*****');
        
        General_One_CRM_Settings__c gocs = new General_One_CRM_Settings__c();
        gocs.value__c='Pearson_Backend_Order_Creation,Pearson_Global_Backend_Sample,Pearson_Global_Backend_Revenue,Pearson_CS_Order_Entry_Team,Pearson_CS_Agent,Pearson_CS_Manager,Pearson_CS_Team_Lead';
        gocs.Name = 'OrderOverrideNew';
        gocs.category__c = 'Order';
        gocs.Description__c = 'OrderOverrideNew';
        System.runAs(u){
        insert gocs;
        }
        List<Account> lstAccount = TestDataFactory.createAccount(15,'Organization');
        lstAccount[0].Market2__c = 'US';
        lstAccount[0].Line_of_Business__c = 'Higher Ed';
        lstAccount[0].Geography__c = 'Core';        
        lstAccount[0].Market2__c = 'US';
        lstAccount[0].Geography__c = 'Core';
        System.runAs(u){
        insert lstAccount;          
        }
        
        List<Contact> lstContacts = TestDataFactory.createContacts(1) ;
        lstContacts[0].accountId = lstAccount[0].id;
        lstContacts[0].Preferred_Address__c='Mailing Address';        
        System.runAs(u){
        insert lstContacts;
        }
        
        List<AccountContact__c> lstaccountcontact = TestDataFactory.createAccountContact(2, lstAccount[0].id, lstContacts[0].id);
        lstaccountcontact[0].Primary__c = true;
        System.runAs(u){            
        insert lstaccountcontact ;
        }
        // lstaccountcontact[0].Account__c = lstAccount[0].id;
        //update lstaccountcontact[0];
        System.runAs(u){
            Test.startTest(); 
            ApexPages.currentPage().getParameters().put('retURL',lstAccount[0].id);
            ApexPages.currentPage().getParameters().put('aid',lstAccount[0].id);
            PS_OverideOrderNewButton  ctrl = new PS_OverideOrderNewButton (new ApexPages.StandardController(new order()));
//            ApexPages.currentPage().getParameters().put('retURL',lstAccount[0].id);
//            ApexPages.currentPage().getParameters().put('aid',lstAccount[0].id);
            ctrl.orderRec.Order_Address_Type__c = 'Account';
            ctrl.redirectMethod();
            ctrl.retURL = '/'+lstContacts[0].Id;
            Permissionset ps = [Select id,name from Permissionset where Name = 'Pearson_CS_Order_Entry_Team'];
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.PermissionSetId = ps.id;
            psa.AssigneeId = u.id;
            insert psa;
            ctrl.OverideOrderNewButton();
            ctrl.retURL = '/'+lstAccount[0].Id;
            ctrl.OverideOrderNewButton();
            Test.stopTest();
        }
    }
        static testMethod void orderNewButtonTestMethod2() {
        LeadConversionChecker.inLeadConversion = true;
        Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
        User u = new User();
        u.LastName = 'territoryuser';
        u.alias = 'terrusr'; 
        u.Email = 'territoryuser@pearson.com';  
        u.Username='territoryuser@gmail1.com';
        u.LanguageLocaleKey='en_US'; 
        u.TimeZoneSidKey='America/New_York';
        u.Price_List__c='US HE All';
        u.LocaleSidKey='en_US';
        u.EmailEncodingKey='ISO-8859-1';
        u.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
        u.Geography__c = 'Growth';
        u.CurrencyIsoCode='USD';
        u.Line_of_Business__c = 'Higher Ed';
        u.Market__c = 'CA';
        u.Business_Unit__c = 'US Field Sales';
        u.License_Pools__c = 'Test User';
        insert u;
       // system.debug(u+'***MY USER ID*****');
        
        General_One_CRM_Settings__c gocs = new General_One_CRM_Settings__c();
        gocs.value__c='Pearson_Backend_Order_Creation,Pearson_Global_Backend_Sample,Pearson_Global_Backend_Revenue,Pearson_CS_Order_Entry_Team,Pearson_CS_Agent,Pearson_CS_Manager,Pearson_CS_Team_Lead';
        gocs.Name = 'OrderOverrideNew';
        gocs.category__c = 'Order';
        gocs.Description__c = 'OrderOverrideNew';
        System.runAs(u){
        insert gocs;
        }
        List<Account> lstAccount = TestDataFactory.createAccount(15,'Organization');
        lstAccount[0].Market2__c = 'US';
        lstAccount[0].Line_of_Business__c = 'Higher Ed';
        lstAccount[0].Geography__c = 'Core';        
        lstAccount[0].Market2__c = 'US';
        lstAccount[0].Geography__c = 'Core';
        System.runAs(u){
        insert lstAccount;          
        }
        
        List<Contact> lstContacts = TestDataFactory.createContacts(1) ;
        lstContacts[0].accountId = lstAccount[0].id;
        lstContacts[0].Preferred_Address__c='Mailing Address';        
        System.runAs(u){
        insert lstContacts;
        }
        
        List<AccountContact__c> lstaccountcontact = TestDataFactory.createAccountContact(2, lstAccount[0].id, lstContacts[0].id);
        lstaccountcontact[0].Primary__c = true;
        System.runAs(u){            
        insert lstaccountcontact ;
        }
        // lstaccountcontact[0].Account__c = lstAccount[0].id;
        //update lstaccountcontact[0];
        System.runAs(u){
            Test.startTest();
            ApexPages.currentPage().getParameters().put('retURL',lstAccount[0].id);
            ApexPages.currentPage().getParameters().put('aid',lstAccount[0].id);
            PS_OverideOrderNewButton  ctrl = new PS_OverideOrderNewButton (new ApexPages.StandardController(new order()));
//            ApexPages.currentPage().getParameters().put('retURL',lstAccount[0].id);
//            ApexPages.currentPage().getParameters().put('aid',lstAccount[0].id);
            ctrl.orderRec.Order_Address_Type__c = 'Contact Other';
            ctrl.redirectMethod();
            ctrl.retURL = '/'+lstContacts[0].Id;
            Permissionset ps = [Select id,name from Permissionset where Name = 'Pearson_CS_Order_Entry_Team'];
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.PermissionSetId = ps.id;
            psa.AssigneeId = u.id;
            insert psa;
            ctrl.OverideOrderNewButton();
            ctrl.retURL = '/'+lstAccount[0].Id;
            ctrl.OverideOrderNewButton();
            Test.stopTest();
        }
    }
        static testMethod void orderNewButtonTestMethod3() {
        LeadConversionChecker.inLeadConversion = true;
        Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
        User u = new User();
        u.LastName = 'territoryuser';
        u.alias = 'terrusr'; 
        u.Email = 'territoryuser@pearson.com';  
        u.Username='territoryuser@gmail1.com';
        u.LanguageLocaleKey='en_US'; 
        u.TimeZoneSidKey='America/New_York';
        u.Price_List__c='US HE All';
        u.LocaleSidKey='en_US';
        u.EmailEncodingKey='ISO-8859-1';
        u.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
        u.Geography__c = 'Growth';
        u.CurrencyIsoCode='USD';
        u.Line_of_Business__c = 'Higher Ed';
        u.Market__c = 'CA';
        u.Business_Unit__c = 'US Field Sales';
        u.License_Pools__c = 'Test User';
        insert u;
       // system.debug(u+'***MY USER ID*****');
        
        General_One_CRM_Settings__c gocs = new General_One_CRM_Settings__c();
        gocs.value__c='Pearson_Backend_Order_Creation,Pearson_Global_Backend_Sample,Pearson_Global_Backend_Revenue,Pearson_CS_Order_Entry_Team,Pearson_CS_Agent,Pearson_CS_Manager,Pearson_CS_Team_Lead';
        gocs.Name = 'OrderOverrideNew';
        gocs.category__c = 'Order';
        gocs.Description__c = 'OrderOverrideNew';
        System.runAs(u){
        insert gocs;
        }
        List<Account> lstAccount = TestDataFactory.createAccount(15,'Organization');
        lstAccount[0].Market2__c = 'US';
        lstAccount[0].Line_of_Business__c = 'Higher Ed';
        lstAccount[0].Geography__c = 'Core';        
        lstAccount[0].Market2__c = 'US';
        lstAccount[0].Geography__c = 'Core';
        System.runAs(u){
        insert lstAccount;          
        }
        
        List<Contact> lstContacts = TestDataFactory.createContacts(1) ;
        lstContacts[0].accountId = lstAccount[0].id;
        lstContacts[0].Preferred_Address__c='Mailing Address';        
        System.runAs(u){
        insert lstContacts;
        }
        
        List<AccountContact__c> lstaccountcontact = TestDataFactory.createAccountContact(2, lstAccount[0].id, lstContacts[0].id);
        lstaccountcontact[0].Primary__c = true;
        System.runAs(u){            
        insert lstaccountcontact ;
        }
        // lstaccountcontact[0].Account__c = lstAccount[0].id;
        //update lstaccountcontact[0];
        System.runAs(u){
            Test.startTest();
            ApexPages.currentPage().getParameters().put('retURL',lstAccount[0].id);
            ApexPages.currentPage().getParameters().put('aid',lstAccount[0].id);
            PS_OverideOrderNewButton  ctrl = new PS_OverideOrderNewButton (new ApexPages.StandardController(new order()));
//            ApexPages.currentPage().getParameters().put('retURL',lstAccount[0].id);
//            ApexPages.currentPage().getParameters().put('aid',lstAccount[0].id);
            ctrl.orderRec.Order_Address_Type__c = 'Contact Mailing';
            ctrl.redirectMethod();
            ctrl.retURL = '/'+lstContacts[0].Id;
            Permissionset ps = [Select id,name from Permissionset where Name = 'Pearson_CS_Order_Entry_Team'];
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.PermissionSetId = ps.id;
            psa.AssigneeId = u.id;
            insert psa;
            ctrl.OverideOrderNewButton();
            ctrl.retURL = '/'+lstAccount[0].Id;
            ctrl.OverideOrderNewButton();
            Test.stopTest();
        }
    }

        static testMethod void orderNewButtonTestMethod4() {
        LeadConversionChecker.inLeadConversion = true;
        Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
        User u = new User();
        u.LastName = 'territoryuser';
        u.alias = 'terrusr'; 
        u.Email = 'territoryuser@pearson.com';  
        u.Username='territoryuser@gmail1.com';
        u.LanguageLocaleKey='en_US'; 
        u.TimeZoneSidKey='America/New_York';
        u.Price_List__c='US HE All';
        u.LocaleSidKey='en_US';
        u.EmailEncodingKey='ISO-8859-1';
        u.ProfileId = pfile.id;       // '00eg0000000M99E';    currently hardcoded  for admin         
        u.Geography__c = 'Growth';
        u.CurrencyIsoCode='USD';
        u.Line_of_Business__c = 'Higher Ed';
        u.Market__c = 'CA';
        u.Business_Unit__c = 'US Field Sales';
        u.License_Pools__c = 'Test User';
        insert u;
      //  system.debug(u+'***MY USER ID*****');
        
        General_One_CRM_Settings__c gocs = new General_One_CRM_Settings__c();
        gocs.value__c='Pearson_Backend_Order_Creation,Pearson_Global_Backend_Sample,Pearson_Global_Backend_Revenue,Pearson_CS_Order_Entry_Team,Pearson_CS_Agent,Pearson_CS_Manager,Pearson_CS_Team_Lead';
        gocs.Name = 'OrderOverrideNew';
        gocs.category__c = 'Order';
        gocs.Description__c = 'OrderOverrideNew';
        System.runAs(u){
        insert gocs;
        }
        List<Account> lstAccount = TestDataFactory.createAccount(15,'Organization');
        lstAccount[0].Market2__c = 'US';
        lstAccount[0].Line_of_Business__c = 'Higher Ed';
        lstAccount[0].Geography__c = 'Core';        
        lstAccount[0].Market2__c = 'US';
        lstAccount[0].Geography__c = 'Core';
        System.runAs(u){
        insert lstAccount;          
        }
        
        List<Contact> lstContacts = TestDataFactory.createContacts(1) ;
        lstContacts[0].accountId = lstAccount[0].id;
        lstContacts[0].Preferred_Address__c='Mailing Address';        
        System.runAs(u){
        insert lstContacts;
        }
        
        List<AccountContact__c> lstaccountcontact = TestDataFactory.createAccountContact(2, lstAccount[0].id, lstContacts[0].id);
        lstaccountcontact[0].Primary__c = true;
        System.runAs(u){            
        insert lstaccountcontact ;
        }
        // lstaccountcontact[0].Account__c = lstAccount[0].id;
        //update lstaccountcontact[0];
        System.runAs(u){
            Test.startTest();
            ApexPages.currentPage().getParameters().put('retURL',lstAccount[0].id);
            ApexPages.currentPage().getParameters().put('aid',lstAccount[0].id);
            PS_OverideOrderNewButton  ctrl = new PS_OverideOrderNewButton (new ApexPages.StandardController(new order()));
//            ApexPages.currentPage().getParameters().put('retURL',lstAccount[0].id);
//            ApexPages.currentPage().getParameters().put('aid',lstAccount[0].id);
            ctrl.orderRec.Order_Address_Type__c = 'Custom';
            ctrl.redirectMethod();
            ctrl.retURL = '/'+lstContacts[0].Id;
            Permissionset ps = [Select id,name from Permissionset where Name = 'Pearson_CS_Order_Entry_Team'];
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.PermissionSetId = ps.id;
            psa.AssigneeId = u.id;
            insert psa;
            ctrl.OverideOrderNewButton();
            ctrl.retURL = '/'+lstAccount[0].Id;
            ctrl.OverideOrderNewButton();
            ctrl.returntoContact();
            ctrl.backOfcChatPost();
            Test.stopTest();
        }
    }    
   
 }