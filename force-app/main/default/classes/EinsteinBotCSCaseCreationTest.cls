@isTest
private class EinsteinBotCSCaseCreationTest {
    
    @TestSetup
    static void setupData(){
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {

            String firstName = 'John';
            String lastName = 'Doe';

            Account acc = new Account();
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_Creation_Request').getRecordTypeId();
            acc.Name = firstName + ' ' + lastName;

            insert acc;
            
            Contact con = new Contact();
            con.FirstName = firstName;
            con.LastName = lastName;
            con.Email = 'john@salesforce.com';
            con.Phone = '+12345678901';
            con.AccountId = acc.Id;
            insert con;
        
            // Create a queue
            Group testGroup = new Group(Name='NAUS_HECS_LM_SMS', Type='Queue');
            insert testGroup;

            // Assign user to queue
            GroupMember testGrpMember = new GroupMember(GroupId = testGroup.Id, UserOrGroupId = UserInfo.getUserId());
            insert testGrpMember;

            MessagingChannel msgChannnel = new MessagingChannel(MasterLabel = '+11234567890', IsActive = TRUE, 
                                                                MessageType = 'Text', TargetQueueId = testGroup.Id,
                                                                MessagingPlatformKey = '+11234567890', DeveloperName = 'Text_11234567890'
                                                                );
            insert msgChannnel;

            // Create a test MessagingEndUser
            MessagingEndUser msgUser =  new MessagingEndUser(MessagingPlatformKey = '+12345678901', Name = '+12345678901',
                                                            IsOptedOut = FALSE, MessagingChannelId = msgChannnel.Id,
                                                            MessageType = 'Text',AccountId = acc.Id, ContactId = con.Id);
            insert msgUser;

            MessagingSession msgSession = new MessagingSession(MessagingEndUserId = msgUser.Id, MessagingChannelId = msgChannnel.Id, 
                                                               Status = 'Active');
            insert msgSession;
        }
    }

    @IsTest
    static void testCreateCase(){
        
        String msgSessionId = [SELECT Id FROM MessagingSession LIMIT 1].Id;
        EinsteinBotCSCaseCreation.CaseCreationRequest csReq = new EinsteinBotCSCaseCreation.CaseCreationRequest();
        csReq.routableId = msgSessionId;
        csReq.email = 'test@gmail.com';
        csReq.institution = 'Pearson';
        csReq.category = 'Order Status';
        csReq.origin = 'SMS';
        csReq.isbn = '9780205977789';

        Test.startTest();
            List<Case> caseList = EinsteinBotCSCaseCreation.createCase(new List<EinsteinBotCSCaseCreation.CaseCreationRequest>{csReq});
        Test.stopTest();
            system.assertEquals(1, caseList.size());
        
    }
}