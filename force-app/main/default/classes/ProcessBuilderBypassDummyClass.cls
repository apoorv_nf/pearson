/************************************************************************************************************
* Apex Interface Name : ProcessBuilderBypassDummyClass 
* Version             : 1 
* Created Date        : 24/1/2017
* Modification Log    : 
* Developer                   Date                
* -----------------------------------------------------------------------------------------------------------
* Christwin                 24/1/2017
-------------------------------------------------------------------------------------------------------------
Class to Activate Bypass Setting for Process Builder
************************************************************************************************************/
Public class ProcessBuilderBypassDummyClass {
    @InvocableMethod
    public static void dummyFunction() {
    
    }
}