public class CustomProductLookupController{
    public Product2 product {get;set;} // new account to create
    Public String searchText1 {get;set;}
    Public String searchText2 {get;set;}
    public Map<String, Schema.SObjectField> fieldMap {get;set;}
    Public List<SelectOption> options {get;set;}
    Public List<SelectOption> condOptions {get;set;}
    Public String selOptions1 {get;set;}
    Public String selCondOptions1 {get;set;}
    Public String selOptions2 {get;set;}
    Public String selCondOptions2 {get;set;}
    public string selandor {get;set;}
    public string soql {get;set;}
    public string searchText11;
    public string searchText21;
    public list<product2> serresult {get;set;}
    public string gsearch {get;set;}
    public string sosl;
    private User loggedUser = new User();
    public String selectedValueFromPopup {get;set;}
    public boolean addNewFilterLink {get;set;}
    public String selectedFilterbyValue {get;set;} 
    public Integer selectedRow {get;set;} 
    public Integer rowNumber {get;set;}
    public String JsonMap{get;set;} 
    public Integer loadTime {get;set;}
   public List < dynamicAddFilterSearch > listWithSelectOptions {
        get {
            return listWithSelectOptions;
        }
        set;
    }
    public String selectedSearchType {get;set;}
    public List < SelectOption > searchType {get;set;}
    public Integer rowToRemove {get;set;}
    public String searchText {get;set;} 
    public String selectedCondition {get;set;}
    public Boolean toDisplayLookup {get;set;} 
    public String UserMessageTitle {get;set;}
    public Boolean ShowUserMessage {get;set;}
    public Boolean ShowUserMessage1 {get;set;}
    public String UserMessage {get;set;}
    public String UserMessage1 {get;set;}
    public boolean isSOQL {get;set;}
    public Map<String,SChema.DisplayType> dataTypeMap {get;set;}
       
    
  public CustomProductLookupController() {
    toDisplayLookup = false;
    ShowUserMessage = false;
    loadTime = 0;
    product = new Product2();
    serresult = new list<Product2>();
    initObjNames();
    getCondOperations();
    
    if(!PS_Util.isFetchedUserRecord){
            PS_Util.getUserDetail(Userinfo.getUserid());
            loggedUser = PS_Util.loggedInUser;
        }
       listWithSelectOptions = new List < dynamicAddFilterSearch > ();
       addNewFilter();
  }
    
    public void initObjNames() {
     string fieldLabel;
     options  = new List<SelectOption>(); 
     addNewFilterLink = true; 
     options.add(new SelectOption('--None--','--None--'));
     SObjectType productType = Schema.getGlobalDescribe().get('Product2');
     fieldMap = productType.getDescribe().fields.getMap();
     dataTypeMap = new Map<String,SChema.DisplayType>();
     dataTypeMap.put('--None--',Schema.DisplayType.STRING);
     for (String fieldName: fieldMap.keySet()) { 
            //fieldLabel = fieldMap.get(fieldName).getDescribe().getLabel();            
       dataTypeMap.put(fieldName,fieldMap.get(fieldName).getDescribe().getType());
        /* if(fieldLabel != 'Product ID')
             if(fieldLabel != 'Created By ID')
            options.add(new SelectOption(fieldName,fieldLabel));*/
             
     }
      options.add(new SelectOption('name','Product Name'));
      options.add(new SelectOption('author__c','Author'));
      options.add(new SelectOption('edition__c','Edition'));
      options.add(new SelectOption('publish_date__c','Publish Date'));
      options.add(new SelectOption('sbn__c','ISBN10'));
      options.add(new SelectOption('isbn__c','ISBN13'));
      options.add(new SelectOption('publisher__c','Publisher'));
      options.add(new SelectOption('competitor_product__c','Competitor Product'));      
      options.add(new SelectOption('brand__c','Brand'));
      options.add(new SelectOption('sub_brand__c','Sub-brand'));
      options.add(new SelectOption('delivery_method__c','Delivery Method')); 
      options.add(new SelectOption('isactive','Active'));
     JsonMap = JSON.serialize(datatypemap); 

    }
    
    public void getCondOperations() {
     condOptions  = new List<SelectOption>(); 
     condOptions.add(new SelectOption('--None--','--None--'));
     condOptions.add(new SelectOption('=','equals'));
     condOptions.add(new SelectOption('!=','not equal to'));
     condOptions.add(new SelectOption('like %','starts with'));
     condOptions.add(new SelectOption('like %%','contains'));
     condOptions.add(new SelectOption('like','does not contain'));
    /* condOptions.add(new SelectOption('<','less than'));
     condOptions.add(new SelectOption('>','greater than'));
     condOptions.add(new SelectOption('<=','less or equal'));
     condOptions.add(new SelectOption('>=','greater or equal'));*/
    // condOptions.add(new SelectOption('INCLUDES','includes'));
    // condOptions.add(new SelectOption('EXCLUDES','excludes'));
    // condOptions.add(new SelectOption('<','within'));
    
    }
    public void searchsosl(){
        serresult = new list<Product2>();        
        ShowUserMessage = false;
        boolean isSOSL = true;   
        isSOQL = true;
        try{
        if(gsearch == '' || gsearch == null) {            
            isSOSL = false;
            String soql = searchResults(isSOSL,sosl);
            
            if(!isSOSL && !isSOQL){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Search term must be longer than one character'));           
                return;
            }  
            soql = appendLookupFilter(issosl,soql);
            serresult = database.query(soql);
            
        }else{
            gsearch = String.escapeSingleQuotes(gsearch); 
            sosl = 'FIND '+'\''+gsearch+'*'+'\''+' IN ALL FIELDS RETURNING Product2(Id,Name, Author__c, Edition__c, Publish_Date__c, SBN__c, ISBN__c, Publisher__c, Brand__c, Sub_brand__c, Competitor_Product__c, Delivery_Method__c, IsActive WHERE ';
            sosl = searchResults(isSOSL,sosl);
            sosl = appendLookupFilter(issosl,sosl);
            List<List<SObject>> searchList = search.query(sosl);
            serresult = ((List<Product2>)searchList[0]);
            gsearch = gsearch.replace('\\','');
            //gsearch = '';         
        }
        if(serresult.isEmpty()){
                UserMessageTitle = 'No Results Found';
                ShowUserMessage = true;
                UserMessage = 'No records were found based on your criteria';
        }
          }
           catch(Exception e){
          }

        }
        
    Public String appendLookupFilter(Boolean isSosl, String soql){
       //loggedUser.Bypass_Product_Lookup_Filter_On_Asset__c = true AND 
       String tempsoql;
        if(!isSosl && isSoql)
          tempsoql = soql+' AND (Market__c ='+'\''+loggedUser.Market__c +'\''+' OR Markets__c LIKE \'%'+loggedUser.Market__c+'%\')'+' AND Line_of_Business__c ='+'\''+loggedUser.Line_of_Business__c+'\''+' AND Business_Unit__c ='+'\''+loggedUser.Product_Business_Unit__c+'\''+' LIMIT 999';
        else if(isSosl && !isSoql)
          tempsoql = soql+' (Market__c ='+'\''+loggedUser.Market__c +'\''+' OR Markets__c LIKE \'%'+loggedUser.Market__c+'%\')'+' AND Line_of_Business__c ='+'\''+loggedUser.Line_of_Business__c+'\''+' AND Business_Unit__c ='+'\''+loggedUser.Product_Business_Unit__c+'\''+') LIMIT 999';   
        else if(isSosl && isSoql)
          tempsoql = soql+' AND (Market__c ='+'\''+loggedUser.Market__c +'\''+' OR Markets__c LIKE \'%'+loggedUser.Market__c+'%\')'+' AND Line_of_Business__c ='+'\''+loggedUser.Line_of_Business__c+'\''+' AND Business_Unit__c ='+'\''+loggedUser.Product_Business_Unit__c+'\''+') LIMIT 999';    
         
       return tempsoql;
    }     
    
     public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
     }
      public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
    public String searchResults(Boolean isSOSL,String sosl){
        serresult = new list<Product2>();
        ShowUserMessage = false;
        try{
            if(listWithSelectOptions.size()>0){
                Boolean flag = false;
                Integer count = 0;
                if(!isSOSL)
                soql = 'Select Id,Name, Author__c, Edition__c, Publish_Date__c, SBN__c, ISBN__c, Publisher__c, Brand__c, Sub_brand__c, Competitor_Product__c, Delivery_Method__c, IsActive from product2 where ';      
                else
                soql = sosl;
                for(dynamicAddFilterSearch wrapper : listWithSelectOptions){                                      
                    String selCondOptions1 = wrapper.selectedCondition;
                    String searchText11 = '';
                    Date dateToCompare;
                    Boolean doesNotContains = false;
                    String searchText1 = wrapper.searchText;
                    searchText1 = String.escapeSingleQuotes(searchText1); 
                    String selectedField = wrapper.selectedProductFilterValue;
                    if (selCondOptions1 == '--None--' || wrapper.selectedProductFilterValue == '--None--') {
                       count=count+1;
                       continue; 
                   } 
                    Schema.DisplayType dataType = fieldMap.get(selectedField).getDescribe().getType();
                    String operator = '';
                   // soql = soql+selectedField;

                    if(selCondOptions1 == 'like %')
                    {   if(searchText1 == '')
                            searchText11 = '= \''+searchText1+'\'';
                        else
                        searchText11 = 'LIKE \''+searchText1+'%\'';
                    }else if(selCondOptions1 == 'like %%')
                    {    if(searchText1 == '')
                            searchText11 = '= \''+searchText1+'\'';
                        else
                        searchText11 = 'LIKE \'%'+searchText1+'%\'';
                    }else if(selCondOptions1 == 'like')
                    {
                        if(searchText1 == '')
                           searchText11 = '<> \''+searchText1+'\'';    
                        else
                        { 
                            searchText11 = ' ( NOT '+selectedField+' LIKE \'%'+searchText1+'%\' )';
                            doesNotContains = true;
                        }
                    }else if(selCondOptions1 == '=')
                    {  
                    if(dataType == Schema.DisplayType.Boolean || dataType == Schema.DisplayType.Date || dataType == Schema.DisplayType.Integer || dataType == Schema.DisplayType.Double )                        
                       {
                           if(searchText1 == '')
                               searchText11 = '= null';
                           else if(dataType == Schema.DisplayType.DATE )
                                searchText11 = '= '+convertToDate(searchText1);
                           else
                             searchText11 = '= '+searchText1;       
                       }                                                                  
                       else
                          searchText11 = '= \''+searchText1+'\'';                       
                      }
                      else if(selCondOptions1 == '!=')
                    {
                       if(dataType == Schema.DisplayType.Boolean || dataType == Schema.DisplayType.Date || dataType == Schema.DisplayType.Integer || dataType == Schema.DisplayType.Double )                        
                       {
                           if(searchText1 == '')
                               searchText11 = '<> null';
                           else if(dataType == Schema.DisplayType.DATE )
                                searchText11 = '<> '+convertToDate(searchText1);
                           else
                             searchText11 = '<> '+searchText1;       
                       }                                                                  
                       else
                          searchText11 = '<> \''+searchText1+'\''; 
 
                    }else if(selCondOptions1 == '<')
                    {
                        if(dataType == Schema.DisplayType.Integer || dataType == Schema.DisplayType.Double || dataType == Schema.DisplayType.DATE )                        
                         {
                               if(searchText1 == '')
                                   searchText11 = '= null';
                               else if(dataType == Schema.DisplayType.DATE )
                                   searchText11 = '< '+convertToDate(searchText1);
                               else
                                searchText11 = '< '+searchText1;       
                         }                        
                        
                    }
                    else if(selCondOptions1 == '>')
                    {
                        if(dataType == Schema.DisplayType.Integer || dataType == Schema.DisplayType.Double || dataType == Schema.DisplayType.DATE )                        
                         {
                               if(searchText1 == '')
                                   searchText11 = '= null';
                               else if(dataType == Schema.DisplayType.DATE )
                                   searchText11 = '>'+convertToDate(searchText1);
                               else
                                searchText11 = '> '+searchText1;       
                         } 
                           
                     }
                                        
                    if(!flag){
                    if(doesNotContains)
                    soql = soql+' '+searchText11;
                    else
                    soql = soql+selectedField+' '+searchText11;
                    
                    flag = true;
                    }
                    else{
                    if(doesNotContains)
                    soql = soql+' AND '+' '+searchText11;    
                    else
                    soql = soql+' AND '+selectedField+' '+searchText11;    
                    }
                   }                   
               if(listWithSelectOptions.size() == count){
                   // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select the search criteria'));
                    isSOQL = false;
                } 
            }
            return soql;
        }
        catch(Exception e){
            return soql;
            
        }
 }
 
       public String convertToDate(String searchText){
           if(searchText == '' || searchText == null)
               return 'null';
                    string[] dateStr = searchText.split('/');
                    String dateToCompare = String.valueOf(date.newInstance(Integer.valueOf(dateStr[2]), Integer.valueOf(dateStr[0]), Integer.valueOf(dateStr[1]))); // yyyy-mm-dd                              
                   
                    return dateToCompare;  
 
         }
 /**
* Description : Inner class for values Added For Custom Filter Search
* @param String
* @return String
* @throws NA
**/
    public class dynamicAddFilterSearch {
        public List < SelectOption > productFieldLists {
            get;
            set;
        }
        public List < SelectOption > conditionLists {
            get;
            set;
        }
        public List < SelectOption > searchType {
            get;
            set;
        }
        public String searchText {
            get;
            set;
        }
        public String searchTextDate {
            get;
            set;
        } 
        public String selectedCondition {
            get;
            set;
        }
        public String selectedProductFilterValue {
            get;
            set;
        }
        public String selectedSearchType {
            get;
            set;
        } 
        public Boolean toDisplayLookup {
            get;
            set;
        } 
        public Integer rowNumber {
            get;
            set;
        } 
        public dynamicAddFilterSearch(String defaultValue,List < SelectOption > productFieldLstWrapperCons, List < SelectOption > conditionLstWrapperCons, List < SelectOption > searchTypeLstWrapperCons, String selectedSearchTypeInput, String searchTextInput, String selectedConditionInput, boolean toDisplayLookupInput, Integer rowNumberInput) {
            productFieldLists = productFieldLstWrapperCons;
            conditionLists = conditionLstWrapperCons;
            searchText = searchTextInput;
            selectedCondition = selectedConditionInput;
            selectedProductFilterValue = defaultValue;
            rowNumber = rowNumberInput;
        }
    }
    /**
* Description : Retrieve Code (4 char) values from Multi - Select picklist
* @param String
* @return String
* @throws NA
**/
    public pageReference addNewFilter() {
        
       toDisplayLookup = false;
       searchText = '';       
       loadTime = loadTime+1; 
        if (listWithSelectOptions.size() <= 4) {
            rowNumber = listWithSelectOptions.size();
            getCondOperations();
           if(loadTime == 1)
           listWithSelectOptions.add(new dynamicAddFilterSearch('author__c',options, condOptions, searchType, selectedSearchType, searchText, 'like %%', toDisplayLookup, rowNumber));
           else
           listWithSelectOptions.add(new dynamicAddFilterSearch('',options, condOptions, searchType, selectedSearchType, searchText, selectedCondition, toDisplayLookup, rowNumber));
           
        } else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'You can have only five filter(s)');
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
    
     /**
* Description : Remove filter functionality 
* @param String
* @return String
* @throws NA
**/
    public void removeFilter() {
        listWithSelectOptions = removeFilterRow(rowToRemove, listWithSelectOptions);
    }
    
     /**
* Description : Remove selected filter functionality 
* @param String
* @return String
* @throws NA
**/
    public static List < dynamicAddFilterSearch > removeFilterRow(Integer rowToRemove, List < dynamicAddFilterSearch > listWithSelectOptionss) {
        listWithSelectOptionss.remove(rowToRemove);
        return listWithSelectOptionss;
    }
    
      /**
* Description : in Serach, if user selects date type then display search text of date etc
* @param String
* @return String
* @throws NA
**/
    public void changeSerachTextType() {
        try{
        selectedFilterbyValue = Apexpages.currentPage().getParameters().get('selectedFilterbyValue');
        selectedRow = Integer.valueOf(Apexpages.currentPage().getParameters().get('selectedRow'));
        for (dynamicAddFilterSearch wrapperClass: listWithSelectOptions) {
            String selectedField = wrapperClass.selectedProductFilterValue;
            if(selectedField != '--None--' && selectedField != '' && selectedField != null){
            Schema.DisplayType dataType = fieldMap.get(selectedField).getDescribe().getType();
        if ((dataType == schema.DisplayType.DATE ||  dataType == schema.DisplayType.DATETIME || dataType == schema.DisplayType.INTEGER || dataType == schema.DisplayType.DOUBLE) && wrapperClass.rowNumber == selectedRow) {
            
            wrapperClass.toDisplayLookup = false;                    
            condOptions = new List < SelectOption > ();
            condOptions.add(new SelectOption('=','equals'));
            condOptions.add(new SelectOption('!=','not equal to'));
            condOptions.add(new SelectOption('<','less than'));
            condOptions.add(new SelectOption('>','greater than'));
            wrapperClass.conditionLists = condOptions;
        }else if((dataType == SChema.DisplayType.BOOLEAN) && wrapperClass.rowNumber == selectedRow){
            wrapperClass.toDisplayLookup = true;
            condOptions = new List < SelectOption > ();
            condOptions.add(new SelectOption('=','equals'));
            condOptions.add(new SelectOption('!=','not equal to'));
            wrapperClass.conditionLists = condOptions;
        }
        else if((dataType == SChema.DisplayType.PICKLIST) && wrapperClass.rowNumber == selectedRow){
            
            wrapperClass.toDisplayLookup = true;
            getCondOperations();
            wrapperClass.conditionLists = condOptions;
        }else if(wrapperClass.rowNumber == selectedRow){
            wrapperClass.toDisplayLookup = false; 
            getCondOperations();
            wrapperClass.conditionLists = condOptions;
        }
        }
    }
        }
        catch(Exception e){
        }
    }
    
    
     /**
* Description : fetch status pikclist field values dynamiccally
* @param NA
* @return list
* @throws NA
**/
    public List < SelectOption > getStatuses() {
        String fieldName = Apexpages.currentPage().getParameters().get('fieldName');
        List < SelectOption > options = new List < SelectOption > ();
        Schema.DescribeFieldResult fieldResult = fieldMap.get(fieldName).getDescribe();
        
        if(fieldResult.getType() == SChema.DisplayType.PICKLIST){
        List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }        
        }
        else if(fieldResult.getType() == SChema.DisplayType.BOOLEAN){
             options.add(new SelectOption('True', 'True'));
             options.add(new SelectOption('False', 'False'));
        }
          return options;
    }
}