public with sharing class Pearson_LandingFormController {
    public class SupportWrapper{
        @AuraEnabled
        @TestVisible String url {get;set;}
        @AuraEnabled
        @TestVisible Boolean community {get;set;}
        @AuraEnabled
        @TestVisible Boolean contact {get;set;}
        @AuraEnabled
        @TestVisible String sfdcBusiness {get;set;}
        @AuraEnabled
        @TestVisible String sfdcRole {get;set;}

        public SupportWrapper(String SupportUrl, Boolean SupportCommunity, Boolean SupportContact, String SupportSFDCBusiness, String SupportSFDCRole){
            url = SupportUrl;
            community = SupportCommunity;
            contact = SupportContact;
            sfdcBusiness = SupportSFDCBusiness;
            sfdcRole = SupportSFDCRole;
        }
    }
    public class OptionWrapper{
        @AuraEnabled
        @TestVisible String country {get;set;}
        @AuraEnabled
        @TestVisible String languages {get;set;}
        @AuraEnabled
        @TestVisible String roles {get;set;}

        public OptionWrapper(String ItemCountry, String ItemLanguages, String ItemRoles){
            country = ItemCountry;
            languages = ItemLanguages;
            roles = ItemRoles;
        }
    }
    @AuraEnabled
    public static List<OptionWrapper> getBasicFields(){
        List<OptionWrapper> options = new List<OptionWrapper>();
        List<Support_Landing_Cnty_Lng_Role__c> fieldsList = [SELECT Id, Name, Landing_Country__c, LandingPage_Role__c, Landing_Language__c FROM Support_Landing_Cnty_Lng_Role__c ORDER BY Landing_Country__c];
        if (!Peak_Utils.isNullOrEmpty(fieldsList)) {
            for(Support_Landing_Cnty_Lng_Role__c field : fieldsList){
                options.add(new OptionWrapper(field.Landing_Country__c, field.Landing_Language__c, field.LandingPage_Role__c));
            }
        }

        System.debug(options);
        return options;
    }
    @AuraEnabled
    public static String getEducation(String country, String role){
        String education;
        List<Support_Landing_Role_EdType__c> edList = [SELECT Id, Education_Type__c FROM Support_Landing_Role_EdType__c WHERE LP_Country__c = :country AND LP_Role__c = :role];
        if (!Peak_Utils.isNullOrEmpty(edList)) {
            education = edList[0].Education_Type__c;
        }
        System.debug(education);
        return education;
    }
    @AuraEnabled
    public static String getProduct(String country, String education){
        String product;
        List<Support_Landing_Website_Type__c> prodList = [SELECT Id, LPW_Product_Category__c FROM Support_Landing_Website_Type__c WHERE LPW_Country__c = :country AND Education_Type__c = :education];
        if (!Peak_Utils.isNullOrEmpty(prodList)) {
            product = prodList[0].LPW_Product_Category__c;
        }
        System.debug(product);
        return product;
    }
    @AuraEnabled
    public static SupportWrapper getSupportUrl(String country, String education, String product){
        SupportWrapper support;
        List<String> values = new List<String>();
        values.add('Printed');
        values.add('Digital');
        values.add('course');
        String queryProduct;
        for(String value : values){
            Boolean result = product.contains(value);
            if(result){
                if(value == 'Printed'){
                    queryProduct = '%physical products%';
                }else{
                    queryProduct = '%' + value + '%';
                }
            }
        }
        List<Support_Website_Address__mdt> supportList = [SELECT Id, Community_Button__c, Contact_Support_Button__c, Support_URL__c, SFDC_Business_Vertical__c, SFDC_Role__c FROM Support_Website_Address__mdt WHERE Web_Country__c = :country AND Education_Type__c LIKE :education AND Product_Category__c LIKE :queryProduct];
        if (!Peak_Utils.isNullOrEmpty(supportList)) {
            support = new SupportWrapper(supportList[0].Support_Url__c, supportList[0].Community_Button__c, supportList[0].Contact_Support_Button__c, supportList[0].SFDC_Business_Vertical__c, supportList[0].SFDC_Role__c);
        }
        return support;
    }
}