@IsTest
private class Peak_FileUploaderControllerTest {
    @isTest
    public static void testFileWrapper() {
        Peak_FileUploaderController.FileWrapper testWrapper = new Peak_FileUploaderController.FileWrapper(Peak_TestConstants.MAXFILESIZE, Peak_TestConstants.ALLOWEDEXTENSIONS);
        String testString = testWrapper.extensions + testWrapper.maxSize;
        String assertString = Peak_TestConstants.ALLOWEDEXTENSIONS + Peak_TestConstants.MAXFILESIZE;

        System.assertEquals(assertString, testString);
    }
    @isTest
    public static void getFileDetailsTest() {
        Network testNetwork;
        List<Network> testNetworkList = [SELECT Id, MaxFileSizeKb, AllowedExtensions FROM Network];
        if (!Peak_Utils.isNullOrEmpty(testNetworkList)) {
            testNetwork = testNetworkList[0];
        }
        Peak_FileUploaderController.FileWrapper testResults = Peak_FileUploaderController.getFileDetails();
        String testString = testResults.extensions + testResults.maxSize;
        String assertString = testNetwork.AllowedExtensions + testNetwork.MaxFileSizeKb;

        System.assertEquals(assertString, testString);
    }
    @isTest
    public static void saveChunkSaveTest() {
        Case testCase = new Case();
        testCase.Status = 'New';
        insert testCase;

        String testAttachment = Peak_FileUploaderController.saveChunk(testCase.Id, Peak_TestConstants.FILENAME, Peak_TestConstants.BASE64FILE, Peak_TestConstants.FILETYPE, null);
//        String testString = testAttachment.Name + testAttachment.ContentType;
//        String assertString = Peak_TestConstants.FILENAME + Peak_TestConstants.FILETYPE;
//
//        System.assertEquals(assertString, testString);
        System.assertNotEquals('', testAttachment);
    }
    @isTest
    public static void saveChunkAppendTest() {
        Case testCase = new Case();
        testCase.Status = 'New';
        insert testCase;
        Attachment testAttachment = new Attachment();

        String base64Data = EncodingUtil.urlDecode(Peak_TestConstants.BASE64FILE, 'UTF-8');
        testAttachment.parentId = testCase.Id;

        testAttachment.Body = EncodingUtil.base64Decode(base64Data);
        testAttachment.Name = Peak_TestConstants.FILENAME;
        testAttachment.ContentType = Peak_TestConstants.FILETYPE;

        insert testAttachment;
        String testResults = Peak_FileUploaderController.saveChunk(testCase.Id, Peak_TestConstants.FILENAME, Peak_TestConstants.BASE64FILE, Peak_TestConstants.FILETYPE, testAttachment.Id);
//        String testString = testResults.Id + testResults.Name + testResults.ContentType;
//        String assertString = testAttachment.Id + Peak_TestConstants.FILENAME + Peak_TestConstants.FILETYPE;

        System.assertNotEquals('', testResults);
    }
    @isTest
    public static void saveTheFileTest() {
        Case testCase = new Case();
        testCase.Status = 'New';
        insert testCase;

        Attachment testAttachment = Peak_FileUploaderController.saveTheFile(testCase.Id, Peak_TestConstants.FILENAME, Peak_TestConstants.BASE64FILE, Peak_TestConstants.FILETYPE);
        String testString = testAttachment.Name + testAttachment.ContentType;
        String assertString = Peak_TestConstants.FILENAME + Peak_TestConstants.FILETYPE;

        System.assertEquals(assertString, testString);
        System.assertNotEquals(null, testAttachment.Id);
    }
    @isTest
    public static void appendToFileTest() {
        Case testCase = new Case();
        testCase.Status = 'New';
        insert testCase;
        Attachment testAttachment = new Attachment();

        String base64Data = EncodingUtil.urlDecode(Peak_TestConstants.BASE64FILE, 'UTF-8');
        testAttachment.parentId = testCase.Id;

        testAttachment.Body = EncodingUtil.base64Decode(base64Data);
        testAttachment.Name = Peak_TestConstants.FILENAME;
        testAttachment.ContentType = Peak_TestConstants.FILETYPE;

        insert testAttachment;
        Attachment testResults = Peak_FileUploaderController.appendToFile(testAttachment.Id, Peak_TestConstants.BASE64FILE);
        String testString = testResults.Id + testResults.Name + testResults.ContentType;
        String assertString = testAttachment.Id + Peak_TestConstants.FILENAME + Peak_TestConstants.FILETYPE;

        System.assertEquals(assertString, testString);
    }
    @isTest
    public static void deleteAttachmentTest() {
        Case testCase = new Case();
        testCase.Status = 'New';
        insert testCase;
        Attachment testAttachment = new Attachment();

        String base64Data = EncodingUtil.urlDecode(Peak_TestConstants.BASE64FILE, 'UTF-8');
        testAttachment.parentId = testCase.Id;

        testAttachment.Body = EncodingUtil.base64Decode(base64Data);
        testAttachment.Name = Peak_TestConstants.FILENAME;
        testAttachment.ContentType = Peak_TestConstants.FILETYPE;

        insert testAttachment;
        Attachment testResults = Peak_FileUploaderController.deleteAttachment(testAttachment.Name, testAttachment.parentId);
        Attachment deletedAttachment = [SELECT Id, IsDeleted FROM Attachment WHERE Id = :testResults.Id ALL ROWS];

        System.assertEquals(true, deletedAttachment.IsDeleted);
    }
}