/**************************************************************************
 * Class Name   : UKTEPPricingController_Test
 * Author       : Aman Sawhney
 * Date Created : 14 / Nov / 2018
 * Description  : This class handels the pricing xml request 
 *                and response test cases.
 * ************************************************************************/
@isTest
private class UKTEPPricingController_Test {
    
    @isTest
    private static void getPriceTestWithNonSampleLineItems(){
        
        Continuation con  = null;
        Test.startTest();
        
        Quote targetQuote = UKTEPPricingTestDataFactory.createQuoteWithoutSampleLines();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(targetQuote);
        UKTEPPricingController UKTEPPricingController = new UKTEPPricingController(sc);
        
        //assert for alert box
        System.assert(UKTEPPricingController.alertBoxCmp.rendered==true);
        
        con = UKTEPPricingController.getPrice();
        
        Test.stopTest();
        
        //get continuation request
        Map<String, HttpRequest> requestMap = con.getRequests();
        
        HttpRequest request = requestMap.get(UKTEPPricingController.requestLabel);
        
        //get the request and assert not null
        System.assert(request!=null);
        
        HttpResponse response = new HttpResponse();
        response.setBody('<OrderLine><OrderLevel><sourceSystem>ONECRM</sourceSystem><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>Order Simulation has been Completed</API_RETURN_MESSAGE><orderFlag></orderFlag><cartId>0</cartId><TEPOrderNumber></TEPOrderNumber><promoCodeStatus></promoCodeStatus><LineLevel><LineItemLevel><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>SUCCESS</API_RETURN_MESSAGE><ISBN>9780131371156</ISBN><productDetails>Pearson Physics</productDetails><TEPLineID></TEPLineID><unitSellingPrice>72.50</unitSellingPrice><unitListPrice>72.50</unitListPrice><orderQuantity>3</orderQuantity><extendedPrice>217.50</extendedPrice><LineFrieghtCharge>0.00</LineFrieghtCharge><handlingAmount>0.00</handlingAmount><taxes>0.00</taxes><pricingMethod>L</pricingMethod><discountPercentage>0.00</discountPercentage><LineRefNumOfOasis>0QL1j0000008dw8GAA</LineRefNumOfOasis><ItemAvailStatus></ItemAvailStatus><substituteItemISBN></substituteItemISBN><substituteReason></substituteReason><discountCode>21-ELT SCHOOLS</discountCode></LineItemLevel><LineItemLevel><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>SUCCESS</API_RETURN_MESSAGE><ISBN>9780131371156</ISBN><productDetails>Pearson Physics</productDetails><TEPLineID></TEPLineID><unitSellingPrice>72.50</unitSellingPrice><unitListPrice>72.50</unitListPrice><orderQuantity>5</orderQuantity><extendedPrice>362.50</extendedPrice><LineFrieghtCharge>0.00</LineFrieghtCharge><handlingAmount>0.00</handlingAmount><taxes>0.00</taxes><pricingMethod>L</pricingMethod><discountPercentage>0.00</discountPercentage><LineRefNumOfOasis>0QL1j0000008dwDGAQ</LineRefNumOfOasis><ItemAvailStatus></ItemAvailStatus><substituteItemISBN></substituteItemISBN><substituteReason></substituteReason><discountCode>21-ELT SCHOOLS</discountCode></LineItemLevel></LineLevel></OrderLevel></OrderLine>');
        
        //set http response
        Test.setContinuationResponse(UKTEPPricingController.requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(UKTEPPricingController, con);
        
        System.assert(response.getBody()!=null);
        
    }
  
    @isTest
    private static void getPriceTestWithSampleLineItems(){
        
        Continuation con  = null;
        Test.startTest();
        
        Quote targetQuote = UKTEPPricingTestDataFactory.createQuoteWithSampleLines();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(targetQuote);
        UKTEPPricingController UKTEPPricingController = new UKTEPPricingController(sc);
        
        //assert for alert box
        System.assert(UKTEPPricingController.alertBoxCmp.rendered==true);
        
        con = UKTEPPricingController.getPrice();
        
        Test.stopTest();
        
        //get continuation request
        System.assert(con==null);
        
    }
  
    @isTest
    private static void getPriceTestWithMixLineItems(){
        
        Continuation con  = null;
        Test.startTest();
        
        Quote targetQuote = UKTEPPricingTestDataFactory.createQuoteWithMixSampleLines();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(targetQuote);
        UKTEPPricingController UKTEPPricingController = new UKTEPPricingController(sc);
        
        //assert for alert box
        System.assert(UKTEPPricingController.alertBoxCmp.rendered==true);
        
        con = UKTEPPricingController.getPrice();
        
        Test.stopTest();
        
        //get continuation request
        Map<String, HttpRequest> requestMap = con.getRequests();
        
        HttpRequest request = requestMap.get(UKTEPPricingController.requestLabel);
        
        //get the request and assert not null
        System.assert(request!=null);
        
        HttpResponse response = new HttpResponse();
        response.setBody('<OrderLine><OrderLevel><sourceSystem>ONECRM</sourceSystem><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>Order Simulation has been Completed</API_RETURN_MESSAGE><orderFlag></orderFlag><cartId>0</cartId><TEPOrderNumber></TEPOrderNumber><promoCodeStatus></promoCodeStatus><LineLevel><LineItemLevel><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>SUCCESS</API_RETURN_MESSAGE><ISBN>9780131371156</ISBN><productDetails>Pearson Physics</productDetails><TEPLineID></TEPLineID><unitSellingPrice>72.50</unitSellingPrice><unitListPrice>72.50</unitListPrice><orderQuantity>3</orderQuantity><extendedPrice>217.50</extendedPrice><LineFrieghtCharge>0.00</LineFrieghtCharge><handlingAmount>0.00</handlingAmount><taxes>0.00</taxes><pricingMethod>L</pricingMethod><discountPercentage>0.00</discountPercentage><LineRefNumOfOasis>0QL1j0000008dw8GAA</LineRefNumOfOasis><ItemAvailStatus></ItemAvailStatus><substituteItemISBN></substituteItemISBN><substituteReason></substituteReason><discountCode>21-ELT SCHOOLS</discountCode></LineItemLevel></LineLevel></OrderLevel></OrderLine>');
        
        //set http response
        Test.setContinuationResponse(UKTEPPricingController.requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(UKTEPPricingController, con);
        
        System.assert(response.getBody()!=null);
        
    }
    
    @isTest
    private static void getPriceTestWithWrongQuoteStatus(){
        
        Continuation con  = null;
        Test.startTest();
        
        Quote targetQuote = UKTEPPricingTestDataFactory.createQuoteWithMixSampleLines();
        targetQuote.Status = 'Draft';
        update targetQuote; 
        
        targetQuote = [Select Id,name,Status,(Select Id from QuoteLineItems) from Quote where Id=:targetQuote.Id];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(targetQuote);
        UKTEPPricingController UKTEPPricingController = new UKTEPPricingController(sc);
        
        //assert for alert box
        System.assert(UKTEPPricingController.alertBoxCmp.rendered==true);
        
        con = UKTEPPricingController.getPrice();
        
        Test.stopTest();
        
        //get continuation request
        system.assert(con==null);
        
    }
    
    @isTest
    private static void getPriceTestWithRightQuoteStatus(){
        
        Continuation con  = null;
        Test.startTest();
        
        Quote targetQuote = UKTEPPricingTestDataFactory.createQuoteWithMixSampleLines();
        targetQuote.Status = Label.Quote_Price_Call_Status;
        update targetQuote; 
        
        targetQuote = [Select Id,name,Status,(Select Id from QuoteLineItems) from Quote where Id=:targetQuote.Id];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(targetQuote);
        UKTEPPricingController UKTEPPricingController = new UKTEPPricingController(sc);
        
        //assert for alert box
        System.assert(UKTEPPricingController.alertBoxCmp.rendered==true);
        
        con = UKTEPPricingController.getPrice();
        
        Test.stopTest();
        
        //get continuation request
        system.assert(con!=null);
        
    }
    
    @isTest
    private static void getPriceTestWithNonSampleLineItemsWithoutResponse(){
        
        Continuation con  = null;
        Test.startTest();
        
        Quote targetQuote = UKTEPPricingTestDataFactory.createQuoteWithoutSampleLines();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(targetQuote);
        UKTEPPricingController UKTEPPricingController = new UKTEPPricingController(sc);
        String xmlResponse = '<OrderLine><OrderLevel><sourceSystem>ONECRM</sourceSystem><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>Order Simulation has been Completed</API_RETURN_MESSAGE><orderFlag/><cartId>7546810</cartId><TEPOrderNumber/><promoCodeStatus/><LineLevel></LineLevel></OrderLevel></OrderLine>';
        
        //assert for alert box
        System.assert(UKTEPPricingController.alertBoxCmp.rendered==true);
        
        con = UKTEPPricingController.getPrice();
        
        Test.stopTest();
        
        //get continuation request
        Map<String, HttpRequest> requestMap = con.getRequests();
        
        HttpRequest request = requestMap.get(UKTEPPricingController.requestLabel);
        
        //get the request and assert not null
        System.assert(request!=null);
        
        HttpResponse response = new HttpResponse();
        response.setBody(xmlResponse);
        
        //set http response
        Test.setContinuationResponse(UKTEPPricingController.requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(UKTEPPricingController, con);
        
        System.assert(response.getBody()!=null);
        
    }
    
    @isTest
    private static void getPriceTestWithNoQuoteLines(){
        
        Continuation con  = null;
        Test.startTest();
        
        Quote targetQuote = UKTEPPricingTestDataFactory.createQuoteWithoutLines();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(targetQuote);
        UKTEPPricingController UKTEPPricingController = new UKTEPPricingController(sc);
        String xmlResponse = '<OrderLine><OrderLevel><sourceSystem>ONECRM</sourceSystem><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>Order Simulation has been Completed</API_RETURN_MESSAGE><orderFlag/><cartId>7546810</cartId><TEPOrderNumber/><promoCodeStatus/><LineLevel></LineLevel></OrderLevel></OrderLine>';
        
        //assert for alert box
        System.assert(UKTEPPricingController.alertBoxCmp.rendered==true);
        
        con = UKTEPPricingController.getPrice();
        
        Test.stopTest();
        
        //get continuation request
        System.assert(con==null);
        
    }
    
    @isTest
    private static void getPriceWithFailResponses(){
        
        Continuation con  = null;
        Test.startTest();
        
        Quote targetQuote = UKTEPPricingTestDataFactory.createQuoteWithoutSampleLines();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(targetQuote);
        UKTEPPricingController UKTEPPricingController = new UKTEPPricingController(sc);
        
        //assert for alert box
        System.assert(UKTEPPricingController.alertBoxCmp.rendered==true);
        
        con = UKTEPPricingController.getPrice();
        
        Test.stopTest();
        
        //get continuation request
        Map<String, HttpRequest> requestMap = con.getRequests();
        
        HttpRequest request = requestMap.get(UKTEPPricingController.requestLabel);
        
        //get the request and assert not null
        System.assert(request!=null);
        
        HttpResponse response = new HttpResponse();
        response.setStatusCode(2000);
        response.setBody('<OrderLine><OrderLevel><sourceSystem>ONECRM</sourceSystem><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>Order Simulation has been Completed</API_RETURN_MESSAGE><orderFlag/><cartId>7546810</cartId><TEPOrderNumber/><promoCodeStatus/><LineLevel><LineItemLevel><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>SUCCESS</API_RETURN_MESSAGE><ISBN>9780131371156</ISBN><productDetails>Pearson Physics</productDetails><TEPLineID/><unitSellingPrice>72.50</unitSellingPrice><unitListPrice>72.50</unitListPrice><orderQuantity>1</orderQuantity><extendedPrice>72.50</extendedPrice><LineFrieghtCharge>0.00</LineFrieghtCharge><handlingAmount>0.00</handlingAmount><taxes>0.00</taxes><pricingMethod>L</pricingMethod><discountPercentage>0.00</discountPercentage><LineRefNumOfOasis>0</LineRefNumOfOasis><ItemAvailStatus/><substituteItemISBN/><substituteReason/><discountCode>21-ELT SCHOOLS</discountCode></LineItemLevel></LineLevel></OrderLevel></OrderLine>');
        
        
        //set http response
        Test.setContinuationResponse(UKTEPPricingController.requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(UKTEPPricingController, con);
        
        System.assert(response.getStatusCode()==2000);
        
        response.setStatusCode(2001);
        
        //set http response
        Test.setContinuationResponse(UKTEPPricingController.requestLabel, response);
        
        // Invoke callback method
        result = Test.invokeContinuationMethod(UKTEPPricingController, con);
        
        System.assert(response.getStatusCode()==2001);
        
        response.setStatusCode(2003);
        
        //set http response
        Test.setContinuationResponse(UKTEPPricingController.requestLabel, response);
        
        // Invoke callback method
        result = Test.invokeContinuationMethod(UKTEPPricingController, con);
        
        System.assert(response.getStatusCode()==2003);
        
        response.setStatusCode(2004);
        
        //set http response
        Test.setContinuationResponse(UKTEPPricingController.requestLabel, response);
        
        // Invoke callback method
        result = Test.invokeContinuationMethod(UKTEPPricingController, con);
        
        System.assert(response.getStatusCode()==2004);
    }
    
    @isTest
    private static void getPriceTestWithNonSampleLineItemsExceptionId(){
        
        Continuation con  = null;
        Test.startTest();
        
        Quote targetQuote = UKTEPPricingTestDataFactory.createQuoteWithoutSampleLines();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(targetQuote);
        UKTEPPricingController UKTEPPricingController = new UKTEPPricingController(sc);
        
        //assert for alert box
        System.assert(UKTEPPricingController.alertBoxCmp.rendered==true);
        
        con = UKTEPPricingController.getPrice();
        
        Test.stopTest();
        
        //get continuation request
        Map<String, HttpRequest> requestMap = con.getRequests();
        
        HttpRequest request = requestMap.get(UKTEPPricingController.requestLabel);
        
        //get the request and assert not null
        System.assert(request!=null);
        
        HttpResponse response = new HttpResponse();
        response.setBody('<OrderLine><OrderLevel><sourceSystem>ONECRM</sourceSystem><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>Order Simulation has been Completed</API_RETURN_MESSAGE><orderFlag></orderFlag><cartId>0</cartId><TEPOrderNumber></TEPOrderNumber><promoCodeStatus></promoCodeStatus><LineLevel><LineItemLevel><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>SUCCESS</API_RETURN_MESSAGE><ISBN>9780131371156</ISBN><productDetails>Pearson Physics</productDetails><TEPLineID></TEPLineID><unitSellingPrice>72.50</unitSellingPrice><unitListPrice>72.50</unitListPrice><orderQuantity>3</orderQuantity><extendedPrice>217.50</extendedPrice><LineFrieghtCharge>0.00</LineFrieghtCharge><handlingAmount>0.00</handlingAmount><taxes>0.00</taxes><pricingMethod>L</pricingMethod><discountPercentage>0.00</discountPercentage><LineRefNumOfOasis>0QL1jdw8GAA</LineRefNumOfOasis><ItemAvailStatus></ItemAvailStatus><substituteItemISBN></substituteItemISBN><substituteReason></substituteReason><discountCode>21-ELT SCHOOLS</discountCode></LineItemLevel><LineItemLevel><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>SUCCESS</API_RETURN_MESSAGE><ISBN>9780131371156</ISBN><productDetails>Pearson Physics</productDetails><TEPLineID></TEPLineID><unitSellingPrice>72.50</unitSellingPrice><unitListPrice>72.50</unitListPrice><orderQuantity>5</orderQuantity><extendedPrice>362.50</extendedPrice><LineFrieghtCharge>0.00</LineFrieghtCharge><handlingAmount>0.00</handlingAmount><taxes>0.00</taxes><pricingMethod>L</pricingMethod><discountPercentage>0.00</discountPercentage><LineRefNumOfOasis>0QL1j0000008dwDGAQ</LineRefNumOfOasis><ItemAvailStatus></ItemAvailStatus><substituteItemISBN></substituteItemISBN><substituteReason></substituteReason><discountCode>21-ELT SCHOOLS</discountCode></LineItemLevel></LineLevel></OrderLevel></OrderLine>');
        
        //set http response
        Test.setContinuationResponse(UKTEPPricingController.requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(UKTEPPricingController, con);
        
        System.assert(response.getBody()!=null);
        
    }
    
    @isTest
    private static void getPriceTestWithNonSampleLineItemsNegativePrice(){
        
        Continuation con  = null;
        Test.startTest();
        
        Quote targetQuote = UKTEPPricingTestDataFactory.createQuoteWithoutSampleLines();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(targetQuote);
        UKTEPPricingController UKTEPPricingController = new UKTEPPricingController(sc);
        
        //assert for alert box
        System.assert(UKTEPPricingController.alertBoxCmp.rendered==true);
        
        con = UKTEPPricingController.getPrice();
        
        Test.stopTest();
        
        //get continuation request
        Map<String, HttpRequest> requestMap = con.getRequests();
        
        HttpRequest request = requestMap.get(UKTEPPricingController.requestLabel);
        
        //get the request and assert not null
        System.assert(request!=null);
        
        HttpResponse response = new HttpResponse();
        response.setBody('<OrderLine><OrderLevel><sourceSystem>ONECRM</sourceSystem><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>Order Simulation has been Completed</API_RETURN_MESSAGE><orderFlag></orderFlag><cartId>0</cartId><TEPOrderNumber></TEPOrderNumber><promoCodeStatus></promoCodeStatus><LineLevel><LineItemLevel><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>SUCCESS</API_RETURN_MESSAGE><ISBN>9780131371156</ISBN><productDetails>Pearson Physics</productDetails><TEPLineID></TEPLineID><unitSellingPrice>-72.50</unitSellingPrice><unitListPrice>-72.50</unitListPrice><orderQuantity>-3</orderQuantity><extendedPrice>-217.50</extendedPrice><LineFrieghtCharge>0.00</LineFrieghtCharge><handlingAmount>0.00</handlingAmount><taxes>0.00</taxes><pricingMethod>L</pricingMethod><discountPercentage>0.00</discountPercentage><LineRefNumOfOasis>0QL1j0000008dw8GAA</LineRefNumOfOasis><ItemAvailStatus></ItemAvailStatus><substituteItemISBN></substituteItemISBN><substituteReason></substituteReason><discountCode>21-ELT SCHOOLS</discountCode></LineItemLevel><LineItemLevel><API_RETURN_STATUS>S</API_RETURN_STATUS><API_RETURN_MESSAGE>SUCCESS</API_RETURN_MESSAGE><ISBN>9780131371156</ISBN><productDetails>Pearson Physics</productDetails><TEPLineID></TEPLineID><unitSellingPrice>-72.50</unitSellingPrice><unitListPrice>-72.50</unitListPrice><orderQuantity>5</orderQuantity><extendedPrice>-362.50</extendedPrice><LineFrieghtCharge>0.00</LineFrieghtCharge><handlingAmount>0.00</handlingAmount><taxes>0.00</taxes><pricingMethod>L</pricingMethod><discountPercentage>0.00</discountPercentage><LineRefNumOfOasis>0QL1j0000008dwDGAQ</LineRefNumOfOasis><ItemAvailStatus></ItemAvailStatus><substituteItemISBN></substituteItemISBN><substituteReason></substituteReason><discountCode>21-ELT SCHOOLS</discountCode></LineItemLevel></LineLevel></OrderLevel></OrderLine>');
        
        //set http response
        Test.setContinuationResponse(UKTEPPricingController.requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(UKTEPPricingController, con);
        
        System.assert(response.getBody()!=null);
        
    }
  
    @isTest
    private static void getPriceTestWithWrongXMLStructure(){
        
        Continuation con  = null;
        Test.startTest();
        
        Quote targetQuote = UKTEPPricingTestDataFactory.createQuoteWithoutSampleLines();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(targetQuote);
        UKTEPPricingController UKTEPPricingController = new UKTEPPricingController(sc);
        
        //assert for alert box
        System.assert(UKTEPPricingController.alertBoxCmp.rendered==true);
        
        con = UKTEPPricingController.getPrice();
        
        Test.stopTest();
        
        //get continuation request
        Map<String, HttpRequest> requestMap = con.getRequests();
        
        HttpRequest request = requestMap.get(UKTEPPricingController.requestLabel);
        
        //get the request and assert not null
        System.assert(request!=null);
        
        HttpResponse response = new HttpResponse();
        response.setBody('');
        
        //set http response
        Test.setContinuationResponse(UKTEPPricingController.requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(UKTEPPricingController, con);
        
        System.assert(response.getBody()=='');
        
    }
  
}