public class IntegrationRequestController {
    @AuraEnabled
    public static List<Integration_Request__c> getIntegrationRequests(String recId){
        system.debug('recId::'+recId);
        List<Integration_Request__c> irDataList = new List<Integration_Request__c>();
        irDataList = [SELECT ID,Name,Status__c,Event__c FROM Integration_Request__c WHere Object_Id__c = : recId LIMIT 50000];
        system.debug('irDataList::'+irDataList);
        return irDataList;
    }
         
}