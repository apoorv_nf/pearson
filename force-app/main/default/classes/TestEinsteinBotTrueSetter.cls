@isTest
private class TestEinsteinBotTrueSetter
{
	@isTest
	static void it_should_return_true()
	{
		List<Boolean> results = EinsteinBotTrueSetter.setTrue();

		System.assert(!results.isEmpty(), 'Results should not be empty');
		System.assert(results[0], 'Result should be true');
	}
}