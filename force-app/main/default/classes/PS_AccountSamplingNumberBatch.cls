/*===========================================================================+
|  HISTORY                                                                   
 |                                                                            
 |  DATE            DEVELOPER        DESCRIPTION                               
 |  ====            =========        =========== 
 |  27/10/2015        IDC:HC        Hasi: Batch class to stamp Sampling Account # on US Organization type Accounts - RD-01495
    16/11/2015        IDC:RG         For change in the logic of stamping sampling account number.
    29/07/2016        IDC:RJ         Updated logic to query for more than one market.   
 +===========================================================================*/
global class PS_AccountSamplingNumberBatch implements Database.Batchable<sObject>{
    global Id orgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Organisation').getRecordTypeId();
    global String marketId;
    global Integer maxSampleNumber;
    global Integer delta=0;
    global String maxSamplingCounterInString;
    global list<String> mkt ;
    //Constructor        
    global PS_AccountSamplingNumberBatch(String market){
        mkt = String.valueof(market).split(',',0);
    } 
    //Start Method      
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('select id,Sampling_Account_Number__c,Primary_Selling_Account_check__c  from account where '+
        '(Sampling_Account_Number__c = null OR Sampling_Account_Number__c = \' \') AND '+
        'Primary_Selling_Account__c=null and Market__c in :mkt AND RecordTypeId =:orgRecordTypeId');
        //Primary_Selling_Account_check__c = true AND Market__c=:marketId AND RecordTypeId =:orgRecordTypeId');
    }
    //Execute Method
    global void execute(Database.BatchableContext BC, List<sObject>scope){
        //List<AggregateResult>  maxsampList=[select MAX(SamplingNumberBatch__c)from account];
        //maxSampleNumber=((decimal)maxsampList[0].get('expr0')).intValue();//356
        List<Account> updateaccountList = new List<Account>();        
        String sSampleNum=[select sampling_Account_Number__c from account where sampling_Account_Number__c != null and 
                        sampling_Account_Number__c != '' and Primary_Selling_Account__c=null order by sampling_Account_Number__c desc 
                        limit 1].sampling_Account_Number__c ;
        //code for checking if the query returns max value than 8800000 then stamp the max value +1 else stamp the label value +1  in the Sampling_Account_Number field              
        if(Integer.valueOf(sSampleNum) > Integer.valueOf(Label.PS_Starting_Sampling_Account)){
            maxSampleNumber = Integer.valueOf(sSampleNum);
        }else{
            maxSampleNumber = Integer.valueOf(Label.PS_Starting_Sampling_Account); 
        }               
        //maxSampleNumber =   Integer.valueOf(sSampleNum);              
        for(sObject s : scope){
            Account typeAcc = (Account)s; 
            if (typeAcc.Primary_Selling_Account_check__c  == true){
                maxSampleNumber = maxSampleNumber + 1;//357,358,359
                /* delta = (Integer.valueOf(Label.PS_SamplingNumberBatchLength)) - ((String.valueOf(maxSampleNumber).length()));//357,358,359 */
                maxSamplingCounterInString=(String.valueOf(maxSampleNumber));
               /* If(delta > 0){
                    for(Integer  i=0;i<delta; i++){
                       maxSamplingCounterInString ='0' +maxSamplingCounterInString;// 0000357,0000358,0000359
                    }
                } */
                typeAcc.Sampling_Account_Number__c = maxSamplingCounterInString;
                typeAcc.Sampling_Sub_Account_Number__c='000';
                updateaccountList.add(typeAcc);
            }
        }
        if (!updateaccountList.isEmpty() && updateaccountList.size() > 0) {
            Database.SaveResult[] lstResult= Database.update(updateaccountList, false); 
            if (lstResult != null){
                List<PS_ExceptionLogger__c> errloggerlist=new List<PS_ExceptionLogger__c>();
                for (Database.SaveResult sr : lstResult) {
                    String ErrMsg='';
                    if (!sr.isSuccess() || Test.isRunningTest()){
                        PS_ExceptionLogger__c errlogger=new PS_ExceptionLogger__c();
                        errlogger.InterfaceName__c='AccountSamplingNumberBatch';
                        errlogger.ApexClassName__c='PS_AccountSamplingNumberBatch';
                        errlogger.CallingMethod__c='Execute';
                        errlogger.UserLogin__c=UserInfo.getUserName(); 
                        errlogger.recordid__c=sr.getId();
                        for(Database.Error err : sr.getErrors()) 
                        ErrMsg=ErrMsg+err.getStatusCode() + ': ' + err.getMessage(); 
                        errlogger.ExceptionMessage__c=  ErrMsg;  
                        errloggerlist.add(errlogger);    
                    }
                }
                if(errloggerlist.size()>0){
                insert errloggerlist;
                }
            }
        }   //if close
    }   
    global void finish(Database.BatchableContext BC)
    {
        PS_AccountSubSamplingNumberBatch batch = new PS_AccountSubSamplingNumberBatch(Label.PS_NA_Market);
        Database.executeBatch(batch);
    }
}