global without sharing class EinsteinBotCreateContact {
	@InvocableMethod(label='Einstein Chatbot - Create contact')
	global static void createContact(List<ContactCreationRequest> contactRequests) 
    {
        List<Einstein_Bot_Event__e> createContactEvents = new List<Einstein_Bot_Event__e>();

        for(ContactCreationRequest contactRequest : contactRequests) {
            Einstein_Bot_Event__e createContactEvent = new Einstein_Bot_Event__e();
            createContactEvent.Type__c = EinsteinBotEventHandler.CREATE_CONTACT;
            createContactEvent.Role__c = contactRequest.role;
            createContactEvent.Email__c = contactRequest.email;
            createContactEvent.First_Name__c = contactRequest.firstName;
            createContactEvent.Last_Name__c = contactRequest.lastName;
            createContactEvent.Institution__c = contactRequest.institutionName;

            createContactEvents.add(createContactEvent);
        }

        List<Database.SaveResult> saveResults = EventBus.publish(createContactEvents);
    }

    global class ContactCreationRequest
    {
        @InvocableVariable
        global String role;

        @InvocableVariable
        global String email;

        @InvocableVariable
        global String firstName;

        @InvocableVariable
        global String lastName;

        @InvocableVariable
        global String institutionName;
    }
}