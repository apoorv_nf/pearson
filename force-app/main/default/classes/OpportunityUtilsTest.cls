@isTest (seeAllData=true)
public class OpportunityUtilsTest {
static testMethod void UnitTest1()
  { 
  List<User> userLst = TestDataFactory.createUser(Userinfo.getProfileid() , 1);
        
    Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=userLst[0].id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,Disable_Process_Builder__c=true,Disable_Workflow_Rules__c=true); 

   System.runas(userLst[0]){

      Set<Id> setOpportunitiesToClone = new Set<Id>();
      Set<String> setCloneOpptyIds = new Set<String>();
      List<opportunity> opptiesToClone = new List<opportunity>();
      OpportunityUtils Oppt = new OpportunityUtils();
       Opportunity opp = (Opportunity)TestClassAutomation.createSObject('Opportunity');
      insert opp;      
      setOpportunitiesToClone.add(opp.id);
      opptiesToClone.add(opp);
      Oppt.createReEngagementClones(setOpportunitiesToClone, setCloneOpptyIds, opptiesToClone);
       
      ActivityTemplate__c act = new ActivityTemplate__c();
      act.Active__c = TRUE;
      act.Related_To__c = 'Opportunity';
      act.Stage__c = 'Prospecting';
      act.Status__c =  'On Schedule'; 
      act.Default_Owner__c = [select id from user where name = 'LaxmanRao Bommisetti' limit 1].id;
      act.Subject__c = 'Test';
     act.Days_Until_Due__c = 10;
     act.Account_Location__c = 'Australia'; 
     act.Description__c = 'Test';
     act.NPS_Rating_Required__c = TRUE;
     act.Product_Type__c = 'Software';
     insert act;
      
      Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
        sAccount.BillingCountry         = 'United Kingdom';
        sAccount.BillingCountryCode     = 'GB';
        sAccount.BillingState           = 'England';
       // sAccount.BillingCountryCode     = 'GB';
        sAccount.BillingStateCode       = 'GBE';
        sAccount.ShippingCountry        = 'United Kingdom';
        sAccount.ShippingState          = 'England';
        sAccount.ShippingStateCode      = 'GBE';
        sAccount.ShippingCountryCode    = 'GB';
        sAccount.PS_AccountSegment1__c  = null;
        sAccount.Payment__c             = null;
        sAccount.Qualification_Level__c = null;
        sAccount.Lead_Sponsor_Type__c   = null;
        sAccount.TEP_Level__c = 'PARTY';
        
        insert sAccount;
      
      Opportunity sOpportunity        = (Opportunity)TestClassAutomation.createSObject('Opportunity');
        sOpportunity.AccountId          = sAccount.Id;
        sOpportunity.StageName          = act.Stage__c;
        sOpportunity.RecordTypeId       = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Global Opportunity').getRecordTypeId();
        sOpportunity.Cadence__c         = '';
        sOpportunity.priority__c         = '';
        sOpportunity.Status__c          = act.Status__c;
        insert sOpportunity;
           
      
      List<Opportunity> Lstopp = new List<Opportunity>();
      Lstopp.add(sOpportunity);    
      Oppt.createFollowUpActivities(Lstopp);      
     //String setCloneOpptyIds  = 'SELECT ';
     
      //Oppt.createReEngagementClones(setOpportunitiesToClone, setCloneOpptyIds, opptiesToClone);
      Pricebook2 priceBook = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
    priceBook.IsActive = true;
    update priceBook;
      Product2 prod = (Product2)TestClassAutomation.createSObject('Product2');
    insert prod;        
       PriceBookEntry sPriceBookEntry = (PriceBookEntry)TestClassAutomation.createSObject('PriceBookEntry');
    sPriceBookEntry.IsActive = true;
    sPriceBookEntry.Product2Id = prod.Id;
    sPriceBookEntry.Pricebook2Id = priceBook.Id;
    sPriceBookEntry.UnitPrice = 34.95;
    sPriceBookEntry.CurrencyIsoCode = 'GBP';
    insert sPriceBookEntry;
      OpportunityLineItem oli1 = new OpportunityLineItem();
    oli1.OpportunityId = opp.Id;
    oli1.PricebookEntryId = sPriceBookEntry.Id;
    oli1.TotalPrice = 200;
    oli1.Quantity = 1;
    insert oli1;
      List<OpportunityLineItem> LstoppLI = new List<OpportunityLineItem>();
      LstoppLI.add(oli1);           
      
      OpportunityUtils.createOLIClone(LstoppLI);
   }   
  }
}