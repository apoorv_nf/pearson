/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
   Name:            AddressSearchHelperTest.cls 
   Description:     Test Class to cover code coverage AddressSearchHelper
   Date             Version         Author                             Summary of Changes 
   -----------      ----------      -----------------    ---------------------------------------------------------------------------------------------------
    28/9/2016         1.0            Payal Popat                       Initial Release 
-------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
//@isTest(seeAllData=true)
@isTest
public class AddressSearchHelperTest {
    static testMethod void SearchAddressController()
{
List<account> lstLeadRecords = new List<account>();
        //lstLeadRecords = TestDataFactory.createaccount(1,'school');
        //insert lstLeadRecords ;
        Bypass_Settings__c TestUser = new Bypass_Settings__c();
         TestUser.SetupOwnerId=UserInfo.getOrganizationId();
         TestUser.Disable_Triggers__c = true;
         TestUser.Disable_Process_Builder__c = true;
         insert TestUser;
        /*
        Bypass_Settings__c bySet = [Select id,Disable_Triggers__c,Disable_Process_Builder__c from Bypass_Settings__c limit 1];
         //bySet.SetupOwnerId=UserInfo.getOrganizationId();
         bySet.Disable_Triggers__c = true;
         bySet.Disable_Process_Builder__c = true;
         update bySet;
         */
        Account AccountRec = new account( Name='Test Account1', Phone='+9100000' ,IsCreatedFromLead__c = True,
            ShippingCountry = 'United kingdom',ShippingState = 'greater london', ShippingCity = 'london', ShippingStreet = 'BNG', ShippingPostalCode = 'SE27 0AA',billingstreet='BNG', billingcity='London', billingstate='Greater london',billingcountry='United Kingdom',billingpostalcode='SE27 0AA');

        insert AccountRec;
        Financial_Account__c FinRec = new Financial_Account__c(Name ='Test Fin rec',Account__c=AccountRec.id);
        insert FinRec ;
         //Account_Addresses__c ac=new Account_Addresses__c(Account__c=AccountRec.id,Financial_Account__c =FinRec.id,Address_Type__c='Billing');
         Account_Addresses__c ac=new Account_Addresses__c(Account__c=AccountRec.id,Financial_Account__c =FinRec.id,billing__c=true,Primary__c=true);
         insert ac;
         
        AccountRec.Shipping_Address_Lookup__c = ac.id;
        AccountRec.Billing_Address_Lookup__c = ac.id;
        
        Update AccountRec;
        
        PageReference pageRef = Page.SearchAddress;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('sId',FinRec.id);
        pageRef.getparameters().put('sObjType','Account');
        pageRef.getparameters().put('sAddressType','Other');
        //pageRef.getparameters().put(bPrimaryFlg,false);
        SearchAddressController SAC =new SearchAddressController();
        SAC.bPrimaryFlg=false;
        SAC.sCountry = 'United Kingdom';
        SAC.sCity = 'United Kingdom';
        sac.mInitCountry();
        sac.mInitState();
        sac.mInitCity();
        sac.mInitAddr();
        sac.mSearchAddress();
        SAC.getAddressTypes();
        SAC.mCancel();
        
        SAC.mPopulateCurrentAddr('UK','UK','test','123654','test','test','test');
        
 
        SAC.mAddressValidation(false);
        SAC.bPrimaryShippingFlg=true;
        SAC.sAddressLine3='test';
        SAC.sAddressLine4='test';
        SAC.bPrimaryFlg=true;
        SAC.mSaveAddress();
       AddressSearchHelper.mgenerateCorrelationId();
        AddressSearchHelper.mgenerateRequestBody('S1','S2','S3','S4','City','AP','County','IN','234565');
}
}