/************************************************************************************************************
* Apex Class Name   : PS_ChatterPostUtilitiesTest.cls
* Version           : 1.0 
* Created Date      : 04 April 2019
* Function          : Test class for PS_ChatterPostUtilities 
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Rajesh Paleti              04/04/2019              Test class for PS_ChatterPostUtilities 
************************************************************************************************************/

@IsTest(SeeAllData=true) // Since we are using ConnectApi methods in apex class, we need to use seealldata=true 
Public class PS_ChatterPostUtilitiesTest{
   
static testMethod void myUnitTest2()
    {
        User usr = new User(LastName = 'happy', alias = 'happy', Email = 'h@gmail.com', Username='test1234'+Math.random()+'@gmail.com', CommunityNickname = 'happy' +Math.random(), LanguageLocaleKey='en_US', TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId = [select Id from Profile where Name =: 'System Administrator'].Id, Geography__c = 'Growth', Price_List__c= 'Math & Science');

        usr.market__c='UK';
        usr.Business_Unit__c='Schools';
        usr.Line_of_Business__c='Schools';
        usr.Group__c='Primary';
        insert usr;
            
         Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usr.id,Disable_Triggers__c=true,Disable_Validation_Rules__c=true,disable_process_builder__c=true);
        insert byp;
        
        system.runAs(usr){
            TestClassAutomation.FillAllFields = true;
            List<Account> sAccount = new List<Account>();          
            sAccount =TestDataFactory.createAccount(1,'Corporate');
            sAccount[0].BillingCountry         = 'Australia';
            sAccount[0].BillingState           = 'Victoria';
            sAccount[0].BillingCountryCode     = 'AU';
            sAccount[0].BillingStateCode       = 'VIC';
            sAccount[0].ShippingCountry        = 'Australia';
            sAccount[0].ShippingState          = 'Victoria';
            sAccount[0].ShippingCountryCode    = 'AU';
            sAccount[0].ShippingStateCode      = 'VIC';
            sAccount[0].Lead_Sponsor_Type__c   = null;
            sAccount[0].PS_AccountSegment1__c  ='1';
            sAccount[0].Pearson_Campus__c=true;
            sAccount[0].Lead_Sponsor_Type__c   = null;
            insert sAccount;
        String status;
        Map<id,id>  userids= new map<id,id>();
        userids.put(usr.id,usr.id);
        Map<Id, Map<String, String>> details = new Map<Id, Map<String, String>>();
        Map<String, String> detail = new Map<String, String>();
        status = ' You have marked Check Duplicates for this Opportunity';
                detail.put('userId', sAccount[0].ownerId);
                detail.put('recordId', sAccount[0].Id);
                detail.put('status', status);
                details.put(sAccount[0].Id, detail);
         PS_ChatterPostUtilities.createPostOnOpportunity(details);
         PS_ChatterPostUtilities.createPostOnAccount(userids);
             
        }
    }
}