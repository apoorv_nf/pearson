/*
 * *********************************************************************************************************
 * @Author : Asha G
 * @Date : 06/07/2019
 * @Description: Controller to 'PS_OpportunityLeadProducts' Page - contains logic to Populate the Lead Products on Opportunity
 * @Version : 1.0
 * *********************************************************************************************************
 */

public class getOpportunityLeadProducts{
    public List<Lead_Product__c> leadProdIds{get;set;}
    public Opportunity opp{get;set;}
    public getOpportunityLeadProducts(ApexPages.StandardController controller)
    {
        opp = (Opportunity)controller.getRecord();
        if(opp.Id!=null){
            opp= [select Id, Converted_Lead_Id__c from Opportunity where id =:opp.Id];            
              Id WebRequestLeadRecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Web Request').getRecordTypeId();
              leadProdIds = [select Id,Name,Requested_ISBN_13__c,Status__c,Requested_Authors__c,Copyright_Date__c,Requested_Title_Edition__c,
             Enrollment__c,Term__c,Product_In_Use__c,Opportunity__c from Lead_Product__c where Lead__c =: Opp.Converted_Lead_Id__c and Lead__r.RecordTypeId =: WebRequestLeadRecordTypeId];
        }
    }
    public void getLeadProducts(){
       List<Lead_Product__c> lProd = new List<Lead_Product__c>();
        for(Lead_Product__c LeadProdlist :leadProdIds)
        {
            LeadProdlist.Opportunity__c=Opp.Id;
            lProd.add(LeadProdlist);
        }
        update lProd;
    }    
}