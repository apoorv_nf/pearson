/*
* Modification History 
* Date              Developer               Description
* 18 Dec 2015       Karan Khanna            Removed reference of TestClassAutomation and used TestDataFactory because TestClassAutomation was populating default values in not required fields which was causing failure
*
*/

@isTest
private class TestOrderLineItemTriggers
{
    static testMethod void myUnitTest()
    {
        TestClassAutomation.FillAllFields = true;
        
        //Account sAccount                = (Account)TestClassAutomation.createSObject('Account');
        List<Account> sAccount = TestDataFactory.createAccount(1,'Organisation');
        sAccount[0].BillingCountry         = 'Australia';
        sAccount[0].BillingState           = 'Victoria';
        sAccount[0].BillingCountryCode     = 'AU';
        sAccount[0].BillingStateCode       = 'VIC';
        sAccount[0].ShippingCountry        = 'Australia';
        sAccount[0].ShippingState          = 'Victoria';
        sAccount[0].ShippingCountryCode    = 'AU';
        sAccount[0].ShippingStateCode      = 'VIC';
        sAccount[0].Lead_Sponsor_Type__c = null;
        
        insert sAccount;
        
        //Opportunity sOpportunity        = (Opportunity)TestClassAutomation.createSObject('Opportunity');
        List<Opportunity> sOpportunity = TestDataFactory.createOpportunity(1,'Opportunity');
        sOpportunity[0].AccountId          = sAccount[0].Id;
        
        insert sOpportunity;
        
        Order__c sOrder                 = (Order__c)TestClassAutomation.createSObject('Order__c');
        sOrder.Account__c               = sAccount[0].Id;
        sOrder.Opportunity__c           = sOpportunity[0].Id;
        
        insert sOrder;
        
        //Product2 sProduct               = (Product2)TestClassAutomation.createSObject('Product2');
        List<Product2> sProduct = TestDataFactory.createProduct(1);
        sProduct[0].Market__c = 'AU';
        insert sProduct;
            
        Test.startTest();
        
            OrderLineItem__c sOLI       = (OrderLineItem__c)TestClassAutomation.createSObject('OrderLineItem__c');
            sOLI.Order__c               = sOrder.Id;
            sOLI.Product__c             = sProduct[0].Id;
            
            insert sOLI;
            
            update sOLI;
            
            delete sOLI;
            
            undelete sOLI;
        
        Test.stopTest();
    }
}