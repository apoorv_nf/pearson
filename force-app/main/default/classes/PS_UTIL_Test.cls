@isTest
private class PS_UTIL_Test{
    
    private static testMethod void recordTypeTest() {

      if(!PS_Util.isFetchedRecordType ){
          PS_Util.fetchAllRecordType();
      }
            
       Map<String, Map<String,ID>> mapRecordType=  PS_Util.recordTypeMap;
       Map<String,ID> accountRecordMap  = mapRecordType.get('Account');
       List<String> recNameLst = new List<String>();
       for(String recName :accountRecordMap.keySet()) {
           recNameLst.add(recName);
         
       }
       Id accountRecordId = PS_Util.fetchRecordTypeByName(Account.SObjectType , recNameLst[0]);
       List<User> usrLst =    TestDataFactory.createUser([select Id from Profile where Name = 'System Administrator'].Id,1);
       insert usrLst;
       Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true);
        insert byp; 
        Test.startTest();
        Order ordWithoutError = new Order();
       System.runas(usrLst[0]){ 
        List<PermissionSet> permLst = [SELECT Id FROM PermissionSet WHERE Name = 'Pearson_Backend_Order_Creation'];
       PermissionSetAssignment psa = new PermissionSetAssignment();
       psa.AssigneeId = usrLst[0].Id;
       psa.PermissionSetId = permLst[0].Id;
       insert psa;
       ordWithoutError =  TestDataFactory.returnorder();
        
               }
                 try{
                System.runas(usrLst[0]){    
                    
                     ordWithoutError.status = 'Open';
                     update ordWithoutError;
                     OrderItem itemObj = [Select id,quantity,unitprice,status__c,StatusReason__c from OrderItem where Orderid =:ordWithoutError.id];
                     itemObj.unitPrice = 50;
                     update itemObj;
                     
                     delete itemObj;
                     
                     delete ordWithoutError;
                }
            }
            catch(Exception ex){
             }
             
             
             
       Test.stopTest();
        
       
        

    }
    
    private static testMethod void orderItemUpdateTest() {
        List<User> usrLst = TestDataFactory.createUser([select Id from Profile where Name = 'System Administrator'].Id,1);
        usrLst[0].Market__c = 'US';
       // usrLst[1].Market__c = 'US';
        insert usrLst;
       
       List<PermissionSet> permLst = [SELECT Id FROM PermissionSet WHERE Name = 'Pearson_Backend_Order_Creation'];
       PermissionSetAssignment psa = new PermissionSetAssignment();
       psa.AssigneeId = usrLst[0].Id;
       psa.PermissionSetId = permLst[0].Id;
       insert psa;
       
        
       
       Test.startTest();
       
        Order ordWithoutError = new Order();
            try{
                System.runas(usrLst[0]){ 
                Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true);
                insert byp;    
                     ordWithoutError =  TestDataFactory.returnorder();
                     
                }
            }
            catch(Exception ex){
             }
             
             
             
             
             
            try{
                System.runas(usrLst[0]){

                     OrderItem itemObj = [Select id,quantity,unitprice,status__c,StatusReason__c from OrderItem where Orderid =:ordWithoutError.id];
                     itemObj.unitPrice = 0;
                     itemObj.quantity = 50;
                     itemObj.status__c = 'Open';
                     itemObj.StatusReason__c = 'Testing 555555';                     
                     update itemObj;
                     delete itemObj;

                }
            }
            catch(Exception ex){
                Boolean expectedExceptionThrown =  ex.getMessage().contains('You do not have permission') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
             }
            
             
             
    
       Test.stopTest();

       
    }
    
        private static testMethod void orderItemDeleteTest() {
        List<User> usrLst = TestDataFactory.createUser([select Id from Profile where Name = 'System Administrator'].Id,1);
       insert usrLst;
       

       List<PermissionSet> permLst = [SELECT Id FROM PermissionSet WHERE Name = 'Pearson_Backend_Order_Creation'];
       PermissionSetAssignment psa = new PermissionSetAssignment();
       psa.AssigneeId = usrLst[0].Id;
       psa.PermissionSetId = permLst[0].Id;
       insert psa;
       
        
       
       
       
        Order ordWithoutError = new Order();
            try{
                System.runas(usrLst[0]){
                Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=usrLst[0].id,Disable_Triggers__c=true);
                insert byp;     
                     ordWithoutError =  TestDataFactory.returnorder();
                     
                }
           }
            catch(Exception ex){
             }
             
             
             
           Test.startTest();  
             
            try{
                System.runas(usrLst[0]){
    
                     OrderItem itemObj = [Select id,quantity,unitprice,status__c,StatusReason__c from OrderItem where Orderid =:ordWithoutError.id];
                    
                    delete itemObj;

                }
            }
            catch(Exception ex){
                Boolean expectedExceptionThrown =  ex.getMessage().contains('delete') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
             }
            
             
             
    
       Test.stopTest();

       
    }
    
        private static testMethod void createOrderTest() {
        List<User> usrLst = TestDataFactory.createUser([select Id from Profile where Name = 'System Administrator'].Id,1);
       insert usrLst;
       
       List<PermissionSet> permLst = [SELECT Id FROM PermissionSet WHERE Name = 'Pearson_Backend_Order_Creation'];
       PermissionSetAssignment psa = new PermissionSetAssignment();
       psa.AssigneeId = usrLst[0].Id;
       psa.PermissionSetId = permLst[0].Id;
       insert psa;
       
        
       
       Test.startTest();
       
        Order ordWithoutError = new Order();
           
             
            try{
                System.runas(usrLst[1]){
                     ordWithoutError =  TestDataFactory.returnorder();

                }
            }
            catch(Exception ex){
                Boolean expectedExceptionThrown =  ex.getMessage().contains('Validation') ? true : false;
                //System.AssertEquals(expectedExceptionThrown, true);
             }
             
             
             try{
                System.runas(usrLst[0]){
                    ordWithoutError.status = 'New';
                     ordWithoutError.AccountId = null;
                     ordWithoutError.EffectiveDate = system.today()+30;
                     update ordWithoutError;

                }
            }
            catch(Exception ex){
                Boolean expectedExceptionThrown =  ex.getMessage().contains('Validation') ? true : false;
                //System.AssertEquals(expectedExceptionThrown, true);
             }
            
             
             
    
       Test.stopTest();

       
    }
    
    private static testMethod void otherUtilTest() {
        Test.startTest();
            PS_Util.getDynamicSOQL(Opportunity.SObjectType,True);
            PS_Util.getWriteableFieldNamesForObject(Opportunity.SObjectType);
        Test.stopTest();
    }

    @isTest
    static void it_should_dtermine_if_user_is_pearson_sales_user() {
        PS_Util.isUserPearsonSalesUser();
    }

    @isTest
    static void it_should_fetch_account_object() {
        PS_Util.fetchRecordTypesByObject(Schema.Account.getSObjectType());
    }

    @isTest
    static void it_should_retrieve_user_permission_sets() {
        PS_Util.retrieveUserPermissionSets(new List<Id>{UserInfo.getUserId()});
    }

    @isTest
    static void it_should_check_if_user_has_a_permission_set() {
        Boolean hasPermission = PS_Util.hasUserPermissionSet(UserInfo.getUserId(), 'Test');
        System.AssertEquals(false, hasPermission);
    }

    @isTest
    static void it_should_add_errors() {
        User user = [SELECT Id FROM User LIMIT 1];
        Map<String,User> userMap = new Map<String,User>{'test' => user};
        Map<String, List<String>> errorMap = new Map<String, List<String>> {
            'test' => new List<String>{'error'}
        };
        PS_Util.addErrors(userMap, errorMap);
    }

    @isTest
    static void it_should_add_error_1() {
        Map<Id,User> userMap = new Map<Id,User>([SELECT Id FROM User LIMIT 1]);
        Map<String, List<String>> errorMap = new Map<String, List<String>> {
            'test' => new List<String>{'error'}
        };
        PS_Util.addErrors_1(userMap, errorMap);
    }

    @isTest
    static void it_should_add_errors_using_a_list_index() {
        List<User> users = [SELECT Id FROM User LIMIT 1];
        Map<String, List<String>> errorMap = new Map<String, List<String>> {
            'test' => new List<String>{'error'}
        };
        PS_Util.addErrorsusinglistindex(users, errorMap);
    }

    @isTest
    static void it_should_get_field_set() {
        List<Schema.FieldSetMember> fieldSet = PS_Util.getFieldSet(Lead.getSObjectType(), 'New_Account_Request');
    }

    @isTest
    static void it_should_get_fields_set_as_a_list_of_strings() {
        PS_Util.getFieldSetAsListOfStrings(Schema.SObjectType.Lead.fieldSets.New_Account_Request.getFields());
    }
}