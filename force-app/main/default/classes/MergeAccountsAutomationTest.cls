/* --------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            MergeAccountsAutomationTest.Cls
Description:     Test class for MergeAccountAutomation - It creates 2 accounts and one with a Contact. Merges the Account with Contact into the master
and asserts only one exists at the end and that it has the contact. --87% coverage
Date             Version           Author                                Summary of Changes 
-----------      ----------   -----------------      ---------------------------------------------------------------------------------------
3/18/2019         0.1          C.Perez                CR-02606 Class to Merge Accounts. Receiving Loser with dupe field has Winner
---------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest 
public class MergeAccountsAutomationTest {
    static testMethod void MergeAccountsAutomationTest() {    
        // Insert new accounts
         Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);
        lstWithTestUser[0].market__c='CA';
        insert lstWithTestUser;
        System.runAs(lstWithTestUser[0]) 
            {
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c =true,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true);
        insert byp; 
                
        List<Account> accList = TestDataFactory.createAccount(52, 'Bookstore');
        Test.startTest();
        insert accList;
        Test.stopTest();
       MergeAccountsAutomation.MergeAccountsAutomation(accList); 
            }
    }
     static testMethod void MergeAccountsAutomationTest1() {    
        // Insert new accounts
         Profile profileId = [select id from profile where Name = 'System Administrator'];
        List<User> lstWithTestUser = TestDataFactory.createUser(profileId.id,2);
        lstWithTestUser[0].market__c='CA';
        insert lstWithTestUser;
        System.runAs(lstWithTestUser[0]) 
            {
        Bypass_Settings__c byp = new Bypass_Settings__c(SetupOwnerId=lstWithTestUser[0].id,Disable_Triggers__c =true,Disable_Process_Builder__c=true,Disable_Validation_Rules__c=true);
        insert byp; 
                
        List<Account> lstAcct = TestDataFactory.createAccount(49, 'Bookstore');
        Test.startTest();
        insert lstAcct;
        Test.stopTest();
                
        //List<Account> lstAcctToUpdate = new <Account>();
        lstAcct[1].Account_Duplicate__c =  lstAcct[0].id;   
        update lstAcct[1];
              
        List<Account_Addresses__c>accAddressList = new  List<Account_Addresses__c>();
        Account_Addresses__c accadd = new Account_Addresses__c(Name='Test',Account__c=lstAcct[0].id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Billing__c=true, Primary__c =true);
        accAddressList.add(accadd);
       
        Account_Addresses__c accadd1 = new Account_Addresses__c(Name='Test1',Account__c=lstAcct[1].id,Address_1__c='Test address1',Source__c='One CRM',Country__c = 'Canada', State__c = 'ALBERTA', City__c = 'Calgary', Zip_Code__c = 'T2V 1G7',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Shipping__c=true,PrimaryShipping__c=true);
        accAddressList.add(accadd1);
        
        Account_Addresses__c accadd2 = new Account_Addresses__c(Name='Testad2',Account__c=lstAcct[1].id,Address_1__c='8416 7 Street SW',Source__c='One CRM',Country__c = 'United Kingdom', State__c = 'London', City__c = 'London', Zip_Code__c = 'OX16 9HY',TEP_Operating_Unit__c='CA Pearson OU',ERP_DEF_BILL_Site_Use_ID__c='1437664',ERP_Billing_Site_Use_ID__c='1437664',Shipping__c=true,PrimaryShipping__c=true,Billing__c=true,Primary__c=true);
        accAddressList.add(accadd2);
                
       MergeAccountsAutomation.MergeAccountsAutomation(lstAcct); 
            }
    }
}