@isTest
private class PS_ClearPotentialTargetsTest
{
    static testMethod void myTest()
    {
     
     //query to get the profile of sales user OneCRM
     Profile pfile = [Select Id,name from profile where name = 'System Administrator'];
     
      //code for creating an User
      User u = new User();
      u.LastName = 'territoryuser';
      u.alias = 'terrusr'; 
      u.Email = 'territoryuser@gmail.com';  
      u.Username='territoryuser@gmail.com';
      u.LanguageLocaleKey='en_US'; 
      u.TimeZoneSidKey='America/New_York';
      u.Price_List__c='Humanities & Social Science';
      u.LocaleSidKey='en_US';
      u.EmailEncodingKey='ISO-8859-1';
      u.ProfileId = pfile.id;             
      u.Geography__c = 'Growth';
      u.Market__c = 'US';
      u.Business_Unit__c = 'US Field Sales';
      u.Line_of_Business__c = 'Higher Ed';
      u.isactive=true;
      u.CurrencyIsoCode='USD';
      insert u;  
      
      system.runas(u){
        //code written for creating Generating Potential Targets
        List<Generate_Potential_Target__c> gptlist = new List<Generate_Potential_Target__c>();
        Generate_Potential_Target__c gpt = new Generate_Potential_Target__c();
        gpt.Action__c ='Create';
        gpt.Opportunity_Type__c= 'Existing Business';
        gpt.Processed__c = false;
        gpt.Product_In_Use_Publisher__c= 'Pearson';
        gpt.Status__c = 'In Progress';
        gpt.TakeAway_Multiple_Frontlist__c = false;
        gpt.CurrencyIsoCode='USD';
        gptlist.add(gpt);
        
        insert gptlist;
        
        //Delete PT 
        PS_ClearPotentialTargets dpt = new PS_ClearPotentialTargets();
        PS_ClearPotentialTargets.clearPotentialTargets();
      }
   }     
}