/* ----------------------------------------------------------------------------------------------------------------------------------------------------------
Name:            GlobalRevenueUpdateOppty_Test.cls 
Description:     Test Class to cover code coverage GlobalRevenueUpdateOppty
Date:           11/21/2017
Author:         Vishista Madati
07/25/2018      Replaced Qualification__c field by QuoteMigration_Qualification__c as part of ZA Apttus Decomm by Mani
------------------------------------------------------------------------------------------------------------------------------------------------------------- */

@isTest
public class GlobalRevenueUpdateOppty_Test {
    static testMethod void unitTest(){
        
        List<User> usList = TestDataFactory.createUser([select Id from Profile where Name =: 'System Administrator'].Id,1); 
        insert usList;
        
        Bypass_Settings__c settings = new Bypass_Settings__c();
        settings.Disable_Validation_Rules__c = true;
        settings.Disable_Triggers__c = false;
        settings.SetupOwnerId = usList[0].id;
        insert settings; 
        List<Account> accs = new List<Account>();
        Account  acc = new Account(Name='Test Account1', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', Line_of_Business__c= 'Schools', Geography__c= 'Core', ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600371');
        acc.Pearson_Campus__c=true;
        accs.add(acc);
        Account  acc1 = new Account(Name='Test Account2', IsCreatedFromLead__c = True,Phone='+91000001',Market2__c= 'UK', Line_of_Business__c= 'Schools', Geography__c= 'Core', ShippingCountry = 'India', ShippingCity = 'Bangalore', ShippingStreet = 'BNG1', ShippingPostalCode = '5600372');
        accs.add(acc1);
        insert accs;
        system.runAs(usList[0]){
            
            test.startTest();
            List<Opportunity> opps = new List<Opportunity>();
            Opportunity opp = new opportunity ( Name = 'OpporName1',AccountId = acc.Id,CloseDate = System.TODAY() + 30,StageName = 'OpporStageName',Channel__c='Direct', QuoteMigration_Qualification__c = 'Graphic Design');
            opps.add(opp);
            Opportunity opp1 = new opportunity ( Name = 'OpporName2',AccountId = acc1.Id,CloseDate = System.TODAY() + 30,StageName = 'OpporStageName',Channel__c='Direct', QuoteMigration_Qualification__c = 'Graphic Design');
            opps.add(opp1);
            insert opps;
            Contact con = new Contact(FirstName='TestContactFirstname1', LastName='TestContactLastname',AccountId=opp.accountid ,Salutation='MR.', Email='happy' + '@email.com',MobilePhone='111222333' , Preferred_Address__c = 'Other Address' , OtherCountry  = 'India' , OtherStreet = 'Test',OtherCity  = 'Test' , OtherPostalCode  = '123456',First_Language__c='English');  
            insert con;
            OpportunityContactRole oc = new OpportunityContactRole( opportunityId =  opp.Id,contactId = con.id ,Role =  'Business User');
            insert oc;
            
            String userId= usList[0].id;
            list<User> currentUser = [SELECT Id, Market__c, Line_of_Business__c, Product_Business_Unit__c FROM User WHERE Id =: userId];
            product2  prod = new product2(name='test', ISBN__c = '9781292099545', Market__c = currentUser[0].Market__c, Line_of_Business__c = currentUser[0].Line_of_Business__c, Business_Unit__c = currentUser[0].Product_Business_Unit__c );
            
            insert prod;
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry standardPrice = new PricebookEntry( Pricebook2Id = pricebookId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true); 
            insert customPB;
            PricebookEntry pbe = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id, UnitPrice = 0, currencyIsocode = userinfo.getDefaultCurrency());
            insert pbe;  
            List<Order> ordlist = new List<Order>();
            RecordType rt = [SELECT Id FROM RecordType WHERE sObjectType = 'Order' AND Name = 'Global Revenue Order'];
            Id sRtId = PS_Util.fetchRecordTypeByName(Order.SobjectType,PS_Constants.ORDER_SAMPLE_ORDER);
            order sampleorder1 = new order(
                Accountid=opp.accountid,
                RecordTypeId =sRtId ,
                ShipToContactid=con.id,Opportunityid=opp.id,
                EffectiveDate=system.today(),Status='New',
                Pricebook2Id=customPB.id,CurrencyIsoCode = userinfo.getDefaultCurrency(),
                type=System.Label.Order_Type, ShippingCountry = 'India', ShippingCity = 'Bangalore', 
                ShippingStreet = 'BNG1', ShippingPostalCode = '5600371', ShippingState = 'Karnataka', 
                Salesperson__c = usList[0].id,
                Business_Unit__c='Higher Ed',Market__c = 'UK',group__c = null,ERP_Order_Type__c='HE-US-SAMPLE'
            ); 
            
            insert sampleorder1;
            test.stopTest();
            sampleorder1.RecordTypeId = rt.id;
            sampleorder1.Status = 'Booked';
            sampleorder1.Opportunityid = opp1.id;
            update sampleorder1;
            sampleorder1.Status = System.Label.Cancelled;
            update sampleorder1;
        }
    }
}