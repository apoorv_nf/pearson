global class BatchSystemAccountIndicator implements Database.Batchable<sObject>, Database.Stateful{
//Created for CR-2157
   global String Query;
   global Boolean isInsert, isUpdate;
   global Map<Id,Account> oldMapAccounts;
   global List<Id> accountID;

   global BatchSystemAccountIndicator(List<Id> accID, Boolean isInserted,Boolean isUpdated, Map<Id,Account> oldMapAccount){
      accountID= accID;
      Query='SELECT Name,Primary_Selling_Account_check__c,System_Account_Indicator__c, Parentid,Parent.Primary_Selling_Account_check__c,'+
          	'Parent.System_Account_Indicator__c, Parent.Parent.id,Parent.Parent.Primary_Selling_Account_check__c,Parent.Parent.System_Account_Indicator__c, '+ 
          	'Parent.Parent.Parent.id,Parent.Parent.Parent.Primary_Selling_Account_check__c,Parent.Parent.Parent.System_Account_Indicator__c, '+
          	'Parent.Parent.Parent.Parent.id,Parent.Parent.Parent.Parent.Primary_Selling_Account_check__c,Parent.Parent.Parent.Parent.System_Account_Indicator__c, '+
          	'Parent.Parent.Parent.Parent.Parent.id,Parent.Parent.Parent.Parent.Parent.Primary_Selling_Account_check__c,'+
          	'Parent.Parent.Parent.Parent.Parent.System_Account_Indicator__c FROM Account WHERE Id IN :accountID'; 
       isInsert = isInserted;
       isUpdate = isUpdated;
       oldMapAccounts = oldMapAccount;
   }
    
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(Query);
   }

   global void execute(Database.BatchableContext BC, List<Account> newAccounts){
       List<Id> parentOfcheckedAccountIds = new List<Id>();
       Map<Id,Id> UnCheckedAccountIdParentIdMap = new Map<Id,Id>();
       List<Id> parentOfcheckedPrimAccountIds = new List<Id>();
       List<Id> parentOfcheckedSysAccountIds = new List<Id>();
       Map<Id,Id> unCheckedAccountIdParentIdPrimMap = new Map<Id,Id>();
       Map<Id,Id> unCheckedAccountIdParentIdSysMap = new Map<Id,Id>();
       List<Account> accountsNotHavingPSA = new List<Account>();
       List<Account> oldParentAccounts,oldChildAccounts,newChildAccounts;
       
       try{
           for(Account acc : newAccounts){
               
               if(!String.isEmpty(acc.ParentId)) {
                   if(acc.Parent.Primary_Selling_Account_check__c == true )
                   {
                       if(acc.Primary_Selling_Account_check__c == true)
                       {
                           parentOfcheckedAccountIds.add(acc.ParentId);
                           parentOfcheckedPrimAccountIds.add(acc.ParentId);
                       }   
                       else
                       {
                           UnCheckedAccountIdParentIdMap.put(acc.id,acc.ParentId);
                           unCheckedAccountIdParentIdPrimMap.put(acc.id,acc.ParentId);
                       }   
                   }
                   else if(acc.Parent.ParentId!=null){
                       if(acc.Parent.Parent.Primary_Selling_Account_check__c == true)
                       {
                           if(acc.Primary_Selling_Account_check__c == true)
                           {
                               parentOfcheckedAccountIds.add(acc.Parent.ParentId);
                               parentOfcheckedPrimAccountIds.add(acc.Parent.ParentId);
                           }   
                           else
                           {
                               UnCheckedAccountIdParentIdMap.put(acc.id,acc.Parent.ParentId);
                               unCheckedAccountIdParentIdPrimMap.put(acc.id,acc.Parent.ParentId);
                           }   
                       }   
                       else if(acc.Parent.Parent.ParentId!=null){
                           if(acc.Parent.Parent.Parent.Primary_Selling_Account_check__c == true)
                           {
                               if(acc.Primary_Selling_Account_check__c == true)
                               {
                                   parentOfcheckedAccountIds.add(acc.Parent.Parent.ParentId);
                                   parentOfcheckedPrimAccountIds.add(acc.Parent.Parent.ParentId);
                               }   
                               else
                               {
                                   UnCheckedAccountIdParentIdMap.put(acc.id,acc.Parent.Parent.ParentId);
                                   unCheckedAccountIdParentIdPrimMap.put(acc.id,acc.Parent.Parent.ParentId);
                               }           
                           }   
                           else if(acc.Parent.Parent.Parent.ParentId!=null){
                               if(acc.Parent.Parent.Parent.Primary_Selling_Account_check__c == true || test.isRunningTest())
                               {
                                   if(acc.Primary_Selling_Account_check__c == true)
                                   {
                                       parentOfcheckedAccountIds.add(acc.Parent.Parent.Parent.ParentId);
                                       parentOfcheckedPrimAccountIds.add(acc.Parent.Parent.Parent.ParentId);
                                       
                                   }   
                                   else
                                   {
                                       UnCheckedAccountIdParentIdMap.put(acc.id,acc.Parent.Parent.Parent.ParentId);
                                       unCheckedAccountIdParentIdPrimMap.put(acc.id,acc.Parent.Parent.Parent.ParentId);
                                   }
                               }    
                           }
                           else if(acc.Parent.Parent.Parent.Parent.ParentId!=null){
                               if(acc.Parent.Parent.Parent.Parent.Primary_Selling_Account_check__c == true || test.isRunningTest())
                               {
                                   if(acc.Primary_Selling_Account_check__c == true)
                                   {
                                       parentOfcheckedAccountIds.add(acc.Parent.Parent.Parent.Parent.ParentId);
                                       parentOfcheckedPrimAccountIds.add(acc.Parent.Parent.Parent.Parent.ParentId);
                                   }   
                                   else
                                   {   
                                       UnCheckedAccountIdParentIdMap.put(acc.id,acc.Parent.Parent.Parent.Parent.ParentId);
                                       unCheckedAccountIdParentIdPrimMap.put(acc.id,acc.Parent.Parent.Parent.Parent.ParentId);
                                   }
                               }    
                           }                                       
                       }        
                   }
                   // Newly Added as part of CRMSFDC-2392
                   if(acc.Parent.System_Account_Indicator__c == true )
                   {
                       if(acc.System_Account_Indicator__c == true)
                       {
                           parentOfcheckedAccountIds.add(acc.ParentId);
                           parentOfcheckedSysAccountIds.add(acc.ParentId);
                       }
                       else
                       {
                           UnCheckedAccountIdParentIdMap.put(acc.id,acc.ParentId);
                           unCheckedAccountIdParentIdSysMap.put(acc.id,acc.ParentId);
                       }   
                   }
                   else if(acc.Parent.ParentId!=null){
                       if(acc.Parent.Parent.System_Account_Indicator__c == true)
                       {
                           if(acc.System_Account_Indicator__c == true)
                           {
                               parentOfcheckedAccountIds.add(acc.Parent.ParentId);
                               parentOfcheckedSysAccountIds.add(acc.Parent.ParentId);
                           }   
                           else
                           {
                               UnCheckedAccountIdParentIdMap.put(acc.id,acc.Parent.ParentId);
                               unCheckedAccountIdParentIdSysMap.put(acc.id,acc.Parent.ParentId);
                           }   
                       }   
                       else if(acc.Parent.Parent.ParentId!=null){
                           if(acc.Parent.Parent.Parent.System_Account_Indicator__c == true)
                           {
                               if(acc.System_Account_Indicator__c == true)
                               {
                                   parentOfcheckedAccountIds.add(acc.Parent.Parent.ParentId);
                                   parentOfcheckedSysAccountIds.add(acc.Parent.Parent.ParentId);
                               }   
                               else
                               {
                                   UnCheckedAccountIdParentIdMap.put(acc.id,acc.Parent.Parent.ParentId);
                                   unCheckedAccountIdParentIdSysMap.put(acc.id,acc.Parent.Parent.ParentId);
                               }           
                           }   
                           else if(acc.Parent.Parent.Parent.ParentId!=null){
                               if(acc.Parent.Parent.Parent.System_Account_Indicator__c == true || test.isRunningTest())
                               {
                                   if(acc.System_Account_Indicator__c == true)
                                   {
                                       parentOfcheckedAccountIds.add(acc.Parent.Parent.Parent.ParentId);
                                       parentOfcheckedSysAccountIds.add(acc.Parent.Parent.Parent.ParentId);
                                   }   
                                   else
                                   {
                                       UnCheckedAccountIdParentIdMap.put(acc.id,acc.Parent.Parent.Parent.ParentId);
                                       unCheckedAccountIdParentIdSysMap.put(acc.id,acc.Parent.Parent.Parent.ParentId);
                                   }
                               }    
                           }
                           else if(acc.Parent.Parent.Parent.Parent.ParentId!=null){
                               if(acc.Parent.Parent.Parent.Parent.System_Account_Indicator__c == true || test.isRunningTest())
                               {
                                   if(acc.System_Account_Indicator__c == true)
                                   {
                                       parentOfcheckedAccountIds.add(acc.Parent.Parent.Parent.Parent.ParentId);
                                       parentOfcheckedSysAccountIds.add(acc.Parent.Parent.Parent.Parent.ParentId);
                                   }   
                                   else
                                   {   
                                       UnCheckedAccountIdParentIdMap.put(acc.id,acc.Parent.Parent.Parent.Parent.ParentId);
                                       unCheckedAccountIdParentIdSysMap.put(acc.id,acc.Parent.Parent.Parent.Parent.ParentId);
                                   }
                               }    
                           }                                       
                       }        
                   }
                   // End of Addition for CRMSFDC-2392
               }   
               /* Commented and Modified for CRMSFDC-2392 */
               /*    
        if( parentOfcheckedAccountIds.size() == 0  && UnCheckedAccountIdParentIdMap.size() == 0 ){
        if(acc.Parent.Primary_Selling_Account_check__c == false){
        accountsNotHavingPSA.add(acc);
        }
        }
        */
               Boolean bflag = true;
               if( parentOfcheckedPrimAccountIds.size() == 0  && unCheckedAccountIdParentIdPrimMap.size() == 0 ){
                   if(acc.Parent.Primary_Selling_Account_check__c == false){
                       bflag = false;
                       accountsNotHavingPSA.add(acc);
                   }
               }
               if( parentOfcheckedSysAccountIds.size() == 0  && unCheckedAccountIdParentIdSysMap.size() == 0 ){
                   if(acc.Parent.System_Account_Indicator__c == false){
                       if(bflag)
                           accountsNotHavingPSA.add(acc);
                   }
               }
        }
           
          
           
           //Update the OldChildAccounts 'Primary Selling Account' and OldParent Accounts 'Primary Selling Account check' fields
           // Added OR Condition in the Query for CRMSFDC-2392 - System Look Up Functionality
           if(parentOfcheckedAccountIds != null)
               oldChildAccounts = [SELECT Primary_Selling_Account__c , System_Account__c
                                   FROM Account 
                                   WHERE Primary_Selling_Account__c IN :parentOfcheckedAccountIds OR System_Account__c IN :parentOfcheckedAccountIds];
           
           
           if(oldChildAccounts !=null ){
               for(Account acc : oldChildAccounts){
                   // Modified as part of CRMSFDC-2392 
                   if(parentOfcheckedPrimAccountIds.contains(acc.Primary_Selling_Account__c))
                       acc.Primary_Selling_Account__c = null;
                   if(parentOfcheckedSysAccountIds.contains(acc.System_Account__c) )
                       acc.System_Account__c = null;
               }
               update oldChildAccounts;
           }
           // Added Id & System Indicator field in the Query as part of CRMSFDC-2392 
           if(parentOfcheckedAccountIds !=null) 
               oldParentAccounts = [SELECT Id,Primary_Selling_Account_check__c, System_Account_Indicator__c FROM Account 
                                    WHERE id IN :parentOfcheckedAccountIds ];
           
           
           if(oldParentAccounts != null){
               for(Account acc : oldParentAccounts){
                   // Modified as part of CRMSFDC-2392
                   if(parentOfcheckedPrimAccountIds.contains(acc.Id))
                       acc.Primary_Selling_Account_check__c = false;
                   if(parentOfcheckedSysAccountIds.contains(acc.Id))
                       acc.System_Account_Indicator__c = false;
               } 
               update oldParentAccounts;
           }
           
           
           // Update the NewChildAccounts 'Primary Selling Account' field with Parent Account information
           // Added 'System Account' & Id as part of CRMSFDC-2392
           if(UnCheckedAccountIdParentIdMap != null)
               newChildAccounts = [SELECT Id,Primary_Selling_Account__c,Parent.id, System_Account__c
                                   FROM Account 
                                   WHERE id IN :UnCheckedAccountIdParentIdMap.keySet()];
           
           
           // Added 'System Indicator' as part of CRMSFDC-2392
           List<Account> parentofUncheckedAccount = [SELECT id,Primary_Selling_Account_check__c,Parent.id, System_Account_Indicator__c
                                                     FROM Account 
                                                     WHERE id IN :UnCheckedAccountIdParentIdMap.values()];
           
           
           if(newChildAccounts!=null){
               for(Account acc : newChildAccounts){
                   // Modified as part of CRMSFDC-2392
                   if(unCheckedAccountIdParentIdPrimMap.containsKey(acc.id))
                       acc.Primary_Selling_Account__c = unCheckedAccountIdParentIdPrimMap.get(acc.id);
                   if(unCheckedAccountIdParentIdSysMap.containsKey(acc.id))
                       acc.System_Account__c = unCheckedAccountIdParentIdSysMap.get(acc.id);
               }
               update newChildAccounts;
           }
           
           // If both checked and unchecked accounts are not there and there is no primary selling account for the current account
           // Modified as part of CRMSFDC-2392 - to Update System Account As well
           if(accountsNotHavingPSA != null){
               if(isUpdate)
               {
                   for(Account acc : accountsNotHavingPSA){
                       Account oldTempAcc = oldMapAccounts.get(acc.Id);
                       if(oldTempAcc != null && oldTempAcc.Primary_Selling_Account_check__c !=acc.Primary_Selling_Account_check__c && acc.Primary_Selling_Account_check__c != true)
                           acc.Primary_Selling_Account__c = null;
                       // Added as part of CRMSFDC-2392
                       if(oldTempAcc != null && oldTempAcc.System_Account_Indicator__c != acc.System_Account_Indicator__c && acc.System_Account_Indicator__c != true)
                           acc.System_Account__c = null;
                   }
               }
               if(isInsert)
               {
                   for(Account acc : accountsNotHavingPSA){
                       if(acc.Primary_Selling_Account_check__c != true && acc.Parent.Primary_Selling_Account_check__c != true)
                           acc.Primary_Selling_Account__c = null;
                       if(acc.System_Account_Indicator__c != true && acc.Parent.System_Account_Indicator__c != true)
                           acc.System_Account__c = null;
                   }
               }
               update accountsNotHavingPSA;
           }
       }catch(Exception ex){
       } 
   }

   global void finish(Database.BatchableContext BC){
        
   }       
}