/*
 * Author: Matthew Mitchener (Neuraflash)
 * Date: 29/05/2018
 */
public class SmsApi {

	public class AccessCodeDetails {
		public String cashedIn, poReqd, status, valid, reason;
		public Date cashedInDate;
		public Datetime validFromDate, expirationDate, creationDate;
		public Integer numCashins, numTimesCashedIn, errorCode;
	}

	public class Identity {
		public String tokenId, successUrl, realm;
	}

	public class CurrentGenProduct {
		public String valid, reason;
		public List<String> productIds;
	}

	public class CurrentGenCourse {
		public List<Product> products;
		public String title, id, description, endDate, kioskCoursePartner, extCourseId, valid, reason;
		public Boolean standaloneCourse;
	}

	public class Product {
		public String currencyCode, description, type, abbrev;
		public Decimal price;
		public Integer id;
	}

	public class Enrollment {
		public String enrollable, enrollBeginDate, enrollEndDate;
		public List<String> errors;
		public Instructor instructor;
	}

	public class Instructor {
		public String title, firstName, lastName, email;
		public Institution institution;
	}

	public class Institution {
		public String name, otherStateProv, city, zipCode, countryCode;
		public Integer countryId, stateId, institutionId, socratesId;
	}
}