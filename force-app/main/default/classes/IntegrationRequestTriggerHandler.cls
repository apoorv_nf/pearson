/************************************************************************************************************
* Apex Class Name   : IntegrationRequestTriggerHandler
* Version           : 1.0 
* Created Date      : 20 March 2015
* Function          : Handler class for Integration_Request__c custom object trigger
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
*                             20/03/2015              Created Default Handler Class Template
************************************************************************************************************/
public with sharing class IntegrationRequestTriggerHandler
{
    IntegrationFrameworkUtilities IntFramUtil = new IntegrationFrameworkUtilities();
    IntegrationFrameworkOuboundQueue IntFramOutboundQueue = new IntegrationFrameworkOuboundQueue();
    
    public void OnBeforeInsert(List<Integration_Request__c> listNewIntegrationRequest)
    {
        //Set the JSON Representation of the Integration Request object
        IntFramUtil.SetObjectsJSONRepresentation(listNewIntegrationRequest);
        
        //Set the initial status of the new outbound Integration Request Records
        IntFramOutboundQueue.SetNewIntReqStatus(listNewIntegrationRequest);
        
    }
    
    public void OnAfterInsert(List<Integration_Request__c> listNewIntegrationRequest, Map<ID,Integration_Request__c> mapNewIntegrationRequest)
    {
        
    }
    
    public void OnBeforeUpdate(List<Integration_Request__c> listOldIntegrationRequest, List<Integration_Request__c> listNewIntegrationRequest, Map<ID,Integration_Request__c> mapOldIntegrationRequest, Map<ID,Integration_Request__c> mapNewIntegrationRequest)
    {
        System.debug('entering on before update1');
        Set<id> ircaseid = new Set<id>();
        map<id,Integration_Request__c> irMap = new map<id,Integration_Request__c>();
        List<Case> caselist = new List<Case>();
        List<Case> updCaseList = new List<Case>();
        //Set status based on change to SystemResponse
        LIST<Integration_Request__c> lstIntRqs=new LIST<Integration_Request__c>();
        for(Integration_Request__c newRec:listNewIntegrationRequest) {
            if(newRec.status__c=='Received' && mapOldIntegrationRequest.get(newRec.Id).Status__c <> 'Ready For Submission') {
                newRec.Status__c= mapOldIntegrationRequest.get(newRec.Id).Status__c;
            } 
        }
        // IntegrationFrameworkUtilities.SetStatusBasedSystemResponse(listNewIntegrationRequest);
        for(Integration_Request__c ir : listNewIntegrationRequest){
            if(ir.Object_Name__c == 'Case' && (ir.Status__c == 'Technical Error' || ir.Status__c == 'Functional Error')){
                ircaseid.add(ir.object_Id__c);
                irMap.put(ir.Object_Id__c, ir);
            }
        }
         System.debug('entering on before update2' +ircaseid);
        if(!ircaseid.isEmpty()){
        System.debug('entering on before update2');
        caselist = [select id,CaseNumber,ContactId, AccountId, OwnerId,owner.email from case where id in : ircaseid];
         for(case tcase : caselist)
        {   
            
            Case tmpCase = new Case();
            tmpCase = tcase;
            tmpCase.IsTriggerUpdated__c = true;
            updCaseList.add(tmpCase);
        }
        update updCaseList;
        updCaseList.clear();
        for(case tcase : caselist)
        {   
            
            Case tmpCase = new Case();
            tmpCase = tcase;
            tmpCase.OwnerId = irMap.get(tcase.Id).CreatedById;
            tmpCase.Escalation_Point__c = '';
            tmpCase.status = 'Additional Information Received';
            updCaseList.add(tmpCase);
        }
        update updCaseList;
        }}
    
    public void OnAfterUpdate(List<Integration_Request__c> listOldIntegrationRequest, List<Integration_Request__c> listNewIntegrationRequest, Map<ID,Integration_Request__c> mapOldIntegrationRequest, Map<ID,Integration_Request__c> mapNewIntegrationRequest)
    {
        // Queueable class that checks if the status has been changed to Complete, Functional Error or Technical error and if so launch the corresponding handler flows 
        System.enqueueJob(new IntegrationFrameworkLaunchHandlerFlow(listNewIntegrationRequest));
        
        
    }
    
    public void irOnCaseFails(List<Integration_Request__c> listNewIntegrationRequest){
        System.debug('entering case failure');
        Set<id> ircaseid = new Set<id>();
        //List<Integration_Request__c> irlist = new List<Integration_Request__c>();
        //map<id,Integration_Request__c> irmaplist = new map<id,Integration_Request__c>();
        List<Case> caselist = new List<Case>();
        Set<Id> usrid = new Set<Id>();
        List<Case> updCaseList = new List<Case>();
        for(Integration_Request__c ir : listNewIntegrationRequest){
            if(ir.Object_Name__c == 'Case' && (ir.Status__c == 'Technical Error' || ir.Status__c == 'Functional Error')){
               ircaseid.add(ir.object_Id__c); 
               usrid.add(ir.createdbyId);
            }
        }
        System.debug('@@@@@ircaseid'+ircaseid);
        System.debug('@@@@@usrid'+usrid);
        
        
        //irlist = [select id,Object_Id__c,Error_Code__c,Error_Description__c from Integration_Request__c where id in : irid ];
        // String ownerEmail = UserInfo.getUserEmail();
        
        if(!ircaseid.isEmpty()){
        caselist = [select id,CaseNumber,ContactId, AccountId, OwnerId,owner.email from case where id in : ircaseid];
        Map<Id,User> usrMap = new Map<Id,User>([Select Id, Email from user where id in:usrid]);     
        List<Wrapper> wprlst = new List<Wrapper>();
        //If(listNewIntegrationRequest.size() > 0){
        If(!listNewIntegrationRequest.isEmpty()){
            for(integer i=0;i<listNewIntegrationRequest.size();i++){
                wprlst.add(new wrapper(listNewIntegrationRequest[i].Object_Id__c,listNewIntegrationRequest[i].Error_Code__c,listNewIntegrationRequest[i].Error_Description__c,usrMap.get(listNewIntegrationRequest[i].CreatedById).email,caselist[i].CaseNumber));
                system.debug('@@@@@@@@@@@@@@@@@@@@@@'+ wprlst);
            }
        }
            
         for(case tcase : caselist)
            {   
                Case tmpCase = new Case();
                tmpCase = tcase;
                tmpCase.IsTriggerUpdated__c = false;
                updCaseList.add(tmpCase);
            }
            
        update updCaseList;
        sendMailCaseFeed(wprlst);}
    }
    
    public class Wrapper{
        public string caseid{get;set;}
        public string ErrorCode{get;set;}
        public string ErrorDescription{get;set;}
        public string caseescalateduseremail{get;set;}
        public string CaseNumber{get;set;}
        
        public Wrapper(String caseid,String ErrorCode, String ErrorDescription, String caseescalateduseremail, String CaseNumber){
            this.caseid = caseid;
            this.ErrorCode = ErrorCode;
            this.ErrorDescription = ErrorDescription;
            this.caseescalateduseremail = caseescalateduseremail;
            this.CaseNumber = CaseNumber;
            
        }
        
        
    } 
    
    public void OnBeforeDelete(List<Integration_Request__c> listOldIntegrationRequest, Map<ID,Integration_Request__c> mapOldIntegrationRequest)
    {
        
    }
    
    public void OnAfterDelete(List<Integration_Request__c> listOldIntegrationRequest, Map<ID,Integration_Request__c> mapOldIntegrationRequest)
    {
        
    }
    
    public void OnUndelete(List<Integration_Request__c> listNewIntegrationRequest, Map<ID,Integration_Request__c> mapNewIntegrationRequest)
    {
        
    }
    
    public void sendMailCaseFeed(List<Wrapper> wprlst){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'mayank.lal@pearson.com'];
        for(Wrapper w : wprlst){
            
            /* List<String> fromAddress = new List<String>();
fromAddress.add(Label.IR_Case_Escalation_FromAddress);
mail.setFromAddress(fromAddress);*/
            List<String> sendTo = new List<String>();
            sendTo.add(w.caseescalateduseremail);
            mail.setToAddresses(sendTo);
            
            //mail.setReplyTo(Label.IR_Case_Escalation_FromAddress);     // Added by Mayank       
            
            
            /*List<String> lstReplyToAddress = new list<String>();
            lstReplyToAddress.add(Label.IR_Case_Escalation_FromAddress);
            System.debug('@@lstReplyToAddress '+lstReplyToAddress);
            if(!lstReplyToAddress.isEmpty()){
                mail.setReplyTo(lstReplyToAddress[0]);
            }*/
            
            
                if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }
            List<IR_Case_Escalation_Fails_ccTO__c> ccTo = IR_Case_Escalation_Fails_ccTO__c.getall().values();
            List<String> cclist = new List<String>();
           
               if(ccTo.size()>0 && ccTo.size()<25)
                    {
                        for(integer i =0;i<ccTo.size();i++)
                        {
                            cclist.add(String.Valueof(ccTo[i].CC_Address__c));
                            system.debug('ccto ############# ' + ccTo);

                        }
                        mail.setCcAddresses(cclist);
                    }
                    
           String Subject = 'Case ' + w.CaseNumber + ' failed to create an incident in Service Now';          
           
           String body = 'Case ' + w.CaseNumber +' failed to create an incident in Service Now for the following reason: '+ w.ErrorDescription ;
            body += '<p><b>Please clone the case and escalate the clone. The API will not pick up the escalation by changing the escalation point. Service Now ticket stays in Pending status.</b></p>';
            body += '<p>Please click the link below to view the record. </p>'; 
            body += '<p></p>';
            body += URL.getSalesforceBaseUrl().toExternalForm() +'/'+w.caseid;
            
            mail.setHtmlBody(body);
            mail.setSubject(Subject);
            /*mail.setHtmlBody('Case ' + w.CaseNumber +' failed to create an incident in Service Now for the following reason: '+ w.ErrorDescription +' <p> '+
                              'Please click the link below to view the record.</p> <p>' +
                               URL.getSalesforceBaseUrl().toExternalForm()+'/'+w.caseid</p>');*/
           // system.debug('body ########'+ body);
            mails.add(mail);
            System.debug('@@mail '+mail);
            Messaging.sendEmail(mails);
            
            FeedItem post = new FeedItem();
            //String URL1 = String.Valueof('https://pearson.my.salesforce.com/');
            post.Title = 'Case'  + w.CaseNumber + 'failed to create an incident in Service Now ';         
            String feedbody = 'Case ' + w.CaseNumber +  '\t\t failed to create an incident in Service Now for the following reason: '+ w.ErrorDescription +' \n\n'; 
            feedbody += 'Please clone the case and escalate the clone. The API will not pick up the escalation by changing the escalation point. Service Now ticket stays in Pending status. \n\n'; 
            feedbody += 'Please click the link below to view the record. \n\n'; 
            feedbody += URL.getSalesforceBaseUrl().toExternalForm() +'/'+w.caseid;
            post.Body = feedbody;
            post.ParentId = w.caseid;
            insert post;
            
        }      
    } 
    
}