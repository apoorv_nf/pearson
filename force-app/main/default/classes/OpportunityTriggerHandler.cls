/************************************************************************************************************
* Apex Class Name   : OpportunityTriggerHandler.cls
* Version           : 1.0 
* Created Date      : 06 MARCH 2014
* Function          : Handler class for Opportunity Object Trigger  
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Mitch Hunt                  06/03/2014               Created Default Handler Class Template
* Ashleey Ashokkumar          16/09/2014              disabled oppty renewal functionality
* Leonard Victor              24/08/2015               RD-01257 - Moved R1 related code to
* Kameswari                   09/2015                 To enable clonning opportunities with product with different pricebook set 
* Gousia begum                1/10/2015              B2B2L Record type added
* Saritha Singamsetty         05/10/2017             Added code for CR-01637 for default selling period on B2B opportunity in before insert
*Krishna priya                05/25/2018             Added a function as a part of CR-01908- Req 21 and Req 122:call to find out whether a opportunity has a contact associated in the opportunity contact roles
************************************************************************************************************/

public with sharing class OpportunityTriggerHandler
{
    private boolean m_bIsExecuting = false;
    private integer iBatchSize = 0;
    
    public OpportunityTriggerHandler(boolean bIsExecuting, integer iSize)
    {
        m_bIsExecuting = bIsExecuting;
        iBatchSize = iSize;
    }
    
    OpportunityUtils Utils = new OpportunityUtils();
    
    // EXECUTE BEFORE INSERT LOGIC
    //
    public void OnBeforeInsert(Opportunity[] lstNewOpportunities)
    {
        /* AA (Deloitte) 16/09/2014 - disabled oppty renewal functionality
         *
        list<Opportunity> lstOpportunitiestoEval        = new list<Opportunity>();
        list<Opportunity> listOpportunitestoClone       = new list<Opportunity>();
        set<Id> setOpptiesToClone                       = new set<Id>();
        set<String> setCloneOpptyIds                    = new set<String>();
        
        for(Opportunity sNewOpportunity : lstNewOpportunities)
        {           
            if(sNewOpportunity.IsClosed)
            {
                // set clone value to null - if they're cloning an oppty that has a clone, this one doesn't have a clone
                sNewOpportunity.Clone_Oppty__c = null;
                // add it to the list we need to clone
                listOpportunitestoClone.add(sNewOpportunity);
                setOpptiesToClone.add(sNewOpportunity.Id);
            }
        }
        // Clone opportunities that have been entered as closed
        Utils.createReEngagementClones(setOpptiesToClone, setCloneOpptyIds, listOpportunitestoClone);
        
        */
        
        //  Logic to default the Price book to standartd pricebook on Opportunity record type = D2L
         List<Pricebook2> stdPBL = [select id from Pricebook2 where IsStandard = TRUE LIMIT 1];//ONECRM Price Book
        List<Pricebook2> ANZPBL = [select id from Pricebook2 where Name like 'ANZ%']; // ANZ Price Book
        Id recordIdB2B = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
        //Id B2BRecId = [SELECT Id FROM RecordType where name = 'B2B'and SobjectType = 'Opportunity'].Id;
        Id d2lRecordType;
        Id b2bRecordType;
         Id opptyRecordType;
        
        //Using Util class to fetch record type instead of SOQL
            if(PS_Util.recordTypeMap.containsKey(PS_Constants.OPPORTUNITY_OBJECT) && (PS_Util.recordTypeMap.get(PS_Constants.OPPORTUNITY_OBJECT).containsKey(PS_Constants.OPPORTUNITY_D2L_RECCORD) && PS_Util.recordTypeMap.get(PS_Constants.OPPORTUNITY_OBJECT).containsKey(PS_Constants.OPPORTUNITY_B2B_RECCORD) &&  PS_Util.recordTypeMap.get(PS_Constants.OPPORTUNITY_OBJECT).containsKey(PS_Constants.OPPORTUNITY_OPPORTUNITY_RECCORD))){                          
                    d2lRecordType = PS_Util.recordTypeMap.get(PS_Constants.OPPORTUNITY_OBJECT).get(PS_Constants.OPPORTUNITY_B2B_RECCORD); 
                    b2bRecordType = PS_Util.recordTypeMap.get(PS_Constants.OPPORTUNITY_OBJECT).get(PS_Constants.OPPORTUNITY_D2L_RECCORD);   
                    opptyRecordType = PS_Util.recordTypeMap.get(PS_Constants.OPPORTUNITY_OBJECT).get(PS_Constants.OPPORTUNITY_OPPORTUNITY_RECCORD);   
            }
            else{
                    d2lRecordType = PS_Util.fetchRecordTypeByName(Opportunity.SobjectType,PS_Constants.OPPORTUNITY_D2L_RECCORD);
                    b2bRecordType = PS_Util.fetchRecordTypeByName(Opportunity.SobjectType,PS_Constants.OPPORTUNITY_B2B_RECCORD); 
                    // CR-01558: Replacing the hardcoded value for Opportunity record type with custom label 
                    opptyRecordType = PS_Util.fetchRecordTypeByName(Opportunity.SobjectType,Label.PS_OpportunityRecordType);   
            }  
           //End of Record type fix     
        
        if(!stdPBL.isEmpty()){
            for(Opportunity o: lstNewOpportunities){
                 //adding the condition to clone opportunity with product with different pricebook set
                if(o.Pricebook2 != null)
                //Updated Record type check for R3 Code stream line
                if(o.RecordTypeId == d2lRecordType ||o.RecordTypeId == b2bRecordType){
                    o.PriceBook2Id = stdPBL[0].id;
                }
            }
        }
        
        
        if(!ANZPBL.isEmpty()){
            for(Opportunity o: lstNewOpportunities){
                //adding the condition to clone opportunity with product with different pricebook set
                if(o.Pricebook2 != null)
                //Updated Record type check for R3 code stream line
                if(o.RecordTypeId == opptyRecordType){
                    o.PriceBook2Id = ANZPBL[0].id;
                }
            }
        }
        
        for(Opportunity o: lstNewOpportunities){
            o.Academic_Start_Date__c = AcademicStartDate();
            // Adding code to populate selling period on creation of new opportunity.
            //system.debug('@@@B2BRT'+B2BRecId);
            //system.debug('@@@B2BRTID'+o.RecordTypeId);
            if(o.RecordTypeID == recordIdB2B && o.Selling_Period__c == 'None')
            {
                if(system.Date.today().month() == 12 || (system.Date.today().month() >=1 && system.Date.today().month() <= 5)){
                if(system.Date.today().month() == 12){
                    o.Selling_Period__c = string.valueof(system.Date.today().year()+1)+' - Fall';
                   // system.debug('@@@12' +o.Selling_Period__c);
                }else{
                    o.Selling_Period__c = string.valueof(system.Date.today().year())+' - Fall';
                   // system.debug('@@@1215' +o.Selling_Period__c);
                }
                
            }else if(system.Date.today().month() >= 6 && system.Date.today().month() <= 11){
                o.Selling_Period__c = string.valueof(system.Date.today().year()+1)+' - Spring';
               // system.debug('@@@611' +o.Selling_Period__c);
                }
            }
        }         
        
        
    }
    
    // EXECUTE AFTER INSERT LOGIC
    //
    public void OnAfterInsert(Opportunity[] lstNewOpportunities)
    {
        List<Contract> conttoinsert = new List<Contract>();

         for (Opportunity opp : lstNewOpportunities)
        {
            if(opp.Registration_Payment_Reference__c != null && opp.Received_Signed_Registration_Contract__c){
            //if(opp.Registration_Payment_Reference1__c && opp.Received_Signed_Registration_Contract__c){
                system.debug('================opp=============='+opp);
                Contract con = new Contract();
                con.Opportunity__c = opp.Id;
                con.AccountId= opp.AccountId;
                con.StartDate =system.today();
                con.ContractTerm = 4; 
                conttoinsert.add(con); // For Bulk processing of the Records.
            }
            // End of For
        }
        
        if ( !conttoinsert.isEmpty())
        {
            insert conttoinsert;
        }
          
    
    }
    
     
    
    // BEFORE UPDATE LOGIC
    //
    public void OnBeforeUpdate(Opportunity[] lstOldOpportunities, Opportunity[] lstUpdatedOpportunities, map<ID, Opportunity> mapIDOpportunity)
    {
        list<Opportunity> lstOpportunitiestoEval        = new list<Opportunity>();
        
        /* AA (Deloitte) 16/09/2014 - disabled oppty renewal functionality
         *
        list<Opportunity> listOpportunitestoClone       = new list<Opportunity>();
        set<Id> setOpptiesToClone                       = new set<Id>();
        set<String> setCloneOpptyIds                    = new set<String>();
        */
        for(Opportunity sOldOpportunity : lstOldOpportunities)
        {
            Opportunity sNewOpportunity = mapIDOpportunity.get(sOldOpportunity.Id);
            /* AA (Deloitte) 16/09/2014 - disabled oppty renewal functionality
             *
            if(sOldOpportunity.IsClosed == false && sNewOpportunity.IsClosed == true)
            {
                setOpptiesToClone.add(sNewOpportunity.Id);
                listOpportunitestoClone.add(sNewOpportunity);
                if (sNewOpportunity.Clone_Oppty__c <> null)
                {
                    setCloneOpptyIds.add(sNewOpportunity.Clone_Oppty__c);
                }
            }*/
                // If an opportunity stage or status has changed evaluate if they need a task created.
            if( sOldOpportunity.StageName != sNewOpportunity.StageName ||
                sOldOpportunity.Status__c != sNewOpportunity.Status__c)
            {
                lstOpportunitiestoEval.add(sNewOpportunity);
            }
        }
        /* AA (Deloitte) 16/09/2014 - disabled oppty renewal functionality
         *
        Utils.createReEngagementClones(setOpptiesToClone, setCloneOpptyIds, listOpportunitestoClone);
        */
        Utils.createFollowUpActivities(lstOpportunitiestoEval);
    }
    
    
   
    public boolean bIsTriggerContext
    {
        get{ return m_bIsExecuting; }
    }
    
    public boolean bIsVisualforcePageContext
    {
        get{ return !bIsTriggerContext; }
    }
    
    public boolean bIsWebServiceContext
    {
        get{ return !bIsTriggerContext; }
    }
    
    public boolean bIsExecuteAnonymousContext
    {
        get{ return !bIsTriggerContext; }
    }
    
   
    
    //writen :@sachin Kadam Date :4/10/2015
    // logic to set the Academic Start Date Based using  4 Academic start date fields on the Custom Settings:
    // The rule for setting the date should be as follows:
    // Select the date that is the closet to the current date
    // Please keep in mind that some of these may be blank initially so we should ignore a blank field

    public Date AcademicStartDate(){
         List<Date> AcademicStartDateTerm = new List<Date>();
         Date seletedAcademicStartdate;
         Quote_Settings__c qsvalue = Quote_Settings__c.getValues('System Properties');
         if(qsvalue != null){
             if(qsvalue.Academic_Start_Date_Term_1__c != null){
                AcademicStartDateTerm.add(qsvalue.Academic_Start_Date_Term_1__c);
             }
             if(qsvalue.Academic_Start_Date_Term_2__c != null){
                 AcademicStartDateTerm.add(qsvalue.Academic_Start_Date_Term_2__c);
             }
             if(qsvalue.Academic_Start_Date_Term_3__c != null){
                 AcademicStartDateTerm.add(qsvalue.Academic_Start_Date_Term_3__c);
             }
             if(qsvalue.Academic_Start_Date_Term_4__c != null){
                 AcademicStartDateTerm.add(qsvalue.Academic_Start_Date_Term_4__c);
             }
             if(AcademicStartDateTerm != null && !AcademicStartDateTerm.isEmpty()){
                 AcademicStartDateTerm.sort();
                 system.debug('AcademicStartDateTerm :'+ AcademicStartDateTerm);
                 for(Date eachAcademicStartDate: AcademicStartDateTerm){
                     if(system.today() < eachAcademicStartDate){
                         system.debug('add this date :'+ eachAcademicStartDate);
                         seletedAcademicStartdate = eachAcademicStartDate;
                         break;
                     }
                 }
             }
             return seletedAcademicStartdate;
         }
         return null;
    }
    //writen :@Veera Date :7/8/2017
    // 

    public void ValidateOpptyClose(Opportunity[] lstOldOpportunities, Opportunity[] lstNewOpportunities, map<ID, Opportunity> oldMap){
        Set<String> roleStr = new Set<String>();
        list<case> changeCases = new list<case>();
        User currUser = new user();
        String userRole;
        String caseSendingCampus;
        roleStr.add('%Higher Education Consultant%');
        roleStr.add('%Student Advisor%');
        roleStr.add('%Campus Director%');
        Id recordTypeD2L = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('D2L').getRecordTypeId();
        //Id RecId = [SELECT Id FROM RecordType where name = 'D2L'and SobjectType = 'Opportunity'].Id;
        
        list<opportunity> lstOppor = new list<opportunity>();
        list<opportunity> cseOppor = new list<opportunity>();
        Map<Id,Opportunity> MOpport = new Map<Id,Opportunity>();
        
        for (Opportunity opp : lstNewOpportunities)
        {   
            
            Opportunity oldOppor = oldMap.get(opp.Id);
            system.debug('@@OldOppportunity'+oldOppor);
            system.debug('@@newopportunity'+opp);
            system.debug('@@recid'+recordTypeD2L);
            System.debug('@@opp.RecordTypeId '+opp.RecordTypeId);
            
            if(opp.StageName == 'Closed' && opp.RecordTypeId == recordTypeD2L && oldOppor.StageName != 'Closed')
            {
                lstOppor.add(opp);
                MOpport.put(opp.Id,opp);
            }
            
        }
        system.debug('@@lstOppor'+lstOppor);
        
        if(!lstOppor.isEmpty())
        {
            try{
                changeCases = [select id, Current_Campus__c,Opportunity__c from case where RecordType.Name = 'Change Request' and Type = 'Change Campus' and status != 'Closed' and Opportunity__c in: lstOppor];
            system.debug('@@1234'+changeCases);
            }
            catch(Exception e){ changeCases = null;}
            
            
            system.debug('@@caselist'+changeCases);
            if(!changeCases.isEmpty())
            {
                try{
                    currUser = [select id,name,UserRole.Name from user where id =: Userinfo.getUserId() and market__c = 'ZA' and userrole.name like :roleStr];   
                    userRole = currUser.UserRole.Name;
                }
                catch(Exception e){currUser = null;}
                
                
                system.debug('@@currUser: '+currUser);
                system.debug('@@@userrole: '+userRole);
                if(currUser != NULL)
                {
                system.debug('@@currUserinside: '+currUser);
                    for(case cas : changeCases)
                    {
                        if (cas.Current_Campus__c == 'Vanderbijlpark')
                        {
                            caseSendingCampus = 'Vanderbijl';
                        }
                        else
                        {
                            caseSendingCampus = cas.Current_Campus__c.deleteWhitespace().trim();    
                        }
                        
                        String currUserRole = userRole.deleteWhitespace().trim();
                        
                        system.debug('@@caseSendingCampus: '+caseSendingCampus);
                        system.debug('@@currUserRole: '+currUserRole);
                        Integer validateRole = currUserRole.toLowerCase().indexOf(caseSendingCampus.toLowerCase());    
                        system.debug('@@validateRole: '+validateRole);
                        System.debug('@@campuslength'+caseSendingCampus.length());
                        system.debug('@@role length'+currUserRole.length());
                        //if(currUserRole.indexOf(caseSendingCampus)==-1){
                        if(currUserRole.toLowerCase().indexOf(caseSendingCampus.toLowerCase()) == -1){
                            
                            MOpport.get(cas.Opportunity__c).addError('There is an open transfer case for this student account, you may not close the opportunty until this is complete.');
                        }
                            
                    }
                }
                else
                {
                    for(Opportunity opptye : lstOppor)
                    {
                        opptye.addError('There is an open transfer case for this student account, you may not close the opportunty until this is complete.');
                    }
                    
                }
            }
        }
    }
    
    //written by Krishnapriya for CR-01908- Req 21 and Req 122
    //To check whether at least one contact and one competitor are present in the opportunity contact role for the Req CR-01908 Req21
    public void OpportunityContactRoleCount(Opportunity[] lstNewOpportunities, Map<ID, Opportunity> oldMap)
    {
        List<OpportunityContactRole> ocrs = new List<OpportunityContactRole>();
        List<OpportunityCompetitor> competitors = new List<OpportunityCompetitor>();
        Map<Id,OpportunityContactRole> ocrsMap = new Map<Id,OpportunityContactRole>();
        Map<Id,OpportunityCompetitor> competitorsMap = new Map<Id,OpportunityCompetitor>();
        Id recordTypeEnterprise = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Enterprise').getRecordTypeId();
        Id recordTypeB2B = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('B2B').getRecordTypeId();
        
        ocrs = [Select Id, OpportunityId from OpportunityContactRole where OpportunityId IN :lstNewOpportunities];
        competitors = [Select Id, OpportunityId from OpportunityCompetitor where opportunityId IN :lstNewOpportunities]; 
        
        for(OpportunityContactRole ocr : ocrs){
            ocrsMap.put(ocr.OpportunityId, ocr);
        }
        
        for(OpportunityCompetitor competitor : competitors){
            competitorsMap.put(competitor.OpportunityId, competitor);
        }
        
        for(Opportunity oppty: lstNewOpportunities)
        {
            Opportunity oldOpp = oldMap.get(oppty.Id);
            Boolean oldValue = oldOpp.StageName.equals('Needs Analysis');
            Boolean newValue = oppty.StageName.equals('Solutioning');
            Boolean Prequalification=oldOpp.StageName.equals('Prequalification') && !oppty.StageName.equals('Prequalification');            
            
            if(oppty.RecordTypeId == recordTypeEnterprise && oldValue==True && newValue ==True && ocrsMap.get(oppty.Id) == null)
            {
                oppty.addError('Please provide at least one Contact before changing the Stage from Needs Analysis to Solutioning');
            }
           /* if(oppty.RecordTypeId == recordTypeEnterprise && oldValue==True && newValue ==True && competitorsMap.get(oppty.Id) == null)
            {
                oppty.addError('Please provide at least one Competitor before changing the Stage from Needs Analysis to Solutioning');
            }CR-2269 laxman asper cr i have commnet above lines*/
            if(oppty.RecordTypeId == recordTypeB2B && Prequalification==True && ocrsMap.get(oppty.Id) == null)
            {
                oppty.addError('Please provide at least one Contact in the Opportunity Contact Role related list before changing the Stage from Prequalification.');
            }
        }
    }
    
    // Added by Shiva - Start    
    // This method created for the GRL Opportunity product duplicate check
    
    public void Opptyproductduplicate(List<Opportunity> GRLOpptys, Map<ID, Opportunity> newMap, Map<ID, Opportunity> oldmap)
    {   
        system.debug('@@1234'+GRLOpptys);
        Id recordTypeGRL = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Global Rights Sales Opportunity').getRecordTypeId(); 
        Set<id> oppids = new Set<id>();
        List<FeedItem> post = new List<FeedItem>();
        String status;
        String fullFileURL = URL.getSalesforceBaseUrl().toExternalForm();             
                     
        for(Opportunity opty : GRLOpptys)
        {
            system.debug('DupCheck: '+(opty.Check_Duplicates__c != oldmap.get(opty.id).Check_Duplicates__c));
            system.debug('DupNewCheck: '+opty.Check_Duplicates__c);
            system.debug('@234-------->#'+opty+'::: '+oldmap.get(opty.id).Check_Duplicates__c+':opty.RecordTypeId:'+opty.RecordTypeId+':recordTypeGRL:'+recordTypeGRL);
            if(opty.Check_Duplicates__c  == True && opty.Check_Duplicates__c != oldmap.get(opty.id).Check_Duplicates__c && opty.RecordTypeId == recordTypeGRL && !(opty.StageName =='Assumed Declined' || opty.StageName =='Lost' || opty.StageName =='Declined' || opty.StageName =='Cancelled')){
                oppids.add(opty.id); 
            }            
        }
        
        system.debug('#123@'+oppids);
        List<OpportunityLineItem>  Oplitems =[SELECT Id,Product2.Name,Product2id,Opportunity.Id,ISBN__c,Sales_Territory__c,Territory_Africa__c,Territory_Americas__c,Territory_Asia_Oceania__c,Territory_Europe__c,Language__c FROM OpportunityLineItem WHERE Opportunity.Id IN :oppids];
        system.debug('@@@Oplitems: '+Oplitems);
        
        List<Id> Prodids = new List<Id>();
        
        Map<String,OpportunityLineItem> checkmap = new Map<String,OpportunityLineItem>();
        
        String SlsTryString='';
        String TryAfricaString='';
        String TryAmericaString='';
        String TryAsiaString='';
        String TryEuropeString='';
        String LanguageString='';
        Integer size=0;
                
        for(OpportunityLineItem lms: Oplitems) {
            //if((lms.Sales_Territory__c != '' || lms.Territory_Africa__c != '' || lms.Territory_Americas__c != '' || lms.Territory_Asia_Oceania__c != '' || lms.Territory_Europe__c != '') && lms.Language__c != '' ){
                if(lms.Sales_Territory__c != null && lms.Sales_Territory__c != ''){
                    SlsTryString+='\''+(lms.Sales_Territory__c).replace('\'', '\\\'')+'\'';
                } 
                if(lms.Territory_Africa__c != null && lms.Territory_Africa__c != ''){   
                    TryAfricaString+='\''+(lms.Territory_Africa__c).replace('\'', '\\\'')+'\'';
                }
                if(lms.Territory_Americas__c != null && lms.Territory_Americas__c != ''){
                    TryAmericaString+='\''+(lms.Territory_Americas__c).replace('\'', '\\\'')+'\'';
                } 
                if(lms.Territory_Asia_Oceania__c != null && lms.Territory_Asia_Oceania__c != ''){
                    TryAsiaString+='\''+(lms.Territory_Asia_Oceania__c).replace('\'', '\\\'')+'\'';
                }
                if(lms.Territory_Europe__c != null && lms.Territory_Europe__c != ''){
                    TryEuropeString+='\''+(lms.Territory_Europe__c).replace('\'', '\\\'')+'\'';
                }
                if(lms.Language__c != null && lms.Language__c != ''){   
                    LanguageString+='\''+(lms.Language__c).replace('\'', '\\\'')+'\'';
                }    
                size++;
                    if(Oplitems.size()!=size){
                        SlsTryString+=',';  
                        TryAfricaString+=',';
                        TryAmericaString+=',';
                        TryAsiaString+=',';
                        TryEuropeString+=',';
                        LanguageString+=',';
                    }   
            //}         
            Prodids.add(lms.Product2id);
            /*** SlsTry.add(lms.Sales_Territory__c);
            TryAfrica.add(lms.Territory_Africa__c);
            TryAmerica.add(lms.Territory_Americas__c);
            TryAsia.add(lms.Territory_Asia_Oceania__c);
            TryEurope.add(lms.Territory_Europe__c);         
            Language.add(lms.Language__c); ***/
            checkmap.put(lms.Product2id,lms);
            //checkmap.put(lms.Product2id + lms.Sales_Territory__c + lms.Territory_Africa__c + lms.Territory_Americas__c + lms.Territory_Asia_Oceania__c + lms.Territory_Europe__c+ lms.Language__c,lms);
            system.debug('@@@checkmap: '+checkmap);
        }
        
        //TryAfricaString = TryAfricaString.replaceAll('[^a-zA-Z0-9\\s+]', '');     
        string DynamicSoql='SELECT Id,Product2id,ISBN__c,Opportunity.Id,Opportunity.owner.Name, Sales_Territory__c,Territory_Africa__c,Territory_Americas__c,Territory_Asia_Oceania__c,Territory_Europe__c, Language__c FROM OpportunityLineItem WHERE Product2id IN :Prodids';
        //if((SlsTryString != '' || TryAfricaString != '' || TryAmericaString != '' || TryAsiaString != '' || TryEuropeString != '') && LanguageString != ''){
        if(SlsTryString != null && SlsTryString != ''){
            SlsTryString='('+SlsTryString+')';
            DynamicSoql+=' AND Sales_Territory__c includes '+SlsTryString+' ';
        }
        if(TryAfricaString != null && TryAfricaString != ''){   
            TryAfricaString='('+TryAfricaString+')';
            DynamicSoql+=' AND Territory_Africa__c includes '+TryAfricaString+' ';
        }
        if(TryAmericaString != null && TryAmericaString != ''){ 
            TryAmericaString='('+TryAmericaString+')';
            DynamicSoql+=' AND Territory_Americas__c includes '+TryAmericaString+' ';
        }
        if(TryAsiaString != null && TryAsiaString != ''){   
            TryAsiaString='('+TryAsiaString+')';
            DynamicSoql+=' AND Territory_Asia_Oceania__c includes '+TryAsiaString+' ';
        }
        if(TryEuropeString != null && TryEuropeString != ''){   
            TryEuropeString='('+TryEuropeString+')';
            DynamicSoql+=' AND Territory_Europe__c includes '+TryEuropeString+' ';
        }
        if(LanguageString != null && LanguageString != ''){ 
            LanguageString='('+LanguageString+')';
            DynamicSoql+=' AND Language__c includes '+LanguageString+' ';
            
        }   
       
        if((SlsTryString != '' || TryAfricaString != '' || TryAmericaString != '' || TryAsiaString != '' || TryEuropeString != '') && LanguageString != ''){
            
        //DynamicSoql+=' AND (Sales_Territory__c includes '+SlsTryString+') OR (Territory_Africa__c includes '+TryAfricaString+') OR (Territory_Americas__c includes '+TryAmericaString+') OR (Territory_Asia_Oceania__c includes '+TryAsiaString+') OR (Territory_Europe__c includes '+TryEuropeString+') AND (Language__c includes '+LanguageString+') ';     
        //}
        DynamicSoql+=' AND (Opportunity.id NOT IN :Oppids) AND Opportunity.StageName NOT IN (\'Assumed Declined\',\'Lost\',\'Declined\',\'Cancelled\') ';
        System.debug('@@@DynamicSoql'+DynamicSoql);
        system.debug('$$$TryAfricaString: '+TryAfricaString);   
              
        //List<OpportunityLineItem>  srchlitems =[SELECT Id,Product2id,ISBN__c,Opportunity.Id,Opportunity.owner.Name, Sales_Territory__c,Territory_Africa__c,Territory_Americas__c,Territory_Asia_Oceania__c,Territory_Europe__c, Language__c FROM OpportunityLineItem WHERE Product2id IN :Prodids AND (Sales_Territory__c IN :SlsTry OR Territory_Africa__c IN:TryAfrica OR Territory_Americas__c IN :TryAmerica OR Territory_Asia_Oceania__c IN :TryAsia OR Territory_Europe__c IN :TryEurope) AND Language__c IN :Language AND Opportunity.id NOT IN :Oppids AND Opportunity.StageName NOT IN ('Assumed Declined','Lost','Declined','Cancelled') ];
        List<OpportunityLineItem>  srchlitems =database.query(DynamicSoql);
        system.debug('@@@srchlitems: '+srchlitems);        
        
        Map<Id, Map<String, String>> details = new Map<Id, Map<String, String>>();
        Map<String, String> detail = new Map<String, String>();
        
        for(OpportunityLineItem lms: srchlitems) {
            
            //if(checkmap.containskey(lms.Product2id + lms.Sales_Territory__c + lms.Territory_Africa__c + lms.Territory_Americas__c + lms.Territory_Asia_Oceania__c + lms.Territory_Europe__c + lms.Language__c)){
               // OpportunityLineItem getlinitem = checkmap.get(lms.Product2id + lms.Sales_Territory__c + lms.Territory_Africa__c + lms.Territory_Americas__c + lms.Territory_Asia_Oceania__c + lms.Territory_Europe__c + lms.Language__c);
            if(checkmap.containskey(lms.Product2id)){
                OpportunityLineItem getlinitem = checkmap.get(lms.Product2id );
                system.debug('@@@7777777: ');
                Opportunity curntrec = newmap.get(getlinitem.opportunityid);
                /** FeedItem fd = new FeedItem();                                                  
                status = +'@'+lms.Opportunity.owner.Name+' you have marked this Opportunity as Exclusive. There already exists an open Opportunity for <b>'+getlinitem.Product2.Name+'  '+lms.ISBN__c+'</b> that matches the territory and language that you have selected on this Opportunity.'+fullFileURL+'/'+lms.opportunityid;                
                fd.ParentId = curntrec.Id;
                fd.IsRichText=true; 
                fd.Body = status;
                post.add(fd); **/
                status = ' You have marked Check Duplicates for this Opportunity. There already exists an open Opportunity for "'+getlinitem.Product2.Name+'  '+lms.ISBN__c+'" that matches the territory and language that you have selected on this Opportunity.'+fullFileURL+'/'+lms.opportunityid;

                detail.put('userId', curntrec.ownerId);
                detail.put('recordId', curntrec.Id);
                detail.put('status', status);
                details.put(Trigger.New[0].Id, detail);
                PS_ChatterPostUtilities.createPostOnOpportunity(details); 
            }
        } 
      }
    }
    // Added by Shiva - End
}